<!DOCTYPE html>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Script-Type" content="text/javascript">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>システム情報 - ${systemName} systemManage</title>
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/bootstrap/css/bootstrap.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/common.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/simple-sidebar.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/sweetalert2.css" />" />
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/polyfill.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/sweetalert2.all.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jsAlert.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.custom.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/messages_const.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/system_manager.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/system_infomanager.js" />" ></script>
	<script type="text/javascript">
		$(function(){
			/* ブラウザ戻るボタン抑止 */
			history.forward();
		});
	</script>
</head>
<body>
	<div class="container-fluid full-height">
		<div class="row full-height toggled" id="wrapper">
			<div class="col-md-2 gray full-height" id="sidebar-wrapper">
				<p>${systemName}<br />画像共有システム</p>
				<h4>システム管理メニュー</h4>
				<ul class="nav nav-pills nav-stacked">
					<li role="presentation" class="active"><a href="javaScript:void(0)" id="systeminfo"><span class="glyphicon glyphicon-info-sign" aria-hidden="true"></span><span>　システム情報</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="tenant"><span class="glyphicon glyphicon-home blue" aria-hidden="true"></span><span class="blue">　テナント</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="tenant_manager"><span class="glyphicon glyphicon-user blue" aria-hidden="true"></span><span class="blue">　テナント管理者</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="accessedlog"><span class="glyphicon glyphicon-time blue" aria-hidden="true"></span><span class="blue">　アクセスログ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="receivedlog"><span class="glyphicon glyphicon-download-alt blue" aria-hidden="true"></span><span class="blue">　受信ログ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id=logout><span class="glyphicon glyphicon-log-out blue" aria-hidden="true"></span><span class="blue">　ログアウト</span></a></li>
				</ul>
			</div>
			<div class="col-md-10 bg-gray full-height scroll-y" id="page-content-wrapper">
				<form:form modelAttribute="systemManageSystemInfoForm">
				<!-- ユーザID -->
				<form:input type="hidden" id="acc_userID" path="acc_userID" class="form-control" value='${acc_userID}' />
					<div class="container-fluid">
						<div class="row form-group"></div>
						<div class="row form-group">
							<c:if test="${not empty message}">
								<div class="alert alert-success alert-dismissible fade in" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${message}
								</div>
							</c:if>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">システム名</label>
							</div>
							<div class="col-sm-8">
								<form:input path="system_name" class="form-control" maxlength="30" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">システムルートパス</label>
							</div>
							<div class="col-sm-8">
								<form:input path="rootPath" class="form-control" maxlength="30" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">内部URL</label>
							</div>
							<div class="col-sm-8">
								<form:input path="inUrl" class="form-control" maxlength="500" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">外部URL</label>
							</div>
							<div class="col-sm-8">
								<form:input path="outUrl" class="form-control" maxlength="500" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">ライブラリ画像</label>
							</div>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="file" id="file_input" name="file_input" accept=".png, .jpg, .jpeg, .gif, .bmp" style="display: none;" />
									<span class="input-group-btn" style="vertical-align: top;">
										<button class="btn btn-primary" type="button" onclick="$('#file_input').click();"><i class="glyphicon glyphicon-folder-open"></i></button>
									</span>
									<form:input path="directory_image" id="directory_image" class="form-control" placeholder="ファイルを選択してください" readonly="true" value='${comment}' />
								</div>
								<br>
								<img id="thumbnail" src='${data_src}' />
								<form:input path="data_src" id="data_src" class="form-control" type="hidden" value='${data_src}' />
							</div>
						</div>
						<div class="row">
							<div class="center">
								<button type="submit" name="update" class="btn btn-primary"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&thinsp;更新</button>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<spring:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
