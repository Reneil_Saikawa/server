<!DOCTYPE html>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Script-Type" content="text/javascript">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>テナント - ${systemName} systemManage</title>
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/bootstrap/css/bootstrap.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/bootstrap-datetimepicker.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/datatables.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/common.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/simple-sidebar.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/sweetalert2.css" />" />
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/polyfill.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/sweetalert2.all.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jsAlert.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.custom.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/moment.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/bootstrap-datetimepicker.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/ja.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/datatables.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/messages_const.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/system_manager.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/tenant.js" />" ></script>
</head>
<body>
	<div class="container-fluid full-height">
		<div class="row full-height toggled" id="wrapper">
			<div class="col-md-2 gray full-height" id="sidebar-wrapper">
				<p>${systemName}<br />画像共有システム</p>
				<h4>システム管理メニュー</h4>
				<ul class="nav nav-pills nav-stacked">
					<li role="presentation"><a href="javaScript:void(0)" id="systeminfo"><span class="glyphicon glyphicon-info-sign blue" aria-hidden="true"></span><span class="blue">　システム情報</span></a></li>
					<li role="presentation" class="active"><a href="javaScript:void(0)" id="tenant"><span class="glyphicon glyphicon-home" aria-hidden="true"></span><span>　テナント</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="tenant_manager"><span class="glyphicon glyphicon-user blue" aria-hidden="true"></span><span class="blue">　テナント管理者</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="accessedlog"><span class="glyphicon glyphicon-time blue" aria-hidden="true"></span><span class="blue">　アクセスログ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="receivedlog"><span class="glyphicon glyphicon-download-alt blue" aria-hidden="true"></span><span class="blue">　受信ログ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id=logout><span class="glyphicon glyphicon-log-out blue" aria-hidden="true"></span><span class="blue">　ログアウト</span></a></li>
				</ul>
			</div>
			<div class="col-md-10 bg-gray full-height scroll-y" id="page-content-wrapper">
				<form:form modelAttribute="systemManageTenantForm">
				<!-- ユーザID -->
				<form:input type="hidden" id="acc_userID" path="acc_userID" class="form-control" value='${acc_userID}' />
					<div class="container-fluid">
						<div class="row form-group"></div>
						<div class="row form-group">
							<c:if test="${not empty success}">
								<div class="alert alert-success alert-dismissible fade in" id="success" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${success}
								</div>
							</c:if>
							<c:if test="${not empty add_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="add_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${add_danger}
								</div>
							</c:if>
							<c:if test="${not empty user_license_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="user_license_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${user_license_danger}
								</div>
							</c:if>
							<c:if test="${not empty term_license_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="term_license_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${term_license_danger}
								</div>
							</c:if>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">テナントID</label>
							</div>
							<div class="col-sm-5">
								<form:input path="tenant_id" id="tenant_id" class="form-control" maxlength="16" readonly="false" style="ime-mode: disabled;" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">テナント名</label>
							</div>
							<div class="col-sm-8">
								<form:input path="tenant_name" id="tenant_name" class="form-control" maxlength="50" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">ライセンス数</label>
							</div>
							<div class="col-sm-1">
								<label class="control-label">ユーザー</label>
							</div>
							<div class="col-sm-2">
								<form:input path="user_license" id="user_license" class="form-control" maxlength="4" />
							</div>
							<div class="col-sm-1 col-sm-offset-1">
								<label class="control-label">端末</label>
							</div>
							<div class="col-sm-2">
								<form:input path="term_license" id="term_license" class="form-control" maxlength="4" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2" style="padding: 15px 0px 0px 15px;">
								<label class="control-label">端末機能</label>
							</div>
							<div class="col-sm-8">
								<table class="table table-bordered dataTable" id="accessTable" style="table-layout: fixed; background-color: white;">
									<thead>
										<tr align="center">
											<th style="text-align: center;">静止画撮影</th>
											<th style="text-align: center;">動画撮影</th>
											<th style="text-align: center;">ライブ撮影</th>
										</tr>
									</thead>
									<tbody>
										<tr  align="center">
											<td><form:checkbox path="stillimage_function" id="stillimage_function" class="chk" /></td>
											<td><form:checkbox path="movie_function" id="movie_function" class="chk" /></td>
											<td><form:checkbox path="live_function" id="live_function" class="chk" onchange="checkLiveEnable();" /></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<div class="collapse" id="disLiveEnable">
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">ライブ接続先</label>
								</div>
								<div class="col-sm-8">
									<form:input path="live_server" id="live_server" class="form-control" maxlength="500" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">ライブアプリ名</label>
								</div>
								<div class="col-sm-8">
									<form:input path="live_app" id="live_app" class="form-control" maxlength="50" style="ime-mode: disabled;" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">ライブルーム名</label>
								</div>
								<div class="col-sm-8">
									<form:input path="live_room" id="live_room" class="form-control" maxlength="50" style="ime-mode: disabled;" />
								</div>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">端末-起動時一時ファイル削除</label>
							</div>
							<div class="col-sm-1" style="padding: 0px 0px 0px 18px;">
								<form:checkbox path="start_tmp_del" id="start_tmp_del" class="chk" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">端末-一時ファイル時間経過削除</label>
							</div>
							<div class="col-sm-2">
								<form:select path="tmp_del_time" id="tmp_del_time" items="${tmpDelTime}" itemLabel="label" itemValue="value" class="form-control" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">端末-アプリ自動停止時間</label>
							</div>
							<div class="col-sm-2">
								<form:select path="stop_time" id="stop_time" items="${stopTime}" itemLabel="label" itemValue="value" class="form-control" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">パスワードエラーロック回数</label>
							</div>
							<div class="col-sm-2">
								<form:select path="password_lock_times" id="password_lock_times" items="${pwdErrCount}" itemLabel="label" itemValue="value" class="form-control" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">サムネイルサイズ (pixel)</label>
							</div>
							<div class="col-sm-1">
								<label class="control-label">幅</label>
							</div>
							<div class="col-sm-2">
								<form:input path="thumbnail_width" id="thumbnail_width" class="form-control" maxlength="3" />
							</div>
							<div class="col-sm-1 col-sm-offset-1">
								<label class="control-label">高さ</label>
							</div>
							<div class="col-sm-2">
								<form:input path="thumbnail_height" id="thumbnail_height" class="form-control" maxlength="3" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">パトライト-有効化</label>
							</div>
							<div class="col-sm-1" style="padding: 0px 0px 0px 18px;">
								<form:checkbox path="patlite_enable" id="patlite_enable" class="chk" onchange="checkPatliteEnable();" />
							</div>
						</div>
						<div class="collapse" id="disPatliteEnable">
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">パトライト-アドレス</label>
								</div>
								<div class="col-sm-8">
									<form:input path="patLite_ip" id="patLite_ip" class="form-control" maxlength="40" style="ime-mode: disabled;" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">パトライト-ログイン名</label>
								</div>
								<div class="col-sm-8">
									<form:input path="patLite_login_name" id="patLite_login_name" class="form-control" maxlength="32" style="ime-mode: disabled;" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">パトライト-受信時オプション</label>
								</div>
								<div class="col-sm-3">
									<form:input path="patLite_option_rcv" id="patLite_option_rcv" class="form-control" maxlength="6" style="ime-mode: disabled;" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">パトライト-受信時鳴動時間(秒)</label>
								</div>
								<div class="col-sm-2">
									<form:select path="patLite_time_rcv" id="patLite_time_rcv" items="${patLiteTime}" itemLabel="label" itemValue="value" class="form-control" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">パトライト-警告時オプション</label>
								</div>
								<div class="col-sm-3">
									<form:input path="patLite_option_warm" id="patLite_option_warm" class="form-control" maxlength="6" style="ime-mode: disabled;" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">パトライト-警告時鳴動時間(秒)</label>
								</div>
								<div class="col-sm-2">
									<form:select path="patLite_time_warm" id="patLite_time_warm" items="${patLiteTime}" itemLabel="label" itemValue="value" class="form-control" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">パトライト-エラー時オプション</label>
								</div>
								<div class="col-sm-3">
									<form:input path="patLite_option_err" id="patLite_option_err" class="form-control" maxlength="6" style="ime-mode: disabled;" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">パトライト-エラー時鳴動時間(秒)</label>
								</div>
								<div class="col-sm-2">
									<form:select path="patLite_time_err" id="patLite_time_err" items="${patLiteTime}" itemLabel="label" itemValue="value" class="form-control" />
								</div>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">地図-有効化</label>
							</div>
							<div class="col-sm-1" style="padding: 0px 0px 0px 18px;">
								<form:checkbox path="map_enable" id="map_enable" class="chk" onchange="checkMapEnable();" />
							</div>
						</div>
						<div class="collapse" id="disMapEnable">
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">地図-サーバ</label>
								</div>
								<div class="col-sm-8">
									<form:input path="map_server" id="map_server" class="form-control" maxlength="500" />
								</div>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">帳票-有効化</label>
							</div>
							<div class="col-sm-1" style="padding: 0px 0px 0px 18px;">
								<form:checkbox path="report_enable" id="report_enable" class="chk" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">ファイルパス表示</label>
							</div>
							<div class="col-sm-1" style="padding: 0px 0px 0px 18px;">
								<form:checkbox path="display_file_path" id="display_file_path" class="chk" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">有効期限</label>
							</div>
							<div class="col-sm-2">
								<div class="input-group date" id="datetimepicker-start">
									<form:input path="start_date" id="start_date" class="form-control" style="ime-mode: disabled; background-color: white" readonly="true" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<div class="error" id="start_date_err" ></div>
							</div>
							<div class="col-sm-1 col text-center" style="padding: 0; line-height:34px;">～</div>
							<div class="col-sm-2">
								<div class="input-group date" id="datetimepicker-end">
									<form:input path="end_date" id="end_date" class="form-control" style="ime-mode: disabled; background-color: white" readonly="true" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<div class="error" id="end_date_err" ></div>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">削除フラグ</label>
							</div>
							<div class="col-sm-1" style="padding: 0px 0px 0px 18px;">
								<form:checkbox path="del_flg" id="del_flg" class="chk" />
							</div>
						</div>
						<div class="row">
							<div class="center">
								<button type="button" name="list" id="list" class="btn btn-default"><span class="glyphicon glyphicon-list" aria-hidden="true"></span>&thinsp;一覧</button>
								<button type="button" id="clearInputArea" class="btn btn-default">クリア</button>
								<button type="submit" name="add" id="reflect" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&thinsp;追加</button>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
	<!-- 一覧表示モーダルダイアログ -->
	<div class="modal fade" id="listModal" tabindex="-1">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
					<h4 class="modal-title"><span class="glyphicon glyphicon-home" aria-hidden="true">&thinsp;テナント一覧</span></h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered dataTable" id="dataTable" width="100%">
						<thead>
							<tr>
								<th />
								<th>テナントID</th>
								<th>テナント名</th>
								<th>ユーザーライセンス数</th>
								<th>端末ライセンス数</th>
								<th>静止画撮影</th>
								<th>動画撮影</th>
								<th>ライブ撮影</th>
								<th style="display: none;">端末-起動時一時ファイル削除</th>
								<th style="display: none;">端末-一時ファイル時間経過削除(時間)</th>
								<th style="display: none;">端末-アプリ自動停止時間(分)</th>
								<th style="display: none;">パスワードエラーカウント</th>
								<th style="display: none;">サムネイルサイズ(横)</th>
								<th style="display: none;">サムネイルサイズ(縦)</th>
								<th style="display: none;">パトライト-有効化</th>
								<th style="display: none;">パトライト-アドレス</th>
								<th style="display: none;">パトライト-ログイン名</th>
								<th style="display: none;">パトライト-受信時オプション</th>
								<th style="display: none;">パトライト-受信時鳴動時間(秒)</th>
								<th style="display: none;">パトライト-警告時オプション</th>
								<th style="display: none;">パトライト-警告時鳴動時間(秒)</th>
								<th style="display: none;">パトライト-エラー時オプション</th>
								<th style="display: none;">パトライト-エラー時鳴動時間(秒)</th>
								<th style="display: none;">ライブ接続先</th>
								<th style="display: none;">ライブアプリ名</th>
								<th style="display: none;">ライブルーム名</th>
								<th style="display: none;">地図-有効化</th>
								<th style="display: none;">地図-サーバ</th>
								<th style="display: none;">帳票-有効化</th>
								<th style="display: none;">ファイルパス表示</th>
								<th>有効開始日</th>
								<th>有効終了日</th>
								<th>削除フラグ</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${list}">
								<tr class="sel_row" style="cursor:pointer">
									<td class="center"><input type="radio" name="r_chk"/></td>
									<td>
										<c:out value="${item.tenant_id}" />
									</td>
									<td>
										<c:out value="${item.tenant_name}" />
									</td>
									<td>
										<c:out value="${item.user_license}" />
									</td>
									<td>
										<c:out value="${item.term_license}" />
									</td>
									<td>
										<c:out value="${item.str_stillimage_function}" />
									</td>
									<td>
										<c:out value="${item.str_movie_function}" />
									</td>
									<td>
										<c:out value="${item.str_live_function}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.str_start_tmp_del}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.tmp_del_time}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.stop_time}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.password_lock_times}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.thumbnail_width}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.thumbnail_height}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.str_patlite_enable}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.patLite_ip}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.patLite_login_name}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.patLite_option_rcv}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.patLite_time_rcv}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.patLite_option_warm}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.patLite_time_warm}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.patLite_option_err}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.patLite_time_err}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.live_server}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.live_app}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.live_room}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.str_map_enable}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.map_server}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.str_report_enable}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.str_display_file_path}" />
									</td>
									<td>
										<c:out value="${item.str_start_date}" />
									</td>
									<td>
										<c:out value="${item.str_end_date}" />
									</td>
									<td>
										<c:out value="${item.str_del_flg}" />
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
				    <button type="button" class="btn btn-default" id="edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>&thinsp;編集</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<spring:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
