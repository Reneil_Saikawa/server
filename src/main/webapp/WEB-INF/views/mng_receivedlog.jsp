<!DOCTYPE html>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Script-Type" content="text/javascript">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>受信ログ - ${systemName}</title>
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/bootstrap/css/bootstrap.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/bootstrap-datetimepicker.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/datatables.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/common.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/simple-sidebar.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/sweetalert2.css" />" />
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/polyfill.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/sweetalert2.all.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jsAlert.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.custom.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/moment.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/bootstrap-datetimepicker.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/ja.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/datatables.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/messages_const.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/manager.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/mng_receivedlog.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/dataTables.buttons.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/buttons.flash.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jszip.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/pdfmake.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/vfs_fonts_japanese.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/buttons.html5.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/buttons.print.min.js" />" ></script>
</head>
<body>
	<div id="loading">
		<div class="loader">Loading...</div>
	</div>
	<div class="container-fluid full-height">
		<div class="row full-height toggled" id="wrapper">
			<div class="col-md-2 gray full-height" id="sidebar-wrapper">
				<p>${systemName}<br />画像共有システム</p>
				<h4>管理メニュー</h4>
				<ul class="nav nav-pills nav-stacked">
					<li role="presentation"><a href="javaScript:void(0)" id="library"><span class="glyphicon glyphicon-folder-close blue" aria-hidden="true"></span><span class="blue">　ライブラリ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="terminal"><span class="glyphicon glyphicon-phone blue" aria-hidden="true"></span><span class="blue">　端末</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="user"><span class="glyphicon glyphicon-user blue" aria-hidden="true"></span><span class="blue">　ユーザー</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="additionalitem"><span class="glyphicon glyphicon-edit blue" aria-hidden="true"></span><span class="blue">　付帯情報項目名</span></a></li>
					<c:if test= "${!empty reportEnable}">
						<li role="presentation"><a href="javaScript:void(0)" id="reportparam"><span class="glyphicon glyphicon-file blue" aria-hidden="true"></span><span class="blue">　帳票</span></a></li>
					</c:if>
					<li role="presentation"><a href="javaScript:void(0)" id="accessedlog"><span class="glyphicon glyphicon-time blue" aria-hidden="true"></span><span class="blue">　アクセスログ</span></a></li>
					<li role="presentation" class="active"><a href="javaScript:void(0)" id="receivedlog"><span class="glyphicon glyphicon-download-alt" aria-hidden="true"></span><span>　受信ログ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="close"><span class="blue">閉じる</span></a></li>
				</ul>
			</div>
			<div class="col-md-10 bg-gray full-height scroll-y" id="page-content-wrapper">
				<form:form modelAttribute="mngReceivedLogForm">
				<!-- ユーザID -->
				<form:input type="hidden" id="acc_userID" path="acc_userID" class="form-control" value='${acc_userID}' />
				<!-- テナントID -->
				<input type="hidden" id="tenantId" value='${tenantId}' />
					<div class="container-fluid">
						<div class="row form-group"></div>
						<div class="row form-group"></div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">期間</label>
							</div>
							<div class="col-sm-2">
								<div class="input-group date" id="datetimepicker-start">
									<form:input path="start_date" id="start_date" class="form-control" style="ime-mode: disabled; background-color: white" readonly="true" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<div class="error" id="start_date_err" ></div>
							</div>
							<div class="col-sm-1 col text-center" style="padding: 0; line-height:34px;">～</div>
							<div class="col-sm-2">
								<div class="input-group date" id="datetimepicker-end">
									<form:input path="end_date" id="end_date" class="form-control" style="ime-mode: disabled; background-color: white" readonly="true" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<div class="error" id="end_date_err" ></div>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">端末</label>
							</div>
							<div class="col-sm-8">
								<form:select path="term_id" id="term_id" class="form-control" >
									<option value=""></option>
									<form:options items="${term}" itemLabel="term_name" itemValue="term_id"/>
								</form:select>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">受信区分</label>
							</div>
							<div class="col-sm-8">
								<form:select path="receive_code" id="receive_code" class="form-control" >
									<option value=""></option>
									<form:options items="${receive}" itemLabel="name_01" itemValue="code"/>
								</form:select>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">処理結果</label>
							</div>
							<div class="col-sm-2">
								<form:select path="result_code" id="result_code" class="form-control" >
									<option value=""></option>
									<form:options items="${result}" itemLabel="name_01" itemValue="code"/>
								</form:select>
							</div>
						</div>
						<div class="row">
							<div class="center">
								<button type="button" id="clearInputArea" class="btn btn-default">クリア</button>
								<button type="button" id="search" class="btn btn-primary"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>&thinsp;検索</button>
								<button type="submit" name="searchReceivelogCsv" id="searchReceivelogCsv" class="btn btn-success"><span class="glyphicon glyphicon-file" aria-hidden="true"></span>&thinsp;CSVダウンロード</button>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-12">
								<div id="logArea">
									<!-- ログ表示エリア -->
								</div>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
	<!-- ログ詳細モーダルダイアログ -->
	<div class="modal fade" id="logModal" tabindex="-1">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
					<h4 class="modal-title"><span class="glyphicon glyphicon-list-alt" aria-hidden="true">&thinsp;ログ詳細</span></h4>
				</div>
				<div class="modal-body">
					<div id="logDetailsArea">
						<!-- ログ詳細表示エリア -->
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<spring:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
