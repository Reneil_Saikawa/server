<!DOCTYPE html>
<!-- <html xmlns:th="http://www.thymeleaf.org" lang="ja"> -->
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Script-Type" content="text/javascript">
	<!--<meta name="viewport" content="width=device-width, initial-scale=1">-->
	<!--<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">-->
	<!--<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">-->
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>${systemName}</title>
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/bootstrap/css/bootstrap.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/bootstrap-treeview.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/bootstrap-datetimepicker.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/datatables.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/leaflet.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/iziModal.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/viewer.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/simple-sidebar.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/sweetalert2.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/common.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/main.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/liveViewer.css" />" />
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/polyfill.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/sweetalert2.all.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jsAlert.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/bootstrap-treeview.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/iziModal.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/viewer.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.pagination.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/datatables.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/leaflet.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/leaflet.zoomfs.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/js.cookie.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/main.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/moment.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/bootstrap-datetimepicker.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/ja.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/dataTables.buttons.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/buttons.flash.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jszip.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/pdfmake.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/vfs_fonts_japanese.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/buttons.html5.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/buttons.print.min.js" />" ></script>
	<c:if test= "${(!empty liveEnable) && (empty notLiveBrowser)}">
		<!-- WebRTCサーバーの判定追加 2021-05-29 -->
		<c:choose>
			<c:when test="${liveServer == 'SkyWay'}">
				<script type="text/javascript" src="//cdn.webrtc.ecl.ntt.com/skyway-latest.js"></script>
				<script type="text/javascript" src="<spring:url value="/resources/js/liveViewer.js" />"></script>
			</c:when>
			<c:when test="${liveServer == 'Janus'}">
				<script type="text/javascript" src="<spring:url value="/resources/js/liveArea.js" />"></script>
			</c:when>
		</c:choose>
	</c:if>
</head>
<body style="background-color:#fff;">
	<div id="loading">
		<div class="loader">Loading...</div>
	</div>
	<form:form modelAttribute="mainForm">
	<!-- ユーザID -->
	<input type="hidden" id="acc_userID" value='${acc_userID}' />
	<c:if test= "${(!empty liveEnable) && (empty notLiveBrowser)}">
		<!-- ライブ情報 -->
		<input type="hidden" id="liveApp" value='${liveApp}' />
		<input type="hidden" id="liveRoom" value='${liveRoom}' />
		<!-- Janus  -->
		<input type="hidden" id="userName" value='${userName}' />
	</c:if>
	<c:if test= "${!empty mapEnable}">
		<!-- 地図サーバー -->
		<input type="hidden" id="mapServer" value='${mapServer}' />
		<!-- システム座標 -->
		<input type="hidden" id="defaultLocateLat" value='36.3912095' />
		<input type="hidden" id="defaultLocateLon" value='139.0486054' />
	</c:if>
	<!-- 付帯情報検索表示判定 -->
	<input type="hidden" id="searchInfoFlg" value='${searchInfoFlg}' />
	<div class="container-fluid full-height">
		<div class="row full-height" id="wrapper">
			<div class="white full-height" id="sidebar-wrapper" style="white-space:nowrap">
				<p>${systemName}<br />画像共有システム</p>
				<h5>ユーザー　　：${userName} さん</h5>
				<h5>前回ログイン：${lastLoginTime}</h5>
				<ul class="nav nav-pills nav-stacked">
					<c:if test= "${(auth == '0198') || (!empty manageAuth)}">
						<li role="presentation"><a href="JavaScript:void(0)" id="manager"><span class="glyphicon glyphicon-cog blue" aria-hidden="true"></span><span class="blue">　管理</span></a></li>
					</c:if>
					<li role="presentation"><a href="JavaScript:void(0)" class="logout"><span class="glyphicon glyphicon-log-out blue" aria-hidden="true"></span><span class="blue">　ログアウト</span></a></li>
				</ul>
				<h4>ライブラリ</h4>
				<!-- ツリー表示 -->
				<div id="tree"></div>
				<input type="hidden" id="directoryTreeData" value='${directoryTreeData}' />
			</div>
			<div class="col-md-10" id="page-content-wrapper" style="padding : 0px 20px 0px">
				<div class="row">
					<div class='${mainSize}'>
						<div class="row">
						<!-- topメニューエリア -->
							<div class="top-area">
								<div class="top-menu ${mainSize}">
									<table>
										<tbody>
											<tr>
												<td style="width:32px;">
													<a class="menu-trigger" style="cursor:pointer">
														<span></span>
														<span></span>
														<span></span>
													</a>
												</td>
												<td style="width:100%; min-width:125px;">
													<div id="current-library"></div>
												</td>
												<td align="right" style="width:190px;">
													<ul class="nav nav-pills" style="width:190px;">
														<li role="presentation" id="searchLi" title="検索">
															<a href="JavaScript:void(0)" class="top-menu-a" id="searchOpen">
																<span class="glyphicon glyphicon-search" aria-hidden="true"></span>
															</a>
														</li>
														<li role="presentation" class="dropdown" id="displayLi" title="表示">
															<a class="dropdown-toggle top-menu-a" data-toggle="dropdown" href="JavaScript:void(0)" role="button" aria-expanded="false" id="displayDrop">
																<span class="glyphicon glyphicon-th-large" aria-hidden="true"></span><span class='caret'></span>
															</a>
															<ul class="dropdown-menu dropdown-menu-right" role="menu">
																<li role="presentation"><a href="JavaScript:void(0)" id="viewIconXl"><span class="displayChoice" id="viewIconXlCh">　</span>特大アイコン</a></li>
																<li role="presentation"><a href="JavaScript:void(0)" id="viewIconL"><span class="displayChoice" id="viewIconLCh">　</span>大アイコン</a></li>
																<li role="presentation"><a href="JavaScript:void(0)" id="viewIconM"><span class="displayChoice" id="viewIconMCh">✓</span>中アイコン</a></li>
																<li role="presentation"><a href="JavaScript:void(0)" id="viewIconS"><span class="displayChoice" id="viewIconSCh">　</span>小アイコン</a></li>
																<li role="presentation"><a href="JavaScript:void(0)" id="viewDetail"><span class="displayChoice" id="viewDetailCh">　</span>詳細</a></li>
																<c:if test= "${!empty mapEnable}">
																	<li role="presentation"><a href="JavaScript:void(0)" id="viewMap"><span class="displayChoice" id="viewMapCh">　</span>地図</a></li>
																</c:if>
																<li role="presentation" class="viewIcon" style="border-top:1px solid #DDDDDD"><a href="JavaScript:void(0)" id="viewFileName"><span class="displayInfoChoice" id="viewFileNameCh">　</span>ファイル名</a></li>
																<li role="presentation" class="viewIcon"><a href="JavaScript:void(0)" id="viewPhotoInfo"><span class="displayInfoChoice" id="viewPhotoInfoCh">✓</span>撮影情報</a></li>
																<li role="presentation" style="border-top:1px solid #DDDDDD"><a href="JavaScript:void(0)" id="update">　最新の情報に更新（F5）</a></li>
															</ul>
														</li>
														<li role="presentation" class="dropdown" id="sortLi" title="並べ替え">
															<a class="dropdown-toggle top-menu-a" data-toggle="dropdown" href="JavaScript:void(0)" role="button" aria-expanded="false" id="sortDrop">
																<span class="glyphicon glyphicon-sort-by-attributes-alt" aria-hidden="true"></span><span class='caret'></span>
															</a>
															<ul class="dropdown-menu dropdown-menu-right" role="menu">
																<li role="presentation"><a href="JavaScript:void(0)" id="librarySort"><span class="sortChoice" id="librarySortCh">　</span>ライブラリ</a></li>
																<li role="presentation"><a href="JavaScript:void(0)" id="nameSort"><span class="sortChoice" id="nameSortCh">　</span>ファイル名</a></li>
																<li role="presentation"><a href="JavaScript:void(0)" id="termSort"><span class="sortChoice" id="termSortCh">　</span>撮影端末</a></li>
																<li role="presentation" style="border-bottom:1px solid #DDDDDD"><a href="JavaScript:void(0)" id="dateTimeSort"><span class="sortChoice" id="dateTimeSortCh">✓</span>撮影日時</a></li>
																<li role="presentation"><a href="JavaScript:void(0)" id="ascSort"><span class="sortOrder" id="ascSortCh">　</span>昇順</a></li>
																<li role="presentation"><a href="JavaScript:void(0)" id="descSort"><span class="sortOrder" id="descSortCh">✓</span>降順</a></li>
															</ul>
														</li>
														<li role="presentation" title="ログアウト">
															<a href="JavaScript:void(0)" class="logout top-menu-a">
																<span class="glyphicon glyphicon-log-out" aria-hidden="true"></span>
															</a>
														</li>
													</ul>
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
							<!-- bottomメニューエリア -->
							<div class="bottom-area">
								<div class="bottom-menu ${mainSize}">
									<table>
										<tbody>
											<tr>
												<td style="width:0.1px;">
												</td>
												<td align="center" style="width:100%;">
													<table style="width:80%; vertical-align:top;">
														<tbody>
															<tr>
																<td>
																	<ul class="nav nav-pills bottom-menu-ul">
																		<c:if test="${!empty reportEnable}">
																			<li role="presentation" class="dropup menu-li" id="formOutputLi">
																				<a class="dropdown-toggle bottom-menu-a" data-toggle="dropdown" href="JavaScript:void(0)" role="button" aria-expanded="false" id="formOutputDrop">
																					<span class="glyphicon glyphicon-file menu-icon" aria-hidden="true"><span class="menu-char"><br>帳票作成</span></span>
																				</a>
																				<ul class="dropdown-menu" role="menu">
																					<c:forEach var="report" items="${report}">
																						<li role="presentation"><a href="JavaScript:void(0)" onclick="reportOutput('${report.report_id}');">${report.report_name}</a></li>
																					</c:forEach>
																				</ul>
																			</li>
																		</c:if>
																		<li role="presentation" class="menu-li" id="additionalInfoLi">
																			<a href="JavaScript:void(0)" class="bottom-menu-a" id="additionalInfo">
																				<span class="glyphicon glyphicon-edit menu-icon" aria-hidden="true"><span class="menu-char"><br>付帯情報</span></span>
																			</a>
																		</li>
																		<c:if test="${(auth == '0198') || (!empty downloadAuth)}">
																			<li role="presentation" class="menu-li" id="downloadLi">
																				<a href="JavaScript:void(0)" class="bottom-menu-a" id="download">
																					<span class="glyphicon glyphicon-download-alt menu-icon" aria-hidden="true"><span class="menu-char"><br>ダウンロード</span></span>
																				</a>
																			</li>
																		</c:if>
																		<c:if test="${(auth == '0198') || (!empty moveAuth)}">
																			<li role="presentation" class="menu-li" id="moveLi">
																				<a href="JavaScript:void(0)" class="bottom-menu-a" id="move">
																					<span class="glyphicon glyphicon-level-up menu-icon" aria-hidden="true"><span class="menu-char"><br>移動</span></span>
																				</a>
																			</li>
																		</c:if>
																		<c:if test="${(auth == '0198') || (!empty deleteAuth)}">
																			<li role="presentation" class="menu-li" id="deleteLi">
																				<a href="JavaScript:void(0)" class="bottom-menu-a" id="delete">
																					<span class="glyphicon glyphicon-trash menu-icon" aria-hidden="true"><span class="menu-char"><br>削除</span></span>
																				</a>
																			</li>
																		</c:if>
																	</ul>
																</td>
																<td>
																	<ul class="nav nav-pills bottom-menu-ch">
																		<li role="presentation" class="menu-li-ch" id="allCheckLi">
																			<a href="JavaScript:void(0)" class="bottom-menu-a" id="allCheck">
																				<span class="glyphicon glyphicon-check menu-icon" aria-hidden="true"><span class="menu-char"><br>全選択</span></span>
																			</a>
																		</li>
																	</ul>
																</td>
															</tr>
														</tbody>
													</table>
												</td>
												<td style="width:0.1px;">
												</td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
						<div class="row">
							<div id="contents">
								<!-- コンテンツエリア -->
							</div>
							<div class="modal-pictures clearfix">
								<!-- モーダルエリア -->
							</div>
						</div>
					</div>
					<c:if test= "${!empty liveEnable}">
						<div class="col-md-2" id="live-area">
							<!-- ライブエリア -->
							<c:if test="${liveServer == 'SkyWay'}">
								<!-- SkyWayのとき -->
								<div id="their-videos">
									<!-- ライブ中継リスト -->
								</div>
							</c:if>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>

	<!-- 検索モーダルダイアログ -->
	<div class="modal fade" id="searchModal" tabindex="-1" style="z-index:2100">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
					<h4 class="modal-title"><span class="glyphicon glyphicon-search" aria-hidden="true">&thinsp;検索</span></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="selectLibrary">ライブラリ</label>
						<!-- 検索ツリー表示 -->
						<div id="searchTree" style="white-space:nowrap"></div>
					</div>
					<div class="form-group">
						<label for="termList">撮影端末</label>
						<select id="searchTermId" class="form-control" >
							<option value=""></option>
							<c:forEach var="term" items="${termList}">
								<option value=<c:out value="${term.term_id}" />>${term.term_name}</option>
							</c:forEach>
						</select>
					</div>
					<div class="form-group">
						<label for="period">撮影日時</label>
						<div class="container-fluid" style="padding: 0;">
						<div class="flex">
							<div class="col-md-5 col-xs-12" style="padding: 0;">
								<div class="input-group date" id="datetimepicker-start">
									<input type="text" class="form-control" id="start_dateTime" style="ime-mode: disabled; background-color: white" readonly="readonly"/>
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
							<div class="col-md-2 col-xs-12 col text-center" style="padding: 0; line-height:34px;">～</div>
							<div class="col-md-5 col-xs-12" style="padding: 0;">
								<div class="input-group date" id="datetimepicker-end">
									<input type="text" class="form-control" id="end_dateTime" style="ime-mode: disabled; background-color: white" readonly="readonly"/>
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
							</div>
						</div>
					</div>
					<div class="form-group" id="searchAddInfo">
						<label for="searchAddInfo" class="control-label">付帯情報</label>
						<textarea class="form-control" id="searchAddInfoText" placeholder="キーワード" maxlength="2000"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" id="searchClear" style="float: left;">クリア</button>
					<button type="button" class="btn btn-primary" id="search" data-dismiss="modal"><span class="glyphicon glyphicon-search" aria-hidden="true"></span>&thinsp;検索</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
				</div>
			</div>
		</div>
	</div>
	</form:form>
	<!-- 付帯情報モーダルダイアログ -->
	<div class="modal fade" id="additionalInfoModal" tabindex="-1" style="z-index:2100">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
					<h4 class="modal-title"><span class="glyphicon glyphicon-edit" aria-hidden="true">&thinsp;付帯情報</span></h4>
				</div>
				<div class="modal-body">
					<div id="additionalInfoArea">
							<!-- 付帯情報表示エリア -->
					</div>
				</div>
				<div class="modal-footer">
					<c:if test= "${(auth == '0198') || (!empty editAuth)}">
						<button type="button" class="btn btn-primary" id="additionalInfoEdit"><span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>&thinsp;更新</button>
					</c:if>
					<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
				</div>
			</div>
		</div>
	</div>
	<!-- 移動モーダルダイアログ -->
	<div class="modal fade" id="moveModal" tabindex="-1" style="z-index:2100">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
					<h4 class="modal-title"><span class="glyphicon glyphicon-level-up" aria-hidden="true">&thinsp;移動</span></h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="selectLibrary">移動先ライブラリ</label>
						<!-- ライブラリツリー表示 -->
						<div id="moveTree" style="white-space:nowrap"></div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" id="moveRun"><span class="glyphicon glyphicon-level-up" aria-hidden="true"></span>&thinsp;実行</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
				</div>
			</div>
		</div>
	</div>
	<c:if test= "${(!empty liveEnable) && (empty notLiveBrowser)}">
		<!-- ライブ中継モーダルダイアログ -->
		<div class="modal fade" id="liveViewModal" tabindex="-1" style="z-index:2100">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" id="liveClose1" data-dismiss="modal"><span>×</span></button>
						<h4 class="modal-title"><span class="glyphicon glyphicon-facetime-video" aria-hidden="true"></span>&thinsp;ライブ中継：<span id="term_name"></span></h4>
					</div>
					<div class="modal-body">
						<video id="viewArea" width="100%" muted autoplay playsinline controls disablepictureinpicture oncontextmenu="return false;"></video>
					</div>
					<div class="modal-footer">
						<c:if test= "${liveServer == 'Janus'}">
							<!-- Janusのとき -->
							<button type="button" class="btn btn-info" id="tarkStart"><span class="glyphicon glyphicon-earphone" aria-hidden="true"></span>&thinsp;通話</button>
							<button type="button" class="btn btn-warning" id="tarkStop"><span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span>&thinsp;終話</button>
						</c:if>
						<button type="button" class="btn btn-primary" id="recStart"><span class="glyphicon glyphicon-record" aria-hidden="true"></span>&thinsp;録画</button>
						<button type="button" class="btn btn-danger" id="recStop"><span class="glyphicon glyphicon-stop" aria-hidden="true"></span>&thinsp;停止</button>
						<button type="button" class="btn btn-success" id="capture"><span class="glyphicon glyphicon-camera" aria-hidden="true"></span>&thinsp;キャプチャ</button>
						<button type="button" class="btn btn-default" id="liveClose2" data-dismiss="modal">閉じる</button>
					</div>
					<p id="tenant_id" style="display: none"></p>
					<p id="term_id" style="display: none"></p>
					<p id="locate_lat" style="display: none"></p>
					<p id="locate_lon" style="display: none"></p>
					<canvas id="canvas" style="display: none"></canvas>
				</div>
			</div>
		</div>
	</c:if>
	<script type="text/javascript" src="<spring:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
