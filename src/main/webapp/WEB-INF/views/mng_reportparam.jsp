<!DOCTYPE html>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Script-Type" content="text/javascript">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>帳票 - ${systemName}</title>
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/bootstrap/css/bootstrap.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/bootstrap-datetimepicker.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/datatables.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/common.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/simple-sidebar.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/sweetalert2.css" />" />
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/polyfill.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/sweetalert2.all.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jsAlert.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.custom.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/moment.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/bootstrap-datetimepicker.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/ja.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/datatables.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/messages_const.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/manager.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/mng_reportparam.js" />" ></script>
</head>
<body>
	<div class="container-fluid full-height">
		<div class="row full-height toggled" id="wrapper">
			<div class="col-md-2 gray full-height" id="sidebar-wrapper">
				<p>${systemName}<br />画像共有システム</p>
				<h4>管理メニュー</h4>
				<ul class="nav nav-pills nav-stacked">
					<li role="presentation"><a href="javaScript:void(0)" id="library"><span class="glyphicon glyphicon-folder-close blue" aria-hidden="true"></span><span class="blue">　ライブラリ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="terminal"><span class="glyphicon glyphicon-phone blue" aria-hidden="true"></span><span class="blue">　端末</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="user"><span class="glyphicon glyphicon-user blue" aria-hidden="true"></span><span class="blue">　ユーザー</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="additionalitem"><span class="glyphicon glyphicon-edit blue" aria-hidden="true"></span><span class="blue">　付帯情報項目名</span></a></li>
					<li role="presentation" class="active"><a href="javaScript:void(0)" id="reportparam"><span class="glyphicon glyphicon-file" aria-hidden="true"></span><span>　帳票</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="accessedlog"><span class="glyphicon glyphicon-time blue" aria-hidden="true"></span><span class="blue">　アクセスログ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="receivedlog"><span class="glyphicon glyphicon-download-alt blue" aria-hidden="true"></span><span class="blue">　受信ログ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="close"><span class="blue">閉じる</span></a></li>
				</ul>
			</div>
			<div class="col-md-10 bg-gray full-height scroll-y" id="page-content-wrapper">
				<form:form modelAttribute="mngReportParamForm" enctype="multipart/form-data">
				<!-- ユーザID -->
				<form:input type="hidden" id="acc_userID" path="acc_userID" class="form-control" value='${acc_userID}' />
				<!-- テナント有効期限 -->
				<form:input type="hidden" id="tenantStart" path="tenantStart" class="form-control" value='${tenantStart}' />
				<form:input type="hidden" id="tenantEnd" path="tenantEnd" class="form-control" value='${tenantEnd}' />
					<div class="container-fluid">
						<div class="row form-group"></div>
						<div class="row form-group">
							<c:if test="${not empty success}">
								<div class="alert alert-success alert-dismissible fade in" id="success" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${success}
								</div>
							</c:if>
							<c:if test="${not empty add_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="add_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${add_danger}
								</div>
							</c:if>
							<c:if test="${not empty add_file_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="add_file_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${add_file_danger}
								</div>
							</c:if>
							<c:if test="${not empty up_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="up_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${up_danger}
								</div>
							</c:if>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">帳票ID</label>
							</div>
							<div class="col-sm-5">
								<form:input path="report_id" id="report_id" class="form-control" maxlength="16" style="ime-mode: disabled;" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">帳票名</label>
							</div>
							<div class="col-sm-8">
								<form:input path="report_name" id="report_name" class="form-control" maxlength="50" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">テンプレートファイル(Excel)</label>
							</div>
							<div class="col-sm-8">
								<div class="input-group">
									<input type="file" id="uploadFile" name="uploadFile" accept=".xls, .xlsx" style="display: none;" />
									<span class="input-group-btn" style="vertical-align: top;">
										<button class="btn btn-primary" type="button" onclick="$('#uploadFile').click();"><i class="glyphicon glyphicon-folder-open"></i></button>
									</span>
									<form:input path="file_name" id="file_name" class="form-control" placeholder="ファイルを選択してください" readonly="true" value='${fileName}' />
									<span class="input-group-btn" id="downloadBtn" style="display: none; vertical-align: top;">
										<button class="btn btn-primary" type="submit" name="download" id="download"><i class="glyphicon glyphicon-download-alt"></i></button>
									</span>
								</div>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">シート名</label>
							</div>
							<div class="col-sm-8">
								<form:input path="sheet_name" id="sheet_name" class="form-control" maxlength="30" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">1ページ当たりの出力件数</label>
							</div>
							<div class="col-sm-2">
								<form:select path="print_rows" id="print_rows" items="${printRows}" itemLabel="label" itemValue="value" class="form-control" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">表紙出力フラグ</label>
							</div>
							<div class="col-sm-1" style="padding: 0px 0px 0px 18px;">
								<form:checkbox path="front_page_flg" id="front_page_flg" class="chk" onchange="checkFrontPageEnable();" />
							</div>
						</div>
						<div class="collapse" id="disFrontPageEnable">
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">表紙シート名</label>
								</div>
								<div class="col-sm-8">
									<form:input path="front_page_sheet" id="front_page_sheet" class="form-control" maxlength="30" />
								</div>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">有効期限</label>
							</div>
							<div class="col-sm-2">
								<div class="input-group date" id="datetimepicker-start">
									<form:input path="start_date" id="start_date" class="form-control" style="ime-mode: disabled; background-color: white" readonly="true" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<div class="error" id="start_date_err" ></div>
							</div>
							<div class="col-sm-1 col text-center" style="padding: 0; line-height:34px;">～</div>
							<div class="col-sm-2">
								<div class="input-group date" id="datetimepicker-end">
									<form:input path="end_date" id="end_date" class="form-control" style="ime-mode: disabled; background-color: white" readonly="true" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<div class="error" id="end_date_err" ></div>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">削除フラグ</label>
							</div>
							<div class="col-sm-1" style="padding: 0px 0px 0px 18px;">
								<form:checkbox path="del_flg" id="del_flg" class="chk" />
							</div>
						</div>
						<div class="row form-group">
							<div class="center">
								<button type="button" name="list" id="list" class="btn btn-default"><span class="glyphicon glyphicon-list" aria-hidden="true"></span>&thinsp;一覧</button>
								<button type="button" id="clearInputArea" class="btn btn-default">クリア</button>
								<button type="submit" name="add" id="reflect" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&thinsp;追加</button>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
	<!-- 一覧表示モーダルダイアログ -->
	<div class="modal fade" id="listModal" tabindex="-1">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
					<h4 class="modal-title"><span class="glyphicon glyphicon-file" aria-hidden="true">&thinsp;帳票一覧</span></h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered dataTable" id="dataTable"  width="100%">
						<thead>
							<tr>
								<th />
								<th>帳票ID</th>
								<th>帳票名</th>
								<th>テンプレートファイル名</th>
								<th>シート名</th>
								<th>1ページ当たりの出力件数</th>
								<th>表紙出力フラグ</th>
								<th>表紙用シート名</th>
								<th>有効開始日</th>
								<th>有効終了日</th>
								<th>削除フラグ</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${report}">
								<tr class="sel_row" style="cursor:pointer">
									<td class="center"><input type="radio" name="r_chk"/></td>
									<td>
										<c:out value="${item.report_id}" />
									</td>
									<td>
										<c:out value="${item.report_name}" />
									</td>
									<td>
										<c:out value="${item.file_name}" />
									</td>
									<td>
										<c:out value="${item.sheet_name}" />
									</td>
									<td>
										<c:out value="${item.print_rows}" />
									</td>
									<td>
										<c:out value="${item.str_front_page_flg}" />
									</td>
									<td>
										<c:out value="${item.front_page_sheet}" />
									</td>
									<td>
										<c:out value="${item.str_start_date}" />
									</td>
									<td>
										<c:out value="${item.str_end_date}" />
									</td>
									<td>
										<c:out value="${item.str_del_flg}" />
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
				    <button type="button" class="btn btn-default" id="edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>&thinsp;編集</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<spring:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
