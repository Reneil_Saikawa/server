<!DOCTYPE html>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Script-Type" content="text/javascript">
	<meta name="viewport" content="width=550px, user-scalable=no">
	<title>ログイン - ${systemName}</title>
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/bootstrap/css/bootstrap.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/common.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/login.css" />" />
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/messages_const.js" />" ></script>
	<script type="text/javascript">
		$(function(){
			/* ブラウザ戻るボタン抑止 */
			history.forward();

			/* バリデーション */
			$('form').validate({
				rules: {
					user_id: {
						required: true
					},
					password: {
						required: true
					}
				},
				messages: {
					user_id: {
						required: required
					},
					password: {
						required: required
					}
				},
				errorPlacement: function(error, element){
					error.insertAfter(element);
				}
			});
		});
	</script>
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<p class="white">${systemName} 画像共有システム</p>
				<h2 class="glyphicon glyphicon-log-in white">&thinsp;ログイン</h2>
				<div class="container login bg-white">
					<form:form modelAttribute="loginForm" id="loginForm">
						<div class="row form-group"></div>
						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">ID</label>
							</div>
							<div class="col-md-9">
								<form:input path="user_id" class="form-control" style="ime-mode: disabled" autofocus="autofocus" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-md-3">
								<label class="control-label">パスワード</label>
							</div>
							<div class="col-md-9">
								<form:password path="password" class="form-control" onpaste="return false" />
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<span class="error"><c:out value="${error}"/></span>
							</div>
						</div>
						<br />
						<div class="row">
							<div class="center">
								<button type="submit" name="login" class="btn btn-default btn-lg">ログイン</button>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<spring:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
