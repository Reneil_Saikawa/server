<!DOCTYPE html>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Script-Type" content="text/javascript">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ユーザー - ${systemName}</title>
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/bootstrap/css/bootstrap.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/bootstrap-datetimepicker.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/datatables.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/common.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/simple-sidebar.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/sweetalert2.css" />" />
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/polyfill.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/sweetalert2.all.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jsAlert.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/bootstrap-treeview.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.custom.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/moment.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/bootstrap-datetimepicker.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/ja.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/datatables.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/js.cookie.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/messages_const.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/manager.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/mng_userinfo.js" />" ></script>
</head>
<body>
	<div class="container-fluid full-height">
		<div class="row full-height toggled" id="wrapper">
			<div class="col-md-2 gray full-height" id="sidebar-wrapper">
				<p>${systemName}<br />画像共有システム</p>
				<h4>管理メニュー</h4>
				<ul class="nav nav-pills nav-stacked">
					<li role="presentation"><a href="javaScript:void(0)" id="library"><span class="glyphicon glyphicon-folder-close blue" aria-hidden="true"></span><span class="blue">　ライブラリ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="terminal"><span class="glyphicon glyphicon-phone blue" aria-hidden="true"></span><span class="blue">　端末</span></a></li>
					<li role="presentation" class="active"><a href="javaScript:void(0)" id="user"><span class="glyphicon glyphicon-user" aria-hidden="true"></span><span>　ユーザー</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="additionalitem"><span class="glyphicon glyphicon-edit blue" aria-hidden="true"></span><span class="blue">　付帯情報項目名</span></a></li>
					<c:if test= "${!empty reportEnable}">
						<li role="presentation"><a href="javaScript:void(0)" id="reportparam"><span class="glyphicon glyphicon-file blue" aria-hidden="true"></span><span class="blue">　帳票</span></a></li>
					</c:if>
					<li role="presentation"><a href="javaScript:void(0)" id="accessedlog"><span class="glyphicon glyphicon-time blue" aria-hidden="true"></span><span class="blue">　アクセスログ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="receivedlog"><span class="glyphicon glyphicon-download-alt blue" aria-hidden="true"></span><span class="blue">　受信ログ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="close"><span class="blue">閉じる</span></a></li>
				</ul>
			</div>
			<div class="col-md-10 bg-gray full-height scroll-y" id="page-content-wrapper">
				<form:form modelAttribute="mngUserForm">
				<!-- ユーザID -->
				<form:input type="hidden" id="acc_userID" path="acc_userID" class="form-control" value='${acc_userID}' />
				<!-- テナント有効期限 -->
				<form:input type="hidden" id="tenantStart" path="tenantStart" class="form-control" value='${tenantStart}' />
				<form:input type="hidden" id="tenantEnd" path="tenantEnd" class="form-control" value='${tenantEnd}' />
					<div class="container-fluid">
						<div class="row form-group"></div>
						<div class="row form-group">
							<c:if test="${not empty success}">
								<div class="alert alert-success alert-dismissible fade in" id="success" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${success}
								</div>
							</c:if>
							<c:if test="${not empty add_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="add_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${add_danger}
								</div>
							</c:if>
							<c:if test="${not empty add_license_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="add_license_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${add_license_danger}
								</div>
							</c:if>
							<c:if test="${not empty up_license_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="up_license_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${up_license_danger}
								</div>
							</c:if>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">ユーザーID</label>
							</div>
							<div class="col-sm-5">
								<form:input path="user_id" id="user_id" class="form-control" maxlength="32" style="ime-mode: disabled;" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">パスワード</label>
							</div>
							<div class="col-sm-5">
								<form:input path="password" id="password" class="form-control" maxlength="50" type="password" style="ime-mode: disabled;" />
							</div>
						</div>
						<div class="row form-group collapse" id="disPwdErrcnt">
							<div class="col-sm-2">
								<label class="control-label">パスワードエラーカウント</label>
							</div>
							<div class="col-sm-1">
								<form:input path="pwd_errcnt" id="pwd_errcnt" class="form-control" maxlength="2" style="ime-mode: disabled;" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">ユーザー名</label>
							</div>
							<div class="col-sm-8">
								<form:input path="user_name" id="user_name" class="form-control" maxlength="50" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">閲覧ライブラリ</label>
							</div>
							<div class="col-sm-8">
								<form:input type="hidden" id="lib_id" path="lib_id" class="form-control" />
								<!-- ツリー表示 -->
								<div id="tree"></div>
								<input type="hidden" id="libraryTreeData" value='${libraryTreeData}' />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2" style="padding: 15px 0px 0px 15px;">
								<label class="control-label">権限</label>
							</div>
							<div class="col-sm-8">
								<table class="table table-bordered dataTable" id="accessTable" style="table-layout: fixed; background-color: white;">
									<thead>
										<tr align="center">
											<th style="text-align: center;">管理</th>
											<c:if test= "${!empty reportEnable}">
												<th style="text-align: center;">帳票出力</th>
											</c:if>
											<th style="text-align: center;">付帯情報編集</th>
											<th style="text-align: center;">ダウンロード</th>
											<th style="text-align: center;">移動</th>
											<th style="text-align: center;">削除</th>
										</tr>
									</thead>
									<tbody>
										<tr  align="center">
											<td><form:checkbox path="manage_auth" id="manage_auth" class="chk" /></td>
											<c:if test= "${!empty reportEnable}">
												<td><form:checkbox path="output_auth" id="output_auth" class="chk" onchange="checkOutputAuth();" /></td>
											</c:if>
											<td><form:checkbox path="edit_auth" id="edit_auth" class="chk" /></td>
											<td><form:checkbox path="download_auth" id="download_auth" class="chk" /></td>
											<td><form:checkbox path="move_auth" id="move_auth" class="chk" /></td>
											<td><form:checkbox path="delete_auth" id="delete_auth" class="chk" /></td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
						<c:if test= "${!empty reportEnable}">
							<div class="row form-group collapse" id="disReportAuth">
								<div class="col-sm-2" style="padding: 4px 0px 0px 15px;">
									<label class="control-label">帳票権限</label>
								</div>
								<div class="col-sm-8" style="padding: 0px 0px 0px 18px;">
									<form:checkboxes path="report_auth" items="${report}" itemLabel="report_name" itemValue="report_id" delimiter="<br>" cssClass="chk" cssStyle="margin: 8px 8px 8px 0px; vertical-align:-0.5em;"/>
								</div>
							</div>
						</c:if>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">有効期限</label>
							</div>
							<div class="col-sm-2">
								<div class="input-group date" id="datetimepicker-start">
									<form:input path="start_date" id="start_date" class="form-control" style="ime-mode: disabled; background-color: white" readonly="true" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<div class="error" id="start_date_err" ></div>
							</div>
							<div class="col-sm-1 col text-center" style="padding: 0; line-height:34px;">～</div>
							<div class="col-sm-2">
								<div class="input-group date" id="datetimepicker-end">
									<form:input path="end_date" id="end_date" class="form-control" style="ime-mode: disabled; background-color: white" readonly="true" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<div class="error" id="end_date_err" ></div>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">削除フラグ</label>
							</div>
							<div class="col-sm-1" style="padding: 0px 0px 0px 18px;">
								<form:checkbox path="del_flg" id="del_flg" class="chk" />
							</div>
						</div>
						<div class="row form-group">
							<div class="center">
								<button type="button" name="list" id="list" class="btn btn-default"><span class="glyphicon glyphicon-list" aria-hidden="true"></span>&thinsp;一覧</button>
								<button type="button" id="clearInputArea" class="btn btn-default">クリア</button>
								<button type="submit" name="add" id="reflect" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&thinsp;追加</button>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
	<!-- 一覧表示モーダルダイアログ -->
	<div class="modal fade" id="listModal" tabindex="-1">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
					<h4 class="modal-title"><span class="glyphicon glyphicon-user" aria-hidden="true">&thinsp;ユーザー一覧</span></h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered dataTable" id="dataTable"  width="100%">
						<thead>
							<tr>
								<th />
								<th>ユーザーID</th>
								<th style="display: none;">パスワード</th>
								<th style="display: none;">パスワードエラーカウント</th>
								<th>ユーザー名</th>
								<th>管理権限</th>
								<th <c:if test= "${empty reportEnable}">style="display: none;"</c:if>><c:if test= "${!empty reportEnable}">帳票出力権限</c:if></th>
								<th>付帯情報編集権限</th>
								<th>ダウンロード権限</th>
								<th>移動権限</th>
								<th>削除権限</th>
								<th>有効開始日</th>
								<th>有効終了日</th>
								<th>削除フラグ</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${user}">
								<tr class="sel_row" style="cursor:pointer">
									<td class="center"><input type="radio" name="r_chk"/></td>
									<td>
										<c:out value="${item.user_id}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.dec_pwd}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.pwd_errcnt}" />
									</td>
									<td>
										<c:out value="${item.user_name}" />
									</td>
									<td>
										<c:out value="${item.manage_auth}" />
									</td>
									<td <c:if test= "${empty reportEnable}">style="display: none;"</c:if>>
										<c:out value="${item.output_auth}" />
									</td>
									<td>
										<c:out value="${item.edit_auth}" />
									</td>
									<td>
										<c:out value="${item.download_auth}" />
									</td>
									<td>
										<c:out value="${item.move_auth}" />
									</td>
									<td>
										<c:out value="${item.delete_auth}" />
									</td>
									<td>
										<c:out value="${item.str_start_date}" />
									</td>
									<td>
										<c:out value="${item.str_end_date}" />
									</td>
									<td>
										<c:out value="${item.str_del_flg}" />
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
				    <button type="button" class="btn btn-default" id="edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>&thinsp;編集</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<spring:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
