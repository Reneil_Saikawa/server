<!DOCTYPE html>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Script-Type" content="text/javascript">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ライブラリ - ${systemName}</title>
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/bootstrap/css/bootstrap.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/bootstrap-datetimepicker.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/datatables.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/common.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/simple-sidebar.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/sweetalert2.css" />" />
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/polyfill.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/sweetalert2.all.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jsAlert.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/bootstrap-treeview.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.custom.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/datatables.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/js.cookie.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/messages_const.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/manager.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/mng_library.js" />" ></script>
</head>
<body>
	<div id="loading">
		<div class="loader">Loading...</div>
	</div>
	<div class="container-fluid full-height">
		<div class="row full-height toggled" id="wrapper">
			<div class="col-md-2 gray full-height" id="sidebar-wrapper">
				<p>${systemName}<br />画像共有システム</p>
				<h4>管理メニュー</h4>
				<ul class="nav nav-pills nav-stacked">
					<li role="presentation" class="active"><a href="javaScript:void(0)" id="library"><span class="glyphicon glyphicon-folder-close" aria-hidden="true"></span><span>　ライブラリ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="terminal"><span class="glyphicon glyphicon-phone blue" aria-hidden="true"></span><span class="blue">　端末</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="user"><span class="glyphicon glyphicon-user blue" aria-hidden="true"></span><span class="blue">　ユーザー</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="additionalitem"><span class="glyphicon glyphicon-edit blue" aria-hidden="true"></span><span class="blue">　付帯情報項目名</span></a></li>
					<c:if test= "${!empty reportEnable}">
						<li role="presentation"><a href="javaScript:void(0)" id="reportparam"><span class="glyphicon glyphicon-file blue" aria-hidden="true"></span><span class="blue">　帳票</span></a></li>
					</c:if>
					<li role="presentation"><a href="javaScript:void(0)" id="accessedlog"><span class="glyphicon glyphicon-time blue" aria-hidden="true"></span><span class="blue">　アクセスログ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="receivedlog"><span class="glyphicon glyphicon-download-alt blue" aria-hidden="true"></span><span class="blue">　受信ログ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="close"><span class="blue">閉じる</span></a></li>
				</ul>
			</div>
			<div class="col-md-10 bg-gray full-height scroll-y" id="page-content-wrapper">
				<form:form modelAttribute="mngLibraryForm">
				<!-- ユーザID -->
				<form:input type="hidden" id="acc_userID" path="acc_userID" class="form-control" value='${acc_userID}' />
					<div class="container-fluid">
						<div class="row form-group"></div>
						<div class="row form-group">
							<c:if test="${not empty success}">
								<div class="alert alert-success alert-dismissible fade in" id="success" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${success}
								</div>
							</c:if>
							<c:if test="${not empty id_add_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="id_add_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${id_add_danger}
								</div>
							</c:if>
							<c:if test="${not empty path_add_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="path_add_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${path_add_danger}
								</div>
							</c:if>
							<c:if test="${not empty up_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="up_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${up_danger}
								</div>
							</c:if>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">親ライブラリ</label>
							</div>
							<div class="col-sm-8">
								<form:input type="hidden" id="parent_lid" path="parent_lid" class="form-control" />
								<!-- ツリー表示 -->
								<div id="tree"></div>
								<input type="hidden" id="libraryTreeData" value='${libraryTreeData}' />
							</div>
						</div>
						<div class="row form-group" hidden>
							<div class="col-sm-2">
								<label class="control-label">親ライブラリパス(非表示)</label>
							</div>
							<div class="col-sm-8">
								<form:select path="parent_path" id="parent_path" items="${parentLid}" itemLabel="lib_path_text" itemValue="lib_id" class="form-control" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">ライブラリID</label>
							</div>
							<div class="col-sm-5">
								<form:input path="lib_id" id="lib_id" class="form-control" maxlength="16" readonly="false" style="ime-mode: disabled;" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">ライブラリ名</label>
							</div>
							<div class="col-sm-8">
								<form:input path="lib_name" id="lib_name" class="form-control" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">ライブラリパス</label>
							</div>
							<div class="col-sm-8">
								<div id="lib_path"></div>
							</div>
						</div>
						<c:if test= "${!empty liveFunction}">
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">ライブルーム名</label>
								</div>
								<div class="col-sm-8">
									<form:input path="live_room" id="live_room" class="form-control" maxlength="50" style="ime-mode: disabled;" />
								</div>
							</div>
						</c:if>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">表示順</label>
							</div>
							<div class="col-sm-2">
								<form:input path="display_no" id="display_no" class="form-control" maxlength="5" />
							</div>
						</div>
						<div class="row">
							<div class="center">
								<button type="button" name="list" id="list" class="btn btn-default"><span class="glyphicon glyphicon-list" aria-hidden="true"></span>&thinsp;一覧</button>
								<button type="button" id="clearInputArea" class="btn btn-default">クリア</button>
								<button type="submit" name="add" id="reflect" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&thinsp;追加</button>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
	<!-- 一覧表示モーダルダイアログ -->
	<div class="modal fade" id="listModal" tabindex="-1">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
					<h4 class="modal-title"><span class="glyphicon glyphicon-folder-close" aria-hidden="true">&thinsp;ライブラリ一覧</span></h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered dataTable" id="dataTable" width="100%">
						<thead>
							<tr>
								<th />
								<th>ライブラリID</th>
								<th style="display: none;">親ライブラリID</th>
								<th>ライブラリ名</th>
								<th>ライブラリパス</th>
								<th>親ライブラリ</th>
								<th style="display: none;">ライブルーム名</th>
								<th>表示順</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${library}">
								<tr class="sel_row" style="cursor:pointer">
									<td class="center"><input type="radio" name="r_chk"/></td>
									<td>
										<c:out value="${item.lib_id}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.parent_lid}" />
									</td>
									<td>
										<c:out value="${item.lib_name}" />
									</td>
									<td>
										<c:out value="${item.lib_path_text}" />
									</td>
									<td>
										<c:out value="${item.parent_lname}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.live_room}" />
									</td>
									<td>
										<c:out value="${item.display_no}" />
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" id="delete" class="btn btn-danger" style="float: left;"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span>&thinsp;削除</button>
				    <button type="button" class="btn btn-default" id="edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>&thinsp;編集</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<spring:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
