<!DOCTYPE html>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Script-Type" content="text/javascript">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>端末 - ${systemName}</title>
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/bootstrap/css/bootstrap.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/bootstrap-datetimepicker.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/datatables.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/common.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/simple-sidebar.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/sweetalert2.css" />" />
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/polyfill.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/sweetalert2.all.min.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jsAlert.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/bootstrap-treeview.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.validate.custom.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/moment.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/bootstrap-datetimepicker.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/ja.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/datatables.min.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/js.cookie.js" />"></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/messages_const.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/manager.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.qrcode.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/qrcode.js" />" ></script>
	<script type="text/javascript" src="<spring:url value="/resources/js/mng_terminal.js" />" ></script>
</head>
<body>
	<div class="container-fluid full-height">
		<div class="row full-height toggled" id="wrapper">
			<div class="col-md-2 gray full-height" id="sidebar-wrapper">
				<p>${systemName}<br />画像共有システム</p>
				<h4>管理メニュー</h4>
				<ul class="nav nav-pills nav-stacked">
					<li role="presentation"><a href="javaScript:void(0)" id="library"><span class="glyphicon glyphicon-folder-close blue" aria-hidden="true"></span><span class="blue">　ライブラリ</span></a></li>
					<li role="presentation" class="active"><a href="javaScript:void(0)" id="terminal"><span class="glyphicon glyphicon-phone" aria-hidden="true"></span><span>　端末</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="user"><span class="glyphicon glyphicon-user blue" aria-hidden="true"></span><span class="blue">　ユーザー</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="additionalitem"><span class="glyphicon glyphicon-edit blue" aria-hidden="true"></span><span class="blue">　付帯情報項目名</span></a></li>
					<c:if test= "${!empty reportEnable}">
						<li role="presentation"><a href="javaScript:void(0)" id="reportparam"><span class="glyphicon glyphicon-file blue" aria-hidden="true"></span><span class="blue">　帳票</span></a></li>
					</c:if>
					<li role="presentation"><a href="javaScript:void(0)" id="accessedlog"><span class="glyphicon glyphicon-time blue" aria-hidden="true"></span><span class="blue">　アクセスログ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="receivedlog"><span class="glyphicon glyphicon-download-alt blue" aria-hidden="true"></span><span class="blue">　受信ログ</span></a></li>
					<li role="presentation"><a href="javaScript:void(0)" id="close"><span class="blue">閉じる</span></a></li>
				</ul>
			</div>
			<div class="col-md-10 bg-gray full-height scroll-y" id="page-content-wrapper">
				<form:form modelAttribute="mngTerminalForm">
				<!-- ユーザID -->
				<form:input type="hidden" id="acc_userID" path="acc_userID" class="form-control" value='${acc_userID}' />
				<!-- システムURL -->
				<input type="hidden" id="inUrl" value='${inUrl}' />
				<input type="hidden" id="outUrl" value='${outUrl}' />
				<!-- テナントID -->
				<input type="hidden" id="tenantId" value='${tenantId}' />
				<!-- テナント有効期限 -->
				<form:input type="hidden" id="tenantStart" path="tenantStart" class="form-control" value='${tenantStart}' />
				<form:input type="hidden" id="tenantEnd" path="tenantEnd" class="form-control" value='${tenantEnd}' />
					<div class="container-fluid">
						<div class="row form-group"></div>
						<div class="row form-group">
							<c:if test="${not empty success}">
								<div class="alert alert-success alert-dismissible fade in" id="success" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${success}
								</div>
							</c:if>
							<c:if test="${not empty add_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="add_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${add_danger}
								</div>
							</c:if>
							<c:if test="${not empty add_license_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="add_license_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${add_license_danger}
								</div>
							</c:if>
							<c:if test="${not empty up_license_danger}">
								<div class="alert alert-danger alert-dismissible fade in" id="up_license_danger" role="alert">
									<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span></button>
									${up_license_danger}
								</div>
							</c:if>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">端末ID</label>
							</div>
							<div class="col-sm-5">
								<form:input path="term_id" id="term_id" class="form-control" maxlength="16" style="ime-mode: disabled;" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">端末区分</label>
							</div>
							<div class="col-sm-2">
								<form:select path="term_ctg" id="term_ctg" items="${code}" itemLabel="name_01" itemValue="code" class="form-control" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">端末名</label>
							</div>
							<div class="col-sm-8">
								<form:input path="term_name" id="term_name" class="form-control" maxlength="50" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">保存先ライブラリ</label>
							</div>
							<div class="col-sm-8">
								<form:input type="hidden" id="lib_id" path="lib_id" class="form-control" />
								<!-- ツリー表示 -->
								<div id="tree"></div>
								<input type="hidden" id="libraryTreeData" value='${libraryTreeData}' />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">撮影年フォルダ作成</label>
							</div>
							<div class="col-sm-1" style="padding: 0px 0px 0px 18px;">
								<form:checkbox path="year_folder_flg" id="year_folder_flg" class="chk" onchange="checkDateFolderEnable();" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">撮影月フォルダ作成</label>
							</div>
							<div class="col-sm-1" style="padding: 0px 0px 0px 18px;">
								<form:checkbox path="month_folder_flg" id="month_folder_flg" class="chk" onchange="checkDateFolderEnable();" />
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">撮影日フォルダ作成</label>
							</div>
							<div class="col-sm-1" style="padding: 0px 0px 0px 18px;">
								<form:checkbox path="day_folder_flg" id="day_folder_flg" class="chk" onchange="checkDateFolderEnable();" />
							</div>
						</div>
						<div class="collapse" id="disDateFolderEnable">
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">撮影日付フォルダ日本語</label>
								</div>
								<div class="col-sm-1" style="padding: 0px 0px 0px 18px;">
									<form:checkbox path="japanese_flg" id="japanese_flg" class="chk" onchange="checkDateFolderEnable();" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">撮影日付フォルダパス</label>
								</div>
								<div class="col-sm-8">
									<div id="date_folder_path"></div>
								</div>
							</div>
						</div>
						<br>
						<c:if test= "${!empty movieFunction}">
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">動画撮影最大時間 (秒)</label>
								</div>
								<div class="col-sm-2">
									<form:select path="m_max_time" id="m_max_time" items="${maxTime}" itemLabel="label" itemValue="value" class="form-control" />
								</div>
							</div>
						</c:if>
						<c:if test= "${!empty liveFunction}">
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">ライブ撮影サイズ (横)</label>
								</div>
								<div class="col-sm-2">
									<form:input path="live_width" id="live_width" class="form-control" maxlength="4" />
								</div>
							</div>
							<div class="row form-group">
								<div class="col-sm-2">
									<label class="control-label">ライブフレームレート (fps)</label>
								</div>
								<div class="col-sm-1">
									<label class="control-label">最小 (2-30)</label>
								</div>
								<div class="col-sm-1">
									<form:select path="live_min_fps" id="live_min_fps" items="${fps}" itemLabel="label" itemValue="value" class="form-control" />
								</div>
								<div class="col-sm-1 col-sm-offset-1">
									<label class="control-label">最大 (2-30)</label>
								</div>
								<div class="col-sm-1">
									<form:select path="live_max_fps" id="live_max_fps" items="${fps}" itemLabel="label" itemValue="value" class="form-control" />
								</div>
							</div>
						</c:if>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">有効期限</label>
							</div>
							<div class="col-sm-2">
								<div class="input-group date" id="datetimepicker-start">
									<form:input path="start_date" id="start_date" class="form-control" style="ime-mode: disabled; background-color: white" readonly="true" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<div class="error" id="start_date_err" ></div>
							</div>
							<div class="col-sm-1 col text-center" style="padding: 0; line-height:34px;">～</div>
							<div class="col-sm-2">
								<div class="input-group date" id="datetimepicker-end">
									<form:input path="end_date" id="end_date" class="form-control" style="ime-mode: disabled; background-color: white" readonly="true" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
								<div class="error" id="end_date_err" ></div>
							</div>
						</div>
						<div class="row form-group">
							<div class="col-sm-2">
								<label class="control-label">削除フラグ</label>
							</div>
							<div class="col-sm-1" style="padding: 0px 0px 0px 18px;">
								<form:checkbox path="del_flg" id="del_flg" class="chk" />
							</div>
						</div>
						<div class="row">
							<div class="center">
								<button type="button" name="list" id="list" class="btn btn-default"><span class="glyphicon glyphicon-list" aria-hidden="true"></span>&thinsp;一覧</button>
								<button type="button" id="clearInputArea" class="btn btn-default">クリア</button>
								<button type="submit" name="add" id="reflect" class="btn btn-primary"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>&thinsp;追加</button>
							</div>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
	<!-- 一覧表示モーダルダイアログ -->
	<div class="modal fade" id="listModal" tabindex="-1">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
					<h4 class="modal-title"><span class="glyphicon glyphicon-phone" aria-hidden="true">&thinsp;端末一覧</span></h4>
				</div>
				<div class="modal-body">
					<table class="table table-bordered dataTable" id="dataTable" width="100%">
						<thead>
							<tr>
								<th />
								<th>端末ID</th>
								<th style="display: none;">端末区分ID</th>
								<th>端末区分</th>
								<th>端末名</th>
								<th style="display: none;">ライブラリID</th>
								<th>保存先ライブラリ</th>
								<th style="display: none;">撮影年フォルダ作成</th>
								<th style="display: none;">撮影月フォルダ作成</th>
								<th style="display: none;">撮影日フォルダ作成</th>
								<th style="display: none;">日本語表記</th>
								<th style="display: none;">動画撮影最大時間(秒)</th>
								<th style="display: none;">ライブ撮影サイズ(横)</th>
								<th style="display: none;">ライブ最小フレームレート</th>
								<th style="display: none;">ライブ最大フレームレート</th>
								<th>有効開始日</th>
								<th>有効終了日</th>
								<th>削除フラグ</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach var="item" items="${terminal}">
								<tr class="sel_row" style="cursor:pointer">
									<td class="center"><input type="radio" name="r_chk"/></td>
									<td>
										<c:out value="${item.term_id}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.term_ctg}" />
									</td>
									<td>
										<c:out value="${item.term_ctg_name}" />
									</td>
									<td>
										<c:out value="${item.term_name}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.lib_id}" />
									</td>
									<td>
										<c:out value="${item.lib_name}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.str_year_folder_flg}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.str_month_folder_flg}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.str_day_folder_flg}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.str_japanese_flg}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.m_max_time}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.live_width}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.live_min_fps}" />
									</td>
									<td style="display: none;">
										<c:out value="${item.live_max_fps}" />
									</td>
									<td>
										<c:out value="${item.str_start_date}" />
									</td>
									<td>
										<c:out value="${item.str_end_date}" />
									</td>
									<td>
										<c:out value="${item.str_del_flg}" />
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="button" name="QRcodeArea" id="QRcodeArea" class="btn btn-success" style="float: left;"><span class="glyphicon glyphicon-qrcode" aria-hidden="true"></span>&thinsp;QRコード</button>
				    <button type="button" class="btn btn-default" id="edit"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>&thinsp;編集</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
				</div>
			</div>
		</div>
	</div>
	<!-- QRコード表示モーダルダイアログ -->
	<div class="modal fade" id="QRcodeModal" tabindex="-1"  style="z-index: 1500">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"><span>×</span></button>
					<h4 class="modal-title"><span class="glyphicon glyphicon-phone" aria-hidden="true" id="QRheader"></span></h4>
				</div>
				<div class="modal-body">
					<div class="row form-group" style="margin-bottom: 0">
						<div class="col-sm-12 col text-center">
							<div id="QRcode"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<spring:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
