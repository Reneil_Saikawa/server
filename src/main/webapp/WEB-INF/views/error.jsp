<!DOCTYPE html>
<html lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Script-Type" content="text/javascript">
	<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>エラー - ${systemName}</title>
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/bootstrap/css/bootstrap.min.css" />" />
	<link rel="stylesheet" type="text/css" href="<spring:url value="/resources/css/common.css" />" />
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->
	<script type="text/javascript" src="<spring:url value="/resources/js/jquery.min.js" />" ></script>
	<script>history.forward();</script><!--ブラウザ戻るボタン抑止-->
</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-12">
				<p class="white">${systemName} 画像共有システム</p>
				<h2 class="glyphicon glyphicon-warning-sign white">&thinsp;エラー</h2>
				<div class="alert alert-danger" role="alert">${message}</div>
				<input type="submit" class="btn btn-warning" value="ログイン画面へ" onClick="location.href='<spring:url value='${loginUrl}' />'" />
			</div>
		</div>
	</div>
	<script type="text/javascript" src="<spring:url value="/resources/bootstrap/js/bootstrap.min.js" />"></script>
</body>
</html>
