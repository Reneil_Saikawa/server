var validator;
var oTable;

$(function(){
	/* ブラウザ戻るボタン抑止 */
	history.forward();

	/* バリデーション */
	validator = $('form').validate({
		rules: {
			report_id: {
				required: true,
				numAlphaSymbol: true
			},
			report_name: {
				required: true
			},
			file_name:{
				required: true,
				xlsFile: true
			},
			sheet_name:{
				required: true
			},
			front_page_sheet:{
				frontPageReq: true
			},
			start_date: {
				required: true
			},
			end_date: {
				required: true,
				compareDate: 'start_date'
			}
		},
		messages: {
			report_id: {
				required: required,
				numAlphaSymbol: numAlphaSymbol
			},
			report_name: {
				required: required
			},
			file_name: {
				required: required,
				xlsFile: xlsFile
			},
			sheet_name: {
				required: required
			},
			front_page_sheet: {
				frontPageReq: required
			},
			start_date: {
				required: required
			},
			end_date: {
				required: required,
				compareDate: compareDate
			}
		},
		errorPlacement: function(error, element){
			if ((element.attr("name") == "start_date") || (element.attr("name") == "end_date")) {
				error.insertAfter($('#'+ element.attr('name') + '_err'));
			} else {
				error.insertAfter(element);
			}
		}
	});

	//ファイル選択時
	$('#uploadFile').change(function() {
		$('#file_name').val($(this).val().replace("C:\\fakepath\\", ""));
		// ダウンロードボタン非表示
		$('#downloadBtn').hide();
	});

	/* リストの行クリック時 */
	$('#dataTable tbody tr').click(function(event) {
		if ($(this).hasClass('row_selected')) {
			// 選択行をクリック（行の選択を解除）
			oTable.$(this).removeClass('row_selected');
			oTable.$(this).find('input[name=r_chk]').prop('checked',false);
		} else {
			// 非選択業をクリック（行を選択）
			// 一旦全ての行の選択を解除する
			oTable.$('.sel_row').removeClass('row_selected');
			oTable.$('.sel_row').find('input[name=r_chk]').prop('checked',false);
			// 行を選択
			oTable.$(this).addClass('row_selected');
			oTable.$(this).find('input[name=r_chk]').prop('checked',true);
		}
	} );

	/* dataTableの初期設定 */
	oTable = $('#dataTable').dataTable({
		"language": {						/* 日本語化 */
			"decimal": ".",
			"thousands": ",",
			"sProcessing": "処理中...",
			"sLengthMenu": "_MENU_ 件表示",
			"sZeroRecords": "データがありません。",
			"sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示中",
			"sInfoEmpty": " 0 件中 0 から 0 まで表示",
			"sInfoFiltered": "（全 _MAX_ 件より抽出）",
			"sInfoPostFix": "",
			"sSearch": "検索:",
			"sUrl": "",
			"oPaginate": {
			"sFirst": "先頭",
			"sPrevious": "<",
			"sNext": ">",
			"sLast": "最終"
			}
		},
		//"sPaginationType": "full_numbers",	/* two_button:進むと戻るのみ, full_numbers:全ボタン表示 */
		"asStripeClasses": ['even','even'],	/* odd, even にすると1行置きに色が変わる(odd, evenはcssで設定) */
		"bAutoWidth": true,					/* 列幅自動調整機能 */
		"sDom": "lfrtip",					/* 列幅変更機能有効(パラメータ不明) */
		"bFilter": true,					/* フィルター機能 */
		"bPaginate": true,					/* ページング機能 */
		"bInfo": true,						/* ページ情報(n件中aからbまで表示) */
		"bLengthChange": true,				/* 1ページの表示件数切替 */
		"aLengthMenu": [10,20,50,100],		/* 1ページの表示件数切替コンボボックスソース */
		"iDisplayLength": 10,				/* 1ページの表示件数初期値 */
		"sScrollX": "",						/* 水平スクロール機能(幅をピクセル指定、空は無効) */
		"sScrollY": "",						/* 垂直スクロール機能(高さをピクセル指定、空は無効) */
		"bScrollCollapse": false,			/* 垂直スクロール有効時に表示幅固定 */
		"bSort": true,						/* 整列機能 */
		"aaSorting": [],					/* 整列初期状態(空[]は整列しない) */
		"aoColumnDefs": [ {
			"bSortable": false, "aTargets": [ 0 ]	/* 0カラム目の整列機能を無効にする */
		} ]
	} );

	/* カレンダー表示 */
	$('#datetimepicker-start').datetimepicker({
		dayViewHeaderFormat: 'YYYY年 MMMM',
		tooltips: {
		close: '閉じる',
		selectMonth: '月を選択',
		prevMonth: '前月',
		nextMonth: '次月',
		selectYear: '年を選択',
		prevYear: '前年',
		nextYear: '次年',
		selectTime: '時間を選択',
		selectDate: '日付を選択',
		prevDecade: '前期間',
		nextDecade: '次期間',
		selectDecade: '期間を選択',
		prevCentury: '前世紀',
		nextCentury: '次世紀'
		},
		format: 'YYYY/MM/DD',
		minDate: $('#tenantStart').val(),
		maxDate: $('#tenantEnd').val() + ' 23:59:59',
		locale: 'ja',
		ignoreReadonly: true,
		//sideBySide: true,
		showClose: true,
		widgetPositioning: {
			horizontal: 'left',
			vertical: 'auto'
		}
	});
	$('#datetimepicker-end').datetimepicker({
		dayViewHeaderFormat: 'YYYY年 MMMM',
		tooltips: {
		close: '閉じる',
		selectMonth: '月を選択',
		prevMonth: '前月',
		nextMonth: '次月',
		selectYear: '年を選択',
		prevYear: '前年',
		nextYear: '次年',
		selectTime: '時間を選択',
		selectDate: '日付を選択',
		prevDecade: '前期間',
		nextDecade: '次期間',
		selectDecade: '期間を選択',
		prevCentury: '前世紀',
		nextCentury: '次世紀'
		},
		format: 'YYYY/MM/DD',
		minDate: $('#tenantStart').val(),
		maxDate: $('#tenantEnd').val() + ' 23:59:59',
		locale: 'ja',
		ignoreReadonly: true,
		//sideBySide: true,
		showClose: true,
		widgetPositioning: {
			horizontal: 'left',
			vertical: 'auto'
		}
	});

	/* 一覧ボタンクリック時 */
	$('#list').click(function() {
		// リストを開く
		$('#listModal').modal();
	});

	/* リストの編集ボタンクリック時 */
	$('#edit').click(function() {
		var selectedRow = getSelectedRow(oTable);
		if (selectedRow.length == 1) {
			// 選択が1行のとき

			// リストの値をフォームに転記する
			$('#report_id').val(selectedRow[0].cells.item(1).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#report_name').val(selectedRow[0].cells.item(2).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#file_name').val(selectedRow[0].cells.item(3).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#sheet_name').val(selectedRow[0].cells.item(4).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#print_rows').val(selectedRow[0].cells.item(5).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			if (selectedRow[0].cells.item(6).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#front_page_flg').prop('checked', true);
				$('#disFrontPageEnable').collapse('show');
			}else {
				$('#front_page_flg').prop('checked', false);
				$('#disFrontPageEnable').collapse('hide');
			}

			$('#front_page_sheet').val(selectedRow[0].cells.item(7).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#start_date').val(selectedRow[0].cells.item(8).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#end_date').val(selectedRow[0].cells.item(9).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			if (selectedRow[0].cells.item(10).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#del_flg').prop('checked', true);
			}else {
				$('#del_flg').prop('checked', false);
			}

			// 帳票IDを編集不可にする
			$('#report_id').attr('readonly', true);

			// バリデートをリセットする
			validator.resetForm();

			// メッセージを閉じる
			$('#success').alert('close');
			$('#add_danger').alert('close');
			$('#add_file_danger').alert('close');
			$('#up_danger').alert('close');

			// ボタンを「更新」に変更
			$('#reflect').attr('name', 'update');
			$('#reflect').html("<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span>&thinsp;更新");

			// ダウンロードボタンを表示
			$('#downloadBtn').show();

			 // リストを閉じる
			$('#listModal').modal('hide');
		}else {
			swalInfo.fire({html: "編集する帳票を選択してください。"});
			return false;
		}
	});

	/* クリアボタンクリック時 */
	$('#clearInputArea').click(function() {
		// メッセージを閉じる
		$('#success').alert('close');
		$('#add_danger').alert('close');
		$('#add_file_danger').alert('close');
		$('#up_danger').alert('close');
		 // フォームをクリアする
		clearForm(this.form);
		// バリデートをリセットする
		validator.resetForm();
		// 帳票IDを編集可にする
		$('#report_id').attr('readonly', false);
		// 1ページ当たりの出力件数のデフォルト値を設定する
		$('#print_rows').val(1);
		// 表紙シート名を非表示にする
		$('#disFrontPageEnable').collapse('hide');
		// ボタンを「追加」に変更
		$('#reflect').attr('name', 'add');
		$('#reflect').html("<span class='glyphicon glyphicon-plus' aria-hidden='true'></span>&thinsp;追加");
		// ダウンロードボタン非表示
		$('#downloadBtn').hide();
	});

	// 表紙シート名表示チェック
	if ($("#front_page_flg").prop("checked") == true) {
		// 表紙シート名表示を50ミリ秒遅らせる(画面チラつき防止)
		setTimeout(function(){$('#disFrontPageEnable').collapse('show')}, 50);
	}else {
		$('#disFrontPageEnable').collapse('hide');
	}

	if ($('#add_danger').length) {
		// 帳票ID入力欄をエラーにする
		$('#report_id').addClass("error");
	}else if ($('#add_file_danger').length) {
		// テンプレートファイル入力欄をエラーにする
		$('#file_name').addClass("error");
		// ダウンロードボタン非表示
		$('#downloadBtn').hide();
	}else if ($('#up_danger').length) {
		// テンプレートファイル入力欄をエラーにする
		$('#file_name').addClass("error");
		// ダウンロードボタン非表示
		$('#downloadBtn').hide();
		// 帳票IDを編集不可にする
		$('#report_id').attr('readonly', true);
		// ボタンを「更新」に変更
		$('#reflect').attr('name', 'update');
		$('#reflect').html("<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span>&thinsp;更新");
	}else {
		if ($('#report_id').val() != "" && $('#report_id').val() != null) {
			// 追加・更新時処理
			// 帳票IDを編集不可にする
			$('#report_id').attr('readonly', true);
			// ボタンを「更新」に変更
			$('#reflect').attr('name', 'update');
			$('#reflect').html("<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span>&thinsp;更新");
			// ダウンロードボタンを表示
			$('#downloadBtn').show();
		}else {
			// 新規時
			// 1ページ当たりの出力件数のデフォルト値を設定する
			$('#print_rows').val(1);
			// ダウンロードボタン非表示
			$('#downloadBtn').hide();
		}
	}

});
/* 選択されている行を返却する */
function getSelectedRow(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}

/* 表紙出力フラグ有効化チェック時 */
function checkFrontPageEnable() {
	// 表紙出力フラグ表示チェック
	if ($("#front_page_flg").prop("checked") == true) {
		$('#disFrontPageEnable').collapse('show');
	}else {
		$('#disFrontPageEnable').collapse('hide');
	}
}
