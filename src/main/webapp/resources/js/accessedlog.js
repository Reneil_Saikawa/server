var validator;

$(function(){
	/* ブラウザ戻るボタン抑止 */
	history.forward();

	/* バリデーション */
	validator = $('form').validate({
		rules: {
			start_date: {
				comparelogStartDate: 'end_date'
			},
			end_date: {
				comparelogEndDate: 'start_date'
			}
		},
		messages: {
			start_date: {
				comparelogStartDate: comparelogDate
			},
			end_date: {
				comparelogEndDate: comparelogDate
			}
		},
		errorPlacement: function(error, element){
			if ((element.attr("name") == "start_date") || (element.attr("name") == "end_date")) {
				error.insertAfter($('#'+ element.attr('name') + '_err'));
			} else {
				error.insertAfter(element);
			}
		}
	});

	/* PDF出力用フォント用意 */
	pdfMake.fonts = {
			GenShin: {
				normal: 'GenShinGothic-Normal-Sub.ttf',
				bold: 'GenShinGothic-Normal-Sub.ttf',
				italics: 'GenShinGothic-Normal-Sub.ttf',
				bolditalics: 'GenShinGothic-Normal-Sub.ttf'
			}
	}

	/* カレンダー表示 */
	$('#datetimepicker-start').datetimepicker({
		dayViewHeaderFormat: 'YYYY年 MMMM',
		tooltips: {
		close: '閉じる',
		selectMonth: '月を選択',
		prevMonth: '前月',
		nextMonth: '次月',
		selectYear: '年を選択',
		prevYear: '前年',
		nextYear: '次年',
		selectTime: '時間を選択',
		selectDate: '日付を選択',
		prevDecade: '前期間',
		nextDecade: '次期間',
		selectDecade: '期間を選択',
		prevCentury: '前世紀',
		nextCentury: '次世紀'
		},
		format: 'YYYY/MM/DD HH:mm',
		locale: 'ja',
		ignoreReadonly: true,
		//sideBySide: true,
		showClose: true,
		widgetPositioning: {
			horizontal: 'left',
			vertical: 'auto'
		}
	});
	$('#datetimepicker-end').datetimepicker({
		dayViewHeaderFormat: 'YYYY年 MMMM',
		tooltips: {
		close: '閉じる',
		selectMonth: '月を選択',
		prevMonth: '前月',
		nextMonth: '次月',
		selectYear: '年を選択',
		prevYear: '前年',
		nextYear: '次年',
		selectTime: '時間を選択',
		selectDate: '日付を選択',
		prevDecade: '前期間',
		nextDecade: '次期間',
		selectDecade: '期間を選択',
		prevCentury: '前世紀',
		nextCentury: '次世紀'
		},
		format: 'YYYY/MM/DD HH:mm',
		locale: 'ja',
		ignoreReadonly: true,
		//sideBySide: true,
		showClose: true,
		widgetPositioning: {
			horizontal: 'left',
			vertical: 'auto'
		}
	});

	/* クリアボタンクリック時 */
	$('#clearInputArea').click(function() {
		 // フォームをクリアする
		clearForm(this.form);
		// ログを消す
		var logArea = $("#logArea");
		logArea.empty();
		// バリデートをリセットする
		validator.resetForm();
		// ユーザをを全表示させる
		$('.wrap').children().unwrap();

	});

	// テナント選択時に表示ユーザーを抽出
	$('#tenant_id').change(function() {
		// 一度ユーザをを全表示させる
		$('.wrap').children().unwrap();
		if ($('#tenant_id').val() != "") {
			setTenantUser($('#tenant_id').val());
		}
	});

	// 検索クリック時
	$("#search").click(function() {
		// 入力値チェック(バリデーション)
		if (!$('form').valid()) {
			return false;
		}
		searchAccesslog();
	});

});

/* 選択したテナントに所属するユーザー以外非表示 */
function setTenantUser(tenantId) {
	try {
		$.ajax({
			type        : "POST",
			url         : "accessedlog.html?getNotTenantUser",
			data        : {acc_userID: $("#acc_userID").val(), tenantId: tenantId},
			dataType    : "JSON"
		})
		// 通信成功
		.done(function(result) {
			if (null != result) {
				for (var i = 0; i < result.length; i++) {
					if (result[i].user_id != null){
						$('select#user_id option[value="' + result[i].user_id + '"]').wrap("<span class='wrap'>");
					}
				}
			}else {
				// エラー発生
				swalAlert.fire({html: "エラーが発生しました。"});
			}
		})
		// 通信失敗
		.fail(function(XMLHttpRequest, textStatus, errorThrown) {
			if (XMLHttpRequest.status == 200) {
				if ("" == XMLHttpRequest.responseText || null == XMLHttpRequest.responseText) {
					// 非表示ユーザー無し
				}else {
					// 例外エラー表示
					document.write(XMLHttpRequest.responseText);
					document.close();
				}
			}else {
				console.log("ajax通信に失敗しました");
				console.log("XMLHttpRequest : " + XMLHttpRequest.status);
				console.log("textStatus     : " + textStatus);
				console.log("errorThrown    : " + errorThrown.message);
				swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
			}
		});
	}catch(e) {
		// エラー発生
		swalAlert.fire({html: "エラーが発生しました。"});
	}

}

/* アクセスログ検索 */
function searchAccesslog() {

	var logArea = $("#logArea");
	logArea.empty();

	try {

		// ロード中画面表示
		$('#loading').css('visibility', 'visible');

		var startDate = "1970/01/01";
		var endDate = "9999/12/31";
		if ($('#start_date').val() != "") {
			startDate = $('#start_date').val();
		}
		if ($('#end_date').val() != "") {
			endDate = $('#end_date').val();
		}

		$.ajax({
			type        : "POST",
			url         : "accessedlog.html?searchAccesslog",
			data        : {acc_userID: $("#acc_userID").val(), startDate: startDate, endDate: endDate, tenantId: $('#tenant_id').val(), userId: $('#user_id').val(), executionCode: $('#execution_code').val(), resultCode: $('#result_code').val()},
			dataType    : "JSON"
		})
		// 通信成功
		.done(function(result) {
			if (null != result) {
				// アクセスログ表示
				loadAccesslog(result);
				// ロード中画面非表示
				$('#loading').css('visibility', 'hidden');
			}else {
				// エラー発生
				swalAlert.fire({html: "エラーが発生しました。"});
				// ロード中画面非表示
				$('#loading').css('visibility', 'hidden');
			}
		})
		// 通信失敗
		.fail(function(XMLHttpRequest, textStatus, errorThrown) {
			if (XMLHttpRequest.status == 200) {
				if ("" == XMLHttpRequest.responseText || null == XMLHttpRequest.responseText) {
					// 該当ログ無し
					logArea.append("<br><font size='3' color='#AAAAAA'>検索条件に一致するログはありません。</font>");
				}else {
					// 例外エラー表示
					document.write(XMLHttpRequest.responseText);
					document.close();
				}
			}else {
				console.log("ajax通信に失敗しました");
				console.log("XMLHttpRequest : " + XMLHttpRequest.status);
				console.log("textStatus     : " + textStatus);
				console.log("errorThrown    : " + errorThrown.message);
				swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
			}
			// ロード中画面非表示
			$('#loading').css('visibility', 'hidden');
		});
	}catch(e) {
		// エラー発生
		swalAlert.fire({html: "エラーが発生しました。"});
		// ロード中画面非表示
		$('#loading').css('visibility', 'hidden');
	}

}

/* アクセスログ表示 */
function loadAccesslog(result) {

	var logArea = $("#logArea");

	var $table = $("<table class='table table-bordered dataTable' id='dataTable' width='100%' style='background-color: #FFFFFF;'></table>");
	var $thead = $("<thead></thead>");
	var $tbody = $("<tbody></tbody>");
	var $tr = $("<tr></tr>");
	var access_tenant_name = "";
	var access_user_id = "";
	var access_user_name = "";

	$tr.append($("<th style='display: none;'>アクセス番号</th>"));
	$tr.append($("<th>アクセス日時</th>"));
	$tr.append($("<th>アクセス元IP</th>"));
	$tr.append($("<th>テナント</th>"));
	$tr.append($("<th>ユーザーID</th>"));
	$tr.append($("<th>ユーザー名</th>"));
	$tr.append($("<th>実行プログラム</th>"));
	$tr.append($("<th>処理結果</th>"));

	$thead.append($tr);
	$table.append($thead);

	for (var i = 0; i < result.length; i++) {

		access_tenant_name = "";
		if (result[i].access_tenant_name != null) {
			access_tenant_name = result[i].access_tenant_name;
		}
		access_user_id = "";
		if (result[i].access_user_id != null && result[i].access_user_id != "null") {
			access_user_id = result[i].access_user_id;
		}
		access_user_name = "";
		if (result[i].access_user_name != null) {
			access_user_name = result[i].access_user_name;
		}

		$tr = $("<tr style='cursor:pointer' onclick=\"logDetails('" + result[i].access_no + "')\"></tr>");
		$tr.append($("<td style='display: none;'>" + result[i].access_no + "</td>"));
		$tr.append($("<td>" + result[i].str_access_date + "</td>"));
		$tr.append($("<td>" + result[i].access_ip + "</td>"));
		$tr.append($("<td>" + access_tenant_name + "</td>"));
		$tr.append($("<td>" + access_user_id + "</td>"));
		$tr.append($("<td>" + access_user_name + "</td>"));
		$tr.append($("<td>" + result[i].prg_name + "</td>"));
		$tr.append($("<td>" + result[i].result_name + "</td>"));
		$tbody.append($tr);
	}

	$table.append($tbody);
	logArea.append($table);

	var limit = "";
	if (10000 <= result.length) {
		limit = "　※検索結果が制限値10,000件を超えています";
	}

	try {
		/* dataTableの初期設定 */
		$('#dataTable').dataTable({
			"language": {						/* 日本語化 */
				"decimal": ".",
				"thousands": ",",
				"sProcessing": "処理中...",
				"sLengthMenu": "_MENU_ 件表示",
				"sZeroRecords": "データがありません。",
				"sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示中" + limit,
				"sInfoEmpty": " 0 件中 0 から 0 まで表示",
				"sInfoFiltered": "（全 _MAX_ 件より抽出）",
				"sInfoPostFix": "",
				"sSearch": "検索:",
				"sUrl": "",
				"oPaginate": {
				"sFirst": "先頭",
				"sPrevious": "<",
				"sNext": ">",
				"sLast": "最終"
				}
			},
			//"sPaginationType": "full_numbers",	/* two_button:進むと戻るのみ, full_numbers:全ボタン表示 */
			"asStripeClasses": ['even','even'],	/* odd, even にすると1行置きに色が変わる(odd, evenはcssで設定) */
			"bAutoWidth": true,					/* 列幅自動調整機能 */
			"sDom": "lfrtip",					/* 列幅変更機能有効(パラメータ不明) */
			"bFilter": true,					/* フィルター機能 */
			"bPaginate": true,					/* ページング機能 */
			"bInfo": true,						/* ページ情報(n件中aからbまで表示) */
			"bLengthChange": true,				/* 1ページの表示件数切替 */
			"aLengthMenu": [10,20,50,100],		/* 1ページの表示件数切替コンボボックスソース */
			"iDisplayLength": 10,				/* 1ページの表示件数初期値 */
			"sScrollX": "100%",					/* 水平スクロール機能(幅をピクセル指定、空は無効) */
			"sScrollY": "",						/* 垂直スクロール機能(高さをピクセル指定、空は無効) */
			"bScrollCollapse": false,			/* 垂直スクロール有効時に表示幅固定 */
			"bSort": true,						/* 整列機能 */
			"aaSorting": [],					/* 整列初期状態(空[]は整列しない) */
			"buttons": [						/* 各種出力ボタン表示(copy,csv,excel,pdf,print) */
				{
					bom: true,					/* エクセルで文字化けしないようにbom付き */
					extend: 'csv',
					filename: 'アクセスログ_' + $('#start_date').val().replace(" ", "") + '-' + $('#end_date').val().replace(" ", "")
				},
				{
					extend: 'excel',
					filename: 'アクセスログ_' + $('#start_date').val().replace(" ", "") + '-' + $('#end_date').val().replace(" ", ""),
					title: 'アクセスログ'
				},
//				{
//					extend: 'pdfHtml5',
//					text: 'PDF',
//					customize: function (doc) {
//						doc.defaultStyle.font= 'GenShin';		// pdf日本語文字化け対策
//						doc.styles.tableHeader.fontSize = 5;	// ヘッダーフォントサイズ
//						doc.defaultStyle.fontSize = 5;			// フォントサイズ
//					},
//					filename: 'アクセスログ_' + $('#start_date').val().replace(" ", "") + '-' + $('#end_date').val().replace(" ", ""),
//					title: 'アクセスログ'
//				},
//				{
//					extend: 'print',
//					title: 'アクセスログ',
//					customize: function (win) {
//						$(win.document.body).css('font-size', '1px');										// 全体フォントサイズ
//						$(win.document.body).css('white-space', 'nowrap');									// 改行しない
//						$(win.document.body).find('table').addClass('compact').css('font-size', '1px');		// テーブルフォントサイズ
//						$(win.document.body).find('h1').css('font-size', '20px');							// タイトルフォントサイズ
//					}
//				}
	        ]
		} );
		// 出力ボタンを右寄せ表示
		$('.dt-buttons').css('float', 'right');
	}catch(e) {
		// エラー発生
		swalAlert.fire({html: "エラーが発生しました。"});
		// ロード中画面非表示
		$('#loading').css('visibility', 'hidden');
	}

}

/* ログ詳細表示 */
function logDetails(access_no) {

	var lda = $("#logDetailsArea");
	lda.empty();

	try {
		$.ajax({
			type        : "POST",
			url         : "accessedlog.html?accessNoSelect",
			data        : {acc_userID: $("#acc_userID").val(), accessNo: access_no},
			dataType    : "JSON"
		})
		// 通信成功
		.done(function(result) {
			if (null != result) {

				var access_tenant_name = "";
				var access_user_id = "";
				var access_user_name = "";
				var contents = "";
				var err_msg = "";

				if (result.access_tenant_name != null) {
					access_tenant_name = result.access_tenant_name;
				}
				if (result.access_user_id != null && result.access_user_id != "null") {
					access_user_id = result.access_user_id;
				}
				if (result.access_user_name != null) {
					access_user_name = result.access_user_name;
				}
				if (result.contents != null) {
					contents = result.contents;
				}
				if (result.err_msg != null) {
					err_msg = result.err_msg;
				}

				lda.append($("<div class='form-group'><label class='control-label'>アクセス日時　　：" + result.str_access_date + "</label></div>"));
				lda.append($("<div class='form-group'><label class='control-label'>アクセス元IP　　：" + result.access_ip + "</label></div>"));
				lda.append($("<div class='form-group'><label class='control-label'>テナント　　　　：" + access_tenant_name + "</label></div>"));
				lda.append($("<div class='form-group'><label class='control-label'>ユーザーID　　　：" + access_user_id + "</label></div>"));
				lda.append($("<div class='form-group'><label class='control-label'>ユーザー名　　　：" + access_user_name + "</label></div>"));
				lda.append($("<div class='form-group'><label class='control-label'>実行プログラム　：" + result.prg_name + "</label></div>"));
				lda.append($("<div class='form-group'><label class='control-label'>処理結果　　　　：" + result.result_name + "</label></div>"));
				lda.append($("<div class='form-group'><label class='control-label'>処理内容</label><textarea class='form-control' rows='9' style='background-color: #FFFFFF;' readonly>" + contents + "</textarea></div>"));
				lda.append($("<div class='form-group'><label class='control-label'>エラーメッセージ</label><textarea class='form-control' rows='3' style='background-color: #FFFFFF;' readonly>" + err_msg + "</textarea></div>"));

				// ログ詳細モーダルを開く
				$('#logModal').modal();
			}else {
				// エラー発生
				swalAlert.fire({html: "エラーが発生しました。"});
			}
		})
		// 通信失敗
		.fail(function(XMLHttpRequest, textStatus, errorThrown) {
			if (XMLHttpRequest.status == 200) {
				if ("" == XMLHttpRequest.responseText || null == XMLHttpRequest.responseText) {
					// 対象ログ無し
					swalInfo.fire({html: "対象のログが見つかりませんでした。"});
				}else {
					// 例外エラー表示
					document.write(XMLHttpRequest.responseText);
					document.close();
				}
			}else {
				console.log("ajax通信に失敗しました");
				console.log("XMLHttpRequest : " + XMLHttpRequest.status);
				console.log("textStatus     : " + textStatus);
				console.log("errorThrown    : " + errorThrown.message);
				swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
			}
		});
	}catch(e) {
		// エラー発生
		swalAlert.fire({html: "エラーが発生しました。"});
	}

}
