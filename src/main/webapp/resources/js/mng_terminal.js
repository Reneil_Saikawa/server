var validator;
var oTable;
var nowlibName;

$(function(){

	/* ブラウザ戻るボタン抑止 */
	history.forward();

	/* バリデーション */
	validator = $('form').validate({
		rules: {
			term_id: {
				required: true,
				numAlphaSymbol: true,
				cFileName: true,
				exFileName: true
			},
			term_ctg: {
				required: true
			},
			term_name: {
				required: true
			},
			lib_id: {
				required: true
			},
			m_max_time: {
				required: true,
				cNumber: true
			},
			live_width: {
				required: true,
				cNumber: true
			},
			live_min_fps: {
				required: true,
				cNumber: true
			},
			live_max_fps: {
				required: true,
				cNumber: true
			},
			start_date: {
				required: true
			},
			end_date: {
				required: true,
				compareDate: 'start_date'
			}
		},
		messages: {
			term_id: {
				required: required,
				numAlphaSymbol: numAlphaSymbol,
				cFileName: cFileName,
				exFileName: exFileName
			},
			term_ctg: {
				required: required
			},
			term_name: {
				required: required
			},
			lib_id: {
				required: required
			},
			m_max_time: {
				required: required,
				cNumber: number
			},
			live_width: {
				required: required,
				cNumber: number
			},
			live_min_fps: {
				required: required,
				cNumber: number
			},
			live_max_fps: {
				required: required,
				cNumber: number
			},
			start_date: {
				required: required
			},
			end_date: {
				required: required,
				compareDate: compareDate
			}
		},
		errorPlacement: function(error, element){
			if ((element.attr("name") == "start_date") || (element.attr("name") == "end_date")) {
				error.insertAfter($('#'+ element.attr('name') + '_err'));
			} else {
				error.insertAfter(element);
			}
		}
	});

	/* リストの行クリック時 */
	$('#dataTable tbody tr').click(function(event) {
		if ($(this).hasClass('row_selected')) {
			// 選択行をクリック（行の選択を解除）
			oTable.$(this).removeClass('row_selected');
			oTable.$(this).find('input[name=r_chk]').prop('checked',false);
		} else {
			// 非選択業をクリック（行を選択）
			// 一旦全ての行の選択を解除する
			oTable.$('.sel_row').removeClass('row_selected');
			oTable.$('.sel_row').find('input[name=r_chk]').prop('checked',false);
			// 行を選択
			oTable.$(this).addClass('row_selected');
			oTable.$(this).find('input[name=r_chk]').prop('checked',true);
		}
	} );

	/* dataTableの初期設定 */
	oTable = $('#dataTable').dataTable({
		"language": {						/* 日本語化 */
			"decimal": ".",
			"thousands": ",",
			"sProcessing": "処理中...",
			"sLengthMenu": "_MENU_ 件表示",
			"sZeroRecords": "データがありません。",
			"sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示中",
			"sInfoEmpty": " 0 件中 0 から 0 まで表示",
			"sInfoFiltered": "（全 _MAX_ 件より抽出）",
			"sInfoPostFix": "",
			"sSearch": "検索:",
			"sUrl": "",
			"oPaginate": {
			"sFirst": "先頭",
			"sPrevious": "<",
			"sNext": ">",
			"sLast": "最終"
			}
		},
		//"sPaginationType": "full_numbers",	/* two_button:進むと戻るのみ, full_numbers:全ボタン表示 */
		"asStripeClasses": ['even','even'],	/* odd, even にすると1行置きに色が変わる(odd, evenはcssで設定) */
		"bAutoWidth": true,					/* 列幅自動調整機能 */
		"sDom": "lfrtip",					/* 列幅変更機能有効(パラメータ不明) */
		"bFilter": true,					/* フィルター機能 */
		"bPaginate": true,					/* ページング機能 */
		"bInfo": true,						/* ページ情報(n件中aからbまで表示) */
		"bLengthChange": true,				/* 1ページの表示件数切替 */
		"aLengthMenu": [10,20,50,100],		/* 1ページの表示件数切替コンボボックスソース */
		"iDisplayLength": 10,				/* 1ページの表示件数初期値 */
		"sScrollX": "",						/* 水平スクロール機能(幅をピクセル指定、空は無効) */
		"sScrollY": "",						/* 垂直スクロール機能(高さをピクセル指定、空は無効) */
		"bScrollCollapse": false,			/* 垂直スクロール有効時に表示幅固定 */
		"bSort": true,						/* 整列機能 */
		"aaSorting": [],					/* 整列初期状態(空[]は整列しない) */
		"aoColumnDefs": [ {
			"bSortable": false, "aTargets": [ 0 ]	/* 0カラム目の整列機能を無効にする */
		} ]
	} );

	// jsonをオブジェクトに変換
	var jsonObj = JSON.parse("[" + $("#libraryTreeData").val() + "]");
	// 最上位階層をソート
	jsonObj.sort(function(o, next_o) {
		if (o.disNo != 0 && o.disNo != null && next_o.disNo != 0 && next_o.disNo != null) {
			// 表示順 - 昇順に並べ替え
			return (o.disNo < next_o.disNo ? -1 : 1);
		}else {
			switch (Cookies.get('reiss_sortOrder')) {
			case "ascSort":
				// 昇順に並べ替え
				return (o.text < next_o.text ? -1 : 1);
			case "descSort":
				// 降順に並べ替え
				return (o.text > next_o.text ? -1 : 1);
			default:
				// 降順に並べ替え
				return (o.text > next_o.text ? -1 : 1);
			}
		}
	})
	// nodesをソート
	jsonObj = sortTreeData(jsonObj);
	// jsonオブジェクトを文字列に変換
	var jsonStr = JSON.stringify(jsonObj);
	// ライブラリツリーデータセット
	$("#libraryTreeData").val(jsonStr);

	$("#tree").treeview({
		data: jsonStr,
		expandIcon: 'glyphicon glyphicon-chevron-right',
		collapseIcon: 'glyphicon glyphicon-chevron-down',
		showCheckbox: true,
		//levels: 99,   // Cllapsed 階層リストがある場合開いた(-)状態で表示
		levels: 1,   // Cllapsed 階層リストがある場合閉じた(+)状態で表示
		showBorder: true,
		//color: "#A9BFD6",
		//backColor: "#494D51",
		//backColor: "#F1F1F1",
		onNodeSelected: function(event, node) {
			// 選択解除
			$('#tree').treeview('unselectNode', [ node.nodeId, { silent: true } ]);
			// 選択時チェック
			if (!node.state.checked) {
				$('#tree').treeview('checkNode', [ node.nodeId, { silent: false } ]);
			}else {
				$('#tree').treeview('uncheckNode', [ node.nodeId, { silent: false } ]);
			}
		}
	});

	// ツリーチェック
	$('#tree').on('nodeChecked', function(event, node) {
		$('#tree').treeview('uncheckAll', { silent: true });
		$('#tree').treeview('checkNode', [ node.nodeId, { silent: true } ]);
		$('#lib_id').val(node.dirId);
		nowlibName = node.text;
		// 撮影日付フォルダパスを変更
		createDateFolderPath();
	});

	// ツリーチェック外し
	$('#tree').on('nodeUnchecked', function(event, node) {
		// チェックを外さない
		$('#tree').treeview('checkNode', [ node.nodeId, { silent: false } ]);
	});

	/* リストの編集ボタンクリック時 */
	$('#edit').click(function() {
		var selectedRow = getSelectedRow(oTable);
		if (selectedRow.length == 1) {
			// 選択が1行のとき

			// 保存先ライブラリツリーを全て折りたたむ
			$('#tree').treeview('collapseAll', { silent: true });
			// ライブラリIDから保存先ライブラリツリーをチェック
			libIdCheckTree(selectedRow[0].cells.item(5).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			// リストの値をフォームに転記する
			$('#term_id').val(selectedRow[0].cells.item(1).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#term_ctg').val(selectedRow[0].cells.item(2).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#term_name').val(selectedRow[0].cells.item(4).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#lib_id').val(selectedRow[0].cells.item(5).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			if (selectedRow[0].cells.item(7).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#year_folder_flg').prop('checked', true);
			}else {
				$('#year_folder_flg').prop('checked', false);
			}
			if (selectedRow[0].cells.item(8).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#month_folder_flg').prop('checked', true);
			}else {
				$('#month_folder_flg').prop('checked', false);
			}
			if (selectedRow[0].cells.item(9).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#day_folder_flg').prop('checked', true);
			}else {
				$('#day_folder_flg').prop('checked', false);
			}
			if (selectedRow[0].cells.item(10).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#japanese_flg').prop('checked', true);
			}else {
				$('#japanese_flg').prop('checked', false);
			}

			$('#m_max_time').val(selectedRow[0].cells.item(11).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#live_width').val(selectedRow[0].cells.item(12).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#live_min_fps').val(selectedRow[0].cells.item(13).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#live_max_fps').val(selectedRow[0].cells.item(14).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#start_date').val(selectedRow[0].cells.item(15).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#end_date').val(selectedRow[0].cells.item(16).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			if (selectedRow[0].cells.item(17).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#del_flg').prop('checked', true);
			}else {
				$('#del_flg').prop('checked', false);
			}

			// 端末IDを編集不可にする
			$('#term_id').attr('readonly', true);

			// バリデートをリセットする
			validator.resetForm();

			// メッセージを閉じる
			$('#success').alert('close');
			$('#add_danger').alert('close');
			$('#add_license_danger').alert('close');
			$('#up_license_danger').alert('close');

			// ボタンを「更新」に変更
			$('#reflect').attr('name', 'update');
			$('#reflect').html("<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span>&thinsp;更新");

			// リストを閉じる
			$('#listModal').modal('hide');

			// 撮影日付フォルダ設定表示・パスを変更
			checkDateFolderEnable();

		}else {
			swalInfo.fire({html: "編集する端末を選択してください。"});
			return false;
		}
	});

	/* カレンダー表示 */
	$('#datetimepicker-start').datetimepicker({
		dayViewHeaderFormat: 'YYYY年 MMMM',
		tooltips: {
		close: '閉じる',
		selectMonth: '月を選択',
		prevMonth: '前月',
		nextMonth: '次月',
		selectYear: '年を選択',
		prevYear: '前年',
		nextYear: '次年',
		selectTime: '時間を選択',
		selectDate: '日付を選択',
		prevDecade: '前期間',
		nextDecade: '次期間',
		selectDecade: '期間を選択',
		prevCentury: '前世紀',
		nextCentury: '次世紀'
		},
		format: 'YYYY/MM/DD',
		minDate: $('#tenantStart').val(),
		maxDate: $('#tenantEnd').val() + ' 23:59:59',
		locale: 'ja',
		ignoreReadonly: true,
		//sideBySide: true,
		showClose: true,
		widgetPositioning: {
			horizontal: 'left',
			vertical: 'auto'
		}
	});
	$('#datetimepicker-end').datetimepicker({
		dayViewHeaderFormat: 'YYYY年 MMMM',
		tooltips: {
		close: '閉じる',
		selectMonth: '月を選択',
		prevMonth: '前月',
		nextMonth: '次月',
		selectYear: '年を選択',
		prevYear: '前年',
		nextYear: '次年',
		selectTime: '時間を選択',
		selectDate: '日付を選択',
		prevDecade: '前期間',
		nextDecade: '次期間',
		selectDecade: '期間を選択',
		prevCentury: '前世紀',
		nextCentury: '次世紀'
		},
		format: 'YYYY/MM/DD',
		minDate: $('#tenantStart').val(),
		maxDate: $('#tenantEnd').val() + ' 23:59:59',
		locale: 'ja',
		ignoreReadonly: true,
		//sideBySide: true,
		showClose: true,
		widgetPositioning: {
			horizontal: 'left',
			vertical: 'auto'
		}
	});

	/* 一覧ボタンクリック時 */
	$('#list').click(function() {
		// リストを開く
		$('#listModal').modal();
	});

	/* クリアボタンクリック時 */
	$('#clearInputArea').click(function() {
		// メッセージを閉じる
		$('#success').alert('close');
		$('#add_danger').alert('close');
		$('#add_license_danger').alert('close');
		$('#up_license_danger').alert('close');
		 // フォームをクリアする
		clearForm(this.form);
		// バリデートをリセットする
		validator.resetForm();
		// 端末区分を先頭位置にする
		$('#term_ctg').prop('selectedIndex', 0);
		// 保存先ライブラリツリーを全て折りたたむ
		$('#tree').treeview('collapseAll', { silent: true });
		// 保存先ライブラリツリーの最上位をチェック
		$('#tree').treeview('checkNode', [ 0, { silent: false } ]);
		// 端末IDを編集可にする
		$('#term_id').attr('readonly', false);
		// 動画撮影最大時間 (秒)のデフォルト値を設定する
		$('#m_max_time').val(60);
		// ライブ撮影サイズ (横)のデフォルト値を設定する
		$('#live_width').val(0);
		// フレームレートのデフォルト値を設定する
		$('#live_min_fps').val(2);
		$('#live_max_fps').val(10);
		// ボタンを「追加」に変更
		$('#reflect').attr('name', 'add');
		$('#reflect').html("<span class='glyphicon glyphicon-plus' aria-hidden='true'></span>&thinsp;追加");
		// 撮影日付フォルダ設定表示・パスをクリア
		checkDateFolderEnable();
	});

	if ($('#add_danger').length) {
		// ライブラリIDから保存先ライブラリツリーをチェック
		libIdCheckTree($('#lib_id').val());
		// 端末ID入力欄をエラーにする
		$('#term_id').addClass("error");
	}else if ($('#add_license_danger').length) {
		// ライブラリIDから保存先ライブラリツリーをチェック
		libIdCheckTree($('#lib_id').val());
	}else {
		if ($('#term_id').val() != "" && $('#term_id').val() != null) {
			// 追加・更新時処理
			// ライブラリIDから保存先ライブラリツリーをチェック
			libIdCheckTree($('#lib_id').val());
			// 端末IDを編集不可にする
			$('#term_id').attr('readonly', true);
			// ボタンを「更新」に変更
			$('#reflect').attr('name', 'update');
			$('#reflect').html("<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span>&thinsp;更新");
		}else {
			// 新規時
			// 保存先ライブラリツリーの最上位をチェック
			$('#tree').treeview('checkNode', [ 0, { silent: false } ]);
			// 動画撮影最大時間 (秒)のデフォルト値を設定する
			$('#m_max_time').val(60);
			// ライブ撮影サイズ (横)のデフォルト値を設定する
			$('#live_width').val(0);
			// フレームレートのデフォルト値を設定する
			$('#live_min_fps').val(2);
			$('#live_max_fps').val(10);
		}
	}

	/* QRコードボタンクリック時 */
	$('#QRcodeArea').click(function() {
		var selectedRow = getSelectedRow(oTable);
		if (selectedRow.length == 1) {
			// 選択が1行のとき

			var termName = selectedRow[0].cells.item(4).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "");
			var inUrl = $('#inUrl').val();
			var outUrl = $('#outUrl').val();
			var tenantId = $('#tenantId').val();
			var termId = selectedRow[0].cells.item(1).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "");
			//ヘッダー変更
			$('#QRheader').html("&thinsp;" + termName);
			//QRコード作成
			var json = '{"1":"' + inUrl + '","2":"' + outUrl + '","3":"' + tenantId + '","4":"' + termId + '"}';
			$('#QRcode').html("");
			$('#QRcode').qrcode({width: 500, height: 500, text: json});
			// リストを開く
			$('#QRcodeModal').modal();
			$('#listModal').css('z-index', 50);

		}else {
			swalInfo.fire({html: "QRコード表示する端末を選択してください。"});
			return false;
		}
	});

	/* QRコードモーダルを閉じる時 */
	$('#QRcodeModal').on('hidden.bs.modal', function () {
		$('#listModal').css('z-index', 1050);
		$('body').addClass('modal-open');
	});

	// 撮影日付フォルダ設定・パスを変更
	checkDateFolderEnable();

});

/* ディレクトリツリーデータ並べ替え(sort)  */
function sortTreeData(jsonObj) {
	for (var key in jsonObj){
		if (typeof jsonObj[key] == "object") {
			if(Array.isArray(jsonObj[key])) {
				// 連想配列の場合
				// nodesをsort
				jsonObj[key].sort(function(o, next_o) {
					if (o.disNo != 0 && o.disNo != null && next_o.disNo != 0 && next_o.disNo != null) {
						// 表示順 - 昇順に並べ替え
						return (o.disNo < next_o.disNo ? -1 : 1);
					}else {
						switch (Cookies.get('reiss_sortOrder')) {
						case "ascSort":
							// 昇順に並べ替え
							return (o.text < next_o.text ? -1 : 1);
						case "descSort":
							// 降順に並べ替え
							return (o.text > next_o.text ? -1 : 1);
						default:
							// 降順に並べ替え
							return (o.text > next_o.text ? -1 : 1);
						}
					}
				})
				// forEachで要素ごとにに再帰呼び出し
				jsonObj[key].forEach(function(item){
					sortTreeData(item) ;
				});
			}else {
				// 配列はそのまま再帰呼び出し
				sortTreeData(jsonObj[key]) ;
			}
		}
	}
	return jsonObj;
}

/* 選択されている行を返却する */
function getSelectedRow(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}

/* ライブラリIDから保存先ライブラリツリーをチェック */
function libIdCheckTree(libId) {
	if ($('#tree').treeview('searchDirId', libId)[0]) {
		var checkNodeId = $('#tree').treeview('searchDirId', libId)[0].nodeId;			// libIdからnodeIdを取得
		$('#tree').treeview('revealNode', [ checkNodeId, { silent: true } ]);			// nodeIdまでツリーを開く
		$('#tree').treeview('checkNode', [ checkNodeId, { silent: false } ]);			// nodeIdのツリー要素をチェック
	}
}

/* 撮影日フォルダ作成チェック時 */
function checkDateFolderEnable() {
	// 撮影日フォルダ設定表示チェック
	if ($("#year_folder_flg").prop("checked") == true || $("#month_folder_flg").prop("checked") == true || $("#day_folder_flg").prop("checked") == true) {
		$('#disDateFolderEnable').collapse('show');
	}else {
		$('#disDateFolderEnable').collapse('hide');
	}
	// 撮影日付フォルダパス変更
	createDateFolderPath();
}

/* 撮影日付フォルダパス変更 */
function createDateFolderPath() {

	var datePath = "/";
	var year  = "yyyy";
	var month = "mm";
	var day   = "dd";
	// 撮影年月日-日本語表記チェック
	if ($("#japanese_flg").prop("checked") == true) {
		year  += "年";
		month += "月";
		day   += "日";
	}

	// 撮影日付フォルダパス作成
	if ($("#year_folder_flg").prop("checked") == true) {
		datePath += year;
		if ($("#month_folder_flg").prop("checked") == true) {
			datePath += "/" + month;
			if ($("#day_folder_flg").prop("checked") == true) {
				datePath += "/" + day;
			}
		}else {
			if ($("#day_folder_flg").prop("checked") == true) {
				datePath += "/";
				if ($("#japanese_flg").prop("checked") == true) {
					datePath += month + day;
				}else {
					datePath += month + "-" + day;
				}
			}
		}
	}else {
		if ($("#japanese_flg").prop("checked") == true) {
			datePath += year + month;
		}else {
			datePath += year + "-" + month;
		}
		if ($("#day_folder_flg").prop("checked") == true) {
			if ($("#month_folder_flg").prop("checked") == true) {
				datePath += "/" + day;
			}else {
				if ($("#japanese_flg").prop("checked") == true) {
					datePath += day;
				}else {
					datePath += "-" + day;
				}
			}
		}else {
			if ($("#month_folder_flg").prop("checked") == false) {
				datePath = "";
			}
		}
	}

	// 撮影日付フォルダパスを変更
	$('#date_folder_path').text(nowlibName + datePath);
}
