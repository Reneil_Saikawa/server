/**
 * validator名：compareDate
 * 入力された日付の大小チェック。
 * 「入力日付＞比較対象」の結果を返します。
 * value : 入力日付
 * element : HTMLInputElement
 * params : 比較対象項目ID
 */
jQuery.validator.addMethod('compareDate', function(value, element, params) {
	var start = document.getElementById(params).value;
	var end = value;

	var dt1 = new Date(start);
	var dt2 = new Date(end);

	if (dt1 > dt2) {
	    // チェックNG
		return false;
	}
	return true;
});

/**
 * validator名：comparelogStartDate
 * 入力された日付の大小チェック。(ログ検索用開始日時)
 * 「入力日付＞比較対象」の結果を返します。
 * value : 入力日付
 * element : HTMLInputElement
 * params : 比較対象項目ID
 */
jQuery.validator.addMethod('comparelogStartDate', function(value, element, params) {
	var start = value;
	var end = document.getElementById(params).value;

	if (start == "" || start == null || end == "" || end == null) {
		return true;
	}

	var dt1 = new Date(start);
	var dt2 = new Date(end);

	if (dt1 > dt2) {
	    // チェックNG
		return false;
	}
	return true;
});

/**
 * validator名：compareEndlogDate
 * 入力された日付の大小チェック。(ログ検索用終了日時)
 * 「入力日付＞比較対象」の結果を返します。
 * value : 入力日付
 * element : HTMLInputElement
 * params : 比較対象項目ID
 */
jQuery.validator.addMethod('comparelogEndDate', function(value, element, params) {
	var start = document.getElementById(params).value;
	var end = value;

	if (start == "" || start == null || end == "" || end == null) {
		return true;
	}

	var dt1 = new Date(start);
	var dt2 = new Date(end);

	if (dt1 > dt2) {
	    // チェックNG
		return false;
	}
	return true;
});

/**
 * validator名：compareLibId
 * 自ライブラリと親ライブラリチェック。
 * 「自ライブラリ==親ライブラリ」の結果を返します。
 * value : 親ライブラリ
 * element : HTMLInputElement
 * params : 比較対象項目ID
 */
jQuery.validator.addMethod('compareLibId', function(value, element, params) {
	var lib_id = document.getElementById(params).value;
	var parent_lid = value;

	if (parent_lid == "") {
		return true;
	}
	else if (lib_id == parent_lid) {
	    // チェックNG
		return false;
	}
	return true;
});

/**
 * validator名：cNumber
 * 半角数字かどうかチェック。
 * value : 入力値
 * element : HTMLInputElement
 */
jQuery.validator.addMethod('cNumber', function(value, element) {
	return value.match(/[^0-9]+/) == null;
});

/**
 * validator名：locateNumber
 * 座標表現かどうかチェック。
 * value : 入力値
 * element : HTMLInputElement
 */
jQuery.validator.addMethod('locateNumber', function(value, element) {
	return value.match(/[^0-9\.]+/) == null;
});

/**
 * validator名：numAlphabet
 * 半角英数かどうかチェック。
 * value : 入力値
 * element : HTMLInputElement
 */
jQuery.validator.addMethod('numAlphabet', function(value, element) {
	return value.match(/[^0-9A-Za-z]+/) == null;
});

/**
 * validator名：numAlphaSymbol
 * 半角文字かどうかチェック。（半角スペースもNG）
 * value : 入力値
 * element : HTMLInputElement
 */
jQuery.validator.addMethod('numAlphaSymbol', function(value, element) {
	for (var i=0; i<value.length; i++) {
        // 1文字ずつ文字コードをエスケープし、その長さが4文字未満なら半角
        var tmp = escape(value.charAt(i));
        if (tmp == '%20') {//半角スペース
        	return false;
        }
        if (tmp.length >= 4) {
        	return false;
        }
    }
	return true;
});

/**
 * validator名：cFileName
 * ファイル・フォルダ名チェック。
 * value : 入力値
 * element : HTMLInputElement
 */
jQuery.validator.addMethod('cFileName', function(value, element) {
	return value.match(/^.*[\\|/|:|\*|?|\"|<|>|\|].*$/) == null;
});

/**
 * validator名：exFileName
 * ファイル・フォルダ名チェック。
 * value : 入力値
 * element : HTMLInputElement
 */
jQuery.validator.addMethod('exFileName', function(value, element) {
	return value.match(/.jpg|.mp4|.webm/) == null;
});

/**
 * validator名：xlsFile
 * 帳票テンプレート対応ファイル(.xls, .xlsx)チェック
 * value : 入力値
 * element : HTMLInputElement
 */
jQuery.validator.addMethod('xlsFile', function(value, element) {
	if (value.slice(-4) == '.xls' || value.slice(-5) == '.xlsx') {
		return true;
	}
	return false;
});

/**
 * validator名：reportTag
 * 項目IDの帳票タグ予約語チェック
 * value : 入力値
 * element : HTMLInputElement
 */
jQuery.validator.addMethod('reportTag', function(value, element) {
	if (value == 'IMAGE') {
		return false;
	}else if (value == 'THUMBNAIL') {
		return false;
	}else if (value == 'FILE_NAME') {
		return false;
	}else if (value == 'FILE_PATH') {
		return false;
	}else if (value == 'FILE_SIZE') {
		return false;
	}else if (value == 'TERM_ID') {
		return false;
	}else if (value == 'TERM_NAME') {
		return false;
	}else if (value == 'PHOTO_DATE') {
		return false;
	}else if (value == 'LOCATE') {
		return false;
	}else if (value == 'LOCATE_LAT') {
		return false;
	}else if (value == 'LOCATE_LON') {
		return false;
	}
	return true;
});

/**
 * validator名：patLiteReq
 * パトライト有効時必須項目チェック
 * value : 入力値
 * element : HTMLInputElement
 */
jQuery.validator.addMethod('patLiteReq', function(value, element) {
	// パトライト制御表示チェック
	if ($("#patlite_enable").prop("checked") == true) {
		if (!value) {
			return false;
		}
	}
	return true;
});

/**
 * validator名：mapReq
 * 地図有効時必須項目チェック
 * value : 入力値
 * element : HTMLInputElement
 */
jQuery.validator.addMethod('mapReq', function(value, element) {
	// 地図有効化制御表示チェック
	if ($("#map_enable").prop("checked") == true) {
		if (!value) {
			return false;
		}
	}
	return true;
});

/**
 * validator名：frontPageReq
 * 帳票表紙有効時必須項目チェック
 * value : 入力値
 * element : HTMLInputElement
 */
jQuery.validator.addMethod('frontPageReq', function(value, element) {
	// 表紙出力フラグ表示チェック
	if ($("#front_page_flg").prop("checked") == true) {
		if (!value) {
			return false;
		}
	}
	return true;
});