// 動画録画用
const options = {mimeType: 'video/webm;codecs=vp8,opus'};
var mediaRecorder;
var recordedBlobs;

var rooms = [];
var myroom = 0;

$(document).ready(function() {
    // 表示ボタン制御
    $('#recStop').hide();

    // LIVE_ROOMを配列に変換
    rooms = LIVE_ROOM.split(',');

	// room重複を削除
    rooms = rooms.filter(function (x, i, self) {
		return self.indexOf(x) === i;
	});

    // 各要素を数値に変換
    rooms = rooms.map(Number);

	rooms.forEach(function(myroom, index) {
		var el = '<iframe id="' + myroom + '"'
			+ ' style="border: 0; width: 100%; height: 0; margin: 0; padding: 0;"'
			+ ' scrolling="no"'
			+ ' frameborder="no"'
			+ ' src="'  + window.location.origin + '/reiss/resources/livePlayer.html?janusHost=' + LIVE_APP + '&room=' + myroom + '"></iframe>';
		$('#live-area').append(el);
	});

	// 録画開始ボタンクリック
	$('#recStart').on('click', () => {
        try {
            // メディアレコーダーの準備
            const el = $('#viewArea').get(0);
            mediaRecorder = new MediaRecorder(el.srcObject, options);
            mediaRecorder.ondataavailable = handleDataAvailable;

            // 録画領域初期化
            recordedBlobs = [];
            // 録画開始(100ミリ秒ごとにondataavailableイベントハンドラを発生させる)
            mediaRecorder.start(100);
            // ボタン表示制御
            $('#recStart').hide();
            $('#recStop').show();
        }catch(e) {
            // 録画開始失敗
            swalAlert.fire({html: "録画を開始できませんでした。対象の中継は切断されています。<br>一度中継を閉じてから再度対象端末の中継を選択してください。"});
        }
    });

	// 録画停止ボタンクリック
    $('#recStop').on('click', () => {
        // 録画停止処理
        recStop();
    });

	// キャプチャボタンクリック
    $('#capture').on('click', () => {
        // 動画を取得しcanvasにセット
        const video = $('#viewArea').get(0);
        const $canvas = $('#canvas');
        $canvas.attr('width', video.videoWidth);
        $canvas.attr('height', video.videoHeight);
        $canvas[0].getContext('2d').drawImage(video, 0, 0, $canvas.width(), $canvas.height());

        // canvasのバイナリ化
        const dataUrl = $canvas[0].toDataURL('image/jpeg');
        const blob = dataURItoBlob(dataUrl);

        // キャプチャー日時を作成
        const dt = new Date();
        const Yer = dt.getFullYear();
        const Mon = dt.getMonth()+1;
        const Dat = dt.getDate();
        const Hor = dt.getHours();
        const Min = dt.getMinutes();
        const Sec = dt.getSeconds();
        const Mil = dt.getMilliseconds();
        const dateTime = Yer + ('00'+Mon).slice(-2) + ('00'+Dat).slice(-2) + ('00'+Hor).slice(-2) + ('00'+Min).slice(-2) + ('00'+Sec).slice(-2) + ('000'+Mil).slice(-3);

        // テナントIDと端末IDと位置情報を取得する
        const TENANT_ID = $('#tenant_id').get(0).innerText;
        const TERM_ID = $('#term_id').get(0).innerText;
        const LOCATE_LAT = $('#locate_lat').get(0).innerText;
        const LOCATE_LON = $('#locate_lon').get(0).innerText;

        // アップロードデータのFormData化
        let formData = new FormData();
        formData.append("TENANT_ID", TENANT_ID);
        formData.append("TERM_ID", TERM_ID);
        formData.append("FILE_FORMAT", "0403");
        formData.append("PHOTO_DATE", dateTime);
        formData.append("LOCATE_LAT", LOCATE_LAT);
        formData.append("LOCATE_LON", LOCATE_LON);
        formData.append("LOCATE", "");
        formData.append("contentsData", blob);

        // --------------------------
        // サーバーにアップロードする
        // --------------------------
        try {
            $.ajax({
                type        : "POST",
                url         : "sendContents.html",
                processData : false,
                contentType : false, // 送信するデータをFormDataにする場合、こうしないといけない。
                cache       : false,
                data        : formData
            })
            // 通信成功
            .done(function(result) {
                if (null != result) {

                    if (result.indexOf('<!DOCTYPE html>') !== -1 || result.indexOf('<!doctype html>') !== -1) {
                        // 例外エラー表示
                        document.write(result);
                        document.close();
                    }else if (result == "Has completed") {
                        toastSuccess.fire({title: "キャプチャーを保存しました。"});
                    }else if (result == "Library not found" || result == "Terminal not found") {
                        swalAlert.fire({html: "端末情報にエラーが発生しています。<br>システム管理者にお問い合わせください。"});
                    }else {
                        swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
                    }

                }else {
                    // エラー発生
                    swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
                }
            })
            // 通信失敗
            .fail(function(XMLHttpRequest, textStatus, errorThrown) {
                if (XMLHttpRequest.status == 200) {
                    if ("" == XMLHttpRequest.responseText || null == XMLHttpRequest.responseText) {
                        // エラー発生
                        swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
                    }else {
                        // 例外エラー表示
                        document.write(XMLHttpRequest.responseText);
                        document.close();
                    }
                }else {
                    console.log("ajax通信に失敗しました");
                    console.log("XMLHttpRequest : " + XMLHttpRequest.status);
                    console.log("textStatus     : " + textStatus);
                    console.log("errorThrown    : " + errorThrown.message);
                    swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
                }
            });
        }catch(e) {
            // エラー発生
            swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
        }
	});

    // ライブ中継モーダルが閉じられたとき
	$('#liveViewModal').on('hide.bs.modal', function () {
        if ($('#recStop').is(':visible')) {
            // 録画停止処理
            recStop();
        }
        // 録画処理待機時間考慮
        setTimeout(function(){
        	const el2 = $('#viewArea').get(0);
        	// モーダルのビデオ停止
        	el2.pause();
        },400);

        // ビデオソースクリア
        el2.srcObject = null;

        // トークを停止
		$('#tarkStop').click();
    });
});

/*
 * 録画停止処理
 */
function recStop() {
    // 録画停止
    mediaRecorder.stop();
    // 録画データをサーバに送信(mediaRecorder.stop時にondataavailable処理が終わらないため200ミリ秒待機)
    setTimeout(function(){
        sendRec();
    },200);
    // ボタン表示制御
    $('#recStart').show();
    $('#recStop').hide();
}

/*
 * 録画データをサーバに送信
 */
function sendRec() {
    if (recordedBlobs.length > 0) {

        // 録画データをバイナリ変換
        const videoBlob = new Blob(recordedBlobs, {type: 'video/webm;codecs=vp8,opus'});

        // キャプチャー日時を作成
        const dt = new Date();
        const Yer = dt.getFullYear();
        const Mon = dt.getMonth()+1;
        const Dat = dt.getDate();
        const Hor = dt.getHours();
        const Min = dt.getMinutes();
        const Sec = dt.getSeconds();
        const Mil = dt.getMilliseconds();
        const dateTime = Yer + ('00'+Mon).slice(-2) + ('00'+Dat).slice(-2) + ('00'+Hor).slice(-2) + ('00'+Min).slice(-2) + ('00'+Sec).slice(-2) + ('000'+Mil).slice(-3);

        // テナントIDと端末IDと位置情報を取得する
        const TENANT_ID = $('#tenant_id').get(0).innerText;
        const TERM_ID = $('#term_id').get(0).innerText;
        const LOCATE_LAT = $('#locate_lat').get(0).innerText;
        const LOCATE_LON = $('#locate_lon').get(0).innerText;

        // アップロードデータのFormData化
        let formData = new FormData();
        formData.append("TENANT_ID", TENANT_ID);
        formData.append("TERM_ID", TERM_ID);
        formData.append("FILE_FORMAT", "0404");
        formData.append("PHOTO_DATE", dateTime);
        formData.append("LOCATE_LAT", LOCATE_LAT);
        formData.append("LOCATE_LON", LOCATE_LON);
        formData.append("LOCATE", "");
        formData.append("contentsData", videoBlob);

        // --------------------------
        // サーバーにアップロードする
        // --------------------------
        try {
            $.ajax({
                type        : "POST",
                url         : "sendContents.html",
                processData : false,
                contentType : false, // 送信するデータをFormDataにする場合、こうしないといけない。
                cache       : false,
                data        : formData
            })
            // 通信成功
            .done(function(result) {
                if (null != result) {

                    if (result.indexOf('<!DOCTYPE html>') !== -1 || result.indexOf('<!doctype html>') !== -1) {
                        // 例外エラー表示
                        document.write(result);
                        document.close();
                    }else if (result == "Has completed") {
                        toastSuccess.fire({title: "録画を保存しました。"});
                    }else if (result == "Library not found" || result == "Terminal not found") {
                        swalAlert.fire({html: "端末情報にエラーが発生しています。<br>システム管理者にお問い合わせください。"});
                    }else {
                        swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
                    }

                }else {
                    // エラー発生
                    swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
                }
            })
            // 通信失敗
            .fail(function(XMLHttpRequest, textStatus, errorThrown) {
                if (XMLHttpRequest.status == 200) {
                    if ("" == XMLHttpRequest.responseText || null == XMLHttpRequest.responseText) {
                        // エラー発生
                        swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
                    }else {
                        // 例外エラー表示
                        document.write(XMLHttpRequest.responseText);
                        document.close();
                    }
                }else {
                    console.log("ajax通信に失敗しました");
                    console.log("XMLHttpRequest : " + XMLHttpRequest.status);
                    console.log("textStatus     : " + textStatus);
                    console.log("errorThrown    : " + errorThrown.message);
                    swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
                }
            });
        }catch(e) {
            // エラー発生
            swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
        }

    }else {
        // 録画失敗
        swalAlert.fire({html: "録画失敗しました。<br>対象の中継は切断されています。"});
    }

    // 録画領域初期化
    recordedBlobs = [];
}

/*
 * ondataavailableイベントハンドラ
 *
 * @param _event
 */
function handleDataAvailable(_event) {
    if (_event.data && _event.data.size > 0) {
        // 録画データを格納
        recordedBlobs.push(_event.data);
    }
}

/*
 * dataURI(base64)のバイナリ化
 *
 * @param _dataURI
 */
function dataURItoBlob(_dataURI) {

    const byteString = atob(_dataURI.split(',')[1]);
    const mimeString = _dataURI.split(',')[0].split(':')[1].split(';')[0]

    const ab = new ArrayBuffer(byteString.length);
    let ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    const bb = new Blob([ab], { "type": mimeString });
    return bb;
}
