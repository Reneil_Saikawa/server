$(function() {

	/* ライブラリクリック時 */
	$("#library").click(function() {
		window.location.href = 'mng_library.html';
	});

	/* 端末クリック時 */
	$("#terminal").click(function() {
		window.location.href = 'mng_terminal.html';
	});

	/* ユーザークリック時 */
	$("#user").click(function() {
		window.location.href = 'mng_userinfo.html';
	});

	/* 付帯情報項目名クリック時 */
	$("#additionalitem").click(function() {
		window.location.href = 'mng_additionalItem.html';
	});

	/* 帳票クリック時 */
	$("#reportparam").click(function() {
		window.location.href = 'mng_reportparam.html';
	});

	/* アクセスログクリック時 */
	$("#accessedlog").click(function() {
		window.location.href = 'mng_accessedlog.html';
	});

	/* 受信ログクリック時 */
	$("#receivedlog").click(function() {
		window.location.href = 'mng_receivedlog.html';
	});

	/* 戻るクリック時 */
	$("#close").click(function() {
		window.close();
	});

});

/* フォームをクリアする */
function clearForm (form) {
    $(form)
        .find("input, select, textarea")
        .not(":button, :submit, :reset, :hidden, [name='report_auth']")
        .val("")
        .prop("checked", false)
        .prop("selected", false)
    ;
    $(form).find("[name='report_auth']").prop("checked", false);
    $(form).find(":radio").filter("[data-default]").prop("checked", true);
    $(form).find(":checkbox").not("[name='report_auth']").val(true);
}
