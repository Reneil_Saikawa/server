/* success */
const swalSuccess = Swal.mixin({
	icon: 'success',
	allowOutsideClick: false,
	stopKeydownPropagation: false,
	keydownListenerCapture: true
})
/* alert */
const swalAlert = Swal.mixin({
	icon: 'warning',
	allowOutsideClick: false,
	stopKeydownPropagation: false,
	keydownListenerCapture: true
})
/* info */
const swalInfo = Swal.mixin({
	icon: 'info',
	allowOutsideClick: false,
	stopKeydownPropagation: false,
	keydownListenerCapture: true
})
/* confirm */
const swalConfirm = Swal.mixin({
	icon: 'question',
	allowOutsideClick: false,
	stopKeydownPropagation: false,
	keydownListenerCapture: true,
	showCancelButton: true,
	cancelButtonText: 'キャンセル',
	cancelButtonColor: '#ddd'
})
/* success */
const toastSuccess = Swal.mixin({
	icon: 'success',
	toast: true,
	position: 'top-end',
	showConfirmButton: false,
	timer: 3000,
	onOpen: function(toast) {
		toast.addEventListener('mouseenter', Swal.stopTimer)
		toast.addEventListener('mouseleave', Swal.resumeTimer)
	}
})
/* info */
const toastInfo = Swal.mixin({
	icon: 'info',
	toast: true,
	position: 'top-end',
	showConfirmButton: false,
	timer: 5000,
	onOpen: function(toast) {
		toast.addEventListener('mouseenter', Swal.stopTimer)
		toast.addEventListener('mouseleave', Swal.resumeTimer)
	}
})
