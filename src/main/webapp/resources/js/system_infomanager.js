$(function(){
	/* ブラウザ戻るボタン抑止 */
	history.forward();

	/* バリデーション */
	$('form').validate({
		rules: {
			system_name: {
				required: true
			},
			rootPath: {
				required: true
			},
			inUrl: {
				required: true
			},
			outUrl: {
				required: true
			},
			directory_image: {
				required: true
			}
		},
		messages: {
			system_name: {
				required: required
			},
			rootPath: {
				required: required
			},
			inUrl: {
				required: required
			},
			outUrl: {
				required: required
			},
			directory_image: {
				required: required
			}
		},
		errorPlacement: function(error, element){
			error.insertAfter(element);
		}
	});

	var blob = null; // 画像(BLOBデータ)

	//ファイル選択,アップロード
	$('#file_input').change(function() {
	      $('#directory_image').val($(this).val().replace("C:\\fakepath\\", ""));
	      if (this.files.length > 0) {
	    	    // 選択されたファイル情報を取得
	    	    var file = this.files[0];

	    	    // readerのresultプロパティに、データURLとしてエンコードされたファイルデータを格納
	    	    var reader = new FileReader();
	    	    reader.readAsDataURL(file);

	    	    reader.onload = function() {
	    	      $('#thumbnail').attr('src', reader.result );
	    	      $('#data_src').val(reader.result);
	    	    }
	      }
	});


});
