var validator;
var oTable;

$(function(){
	/* ブラウザ戻るボタン抑止 */
	history.forward();

	/* バリデーション */
	validator = $('form').validate({
		rules: {
			tenant_id: {
				required: true,
				numAlphaSymbol: true,
				cFileName: true,
				exFileName: true
			},
			tenant_name: {
				required: true
			},
			user_license: {
				required: true,
				cNumber: true
			},
			term_license: {
				required: true,
				cNumber: true
			},
			password_lock_times: {
				required: true,
				cNumber: true
			},
			thumbnail_width: {
				required: true,
				cNumber: true
			},
			thumbnail_height: {
				required: true,
				cNumber: true
			},
			patLite_ip: {
				patLiteReq: true,
				numAlphaSymbol: true
			},
			patLite_login_name: {
				patLiteReq: true,
				numAlphaSymbol: true
			},
			patLite_option_rcv: {
				cNumber: true
			},
			patLite_option_warm: {
				cNumber: true
			},
			patLite_option_err: {
				cNumber: true
			},
			live_app: {
				numAlphaSymbol: true
			},
			live_room: {
				numAlphaSymbol: true
			},
			map_server: {
				mapReq: true
			},
			start_date: {
				required: true
			},
			end_date: {
				required: true,
				compareDate: 'start_date'
			}
		},
		messages: {
			tenant_id: {
				required: required,
				numAlphaSymbol: numAlphaSymbol,
				cFileName: cFileName,
				exFileName: exFileName
			},
			tenant_name: {
				required: required
			},
			user_license: {
				required: required,
				cNumber: number
			},
			term_license: {
				required: required,
				cNumber: number
			},
			password_lock_times: {
				required: required,
				cNumber: number
			},
			thumbnail_width: {
				required: required,
				cNumber: number
			},
			thumbnail_height: {
				required: required,
				cNumber: number
			},
			patLite_ip: {
				patLiteReq: required,
				numAlphaSymbol: numAlphaSymbol
			},
			patLite_login_name: {
				patLiteReq: required,
				numAlphaSymbol: numAlphaSymbol
			},
			patLite_option_rcv: {
				cNumber: number
			},
			patLite_option_warm: {
				cNumber: number
			},
			patLite_option_err: {
				cNumber: number
			},
			live_app: {
				numAlphaSymbol: numAlphaSymbol
			},
			live_room: {
				numAlphaSymbol: numAlphaSymbol
			},
			map_server: {
				mapReq: required
			},
			start_date: {
				required: required
			},
			end_date: {
				required: required,
				compareDate: compareDate
			}
		},
		errorPlacement: function(error, element){
			if ((element.attr("name") == "start_date") || (element.attr("name") == "end_date")) {
				error.insertAfter($('#'+ element.attr('name') + '_err'));
			} else {
				error.insertAfter(element);
			}
		}
	});

	/* リストの行クリック時 */
	$('#dataTable tbody tr').click(function(event) {
		if ($(this).hasClass('row_selected')) {
			// 選択行をクリック（行の選択を解除）
			oTable.$(this).removeClass('row_selected');
			oTable.$(this).find('input[name=r_chk]').prop('checked',false);
		} else {
			// 非選択業をクリック（行を選択）
			// 一旦全ての行の選択を解除する
			oTable.$('.sel_row').removeClass('row_selected');
			oTable.$('.sel_row').find('input[name=r_chk]').prop('checked',false);
			// 行を選択
			oTable.$(this).addClass('row_selected');
			oTable.$(this).find('input[name=r_chk]').prop('checked',true);
		}
	} );

	/* dataTableの初期設定 */
	oTable = $('#dataTable').dataTable({
		"language": {						/* 日本語化 */
			"decimal": ".",
			"thousands": ",",
			"sProcessing": "処理中...",
			"sLengthMenu": "_MENU_ 件表示",
			"sZeroRecords": "データがありません。",
			"sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示中",
			"sInfoEmpty": " 0 件中 0 から 0 まで表示",
			"sInfoFiltered": "（全 _MAX_ 件より抽出）",
			"sInfoPostFix": "",
			"sSearch": "検索:",
			"sUrl": "",
			"oPaginate": {
			"sFirst": "先頭",
			"sPrevious": "<",
			"sNext": ">",
			"sLast": "最終"
			}
		},
		//"sPaginationType": "full_numbers",	/* two_button:進むと戻るのみ, full_numbers:全ボタン表示 */
		"asStripeClasses": ['even','even'],	/* odd, even にすると1行置きに色が変わる(odd, evenはcssで設定) */
		"bAutoWidth": true,					/* 列幅自動調整機能 */
		"sDom": "lfrtip",					/* 列幅変更機能有効(パラメータ不明) */
		"bFilter": true,					/* フィルター機能 */
		"bPaginate": true,					/* ページング機能 */
		"bInfo": true,						/* ページ情報(n件中aからbまで表示) */
		"bLengthChange": true,				/* 1ページの表示件数切替 */
		"aLengthMenu": [10,20,50,100],		/* 1ページの表示件数切替コンボボックスソース */
		"iDisplayLength": 10,				/* 1ページの表示件数初期値 */
		"sScrollX": "",					/* 水平スクロール機能(幅をピクセル指定、空は無効) */
		"sScrollY": "",						/* 垂直スクロール機能(高さをピクセル指定、空は無効) */
		"bScrollCollapse": false,			/* 垂直スクロール有効時に表示幅固定 */
		"bSort": true,						/* 整列機能 */
		"aaSorting": [],					/* 整列初期状態(空[]は整列しない) */
		"aoColumnDefs": [ {
			"bSortable": false, "aTargets": [ 0 ]	/* 0カラム目の整列機能を無効にする */
		} ]
	} );

	/* カレンダー表示 */
	$('#datetimepicker-start').datetimepicker({
		dayViewHeaderFormat: 'YYYY年 MMMM',
		tooltips: {
		close: '閉じる',
		selectMonth: '月を選択',
		prevMonth: '前月',
		nextMonth: '次月',
		selectYear: '年を選択',
		prevYear: '前年',
		nextYear: '次年',
		selectTime: '時間を選択',
		selectDate: '日付を選択',
		prevDecade: '前期間',
		nextDecade: '次期間',
		selectDecade: '期間を選択',
		prevCentury: '前世紀',
		nextCentury: '次世紀'
		},
		format: 'YYYY/MM/DD',
		locale: 'ja',
		ignoreReadonly: true,
		//sideBySide: true,
		showClose: true,
		widgetPositioning: {
			horizontal: 'left',
			vertical: 'auto'
		}
	});
	$('#datetimepicker-end').datetimepicker({
		dayViewHeaderFormat: 'YYYY年 MMMM',
		tooltips: {
		close: '閉じる',
		selectMonth: '月を選択',
		prevMonth: '前月',
		nextMonth: '次月',
		selectYear: '年を選択',
		prevYear: '前年',
		nextYear: '次年',
		selectTime: '時間を選択',
		selectDate: '日付を選択',
		prevDecade: '前期間',
		nextDecade: '次期間',
		selectDecade: '期間を選択',
		prevCentury: '前世紀',
		nextCentury: '次世紀'
		},
		format: 'YYYY/MM/DD',
		locale: 'ja',
		ignoreReadonly: true,
		//sideBySide: true,
		showClose: true,
		widgetPositioning: {
			horizontal: 'left',
			vertical: 'auto'
		}
	});

	/* 一覧ボタンクリック時 */
	$('#list').click(function() {
		// リストを開く
		$('#listModal').modal();
	});

	/* リストの編集ボタンクリック時 */
	$('#edit').click(function() {
		var selectedRow = getSelectedRow(oTable);
		if (selectedRow.length == 1) {
			// 選択が1行のとき

			// リストの値をフォームに転記する
			$('#tenant_id').val(selectedRow[0].cells.item(1).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#tenant_name').val(selectedRow[0].cells.item(2).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#user_license').val(selectedRow[0].cells.item(3).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#term_license').val(selectedRow[0].cells.item(4).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			if (selectedRow[0].cells.item(5).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#stillimage_function').prop('checked', true);
			}else {
				$('#stillimage_function').prop('checked', false);
			}
			if (selectedRow[0].cells.item(6).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#movie_function').prop('checked', true);
			}else {
				$('#movie_function').prop('checked', false);
			}
			if (selectedRow[0].cells.item(7).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#live_function').prop('checked', true);
				$('#disLiveEnable').collapse('show');
			}else {
				$('#live_function').prop('checked', false);
				$('#disLiveEnable').collapse('hide');
			}

			if (selectedRow[0].cells.item(8).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#start_tmp_del').prop('checked', true);
			}else {
				$('#start_tmp_del').prop('checked', false);
			}

			$('#tmp_del_time').val(selectedRow[0].cells.item(9).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#stop_time').val(selectedRow[0].cells.item(10).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#password_lock_times').val(selectedRow[0].cells.item(11).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#thumbnail_width').val(selectedRow[0].cells.item(12).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#thumbnail_height').val(selectedRow[0].cells.item(13).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			if (selectedRow[0].cells.item(14).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#patlite_enable').prop('checked', true);
				$('#disPatliteEnable').collapse('show');
			}else {
				$('#patlite_enable').prop('checked', false);
				$('#disPatliteEnable').collapse('hide');
			}

			$('#patLite_ip').val(selectedRow[0].cells.item(15).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#patLite_login_name').val(selectedRow[0].cells.item(16).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#patLite_option_rcv').val(selectedRow[0].cells.item(17).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#patLite_time_rcv').val(selectedRow[0].cells.item(18).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#patLite_option_warm').val(selectedRow[0].cells.item(19).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#patLite_time_warm').val(selectedRow[0].cells.item(20).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#patLite_option_err').val(selectedRow[0].cells.item(21).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#patLite_time_err').val(selectedRow[0].cells.item(22).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#live_server').val(selectedRow[0].cells.item(23).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#live_app').val(selectedRow[0].cells.item(24).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#live_room').val(selectedRow[0].cells.item(25).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			if (selectedRow[0].cells.item(26).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#map_enable').prop('checked', true);
				$('#disMapEnable').collapse('show');
			}else {
				$('#map_enable').prop('checked', false);
				$('#disMapEnable').collapse('hide');
			}

			$('#map_server').val(selectedRow[0].cells.item(27).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			if (selectedRow[0].cells.item(28).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#report_enable').prop('checked', true);
			}else {
				$('#report_enable').prop('checked', false);
			}

			if (selectedRow[0].cells.item(29).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#display_file_path').prop('checked', true);
			}else {
				$('#display_file_path').prop('checked', false);
			}

			$('#start_date').val(selectedRow[0].cells.item(30).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#end_date').val(selectedRow[0].cells.item(31).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			if (selectedRow[0].cells.item(32).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#del_flg').prop('checked', true);
			}else {
				$('#del_flg').prop('checked', false);
			}

			// テナントIDを編集不可にする
			$('#tenant_id').attr('readonly', true);

			// バリデートをリセットする
			validator.resetForm();

			// メッセージを閉じる
			$('#success').alert('close');
			$('#add_danger').alert('close');
			$('#user_license_danger').alert('close');
			$('#term_license_danger').alert('close');

			// ボタンを「更新」に変更
			$('#reflect').attr('name', 'update');
			$('#reflect').html("<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span>&thinsp;更新");

			 // リストを閉じる
			$('#listModal').modal('hide');
		}else {
			swalInfo.fire({html: "編集するテナントを選択してください。"});
			return false;
		}
	});

	/* クリアボタンクリック時 */
	$('#clearInputArea').click(function() {
		// メッセージを閉じる
		$('#success').alert('close');
		$('#add_danger').alert('close');
		$('#user_license_danger').alert('close');
		$('#term_license_danger').alert('close');
		 // フォームをクリアする
		clearForm(this.form);
		// バリデートをリセットする
		validator.resetForm();
		// テナントIDを編集可にする
		$('#tenant_id').removeAttr('readonly');
		// ユーザーライセンス数のデフォルト値を設定する
		$('#user_license').val(0);
		// 端末ライセンス数のデフォルト値を設定する
		$('#term_license').val(0);
		// ライブ設定を非表示にする
		$('#disLiveEnable').collapse('hide');
		// 端末-一時ファイル時間経過削除のデフォルト値を設定する
		$('#tmp_del_time').val(0);
		// 端末-アプリ自動停止時間のデフォルト値を設定する
		$('#stop_time').val(0);
		// パスワードエラーカウントのデフォルト値を設定する
		$('#password_lock_times').val(5);
		// サムネイルサイズ（横）のデフォルト値を設定する
		$('#thumbnail_width').val(400);
		// サムネイルサイズ（縦）のデフォルト値を設定する
		$('#thumbnail_height').val(300);
		// パトライト制御を非表示にする
		$('#disPatliteEnable').collapse('hide');
		// パトライト-オプションのデフォルト値を設定する
		$('#patLite_option_rcv').val("993991");
		$('#patLite_option_warm').val("939991");
		$('#patLite_option_err').val("299992");
		// パトライト-鳴動時間のデフォルト値を設定する
		$('#patLite_time_rcv').val(1);
		$('#patLite_time_warm').val(1);
		$('#patLite_time_err').val(1);
		// 地図有効化制御を非表示にする
		$('#disMapEnable').collapse('hide');
		// ボタンを「追加」に変更
		$('#reflect').attr('name', 'add');
		$('#reflect').html("<span class='glyphicon glyphicon-plus' aria-hidden='true'></span>&thinsp;追加");
	});

	// ライブ設定表示チェック
	if ($("#live_function").prop("checked") == true) {
		$('#disLiveEnable').collapse('show');
	}else {
		$('#disLiveEnable').collapse('hide');
	}

	// パトライト制御表示チェック
	if ($("#patlite_enable").prop("checked") == true) {
		$('#disPatliteEnable').collapse('show');
	}else {
		$('#disPatliteEnable').collapse('hide');
	}

	// 地図有効化制御表示チェック
	if ($("#map_enable").prop("checked") == true) {
		$('#disMapEnable').collapse('show');
	}else {
		$('#disMapEnable').collapse('hide');
	}

	if ($('#add_danger').length) {
		// 端末ID入力欄をエラーにする
		$('#tenant_id').addClass("error");
	}else {
		if ($('#tenant_id').val() != "" && $('#tenant_id').val() != null) {
			// 追加・更新時処理
			// テナントIDを編集不可にする
			$('#tenant_id').attr('readonly', true);
			// ボタンを「更新」に変更
			$('#reflect').attr('name', 'update');
			$('#reflect').html("<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span>&thinsp;更新");
		}else {
			// 新規時
			// ユーザーライセンス数のデフォルト値を設定する
			$('#user_license').val(0);
			// 端末ライセンス数のデフォルト値を設定する
			$('#term_license').val(0);
			// 端末-一時ファイル時間経過削除のデフォルト値を設定する
			$('#tmp_del_time').val(0);
			// 端末-アプリ自動停止時間のデフォルト値を設定する
			$('#stop_time').val(0);
			// パスワードエラーカウントのデフォルト値を設定する
			$('#password_lock_times').val(5);
			// サムネイルサイズ（横）のデフォルト値を設定する
			$('#thumbnail_width').val(400);
			// サムネイルサイズ（縦）のデフォルト値を設定する
			$('#thumbnail_height').val(300);
			// パトライト-オプションのデフォルト値を設定する
			$('#patLite_option_rcv').val("993991");
			$('#patLite_option_warm').val("939991");
			$('#patLite_option_err').val("299992");
			// パトライト-鳴動時間のデフォルト値を設定する
			$('#patLite_time_rcv').val(1);
			$('#patLite_time_warm').val(1);
			$('#patLite_time_err').val(1);
		}
	}

	if ($('#user_license_danger').length) {
		// ユーザーライセンス数入力欄をエラーにする
		$('#user_license').addClass("error");
	}
	if ($('#term_license_danger').length) {
		// 端末ライセンス数入力欄をエラーにする
		$('#term_license').addClass("error");
	}

});

/* 選択されている行を返却する */
function getSelectedRow(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}

/* ライブ機能有効化チェック時 */
function checkLiveEnable() {
	// ライブ設定表示チェック
	if ($("#live_function").prop("checked") == true) {
		$('#disLiveEnable').collapse('show');
	}else {
		$('#disLiveEnable').collapse('hide');
	}
}

/* パトライト有効化チェック時 */
function checkPatliteEnable() {
	// パトライト制御表示チェック
	if ($("#patlite_enable").prop("checked") == true) {
		$('#disPatliteEnable').collapse('show');
	}else {
		$('#disPatliteEnable').collapse('hide');
	}
}

/* 地図有効化チェック時 */
function checkMapEnable() {
	// 地図有効化制御表示チェック
	if ($("#map_enable").prop("checked") == true) {
		$('#disMapEnable').collapse('show');
	}else {
		$('#disMapEnable').collapse('hide');
	}
}
