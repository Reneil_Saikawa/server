var validator;
var oTable;

$(function(){
	/* ブラウザ戻るボタン抑止 */
	history.forward();

	/* バリデーション */
	validator = $('form').validate({
		rules: {
			item_id: {
				required: true,
				numAlphaSymbol: true,
				reportTag: true
			},
			item_name: {
				required: true
			},
			display_no: {
				cNumber: true
			},
			start_date: {
				required: true
			},
			end_date: {
				required: true,
				compareDate: 'start_date'
			}
		},
		messages: {
			item_id: {
				required: required,
				numAlphaSymbol: numAlphaSymbol,
				reportTag: reportTag
			},
			item_name: {
				required: required
			},
			display_no: {
				cNumber: number
			},
			start_date: {
				required: required
			},
			end_date: {
				required: required,
				compareDate: compareDate
			}
		},
		errorPlacement: function(error, element){
			if ((element.attr("name") == "start_date") || (element.attr("name") == "end_date")) {
				error.insertAfter($('#'+ element.attr('name') + '_err'));
			} else {
				error.insertAfter(element);
			}
		}
	});

	/* リストの行クリック時 */
	$('#dataTable tbody tr').click(function(event) {
		if ($(this).hasClass('row_selected')) {
			// 選択行をクリック（行の選択を解除）
			oTable.$(this).removeClass('row_selected');
			oTable.$(this).find('input[name=r_chk]').prop('checked',false);
		} else {
			// 非選択業をクリック（行を選択）
			// 一旦全ての行の選択を解除する
			oTable.$('.sel_row').removeClass('row_selected');
			oTable.$('.sel_row').find('input[name=r_chk]').prop('checked',false);
			// 行を選択
			oTable.$(this).addClass('row_selected');
			oTable.$(this).find('input[name=r_chk]').prop('checked',true);
		}
	} );

	/* dataTableの初期設定 */
	oTable = $('#dataTable').dataTable({
		"language": {						/* 日本語化 */
			"decimal": ".",
			"thousands": ",",
			"sProcessing": "処理中...",
			"sLengthMenu": "_MENU_ 件表示",
			"sZeroRecords": "データがありません。",
			"sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示中",
			"sInfoEmpty": " 0 件中 0 から 0 まで表示",
			"sInfoFiltered": "（全 _MAX_ 件より抽出）",
			"sInfoPostFix": "",
			"sSearch": "検索:",
			"sUrl": "",
			"oPaginate": {
			"sFirst": "先頭",
			"sPrevious": "<",
			"sNext": ">",
			"sLast": "最終"
			}
		},
		//"sPaginationType": "full_numbers",	/* two_button:進むと戻るのみ, full_numbers:全ボタン表示 */
		"asStripeClasses": ['even','even'],	/* odd, even にすると1行置きに色が変わる(odd, evenはcssで設定) */
		"bAutoWidth": true,					/* 列幅自動調整機能 */
		"sDom": "lfrtip",					/* 列幅変更機能有効(パラメータ不明) */
		"bFilter": true,					/* フィルター機能 */
		"bPaginate": true,					/* ページング機能 */
		"bInfo": true,						/* ページ情報(n件中aからbまで表示) */
		"bLengthChange": true,				/* 1ページの表示件数切替 */
		"aLengthMenu": [10,20,50,100],		/* 1ページの表示件数切替コンボボックスソース */
		"iDisplayLength": 10,				/* 1ページの表示件数初期値 */
		"sScrollX": "",						/* 水平スクロール機能(幅をピクセル指定、空は無効) */
		"sScrollY": "",						/* 垂直スクロール機能(高さをピクセル指定、空は無効) */
		"bScrollCollapse": false,			/* 垂直スクロール有効時に表示幅固定 */
		"bSort": true,						/* 整列機能 */
		"aaSorting": [],					/* 整列初期状態(空[]は整列しない) */
		"aoColumnDefs": [ {
			"bSortable": false, "aTargets": [ 0 ]	/* 0カラム目の整列機能を無効にする */
		} ]
	} );

	/* カレンダー表示 */
	$('#datetimepicker-start').datetimepicker({
		dayViewHeaderFormat: 'YYYY年 MMMM',
		tooltips: {
		close: '閉じる',
		selectMonth: '月を選択',
		prevMonth: '前月',
		nextMonth: '次月',
		selectYear: '年を選択',
		prevYear: '前年',
		nextYear: '次年',
		selectTime: '時間を選択',
		selectDate: '日付を選択',
		prevDecade: '前期間',
		nextDecade: '次期間',
		selectDecade: '期間を選択',
		prevCentury: '前世紀',
		nextCentury: '次世紀'
		},
		format: 'YYYY/MM/DD',
		minDate: $('#tenantStart').val(),
		maxDate: $('#tenantEnd').val() + ' 23:59:59',
		locale: 'ja',
		ignoreReadonly: true,
		//sideBySide: true,
		showClose: true,
		widgetPositioning: {
			horizontal: 'left',
			vertical: 'auto'
		}
	});
	$('#datetimepicker-end').datetimepicker({
		dayViewHeaderFormat: 'YYYY年 MMMM',
		tooltips: {
		close: '閉じる',
		selectMonth: '月を選択',
		prevMonth: '前月',
		nextMonth: '次月',
		selectYear: '年を選択',
		prevYear: '前年',
		nextYear: '次年',
		selectTime: '時間を選択',
		selectDate: '日付を選択',
		prevDecade: '前期間',
		nextDecade: '次期間',
		selectDecade: '期間を選択',
		prevCentury: '前世紀',
		nextCentury: '次世紀'
		},
		format: 'YYYY/MM/DD',
		minDate: $('#tenantStart').val(),
		maxDate: $('#tenantEnd').val() + ' 23:59:59',
		locale: 'ja',
		ignoreReadonly: true,
		//sideBySide: true,
		showClose: true,
		widgetPositioning: {
			horizontal: 'left',
			vertical: 'auto'
		}
	});

	/* 一覧ボタンクリック時 */
	$('#list').click(function() {
		// リストを開く
		$('#listModal').modal();
	});

	/* 付帯情報項目名リストの編集ボタンクリック時 */
	$('#edit').click(function() {
		var selectedRow = getSelectedRow(oTable);
		if (selectedRow.length == 1) {
			// 選択が1行のとき

			// リストの値をフォームに転記する
			$('#item_id').val(selectedRow[0].cells.item(1).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#item_name').val(selectedRow[0].cells.item(2).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#display_no').val(selectedRow[0].cells.item(3).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#start_date').val(selectedRow[0].cells.item(4).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#end_date').val(selectedRow[0].cells.item(5).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			if (selectedRow[0].cells.item(6).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#del_flg').prop('checked', true);
			}else {
				$('#del_flg').prop('checked', false);
			}

			// 項目IDを編集不可にする
			$('#item_id').attr('readonly', true);

			// バリデートをリセットする
			validator.resetForm();

			// メッセージを閉じる
			$('#success').alert('close');
			$('#add_danger').alert('close');
			$('#up_danger').alert('close');

			// ボタンを「更新」に変更
			$('#reflect').attr('name', 'update');
			$('#reflect').html("<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span>&thinsp;更新");

			 // リストを閉じる
			$('#listModal').modal('hide');

		}else {
			swalInfo.fire({html: "編集する付帯情報項目名を選択してください。"});
			return false;
		}
	});

	/* クリアボタンクリック時 */
	$('#clearInputArea').click(function() {
		// メッセージを閉じる
		$('#success').alert('close');
		$('#add_danger').alert('close');
		$('#up_danger').alert('close');
		 // フォームをクリアする
		clearForm(this.form);
		// バリデートをリセットする
		validator.resetForm();
		// 項目IDを編集可にする
		$('#item_id').attr('readonly', false);
		// ボタンを「追加」に変更
		$('#reflect').attr('name', 'add');
		$('#reflect').html("<span class='glyphicon glyphicon-plus' aria-hidden='true'></span>&thinsp;追加");
	});

	// 追加・更新時処理
	if ($('#item_id').val() != "" && $('#item_id').val() != null && !($('#add_danger').length)) {
		// 項目IDを編集不可にする
		$('#item_id').attr('readonly', true);
		// ボタンを「更新」に変更
		$('#reflect').attr('name', 'update');
		$('#reflect').html("<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span>&thinsp;更新");
	}

	if ($('#add_danger').length) {
		// 項目ID入力欄をエラーにする
		$('#item_id').addClass("error");
	}
});

/* 選択されている行を返却する */
function getSelectedRow(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}

