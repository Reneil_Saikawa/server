$(function() {
	/* システム情報クリック時 */
	$("#systeminfo").click(function() {
		window.location.href = 'systeminfo.html';
	});

	/* テナントクリック時 */
	$("#tenant").click(function() {
		window.location.href = 'tenant.html';
	});

	/* テナント管理者クリック時 */
	$("#tenant_manager").click(function() {
		window.location.href = 'tenant_manager.html';
	});

	/* アクセスログクリック時 */
	$("#accessedlog").click(function() {
		window.location.href = 'accessedlog.html';
	});

	/* 受信ログクリック時 */
	$("#receivedlog").click(function() {
		window.location.href = 'receivedlog.html';
	});

	/* ログアウトクリック時 */
	$("#logout").click(function() {
		$.ajax({
			type        : "POST",
			url         : "../sysLogOutWriting.html",
			data        : {acc_userID: $("#acc_userID").val()},
			dataType    : "JSON"
		})
		// 通信成功
		.done(function(result) {

		})
		// 通信失敗
		.fail(function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("ajax通信に失敗しました");
			console.log("XMLHttpRequest : " + XMLHttpRequest.status);
			console.log("textStatus     : " + textStatus);
			console.log("errorThrown    : " + errorThrown.message);
		});
		// ログインページへ遷移
		// ログイン画面表示を50ミリ秒遅らせる(ログアウトのログと前後してしまうことがあるため)
		setTimeout(function(){window.location.href='login.html'}, 50);
	});
});

/* フォームをクリアする */
function clearForm (form) {
    $(form)
        .find("input, select, textarea")
        .not(":button, :submit, :reset, :hidden")
        .val("")
        .prop("checked", false)
        .prop("selected", false)
    ;
    $(form).find(":radio").filter("[data-default]").prop("checked", true);
    $(form).find(":checkbox").val(true);
}