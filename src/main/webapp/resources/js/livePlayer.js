var server = null;
var janus = null;
var sfutest = null;
var opaqueId = "videoroom-"+Janus.randomString(12);
var myroom = 0;
var myname = null;
var myid = null;
var mystream = null;
// We use this other ID just to map our subscriptions to us
var mypvtid = null;

var feeds = [];
var bitrateTimer = [];

// Simulcastは使用しない. Saikawa on 2021-05-12
var doSimulcast = false;
var doSimulcast2 = false;

$(document).ready(function() {
	// URLパラメタからサーバーアドレスを取得する
	var host = getParam('janusHost');
	if (host == null || host == '') {
		alert("異常終了：Janusサーバーのアドレスが取得できませんでした。");
		window.open('about:blank','_self').close();
		return false;
	}
    server = host + ":8089/janus";

	// URLパラメタからルーム名を取得する
	myroom = Number(getParam('room'));
	if (myroom == null || myroom == '') {
		alert("異常終了：ルーム名が取得できませんでした。");
		window.open('about:blank','_self').close();
		return false;
	}

	myname = $("#userName").val();

	// 初期化（すべてのコンソールデバッガーが有効）
	Janus.init({debug: "all", callback: function() {
		$(this).attr('disabled', true).unbind('click');
		// ブラウザがWebRTCをサポートしていることを確認
		if(!Janus.isWebrtcSupported()) {
			bootbox.alert("No WebRTC support... ");
			return;
		}
		// セッションを作成
		janus = new Janus(
			{
				server: server,
				// セッションの作成に成功したとき
				success: function() {
					// プラグインにアタッチ
					janus.attach(
						{
							plugin: "janus.plugin.videoroom",
							opaqueId: opaqueId,
							// プラグインのアタッチに成功したとき
							success: function(pluginHandle) {
								sfutest = pluginHandle;
								Janus.log("Plugin attached! (" + sfutest.getPlugin() + ", id=" + sfutest.getId() + ")");
								Janus.log("  -- This is a publisher/manager");
								registerUsername();
							},
							// プラグインのアタッチに失敗したとき
							error: function(error) {
//								Janus.error("  -- Error attaching plugin...", error);
								bootbox.alert("Error attaching plugin... " + error);
							},
							// getUserMediaが呼び出される直前
							consentDialog: function(on) {
								Janus.debug("Consent dialog should be " + (on ? "on" : "off") + " now");
							},
							// PeerConnectionのICE状態が変化したとき
							iceState: function(state) {
								Janus.log("ICE state changed to " + state);
							},
							// メディアの受信を開始または停止したとき
							mediaState: function(medium, on) {
								Janus.log("Janus " + (on ? "started" : "stopped") + " receiving our " + medium);
							},
							// PeerConnectionがアクティブになったとき
							webrtcState: function(on) {
								Janus.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
							},
							// メッセージイベントを受け取ったとき
							onmessage: function(msg, jsep) {
								Janus.debug(" ::: Got a message (publisher) :::", msg);
								var event = msg["videoroom"];
								Janus.debug("Event: " + event);
								if(event) {
									if(event === "joined") {
										// Publisher/manager created, negotiate WebRTC and attach to existing feeds, if any
										myid = msg["id"];
										mypvtid = msg["private_id"];
										Janus.log("Successfully joined room " + msg["room"] + " with ID " + myid);

										// 自分自身はモーダルウィンドウで発話が開始されるまで参加しない
//										publishOwnFeed(true);

										// Any new feed to attach to?
										if(msg["publishers"]) {
											var list = msg["publishers"];
											Janus.debug("Got a list of available publishers/feeds:", list);
											for(var f in list) {
												var id = list[f]["id"];
												var display = list[f]["display"]; // dispray: 端末名、端末ID、テナントID
												var audio = list[f]["audio_codec"];
												var video = list[f]["video_codec"];
												Janus.debug("  >> [" + id + "] " + display + " (audio: " + audio + ", video: " + video + ")");
												newRemoteFeed(id, display, audio, video);
											}
										}
									} else if(event === "destroyed") {
										// The room has been destroyed
										Janus.warn("The room has been destroyed!");
										bootbox.alert("The room has been destroyed", function() {
											window.location.reload();
										});
									} else if(event === "event") {
										// Any new feed to attach to?
										if(msg["publishers"]) {
											var list = msg["publishers"];
											Janus.debug("Got a list of available publishers/feeds:", list);
											for(var f in list) {
												var id = list[f]["id"];
												var display = list[f]["display"]; // dispray: 端末名、端末ID、テナントID
												var audio = list[f]["audio_codec"];
												var video = list[f]["video_codec"];
												Janus.debug("  >> [" + id + "] " + display + " (audio: " + audio + ", video: " + video + ")");
												newRemoteFeed(id, display, audio, video);
											}
										} else if(msg["leaving"]) {
											// One of the publishers has gone away?
											var leaving = msg["leaving"];
											Janus.log("Publisher left: " + leaving);
											var remoteFeed = null;
											for(var i=1; i<6; i++) {
												if(feeds[i] && feeds[i].rfid == leaving) {
													remoteFeed = feeds[i];
													break;
												}
											}
											if(remoteFeed != null) {
												Janus.debug("Feed " + remoteFeed.rfid + " (" + remoteFeed.rfdisplay + ") has left the room, detaching");
												$('#remote'+remoteFeed.rfindex).empty();
												feeds[remoteFeed.rfindex] = null;
												remoteFeed.detach();
											}
										} else if(msg["unpublished"]) {
											// One of the publishers has unpublished?
											var unpublished = msg["unpublished"];
											Janus.log("Publisher left: " + unpublished);
											if(unpublished === 'ok') {
												// That's us
												sfutest.hangup();
												return;
											}
											var remoteFeed = null;
											for(var i=1; i<6; i++) {
												if(feeds[i] && feeds[i].rfid == unpublished) {
													remoteFeed = feeds[i];
													break;
												}
											}
											if(remoteFeed != null) {
												Janus.debug("Feed " + remoteFeed.rfid + " (" + remoteFeed.rfdisplay + ") has left the room, detaching");
												$('#remote'+remoteFeed.rfindex).empty();
												feeds[remoteFeed.rfindex] = null;
												remoteFeed.detach();
											}
										} else if(msg["error"]) {
											if(msg["error_code"] === 426) {
												// This is a "no such room" error: give a more meaningful description
												bootbox.alert(
													"<p>Apparently room <code>" + myroom + "</code> (the one this demo uses as a test room) " +
													"does not exist...</p><p>Do you have an updated <code>janus.plugin.videoroom.jcfg</code> " +
													"configuration file? If not, make sure you copy the details of room <code>" + myroom + "</code> " +
													"from that sample in your current configuration file, then restart Janus and try again."
												);
											} else {
												bootbox.alert(msg["error"]);
											}
										}
									}
								}
								if(jsep) {
									Janus.debug("Handling SDP as well...", jsep);
									sfutest.handleRemoteJsep({ jsep: jsep });
									// Check if any of the media we wanted to publish has
									// been rejected (e.g., wrong or unsupported codec)
									var audio = msg["audio_codec"];
									if(mystream && mystream.getAudioTracks() && mystream.getAudioTracks().length > 0 && !audio) {
										// Audio has been rejected
										toastr.warning("Our audio stream has been rejected, viewers won't hear us");
									}
								}
							},
							// ローカルMediaStreamの準備ができたとき
							onlocalstream: function(stream) {
								Janus.debug(" ::: Got a local stream :::", stream);
								mystream = stream;
								Janus.attachMediaStream(stream);
							},
							// リモートMediaStreamの準備ができたとき
							onremotestream: function(stream) {
								//
							},
							// PeerConnectionが閉じられたとき
							oncleanup: function() {
								Janus.log(" ::: Got a cleanup notification: we are unpublished now :::");
								mystream = null;
							}
						});
				},
				// セッションの作成に失敗したとき
				error: function(error) {
//					Janus.error(error);
					bootbox.alert(error, function() {
						window.location.reload();
					});
				},
				// セッションが破棄され使用できなくなったとき
				destroyed: function() {
					window.location.reload();
				}
			});
	}});

	// モーダルウィンドウの発話ボタンクリック
	$('#tarkStart', parent.document).on('click', () => {
        // 自身の送信を開始
		publishOwnFeed(true);

		// ボタン制御
		$('#tarkStart', parent.document).hide();
		$('#tarkStop', parent.document).show();

		// ミュート解除
		var video = $('#viewArea', parent.document).get(0)
        video.muted = false;
    });

    // モーダルウィンドウの終話ボタンクリック
	$('#tarkStop', parent.document).on('click', () => {
		// ボタン制御
		$('#tarkStart', parent.document).show();
		$('#tarkStop', parent.document).hide();

		// ミュートにしてビデオソースをクリア
        var video = $('#viewArea', parent.document).get(0)
        video.muted = true;

        // 自身の送信を停止
//        unpublishOwnFeed()
        mystream = null;
    });

});

function registerUsername() {
	var register = {
			request: "join",
			room: myroom,
			ptype: "publisher",
			display: myname
		};
	sfutest.send({ message: register });
}

function publishOwnFeed(useAudio) {
	// Publish our stream
	sfutest.createOffer(
		{
			// Add data:true here if you want to publish datachannels as well
			media: { audioRecv: false, videoRecv: false, audioSend: useAudio, videoSend: false },	// Publishers are sendonly
			// If you want to test simulcasting (Chrome and Firefox only), then
			// pass a ?simulcast=true when opening this demo page: it will turn
			// the following 'simulcast' property to pass to janus.js to true
			simulcast: doSimulcast,
			simulcast2: doSimulcast2,
			success: function(jsep) {
				Janus.debug("Got publisher SDP!", jsep);
				var publish = { request: "configure", audio: useAudio, video: false };
				// You can force a specific codec to use when publishing by using the
				// audiocodec and videocodec properties, for instance:
				 		publish["audiocodec"] = "opus";
				// to force Opus as the audio codec to use, or:
				// 		publish["videocodec"] = "vp9"
				// to force VP9 as the videocodec to use. In both case, though, forcing
				// a codec will only work if: (1) the codec is actually in the SDP (and
				// so the browser supports it), and (2) the codec is in the list of
				// allowed codecs in a room. With respect to the point (2) above,
				// refer to the text in janus.plugin.videoroom.jcfg for more details
				sfutest.send({ message: publish, jsep: jsep });
			},
			error: function(error) {
				Janus.error("WebRTC error:", error);
				if(useAudio) {
					 publishOwnFeed(false);
				} else {
					bootbox.alert("WebRTC error... " + error.message);
				}
			}
		});
}

function unpublishOwnFeed() {
	// Unpublish our stream
	var unpublish = { request: "unpublish" };
	sfutest.send({ message: unpublish });
}

function newRemoteFeed(id, display, audio, video) {
	// A new feed has been published, create a new plugin handle and attach to it as a subscriber
	var remoteFeed = null;

	// 送信者の情報
	var terminfo = [];

	// プラグインにアタッチ
	janus.attach(
		{
			plugin: "janus.plugin.videoroom",
			opaqueId: opaqueId,
			// プラグインのアタッチに成功したとき
			success: function(pluginHandle) {
				remoteFeed = pluginHandle;
				remoteFeed.simulcastStarted = false;
				Janus.log("Plugin attached! (" + remoteFeed.getPlugin() + ", id=" + remoteFeed.getId() + ")");
				Janus.log("  -- This is a subscriber");
				// We wait for the plugin to send us an offer
				var subscribe = {
						request: "join",
						room: myroom,
						ptype: "subscriber",
						feed: id,
						private_id: mypvtid
					};
				// In case you don't want to receive audio, video or data, even if the
				// publisher is sending them, set the 'offer_audio', 'offer_video' or
				// 'offer_data' properties to false (they're true by default), e.g.:
				// 		subscribe["offer_video"] = false;
				// For example, if the publisher is VP8 and this is Safari, let's avoid video
				if(Janus.webRTCAdapter.browserDetails.browser === "safari" &&
						(video === "vp9" || (video === "vp8" && !Janus.safariVp8))) {
					if(video)
						video = video.toUpperCase()
					toastr.warning("Publisher is using " + video + ", but Safari doesn't support it: disabling video");
					subscribe["offer_video"] = false;
				}
				remoteFeed.videoCodec = video;
				remoteFeed.send({ message: subscribe });
			},
			// プラグインのアタッチに失敗したとき
			error: function(error) {
				Janus.error("  -- Error attaching plugin...", error);
				bootbox.alert("Error attaching plugin... " + error);
			},
			// メッセージイベントを受け取ったとき
			onmessage: function(msg, jsep) {
				Janus.debug(" ::: Got a message (subscriber) :::", msg);
				var event = msg["videoroom"];
				Janus.debug("Event: " + event);
				if(msg["error"]) {
					bootbox.alert(msg["error"]);
				} else if(event) {
					if(event === "attached") {
						// Subscriber created and attached
						for(var i=1;i<6;i++) {
							if(!feeds[i]) {
								feeds[i] = remoteFeed;
								remoteFeed.rfindex = i;
								break;
							}
						}
						remoteFeed.rfid = msg["id"];
						remoteFeed.rfdisplay = msg["display"];
						Janus.log("Successfully attached to feed " + remoteFeed.rfid + " (" + remoteFeed.rfdisplay + ") in room " + msg["room"]);
					} else {
						// What has just happened?
					}
				}
				if(jsep) {
					Janus.debug("Handling SDP as well...", jsep);
					// Answer and attach
					remoteFeed.createAnswer(
						{
							jsep: jsep,
							// Add data:true here if you want to subscribe to datachannels as well
							// (obviously only works if the publisher offered them in the first place)
							media: { audioSend: false, videoSend: false },	// We want recvonly audio/video
							success: function(jsep) {
								Janus.debug("Got SDP!", jsep);
								var body = {
										request: "start",
										room: myroom,
									};
								remoteFeed.send({ message: body, jsep: jsep });
							},
							error: function(error) {
								Janus.error("WebRTC error:", error);
								bootbox.alert("WebRTC error... " + error.message);
							}
						});
				}
			},
			// PeerConnectionのICE状態が変化したとき
			iceState: function(state) {
				Janus.log("ICE state of this WebRTC PeerConnection (feed #" + remoteFeed.rfindex + ") changed to " + state);
			},
			// PeerConnectionがアクティブになったとき
			webrtcState: function(on) {
				Janus.log("Janus says this WebRTC PeerConnection (feed #" + remoteFeed.rfindex + ") is " + (on ? "up" : "down") + " now");
			},
			// ローカルMediaStreamの準備ができたとき
			onlocalstream: function(stream) {
				//
			},
			// リモートMediaStreamの準備ができたとき
			onremotestream: function(stream) {
				// リモートビデオを追加する
				Janus.debug("Remote feed #" + remoteFeed.rfindex + ", stream:", stream);

				// remoteFeed.rfdisplayから送信者の情報を取り出す
				// テナントID, 端末ID, 端末名, 緯度, 軽度
			    terminfo = remoteFeed.rfdisplay.split(',');

//				var addButtons = false;
				if($('#remotevideo'+remoteFeed.rfindex).length === 0) {
//					addButtons = true;
					$('#their-videos').append($('<div id="remote' + remoteFeed.rfindex + '"></div>'));
					// サイズ
//					$('#remote'+ remoteFeed.rfindex).append($('<span class="label label-primary hide" id="curres'+remoteFeed.rfindex+'" style="position: relative; bottom: 0px; left: 0px;"></span>'));
					// ビットレート
//					$('#remote'+ remoteFeed.rfindex).append($('<span class="label label-info hide" id="curbitrate'+remoteFeed.rfindex+'" style="position: relative; bottom: 0px; right: 0px;"></span>'));
					// テナントID
					$('#remote'+ remoteFeed.rfindex).append($('<span style="display: none" id="'+remoteFeed.rfindex+'_TENANT">'+terminfo[0]+'</span>'));
					// 端末ID
					$('#remote'+ remoteFeed.rfindex).append($('<span style="display: none" id="'+remoteFeed.rfindex+'_TERM">'+terminfo[1]+'</span>'));
					// 端末名
					$('#remote'+ remoteFeed.rfindex).append($('<span class="label label-primary" id="'+remoteFeed.rfindex+'_TERMNAME">'+terminfo[2]+'</span>'));
					// 緯度
					$('#remote'+ remoteFeed.rfindex).append($('<span style="display: none" id="'+remoteFeed.rfindex+'_LAT">'+terminfo[3]+'</span>'));
//					$('#remote'+ remoteFeed.rfindex).append($('<p style="" id="'+remoteFeed.rfindex+'_LAT2">'+terminfo[3]+'</p>'));
					// 軽度
					$('#remote'+ remoteFeed.rfindex).append($('<span style="display: none" id="'+remoteFeed.rfindex+'_LON">'+terminfo[4]+'</span>'));
//					$('#remote'+ remoteFeed.rfindex).append($('<p style="" id="'+remoteFeed.rfindex+'_LON2">'+terminfo[4]+'</p>'));
					// ストリーム
					$('#remote'+ remoteFeed.rfindex).append($('<video class="centered relative hide" id="remotevideo' + remoteFeed.rfindex + '" width="100%" height="100%" muted autoplay playsinline onclick="showPopup(\'' + remoteFeed.rfindex + '\')"></video>'));
				}
				Janus.attachMediaStream($('#remotevideo'+remoteFeed.rfindex).get(0), stream);
				var videoTracks = stream.getVideoTracks();
				if(!videoTracks || videoTracks.length === 0) {
					// No remote video
					$('#remotevideo'+remoteFeed.rfindex).hide();
				} else {
					$('#remotevideo'+remoteFeed.rfindex).removeClass('hide').show();
					// 親フレームの高さを変更
					var target = parent.document.getElementById(myroom);
					target.style.height = "100%";
				}
//				if(!addButtons)
//					return;
				// サイズ、ビットレート表示
//				if(Janus.webRTCAdapter.browserDetails.browser === "chrome" || Janus.webRTCAdapter.browserDetails.browser === "firefox" ||
//						Janus.webRTCAdapter.browserDetails.browser === "safari") {
//					bitrateTimer[remoteFeed.rfindex] = setInterval(function() {
//						// Display updated bitrate, if supported
//						var bitrate = remoteFeed.getBitrate();
//						if (bitrate != "0 kbits/sec")
//							$('#curbitrate'+remoteFeed.rfindex).removeClass('hide').text(bitrate).show();
//						// Check if the resolution changed too
//						var width = $("#remotevideo"+remoteFeed.rfindex).get(0).videoWidth;
//						var height = $("#remotevideo"+remoteFeed.rfindex).get(0).videoHeight;
//						if(width > 0 && height > 0)
//							$('#curres'+remoteFeed.rfindex).removeClass('hide').text(width+'x'+height).show();
//					}, 1000);
//				}
			},
			// PeerConnectionが閉じられたとき
			oncleanup: function() {
				Janus.log(" ::: Got a cleanup notification (remote feed " + id + ") :::");
				$('#remotevideo'+remoteFeed.rfindex).remove();
				$('#curbitrate'+remoteFeed.rfindex).remove();
				$('#curres'+remoteFeed.rfindex).remove();
				if(bitrateTimer[remoteFeed.rfindex] !== null && bitrateTimer[remoteFeed.rfindex] !== null)
					clearInterval(bitrateTimer[remoteFeed.rfindex]);
				bitrateTimer[remoteFeed.rfindex] = null;
				remoteFeed.simulcastStarted = false;
				$('#remote'+remoteFeed.rfindex).remove();
				// 親フレームの高さを変更
				var target = parent.document.getElementById(myroom);
				target.style.height = "0";
			}
		});
}

// Helper to parse query string
function getQueryStringValue(name) {
	name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
		results = regex.exec(location.search);
	return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

/*
 * URLパラメータ取得
 *
 * @param _name {string} パラメータのキー文字列
 * @return _url {url} 対象のURL文字列（任意）
 */
function getParam(_name, _url) {
	if (!_url) _url = window.location.href;
	_name = _name.replace(/[\[\]]/g, "\\$&");
	const regex = new RegExp("[?&]" + _name + "(=([^&#]*)|&|#|$)"),
		results = regex.exec(_url);
	if (!results) return null;
	if (!results[2]) return '';
	return decodeURIComponent(results[2].replace(/\+/g, " "));
}

/*
 * 選択したストリームをモーダル表示
 *
 * @param _rfindex
 */
function showPopup(_rfindex) {
    // モーダルのビデオエリアに選択したビデオのソースをセットして再生
    const el1 = $('#remotevideo' + _rfindex).get(0);
    const el2 = $('#viewArea', parent.document).get(0);
    el2.srcObject = el1.srcObject;
    el2.muted = false;
    el2.play();

    // 送信者をセット
    const el3 = $('#' + _rfindex + '_TENANT').get(0);
    const el4 = $('#' + _rfindex + '_TERM').get(0);
    const el5 = $('#' + _rfindex + '_TERMNAME').get(0);
    const el6 = $('#' + _rfindex + '_LAT').get(0);
    const el7 = $('#' + _rfindex + '_LON').get(0);

    if (el3 == null) {
        $('#tenant_id', parent.document).text('');
    } else {
        $('#tenant_id', parent.document).text(el3.innerText);
    }
    if (el4 == null) {
        $('#term_id', parent.document).text('');
    } else {
        $('#term_id', parent.document).text(el4.innerText);
    }
    if (el5 == null) {
        $('#term_name', parent.document).text('端末名が取得できませんでした');
    } else {
        $('#term_name', parent.document).text(el5.innerText);
    }

    $('#locate_lat', parent.document).attr('name', _rfindex + '_LAT');
    $('#locate_lon', parent.document).attr('name', _rfindex + '_LON');
    $('#locate_lat', parent.document).text(el6.innerText);
    $('#locate_lon', parent.document).text(el7.innerText);

    // 発話／終話ボタン制御
    $('#tarkStart', parent.document).show();
    $('#tarkStop', parent.document).hide();

    // テナントIDと端末IDが取得できたら録画ボタンとキャプチャボタンを活性化
    if ((el3 != null) && (el3.innerText != null) && (el4 != null) && (el4.innerText != null)) {
        $('#recStart', parent.document).show();
        $('#capture', parent.document).show();
    } else {
        $('#recStart', parent.document).hide();
        $('#capture', parent.document).hide();
    }

    // モーダルを開く
    $('#liveViewModal', parent.document).modal();
}
