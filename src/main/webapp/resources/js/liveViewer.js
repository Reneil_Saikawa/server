// 動画録画用
const options = {mimeType: 'video/webm;codecs=vp8,opus'};
let mediaRecorder;
let recordedBlobs;
let localStream;
let peer;
let calls  = [];
let selectPeerId;
let boolMute = false;

$(function() {

    // 表示ボタン制御
    $('#recStop').hide();

    // メディアアクセス許可(https:かlocalhostの場合のみ)
    if(location.protocol == 'https:' || location.hostname == 'localhost') {
        navigator.mediaDevices.getUserMedia({video: false, audio: true})
	        .then(function (stream) {
	            // Success
	            localStream = stream;
	        })
	        .catch(function (error) {
	            // Error
	            return;
	        });
    }

    // Peerオブジェクトの作成
    peer = new Peer({
        key:   LIVE_APP,
        debug: 3,
    });

    // LIVE_ROOMを配列に変換
    var rooms = LIVE_ROOM.split(',');

    // room重複を削除
    rooms = rooms.filter(function (x, i, self) {
            return self.indexOf(x) === i;
        });

    // openイベント
    peer.on('open', open => {
        for (var i = 0; i < rooms.length; i++) {
            // Roomオブジェクトの作成
            const room = peer.joinRoom(rooms[i], {mode: 'mesh'});
            setupRoomEventHandlers(room);
        }
    });

    // errorイベント
    peer.on('error', error => {
        alert(error.message);
    });

    // roomオブジェクト・イベントリスナー
    function setupRoomEventHandlers(_room){
        // ストリーム受信イベント
        _room.on('stream', stream => {
            const peerId = stream.peerId;
            if (document.getElementById(peerId) == null) {
                $('#their-videos').append($(
                    '<div id="' + peerId + '">' +
                    '<video class="remoteVideos" style="cursor:pointer" oncontextmenu="return false;" muted autoplay playsinline onclick="live(\'' + peerId + '\')"></video>' +
                    '</div>'));
            } else {
                $('#' + peerId).append('<video class="remoteVideos" style="cursor:pointer" oncontextmenu="return false;" muted autoplay playsinline onclick="live(\'' + peerId + '\')"></video>');
            }
            const el = $('#' + peerId).find('video').get(0);
            el.srcObject = stream;
            el.play();
        });

        // データ受信イベント
        _room.on('data', ({src, data}) => {
            const DATA = data.split('&');
            const LOCATE_LAT = DATA[3];
            const LOCATE_LON = DATA[4];
            if (document.getElementById(src + '_ID') == null) {
                const TENANT_ID = DATA[0];
                const TERM_ID   = DATA[1];
                const TERM_NAME = DATA[2];
                if (document.getElementById(src) == null) {
                    $('#their-videos').append($(
                        '<div id="' + src + '">' +
                        '<p style="display: none" id="' + src + '_TENANT">' + TENANT_ID + '</p>' +
                        '<p style="display: none" id="' + src + '_ID">' + TERM_ID + '</p>' +
                        '<p style="display: none" id="' + src + '_LAT">' + LOCATE_LAT + '</p>' +
                        '<p style="display: none" id="' + src + '_LON">' + LOCATE_LON + '</p>' +
                        '<p id="' + src + '_NAME"><span class="glyphicon glyphicon-facetime-video red" aria-hidden="true">&thinsp;</span>' + TERM_NAME +
                        '<a href="JavaScript:void(0)" id="' + src + '_volume" title="ミュートの切り替え" onclick="mute(\'' + src + '\')"><span style="float: right; font-size: 18px" class="glyphicon glyphicon-volume-off" aria-hidden="true"></span></a></p>' +
                        '</div>'));
                }else {
                    $('#' + src).prepend('<p style="display: none" id="' + src + '_TENANT">' + TENANT_ID + '</p>');
                    $('#' + src).prepend('<p style="display: none" id="' + src + '_ID">' + TERM_ID + '</p>');
                    $('#' + src).prepend('<p style="display: none" id="' + src + '_LAT">' + LOCATE_LAT + '</p>');
                    $('#' + src).prepend('<p style="display: none" id="' + src + '_LON">' + LOCATE_LON + '</p>');
                    $('#' + src).prepend('<p id="' + src + '_NAME"><span class="glyphicon glyphicon-facetime-video red" aria-hidden="true">&thinsp;</span>' + TERM_NAME +
                    					 '<a href="JavaScript:void(0)" id="' + src + '_volume" title="ミュートの切り替え" onclick="mute(\'' + src + '\')"><span style="float: right; font-size: 18px" class="glyphicon glyphicon-volume-off" aria-hidden="true"></span></a></p>');
                }
            }else {
                // 位置情報更新
                $('#' + src + '_LAT').text(LOCATE_LAT);
                $('#' + src + '_LON').text(LOCATE_LON);
                if (document.getElementsByName(src + '_LAT') != null) {
                    $('p[name=' + src + '_LAT]').text(LOCATE_LAT);
                }
                if (document.getElementsByName(src + '_LON') != null) {
                    $('p[name=' + src + '_LON]').text(LOCATE_LON);
                }
            }
        });

        // ストリーム切断イベント
        _room.on('removeStream', stream => {
            const peerId = stream.peerId;
            $('#' + peerId).remove();
            peerClose(peerId);
        });

        // Peer接続イベント
        _room.on('peerJoin', peerId => {
        });

        // Peer切断イベント
        _room.on('peerLeave', peerId => {
            $('#' + peerId).remove();
            peerClose(peerId);
        });

        // MediaConnectionを閉じる・イベントリスナー
        function peerClose(_peerId) {
        	// peerIdとMediaConnection比較
        	for (var i=0; i<calls.length; i++) {
        		if(_peerId == calls[i].remoteId) {
        			// MediaConnectionを閉じる
        			calls[i].close(true);
        			// MediaConnection削除
        			calls.splice(i, 1);
        		}
        	}
        }
    }

    $('#recStart').on('click', () => {
        try {
            // 録画領域初期化
            recordedBlobs = [];
            // 録画開始(100ミリ秒ごとにondataavailableイベントハンドラを発生させる)
            mediaRecorder.start(100);
            // ボタン表示制御
            $('#recStart').hide();
            $('#recStop').show();
        }catch(e) {
            // 録画開始失敗
            swalAlert.fire({html: "録画を開始できませんでした。対象の中継は切断されています。<br>一度中継を閉じてから再度対象端末の中継を選択してください。"});
        }

    });

    $('#recStop').on('click', () => {
        // 録画停止処理
        recStop();
    });

    $('#capture').on('click', () => {

        // 動画を取得しcanvasにセット
        const video = $('#viewArea').get(0);
        const $canvas = $('#canvas');
        $canvas.attr('width', video.videoWidth);
        $canvas.attr('height', video.videoHeight);
        $canvas[0].getContext('2d').drawImage(video, 0, 0, $canvas.width(), $canvas.height());

        // canvasのバイナリ化
        const dataUrl = $canvas[0].toDataURL('image/jpeg');
        const blob = dataURItoBlob(dataUrl);

        // キャプチャー日時を作成
        const dt = new Date();
        const Yer = dt.getFullYear();
        const Mon = dt.getMonth()+1;
        const Dat = dt.getDate();
        const Hor = dt.getHours();
        const Min = dt.getMinutes();
        const Sec = dt.getSeconds();
        const Mil = dt.getMilliseconds();
        const dateTime = Yer + ('00'+Mon).slice(-2) + ('00'+Dat).slice(-2) + ('00'+Hor).slice(-2) + ('00'+Min).slice(-2) + ('00'+Sec).slice(-2) + ('000'+Mil).slice(-3);

        // テナントIDと端末IDと位置情報を取得する
        const TENANT_ID = $('#tenant_id').get(0).innerText;
        const TERM_ID = $('#term_id').get(0).innerText;
        const LOCATE_LAT = $('#locate_lat').get(0).innerText;
        const LOCATE_LON = $('#locate_lon').get(0).innerText;

        // アップロードデータのFormData化
        let formData = new FormData();
        formData.append("TENANT_ID", TENANT_ID);
        formData.append("TERM_ID", TERM_ID);
        formData.append("FILE_FORMAT", "0403");
        formData.append("PHOTO_DATE", dateTime);
        formData.append("LOCATE_LAT", LOCATE_LAT);
        formData.append("LOCATE_LON", LOCATE_LON);
        formData.append("LOCATE", "");
        formData.append("contentsData", blob);

        // --------------------------
        // サーバーにアップロードする
        // --------------------------
        try {
            $.ajax({
                type        : "POST",
                url         : "sendContents.html",
                processData : false,
                contentType : false, // 送信するデータをFormDataにする場合、こうしないといけない。
                cache       : false,
                data        : formData
            })
            // 通信成功
            .done(function(result) {
                if (null != result) {

                    if (result.indexOf('<!DOCTYPE html>') !== -1 || result.indexOf('<!doctype html>') !== -1) {
                        // 例外エラー表示
                        document.write(result);
                        document.close();
                    }else if (result == "Has completed") {
                        toastSuccess.fire({title: "キャプチャーを保存しました。"});
                    }else if (result == "Library not found" || result == "Terminal not found") {
                        swalAlert.fire({html: "端末情報にエラーが発生しています。<br>システム管理者にお問い合わせください。"});
                    }else {
                        swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
                    }

                }else {
                    // エラー発生
                    swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
                }
            })
            // 通信失敗
            .fail(function(XMLHttpRequest, textStatus, errorThrown) {
                if (XMLHttpRequest.status == 200) {
                    if ("" == XMLHttpRequest.responseText || null == XMLHttpRequest.responseText) {
                        // エラー発生
                        swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
                    }else {
                        // 例外エラー表示
                        document.write(XMLHttpRequest.responseText);
                        document.close();
                    }
                }else {
                    console.log("ajax通信に失敗しました");
                    console.log("XMLHttpRequest : " + XMLHttpRequest.status);
                    console.log("textStatus     : " + textStatus);
                    console.log("errorThrown    : " + errorThrown.message);
                    swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
                }
            });
        }catch(e) {
            // エラー発生
            swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
        }

    });

    $('#liveViewModal').on('hide.bs.modal', function () {
        if ($('#recStop').is(':visible')) {
            // 録画停止処理
            recStop();
        }
        // 録画処理待機時間考慮
        setTimeout(function(){
        	const el2 = $('#viewArea').get(0);
        	// モーダルのビデオ停止
        	el2.pause();
        },400);
        if (boolMute) {
        	// サムネイルの存在確認
        	if (document.getElementById(selectPeerId) != null) {
            	// サムネイルがミュートだった場合状態を戻す
            	const video = $('#' + selectPeerId).find('video').get(0);
            	video.muted = true;
            	// peerIdとMediaConnection比較
        		for (var i=0; i<calls.length; i++) {
        			if(selectPeerId == calls[i].remoteId) {
        				// MediaConnectionを閉じる
        				calls[i].close(true);
        				// MediaConnection削除
        				calls.splice(i, 1);
        			}
        		}
            	$('#' + selectPeerId + '_volume').html("<span style='float: right; font-size: 18px' class='glyphicon glyphicon-volume-off' aria-hidden='true'>");
        	}
        }
        boolMute = false;
    });

});

/*
 * サムネイル中継ミュート切り替え
 */
function mute(_peerId) {

	// サムネイルの存在確認
	if (document.getElementById(_peerId) != null) {
	    // ビデオソース取得
	    const video = $('#' + _peerId).find('video').get(0);
	    // ミュート判定
	    if(video.muted){
	    	video.muted = false;
	    	// 自身のlocalStreamを設定して相手に発信
	    	calls.push(peer.call(_peerId, localStream));
	    	$('#' + _peerId + '_volume').html("<span style='float: right; font-size: 18px' class='glyphicon glyphicon-volume-up' aria-hidden='true'>");
	    }else {
	    	video.muted = true;
	    	// peerIdとMediaConnection比較
			for (var i=0; i<calls.length; i++) {
				if(_peerId == calls[i].remoteId) {
					// MediaConnectionを閉じる
					calls[i].close(true);
					// MediaConnection削除
					calls.splice(i, 1);
				}
			}
	    	$('#' + _peerId + '_volume').html("<span style='float: right; font-size: 18px' class='glyphicon glyphicon-volume-off' aria-hidden='true'>");
	    }
	}

}

/*
 * 中継選択
 */
function live(_peerId) {

	// peerId保存
	selectPeerId = _peerId;

    // モーダルのビデオエリアに選択したビデオのソースをセットして再生
    const el1 = $('#' + _peerId).find('video').get(0);
    const el2 = $('#viewArea').get(0);

	// サムネイルの存在確認
	if (document.getElementById(_peerId) != null) {
	    el2.srcObject = el1.srcObject;
	    el2.play();

	    // ミュート判定
	    if(el1.muted){
	    	// サムネイルミュート状態保存
	    	boolMute = true;
	    	// ミュート解除
	    	el1.muted = false;
	    	// 自身のlocalStreamを設定して相手に発信
	    	calls.push(peer.call(_peerId, localStream));
	    	$('#' + _peerId + '_volume').html("<span style='float: right; font-size: 18px' class='glyphicon glyphicon-volume-up' aria-hidden='true'>");
	    }
	}

    // 送信者をセット
    const el3 = $('#' + _peerId + '_TENANT').get(0);
    const el4 = $('#' + _peerId + '_ID').get(0);
    const el5 = $('#' + _peerId + '_NAME').get(0);
    const el6 = $('#' + _peerId + '_LAT').get(0);
    const el7 = $('#' + _peerId + '_LON').get(0);

    if (el3 == null) {
        $('#tenant_id').text('');
    } else {
        $('#tenant_id').text(el3.innerText);
    }
    if (el4 == null) {
        $('#term_id').text('');
    } else {
        $('#term_id').text(el4.innerText);
    }
    if (el5 == null) {
        $('#term_name').text('端末名が取得できませんでした');
    } else {
        $('#term_name').text(el5.innerText.slice(1));
    }

    $('#locate_lat').attr('name', _peerId + '_LAT');
    $('#locate_lon').attr('name', _peerId + '_LON');
    $('#locate_lat').text(el6.innerText);
    $('#locate_lon').text(el7.innerText);

    // テナントIDと端末IDが取得できたら録画ボタンとキャプチャボタンを活性化
    if ((el3 != null) && (el3.innerText != null) && (el4 != null) && (el4.innerText != null)) {
        $('#recStart').show();
        $('#capture').show();
    } else {
        $('#recStart').hide();
        $('#capture').hide();
    }

    // メディアレコーダーの準備
    mediaRecorder = new MediaRecorder(el2.srcObject, options);
    mediaRecorder.ondataavailable = handleDataAvailable;

    // モーダルを開く
    $('#liveViewModal').modal()

}

/*
 * ondataavailableイベントハンドラ
 */
function handleDataAvailable(event) {

    if (event.data && event.data.size > 0) {
        // 録画データを格納
        recordedBlobs.push(event.data);
    }

}

/*
 * 録画停止処理
 */
function recStop() {

    // 録画停止
    mediaRecorder.stop();
    // 録画データをサーバに送信(mediaRecorder.stop時にondataavailable処理が終わらないため200ミリ秒待機)
    setTimeout(function(){
        sendRec();
    },200);
    // ボタン表示制御
    $('#recStart').show();
    $('#recStop').hide();

}

/*
 * 録画データをサーバに送信
 */
function sendRec() {

    if (recordedBlobs.length > 0) {

        // 録画データをバイナリ変換
        const videoBlob = new Blob(recordedBlobs, {type: 'video/webm;codecs=vp8,opus'});

        // キャプチャー日時を作成
        const dt = new Date();
        const Yer = dt.getFullYear();
        const Mon = dt.getMonth()+1;
        const Dat = dt.getDate();
        const Hor = dt.getHours();
        const Min = dt.getMinutes();
        const Sec = dt.getSeconds();
        const Mil = dt.getMilliseconds();
        const dateTime = Yer + ('00'+Mon).slice(-2) + ('00'+Dat).slice(-2) + ('00'+Hor).slice(-2) + ('00'+Min).slice(-2) + ('00'+Sec).slice(-2) + ('000'+Mil).slice(-3);

        // テナントIDと端末IDと位置情報を取得する
        const TENANT_ID = $('#tenant_id').get(0).innerText;
        const TERM_ID = $('#term_id').get(0).innerText;
        const LOCATE_LAT = $('#locate_lat').get(0).innerText;
        const LOCATE_LON = $('#locate_lon').get(0).innerText;

        // アップロードデータのFormData化
        let formData = new FormData();
        formData.append("TENANT_ID", TENANT_ID);
        formData.append("TERM_ID", TERM_ID);
        formData.append("FILE_FORMAT", "0404");
        formData.append("PHOTO_DATE", dateTime);
        formData.append("LOCATE_LAT", LOCATE_LAT);
        formData.append("LOCATE_LON", LOCATE_LON);
        formData.append("LOCATE", "");
        formData.append("contentsData", videoBlob);

        // --------------------------
        // サーバーにアップロードする
        // --------------------------
        try {
            $.ajax({
                type        : "POST",
                url         : "sendContents.html",
                processData : false,
                contentType : false, // 送信するデータをFormDataにする場合、こうしないといけない。
                cache       : false,
                data        : formData
            })
            // 通信成功
            .done(function(result) {
                if (null != result) {

                    if (result.indexOf('<!DOCTYPE html>') !== -1 || result.indexOf('<!doctype html>') !== -1) {
                        // 例外エラー表示
                        document.write(result);
                        document.close();
                    }else if (result == "Has completed") {
                        toastSuccess.fire({title: "録画を保存しました。"});
                    }else if (result == "Library not found" || result == "Terminal not found") {
                        swalAlert.fire({html: "端末情報にエラーが発生しています。<br>システム管理者にお問い合わせください。"});
                    }else {
                        swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
                    }

                }else {
                    // エラー発生
                    swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
                }
            })
            // 通信失敗
            .fail(function(XMLHttpRequest, textStatus, errorThrown) {
                if (XMLHttpRequest.status == 200) {
                    if ("" == XMLHttpRequest.responseText || null == XMLHttpRequest.responseText) {
                        // エラー発生
                        swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
                    }else {
                        // 例外エラー表示
                        document.write(XMLHttpRequest.responseText);
                        document.close();
                    }
                }else {
                    console.log("ajax通信に失敗しました");
                    console.log("XMLHttpRequest : " + XMLHttpRequest.status);
                    console.log("textStatus     : " + textStatus);
                    console.log("errorThrown    : " + errorThrown.message);
                    swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
                }
            });
        }catch(e) {
            // エラー発生
            swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"});
        }

    }else {
        // 録画失敗
        swalAlert.fire({html: "録画失敗しました。<br>対象の中継は切断されています。"});
    }

    // 録画領域初期化
    recordedBlobs = [];

}

/*
 * dataURI(base64)のバイナリ化
 */
function dataURItoBlob(dataURI) {

    const byteString = atob(dataURI.split(',')[1]);
    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0]

    const ab = new ArrayBuffer(byteString.length);
    let ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    const bb = new Blob([ab], { "type": mimeString });
    return bb;

}
