var systemName = document.title;									// システム名取得
var view = "icons";													// 表示判定変数
var viewIconsInfo = "photoInfo";									// アイコン表示時情報表示判定変数
var view_ID = -1;													// 表示中フォルダID格納変数
var grid = "contents-grid col-xs-4 col-md-2";						// gridサイズ格納変数
var contentsCount = 0;												// コンテンツ数格納変数
var checkNumber = 0;												// チェック順格納変数
var orderCheck  = [];												// チェックファイル格納変数
var orderDir  = [];													// ファイルディレクトリ格納変数
var searchCheck  = [];												// 検索ディレクトリID格納変数
var moveCheckDir;													// 移動先ディレクトリID格納変数
var sortCheck;														// 送信用検索変数 - ディレクトリID
var searchTerm;														// 送信用検索変数 - 撮影端末ID
var startDateTime;													// 送信用検索変数 - 開始日時
var endDateTime;													// 送信用検索変数 - 終了日時
var searchAddInfo;													// 送信用検索変数 - 付帯情報
var sortChoice = "dateTime";										// 並び順基準格納変数
var sortBy = "desc";												// 並び順格納変数
var searchFlg = false;												// 検索中フラグ
var allChkDispFlg = true;											// 全選択表示フラグ
var getDevice;														// 閲覧端末判断
var detailsFixedCol = 0;											// 詳細表示時に固定する先頭カラム数
var iziModalWidthWide;												// iziModalワイド幅(16:9画像用)
var iziModalWidthStd;												// iziModal標準幅(4:3画像用)
var iziModalWidthStdV;												// iziModal標準幅(3:4画像用)
var iziModalWidthSmall;												// iziModalスモール幅(9:16画像用)
var iziModalWidthSquare;											// iziModalスクエア幅(1:1画像用)
var LIVE_APP;														// ライブ接続先（アプリ名）
var LIVE_ROOM;														// ライブ接続先（ルーム名）

/***********************************************************************************************
* コンテンツ取得
***********************************************************************************************/
function getContents(dirId) {

	var cts = $("#contents");
	var mdl = $(".modal-pictures");

	try {

		cts.height(window.innerHeight - 78);													// コンテンツエリアサイズリセット
		$('#loading').css('visibility', 'visible');												// ロード中画面表示
		view_ID = dirId;																		// 表示フォルダID格納
		cts.empty();																			// コンテンツ削除
		mdl.empty();																			// モーダル領域削除
		contentsCount = 0;																		// コンテンツ数初期化
		checkNumber = 0;																		// チェック順初期化
		orderCheck  = [];																		// チェックファイル初期化
		orderDir  = [];																			// ファイルディレクトリ初期化
		searchFlg = false;																		// 検索中フラグ
		allChkBlock();																			// 全選択表示

		if (dirId != -1){
			if ($('#tree').treeview('searchDirId', dirId)[0]) {													// dirIdから対象ノード存在確認
				$("#current-library").html($('#tree').treeview('searchDirId', dirId)[0].text);					// 現在表示ライブラリを更新
				var nodeId = $('#tree').treeview('searchDirId', dirId)[0].nodeId;								// dirIdからnodeIdを取得
				$('#tree').treeview('revealNode', [ nodeId, { silent: true } ]);								// nodeIdまでツリーを開く
				$('#tree').treeview('expandNode', [ nodeId, { levels: 1, silent: true } ]);						// nodeIdのツリーを開く
				$('#tree').treeview('selectNode', [ nodeId, { silent: true } ]);								// nodeIdのツリー要素を選択
			}else {
				$("#current-library").html(systemName);
			}
		}else {
			$("#current-library").html(systemName);
		}

		if (view != "map") {
			// パンくずリストをコンテンツエリアに追加
			cts.prepend("<div id='topic-path'><a href='JavaScript:void(0)' onclick=\"getContents('-1')\">TOP</a></div>");
		}

		// ツリービュー更新
		updateTree();

		$.ajax({
			type        : "POST",
			url         : "main.html?getContents",
			data        : {acc_userID: $("#acc_userID").val(), dirId: dirId, view: view},
			dataType    : "JSON"
		})
		// 通信成功
		.done(function(result) {
			if (null != result) {
				// コンテンツ表示更新
				var loadResult = loadContents(result);
				if (loadResult) {
					// 詳細表示時は遅延させる
					if (view == "details") {
						setTimeout(function(){
							// ロード中画面非表示
							$('#loading').css('visibility', 'hidden');
						},50);
					}else {
						if (loadResult != "noMap") {
							// ロード中画面非表示
							$('#loading').css('visibility', 'hidden');
						}
					}
				}else {
					// エラー発生
					contentsErr();
				}
			}else {
				// エラー発生
				contentsErr();
			}
		})
		// 通信失敗
		.fail(function(XMLHttpRequest, textStatus, errorThrown) {
			//「表示」以外メニュー非表示
			menuDisabled();
			if (XMLHttpRequest.status == 200) {
				if ("" == XMLHttpRequest.responseText || null == XMLHttpRequest.responseText) {
					if (view == "map") {
						// 位置情報を持つコンテンツ無し
						// 中アイコンクリック(強制表示切り替え)
						$("#viewIconM").click();
						setTimeout(function(){toastInfo.fire({html: "<div style='text-align:left; padding:0px 0px 0px 10px;'>地図表示できる写真や動画はありません。<br>中アイコン表示に切り替えました。</div>"});}, 1500);
						return false;
					}else {
						// ディレクトリツリーデータ確認
						if ($("#directoryTreeData").val() == "error") {
							// 全メニュー非表示
							menuAllDisabled();
							cts.append("<font size='3' color='#AAAAAA'>　ユーザーに閲覧ライブラリが設定されていません。</font>");
						}else {
							cts.append("<font size='3' color='#AAAAAA'>　このライブラリは空です。</font>");
						}
					}
				}else {
					// 例外エラー表示
					document.write(XMLHttpRequest.responseText);
					document.close();
				}
			}else {
				console.log("ajax通信に失敗しました");
				console.log("XMLHttpRequest : " + XMLHttpRequest.status);
				console.log("textStatus     : " + textStatus);
				console.log("errorThrown    : " + errorThrown.message);
				swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"}).then(function(result) {
					cts.append("<font size='3' color='#FF0000'>　通信に失敗しました。<br>　ネットワークの接続状態をご確認ください。</font>");
				});
			}
			// ロード中画面非表示
			$('#loading').css('visibility', 'hidden');
		});
	}catch(e) {
		// エラー発生
		contentsErr();
	}

}

/***********************************************************************************************
* モーダル表示open
***********************************************************************************************/
function pop(self) {
	$(self).iziModal('open');
}

/***********************************************************************************************
* F5キー制御
***********************************************************************************************/
window.document.onkeydown = function ()
{
	// F5キーでtreeとcontents更新
	if (event.keyCode == 116)
	{
		// iziModalかsweetalert2を開いている場合は更新させない
		if (!$('.iziModal-overlay').length && !$('.swal2-container').length) {
			// ロード中は更新させない
			if ($('#loading').css('visibility') == 'hidden') {
				updateContents(false);
			}
		}
		event.keyCode = null;
		return false;
	}
}

/***********************************************************************************************
* ツリービュー更新
***********************************************************************************************/
function updateTree() {

	// 現在表示を保持
	var nowView = view;

	// ロード中画面表示
	$('#loading').css('visibility', 'visible');
	$.ajax({
		type: "POST",
		url: "main.html?updateTree",
		data: {acc_userID: $("#acc_userID").val()}
	})
	// 通信成功
	.done(function(result) {

		if (result.indexOf('<!DOCTYPE html>') !== -1 || result.indexOf('<!doctype html>') !== -1) {
			// 例外エラー表示
			document.write(result);
			document.close();
		}

		var expandedId = [];
		var tmpCheck = searchCheck;
		var tmpMoveCheck = moveCheckDir;

		if (result != "error") {

			// jsonをオブジェクトに変換
			var jsonObj = JSON.parse("[" + result + "]");
			// 最上位階層をソート
			jsonObj.sort(function(o, next_o) {
				if (o.disNo != 0 && o.disNo != null && next_o.disNo != 0 && next_o.disNo != null) {
					// 表示順 - 昇順に並べ替え
					return (o.disNo < next_o.disNo ? -1 : 1);
				}else {
					if (sortBy == "asc") {
						// 昇順に並べ替え
						return (o.text < next_o.text ? -1 : 1);
					}else {
						// 降順に並べ替え
						return (o.text > next_o.text ? -1 : 1);
					}
				}
			})
			// nodesをtextでソート
			jsonObj = sortTreeData(jsonObj);
			// jsonオブジェクトを文字列に変換
			var jsonStr = JSON.stringify(jsonObj);

			// 前回ディレクトリツリーデータを保存
			var oldDirectoryTreeData = $("#directoryTreeData").val();

			// ディレクトリツリーデータセット
			$("#directoryTreeData").val(jsonStr);

			// 前回ディレクトリツリーが表示されていれば処理
			if (oldDirectoryTreeData != "" && oldDirectoryTreeData != null && oldDirectoryTreeData != "error") {
				// ライブラリツリー前処理
				// 展開されているライブラリを保持
				if ($('#tree').treeview('getExpanded').length > 0) {
					for (var i = 0; i < $('#tree').treeview('getExpanded').length; i++) {
						expandedId[i] = $('#tree').treeview('getExpanded')[i].dirId;
					}
				}
				// 検索ツリー前処理
				// 検索ツリーがチェックされている場合
				if (tmpCheck.length != 0) {
					for (var i = 0; i < searchCheck.length; i++) {
						// チェックしたノードに子が存在する場合
						if ($('#searchTree').treeview('getNode', searchCheck[i]).nodes) {
							// 子ノードIDをすべて消す
							for (var x = 0; x < $('#searchTree').treeview('getNode', searchCheck[i]).nodes.length; x++) {
								tmpCheck = tmpCheck.filter(function(tmpCheck) {
									return tmpCheck !== $('#searchTree').treeview('getNode', searchCheck[i]).nodes[x].nodeId;
								});
							}
						}
					}
					// ノードIDをdirIdに変換
					for (var i = 0; i < tmpCheck.length; i++) {
						tmpCheck[i] = $('#searchTree').treeview('getNode', tmpCheck[i]).dirId;
					}
				}

				// ツリー削除
				$('#tree').treeview('remove');
				$('#searchTree').treeview('remove');
				$('#moveTree').treeview('remove');
			}

			// 検索ディレクトリID格納変数初期化
			searchCheck  = [];
			// 移動先ディレクトリID格納変数初期化
			moveCheckDir = "";

			// ツリー再表示
			treeDisplay();

			// パンくずリスト用表示領域取得
			var cts = $("#contents");

			// ライブラリツリー処理
			if (expandedId.length > 0) {
				var expLibNodeId;
				// ライブラリツリー展開
				for (var i = 0; i < expandedId.length; i++) {
					if ($('#tree').treeview('searchDirId', expandedId[i])[0]) {
						expLibNodeId = $('#tree').treeview('searchDirId', expandedId[i])[0].nodeId;				// dirIdからnodeIdを取得
						$('#tree').treeview('expandNode', [ expLibNodeId, { levels: 1, silent: true } ]);		// nodeIdのツリーを開く
					}
				}
			}
			if (view_ID != -1){
				if ($('#tree').treeview('searchDirId', view_ID)[0]) {
					// 検索中判定
					if (!searchFlg) {
						var libNodeId = $('#tree').treeview('searchDirId', view_ID)[0].nodeId;					// view_IDからnodeIdを取得
						$('#tree').treeview('selectNode', [ libNodeId, { silent: true } ]);						// nodeIdのツリー要素を選択
						if (nowView != "map") {
							// パンくずリストをコンテンツエリアに追加
							$("#topic-path").append("&ensp;>&ensp;" + createTopicPath(view_ID));
						}
					}
				}
			}
			if (nowView != "map") {
				// パンくずリスト表示
				$("#topic-path").css('visibility', 'visible');
				// サイズ判定
				judgeSize();
			}
			// 検索ツリー後処理
			// 検索ツリーがチェックされている場合
			if (tmpCheck.length > 0) {
				var searchNodeId;
				// tmpCheck内のdirIdをチェックする
				for (var i = 0; i < tmpCheck.length; i++) {
					if ($('#searchTree').treeview('searchDirId', tmpCheck[i])[0]) {
						searchNodeId = $('#searchTree').treeview('searchDirId', tmpCheck[i])[0].nodeId;			// dirIdからnodeIdを取得
						$('#searchTree').treeview('revealNode', [ searchNodeId, { silent: true } ]);			// nodeIdまでツリーを開く
						$('#searchTree').treeview('checkNode', [ searchNodeId, { silent: false } ]);			// nodeIdのツリー要素をチェック
					}
				}
			}
			// 移動ツリー後処理
			// 移動ツリーがチェックされている場合
			if (tmpMoveCheck) {
				var moveNodeId;
				// tmpMoveCheckのdirIdをチェックする
				if ($('#moveTree').treeview('searchDirId', tmpMoveCheck)[0]) {
					moveNodeId = $('#moveTree').treeview('searchDirId', tmpMoveCheck)[0].nodeId;		// dirIdからnodeIdを取得
					$('#moveTree').treeview('revealNode', [ moveNodeId, { silent: true } ]);			// nodeIdまでツリーを開く
					$('#moveTree').treeview('checkNode', [ moveNodeId, { silent: false } ]);			// nodeIdのツリー要素をチェック
				}
			}

		}else {
			if ($("#directoryTreeData").val() != "" && $("#directoryTreeData").val() != null && $("#directoryTreeData").val() != "error") {
				// ツリー削除
				$('#tree').treeview('remove');
				$('#searchTree').treeview('remove');
				$('#moveTree').treeview('remove');
			}
			// ディレクトリツリーデータに"error"をセット
			$("#directoryTreeData").val("error");
		}

	})
	// 通信失敗
	.fail(function(XMLHttpRequest, textStatus, errorThrown) {
		if (XMLHttpRequest.status == 200) {
			if ("" != XMLHttpRequest.responseText && null != XMLHttpRequest.responseText) {
				// 例外エラー表示
				document.write(XMLHttpRequest.responseText);
				document.close();
			}
		}else {
			console.log("ajax通信に失敗しました");
			console.log("XMLHttpRequest : " + XMLHttpRequest.status);
			console.log("textStatus     : " + textStatus);
			console.log("errorThrown    : " + errorThrown.message);
			swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
		}
		// ロード中画面非表示
		$('#loading').css('visibility', 'hidden');
	});

}

/***********************************************************************************************
* ディレクトリツリーデータ並べ替え(sort)
***********************************************************************************************/
function sortTreeData(jsonObj) {

	for (var key in jsonObj){
		if (typeof jsonObj[key] == "object") {
			if(Array.isArray(jsonObj[key])) {
				// 連想配列の場合
				// nodesをsort
				jsonObj[key].sort(function(o, next_o) {
					if (o.disNo != 0 && o.disNo != null && next_o.disNo != 0 && next_o.disNo != null) {
						// 表示順 - 昇順に並べ替え
						return (o.disNo < next_o.disNo ? -1 : 1);
					}else {
						if (sortBy == "asc") {
							// 昇順に並べ替え
							return (o.text < next_o.text ? -1 : 1);
						}else {
							// 降順に並べ替え
							return (o.text > next_o.text ? -1 : 1);
						}
					}
				})
				// forEachで要素ごとにに再帰呼び出し
				jsonObj[key].forEach(function(item){
					sortTreeData(item) ;
				});
			}else {
				// 配列はそのまま再帰呼び出し
				sortTreeData(jsonObj[key]) ;
			}
		}
	}

	return jsonObj;
}

/***********************************************************************************************
* ツリー表示
***********************************************************************************************/
function treeDisplay() {

	var directoryData = $("#directoryTreeData").val();

	// ライブラリツリー
	$("#tree").treeview({
		data: directoryData,
		expandIcon: 'glyphicon glyphicon-chevron-right',
		collapseIcon: 'glyphicon glyphicon-chevron-down',
		nodeIcon: 'glyphicon glyphicon-folder-close tree-margin',
        selectedIcon: "glyphicon glyphicon-folder-open tree-margin",
		showCheckbox: false,
		//levels: 99,   // Cllapsed 階層リストがある場合開いた(-)状態で表示
		levels: 1,   // Cllapsed 階層リストがある場合閉じた(+)状態で表示
		showBorder: false,
		color: "#A9BFD6",
		//backColor: "#494D51",
		backColor: "#000000",
		onNodeSelected: function(event, node) {
			getContents(node.dirId);
		}
	});

	// 検索ツリー
	$("#searchTree").treeview({
		data: directoryData,
		expandIcon: 'glyphicon glyphicon-chevron-right',
		collapseIcon: 'glyphicon glyphicon-chevron-down',
		showCheckbox: true,
		//levels: 99,   // Cllapsed 階層リストがある場合開いた(-)状態で表示
		levels: 1,   // Cllapsed 階層リストがある場合閉じた(+)状態で表示
		showBorder: false,
		//color: "#A9BFD6",
		//backColor: "#494D51",
		//backColor: "#000000",
		onNodeSelected: function(event, node) {
			// 選択解除
			$('#searchTree').treeview('unselectNode', [ node.nodeId, { silent: true } ]);
			// 選択時チェック
			if (!node.state.checked) {
				$('#searchTree').treeview('checkNode', [ node.nodeId, { silent: false } ]);
			}else {
				$('#searchTree').treeview('uncheckNode', [ node.nodeId, { silent: false } ]);
			}
		}
	});

	// 検索ツリーチェック
	$('#searchTree').on('nodeChecked', function(event, node) {
		// チェックしたノードIDを配列に格納
		searchCheck.push(node.nodeId);
		// チェックしたノードに子が存在する場合
		if ($('#searchTree').treeview('getNode', node.nodeId).nodes) {
			// 子ノードをすべてチェックする
			for (var i = 0; i < $('#searchTree').treeview('getNode', node.nodeId).nodes.length; i++) {
				// チェックされていなければチェック
				if (!$('#searchTree').treeview('getNode', node.nodeId).nodes[i].state.checked) {
					$('#searchTree').treeview('checkNode', [ $('#searchTree').treeview('getNode', node.nodeId).nodes[i].nodeId, { silent: false } ]);
				}
			}
		}
		// 兄弟ノードチェック
		var siblingsChkFkg = true;
		for (var i = 0; i < $('#searchTree').treeview('getSiblings', node).length; i++) {
			// チェックされていなければ抜ける
			if (!$('#searchTree').treeview('getSiblings', node)[i].state.checked) {
				siblingsChkFkg = false;
				break;
			}
		}
		// 兄弟ノードがすべてチェックされていた場合
		if (siblingsChkFkg) {
			// チェックしたノードに親が存在する場合
			if ($('#searchTree').treeview('getParent', node.nodeId).nodes) {
				// 親ノードがチェックされていない場合
				if (!$('#searchTree').treeview('getParent', node.nodeId).state.checked) {
					// 親ノードをチェックする
					$('#searchTree').treeview('checkNode', [ $('#searchTree').treeview('getParent', node.nodeId).nodeId, { silent: false } ]);
				}
			}
		}
	});

	// 検索ツリーチェック外し
	$('#searchTree').on('nodeUnchecked', function(event, node) {
		// チェックを外したノードIDを配列から除去
		searchCheck = searchCheck.filter(function(searchCheck) {
			return searchCheck !== node.nodeId
		});
		// チェックを外したノードに子が存在する場合
		if ($('#searchTree').treeview('getNode', node.nodeId).nodes) {
			// 子ノードすべてからチェックを外す
			for (var i = 0; i < $('#searchTree').treeview('getNode', node.nodeId).nodes.length; i++) {
				// チェックされていればチェックを外す
				if ($('#searchTree').treeview('getNode', node.nodeId).nodes[i].state.checked) {
					$('#searchTree').treeview('uncheckNode', [ $('#searchTree').treeview('getNode', node.nodeId).nodes[i].nodeId, { silent: false } ]);
				}
			}
		}
		// チェックを外したノードに親が存在する場合
		if ($('#searchTree').treeview('getParent', node.nodeId).nodes) {
			// 親ノードがチェックされていればチェックを外す
			if ($('#searchTree').treeview('getParent', node.nodeId).state.checked) {
				uncheckParent($('#searchTree').treeview('getParent', node.nodeId).nodeId);
			}

		}
	});

	// 移動ツリー
	$("#moveTree").treeview({
		data: directoryData,
		expandIcon: 'glyphicon glyphicon-chevron-right',
		collapseIcon: 'glyphicon glyphicon-chevron-down',
		showCheckbox: true,
		//levels: 99,   // Cllapsed 階層リストがある場合開いた(-)状態で表示
		levels: 1,   // Cllapsed 階層リストがある場合閉じた(+)状態で表示
		showBorder: false,
		//color: "#A9BFD6",
		//backColor: "#494D51",
		//backColor: "#F1F1F1",
		onNodeSelected: function(event, node) {
			// 選択解除
			$('#moveTree').treeview('unselectNode', [ node.nodeId, { silent: true } ]);
			// 選択時チェック
			if (!node.state.checked) {
				$('#moveTree').treeview('checkNode', [ node.nodeId, { silent: false } ]);
			}else {
				$('#moveTree').treeview('uncheckNode', [ node.nodeId, { silent: false } ]);
			}
		}
	});

	// 移動ツリーチェック
	$('#moveTree').on('nodeChecked', function(event, node) {
		$('#moveTree').treeview('uncheckAll', { silent: true });
		$('#moveTree').treeview('checkNode', [ node.nodeId, { silent: true } ]);
		// 移動先ディレクトリID格納
		moveCheckDir = node.dirId;
	});

	// 移動ツリーチェック外し
	$('#moveTree').on('nodeUnchecked', function(event, node) {
		// チェックを外さない
		$('#moveTree').treeview('checkNode', [ node.nodeId, { silent: false } ]);
	});

}

/***********************************************************************************************
* パンくずリスト作成
***********************************************************************************************/
function createTopicPath(dir_ID) {

	// dir_IDからtextを取得
	var nodeText = $('#tree').treeview('searchDirId', dir_ID)[0].text;
	// dir_IDからnodeIdを取得
	var libNodeId = $('#tree').treeview('searchDirId', dir_ID)[0].nodeId;
	// パンくずリスト作成
	var topicPath = "<a href='JavaScript:void(0)' onclick=\"getContents('" + dir_ID + "')\">" + nodeText + "</a>";

	// ノードに親が存在する場合
	if ($('#tree').treeview('getParent', libNodeId).nodes) {
		// パンくずリスト作成再帰呼び出し
		topicPath = createTopicPath($('#tree').treeview('getParent', libNodeId).dirId) + "&ensp;>&ensp;" + topicPath;
	}

	return topicPath;
}

/***********************************************************************************************
* コンテンツエリア更新
***********************************************************************************************/
function updateContents(search) {
	// 検索中判定
	if (searchFlg) {
		searchContents(search);
	}else {
		getContents(view_ID);
	}
}

/***********************************************************************************************
* ロード処理
***********************************************************************************************/
$(function() {

	/* ブラウザ戻るボタン抑止 */
	history.forward();

    // ブラウザ判定
    let userAgent = window.navigator.userAgent.toLowerCase();
    if (userAgent.indexOf('msie') !== -1 || userAgent.indexOf('trident') !== -1 || userAgent.indexOf('edge') !== -1) {
    	// IE or 旧Edge
    	$('#their-videos').append($('<div><p>※現在お使いのブラウザではライブ中継をご利用いただけません。</p></div>'));
    }

	// ライブ接続先設定
	LIVE_APP  = $("#liveApp").val();
	LIVE_ROOM = $("#liveRoom").val();

    // 閲覧端末判断
    getDevice = (function(){
        var ua = navigator.userAgent;
        if(ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0){
        	detailsFixedCol = 2;
            return 'sp';
        }else if(ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0){
        	detailsFixedCol = 3;
            return 'tab';
        }else{
        	detailsFixedCol = 3;
            return 'other';
        }
    })();

    // 付帯情報検索表示判定
    if($("#searchInfoFlg").val() == 0) {
    	$("#searchAddInfo").hide()
    }

	// iziModalにスワイプ動作を実装
	setSwipe('.modal-pictures');

	// 画面サイズ変更時に呼ばれる
	$(window).resize(function(){
		// サイズ判定
		judgeSize();
	});

	// 管理クリック時
	$("#manager").click(function() {
		window.open('mng_library.html', '_blank');
	});

	// ログアウトクリック時
	$(".logout").click(function() {
		$.ajax({
			type        : "POST",
			url         : "logOutWriting.html",
			data        : {acc_userID: $("#acc_userID").val()},
			dataType    : "JSON"
		})
		// 通信成功
		.done(function(result) {

		})
		// 通信失敗
		.fail(function(XMLHttpRequest, textStatus, errorThrown) {
			console.log("ajax通信に失敗しました");
			console.log("XMLHttpRequest : " + XMLHttpRequest.status);
			console.log("textStatus     : " + textStatus);
			console.log("errorThrown    : " + errorThrown.message);
		});
		// ログインページへ遷移
		// ログイン画面表示を50ミリ秒遅らせる(ログアウトのログと前後してしまうことがあるため)
		setTimeout(function(){window.location.href='login.html'}, 50);
	});

	// 検索アイコンクリック時
	$("#searchOpen").click(function() {
		// ボタンの活性状態確認
		if ($("#searchLi").attr('class') == "enabled") {
			$('#searchModal').modal();
		}
	});

	// 検索モーダル - 検索クリック時
	$("#search").click(function() {
		searchContents(true);
	});

	// 検索モーダル - クリアクリック時
	$("#searchClear").click(function() {
		// 検索ツリー全チェック外し
		$('#searchTree').treeview('uncheckAll', { silent: true });
		// 検索ディレクトリID初期化
		searchCheck  = [];
		// 入力項目初期化
		$("#searchTermId").val("");
		$("#start_dateTime").val("");
		$("#end_dateTime").val("");
		$("#searchAddInfoText").val("");
	});

	/* カレンダー表示 */
	$('#datetimepicker-start').datetimepicker({
		dayViewHeaderFormat: 'YYYY年 MMMM',
		tooltips: {
		close: '閉じる',
		selectMonth: '月を選択',
		prevMonth: '前月',
		nextMonth: '次月',
		selectYear: '年を選択',
		prevYear: '前年',
		nextYear: '次年',
		selectTime: '時間を選択',
		selectDate: '日付を選択',
		prevDecade: '前期間',
		nextDecade: '次期間',
		selectDecade: '期間を選択',
		prevCentury: '前世紀',
		nextCentury: '次世紀'
		},
		format: 'YYYY/MM/DD HH:mm',
		locale: 'ja',
		ignoreReadonly: true,
		//sideBySide: true,
		showClose: true,
		widgetPositioning: {
			horizontal: 'left',
			vertical: 'auto'
		}
	});
	$('#datetimepicker-end').datetimepicker({
		dayViewHeaderFormat: 'YYYY年 MMMM',
		tooltips: {
		close: '閉じる',
		selectMonth: '月を選択',
		prevMonth: '前月',
		nextMonth: '次月',
		selectYear: '年を選択',
		prevYear: '前年',
		nextYear: '次年',
		selectTime: '時間を選択',
		selectDate: '日付を選択',
		prevDecade: '前期間',
		nextDecade: '次期間',
		selectDecade: '期間を選択',
		prevCentury: '前世紀',
		nextCentury: '次世紀'
		},
		format: 'YYYY/MM/DD HH:mm',
		locale: 'ja',
		ignoreReadonly: true,
		//sideBySide: true,
		showClose: true,
		widgetPositioning: {
			horizontal: 'left',
			vertical: 'auto'
		}
	});

	/* 検索日時テキストボックスクリック時 カレンダー表示 */
//	$('#datetimepicker-start').click(function(e) {
//		$('#datetimepicker-start').datetimepicker('show');
//	});
//	$('#datetimepicker-end').click(function(e) {
//		$('#datetimepicker-end').datetimepicker('show');
//	});

	// Libraryアイコンクリック時
	$(".menu-trigger").click(function(e) {
		e.preventDefault();
		$(this).toggleClass('active');
		$("#wrapper").toggleClass("toggled");
	});

	// 特大アイコンクリック時
	$("#viewIconXl").click(function() {
		menuChk("displayChoice","viewIconXlCh");
		if (view != "icons") {
			view = "icons";
			grid = "contents-grid col-xs-12 col-md-4";
			updateContents(false);
		}else if (grid != "contents-grid col-xs-12 col-md-4") {
			var elements = document.getElementsByClassName('contents-grid');
			for(i = elements.length; 0 < i ;i--){
				elements[i - 1].className="contents-grid col-xs-12 col-md-4";
			}
			grid = "contents-grid col-xs-12 col-md-4";
			$('#contents').height($("#contents-area").innerHeight());
		}
		$('.viewIcon').show();
		$('.bottom-area').css('visibility', 'visible');
		$("#displayDrop").html("<span class='glyphicon glyphicon-picture' aria-hidden='true'></span><span class='caret'></span>");
		Cookies.set('reiss_display', 'viewIconXl');
	});

	// 大アイコンクリック時
	$("#viewIconL").click(function() {
		menuChk("displayChoice","viewIconLCh");
		if (view != "icons") {
			view = "icons";
			grid = "contents-grid col-xs-6 col-md-3";
			updateContents(false);
		}else if (grid != "contents-grid col-xs-6 col-md-3") {
			var elements = document.getElementsByClassName('contents-grid');
			for(i = elements.length; 0 < i ;i--){
				elements[i - 1].className="contents-grid col-xs-6 col-md-3";
			}
			grid = "contents-grid col-xs-6 col-md-3";
			$('#contents').height($("#contents-area").innerHeight());
		}
		$('.viewIcon').show();
		$('.bottom-area').css('visibility', 'visible');
		$("#displayDrop").html("<span class='glyphicon glyphicon-stop' aria-hidden='true'></span><span class='caret'></span>");
		Cookies.set('reiss_display', 'viewIconL');
	});

	// 中アイコンクリック時
	$("#viewIconM").click(function() {
		menuChk("displayChoice","viewIconMCh");
		if (view != "icons") {
			view = "icons";
			grid = "contents-grid col-xs-4 col-md-2";
			updateContents(false);
		}else if (grid != "contents-grid col-xs-4 col-md-2") {
			var elements = document.getElementsByClassName('contents-grid');
			for(i = elements.length; 0 < i ;i--){
				elements[i - 1].className="contents-grid col-xs-4 col-md-2";
			}
			grid = "contents-grid col-xs-4 col-md-2";
			$('#contents').height($("#contents-area").innerHeight());
		}
		$('.viewIcon').show();
		$('.bottom-area').css('visibility', 'visible');
		$("#displayDrop").html("<span class='glyphicon glyphicon-th-large' aria-hidden='true'></span><span class='caret'></span>");
		Cookies.set('reiss_display', 'viewIconM');
	});

	// 小アイコンクリック時
	$("#viewIconS").click(function() {
		menuChk("displayChoice","viewIconSCh");
		if (view != "icons") {
			view = "icons";
			grid = "contents-grid col-xs-2 col-md-1";
			updateContents(false);
		}else if (grid != "contents-grid col-xs-2 col-md-1") {
			var elements = document.getElementsByClassName('contents-grid');
			for(i = elements.length; 0 < i ;i--){
				elements[i - 1].className="contents-grid col-xs-2 col-md-1";
			}
			grid = "contents-grid col-xs-2 col-md-1";
			$('#contents').height($("#contents-area").innerHeight());
		}
		$('.viewIcon').show();
		$('.bottom-area').css('visibility', 'visible');
		$("#displayDrop").html("<span class='glyphicon glyphicon-th' aria-hidden='true'></span><span class='caret'></span>");
		Cookies.set('reiss_display', 'viewIconS');
	});

	// 詳細クリック時
	$("#viewDetail").click(function() {
		menuChk("displayChoice","viewDetailCh");
		if (view != "details") {
			view = "details";
			updateContents(false);
		}
		$('.viewIcon').hide();
		$('.bottom-area').css('visibility', 'visible');
		$("#displayDrop").html("<span class='glyphicon glyphicon-list' aria-hidden='true'></span><span class='caret'></span>");
		Cookies.set('reiss_display', 'viewDetail');
	});

	// 地図クリック時
	$("#viewMap").click(function() {
		menuChk("displayChoice","viewMapCh");
		if (view != "map") {
			view = "map";
			updateContents(false);
		}
		$('.viewIcon').hide();
		$('.bottom-area').css('visibility', 'hidden');
		$("#displayDrop").html("<span class='glyphicon glyphicon-map-marker' aria-hidden='true'></span><span class='caret'></span>");
		Cookies.set('reiss_display', 'viewMap');
	});

	// ファイル名クリック時
	$("#viewFileName").click(function() {
		menuChk("displayInfoChoice","viewFileNameCh");
		viewIconsInfo = "fileName";
		$('.photo-info').hide();
		$('.file-name').show();
		Cookies.set('reiss_displayInfo', 'fileName');
	});

	// 撮影情報クリック時
	$("#viewPhotoInfo").click(function() {
		menuChk("displayInfoChoice","viewPhotoInfoCh");
		viewIconsInfo = "photoInfo";
		$('.file-name').hide();
		$('.photo-info').show();
		Cookies.set('reiss_displayInfo', 'photoInfo');
	});

	// 最新の情報に更新クリック時
	$("#update").click(function() {
		updateContents(false);
	});

	// ライブラリ(並べ替え)クリック時
	$("#librarySort").click(function() {
		if (sortChoice != "library") {
			menuChk("sortChoice","librarySortCh");
			sortChoice = "library";
			updateContents(false);
		}
		Cookies.set('reiss_sortChoice', 'librarySort');
	});

	// ファイル名(並べ替え)クリック時
	$("#nameSort").click(function() {
		if (sortChoice != "name") {
			menuChk("sortChoice","nameSortCh");
			sortChoice = "name";
			updateContents(false);
		}
		Cookies.set('reiss_sortChoice', 'nameSort');
	});

	// 撮影端末(並べ替え)クリック時
	$("#termSort").click(function() {
		if (sortChoice != "term") {
			menuChk("sortChoice","termSortCh");
			sortChoice = "term";
			updateContents(false);
		}
		Cookies.set('reiss_sortChoice', 'termSort');
	});

	// 撮影日時(並べ替え)クリック時
	$("#dateTimeSort").click(function() {
		if (sortChoice != "dateTime") {
			menuChk("sortChoice","dateTimeSortCh");
			sortChoice = "dateTime";
			updateContents(false);
		}
		Cookies.set('reiss_sortChoice', 'dateTimeSort');
	});

	// 昇順(並べ替え)クリック時
	$("#ascSort").click(function() {
		if (sortBy != "asc") {
			menuChk("sortOrder","ascSortCh");
			sortBy = "asc";
			updateContents(false);
		}
		$("#sortDrop").html("<span class='glyphicon glyphicon-sort-by-attributes' aria-hidden='true'></span><span class='caret'></span>");
		Cookies.set('reiss_sortOrder', 'ascSort');
	});

	// 降順(並べ替え)クリック時
	$("#descSort").click(function() {
		if (sortBy != "desc") {
			menuChk("sortOrder","descSortCh");
			sortBy = "desc";
			updateContents(false);
		}
		$("#sortDrop").html("<span class='glyphicon glyphicon-sort-by-attributes-alt' aria-hidden='true'></span><span class='caret'></span>");
		Cookies.set('reiss_sortOrder', 'descSort');
	});

	// 付帯情報クリック時
	$("#additionalInfo").click(function() {
		// ボタンの活性状態確認
		if ($("#additionalInfoLi").attr('class') == "menu-li enabled") {
			var aia = $("#additionalInfoArea");
			aia.empty();
			if (Object.keys(orderCheck).length === 0) {
				swalInfo.fire({html: "付帯情報を編集するファイルを選択してください。"});
			}else {
				$.ajax({
					type: "POST",
					url: "main.html?additionalInfo",
					data: {acc_userID: $("#acc_userID").val(), orderCheck: orderCheck},
					dataType    : "JSON"
				})
				// 通信成功
				.done(function(result) {
					aia.append(result.modalHtml);
					var $allInfo = document.getElementsByClassName("addInfo");

					//付帯情報項目が無い場合は「更新」ボタンを表示しない
					if ($allInfo.length == 0) {
						$('#additionalInfoEdit').hide();
					}else {
						$('#additionalInfoEdit').show();
					}

					//座標情報があれば地図表示
					if (result.locateLat != "0.0" && result.locateLon != "0.0") {
						var map = additionalInfoMap();
						//地図表示コンテンツ格納用
						var bounds = L.latLngBounds([]);
						L.marker([result.locateLat,result.locateLon]).addTo(map);
						bounds.extend([result.locateLat,result.locateLon]);
						//地図の中心の緯度と経度と縮尺の設定
						map.fitBounds(bounds);
					}

					$('#additionalInfoModal').modal();

					if (result.locateLat != "0.0" && result.locateLon != "0.0") {
						//モーダルが表示しきってからmap表示調整
						setTimeout(function(){ map.invalidateSize()}, 500);
					}
				})
				// 通信失敗
				.fail(function(XMLHttpRequest, textStatus, errorThrown) {
					if (XMLHttpRequest.status == 200) {
						if ("" != XMLHttpRequest.responseText && null != XMLHttpRequest.responseText) {
							// 例外エラー表示
							document.write(XMLHttpRequest.responseText);
							document.close();
						}
					}else {
						console.log("ajax通信に失敗しました");
						console.log("XMLHttpRequest : " + XMLHttpRequest.status);
						console.log("textStatus     : " + textStatus);
						console.log("errorThrown    : " + errorThrown.message);
						swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
					}
				});
			}
		}
	});

	// 更新（付帯情報モーダル）クリック時
	$("#additionalInfoEdit").click(function() {
		var $allInfo = document.getElementsByClassName("addInfo");
		var orderFile = [];
		var orderInfoId = [];
		var orderInfoValue = [];
		var $allModal = document.getElementsByClassName("iziModal");
		var iziModalCall = [];
		var iziModalId = null;

		for (i = 0; i < $allInfo.length; i++) {
			orderInfoId[i] = $allInfo[i].getAttribute('addinfo-id');
			orderInfoValue[i] = $allInfo[i].value;
		}
		for (i = 0; i < $allModal.length; i++) {
			if ($allModal[i].getAttribute('aria-hidden') === 'false') {
				iziModalCall[0] = $allModal[i].getAttribute('fileName');
				iziModalId = $allModal[i].id;
				break;
			}
		}

		if (iziModalCall.length == 0) {
			orderFile = orderCheck;
		}else {
			orderFile = iziModalCall;
		}

		$.ajax({
			type: "POST",
			url: "main.html?additionalInfoEdit",
			data: {acc_userID: $("#acc_userID").val(), orderFile:orderFile, orderInfoId:orderInfoId, orderInfoValue:orderInfoValue},
		})
		// 通信成功
		.done(function(result) {
			if (result.indexOf('<!DOCTYPE html>') !== -1 || result.indexOf('<!doctype html>') !== -1) {
				// 例外エラー表示
				document.write(result);
				document.close();
			}else if (result == "true") {
				swalSuccess.fire({html: "付帯情報を更新しました。"}).then(function(result) {
					// 詳細表示時は画面更新
					if (view == "details") {
						swalInfo.fire({html: "画面を更新します。"}).then(function(result) {
							setTimeout(function() {
								$('#additionalInfoModal').modal('hide');
							}, 100);
							setTimeout(function() {
								if (iziModalId != null) {
									$('#' + iziModalId).iziModal('close');
								}
							}, 200);
							setTimeout(function() {
								updateContents(false);
							}, 300);
						});
					}
				});
			}else {
				// 更新失敗（いくつかの更新が失敗）
				swalAlert.fire({html: "いくつかの更新に失敗しました。"}).then(function(result) {
					// 詳細表示時は画面更新
					if (view == "details") {
						swalInfo.fire({html: "画面を更新します。"}).then(function(result) {
							setTimeout(function() {
								$('#additionalInfoModal').modal('hide');
							}, 100);
							setTimeout(function() {
								if (iziModalId != null) {
									$('#' + iziModalId).iziModal('close');
								}
							}, 200);
							setTimeout(function() {
								updateContents(false);
							}, 300);
						});
					}
				});
			}
		})
		// 通信失敗
		.fail(function(XMLHttpRequest, textStatus, errorThrown) {
			if (XMLHttpRequest.status == 200) {
				if ("" != XMLHttpRequest.responseText && null != XMLHttpRequest.responseText) {
					// 例外エラー表示
					document.write(XMLHttpRequest.responseText);
					document.close();
				}
			}else {
				console.log("ajax通信に失敗しました");
				console.log("XMLHttpRequest : " + XMLHttpRequest.status);
				console.log("textStatus     : " + textStatus);
				console.log("errorThrown    : " + errorThrown.message);
				swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
			}
		});

	});

	// ダウンロードクリック時
	$("#download").click(function() {
		// ボタンの活性状態確認
		if ($("#downloadLi").attr('class') == "menu-li enabled") {
			if (Object.keys(orderCheck).length === 0) {
				swalInfo.fire({html: "ダウンロードするファイルを選択してください。"});
			}else {
				$.ajax({
					type: "POST",
					url: "main.html?download",
					data: {acc_userID: $("#acc_userID").val(), orderCheck: orderCheck, orderDir: orderDir}
				})
				// 通信成功
				.done(function(result) {
					if (result.indexOf('<!DOCTYPE html>') !== -1 || result.indexOf('<!doctype html>') !== -1) {
						// 例外エラー表示
						document.write(result);
						document.close();
					}
					document.location.href = result;
				})
				// 通信失敗
				.fail(function(XMLHttpRequest, textStatus, errorThrown) {
					if (XMLHttpRequest.status == 200) {
						if ("" != XMLHttpRequest.responseText && null != XMLHttpRequest.responseText) {
							// 例外エラー表示
							document.write(XMLHttpRequest.responseText);
							document.close();
						}
					}else {
						console.log("ajax通信に失敗しました");
						console.log("XMLHttpRequest : " + XMLHttpRequest.status);
						console.log("textStatus     : " + textStatus);
						console.log("errorThrown    : " + errorThrown.message);
						swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
					}
				});
			}
		}
	});

	// 移動クリック時
	$("#move").click(function() {
		// ボタンの活性状態確認
		if ($("#moveLi").attr('class') == "menu-li enabled") {
			if (Object.keys(orderCheck).length === 0) {
				swalInfo.fire({html: "移動するファイルを選択してください。"});
			}else {
				$('#moveModal').modal();
			}
		}
	});

	// 実行（移動モーダル）クリック時
	$("#moveRun").click(function() {
		// 移動先ディレクトリID存在確認
		if (moveCheckDir) {
			// ロード中画面表示
			$('#loading').css('visibility', 'visible');
			$.ajax({
				type: "POST",
				url: "main.html?move",
				data: {acc_userID: $("#acc_userID").val(), orderCheck: orderCheck, orderDir: orderDir, moveCheckDir: moveCheckDir},
			})
			// 通信成功
			.done(function(result) {
				$('#moveModal').modal('hide');
				if (result.indexOf('<!DOCTYPE html>') !== -1 || result.indexOf('<!doctype html>') !== -1) {
					// 例外エラー表示
					document.write(result);
					document.close();
				}else if (result == "true") {
					// 移動成功
				}else {
					// 移動失敗（いくつかの移動が失敗）
					swalAlert.fire({html: "いくつかの移動が失敗しました。"});
				}
				setTimeout(function(){
					updateContents(false);
				},200);
			})
			// 通信失敗
			.fail(function(XMLHttpRequest, textStatus, errorThrown) {
				if (XMLHttpRequest.status == 200) {
					if ("" != XMLHttpRequest.responseText && null != XMLHttpRequest.responseText) {
						// 例外エラー表示
						document.write(XMLHttpRequest.responseText);
						document.close();
					}
				}else {
					console.log("ajax通信に失敗しました");
					console.log("XMLHttpRequest : " + XMLHttpRequest.status);
					console.log("textStatus     : " + textStatus);
					console.log("errorThrown    : " + errorThrown.message);
					swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
				}
				// ロード中画面非表示
				$('#loading').css('visibility', 'hidden');
			});
		}else {
			swalInfo.fire({html: "移動先ライブラリを選択してください。"});
		}
	});

	// 削除クリック時
	$("#delete").click(function() {
		// ボタンの活性状態確認
		if ($("#deleteLi").attr('class') == "menu-li enabled") {
			if (Object.keys(orderCheck).length === 0) {
				swalInfo.fire({html: "削除するファイルを選択してください。"});
			}else {
				swalConfirm.fire({
					html: "選択したファイルを削除しますか？"
				}).then(function(result) {
					if (result.value) {
						// ロード中画面表示
						$('#loading').css('visibility', 'visible');
						$.ajax({
							type: "POST",
							url: "main.html?delete",
							data: {acc_userID: $("#acc_userID").val(), orderCheck: orderCheck, orderDir: orderDir}
						})
						// 通信成功
						.done(function(result) {
							if (result.indexOf('<!DOCTYPE html>') !== -1 || result.indexOf('<!doctype html>') !== -1) {
								// 例外エラー表示
								document.write(result);
								document.close();
							}else if (result == "true") {
								// 削除成功
							}else {
								// 削除失敗（いくつかの削除が失敗）
								swalAlert.fire({html: "いくつかの削除が失敗しました。"});
							}
							updateContents(false);
						})
						// 通信失敗
						.fail(function(XMLHttpRequest, textStatus, errorThrown) {
							if (XMLHttpRequest.status == 200) {
								if ("" != XMLHttpRequest.responseText && null != XMLHttpRequest.responseText) {
									// 例外エラー表示
									document.write(XMLHttpRequest.responseText);
									document.close();
								}
							}else {
								console.log("ajax通信に失敗しました");
								console.log("XMLHttpRequest : " + XMLHttpRequest.status);
								console.log("textStatus     : " + textStatus);
								console.log("errorThrown    : " + errorThrown.message);
								swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
							}
						});
						// ロード中画面非表示
						$('#loading').css('visibility', 'hidden');
					}
				})
			}
		}
	});

	// 全選択/全解除クリック時
	$("#allCheck").click(function() {
		// ボタンの活性状態確認
		if ($("#allCheckLi").attr('class') == "menu-li-ch enabled") {
			if (allChkDispFlg) {
				// 全選択クリック時
				allCheck();
			}else {
				// 全解除クリック時
				allUnCheck();
			}
		}
	});

	// 閲覧端末判定から操作を実行
    if(getDevice == 'sp'){
        //スマホ
    	//メニュー表示設定
    	$('#manager').css('display', 'none');
    }else if(getDevice == 'tab'){
        //タブレット
    	//メニュー表示設定
    	$(".menu-trigger").click();
    	$('#manager').css('display', 'none');
    	//wrapperにスワイプ動作を実装
    	setSwipe2('#wrapper');
    }else if(getDevice == 'other'){
        //その他
    	//メニュー表示設定
    	$(".menu-trigger").click();
    }

	// サイズ判定
	judgeSize();

	// cookieから前回表示状態を取得
	switch (Cookies.get('reiss_display')) {
		case "viewIconXl":
			menuChk("displayChoice","viewIconXlCh");
			view = "icons";
			$('.viewIcon').show();
			grid = "contents-grid col-xs-12 col-md-4";
			$('.bottom-area').css('visibility', 'visible');
			$("#displayDrop").html("<span class='glyphicon glyphicon-picture' aria-hidden='true'></span><span class='caret'></span>");
			break;
		case "viewIconL":
			menuChk("displayChoice","viewIconLCh");
			view = "icons";
			$('.viewIcon').show();
			grid = "contents-grid col-xs-6 col-md-3";
			$('.bottom-area').css('visibility', 'visible');
			$("#displayDrop").html("<span class='glyphicon glyphicon-stop' aria-hidden='true'></span><span class='caret'></span>");
			break;
		case "viewIconM":
			menuChk("displayChoice","viewIconMCh");
			view = "icons";
			$('.viewIcon').show();
			grid = "contents-grid col-xs-4 col-md-2";
			$('.bottom-area').css('visibility', 'visible');
			$("#displayDrop").html("<span class='glyphicon glyphicon-th-large' aria-hidden='true'></span><span class='caret'></span>");
			break;
		case "viewIconS":
			menuChk("displayChoice","viewIconSCh");
			view = "icons";
			$('.viewIcon').show();
			grid = "contents-grid col-xs-2 col-md-1";
			$('.bottom-area').css('visibility', 'visible');
			$("#displayDrop").html("<span class='glyphicon glyphicon-th' aria-hidden='true'></span><span class='caret'></span>");
			break;
		case "viewDetail":
			menuChk("displayChoice","viewDetailCh");
			view = "details";
			$('.viewIcon').hide();
			$('.bottom-area').css('visibility', 'visible');
			$("#displayDrop").html("<span class='glyphicon glyphicon-list' aria-hidden='true'></span><span class='caret'></span>");
			break;
		case "viewMap":
			// map権限確認
			if ($("#viewMap").length) {
				menuChk("displayChoice","viewMapCh");
				view = "map";
				$('.viewIcon').hide();
				$('.bottom-area').css('visibility', 'hidden');
				$("#displayDrop").html("<span class='glyphicon glyphicon-map-marker' aria-hidden='true'></span><span class='caret'></span>");
			}
			break;
	}
	switch (Cookies.get('reiss_displayInfo')) {
		case "fileName":
			menuChk("displayInfoChoice","viewFileNameCh");
			viewIconsInfo = "fileName";
			break;
		case "photoInfo":
			menuChk("displayInfoChoice","viewPhotoInfoCh");
			viewIconsInfo = "photoInfo";
			break;
	}
	switch (Cookies.get('reiss_sortChoice')) {
		case "librarySort":
			menuChk("sortChoice","librarySortCh");
			sortChoice = "library";
			break;
		case "nameSort":
			menuChk("sortChoice","nameSortCh");
			sortChoice = "name";
			break;
		case "termSort":
			menuChk("sortChoice","termSortCh");
			sortChoice = "term";
			break;
		case "dateTimeSort":
			menuChk("sortChoice","dateTimeSortCh");
			sortChoice = "dateTime";
		break;
	}
	switch (Cookies.get('reiss_sortOrder')) {
		case "ascSort":
			menuChk("sortOrder","ascSortCh");
			sortBy = "asc";
			$("#sortDrop").html("<span class='glyphicon glyphicon-sort-by-attributes' aria-hidden='true'></span><span class='caret'></span>");
			break;
		case "descSort":
			menuChk("sortOrder","descSortCh");
			sortBy = "desc";
			$("#sortDrop").html("<span class='glyphicon glyphicon-sort-by-attributes-alt' aria-hidden='true'></span><span class='caret'></span>");
			break;
	}

	// ディレクトリツリーデータ確認
	if ($("#directoryTreeData").val() != "error") {
		// jsonをオブジェクトに変換
		var jsonObj = JSON.parse("[" + $("#directoryTreeData").val() + "]");
		// 最上位階層をソート
		jsonObj.sort(function(o, next_o) {
			if (o.disNo != 0 && o.disNo != null && next_o.disNo != 0 && next_o.disNo != null) {
				// 表示順 - 昇順に並べ替え
				return (o.disNo < next_o.disNo ? -1 : 1);
			}else {
				if (sortBy == "asc") {
					// 昇順に並べ替え
					return (o.text < next_o.text ? -1 : 1);
				}else {
					// 降順に並べ替え
					return (o.text > next_o.text ? -1 : 1);
				}
			}
		})
		// nodesをソート
		jsonObj = sortTreeData(jsonObj);
		// jsonオブジェクトを文字列に変換
		var jsonStr = JSON.stringify(jsonObj);
		// ディレクトリツリーデータセット
		$("#directoryTreeData").val(jsonStr);
		// ツリー表示
		treeDisplay();
	}

	// コンテンツエリア表示
	updateContents(false);

});

/***********************************************************************************************
* メニューチェック処理
***********************************************************************************************/
function menuChk(menuClass,id)
{
	var $replacement = document.getElementsByClassName(menuClass);
	for (i = 0; i < $replacement.length; i++) {
		$replacement[i].innerHTML = "　";
	}
	document.getElementById(id).innerHTML = "✓";
}

/***********************************************************************************************
* チェックボックス処理
***********************************************************************************************/
function chkdisp(obj,id)
{

	//チェック
	if (obj.checked) {
		orderCheck[checkNumber] = $(obj).attr('name');
		orderDir[checkNumber] = $(obj).attr('dir');
		checkNumber++;

		var allCheck = true;

		if (view == "icons") {
			// アイコン表示の場合
			document.getElementById(id).innerHTML = checkNumber;
			document.getElementById(id).style.display = "block";

			// 全チェックか確認
			for (var i=0; i<contentsCount; i++) {
				if($("#Chk-No" + i).prop('checked') == false) {
					allCheck = false;
					break;
				}
			}
			if (allCheck == true) {
				// 全解除表示
				allUnChkBlock();
			}

		}else if (view == "details") {
			// 詳細表示の場合
			var dataTable_List = $('#dataTable').DataTable();
			dataTable_List.$("#Chk-No" + id).prop('checked',true);

			// dataTable_Listから全チェックか確認
			for (var i=0; i<contentsCount; i++) {
				if(dataTable_List.$("#Chk-No" + i).prop('checked') == false) {
					allCheck = false;
					break;
				}
			}

			if (allCheck == true) {
				// dataTableの全チェックにチェックを入れる
				$('.allChk').prop('checked',true);
				dataTable_List.$('.allChk').prop('checked',true);
				// 全解除表示
				allUnChkBlock();
			}
		}
		// メニュー表示 判定
		menuDisabledCancel();
	}

	//チェック外し
	else {
		checkNumber--;

		if (view == "icons") {
			// アイコン表示の場合
			document.getElementById(id).style.display = "none";
			for (var i=0; i<contentsCount; i++) {
				if(Number(document.getElementById(id).innerHTML) < Number(document.getElementById(i).innerHTML)) {
					document.getElementById(i).innerHTML--;
				}
			}
			orderCheck.splice(document.getElementById(id).innerHTML - 1,1);
			orderDir.splice(document.getElementById(id).innerHTML - 1,1);
			document.getElementById(id).innerHTML = "";
		}else if (view == "details") {
			// 詳細表示の場合
			var idx = orderCheck.indexOf($(obj).attr('name'));
			if (idx >= 0) {
				orderCheck.splice(idx, 1);
				orderDir.splice(idx, 1);
			}
			$('.allChk').prop('checked',false);
			var dataTable_List = $('#dataTable').DataTable();
			dataTable_List.$("#Chk-No" + id).prop('checked',false);
			dataTable_List.$('.allChk').prop('checked',false);
		}

		// 全選択表示
		allChkBlock();
		if (Object.keys(orderCheck).length === 0) {
			// チェックメニュー非表示
			chkMenuDisabled();
		}else {
			// メニュー表示 判定
			menuDisabledCancel();
		}
	}

}

/***********************************************************************************************
* 全選択チェックボックス処理
***********************************************************************************************/
function allCheck()
{

	// 全チェック
	$('.Chk').prop('checked',true);

	if (view == "icons") {
		// アイコン表示の場合
		// チェック順挿入・表示
		for (var i=0; i<contentsCount; i++) {
			if(document.getElementById(i).style.display == "none") {
				var obj = document.getElementById("Chk-No" + i);
				orderCheck[checkNumber] = $(obj).attr('name');
				orderDir[checkNumber] = $(obj).attr('dir');
				checkNumber++;
				document.getElementById(i).innerHTML = checkNumber;
				document.getElementById(i).style.display = "block";
			}
		}

	}else if (view == "details") {
		// 詳細表示の場合
		checkNumber = 0;
		orderCheck  = [];
		orderDir  = [];
		$('.allChk').prop('checked',true);
		var dataTable_List = $('#dataTable').DataTable();
		dataTable_List.$('.Chk').prop('checked',true);
		dataTable_List.$('.allChk').prop('checked',true);

		for (var i=0; i<contentsCount; i++) {
			orderCheck[checkNumber] = dataTable_List.$("#Chk-No" + i).attr('name');
			orderDir[checkNumber] = dataTable_List.$("#Chk-No" + i).attr('dir');
			checkNumber++;
		}

	}

	// 全解除表示
	allUnChkBlock();
	// メニュー表示 判定
	menuDisabledCancel();
}

/***********************************************************************************************
* 全解除チェックボックス処理
***********************************************************************************************/
function allUnCheck()
{
	checkNumber = 0;
	orderCheck  = [];
	orderDir  = [];

	// 全チェック解除
	$('.Chk').prop('checked',false);

	if (view == "icons") {
		// アイコン表示の場合
		// チェック順削除・非表示
		for (var i=0; i<contentsCount; i++)
		{
			document.getElementById(i).style.display = "none";
			document.getElementById(i).innerHTML = "";
		}

	}else if (view == "details") {
		// 詳細表示の場合
		$('.allChk').prop('checked',false);
		var dataTable_List = $('#dataTable').DataTable();
		dataTable_List.$('.Chk').prop('checked',false);
		dataTable_List.$('.allChk').prop('checked',false);

	}

	// 全選択表示
	allChkBlock();
	// チェックメニュー非表示
	chkMenuDisabled();
}

/***********************************************************************************************
* dataTable全チェック処理
***********************************************************************************************/
function tableAllChk(obj)
{
	// チェック
	if ( obj.checked ) {
		allCheck();
	}
	// チェック外し
	else {
		allUnCheck();
	}
}

/***********************************************************************************************
* 全選択表示処理
***********************************************************************************************/
function allChkBlock()
{
	allChkDispFlg = true;
	$("#allCheck").html("<span class='glyphicon glyphicon-check menu-icon' aria-hidden='true'><span class='menu-char'><br>全選択</span></span>");
}

/***********************************************************************************************
* 全解除表示処理
***********************************************************************************************/
function allUnChkBlock()
{
	allChkDispFlg = false;
	$("#allCheck").html("<span class='glyphicon glyphicon-unchecked menu-icon' aria-hidden='true'><span class='menu-char'><br>全解除</span></span>");
}

/***********************************************************************************************
* 検索ツリー親チェック外し処理
***********************************************************************************************/
function uncheckParent(id)
{
	// チェックを外したノードIDを配列から除去
	searchCheck = searchCheck.filter(function(searchCheck) {
		return searchCheck !== id
	});
	// 親ノードのチェック外しはサイレント
	$('#searchTree').treeview('uncheckNode', [ id, { silent: true } ]);

	// チェックを外したノードに親が存在する場合
	if ($('#searchTree').treeview('getParent', id).nodes) {
		// 親ノードがチェックされている場合
		if ($('#searchTree').treeview('getParent', id).state.checked) {
			// 親ノードのチェックを外す
			uncheckParent($('#searchTree').treeview('getParent', id).nodeId);
		}
	}
}

/***********************************************************************************************
* コンテンツ表示更新
***********************************************************************************************/
function loadContents(imageData)
{
	// チェックメニュー非表示
	chkMenuDisabled();
	var errFlg = true;
	try {
		var cts = $("#contents");
		var mdl = $(".modal-pictures");

		contentsCount = imageData.length;

		if (sortChoice == "library") {
			if (sortBy == "desc") {
				// 降順に並べ替え
				imageData.reverse();
			}
		}else {
			// ライブラリ(並べ替え)以外
			imageData.sort(compareDisNo);
		}

		if (view == "icons") {
			//アイコン表示
			var $flex = $("<div class='flex' id='contents-area'></div>");

			for (var i = 0; i < imageData.length; i++) {

				var $grid = $("<div class='" + grid + "'></div>");
				var $thumbnail = $("<div class='thumbnail'></div>");
				var $center = $("<div class='col text-center'></div>");
				var $imgUndefined = $("<a class='img-over' href='JavaScript:void(0)'></a>");
				var $img = $("<img />");
				var $undefined = $("<a href='JavaScript:void(0)'></a>");
				var $label = $("<label></label>");
				$img.attr("src", imageData[i].src);

				if (imageData[i].directoryID != null){
					$imgUndefined.attr("onclick", "getContents('" + imageData[i].directoryID + "');return false;");
				}

				if (imageData[i].modalHtml != null) {
					var modalImage = imageData[i].modalHtml.replace('@izimodal-width16:9', iziModalWidthWide);
					modalImage = modalImage.replace('@izimodal-width4:3', iziModalWidthStd);
					modalImage = modalImage.replace('@izimodal-width3:4', iziModalWidthStdV);
					modalImage = modalImage.replace('@izimodal-width9:16', iziModalWidthSmall);
					modalImage = modalImage.replace('@izimodal-width1:1', iziModalWidthSquare);
					mdl.append(modalImage);
					$imgUndefined.attr("onclick", "pop('#Modal-No" + imageData[i].modalNo + "');return false;");
				}

				$imgUndefined.append($img);
				$center.append($imgUndefined);
				$label.append("<p id='" + i + "' style='display:none; margin:2px 0 0; color: #0A7AFF;'></p>");
				if (imageData[i].directoryID != null){
					$label.append("<table><tbody><tr valign='top'><td><input type='checkbox' class='Chk' id='Chk-No" + i + "' dir='" + imageData[i].directoryPath
							+ "' name='" + imageData[i].fileName + "' onclick=\"chkdisp(this, " + i + ")\"></td><td>" + imageData[i].fileName + "</td></tr></tbody></table>");
				}else {
					$label.append("<table><tbody><tr valign='top'><td align='left'><input type='checkbox' class='Chk' id='Chk-No" + i + "' dir='" + imageData[i].directoryPath
							+ "' name='" + imageData[i].fileName + "' onclick=\"chkdisp(this, " + i + ")\"></td><td class='file-name'>" + imageData[i].fileName + "</td><td class='photo-info info-tag' align='left'>端末：</td><td class='photo-info' align='left'>" + imageData[i].termName
							+ "</td></tr><tr class='photo-info' valign='top'><td></td><td class='info-tag' align='left'>日時：</td><td align='left' style='word-break: keep-all;'>" + imageData[i].photoDate + "</td></tr></tbody></table>");
				}
				$undefined.append($label);
				$center.append($undefined);
				$thumbnail.append($center);
				$grid.append($thumbnail);
				$flex.append($grid);
			}

			cts.append($flex);

			//ページング作成
		    $('.flex').pagination({
		        itemElement : '> .contents-grid',							// アイテムの要素
		        wrapElement : 'div',										// 親要素を包む要素
		        displayItemCount : 100,										// 1ページ毎に表示する個数
		        prevNextPageBtnMode : true,									// 前・次のページへ移動するボタン機能の有無
		        firstEndPageBtnMode : false,								// 最初・最後のページへ移動するボタン機能の有無
		        pageInfoDisplay : true,										// ページ情報の表示の有無
		        pageNumberDisplayNumber : 5,								// ページ番号の表示個数
		        onePageOnlyDisplay : false,									// 1ページのみの場合にページネーションを表示するかどうか
		        ellipsisMode : true,										// 省略記号を表示するかどうか
		        ellipsisMaxPageNumber : 6,									// 省略記号が表示される最大ページ数
		        paginationClassName : 'col-md-12 col-xs-12 text-center',	// ページネーション本体のクラス名
		        setPaginationMode : 'prepend'								// ページネーションを設置する方法  'after''before''append''prepend'
		    });

		    //ファイル情報表示判定
		    if (viewIconsInfo == "fileName") {
		    	$('.photo-info').hide();
		    	$('.file-name').show();
		    }else if (viewIconsInfo == "photoInfo") {
		    	$('.file-name').hide();
		    	$('.photo-info').show();
		    }

		    setTimeout(function(){
		    	//表示領域高さ設定
		    	$('#contents').height($("#contents-area").innerHeight());
			},50);

		}else if (view == "details") {
			//詳細表示
			var $table = $("<table class='table table-bordered dataTable' id='dataTable' width='100%'></table>");
			var $thead = $("<thead></thead>");
			var $tbody = $("<tbody></tbody>");
			var $tr = $("<tr style='white-space:nowrap'></tr>");			//改行しない

			$tr.append($("<th><input type='checkbox' class='allChk' id='allChk' onclick='tableAllChk(this)'></th>"));
			$tr.append($("<th>img</th>"));
			$tr.append($("<th>ファイル名</th>"));

			if (imageData[0].filePath != null) {
				$tr.append($("<th>ファイルパス</th>"));
			}

			$tr.append($("<th>撮影端末</th>"));
			$tr.append($("<th>撮影日時</th>"));
			$tr.append($("<th>撮影場所</th>"));

			if (imageData[0].addItemHtml != null) {
				$tr.append($(imageData[0].addItemHtml));
			}

			$tr.append($("<th>更新ID</th>"));
			$tr.append($("<th>更新日時</th>"));
			$thead.append($tr);
			$table.append($thead);

			for (var i = 0; i < imageData.length; i++) {
				$tr = $("<tr></tr>");
				$tr.append($("<td><input type='checkbox' class='Chk' id='Chk-No" + i + "' dir='" + imageData[i].directoryPath + "'name='" + imageData[i].fileName + "' onclick=\"chkdisp(this, " + i + ")\"></td>"));

				if (imageData[i].directoryID != null){
					$tr.append($("<td style='cursor:pointer' onclick = getContents('" + imageData[i].directoryID + "')><img src = '" + imageData[i].src + "' width='50px'/></td>"));
					$tr.append($("<td style='cursor:pointer' onclick = getContents('" + imageData[i].directoryID + "')>" + imageData[i].fileName + "</td>"));
					if (imageData[i].filePath != null) {
						$tr.append($("<td>" + imageData[i].filePath + "</td>"));
					}
					$tr.append($(imageData[i].addInfoHtml));
				}

				if (imageData[i].modalHtml != null) {
					$tr.append($("<td style='cursor:pointer' onclick = pop('#Modal-No" + imageData[i].modalNo + "')><img src = '" + imageData[i].src + "' width='50px'/></td>"));
					$tr.append($("<td style='cursor:pointer' onclick = pop('#Modal-No" + imageData[i].modalNo + "')>" + imageData[i].fileName + "</td>"));
					if (imageData[i].filePath != null) {
						$tr.append($("<td>" + imageData[i].filePath + "</td>"));
					}
					$tr.append($(imageData[i].addInfoHtml));
					var modalImage = imageData[i].modalHtml.replace('@izimodal-width16:9', iziModalWidthWide);
					modalImage = modalImage.replace('@izimodal-width4:3', iziModalWidthStd);
					modalImage = modalImage.replace('@izimodal-width3:4', iziModalWidthStdV);
					modalImage = modalImage.replace('@izimodal-width9:16', iziModalWidthSmall);
					modalImage = modalImage.replace('@izimodal-width1:1', iziModalWidthSquare);
					mdl.append(modalImage);
				}

				$tbody.append($tr);
			}
			$table.append($tbody);
			cts.append($table);

			// 表示崩れ対策
			setTimeout(function(){
				try {
					/* dataTableの初期設定 */
					$('#dataTable').dataTable({
						"language": {						/* 日本語化 */
							"decimal": ".",
							"thousands": ",",
							"sProcessing": "処理中...",
							"sLengthMenu": "_MENU_ 件表示",
							"sZeroRecords": "データがありません。",
							"sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示中",
							"sInfoEmpty": " 0 件中 0 から 0 まで表示",
							"sInfoFiltered": "（全 _MAX_ 件より抽出）",
							"sInfoPostFix": "",
							"sSearch": "検索:",
							"sUrl": "",
							"oPaginate": {
							"sFirst": "先頭",
							"sPrevious": "<",
							"sNext": ">",
							"sLast": "最終"
							}
						},
						//"sPaginationType": "full_numbers",		/* two_button:進むと戻るのみ, full_numbers:全ボタン表示 */
						"asStripeClasses": ['even','even'],		/* odd, even にすると1行置きに色が変わる(odd, evenはcssで設定) */
						"bAutoWidth": true,						/* 列幅自動調整機能 */
						"sDom": "Blfrtip",						/* 列幅変更機能有効(パラメータ不明) */
						"bFilter": true,						/* フィルター機能 */
						"bPaginate": true,						/* ページング機能 */
						"bInfo": true,							/* ページ情報(n件中aからbまで表示) */
						"bLengthChange": true,					/* 1ページの表示件数切替 */
						"aLengthMenu": [10,20,50,100],			/* 1ページの表示件数切替コンボボックスソース */
						"iDisplayLength": 10,					/* 1ページの表示件数初期値 */
						"sScrollX": "100%",						/* 水平スクロール機能(幅をピクセル指定、空は無効) */
						"sScrollY": "",							/* 垂直スクロール機能(高さをピクセル指定、空は無効) */
						"bScrollCollapse": false,				/* 垂直スクロール有効時に表示幅固定 */
						"bSort": true,							/* 整列機能 */
						"aaSorting": [],						/* 整列初期状態(空[]は整列しない) */
						"columnDefs": [ {
							"orderable": false,
							"targets": [0,1]					/* 0,1カラム目の整列機能を無効にする */
						} ],
						"fixedColumns": {
							"leftColumns": detailsFixedCol,		/* スマホ：2列目まで列固定 タブレット：3列目まで列固定 その他：3列目まで列固定*/
						},
						"buttons": [							/* 各種出力ボタン表示(copy,csv,excel,pdf,print) */
							{
								bom: true,						/* エクセルで文字化けしないようにbom付き */
								extend: 'csv',
								exportOptions: {
									columns: ':gt(1)'			/* チェックボックス,imgカラムは出力しない（出力しても空白） */
				                }
							},
							{
								extend: 'excel',
								exportOptions: {
									columns: ':gt(1)'			/* チェックボックス,imgカラムは出力しない（出力しても空白） */
				                }
							}
				        ]
					} );
					//出力ボタンを右寄せ表示
					$('.dt-buttons').css('float', 'right');
					//表示領域高さ設定
					$('#contents').height($("#dataTable_wrapper").innerHeight());

				}catch(e) {
					//エラー発生
					errFlg = false;
				}
			},50);

		}else if (view == "map") {

			//地図表示コンテンツ数確認
			var mapCounter = 0;
			for (var i = 0; i < imageData.length; i++) {
				if (imageData[i].locateLat != "0.0" && imageData[i].locateLon != "0.0") {
					mapCounter++;
				}
			}

			if (mapCounter == 0) {
		    	// 位置情報を持つコンテンツ無し
				// 中アイコンクリック(強制表示切り替え)
				$("#viewIconM").click();
				setTimeout(function(){toastInfo.fire({html: "<div style='text-align:left; padding:0px 0px 0px 10px;'>地図表示できる写真や動画はありません。<br>中アイコン表示に切り替えました。</div>"});}, 1500);
				return "noMap";
		    }

			//「表示」以外メニュー非表示
			menuDisabled();
			//地図表示
			var map = defaultMap();
			//地図表示コンテンツ格納用
		    var bounds = L.latLngBounds([]);
		    //撮影場所格納用
		    var locateHtml = "";

			for (var i = 0; i < imageData.length; i++) {

				if (imageData[i].locateLat != "0.0" && imageData[i].locateLon != "0.0") {

					if (imageData[i].locate != null && imageData[i].locate != "") {
						locateHtml = "<br>撮影場所：" + imageData[i].locate;
					}else {
						locateHtml = "";
					}

				    L.marker([imageData[i].locateLat,imageData[i].locateLon], {title:imageData[i].fileName}, {riseOnHover:true}).bindPopup("<img src='" + imageData[i].src + "' onclick=\"pop('#Modal-No" + imageData[i].modalNo + "')\" width='250px' style='cursor:pointer'/>" +
				    		"<p>撮影端末：" + imageData[i].termName + "<br>撮影日時：" + imageData[i].photoDate + locateHtml + "</p>").addTo(map);

				    bounds.extend([imageData[i].locateLat,imageData[i].locateLon]);

					if (imageData[i].modalHtml != null) {
						var modalImage = imageData[i].modalHtml.replace('@izimodal-width16:9', iziModalWidthWide);
						modalImage = modalImage.replace('@izimodal-width4:3', iziModalWidthStd);
						modalImage = modalImage.replace('@izimodal-width3:4', iziModalWidthStdV);
						modalImage = modalImage.replace('@izimodal-width9:16', iziModalWidthSmall);
						modalImage = modalImage.replace('@izimodal-width1:1', iziModalWidthSquare);
						mdl.append(modalImage);
					}

				}
			}

			//地図の中心の緯度と経度と縮尺の設定
			map.fitBounds(bounds);

		    //表示領域高さ設定
		    $('#contents').height(window.innerHeight - 78);

		}

		//モーダルダイアログリスト作成
		//iziModal.js
		$(".iziModal").iziModal({
			editButton:true,
			//fullscreen:true,
			zindex:2000,
			//navigateCaption:false,
			navigateArrows:"closeScreenEdge",
			group: "modalGroup",
			loop: true,
			focusInput: false,
			//headerColor: "#91b500",
			//headerColor: "#A9BFD6",
			headerColor: "#858585",
			overlayColor:"rgba(0,0,0,0.8)",
			width: "95vw",
			//width:modalww,
			appendTo:'.modal-pictures',
			appendToOverlay:'.modal-pictures',
		});
		//viewer.js
		$('.modal-pictures').viewer('destroy');		//古いビューアを破棄
		$('.modal-pictures').viewer({
		    navbar: true,							//下画像一覧表示
		    toolbar: true,							//ツールボタン
		    transition: true,						//動きの表現
		});											//ビューア構築

	}catch(e) {
		// エラー発生
		errFlg = false;
	}
	return errFlg;
}

/***********************************************************************************************
* コンテンツ検索
***********************************************************************************************/
function searchContents(search)
{
	var cts = $("#contents");
	var mdl = $(".modal-pictures");

	try {

		cts.height(window.innerHeight - 78);												// コンテンツエリアサイズリセット
		$('#loading').css('visibility', 'visible');											// ロード中画面表示
		cts.empty();																		// コンテンツ削除
		mdl.empty();																		// モーダル領域削除
		contentsCount = 0;																	// コンテンツ数初期化
		checkNumber = 0;																	// チェック順初期化
		orderCheck  = [];																	// チェックファイル初期化
		orderDir  = [];																		// ファイルディレクトリ初期化
		searchFlg = true;																	// 検索中フラグ
		allChkBlock();																		// 全選択表示
		$("#current-library").html("検索");													// 現在表示ライブラリを"検索"に設定

		if (view != "map") {
			// パンくずリストをコンテンツエリアに追加
			cts.prepend("<div id='topic-path'>検索結果</div>");
		}

		// 検索クリック判定
		if (search) {

			// 昇順にソート
			searchCheck.sort(function(a,b) {
				return (a < b ? -1 : 1);
			});

			// 値渡し
			sortCheck = searchCheck.slice();

			if (sortCheck.length == 0) {
				// 検索ツリーがチェックされていない場合
				sortCheck[0] = -1;
			}else {
				for (var i = 0; i < searchCheck.length; i++) {
					// チェックしたノードに子が存在する場合
					if ($('#searchTree').treeview('getNode', searchCheck[i]).nodes) {
						// 子ノードIDをすべて消す
						for (var x = 0; x < $('#searchTree').treeview('getNode', searchCheck[i]).nodes.length; x++) {
							sortCheck = sortCheck.filter(function(sortCheck) {
								return sortCheck !== $('#searchTree').treeview('getNode', searchCheck[i]).nodes[x].nodeId;
							});
						}
					}
				}
				// ノードIDをdirIdに変換
				for (var i = 0; i < sortCheck.length; i++) {
					sortCheck[i] = $('#searchTree').treeview('getNode', sortCheck[i]).dirId;
				}
			}

			searchTerm = $("#searchTermId").val();

			if ($("#start_dateTime").val() != "") {
				startDateTime = $("#start_dateTime").val();
			}else {
				startDateTime = "1900/01/01 00:00";
			}

			if ($("#end_dateTime").val() != "") {
				endDateTime = $("#end_dateTime").val();
			}else {
				endDateTime = "9999/12/31 23:59";
			}

			searchAddInfo = $("#searchAddInfoText").val();
		}

		// ツリービュー更新
		updateTree();

		$.ajax({
			type        : "POST",
			url         : "main.html?search",
			data        : {acc_userID: $("#acc_userID").val(), view: view, searchTerm: searchTerm,  startDateTime: startDateTime, endDateTime: endDateTime, searchAddInfo:searchAddInfo, sortCheck: sortCheck},
			dataType    : "JSON"
		})
		// 通信成功
		.done(function(result) {
			if (null != result) {
				// コンテンツ表示更新
				var loadResult = loadContents(result);
				if (loadResult) {
					// 詳細表示時は遅延させる
					if (view == "details") {
						setTimeout(function(){
							// ロード中画面非表示
							$('#loading').css('visibility', 'hidden');
						},50);
					}else {
						if (loadResult != "noMap") {
							// ロード中画面非表示
							$('#loading').css('visibility', 'hidden');
						}
					}
				}else {
					// エラー発生
					contentsErr();
				}
			}else {
				// エラー発生
				contentsErr();
			}
		})
		// 通信失敗
		.fail(function(XMLHttpRequest, textStatus, errorThrown) {
			//「表示」以外メニュー非表示
			menuDisabled();
			if (XMLHttpRequest.status == 200) {
				if ("" == XMLHttpRequest.responseText || null == XMLHttpRequest.responseText) {
					if (view == "map") {
						// 位置情報を持つコンテンツ無し
						// 中アイコンクリック(強制表示切り替え)
						$("#viewIconM").click();
						setTimeout(function(){toastInfo.fire({html: "<div style='text-align:left; padding:0px 0px 0px 10px;'>地図表示できる写真や動画はありません。<br>中アイコン表示に切り替えました。</div>"});}, 1500);
						return false;
					}else {
						cts.append("<font size='3' color='#AAAAAA'>　検索条件に一致する写真や動画はありません。</font>");
					}
				}else {
					// 例外エラー表示
					document.write(XMLHttpRequest.responseText);
					document.close();
				}
			}else {
				console.log("ajax通信に失敗しました");
				console.log("XMLHttpRequest : " + XMLHttpRequest.status);
				console.log("textStatus     : " + textStatus);
				console.log("errorThrown    : " + errorThrown.message);
				swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"}).then(function(result) {
					cts.append("<font size='3' color='#FF0000'>　通信に失敗しました。<br>　ネットワークの接続状態をご確認ください。</font>");
				});
			}
			// ロード中画面非表示
			$('#loading').css('visibility', 'hidden');
		});
	}catch(e) {
		// エラー発生
		contentsErr();
	}

}

/***********************************************************************************************
* コンテンツ比較関数(表示順)
***********************************************************************************************/
function compareDisNo(a, b){
	var r = 0;

	if (a.disNo != 0 && a.disNo != null && b.disNo != 0 && b.disNo != null) {
		if (a.disNo < b.disNo) {
			r = -1;
		}else if (a.disNo > b.disNo) {
			r = 1;
		}
	}else {
		// 通常比較
		r = compare(a, b);
	}

	return r;
}

/***********************************************************************************************
* コンテンツ比較関数
***********************************************************************************************/
function compare(a, b){
	var r = 0;

	if (sortChoice == "name") {

		if (a.fileName < b.fileName) {
			r = -1;
		}else if (a.fileName > b.fileName) {
			r = 1;
		}

	}else if (sortChoice == "term") {

		if (a.termName < b.termName) {
			r = -1;
		}else if (a.termName > b.termName) {
			r = 1;
		}else {
			if (a.dateTime < b.dateTime) {
				r = -1;
			}else if (a.dateTime > b.dateTime) {
				r = 1;
			}
		}

	}else if (sortChoice == "dateTime") {

		if (a.dateTime < b.dateTime) {
			r = -1;
		}else if (a.dateTime > b.dateTime) {
			r = 1;
		}

	}

	if (sortBy == "desc") {
		// 降順に並べ替え
		r = -1 * r;
	}

	return r;
}

/***********************************************************************************************
* エラー処理
***********************************************************************************************/
function contentsErr()
{
	var cts = $("#contents");
	var mdl = $(".modal-pictures");
	cts.height(window.innerHeight - 78);												// コンテンツエリアサイズリセット
	cts.empty();																		// コンテンツ削除
	mdl.empty();																		// モーダル領域削除
	contentsCount = 0;																	// コンテンツ数初期化
	checkNumber = 0;																	// チェック順初期化
	orderCheck  = [];																	// チェックファイル初期化
	orderDir  = [];																		// ファイルディレクトリ初期化
	allChkBlock();																		// 全選択表示
	menuAllDisabled();																	// 全メニュー非表示
	//コンテンツが画面から消えないので200ミリ秒待機
    setTimeout(function(){
		swalAlert.fire({html: "エラーが発生しました。<br>一度ログアウトし、再度ログインしてからもう一度お試しください。"}).then(function(result) {
			cts.append("<font size='3' color='#FF0000'>　エラーが発生しました。<br>　一度ログアウトし、再度ログインしてからもう一度お試しください。</font>");
		});
		// ロード中画面非表示
		$('#loading').css('visibility', 'hidden');
    },200);
}

/***********************************************************************************************
* デフォルト地図表示
***********************************************************************************************/
function defaultMap()
{
	var cts = $("#contents");

	//地図表示
	cts.append($("<div id='map'></div>"));

	// 地図のデフォルトの緯度経度と拡大率
	// 初期座標をZoomレベル15で
	var map = L.map('map').setView([$("#defaultLocateLat").val(),$("#defaultLocateLon").val()], 15);

	// 描画する(Copyrightは消しちゃダメ)
	var tileLayer = L.tileLayer($("#mapServer").val() + '{z}/{x}/{y}.png',{
	  attribution: '© <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
	  maxZoom: 18
	});
	tileLayer.addTo(map);

	//フルスクリーンボタン配置
	var zoomFS = new L.Control.ZoomFS();
	map.addControl(zoomFS);

	//フルスクリーン表示時のイベント
	map.on('enterFullscreen', function() {
		document.getElementById("map").style.zIndex = "1100";
		$('.leaflet-control-fullscreen').attr('id', 'exitFullscreenButton');
		$('.leaflet-control-fullscreen').attr('title', '元に戻す');
	});
	//フルスクリーン解除時のイベント
	map.on('exitFullscreen', function() {
		document.getElementById("map").style.zIndex = "10";
		$('.leaflet-control-fullscreen').attr('id', 'enterFullscreenButton');
		$('.leaflet-control-fullscreen').attr('title', 'フルスクリーン');
	});

	//ズームボタンを右下に
	map.zoomControl.setPosition("bottomright");

	//スケールバー表示
	L.control.scale({
		imperial: false,
		metric: true
	  }).addTo(map);

	// フルスクリーン 初期設定
	document.getElementById("map").style.zIndex = "10";
	$('.leaflet-control-fullscreen').attr('id', 'enterFullscreenButton');
	$('.leaflet-control-fullscreen').attr('title', 'フルスクリーン');

	return map;
}

/***********************************************************************************************
* 付帯情報地図表示
***********************************************************************************************/
function additionalInfoMap()
{

	// 地図のデフォルトの緯度経度と拡大率
	// 初期座標をZoomレベル15で
	var map = L.map('additionalInfoMap').setView([$("#defaultLocateLat").val(),$("#defaultLocateLon").val()], 15);

	// 描画する(Copyrightは消しちゃダメ)
	var tileLayer = L.tileLayer($("#mapServer").val() + '{z}/{x}/{y}.png',{
	  attribution: '© <a href="http://osm.org/copyright">OpenStreetMap</a> contributors',
	  maxZoom: 18
	});
	tileLayer.addTo(map);

	//フルスクリーンボタン配置
	var zoomFS = new L.Control.ZoomFS();
	map.addControl(zoomFS);

	var paddingLeft = $('#additionalInfoModal').css('padding-left');

	//フルスクリーン表示時のイベント
	map.on('enterFullscreen', function() {
		$('.modal-dialog').css({'width':'100%','height':'100%','margin':'0'});
		$('#additionalInfoModal').css('padding-left','');
		document.getElementById("additionalInfoMap").style.zIndex = "1100";
		$('.leaflet-control-fullscreen').attr('id', 'exitFullscreenButton');
		$('.leaflet-control-fullscreen').attr('title', '元に戻す');
	});
	//フルスクリーン解除時のイベント
	map.on('exitFullscreen', function() {
		$('.modal-dialog').css({'width':'','height':'','margin':''});
		$('#additionalInfoModal').css('padding-left',paddingLeft);
		document.getElementById("additionalInfoMap").style.zIndex = "10";
		$('.leaflet-control-fullscreen').attr('id', 'enterFullscreenButton');
		$('.leaflet-control-fullscreen').attr('title', 'フルスクリーン');
	});

	//ズームボタンを右下に
	map.zoomControl.setPosition("bottomright");

	//スケールバー表示
	L.control.scale({
		imperial: false,
		metric: true
	  }).addTo(map);

	// フルスクリーン 初期設定
	document.getElementById("additionalInfoMap").style.zIndex = "10";
	$('.leaflet-control-fullscreen').attr('id', 'enterFullscreenButton');
	$('.leaflet-control-fullscreen').attr('title', 'フルスクリーン');

	return map;
}

/***********************************************************************************************
* メニュー表示 判定(フォルダ・動画ファイルがチェックされている場合は帳票が無効)
***********************************************************************************************/
function menuDisabledCancel()
{
	$('#searchLi').removeClass().addClass("enabled");
	$('#displayLi').removeClass().addClass("dropdown");
	$('#displayDrop').removeClass().addClass("top-menu-a dropdown-toggle");
	$('#sortLi').removeClass().addClass("dropdown");
	$('#sortDrop').removeClass().addClass("top-menu-a dropdown-toggle");

	// 画像・動画ファイルチェック確認
	var i = orderCheck.length;
	var movFile = false;
	var folder = false;
	while (i--) {
		// フォルダ確認
		if (orderCheck[i].substr(-4,4) !== ".jpg" && orderCheck[i].substr(-4,4) !== ".mp4" && orderCheck[i].substr(-5,5) !== ".webm") {
			folder = true;
			break;
		}
	}
	if (!folder) {
		i = orderCheck.length;
		while (i--) {
			// 動画確認
			if (orderCheck[i].substr(-4,4) == ".mp4" || orderCheck[i].substr(-5,5) == ".webm") {
				movFile = true;
				break;
			}
		}
	}
	if (folder) {
		$('#formOutputLi').removeClass().addClass("menu-li dropup disabled");
		$('#formOutputDrop').removeClass().addClass("bottom-menu-a dropdown-toggle disabled");
		$('#additionalInfoLi').removeClass().addClass("menu-li disabled");
		$('#moveLi').removeClass().addClass("menu-li disabled");
	}else if (movFile) {
		$('#formOutputLi').removeClass().addClass("menu-li dropup disabled");
		$('#formOutputDrop').removeClass().addClass("bottom-menu-a dropdown-toggle disabled");
		$('#additionalInfoLi').removeClass().addClass("menu-li enabled");
		$('#moveLi').removeClass().addClass("menu-li enabled");
	}else {
		$('#formOutputLi').removeClass().addClass("menu-li dropup");
		$('#formOutputDrop').removeClass().addClass("bottom-menu-a dropdown-toggle");
		$('#additionalInfoLi').removeClass().addClass("menu-li enabled");
		$('#moveLi').removeClass().addClass("menu-li enabled");
	}

	$('#downloadLi').removeClass().addClass("menu-li enabled");
	$('#deleteLi').removeClass().addClass("menu-li enabled");
	$('#allCheckLi').removeClass().addClass("menu-li-ch enabled");
}

/***********************************************************************************************
* 全メニュー非表示
***********************************************************************************************/
function menuAllDisabled()
{
	$('#searchLi').removeClass().addClass("disabled");
	$('#displayLi').removeClass().addClass("dropdown disabled");
	$('#displayDrop').removeClass().addClass("top-menu-a dropdown-toggle disabled");
	$('#sortLi').removeClass().addClass("dropdown disabled");
	$('#sortDrop').removeClass().addClass("top-menu-a dropdown-toggle disabled");
	$('#formOutputLi').removeClass().addClass("menu-li dropup disabled");
	$('#formOutputDrop').removeClass().addClass("bottom-menu-a dropdown-toggle disabled");
	$('#additionalInfoLi').removeClass().addClass("menu-li disabled");
	$('#downloadLi').removeClass().addClass("menu-li disabled");
	$('#moveLi').removeClass().addClass("menu-li disabled");
	$('#deleteLi').removeClass().addClass("menu-li disabled");
	$('#allCheckLi').removeClass().addClass("menu-li-ch disabled");
}

/***********************************************************************************************
* 「表示」以外メニュー非表示
***********************************************************************************************/
function menuDisabled()
{
	$('#searchLi').removeClass().addClass("enabled");
	$('#displayLi').removeClass().addClass("dropdown");
	$('#displayDrop').removeClass().addClass("top-menu-a dropdown-toggle");
	$('#sortLi').removeClass().addClass("dropdown disabled");
	$('#sortDrop').removeClass().addClass("top-menu-a dropdown-toggle disabled");
	$('#formOutputLi').removeClass().addClass("menu-li dropup disabled");
	$('#formOutputDrop').removeClass().addClass("bottom-menu-a dropdown-toggle disabled");
	$('#additionalInfoLi').removeClass().addClass("menu-li disabled");
	$('#downloadLi').removeClass().addClass("menu-li disabled");
	$('#moveLi').removeClass().addClass("menu-li disabled");
	$('#deleteLi').removeClass().addClass("menu-li disabled");
	$('#allCheckLi').removeClass().addClass("menu-li-ch disabled");
}

/***********************************************************************************************
* チェックメニュー非表示
***********************************************************************************************/
function chkMenuDisabled()
{
	$('#searchLi').removeClass().addClass("enabled");
	$('#displayLi').removeClass().addClass("dropdown");
	$('#displayDrop').removeClass().addClass("top-menu-a dropdown-toggle");
	$('#sortLi').removeClass().addClass("dropdown");
	$('#sortDrop').removeClass().addClass("top-menu-a dropdown-toggle");
	$('#formOutputLi').removeClass().addClass("menu-li dropup disabled");
	$('#formOutputDrop').removeClass().addClass("bottom-menu-a dropdown-toggle disabled");
	$('#additionalInfoLi').removeClass().addClass("menu-li disabled");
	$('#downloadLi').removeClass().addClass("menu-li disabled");
	$('#moveLi').removeClass().addClass("menu-li disabled");
	$('#deleteLi').removeClass().addClass("menu-li disabled");
	$('#allCheckLi').removeClass().addClass("menu-li-ch enabled");
}

/***********************************************************************************************
* メニューサイズ判定
***********************************************************************************************/
function judgeSize()
{
	// コンテンツエリア高さ判定
	$('#contents').css('min-height', window.innerHeight - 78);
	switch (view) {
	case "icons":
		$('#contents').height($("#contents-area").innerHeight());
		break;
	case "details":
		$('#contents').height($("#dataTable_wrapper").innerHeight());
		break;
	case "map":
		$('#contents').height(window.innerHeight - 78);
		break;
	}
	// iziModalサイズ判定
	var width = window.innerWidth;
	var height = window.innerHeight;
	if (width > height) {
		// 横向き画面
		if (1.2 < width / height) {
			// ワイド画面
		    if(getDevice == 'sp'){
		        //スマホ
				iziModalWidthWide = "123vmin";
				iziModalWidthStd = "93vmin";
				iziModalWidthStdV= "53vmin";
				iziModalWidthSmall= "40vmin";
				iziModalWidthSquare= "70vmin";
		    }else if(getDevice == 'tab'){
		        //タブレット
				iziModalWidthWide = "161vmin";
				iziModalWidthStd = "121vmin";
				iziModalWidthStdV= "68vmin";
				iziModalWidthSmall= "51vmin";
				iziModalWidthSquare= "91vmin";
		    }else if(getDevice == 'other'){
		        //その他
				iziModalWidthWide = "161vmin";
				iziModalWidthStd = "121vmin";
				iziModalWidthStdV= "68vmin";
				iziModalWidthSmall= "51vmin";
				iziModalWidthSquare= "91vmin";
		    }
		}else {
			iziModalWidthWide = "95vw";
			iziModalWidthStd = "85vw";
			iziModalWidthStdV= "55vw";
			iziModalWidthSmall= "40vw";
			iziModalWidthSquare= "75vw";
		}
	}else {
		// 縦向き画面
		if (1.2 < height / width) {
			// ワイド画面
		    if(getDevice == 'sp'){
		        //スマホ
				iziModalWidthWide = "123vmin";
				iziModalWidthStd = "93vmin";
				iziModalWidthStdV= "93vmin";
				iziModalWidthSmall= "83vmin";
				iziModalWidthSquare= "93vmin";
		    }else if(getDevice == 'tab'){
		        //タブレット
				iziModalWidthWide = "161vmin";
				iziModalWidthStd = "121vmin";
				iziModalWidthStdV= "110vmin";
				iziModalWidthSmall= "75vmin";
				iziModalWidthSquare= "120vmin";
		    }else if(getDevice == 'other'){
		        //その他
				iziModalWidthWide = "161vmin";
				iziModalWidthStd = "121vmin";
				iziModalWidthStdV= "110vmin";
				iziModalWidthSmall= "75vmin";
				iziModalWidthSquare= "120vmin";
		    }
		}else {
			iziModalWidthWide = "95vw";
			iziModalWidthStd = "85vw";
			iziModalWidthStdV= "65vw";
			iziModalWidthSmall= "50vw";
			iziModalWidthSquare= "85vw";
		}
	}
}

/***********************************************************************************************
* iziModalにスワイプ動作を実装
***********************************************************************************************/
function setSwipe(elem) {
	var t = document.querySelector(elem);
	var r = document.getElementById('result');
	var startX;
	var moveX = 0;
	var dist = 30;
	t.addEventListener('touchstart', function(e) {
		e.preventDefault();
		startX = e.touches[0].pageX;
	});
	t.addEventListener('touchmove', function(e) {
		e.preventDefault();
		moveX = e.changedTouches[0].pageX;
	});
	t.addEventListener('touchend', function(e) {
		if (moveX != 0) {
			if (startX > moveX && startX > moveX + dist) {
				$('.iziModal-navigate-next').click();
			}else if (startX < moveX && startX + dist < moveX) {
			  	$('.iziModal-navigate-prev').click();
			}else {
				e.target.click();
			}
		}else {
			e.target.click();
		}
		moveX = 0;
	});
}

/***********************************************************************************************
* wrapperにスワイプ動作を実装
***********************************************************************************************/
function setSwipe2(elem) {
	var t = document.querySelector(elem);
	var r = document.getElementById('result');
	var startX;
	var moveX = 0;
	var dist = 30;
	t.addEventListener('touchstart', function(e) {
		startX = e.touches[0].pageX;
	});
	t.addEventListener('touchmove', function(e) {
		moveX = e.changedTouches[0].pageX;
	});
	t.addEventListener('touchend', function(e) {
		// iziModalかsweetalert2を開いている場合は動作させない
		if (!$('.iziModal-overlay').length && !$('.swal2-container').length) {
			if ($("#wrapper").hasClass("toggled")) {
				if (moveX < 320) {
					if (moveX != 0) {
						if (startX > moveX && startX > moveX + dist) {
							$(".menu-trigger").removeClass('active');
							$("#wrapper").removeClass("toggled");
						}
					}
				}
			}else {
				if (50 < moveX && moveX < 280) {
					if (moveX != 0) {
						if (startX < moveX && startX + dist < moveX) {
							$(".menu-trigger").addClass('active');
							$("#wrapper").addClass("toggled");
						}
					}
				}
			}
		}
		moveX = 0;
	});
}

/***********************************************************************************************
* 帳票出力
***********************************************************************************************/
function reportOutput(report_ID)
{
	// ボタンの活性状態確認
	if ($("#formOutputLi").attr('class') != "menu-li dropup disabled") {
		if (Object.keys(orderCheck).length === 0) {
			swalInfo.fire({html: "帳票に出力するファイルを選択してください。"});
		}else {
			$.ajax({
				type: "POST",
				url: "main.html?report",
				data: {acc_userID: $("#acc_userID").val(), report_ID: report_ID, orderCheck: orderCheck, orderDir: orderDir}
			})
			// 通信成功
			.done(function(result) {
				if (result.indexOf('<!DOCTYPE html>') !== -1 || result.indexOf('<!doctype html>') !== -1) {
					// 例外エラー表示
					document.write(result);
					document.close();
				}else if (result == "false") {
					swalAlert.fire({html: "帳票出力に失敗しました。<br>帳票が削除されているか有効期限切れです。"});
				}else {
					document.location.href = result;
				}
			})
			// 通信失敗
			.fail(function(XMLHttpRequest, textStatus, errorThrown) {
				if (XMLHttpRequest.status == 200) {
					if ("" != XMLHttpRequest.responseText && null != XMLHttpRequest.responseText) {
						// 例外エラー表示
						document.write(XMLHttpRequest.responseText);
						document.close();
					}
				}else {
					console.log("ajax通信に失敗しました");
					console.log("XMLHttpRequest : " + XMLHttpRequest.status);
					console.log("textStatus     : " + textStatus);
					console.log("errorThrown    : " + errorThrown.message);
					swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
				}
			});
		}
	}
}
