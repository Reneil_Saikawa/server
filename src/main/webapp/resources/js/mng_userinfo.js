var validator;
var oTable;
var libTreeCheck  = [];

$(function(){
	/* ブラウザ戻るボタン抑止 */
	history.forward();

	/* バリデーション */
	validator = $('form').validate({
		rules: {
			user_id: {
				required: true,
				numAlphaSymbol: true
			},
			password: {
				required: true,
				numAlphaSymbol: true
			},
			pwd_errcnt: {
				required: true,
				cNumber: true
			},
			user_name: {
				required: true,
			},
			start_date: {
				required: true
			},
			end_date: {
				required: true,
				compareDate: 'start_date'
			}
		},
		messages: {
			user_id: {
				required: required,
				numAlphaSymbol: numAlphaSymbol
			},
			password: {
				required: required,
				numAlphaSymbol: numAlphaSymbol
			},
			pwd_errcnt: {
				required: required,
				cNumber: number
			},
			user_name: {
				required: required
			},
			start_date: {
				required: required
			},
			end_date: {
				required: required,
				compareDate: compareDate
			}
		},
		errorPlacement: function(error, element){
			if ((element.attr("name") == "start_date") || (element.attr("name") == "end_date")) {
				error.insertAfter($('#'+ element.attr('name') + '_err'));
			} else {
				error.insertAfter(element);
			}
		}
	});

	/* リストの行クリック時 */
	$('#dataTable tbody tr').click(function(event) {
		if ($(this).hasClass('row_selected')) {
			// 選択行をクリック（行の選択を解除）
			oTable.$(this).removeClass('row_selected');
			oTable.$(this).find('input[name=r_chk]').prop('checked',false);
		} else {
			// 非選択業をクリック（行を選択）
			// 一旦全ての行の選択を解除する
			oTable.$('.sel_row').removeClass('row_selected');
			oTable.$('.sel_row').find('input[name=r_chk]').prop('checked',false);
			// 行を選択
			oTable.$(this).addClass('row_selected');
			oTable.$(this).find('input[name=r_chk]').prop('checked',true);
		}
	} );

	/* dataTableの初期設定 */
	oTable = $('#dataTable').dataTable({
		"language": {						/* 日本語化 */
			"decimal": ".",
			"thousands": ",",
			"sProcessing": "処理中...",
			"sLengthMenu": "_MENU_ 件表示",
			"sZeroRecords": "データがありません。",
			"sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示中",
			"sInfoEmpty": " 0 件中 0 から 0 まで表示",
			"sInfoFiltered": "（全 _MAX_ 件より抽出）",
			"sInfoPostFix": "",
			"sSearch": "検索:",
			"sUrl": "",
			"oPaginate": {
			"sFirst": "先頭",
			"sPrevious": "<",
			"sNext": ">",
			"sLast": "最終"
			}
		},
		//"sPaginationType": "full_numbers",	/* two_button:進むと戻るのみ, full_numbers:全ボタン表示 */
		"asStripeClasses": ['even','even'],	/* odd, even にすると1行置きに色が変わる(odd, evenはcssで設定) */
		"bAutoWidth": true,					/* 列幅自動調整機能 */
		"sDom": "lfrtip",					/* 列幅変更機能有効(パラメータ不明) */
		"bFilter": true,					/* フィルター機能 */
		"bPaginate": true,					/* ページング機能 */
		"bInfo": true,						/* ページ情報(n件中aからbまで表示) */
		"bLengthChange": true,				/* 1ページの表示件数切替 */
		"aLengthMenu": [10,20,50,100],		/* 1ページの表示件数切替コンボボックスソース */
		"iDisplayLength": 10,				/* 1ページの表示件数初期値 */
		"sScrollX": "",						/* 水平スクロール機能(幅をピクセル指定、空は無効) */
		"sScrollY": "",						/* 垂直スクロール機能(高さをピクセル指定、空は無効) */
		"bScrollCollapse": false,			/* 垂直スクロール有効時に表示幅固定 */
		"bSort": true,						/* 整列機能 */
		"aaSorting": [],					/* 整列初期状態(空[]は整列しない) */
		"aoColumnDefs": [ {
			"bSortable": false, "aTargets": [ 0 ]	/* 0カラム目の整列機能を無効にする */
		} ]
	} );

	// jsonをオブジェクトに変換
	var jsonObj = JSON.parse("[" + $("#libraryTreeData").val() + "]");
	// 最上位階層をソート
	jsonObj.sort(function(o, next_o) {
		if (o.disNo != 0 && o.disNo != null && next_o.disNo != 0 && next_o.disNo != null) {
			// 表示順 - 昇順に並べ替え
			return (o.disNo < next_o.disNo ? -1 : 1);
		}else {
			switch (Cookies.get('reiss_sortOrder')) {
			case "ascSort":
				// 昇順に並べ替え
				return (o.text < next_o.text ? -1 : 1);
			case "descSort":
				// 降順に並べ替え
				return (o.text > next_o.text ? -1 : 1);
			default:
				// 降順に並べ替え
				return (o.text > next_o.text ? -1 : 1);
			}
		}
	})
	// nodesをソート
	jsonObj = sortTreeData(jsonObj);
	// jsonオブジェクトを文字列に変換
	var jsonStr = JSON.stringify(jsonObj);
	// ライブラリツリーデータセット
	$("#libraryTreeData").val(jsonStr);

	$("#tree").treeview({
		data: jsonStr,
		expandIcon: 'glyphicon glyphicon-chevron-right',
		collapseIcon: 'glyphicon glyphicon-chevron-down',
		showCheckbox: true,
		//levels: 99,   // Cllapsed 階層リストがある場合開いた(-)状態で表示
		levels: 1,   // Cllapsed 階層リストがある場合閉じた(+)状態で表示
		showBorder: true,
		//color: "#A9BFD6",
		//backColor: "#494D51",
		//backColor: "#F1F1F1",
		onNodeSelected: function(event, node) {
			// 選択解除
			$('#tree').treeview('unselectNode', [ node.nodeId, { silent: true } ]);
			// 選択時チェック
			if (!node.state.checked) {
				$('#tree').treeview('checkNode', [ node.nodeId, { silent: false } ]);
			}else {
				$('#tree').treeview('uncheckNode', [ node.nodeId, { silent: false } ]);
			}
		}
	});

	// ツリーチェック
	$('#tree').on('nodeChecked', function(event, node) {
		// チェックしたdirIdを配列に格納
		libTreeCheck.push(node.dirId);
		// チェックしたノードに子が存在する場合
		if ($('#tree').treeview('getNode', node.nodeId).nodes) {
			// 子ノードからすべてチェックを外し非活性化する
			for (var i = 0; i < $('#tree').treeview('getNode', node.nodeId).nodes.length; i++) {
				// 閲覧ライブラリーツリー子チェック外し処理
				uncheckChild($('#tree').treeview('getNode', node.nodeId).nodes[i]);
				// 非活性化
				$('#tree').treeview('disableNode', [ $('#tree').treeview('getNode', node.nodeId).nodes[i].nodeId, { silent: true } ]);
			}
		}
		$('#lib_id').val(libTreeCheck);
	});

	// ツリーチェック外し
	$('#tree').on('nodeUnchecked', function(event, node) {
		// チェックを外したdirIdを配列から除去
		libTreeCheck = libTreeCheck.filter(function(libTreeCheck) {
			return libTreeCheck !== node.dirId
		});
		// チェックを外したノードに子が存在する場合
		if ($('#tree').treeview('getNode', node.nodeId).nodes) {
			// 子ノードをすべて活性化する
			for (var i = 0; i < $('#tree').treeview('getNode', node.nodeId).nodes.length; i++) {
				// 活性化
				$('#tree').treeview('enableNode', [ $('#tree').treeview('getNode', node.nodeId).nodes[i].nodeId, { silent: true } ]);
			}
		}
		$('#lib_id').val(libTreeCheck);
	});

	/* カレンダー表示 */
	$('#datetimepicker-start').datetimepicker({
		dayViewHeaderFormat: 'YYYY年 MMMM',
		tooltips: {
		close: '閉じる',
		selectMonth: '月を選択',
		prevMonth: '前月',
		nextMonth: '次月',
		selectYear: '年を選択',
		prevYear: '前年',
		nextYear: '次年',
		selectTime: '時間を選択',
		selectDate: '日付を選択',
		prevDecade: '前期間',
		nextDecade: '次期間',
		selectDecade: '期間を選択',
		prevCentury: '前世紀',
		nextCentury: '次世紀'
		},
		format: 'YYYY/MM/DD',
		minDate: $('#tenantStart').val(),
		maxDate: $('#tenantEnd').val() + ' 23:59:59',
		locale: 'ja',
		ignoreReadonly: true,
		//sideBySide: true,
		showClose: true,
		widgetPositioning: {
			horizontal: 'left',
			vertical: 'auto'
		}
	});
	$('#datetimepicker-end').datetimepicker({
		dayViewHeaderFormat: 'YYYY年 MMMM',
		tooltips: {
		close: '閉じる',
		selectMonth: '月を選択',
		prevMonth: '前月',
		nextMonth: '次月',
		selectYear: '年を選択',
		prevYear: '前年',
		nextYear: '次年',
		selectTime: '時間を選択',
		selectDate: '日付を選択',
		prevDecade: '前期間',
		nextDecade: '次期間',
		selectDecade: '期間を選択',
		prevCentury: '前世紀',
		nextCentury: '次世紀'
		},
		format: 'YYYY/MM/DD',
		minDate: $('#tenantStart').val(),
		maxDate: $('#tenantEnd').val() + ' 23:59:59',
		locale: 'ja',
		ignoreReadonly: true,
		//sideBySide: true,
		showClose: true,
		widgetPositioning: {
			horizontal: 'left',
			vertical: 'auto'
		}
	});

	/* 一覧ボタンクリック時 */
	$('#list').click(function() {
		// リストを開く
		$('#listModal').modal();
	});

	/* リストの編集ボタンクリック時 */
	$('#edit').click(function() {
		var selectedRow = getSelectedRow(oTable);
		if (selectedRow.length == 1) {
			// 選択が1行のとき

			// リストの値をフォームに転記する
			$('#user_id').val(selectedRow[0].cells.item(1).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#password').val(selectedRow[0].cells.item(2).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#pwd_errcnt').val(selectedRow[0].cells.item(3).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#user_name').val(selectedRow[0].cells.item(4).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			if (selectedRow[0].cells.item(5).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#manage_auth').prop('checked', true);
			}else {
				$('#manage_auth').prop('checked', false);
			}
			if (selectedRow[0].cells.item(6).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#output_auth').prop('checked', true);
				$('#disReportAuth').collapse('show');
			}else {
				$('#output_auth').prop('checked', false);
				$('#disReportAuth').collapse('hide');
			}
			if (selectedRow[0].cells.item(7).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#edit_auth').prop('checked', true);
			}else {
				$('#edit_auth').prop('checked', false);
			}
			if (selectedRow[0].cells.item(8).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#download_auth').prop('checked', true);
			}else {
				$('#download_auth').prop('checked', false);
			}
			if (selectedRow[0].cells.item(9).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#move_auth').prop('checked', true);
			}else {
				$('#move_auth').prop('checked', false);
			}
			if (selectedRow[0].cells.item(10).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#delete_auth').prop('checked', true);
			}else {
				$('#delete_auth').prop('checked', false);
			}

			$('#start_date').val(selectedRow[0].cells.item(11).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#end_date').val(selectedRow[0].cells.item(12).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			if (selectedRow[0].cells.item(13).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "") != "") {
				$('#del_flg').prop('checked', true);
			}else {
				$('#del_flg').prop('checked', false);
			}

			// ユーザーIDから閲覧ライブラリ権限を取得する
			getLibraryAuth($('#user_id').val());

			// ユーザーIDから帳票権限を取得する
			getReportAuth($('#user_id').val());

			// ユーザーIDを編集不可にする
			$('#user_id').attr('readonly', true);

			// バリデートをリセットする
			validator.resetForm();

			// メッセージを閉じる
			$('#success').alert('close');
			$('#add_danger').alert('close');
			$('#add_license_danger').alert('close');
			$('#up_license_danger').alert('close');

			// パスワードエラーカウントを表示する
			$('#disPwdErrcnt').collapse('show');

			// ボタンを「更新」に変更
			$('#reflect').attr('name', 'update');
			$('#reflect').html("<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span>&thinsp;更新");

			 // リストを閉じる
			$('#listModal').modal('hide');
		}else {
			swalInfo.fire({html: "編集するユーザーを選択してください。"});
			return false;
		}
	});

	/* クリアボタンクリック時 */
	$('#clearInputArea').click(function() {
		// メッセージを閉じる
		$('#success').alert('close');
		$('#add_danger').alert('close');
		$('#add_license_danger').alert('close');
		$('#up_license_danger').alert('close');
		 // フォームをクリアする
		clearForm(this.form);
		// バリデートをリセットする
		validator.resetForm();
		// 閲覧ライブラリツリーのチェックをすべて外す
		$('#tree').treeview('uncheckAll', { silent: false });
		// 閲覧ライブラリツリーを全て折りたたむ
		$('#tree').treeview('collapseAll', { silent: true });
		// パスワードエラーカウントを非表示にする
		$('#disPwdErrcnt').collapse('hide');
		// 帳票権限を非表示にする
		$('#disReportAuth').collapse('hide');
		// パスワードエラーカウントを0に設定する
		$('#pwd_errcnt').val(0);
		// ユーザーIDを編集可にする
		$('#user_id').attr('readonly', false);
		// ボタンを「追加」に変更
		$('#reflect').attr('name', 'add');
		$('#reflect').html("<span class='glyphicon glyphicon-plus' aria-hidden='true'></span>&thinsp;追加");
	});

	// 帳票権限表示チェック
	if ($("#output_auth").prop("checked") == true) {
		// 帳票権限表示を50ミリ秒遅らせる(画面チラつき防止)
		setTimeout(function(){$('#disReportAuth').collapse('show')}, 50);
	}else {
		$('#disReportAuth').collapse('hide');
	}

	// 追加・更新時処理
	if ($('#user_id').val() != "" && $('#user_id').val() != null && !$('#add_danger').length && !$('#add_license_danger').length) {
		// ユーザーIDを編集不可にする
		$('#user_id').attr('readonly', true);
		// ボタンを「更新」に変更
		$('#reflect').attr('name', 'update');
		$('#reflect').html("<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span>&thinsp;更新");
		// パスワードエラーカウント表示を50ミリ秒遅らせる(画面チラつき防止)
		setTimeout(function(){$('#disPwdErrcnt').collapse('show')}, 50);
	}else {
		// 新規時
		// パスワードエラーカウントを0に設定する
		$('#pwd_errcnt').val(0);
	}

	if ($('#add_danger').length) {
		// ユーザーID入力欄をエラーにする
		$('#user_id').addClass("error");
	}

	// 更新ユーザのライブラリ権限を閲覧ライブラリーツリーにチェック
	checkLibraryAuth();

});

/* ディレクトリツリーデータ並べ替え(sort)  */
function sortTreeData(jsonObj) {
	for (var key in jsonObj){
		if (typeof jsonObj[key] == "object") {
			if(Array.isArray(jsonObj[key])) {
				// 連想配列の場合
				// nodesをsort
				jsonObj[key].sort(function(o, next_o) {
					if (o.disNo != 0 && o.disNo != null && next_o.disNo != 0 && next_o.disNo != null) {
						// 表示順 - 昇順に並べ替え
						return (o.disNo < next_o.disNo ? -1 : 1);
					}else {
						switch (Cookies.get('reiss_sortOrder')) {
						case "ascSort":
							// 昇順に並べ替え
							return (o.text < next_o.text ? -1 : 1);
						case "descSort":
							// 降順に並べ替え
							return (o.text > next_o.text ? -1 : 1);
						default:
							// 降順に並べ替え
							return (o.text > next_o.text ? -1 : 1);
						}
					}
				})
				// forEachで要素ごとにに再帰呼び出し
				jsonObj[key].forEach(function(item){
					sortTreeData(item) ;
				});
			}else {
				// 配列はそのまま再帰呼び出し
				sortTreeData(jsonObj[key]) ;
			}
		}
	}
	return jsonObj;
}

/* 選択されている行を返却する */
function getSelectedRow(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}

/* 帳票出力権限チェック時 */
function checkOutputAuth() {
	// 帳票権限表示チェック
	if ($("#output_auth").prop("checked") == true) {
		$('#disReportAuth').collapse('show');
	}else {
		$('#disReportAuth').collapse('hide');
	}
}

/* 選択ユーザの帳票権限を取得 */
function getReportAuth(userId) {

	// 帳票権限チェックをすべて外す
	$('input[name="report_auth"]').prop('checked',false);

	try {
		$.ajax({
			type        : "POST",
			url         : "mng_userinfo.html?getReportAuth",
			data        : {acc_userID: $("#acc_userID").val(), userId: userId},
			dataType    : "JSON"
		})
		// 通信成功
		.done(function(result) {
			if (null != result) {
				for (var i = 0; i < result.length; i++) {
					if (result[i].report_id != null){
						$('input[value="' + result[i].report_id + '"]').prop('checked',true);
					}
				}
			}else {
				// エラー発生
				swalAlert.fire({html: "エラーが発生しました。"});
			}
		})
		// 通信失敗
		.fail(function(XMLHttpRequest, textStatus, errorThrown) {
			if (XMLHttpRequest.status == 200) {
				if ("" == XMLHttpRequest.responseText || null == XMLHttpRequest.responseText) {
					// 帳票権限無し
				}else {
					// 例外エラー表示
					document.write(XMLHttpRequest.responseText);
					document.close();
				}
			}else {
				console.log("ajax通信に失敗しました");
				console.log("XMLHttpRequest : " + XMLHttpRequest.status);
				console.log("textStatus     : " + textStatus);
				console.log("errorThrown    : " + errorThrown.message);
				swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
			}
		});
	}catch(e) {
		// エラー発生
		swalAlert.fire({html: "エラーが発生しました。"});
	}

}

/* 選択ユーザのライブラリ権限を取得 */
function getLibraryAuth(userId) {

	// 閲覧ライブラリツリーのチェックをすべて外す
	$('#tree').treeview('uncheckAll', { silent: false });
	// 閲覧ライブラリツリーを全て折りたたむ
	$('#tree').treeview('collapseAll', { silent: true });

	try {
		$.ajax({
			type        : "POST",
			url         : "mng_userinfo.html?getLibraryAuth",
			data        : {acc_userID: $("#acc_userID").val(), userId: userId},
			dataType    : "JSON"
		})
		// 通信成功
		.done(function(result) {
			if (null != result) {
				var checkNodeId
				for (var i = 0; i < result.length; i++) {
					if (result[i].lib_id != null){
						// lib_idの存在確認
						if ($('#tree').treeview('searchDirId', result[i].lib_id)[0]) {
							checkNodeId = $('#tree').treeview('searchDirId', result[i].lib_id)[0].nodeId;		// lib_idからnodeIdを取得
							$('#tree').treeview('revealNode', [ checkNodeId, { silent: true } ]);				// nodeIdまで閲覧ライブラリツリーを開く
							$('#tree').treeview('checkNode', [ checkNodeId, { silent: false } ]);				// nodeIdから閲覧ライブラリツリーをチェック
						}
					}
				}
			}else {
				// エラー発生
				swalAlert.fire({html: "エラーが発生しました。"});
			}
		})
		// 通信失敗
		.fail(function(XMLHttpRequest, textStatus, errorThrown) {
			if (XMLHttpRequest.status == 200) {
				if ("" == XMLHttpRequest.responseText || null == XMLHttpRequest.responseText) {
					// ライブラリ権限無し
				}else {
					// 例外エラー表示
					document.write(XMLHttpRequest.responseText);
					document.close();
				}
			}else {
				console.log("ajax通信に失敗しました");
				console.log("XMLHttpRequest : " + XMLHttpRequest.status);
				console.log("textStatus     : " + textStatus);
				console.log("errorThrown    : " + errorThrown.message);
				swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
			}
		});
	}catch(e) {
		// エラー発生
		swalAlert.fire({html: "エラーが発生しました。"});
	}

}

/* 更新ユーザのライブラリ権限を閲覧ライブラリーツリーにチェック */
function checkLibraryAuth() {

	if ($('#lib_id').val() != "") {
		var formLibId = $('#lib_id').val().split(',');
		var checkNodeId
		for (var i = 0; i < formLibId.length; i++) {
			if (formLibId[i] != null && formLibId[i] != ""){
				// lib_idの存在確認
				if ($('#tree').treeview('searchDirId', formLibId[i])[0]) {
					checkNodeId = $('#tree').treeview('searchDirId', formLibId[i])[0].nodeId;			// lib_idからnodeIdを取得
					$('#tree').treeview('revealNode', [ checkNodeId, { silent: true } ]);				// nodeIdまで閲覧ライブラリツリーを開く
					$('#tree').treeview('checkNode', [ checkNodeId, { silent: false } ]);				// nodeIdから閲覧ライブラリツリーをチェック
				}
			}
		}
	}

}

/* 閲覧ライブラリーツリー子チェック外し処理 */
function uncheckChild(node) {

	// チェックされていればチェック外し
	if (node.state.checked) {
		// チェックを外したdirIdを配列から除去
		libTreeCheck = libTreeCheck.filter(function(libTreeCheck) {
			return libTreeCheck !== node.dirId
		});
		// チェック外しはサイレント
		$('#tree').treeview('uncheckNode', [ node.nodeId, { silent: true } ]);
	}

	// ノードに子が存在する場合
	if ($('#tree').treeview('getNode', node.nodeId).nodes) {
		// 子ノードのチェックを外す
		for (var i = 0; i < $('#tree').treeview('getNode', node.nodeId).nodes.length; i++) {
			uncheckChild($('#tree').treeview('getNode', node.nodeId).nodes[i]);
		}
	}

}
