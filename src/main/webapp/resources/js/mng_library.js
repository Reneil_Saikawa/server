var validator;
var oTable;

$(function(){
	/* ブラウザ戻るボタン抑止 */
	history.forward();

	/* バリデーション */
	validator = $('form').validate({
		rules: {
			parent_lid:{
				compareLibId: 'lib_id'
			},
			lib_id:{
				required: true,
				numAlphaSymbol: true
			},
			lib_name:{
				required: true,
				cFileName: true,
				exFileName: true
			},
			live_room: {
				numAlphaSymbol: true
			},
			display_no: {
				cNumber: true
			}
		},
		messages: {
			parent_lid:{
				compareLibId: compareLibId
			},
			lib_id:{
				required: required,
				numAlphaSymbol: numAlphaSymbol
			},
			lib_name:{
				required: required,
				cFileName: cFileName,
				exFileName: exFileName
			},
			live_room: {
				numAlphaSymbol: numAlphaSymbol
			},
			display_no: {
				cNumber: number
			}
		}
	});

	/* リストの行クリック時 */
	$('#dataTable tbody tr').click(function(event) {
		if ($(this).hasClass('row_selected')) {
			// 選択行をクリック（行の選択を解除）
			oTable.$(this).removeClass('row_selected');
			oTable.$(this).find('input[name=r_chk]').prop('checked',false);
		} else {
			// 非選択業をクリック（行を選択）
			// 一旦全ての行の選択を解除する
			oTable.$('.sel_row').removeClass('row_selected');
			oTable.$('.sel_row').find('input[name=r_chk]').prop('checked',false);
			// 行を選択
			oTable.$(this).addClass('row_selected');
			oTable.$(this).find('input[name=r_chk]').prop('checked',true);
		}
	} );

	/* dataTableの初期設定 */
	oTable = $('#dataTable').dataTable({
		"language": {						/* 日本語化 */
			"decimal": ".",
			"thousands": ",",
			"sProcessing": "処理中...",
			"sLengthMenu": "_MENU_ 件表示",
			"sZeroRecords": "データがありません。",
			"sInfo": " _TOTAL_ 件中 _START_ から _END_ まで表示中",
			"sInfoEmpty": " 0 件中 0 から 0 まで表示",
			"sInfoFiltered": "（全 _MAX_ 件より抽出）",
			"sInfoPostFix": "",
			"sSearch": "検索:",
			"sUrl": "",
			"oPaginate": {
			"sFirst": "先頭",
			"sPrevious": "<",
			"sNext": ">",
			"sLast": "最終"
			}
		},
		//"sPaginationType": "full_numbers",	/* two_button:進むと戻るのみ, full_numbers:全ボタン表示 */
		"asStripeClasses": ['even','even'],	/* odd, even にすると1行置きに色が変わる(odd, evenはcssで設定) */
		"bAutoWidth": true,					/* 列幅自動調整機能 */
		"sDom": "lfrtip",					/* 列幅変更機能有効(パラメータ不明) */
		"bFilter": true,					/* フィルター機能 */
		"bPaginate": true,					/* ページング機能 */
		"bInfo": true,						/* ページ情報(n件中aからbまで表示) */
		"bLengthChange": true,				/* 1ページの表示件数切替 */
		"aLengthMenu": [10,20,50,100],		/* 1ページの表示件数切替コンボボックスソース */
		"iDisplayLength": 10,				/* 1ページの表示件数初期値 */
		"sScrollX": "",						/* 水平スクロール機能(幅をピクセル指定、空は無効) */
		"sScrollY": "",						/* 垂直スクロール機能(高さをピクセル指定、空は無効) */
		"bScrollCollapse": false,			/* 垂直スクロール有効時に表示幅固定 */
		"bSort": true,						/* 整列機能 */
		"aaSorting": [],					/* 整列初期状態(空[]は整列しない) */
		"aoColumnDefs": [ {
			"bSortable": false, "aTargets": [ 0 ]	/* 0カラム目の整列機能を無効にする */
		} ]
	} );

	// jsonをオブジェクトに変換
	var jsonObj = JSON.parse("[" + $("#libraryTreeData").val() + "]");
	// 最上位階層をソート
	jsonObj.sort(function(o, next_o) {
		if (o.disNo != 0 && o.disNo != null && next_o.disNo != 0 && next_o.disNo != null) {
			// 表示順 - 昇順に並べ替え
			return (o.disNo < next_o.disNo ? -1 : 1);
		}else {
			switch (Cookies.get('reiss_sortOrder')) {
			case "ascSort":
				// 昇順に並べ替え
				return (o.text < next_o.text ? -1 : 1);
			case "descSort":
				// 降順に並べ替え
				return (o.text > next_o.text ? -1 : 1);
			default:
				// 降順に並べ替え
				return (o.text > next_o.text ? -1 : 1);
			}
		}
	})
	// nodesをソート
	jsonObj = sortTreeData(jsonObj);
	// jsonオブジェクトを文字列に変換
	var jsonStr = JSON.stringify(jsonObj);
	// ライブラリツリーデータセット
	$("#libraryTreeData").val(jsonStr);

	$("#tree").treeview({
		data: jsonStr,
		expandIcon: 'glyphicon glyphicon-chevron-right',
		collapseIcon: 'glyphicon glyphicon-chevron-down',
		showCheckbox: true,
		//levels: 99,   // Cllapsed 階層リストがある場合開いた(-)状態で表示
		levels: 1,   // Cllapsed 階層リストがある場合閉じた(+)状態で表示
		showBorder: true,
		//color: "#A9BFD6",
		//backColor: "#494D51",
		//backColor: "#F1F1F1",
		onNodeSelected: function(event, node) {
			// 選択解除
			$('#tree').treeview('unselectNode', [ node.nodeId, { silent: true } ]);
			// 選択時チェック
			if (!node.state.checked) {
				$('#tree').treeview('checkNode', [ node.nodeId, { silent: false } ]);
			}else {
				$('#tree').treeview('uncheckNode', [ node.nodeId, { silent: false } ]);
			}
		}
	});

	// 親ライブラリツリーの最上位をチェック
	$('#tree').treeview('checkNode', [ 0, { silent: true } ]);

	// ツリーチェック
	$('#tree').on('nodeChecked', function(event, node) {
		$('#tree').treeview('uncheckAll', { silent: true });
		$('#tree').treeview('checkNode', [ node.nodeId, { silent: true } ]);
		$('#parent_lid').val(node.dirId);
		//ライブラリパス入力
		setLibName();
	});

	// ツリーチェック外し
	$('#tree').on('nodeUnchecked', function(event, node) {
		// チェックを外さない
		$('#tree').treeview('checkNode', [ node.nodeId, { silent: false } ]);
	});

	/* 一覧ボタンクリック時 */
	$('#list').click(function() {
		// リストを開く
		$('#listModal').modal();
	});

	// 削除クリック時
	$("#delete").click(function() {
		var selectedRow = getSelectedRow(oTable);
		if (selectedRow.length == 1) {
			// 選択が1行のとき
			swalConfirm.fire({
				html: "選択したライブラリを削除しますか？<br>配下のライブラリ・ファイルがすべて削除されます。"
			}).then(function(result) {
				if (result.value) {
					// ロード中画面表示
					$('#loading').css('visibility', 'visible');
					$.ajax({
						type: "POST",
						url: "mng_library.html?delete",
						data: {acc_userID: $("#acc_userID").val(), libId: selectedRow[0].cells.item(1).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, "")}
					})
					// 通信成功
					.done(function(result) {
						if (result.indexOf('<!DOCTYPE html>') !== -1 || result.indexOf('<!doctype html>') !== -1) {
							// 例外エラー表示
							document.write(result);
							document.close();
						}else if (result == "true") {
							// 削除成功
							swalSuccess.fire({html: "ライブラリを削除しました。"}).then(function(result) {
								swalInfo.fire({html: "画面を更新します。"}).then(function(result) {
									// リストを閉じる
									$('#listModal').modal('hide');
									// 再読み込み
									setTimeout(function() {
										window.location.href = 'mng_library.html';
									}, 150);
								});
							});
						}else {
							// 削除失敗（いくつかの削除が失敗）
							swalAlert.fire({html: "いくつかの削除が失敗しました。"}).then(function(result) {
								swalInfo.fire({html: "画面を更新します。"}).then(function(result) {
									// リストを閉じる
									$('#listModal').modal('hide');
									// 再読み込み
									setTimeout(function() {
										window.location.href = 'mng_library.html';
									}, 150);
								});
							});
						}
					})
					// 通信失敗
					.fail(function(XMLHttpRequest, textStatus, errorThrown) {
						if (XMLHttpRequest.status == 200) {
							if ("" != XMLHttpRequest.responseText && null != XMLHttpRequest.responseText) {
								// 例外エラー表示
								document.write(XMLHttpRequest.responseText);
								document.close();
							}
						}else {
							console.log("ajax通信に失敗しました");
							console.log("XMLHttpRequest : " + XMLHttpRequest.status);
							console.log("textStatus     : " + textStatus);
							console.log("errorThrown    : " + errorThrown.message);
							swalAlert.fire({html: "通信に失敗しました。<br>ネットワークの接続状態をご確認ください。"});
						}
						// ロード中画面非表示
						$('#loading').css('visibility', 'hidden');
					});
				}
			})
		}else {
			swalInfo.fire({html: "削除するライブラリを選択してください。"});
			// ロード中画面非表示
			$('#loading').css('visibility', 'hidden');
			return false;
		}

	});

	/* リストの編集ボタンクリック時 */
	$('#edit').click(function() {
		var selectedRow = getSelectedRow(oTable);
		if (selectedRow.length == 1) {
			// 選択が1行のとき

			// 親ライブラリツリーを全て折りたたむ
			$('#tree').treeview('collapseAll', { silent: true });
			// ライブラリIDから親ライブラリ表示の対象ライブラリを非活性化
			libIdDisableTree(selectedRow[0].cells.item(1).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			// ライブラリIDから親ライブラリ表示の対象ライブラリをチェック
			libIdCheckTree(selectedRow[0].cells.item(2).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			// リストの値をフォームに転記する
			$('#lib_id').val(selectedRow[0].cells.item(1).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#parent_lid').val(selectedRow[0].cells.item(2).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#lib_name').val(selectedRow[0].cells.item(3).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#lib_path').text(selectedRow[0].cells.item(4).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#live_room').val(selectedRow[0].cells.item(6).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));
			$('#display_no').val(selectedRow[0].cells.item(7).firstChild.nodeValue.replace(/(^\s+)|(\s+$)/g, ""));

			// ライブラリIDを編集不可にする
			$('#lib_id').attr('readonly', true);

			// バリデートをリセットする
			validator.resetForm();

			// ライブラリパスエラー表示を解除する
			$('#lib_path').removeClass("error");

			// メッセージを閉じる
			$('#success').alert('close');
			$('#id_add_danger').alert('close');
			$('#path_add_danger').alert('close');
			$('#up_danger').alert('close');

			// ボタンを「更新」に変更
			$('#reflect').attr('name', 'update');
			$('#reflect').html("<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span>&thinsp;更新");

			 // リストを閉じる
			$('#listModal').modal('hide');
		}else {
			swalInfo.fire({html: "編集するライブラリを選択してください。"});
			return false;
		}
	});

	/* クリアボタンクリック時 */
	$('#clearInputArea').click(function() {
		// メッセージを閉じる
		$('#success').alert('close');
		$('#id_add_danger').alert('close');
		$('#path_add_danger').alert('close');
		$('#up_danger').alert('close');
		// フォームをクリアする
		clearForm(this.form);
		// バリデートをリセットする
		validator.resetForm();
		// ライブラリパスエラー表示を解除する
		$('#lib_path').removeClass("error");
		// ライブラリパス表示をクリアする
		$('#lib_path').text("");
		// 親ライブラリツリーを全て活性化
		$('#tree').treeview('enableAll', { silent: true });
		// 親ライブラリツリーを全て折りたたむ
		$('#tree').treeview('collapseAll', { silent: true });
		// 親ライブラリツリーの最上位をチェック
		$('#tree').treeview('checkNode', [ 0, { silent: false } ]);
		// ライブラリIDを編集可にする
		$('#lib_id').attr('readonly', false);
		// ボタンを「追加」に変更
		$('#reflect').attr('name', 'add');
		$('#reflect').html("<span class='glyphicon glyphicon-plus' aria-hidden='true'></span>&thinsp;追加");
	});

	/* ライブラリ名変更時 */
	$('#lib_name').keyup(function() {
		//ライブラリパス入力
		setLibName();
	});

	/* ライブラリ名からフォーカスが外れた時 */
	$('#lib_name').focusout(function() {
		//ライブラリパス入力
		setLibName();
	});

	//ロード時ライブラリパス入力
	setLibName();

	// 追加・更新時処理
	if ($('#lib_id').val() != "" && $('#lib_id').val() != null && !($('#id_add_danger').length) && !($('#path_add_danger').length)) {
		// ライブラリIDから親ライブラリ表示の対象ライブラリを非活性化
		libIdDisableTree($('#lib_id').val());
		// ライブラリIDから親ライブラリ表示の対象ライブラリをチェック
		libIdCheckTree($('#parent_lid').val());
		// ライブラリIDを編集不可にする
		$('#lib_id').attr('readonly', true);
		// ボタンを「更新」に変更
		$('#reflect').attr('name', 'update');
		$('#reflect').html("<span class='glyphicon glyphicon-refresh' aria-hidden='true'></span>&thinsp;更新");
	}

	if ($('#id_add_danger').length) {
		// ライブラリIDから親ライブラリ表示の対象ライブラリを非活性化
		libIdDisableTree($('#lib_id').val());
		// ライブラリIDから親ライブラリ表示の対象ライブラリをチェック
		libIdCheckTree($('#parent_lid').val());
		// ライブラリID入力欄をエラーにする
		$('#lib_id').addClass("error");
	}

	if ($('#path_add_danger').length || $('#up_danger').length) {
		// ライブラリIDから親ライブラリ表示の対象ライブラリを非活性化
		libIdDisableTree($('#lib_id').val());
		// ライブラリIDから親ライブラリ表示の対象ライブラリをチェック
		libIdCheckTree($('#parent_lid').val());
		// 親ライブラリをエラーにする
		$('#tree').addClass("error");
		// ライブラリ名をエラーにする
		$('#lib_name').addClass("error");
		// ライブラリパスをエラーにする
		$('#lib_path').addClass("error");
	}

});

/* ディレクトリツリーデータ並べ替え(sort)  */
function sortTreeData(jsonObj) {
	for (var key in jsonObj){
		if (typeof jsonObj[key] == "object") {
			if(Array.isArray(jsonObj[key])) {
				// 連想配列の場合
				// nodesをsort
				jsonObj[key].sort(function(o, next_o) {
					if (o.disNo != 0 && o.disNo != null && next_o.disNo != 0 && next_o.disNo != null) {
						// 表示順 - 昇順に並べ替え
						return (o.disNo < next_o.disNo ? -1 : 1);
					}else {
						switch (Cookies.get('reiss_sortOrder')) {
						case "ascSort":
							// 昇順に並べ替え
							return (o.text < next_o.text ? -1 : 1);
						case "descSort":
							// 降順に並べ替え
							return (o.text > next_o.text ? -1 : 1);
						default:
							// 降順に並べ替え
							return (o.text > next_o.text ? -1 : 1);
						}
					}
				})
				// forEachで要素ごとにに再帰呼び出し
				jsonObj[key].forEach(function(item){
					sortTreeData(item) ;
				});
			}else {
				// 配列はそのまま再帰呼び出し
				sortTreeData(jsonObj[key]) ;
			}
		}
	}
	return jsonObj;
}

/* 選択されている行を返却する */
function getSelectedRow(oTableLocal) {
	return oTableLocal.$('tr.row_selected');
}

/* ライブラリパス入力 */
function setLibName() {
	//更新ボタン活性化
	$("#reflect").attr("disabled",false);
	//親ライブラリのvalue値を変数に格納
    var parent_id = $('#parent_lid').val();
    //不可視化している親ライブラリの選択
    $('#parent_path').val(parent_id);
    //選択されているテキストを変数に格納
	var path = $('#parent_path option:selected').text();
	//ライブラリ名の文字数制限設定
	var maxlength = 119 - path.length;
	if (maxlength > 50) {
		maxlength = 50;
	}else if (maxlength <= 0) {
		//更新ボタン不活性化
		$("#reflect").attr("disabled",true);
		$('#tree').addClass("error");
		swalAlert.fire({html: "選択した親ライブラリには、これ以上ライブラリを追加できません。"});
		maxlength = 0;
	}
	$("#lib_name").attr("maxlength",maxlength);
	//ライブラリパスを変更
	$('#lib_path').text(path + $('#lib_name').val());
}

/* ライブラリIDから親ライブラリ表示の対象ライブラリを非活性化 */
function libIdDisableTree(libId) {
	$('#tree').treeview('enableAll', { silent: true });
	if ($('#tree').treeview('searchDirId', libId)[0]) {
		var disableNodeId = $('#tree').treeview('searchDirId', libId)[0].nodeId;		// libIdからnodeIdを取得
		$('#tree').treeview('disableNode', [ disableNodeId, { silent: true } ]);		// nodeIdのツリー要素を非活性化
	}
}

/* ライブラリIDから親ライブラリ表示の対象ライブラリをチェック */
function libIdCheckTree(libId) {
	if ($('#tree').treeview('searchDirId', libId)[0]) {
		var checkNodeId = $('#tree').treeview('searchDirId', libId)[0].nodeId;			// libIdからnodeIdを取得
		$('#tree').treeview('revealNode', [ checkNodeId, { silent: true } ]);			// nodeIdまでツリーを開く
		$('#tree').treeview('checkNode', [ checkNodeId, { silent: false } ]);			// nodeIdのツリー要素をチェック
	}
}
