/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/03      1.0.1           女屋            receiveContentsエラー時動作強化
 *    3    2019/10/23      1.0.2           女屋            不要メンバ変数削除
 *    4    2020/03/03      1.0.3           斎川            ffmpegのLinux対応
 *    5    2020/03/03      1.0.4           女屋            パトライトのLinux対応
 */
package jp.reneil.reiss.terminal;

import java.awt.Dimension;
import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.imaging.formats.jpeg.exif.ExifRewriter;
import org.apache.commons.imaging.formats.tiff.write.TiffOutputSet;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.dao.AdditionalInfoDao;
import jp.reneil.reiss.dao.AdditionalItemDao;
import jp.reneil.reiss.dao.CodeDao;
import jp.reneil.reiss.dao.ContentsDao;
import jp.reneil.reiss.dao.LibraryDao;
import jp.reneil.reiss.dao.ReceiveLogDao;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.TenantDao;
import jp.reneil.reiss.dao.TerminalDao;
import jp.reneil.reiss.model.AdditionalInfoModel;
import jp.reneil.reiss.model.AdditionalItemModel;
import jp.reneil.reiss.model.CodeModel;
import jp.reneil.reiss.model.ContentsModel;
import jp.reneil.reiss.model.LibraryModel;
import jp.reneil.reiss.model.ReceiveLogModel;
import jp.reneil.reiss.model.SystemInfoModel;
import jp.reneil.reiss.model.TenantModel;
import jp.reneil.reiss.model.TerminalModel;
import jp.reneil.reiss.util.FileUtil;
import jp.reneil.reiss.util.ListThumbnail;
import jp.reneil.reiss.util.PatLite;
import jp.reneil.reiss.util.PlatformUtil;
import jp.reneil.reiss.util.TransFormat;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * コンテンツ情報受信クラスです。<br>
 *
 * @version 1.0.4
 */
@Controller
public class Reception {

	/* ログ制御 */
	private static Log log = LogFactory.getLog(Reception.class);

	/** 受信ログテーブルアクセス制御クラス */
	@Autowired
	private ReceiveLogDao receiveLogDao;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** コードテーブルアクセス制御クラス */
	@Autowired
	private CodeDao codeDao;

	/** テナント情報テーブルアクセス制御クラス */
	@Autowired
	private TenantDao tenantDao;

	/** 端末情報テーブルアクセス制御クラス */
	@Autowired
	private TerminalDao terminalDao;

	/** ライブラリ情報テーブルアクセス制御クラス */
	@Autowired
	private LibraryDao libraryDao;

	/** コンテンツテーブルアクセス制御クラス */
	@Autowired
	private ContentsDao contentsDao;

	/** 付帯情報項目名テーブルアクセス制御クラス */
	@Autowired
	private AdditionalItemDao additionalItemDao;

	/** 付帯情報テーブルアクセス制御クラス */
	@Autowired
	private AdditionalInfoDao additionalInfoDao;

	/** ファイルユーティリティ */
	FileUtil fileUtil = new FileUtil();

	/**
	 * コンテンツを受信します．<br>
	 * @param TENANT_ID テナントID
	 * @param TERM_ID 端末ID
	 * @param FILE_FORMAT ファイルフォーマットID(0401:jpg  0402:mp4  0403:jpg  0404:webm)
	 * @param PHOTO_DATE 撮影日時(yyyyMMddHHmmssSSS)
	 * @param LOCATE_LAT 緯度
	 * @param LOCATE_LON 経度
	 * @param LOCATE 住所
	 * @param contentsData 受信ファイル
	 * @return 処理結果情報
	 * @throws Exception
	 */
	@CrossOrigin
	@RequestMapping(value = "sendContents", method = RequestMethod.POST)
	@ResponseBody
	public String receiveContents(@RequestParam String TENANT_ID, String TERM_ID, String FILE_FORMAT, String PHOTO_DATE, String LOCATE_LAT, String LOCATE_LON, String LOCATE, @RequestParam("contentsData") MultipartFile contentsData, HttpServletRequest request) throws Exception {

		System.out.println("●コンテンツ受信開始");
		log.info("@@Reception.receiveContents ●コンテンツ受信開始");
		log.info("@@Reception.receiveContents 送信元IP = " + request.getRemoteAddr());

		log.info("@@Reception.receiveContents コンテンツ受信 TENANT_ID = " + TENANT_ID);
		log.info("@@Reception.receiveContents 　　　　　　　 TERM_ID = " + TERM_ID);
		log.info("@@Reception.receiveContents 　　　　　　　 FILE_FORMAT = " + FILE_FORMAT);
		log.info("@@Reception.receiveContents 　　　　　　　 PHOTO_DATE = " + PHOTO_DATE);
		log.info("@@Reception.receiveContents 　　　　　　　 LOCATE_LAT = " + LOCATE_LAT);
		log.info("@@Reception.receiveContents 　　　　　　　 LOCATE_LON = " + LOCATE_LON);
		log.info("@@Reception.receiveContents 　　　　　　　 LOCATE = " + LOCATE);
		log.info("@@Reception.receiveContents 　　　　　　　 contentsData_SIZE = " + contentsData.getSize());

		//受信ログ準備
		ReceiveLogModel receiveLogModel = new ReceiveLogModel();
		receiveLogModel.setSender_ip(request.getRemoteAddr());
		receiveLogModel.setTenant_id(TENANT_ID);
		receiveLogModel.setTerm_id(TERM_ID);
		receiveLogModel.setRecv_division(CommonConst.RECEPTION_RECEIVECONTENTS);
		receiveLogModel.setStart_date(new Date());
		//処理内容
		String processing = "データサイズ = " + contentsData.getSize() + "byte";
		//エラーメッセージ
		String errMessage = "";
		//処理結果通知
		String rtnMessage = "";

		try {

			//システム情報を取得
			SystemInfoModel systeminfoModel = systemInfoDao.findAll();
			String strSystemRoot = systeminfoModel.getSystem_rootpath();

			//システム情報からパトライトコマンドを生成
			String patLiteCommand = strSystemRoot + CommonConst.YEN_MRK + CommonConst.PATLITE;
			// Linux版対応 on 2020.03.03
			if (PlatformUtil.isLinux()) {
				patLiteCommand = CommonConst.PATLITECOM;
			}

			//テナント情報を取得
			TenantModel tenantModel = new TenantModel();

			//テナントが有効か確認
			if (tenantDao.auth(TENANT_ID) == 0) {
				System.out.println("有効テナント情報無し");
				log.info("@@Reception.receiveContents テナント確認 = NG");
				log.info("@@Reception.receiveContents 有効テナント情報が存在しません。");
				log.info("@@Reception.receiveContents 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "\nテナント確認 = NG";
				processing += "\n\n異常終了";
				errMessage += "有効テナント情報が存在しません。";
				rtnMessage = "Tenant not found";

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●コンテンツ受信終了");
				log.info("@@Reception.receiveContents ●コンテンツ受信終了");

				return rtnMessage;
			}

			tenantModel = tenantDao.findTenant(TENANT_ID);

			//テナント確認は受信ログに出力しない
			log.info("@@Reception.receiveContents テナント確認 = OK");

			//端末が有効か確認
			if(terminalDao.auth(TENANT_ID, TERM_ID) == 0) {
				System.out.println("有効端末情報無し");
				log.info("@@Reception.receiveContents 端末確認 = NG");
				log.info("@@Reception.receiveContents 有効端末情報が存在しません。");
				log.info("@@Reception.receiveContents 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "\n端末確認 = NG";
				processing += "\n\n異常終了";
				errMessage += "有効端末情報が存在しません。";
				rtnMessage = "Terminal not found";

				if (!StringUtils.isEmpty(tenantModel.getStr_patlite_enable()) && !StringUtils.isEmpty(tenantModel.getPatLite_option_warm()) && tenantModel.getPatLite_time_warm() != 0) {
					//パトライト-警報発報
					PatLite.call(patLiteCommand, tenantModel.getPatLite_ip(), tenantModel.getPatLite_login_name(), tenantModel.getPatLite_option_warm(), String.valueOf(tenantModel.getPatLite_time_warm()));
				}

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●コンテンツ受信終了");
				log.info("@@Reception.receiveContents ●コンテンツ受信終了");

				return rtnMessage;
			}

			log.info("@@Reception.receiveContents 端末確認 = OK");
			processing += "\n端末確認 = OK";

			String code_01 = FILE_FORMAT.substring(0, 2);
			String code_02 = FILE_FORMAT.substring(2, 4);

			//ファイルフォーマットからコード情報を取得
			CodeModel codeModel = codeDao.getCodeData(code_01, code_02);

			if (!codeModel.getName_01().equals("静止画") && !codeModel.getName_01().equals("動画") && !codeModel.getName_01().equals("ライブキャプチャー") && !codeModel.getName_01().equals("ライブ録画")) {
				System.out.println("ファイル種別無し");
				log.info("@@Reception.receiveContents ファイル種別 = NG");
				log.info("@@Reception.receiveContents ファイル種別が存在しません。");
				log.info("@@Reception.receiveContents 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "\nファイル種別 = NG";
				processing += "\n\n異常終了";
				errMessage += "ファイル種別が存在しません。";
				rtnMessage = "Format not found";

				if (!StringUtils.isEmpty(tenantModel.getStr_patlite_enable()) && !StringUtils.isEmpty(tenantModel.getPatLite_option_warm()) && tenantModel.getPatLite_time_warm() != 0) {
					//パトライト-警報発報
					PatLite.call(patLiteCommand, tenantModel.getPatLite_ip(), tenantModel.getPatLite_login_name(), tenantModel.getPatLite_option_warm(), String.valueOf(tenantModel.getPatLite_time_warm()));
				}

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●コンテンツ受信終了");
				log.info("@@Reception.receiveContents ●コンテンツ受信終了");

				return rtnMessage;
			}

			log.info("@@Reception.receiveContents ファイル種別 = OK");
			processing += "\nファイル種別 = OK";
			log.info("@@Reception.receiveContents ファイル種別 = " + codeModel.getName_01());
			processing += "\nファイル種別：" + codeModel.getName_01();

			//端末情報を取得
			TerminalModel terminalModel = terminalDao.findTerm(TENANT_ID, TERM_ID);
			log.info("@@Reception.receiveContents 端末名 = " + terminalModel.getTerm_name());

			//ライブラリ情報存在確認
			if(libraryDao.count(TENANT_ID,terminalModel.getLib_id(),"chk") == 0) {
				System.out.println("ライブラリ情報無し");
				log.info("@@Reception.receiveContents 保存先ライブラリ = NG");
				log.info("@@Reception.receiveContents 保存先ライブラリ情報が存在しません。");
				log.info("@@Reception.receiveContents 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "\n保存先ライブラリ = NG";
				processing += "\n\n異常終了";
				errMessage += "保存先ライブラリ情報が存在しません。";
				rtnMessage = "Library not found";

				if (!StringUtils.isEmpty(tenantModel.getStr_patlite_enable()) && !StringUtils.isEmpty(tenantModel.getPatLite_option_warm()) && tenantModel.getPatLite_time_warm() != 0) {
					//パトライト-警報発報
					PatLite.call(patLiteCommand, tenantModel.getPatLite_ip(), tenantModel.getPatLite_login_name(), tenantModel.getPatLite_option_warm(), String.valueOf(tenantModel.getPatLite_time_warm()));
				}

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●コンテンツ受信終了");
				log.info("@@Reception.receiveContents ●コンテンツ受信終了");

				return rtnMessage;
			}

			log.info("@@Reception.receiveContents 保存先ライブラリ = OK");
			processing += "\n保存先ライブラリ = OK";

			// ライブラリ情報を取得
			LibraryModel libraryModel = libraryDao.findLib(TENANT_ID, terminalModel.getLib_id());

			// 撮影日付ファイルパス作成
			String dateFolder = CommonConst.YEN_MRK;
			String year  = PHOTO_DATE.substring(0, 4);
			String month = PHOTO_DATE.substring(4, 6);
			String day   = PHOTO_DATE.substring(6, 8);
			// 撮影年月日-日本語表記チェック
			if (terminalModel.getJapanese_flg() == (byte) 1) {
				year  += "年";
				month += "月";
				day   += "日";
			}
			// 撮影日付フォルダパス作成
			if (terminalModel.getYear_folder_flg() == (byte) 1) {
				dateFolder += year;
				if (terminalModel.getMonth_folder_flg() == (byte) 1) {
					dateFolder += CommonConst.YEN_MRK + month;
					if (terminalModel.getDay_folder_flg() == (byte) 1) {
						dateFolder += CommonConst.YEN_MRK + day;
					}
				}else {
					if (terminalModel.getDay_folder_flg() == (byte) 1) {
						dateFolder += CommonConst.YEN_MRK;
						if (terminalModel.getJapanese_flg() == (byte) 1) {
							dateFolder += month + day;
						}else {
							dateFolder += month + CommonConst.HYPHEN + day;
						}
					}
				}
			}else {
				if (terminalModel.getJapanese_flg() == (byte) 1) {
					dateFolder += year + month;
				}else {
					dateFolder += year + CommonConst.HYPHEN + month;
				}
				if (terminalModel.getDay_folder_flg() == (byte) 1) {
					if (terminalModel.getMonth_folder_flg() == (byte) 1) {
						dateFolder += CommonConst.YEN_MRK + day;
					}else {
						if (terminalModel.getJapanese_flg() == (byte) 1) {
							dateFolder += day;
						}else {
							dateFolder += CommonConst.HYPHEN + day;
						}
					}
				}else {
					if (terminalModel.getMonth_folder_flg() == (byte) 0) {
						dateFolder = "";
					}
				}
			}

			// 格納パス
			String file_path= TENANT_ID + CommonConst.YEN_MRK + libraryModel.getLib_path() + dateFolder;
			processing += "\nファイル保存先：" + libraryModel.getLib_path() + dateFolder;

			log.info("@@Reception.receiveContents ファイル保存先 = " + file_path);

			// 登録番号/ファイル名作成
			String reg_no= "";
			String file_name= "";
			if (codeModel.getName_01().equals("静止画")) {
				reg_no = CommonConst.STILLIMAGE + TENANT_ID + CommonConst.UNDERSCORE + TERM_ID + CommonConst.UNDERSCORE + PHOTO_DATE.substring(0, 8)  + CommonConst.UNDERSCORE + PHOTO_DATE.substring(8, 17);
				file_name = reg_no + CommonConst.JPG;
			}else if (codeModel.getName_01().equals("動画")) {
				reg_no = CommonConst.MOVIE + TENANT_ID + CommonConst.UNDERSCORE + TERM_ID + CommonConst.UNDERSCORE + PHOTO_DATE.substring(0, 8)  + CommonConst.UNDERSCORE + PHOTO_DATE.substring(8, 17);
				file_name = reg_no + CommonConst.MP4;
			}else if (codeModel.getName_01().equals("ライブキャプチャー")) {
				reg_no = CommonConst.CAPTURE + TENANT_ID + CommonConst.UNDERSCORE + TERM_ID + CommonConst.UNDERSCORE + PHOTO_DATE.substring(0, 8)  + CommonConst.UNDERSCORE + PHOTO_DATE.substring(8, 17);
				file_name = reg_no + CommonConst.JPG;
			}else {
				reg_no = CommonConst.RECORDING + TENANT_ID + CommonConst.UNDERSCORE + TERM_ID + CommonConst.UNDERSCORE + PHOTO_DATE.substring(0, 8)  + CommonConst.UNDERSCORE + PHOTO_DATE.substring(8, 17);
				file_name = reg_no + CommonConst.WEBM;
			}

			receiveLogModel.setFile_name(file_name);
			log.info("@@Reception.receiveContents 登録番号 = " + reg_no);
			log.info("@@Reception.receiveContents ファイル名 = " + file_name);
			processing += "\nファイル名：" + file_name;

			// 登録番号でCONTENTSテーブルを検索
			if (contentsDao.findContents_regno(reg_no) != 0) {
				System.out.println("登録番号が存在します");
				log.info("@@Reception.receiveContents データベース登録 = NG");
				log.info("@@Reception.receiveContents データベースに同名ファイルが存在します。");
				log.info("@@Reception.receiveContents 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "\nデータベース登録 = NG";
				processing += "\n\n異常終了";
				errMessage += "データベースに同名ファイルが存在します。";
				rtnMessage = "Registration exists";

				if (!StringUtils.isEmpty(tenantModel.getStr_patlite_enable()) && !StringUtils.isEmpty(tenantModel.getPatLite_option_warm()) && tenantModel.getPatLite_time_warm() != 0) {
					//パトライト-警報発報
					PatLite.call(patLiteCommand, tenantModel.getPatLite_ip(), tenantModel.getPatLite_login_name(), tenantModel.getPatLite_option_warm(), String.valueOf(tenantModel.getPatLite_time_warm()));
				}

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●コンテンツ受信終了");
				log.info("@@Reception.receiveContents ●コンテンツ受信終了");

				return rtnMessage;
			}

			//ライブラリパス
			String lbraryPath = strSystemRoot + CommonConst.YEN_MRK + CommonConst.LIBRARY + CommonConst.YEN_MRK + file_path;

			//サムネイルパス
			String thumbnailPath = strSystemRoot + CommonConst.YEN_MRK + CommonConst.THUMBNAIL + CommonConst.YEN_MRK + file_path;

			//Tempパス
			String tempPath = strSystemRoot + CommonConst.YEN_MRK + CommonConst.TEMP;

			//ライブラリルートパス付ファイル名
			String contentsFilePath = lbraryPath + CommonConst.YEN_MRK + file_name;
			log.info("@@Reception.receiveContents ライブラリルートパス = " + contentsFilePath);

			//サムネイルルートパス付ファイル名
			String thumbnailFilePath = thumbnailPath + CommonConst.YEN_MRK + file_name.replaceAll(CommonConst.MP4, CommonConst.JPG).replaceAll(CommonConst.WEBM, CommonConst.JPG).replaceAll(CommonConst.JPG, CommonConst.THJPG);
			log.info("@@Reception.receiveContents サムネイルルートパス = " + thumbnailFilePath);

			//Tempルートパス付ファイル名
			String tempFilePath = tempPath + CommonConst.YEN_MRK + file_name.replaceAll(CommonConst.MP4, CommonConst.JPG).replaceAll(CommonConst.WEBM, CommonConst.JPG);
			log.info("@@Reception.receiveContents Tempルートパス = " + tempFilePath);

			//FFmpeg実行ファイル ルートパス設定
			String ffmpegRoot = strSystemRoot + CommonConst.YEN_MRK + CommonConst.FFMPEG;
			// Linux版対応 on 2020.03.03
			if (PlatformUtil.isLinux()) {
				ffmpegRoot = CommonConst.FFMPEGCOM;
			}
			log.info("@@Reception.receiveContents FFmpeg実行ファイル = " + ffmpegRoot);

			//サムネイル作成元ファイル名
			String thuInpFilePath = "";

			//ファイル拡張子
			String extension = "";

			//ライブラリフォルダが存在しない場合は作成する
			Path FILIB = Paths.get(lbraryPath);
			if (!Files.exists(FILIB)) {
				//ライブラリフォルダが存在しない場合
				//失敗したらリトライ
				for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
					try {
						Files.createDirectories(FILIB);
						if (Files.exists(FILIB)) {
							//ライブラリフォルダ作成に成功したらループから抜ける
							break;
						}else {
							//ライブラリフォルダ作成失敗
							log.info("@@Reception.receiveContents " + FILIB.toAbsolutePath().toString() + " ライブラリフォルダ作成失敗回数 = " + (i + 1));
							errMessage += "ライブラリフォルダ作成失敗回数 = " + (i + 1);
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}
					}catch (Exception e) {
						// ライブラリフォルダ作成失敗(例外)
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw);
						e.printStackTrace(pw);
						pw.flush();
						String str = sw.toString();
						log.info("@@Reception.receiveContents " + FILIB.toAbsolutePath().toString() + " ライブラリフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
						errMessage += "ライブラリフォルダ作成失敗回数(例外) = " + (i + 1) + "\n" + str;
						try
						{
							Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
						}
						catch(InterruptedException ex){}
					}
				}
			}

			//サムネイルフォルダが存在しない場合は作成する
			Path FITHU = Paths.get(thumbnailPath);
			if (!Files.exists(FITHU)) {
				//サムネイルフォルダが存在しない場合
				//失敗したらリトライ
				for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
					try {
						Files.createDirectories(FITHU);
						if (Files.exists(FITHU)) {
							//サムネイルフォルダ作成に成功したらループから抜ける
							break;
						}else {
							//サムネイルフォルダ作成失敗
							log.info("@@Reception.receiveContents " + FITHU.toAbsolutePath().toString() + " サムネイルフォルダ作成失敗回数 =" + (i + 1));
							errMessage += "\nサムネイルフォルダ作成失敗回数 = " + (i + 1);
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}
					}catch (Exception e) {
						// サムネイルフォルダ作成失敗(例外)
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw);
						e.printStackTrace(pw);
						pw.flush();
						String str = sw.toString();
						log.info("@@Reception.receiveContents " + FITHU.toAbsolutePath().toString() + " サムネイルフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
						errMessage += "\nサムネイルフォルダ作成失敗回数(例外) = " + (i + 1) + "\n" + str;
						try
						{
							Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
						}
						catch(InterruptedException ex){}
					}
				}
			}

			//ファイル正常生成チェック用
			boolean createFile = false;

			//MultipartFileからファイルを生成
			//失敗したらリトライ
			for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
				OutputStream os = null;
				InputStream is = null;
				Path convFile = Paths.get(contentsFilePath);
				try {
					//静止画・キャプチャーかつ位置情報有ならExif(位置情報)追加
					if ((codeModel.getName_01().equals("静止画") || codeModel.getName_01().equals("ライブキャプチャー")) &&
							!StringUtils.isEmpty(LOCATE_LAT) && !StringUtils.isEmpty(LOCATE_LON) && !LOCATE_LAT.equals("0.0") && !LOCATE_LON.equals("0.0")) {

						os = Files.newOutputStream(convFile);

						//メタデータのオブジェクト作成
						TiffOutputSet outputSet = new TiffOutputSet();
						final double latitude = Double.parseDouble(LOCATE_LAT);
						final double longitude = Double.parseDouble(LOCATE_LON);
						outputSet.setGPSInDegrees(longitude, latitude);

						new ExifRewriter().updateExifMetadataLossless(contentsData.getBytes(), os, outputSet);
						log.info("@@Reception.receiveContents Exif(位置情報)追加");

					}else {
						//動画・録画または位置情報無の場合
						is = new BufferedInputStream(contentsData.getInputStream());
						Files.copy(is, convFile, StandardCopyOption.REPLACE_EXISTING);
					}
					createFile = true;
				    log.info("@@Reception.receiveContents ファイル生成成功");
					//ファイル生成に成功したらループから抜ける
				    break;
				}catch (Exception e) {
					//ファイル作成失敗
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw);
					e.printStackTrace(pw);
					pw.flush();
					String str = sw.toString();
					log.info("@@Reception.receiveContents " + convFile.toAbsolutePath().toString() + " ファイル生成失敗回数(例外) = " + (i + 1) + "\n" + str);
					errMessage += "\nファイル生成失敗回数 = " + (i + 1) + "\n" + str;
					try
					{
						Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
					}
					catch(InterruptedException ex){}
				}finally {
					if (os != null) {
						os.flush();
						os.close();
					}
					if (is != null) {
						is.close();
					}
				}
			}

			// ファイル正常生成チェック
			Path contents = Paths.get(contentsFilePath);
			if (Files.exists(contents) && !createFile) {
				// 生成失敗ファイル削除
				deleteFile(contents);
			}

			// コンテンツファイル生成に成功したらDBにコンテンツ情報を登録
			if (Files.exists(contents) && !file_name.equals("") && createFile) {

				ContentsModel contentsModel = new ContentsModel();

		        // Date型変換
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");

				contentsModel.setReg_no(reg_no);
				contentsModel.setFile_format(FILE_FORMAT);
				contentsModel.setFile_path(file_path);
				contentsModel.setFile_name(file_name);
				contentsModel.setTerm_id(TERM_ID);
				contentsModel.setPhoto_date(sdf.parse(PHOTO_DATE));

				//LOCATE_LAT座標数値チェック
				if (Pattern.compile("^-?(0|[1-9]\\d*)(\\.\\d+|)$").matcher(LOCATE_LAT).find()) {
					contentsModel.setLocate_lat(Double.parseDouble(LOCATE_LAT));
				}else {
					contentsModel.setLocate_lat(Double.parseDouble("0"));
				}
				//LOCATE_LON座標数値チェック
				if (Pattern.compile("^-?(0|[1-9]\\d*)(\\.\\d+|)$").matcher(LOCATE_LON).find()) {
					contentsModel.setLocate_lon(Double.parseDouble(LOCATE_LON));
				}else {
					contentsModel.setLocate_lon(Double.parseDouble("0"));
				}

				contentsModel.setLocate(LOCATE);
				contentsModel.setUpdate_user_id(TERM_ID);

				contentsDao.insert(contentsModel);
				log.info("@@Reception.receiveContents 登録番号 = " + reg_no);
				log.info("@@Reception.receiveContents データベース登録 = OK");
				processing += "\nデータベース登録 = OK";
			}

			//動画・録画の場合FFMpeg変換処理（動画フォイル=>サムネイル用jpg作成）
			Path MOVFITHU = Paths.get(tempFilePath);
			if (codeModel.getName_01().equals("動画") || codeModel.getName_01().equals("ライブ録画")) {
				//動画の場合
				//FFMpeg変換処理（動画フォイル=>jpg作成）
				//失敗したらリトライ
				for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
					try {
						TransFormat.executeThumbnail(lbraryPath, file_name, tempPath, ffmpegRoot);
						if (Files.exists(MOVFITHU)) {
							//サムネイル作成用jpg作成に成功したらループから抜ける
							break;
						}else {
							//サムネイル作成用jpg作成失敗
							log.info("@@Reception.receiveContents " + MOVFITHU.toAbsolutePath().toString() + " サムネイル作成用jpg作成失敗回数 = " + (i + 1));
							errMessage += "\nサムネイル作成用jpg作成失敗回数 = " + (i + 1);
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}
					}catch (Exception e) {
						//サムネイル作成用jpg作成エラー
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw);
						e.printStackTrace(pw);
						pw.flush();
						String str = sw.toString();
						log.info("@@Reception.receiveContents " + MOVFITHU.toAbsolutePath().toString() + " サムネイル作成用jpg作成エラー回数(例外) = " + (i + 1) + "\n" + str);
						errMessage += "\nサムネイル作成用jpg作成エラー回数 = " + (i + 1) + "\n" + str;
						try
						{
							Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
						}
						catch(InterruptedException ex){}
					}
				}
				thuInpFilePath = tempFilePath;
				if (codeModel.getName_01().equals("動画")) {
					extension = CommonConst.MP4;
				}else {
					extension = CommonConst.WEBM;
				}
			}else {
				//静止画の場合
				thuInpFilePath = contentsFilePath;
				extension = CommonConst.JPG;
			}

			//サムネイル用jpg=>サムネイル作成
			Path thuInpFile = Paths.get(thuInpFilePath);
			if (Files.exists(thuInpFile)) {
			   	//サムネイル作成クラス
		    	ListThumbnail LS;
				//サムネイル作成
				//失敗したらリトライ
				for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
			    	//サムネイルサイズ（横）
			    	short thumbnail_width;
			    	//サムネイルサイズ（縦）
			    	short thumbnail_height;
					//画像サイズ比率からサムネイルサイズを判定
					try {
						Dimension size = fileUtil.getSize(thuInpFile);
						Float wrkX = Float.valueOf(size.width);
						Float wrkY = Float.valueOf(size.height);
						Float wrk1 = wrkX / wrkY;

						if ( wrk1 > 1.2 ) {
							//横撮影サムネイル(幅×高さ)
							thumbnail_width = tenantModel.getThumbnail_width();
							thumbnail_height = tenantModel.getThumbnail_height();
						}else if ( wrk1 < 0.8 ) {
							//縦撮影サムネイル(高さ×幅)
							thumbnail_width = tenantModel.getThumbnail_height();
							thumbnail_height = tenantModel.getThumbnail_width();
						}else {
							//正方形サムネイル(幅×幅)
							thumbnail_width = tenantModel.getThumbnail_width();
							thumbnail_height = tenantModel.getThumbnail_width();
						}

					}catch (Exception e1) {
						//エラー時は標準サイズ(幅×高さ)
						thumbnail_width = tenantModel.getThumbnail_width();
						thumbnail_height = tenantModel.getThumbnail_height();
						log.info("@@Reception.receiveContents サムネイルサイズ取得失敗");
						errMessage += "\n閲覧サイズ取得失敗";
						e1.printStackTrace();
					}

					log.info("@@Reception.receiveContents サムネイルサイズ = " + thumbnail_width + " × " + thumbnail_height);

					Path JPGFITHU = Paths.get(thumbnailFilePath);
					try {
						//シュリンク実行
						LS = new ListThumbnail(thuInpFilePath, thumbnailFilePath, extension);
						LS.ShrinkJpeg(thumbnail_width, thumbnail_height);
						if (Files.exists(JPGFITHU)) {
							//動画・録画ならTempファイル削除
							if (extension.equals(CommonConst.MP4) || extension.equals(CommonConst.WEBM)) {
								deleteFile(MOVFITHU);
							}
							//サムネイル作成に成功したらループから抜ける
							break;
						}else {
							//サムネイル作成失敗
							log.info("@@Reception.receiveContents " + JPGFITHU.toAbsolutePath().toString() + " サムネイル作成失敗回数 =" + (i + 1));
							errMessage += "\nサムネイル作成失敗回数 = " + (i + 1);
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}
					}catch (Exception e) {
						//サムネイル作成エラー
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw);
						e.printStackTrace(pw);
						pw.flush();
						String str = sw.toString();
						log.info("@@Reception.receiveContents " + JPGFITHU.toAbsolutePath().toString() + " サムネイル作成エラー回数(例外) = " + (i + 1) + "\n" + str);
						errMessage += "\nサムネイル作成エラー回数 = " + (i + 1);
						try
						{
							Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
						}
						catch(InterruptedException ex){}
					}
				}
			}else {
				//サムネイル作成用jpg作成失敗
				log.info("@@Reception.receiveContents サムネイル作成用jpg作成失敗");
				errMessage += "\nサムネイル作成用jpg作成失敗";
			}

			//ファイル登録確認
			Path contentsFile = Paths.get(contentsFilePath);
			Path thumbnailFile = Paths.get(thumbnailFilePath);
			Path tempFile = Paths.get(tempFilePath);
			if (contentsDao.findContents_regno(reg_no) == 0 || !Files.exists(contentsFile) || !Files.exists(thumbnailFile) || Files.exists(tempFile)) {
				//ファイル登録失敗
				if (contentsDao.findContents_regno(reg_no) != 0) {
					//DBからコンテンツ情報削除
					contentsDao.deleteContents_regno(reg_no);
				}else {
					log.info("@@Reception.receiveContents データベース登録失敗");
					errMessage += "\nデータベース登録失敗";
				}
				if (Files.exists(contentsFile)) {
					//コンテンツファイル削除
					deleteFile(contentsFile);
				}else {
					log.info("@@Reception.receiveContents コンテンツファイル作成失敗");
					errMessage += "\nコンテンツファイル作成失敗";
				}
				if (Files.exists(thumbnailFile)) {
					//サムネイルファイル削除
					deleteFile(thumbnailFile);
				}else {
					log.info("@@Reception.receiveContents サムネイルファイル作成失敗");
					errMessage += "\nサムネイルファイル作成失敗";
				}
				if (Files.exists(tempFile)) {
					//Tempファイル削除
					deleteFile(tempFile);
					log.info("@@Reception.receiveContents Tempファイル削除失敗");
					errMessage += "\n一時ファイル削除失敗";
				}
				System.out.println("コンテンツ登録エラー");
				log.info("@@Reception.receiveContents コンテンツ登録エラー");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "\n\n異常終了";
				errMessage += "\n\nコンテンツ登録エラー";
				rtnMessage = "Registration error";

				if (!StringUtils.isEmpty(tenantModel.getStr_patlite_enable()) && !StringUtils.isEmpty(tenantModel.getPatLite_option_err()) && tenantModel.getPatLite_time_err() != 0) {
					//パトライト-エラー発報
					PatLite.call(patLiteCommand, tenantModel.getPatLite_ip(), tenantModel.getPatLite_login_name(), tenantModel.getPatLite_option_err(), String.valueOf(tenantModel.getPatLite_time_err()));
				}

			}else {
				System.out.println("正常終了");
				log.info("@@Reception.receiveContents 正常終了");
				receiveLogModel.setResult_id(CommonConst.SUCCESS_CO);
				processing += "\n\n正常終了";
				rtnMessage = "Has completed";

				if (!StringUtils.isEmpty(tenantModel.getStr_patlite_enable()) && !StringUtils.isEmpty(tenantModel.getPatLite_option_rcv()) && tenantModel.getPatLite_time_rcv() != 0) {
					//パトライト-受信発報
					PatLite.call(patLiteCommand, tenantModel.getPatLite_ip(), tenantModel.getPatLite_login_name(), tenantModel.getPatLite_option_rcv(), String.valueOf(tenantModel.getPatLite_time_rcv()));
				}
			}

		}catch(Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			String str = sw.toString();
			System.out.println("例外エラー");
			log.info("@@Reception.receiveContents 例外エラー：\n" + str);
			receiveLogModel.setResult_id(CommonConst.FAILED_CO);
			processing += "\n\n異常終了";
			errMessage += "\n\n例外エラー：\n" + str;
			rtnMessage = "Exception error";
		}

		receiveLogModel.setContents(processing);
		receiveLogModel.setErr_msg(errMessage);
		receiveLogModel.setEnd_date(new Date());

		//受信ログ登録
		receiveLogDao.insert(receiveLogModel);

		System.out.println("●コンテンツ受信終了");
		log.info("@@Reception.receiveContents ●コンテンツ受信終了");

		return rtnMessage;
    }

	/**
	 * 付帯情報を受信します．<br>
	 * @param TENANT_ID テナントID
	 * @param TERM_ID 端末ID
	 * @param FILE_FORMAT ファイルフォーマットID(0401:jpg  0402:mp4  0403:jpg  0404:webm)
	 * @param PHOTO_DATE 撮影日時(yyyyMMddHHmmssSSS)
	 * @param ITEM_ID 付帯情報項目ID
	 * @param ITEM_VALUE 付帯情報
	 * @return 処理結果情報
	 * @throws Exception
	 */
	@RequestMapping(value = "sendAdditionalInfo", method = RequestMethod.POST)
	@ResponseBody
	public String receiveAdditionalInfo(@RequestParam String TENANT_ID, String TERM_ID, String FILE_FORMAT, String PHOTO_DATE, String ITEM_ID, String ITEM_VALUE, HttpServletRequest request) throws Exception {

		System.out.println("●付帯情報受信開始");
		log.info("@@Reception.receiveAdditionalInfo ●付帯情報受信開始");
		log.info("@@Reception.receiveAdditionalInfo 送信元IP = " + request.getRemoteAddr());

		log.info("@@Reception.receiveAdditionalInfo 付帯情報受信 TENANT_ID = " + TENANT_ID);
		log.info("@@Reception.receiveAdditionalInfo 　　　　　　 TERM_ID = " + TERM_ID);
		log.info("@@Reception.receiveAdditionalInfo 　　　　　　 FILE_FORMAT = " + FILE_FORMAT);
		log.info("@@Reception.receiveAdditionalInfo 　　　　　　 PHOTO_DATE = " + PHOTO_DATE);
		log.info("@@Reception.receiveAdditionalInfo 　　　　　　 ITEM_ID = " + ITEM_ID);
		log.info("@@Reception.receiveAdditionalInfo 　　　　　　 ITEM_VALUE = " + ITEM_VALUE);

		//受信ログ準備
		ReceiveLogModel receiveLogModel = new ReceiveLogModel();
		receiveLogModel.setSender_ip(request.getRemoteAddr());
		receiveLogModel.setTenant_id(TENANT_ID);
		receiveLogModel.setTerm_id(TERM_ID);
		receiveLogModel.setRecv_division(CommonConst.RECEPTION_RECEIVEADDITIONALINFO);
		receiveLogModel.setStart_date(new Date());
		//処理内容
		String processing = "";
		//エラーメッセージ
		String errMessage = "";
		//処理結果通知
		String rtnMessage = "";

		try {

			processing += "\n項目ID = " + ITEM_ID;
			processing += "\n内容 = " + ITEM_VALUE;

			//テナントが有効か確認
			if (tenantDao.auth(TENANT_ID) == 0) {
				System.out.println("有効テナント情報無し");
				log.info("@@Reception.receiveAdditionalInfo テナント確認 = NG");
				log.info("@@Reception.receiveAdditionalInfo 有効テナント情報が存在しません。");
				log.info("@@Reception.receiveAdditionalInfo 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "テナント確認 = NG";
				processing += "\n\n異常終了";
				errMessage += "有効テナント情報が存在しません。";
				rtnMessage = "Tenant not found";

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●付帯情報受信終了");
				log.info("@@Reception.receiveAdditionalInfo ●付帯情報受信終了");

				return rtnMessage;
			}

			//テナント確認は受信ログに出力しない
			log.info("@@Reception.receiveAdditionalInfo テナント確認 = OK");

			//端末が有効か確認
			if(terminalDao.auth(TENANT_ID, TERM_ID) == 0) {
				System.out.println("有効端末情報無し");
				log.info("@@Reception.receiveAdditionalInfo 端末確認 = NG");
				log.info("@@Reception.receiveAdditionalInfo 有効端末情報が存在しません。");
				log.info("@@Reception.receiveAdditionalInfo 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "端末確認 = NG";
				processing += "\n\n異常終了";
				errMessage += "有効端末情報が存在しません。";
				rtnMessage = "Terminal not found";

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●付帯情報受信終了");
				log.info("@@Reception.receiveAdditionalInfo ●付帯情報受信終了");

				return rtnMessage;
			}

			log.info("@@Reception.receiveAdditionalInfo 端末確認 = OK");
			processing += "\n端末確認 = OK";

			String code_01 = FILE_FORMAT.substring(0, 2);
			String code_02 = FILE_FORMAT.substring(2, 4);

			//ファイルフォーマットからコード情報を取得
			CodeModel codeModel = codeDao.getCodeData(code_01, code_02);

			if (!codeModel.getName_01().equals("静止画") && !codeModel.getName_01().equals("動画") && !codeModel.getName_01().equals("ライブキャプチャー") && !codeModel.getName_01().equals("ライブ録画")) {
				System.out.println("フォーマット区分無し");
				log.info("@@Reception.receiveAdditionalInfo ファイル種別 = NG");
				log.info("@@Reception.receiveAdditionalInfo ファイル種別が存在しません。");
				log.info("@@Reception.receiveAdditionalInfo 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "\nファイル種別 = NG";
				processing += "\n\n異常終了";
				errMessage += "ファイル種別が存在しません。";
				rtnMessage = "Format not found";

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●付帯情報受信終了");
				log.info("@@Reception.receiveAdditionalInfo ●付帯情報受信終了");

				return rtnMessage;
			}

			log.info("@@Reception.receiveAdditionalInfo ファイル種別 = OK");
			processing += "\nファイル種別 = OK";
			log.info("@@Reception.receiveAdditionalInfo ファイル種別 = " + codeModel.getName_01());
			processing += "\nファイル種別：" + codeModel.getName_01();

			//登録番号/ファイル名作成
			String reg_no= "";
			String file_name = "";
			if (codeModel.getName_01().equals("静止画")) {
				reg_no = CommonConst.STILLIMAGE + TENANT_ID + CommonConst.UNDERSCORE + TERM_ID + CommonConst.UNDERSCORE + PHOTO_DATE.substring(0, 8)  + CommonConst.UNDERSCORE + PHOTO_DATE.substring(8, 17);
				file_name = reg_no + CommonConst.JPG;
			}else if (codeModel.getName_01().equals("動画")) {
				reg_no = CommonConst.MOVIE + TENANT_ID + CommonConst.UNDERSCORE + TERM_ID + CommonConst.UNDERSCORE + PHOTO_DATE.substring(0, 8)  + CommonConst.UNDERSCORE + PHOTO_DATE.substring(8, 17);
				file_name = reg_no + CommonConst.MP4;
			}else if (codeModel.getName_01().equals("ライブキャプチャー")) {
				reg_no = CommonConst.CAPTURE + TENANT_ID + CommonConst.UNDERSCORE + TERM_ID + CommonConst.UNDERSCORE + PHOTO_DATE.substring(0, 8)  + CommonConst.UNDERSCORE + PHOTO_DATE.substring(8, 17);
				file_name = reg_no + CommonConst.JPG;
			}else {
				reg_no = CommonConst.RECORDING + TENANT_ID + CommonConst.UNDERSCORE + TERM_ID + CommonConst.UNDERSCORE + PHOTO_DATE.substring(0, 8)  + CommonConst.UNDERSCORE + PHOTO_DATE.substring(8, 17);
				file_name = reg_no + CommonConst.MP4;
			}

			receiveLogModel.setFile_name(file_name);
			log.info("@@Reception.receiveAdditionalInfo 登録番号 = " + reg_no);
			log.info("@@Reception.receiveAdditionalInfo ファイル名 = " + file_name);
			processing += "\nファイル名：" + file_name;

			// 登録番号と項目IDでADDITIONALINFOテーブルを検索
			if (additionalInfoDao.count(reg_no, ITEM_ID) != 0 ) {
				System.out.println("登録が存在します");
				log.info("@@Reception.receiveAdditionalInfo データベース登録 = NG");
				log.info("@@Reception.receiveAdditionalInfo データベースに同名ファイル/項目IDが存在します。");
				log.info("@@Reception.receiveAdditionalInfo 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "\nデータベース登録 = NG";
				processing += "\n\n異常終了";
				errMessage += "データベースに同名ファイル/項目IDが存在します。";
				rtnMessage = "Registration exists";

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●付帯情報受信終了");
				log.info("@@Reception.receiveAdditionalInfo ●付帯情報受信終了");

				return rtnMessage;
			}

			AdditionalInfoModel AddInfoModel = new AdditionalInfoModel();

			AddInfoModel.setReg_no(reg_no);
			AddInfoModel.setItem_id(ITEM_ID);
			AddInfoModel.setItem_value(ITEM_VALUE);
			AddInfoModel.setUpdate_user_id(TERM_ID);

			additionalInfoDao.insert(AddInfoModel);
			log.info("@@Reception.receiveAdditionalInfo データベース登録 = OK");
			processing += "\nデータベース登録 = OK";
			System.out.println("正常終了");
			log.info("@@Reception.receiveAdditionalInfo 正常終了");
			receiveLogModel.setResult_id(CommonConst.SUCCESS_CO);
			processing += "\n\n正常終了";
			rtnMessage = "Has completed";

		}catch(Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			String str = sw.toString();
			System.out.println("例外エラー");
			log.info("@@Reception.receiveAdditionalInfo 例外エラー：\n" + str);
			receiveLogModel.setResult_id(CommonConst.FAILED_CO);
			processing += "\n\n異常終了";
			errMessage += "\n\n例外エラー：\n" + str;
			rtnMessage = "Exception error";
		}

		receiveLogModel.setContents(processing);
		receiveLogModel.setErr_msg(errMessage);
		receiveLogModel.setEnd_date(new Date());

		//受信ログ登録
		receiveLogDao.insert(receiveLogModel);

		System.out.println("●付帯情報受信終了");
		log.info("@@Reception.receiveAdditionalInfo ●付帯情報受信終了");

		return rtnMessage;
    }

	/**
	 * 端末情報を送信します．<br>
	 * @param TENANT_ID テナントID
	 * @param TERM_ID 端末ID
	 * @return 処理結果情報
	 * @throws Exception
	 */
	@RequestMapping(value = "getTerminalInfo", method = RequestMethod.POST, produces="text/html;charset=UTF-8")
	@ResponseBody
	public String getTerminalInfo(@RequestParam String TENANT_ID, String TERM_ID, HttpServletRequest request) throws Exception {

		System.out.println("●端末情報要求開始");
		log.info("@@Reception.getTerminalInfo ●端末情報要求開始");
		log.info("@@Reception.getTerminalInfo 送信元IP = " + request.getRemoteAddr());

		log.info("@@Reception.getTerminalInfo 端末情報要求 TENANT_ID = " + TENANT_ID);
		log.info("@@Reception.getTerminalInfo 　　　　　　 TERM_ID = " + TERM_ID);

		//受信ログ準備
		ReceiveLogModel receiveLogModel = new ReceiveLogModel();
		receiveLogModel.setSender_ip(request.getRemoteAddr());
		receiveLogModel.setTenant_id(TENANT_ID);
		receiveLogModel.setTerm_id(TERM_ID);
		receiveLogModel.setRecv_division(CommonConst.RECEPTION_GETTERMINALINFO);
		receiveLogModel.setStart_date(new Date());
		//処理内容
		String processing = "";
		//エラーメッセージ
		String errMessage = "";
		//処理結果通知
		String rtnMessage = "";

		try {

			// テナントが存在するか確認
			if(tenantDao.auth(TENANT_ID) == 0) {
				System.out.println("有効テナント情報無し");
				log.info("@@Reception.getTerminalInfo テナント情報確認 = NG");
				log.info("@@Reception.getTerminalInfo 有効テナント情報が存在しません。");
				log.info("@@Reception.getTerminalInfo 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "テナント情報確認 = NG";
				processing += "\n\n異常終了";
				errMessage += "有効テナント情報が存在しません。";
				rtnMessage = "Tenant not found";

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●端末情報要求終了");
				log.info("@@Reception.getTerminalInfo ●端末情報要求終了");

				return rtnMessage;
			}

			//テナント確認は受信ログに出力しない
			log.info("@@Reception.getTerminalInfo テナント確認 = OK");

			//端末が存在するか確認(有効期間外の端末も含む)
			if(terminalDao.count(TENANT_ID, TERM_ID,"chk") == 0) {
				System.out.println("端末情報無し");
				log.info("@@Reception.getTerminalInfo 端末確認 = NG");
				log.info("@@Reception.getTerminalInfo 端末情報が存在しません。");
				log.info("@@Reception.getTerminalInfo 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "端末確認 = NG";
				processing += "\n\n異常終了";
				errMessage += "端末情報が存在しません。";
				rtnMessage = "Terminal not found";

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●端末情報要求終了");
				log.info("@@Reception.getTerminalInfo ●端末情報要求終了");

				return rtnMessage;
			}

			log.info("@@Reception.getTerminalInfo 端末確認 = OK");
			processing += "\n端末確認 = OK";

			//端末情報を取得
			TerminalModel terminalModel = terminalDao.findTerm(TENANT_ID, TERM_ID);

			//ライブラリ情報存在確認
			if(libraryDao.count(TENANT_ID,terminalModel.getLib_id(),"chk") == 0) {
				System.out.println("ライブラリ情報無し");
				log.info("@@Reception.getTerminalInfo 保存先ライブラリ情報確認 = NG");
				log.info("@@Reception.getTerminalInfo 保存先ライブラリ情報が存在しません。");
				log.info("@@Reception.getTerminalInfo 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "保存先ライブラリ情報確認 = NG";
				processing += "\n\n異常終了";
				errMessage += "保存先ライブラリ情報が存在しません。";
				rtnMessage = "Library not found";

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●端末情報要求終了");
				log.info("@@Reception.getTerminalInfo ●端末情報要求終了");

				return rtnMessage;
			}

			log.info("@@Reception.getTerminalInfo 保存先ライブラリ情報 = OK");
			processing += "\n保存先ライブラリ情報 = OK";

			// テナント情報を取得
			TenantModel tenantModel = tenantDao.findTenant(TENANT_ID);
			// ライブラリ情報を取得
			LibraryModel libraryModel = libraryDao.findLib(TENANT_ID, terminalModel.getLib_id());

			String liveRoom = "";

			if (!StringUtils.isEmpty(tenantModel.getLive_room())) {
				liveRoom = tenantModel.getLive_room();
			}
			if (!StringUtils.isEmpty(libraryModel.getLive_room())) {
				liveRoom = libraryModel.getLive_room();
			}

			rtnMessage = "{\"TERM_NAME\":\"" + terminalModel.getTerm_name()
						+ "\",\"STILLIMAGE_FUNCTION\":\"" + tenantModel.getStillimage_function()
						+ "\",\"MOVIE_FUNCTION\":\"" + tenantModel.getMovie_function()
						+ "\",\"LIVE_FUNCTION\":\"" + tenantModel.getLive_function()
						+ "\",\"START_TMP_DEL\":\"" + tenantModel.getStart_tmp_del()
						+ "\",\"TMP_DEL_TIME\":\"" + tenantModel.getTmp_del_time()
						+ "\",\"STOP_TIME\":\"" + tenantModel.getStop_time()
						+ "\",\"M_MAX_TIME\":\"" + terminalModel.getM_max_time()
						+ "\",\"LIVE_SERVER\":\"" + tenantModel.getLive_server()
						+ "\",\"LIVE_APP\":\"" + tenantModel.getLive_app()
						+ "\",\"LIVE_ROOM\":\"" + liveRoom
						+ "\",\"LIVE_WIDTH\":\"" + terminalModel.getLive_width()
						+ "\",\"LIVE_MIN_FPS\":\"" + terminalModel.getLive_min_fps()
						+ "\",\"LIVE_MAX_FPS\":\"" + terminalModel.getLive_max_fps()
						+ "\",\"START_DATE\":\"" + terminalModel.getStr_start_date()
						+ "\",\"END_DATE\":\"" + terminalModel.getStr_end_date()
						+ "\",\"DEL_FLG\":\"" + terminalModel.getDel_flg()
						+ "\"}";

			log.info("@@Reception.getTerminalInfo TERM_NAME = " + terminalModel.getTerm_name());
			log.info("@@Reception.getTerminalInfo STILLIMAGE_FUNCTION = " + tenantModel.getStillimage_function());
			log.info("@@Reception.getTerminalInfo MOVIE_FUNCTION = " + tenantModel.getMovie_function());
			log.info("@@Reception.getTerminalInfo LIVE_FUNCTION = " + tenantModel.getLive_function());
			log.info("@@Reception.getTerminalInfo START_TMP_DEL = " + tenantModel.getStart_tmp_del());
			log.info("@@Reception.getTerminalInfo TMP_DEL_TIME = " + tenantModel.getTmp_del_time());
			log.info("@@Reception.getTerminalInfo STOP_TIME = " + tenantModel.getStop_time());
			log.info("@@Reception.getTerminalInfo M_MAX_TIME = " + terminalModel.getM_max_time());
			log.info("@@Reception.getTerminalInfo LIVE_SERVER = " + tenantModel.getLive_server());
			log.info("@@Reception.getTerminalInfo LIVE_APP = " + tenantModel.getLive_app());
			log.info("@@Reception.getTerminalInfo LIVE_ROOM = " + liveRoom);
			log.info("@@Reception.getTerminalInfo LIVE_WIDTH = " + terminalModel.getLive_width());
			log.info("@@Reception.getTerminalInfo LIVE_MIN_FPS = " + terminalModel.getLive_min_fps());
			log.info("@@Reception.getTerminalInfo LIVE_MAX_FPS = " + terminalModel.getLive_max_fps());
			log.info("@@Reception.getTerminalInfo START_DATE = " + terminalModel.getStr_start_date());
			log.info("@@Reception.getTerminalInfo END_DATE = " + terminalModel.getStr_end_date());
			log.info("@@Reception.getTerminalInfo DEL_FLG = " + terminalModel.getDel_flg());

			if (!StringUtils.isEmpty(tenantModel.getStr_stillimage_function())) {
				processing += "\n静止画撮影機能 = ON";
			}else {
				processing += "\n静止画撮影機能 = OFF";
			}
			if (!StringUtils.isEmpty(tenantModel.getStr_movie_function())) {
				processing += "\n動画撮影機能 = ON";
			}else {
				processing += "\n動画撮影機能 = OFF";
			}
			if (!StringUtils.isEmpty(tenantModel.getStr_live_function())) {
				processing += "\nライブ撮影機能 = ON";
			}else {
				processing += "\nライブ撮影機能 = OFF";
			}
			if (!StringUtils.isEmpty(tenantModel.getStr_start_tmp_del())) {
				processing += "\n起動時一時ファイル削除機能 = ON";
			}else {
				processing += "\n起動時一時ファイル削除機能 = OFF";
			}
			processing += "\n一時ファイル時間経過削除(時間) = " + tenantModel.getTmp_del_time();
			processing += "\nアプリ自動停止時間(分) = " + tenantModel.getStop_time();
			processing += "\n動画撮影最大時間(秒) = " + terminalModel.getM_max_time();
			processing += "\nライブルーム名 = " + liveRoom;
			processing += "\nライブ撮影サイズ(横) = " + terminalModel.getLive_width();
			processing += "\n最小ライブフレームレート(fps) = " + terminalModel.getLive_min_fps();
			processing += "\n最大ライブフレームレート(fps) = " + terminalModel.getLive_max_fps();
			processing += "\n有効期限 = " + terminalModel.getStr_start_date() + " ～ " + terminalModel.getStr_end_date();
			processing += "\n削除フラグ = " + terminalModel.getDel_flg();

			System.out.println("正常終了");
			log.info("@@Reception.getTerminalInfo 正常終了");
			receiveLogModel.setResult_id(CommonConst.SUCCESS_CO);
			processing += "\n\n正常終了";

		}catch(Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			String str = sw.toString();
			System.out.println("例外エラー");
			log.info("@@Reception.getTerminalInfo 例外エラー：\n" + str);
			receiveLogModel.setResult_id(CommonConst.FAILED_CO);
			processing += "\n\n異常終了";
			errMessage += "\n\n例外エラー：\n" + str;
			rtnMessage = "Exception error";
		}

		receiveLogModel.setContents(processing);
		receiveLogModel.setErr_msg(errMessage);
		receiveLogModel.setEnd_date(new Date());

		//受信ログ登録
		receiveLogDao.insert(receiveLogModel);

		System.out.println("●端末情報要求終了");
		log.info("@@Reception.getTerminalInfo ●端末情報要求終了");

		return rtnMessage;
    }

	/**
	 * 付帯情報項目名を送信します．<br>
	 * @param TENANT_ID テナントID
	 * @param TERM_ID 端末ID
	 * @return 処理結果情報
	 * @throws Exception
	 */
	@RequestMapping(value = "getAdditionalItem", method = RequestMethod.POST, produces="text/html;charset=UTF-8")
	@ResponseBody
	public String getAdditionalItem(@RequestParam String TENANT_ID, String TERM_ID, HttpServletRequest request) throws Exception {

		System.out.println("●付帯情報項目名要求開始");
		log.info("@@Reception.getAdditionalItem ●付帯情報項目名要求開始");
		log.info("@@Reception.getAdditionalItem 送信元IP = " + request.getRemoteAddr());

		log.info("@@Reception.getAdditionalItem 付帯情報項目名要求 TENANT_ID = " + TENANT_ID);
		log.info("@@Reception.getAdditionalItem 　　　　　　　　　 TERM_ID = " + TERM_ID);

		//受信ログ準備
		ReceiveLogModel receiveLogModel = new ReceiveLogModel();
		receiveLogModel.setSender_ip(request.getRemoteAddr());
		receiveLogModel.setTenant_id(TENANT_ID);
		receiveLogModel.setTerm_id(TERM_ID);
		receiveLogModel.setRecv_division(CommonConst.RECEPTION_GETADDITIONALITEM);
		receiveLogModel.setStart_date(new Date());
		//処理内容
		String processing = "";
		//エラーメッセージ
		String errMessage = "";
		//処理結果通知
		String rtnMessage = "";

		try {

			//テナントが有効か確認
			if (tenantDao.auth(TENANT_ID) == 0) {
				System.out.println("有効テナント情報無し");
				log.info("@@Reception.getAdditionalItem テナント確認 = NG");
				log.info("@@Reception.getAdditionalItem 有効テナント情報が存在しません。");
				log.info("@@Reception.getAdditionalItem 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "テナント確認 = NG";
				processing += "\n\n異常終了";
				errMessage += "有効テナント情報が存在しません。";
				rtnMessage = "Tenant not found";

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●付帯情報項目名要求終了");
				log.info("@@Reception.getAdditionalItem ●付帯情報項目名要求終了");

				return rtnMessage;
			}

			//テナント確認は受信ログに出力しない
			log.info("@@Reception.getAdditionalItem テナント確認 = OK");

			//端末が存在するか確認(有効期間外の端末も含む)
			if(terminalDao.count(TENANT_ID, TERM_ID,"chk") == 0) {
				System.out.println("端末情報無し");
				log.info("@@Reception.getAdditionalItem 端末確認 = NG");
				log.info("@@Reception.getAdditionalItem 端末情報が存在しません。");
				log.info("@@Reception.getAdditionalItem 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "端末確認 = NG";
				processing += "\n\n異常終了";
				errMessage += "端末情報が存在しません。";
				rtnMessage = "Terminal not found";

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●付帯情報項目名要求終了");
				log.info("@@Reception.getAdditionalItem ●付帯情報項目名要求終了");

				return rtnMessage;
			}

			log.info("@@Reception.getAdditionalItem 端末確認 = OK");
			processing += "\n端末確認 = OK";

			//付帯情報項目名存在判定
			if (additionalItemDao.searchCount(TENANT_ID) == 0) {
				System.out.println("付帯情報項目名情報無し");
				log.info("@@Reception.getAdditionalItem 付帯情報項目確認 = NG");
				log.info("@@Reception.getAdditionalItem 付帯情報項目が存在しません。");
				log.info("@@Reception.getAdditionalItem 異常終了");
				receiveLogModel.setResult_id(CommonConst.FAILED_CO);
				processing += "\n付帯情報項目確認 = NG";
				processing += "\n\n異常終了";
				errMessage += "付帯情報項目が存在しません。";
				rtnMessage = "AdditionalItem not found";

				receiveLogModel.setContents(processing);
				receiveLogModel.setErr_msg(errMessage);
				receiveLogModel.setEnd_date(new Date());

				//受信ログ登録
				receiveLogDao.insert(receiveLogModel);

				System.out.println("●付帯情報項目名要求終了");
				log.info("@@Reception.getAdditionalItem ●付帯情報項目名要求終了");

				return rtnMessage;
			}

			log.info("@@Reception.getAdditionalItem 付帯情報項目確認 = OK");
			processing += "\n付帯情報項目確認 = OK";

			//テナントIDから付帯情報項目名を検索
			List<AdditionalItemModel> addItemModel = additionalItemDao.useItem(TENANT_ID);
			for(int t=0; t < addItemModel.size(); t++) {
				if (t == 0) {
					rtnMessage += "[";
				}
				rtnMessage += "{\"ITEM_ID\":\"" + addItemModel.get(t).getItem_id() + "\",\"ITEM_NAME\":\"" + addItemModel.get(t).getItem_name() + "\"}";
				log.info("@@Reception.getAdditionalItem 項目ID：" + addItemModel.get(t).getItem_id() + "　/　項目名：" + addItemModel.get(t).getItem_name());
				processing += "\n項目ID：" + addItemModel.get(t).getItem_id() + "　/　項目名：" + addItemModel.get(t).getItem_name();
				if (t == addItemModel.size() -1) {
					rtnMessage += "]";
				}else {
					rtnMessage += ",";
				}
			}

			System.out.println("正常終了");
			log.info("@@Reception.getAdditionalItem 正常終了");
			receiveLogModel.setResult_id(CommonConst.SUCCESS_CO);
			processing += "\n\n正常終了";

		}catch(Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			String str = sw.toString();
			System.out.println("例外エラー");
			log.info("@@Reception.getAdditionalItem 例外エラー：\n" + str);
			receiveLogModel.setResult_id(CommonConst.FAILED_CO);
			processing += "\n\n異常終了";
			errMessage += "\n\n例外エラー：\n" + str;
			rtnMessage = "Exception error";
		}

		receiveLogModel.setContents(processing);
		receiveLogModel.setErr_msg(errMessage);
		receiveLogModel.setEnd_date(new Date());

		//受信ログ登録
		receiveLogDao.insert(receiveLogModel);

		System.out.println("●付帯情報項目名要求終了");
		log.info("@@Reception.getAdditionalItem ●付帯情報項目名要求終了");

		return rtnMessage;
    }

	/**
	 * 対象のファイルオブジェクトの削除を行う.<BR>
	 * @param path ファイルパスオブジェクト
	 * @throws Exception
	 */
	private boolean deleteFile(final Path path) throws Exception {

		boolean fileDelete = true;

		// 存在しない場合は処理終了
		if (Files.notExists(path)) {
			fileDelete = true;
			return fileDelete;
		}

		// 対象がファイルを削除する
		// 失敗したらリトライ
		for (int x = 0 ; x < CommonConst.RETRY_COUNT ; x++) {
			try {
				Files.deleteIfExists(path);
				if (Files.notExists(path)) {
					// 削除に成功したらループから抜ける
					break;
				}else {
					// 削除失敗
					log.info("@@Reception.deleteFile 削除失敗ファイル = " + path.toAbsolutePath().toString() + " 削除失敗回数 =" + (x + 1));
					try
					{
						Thread.sleep(x * 10); //x * 10ミリ秒Sleepする
					}
					catch(InterruptedException ex){}
				}
			}catch (Exception e) {
				// 削除失敗(例外)
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				pw.flush();
				String str = sw.toString();
				log.info("@@Reception.deleteFile 削除失敗ファイル = " + path.toAbsolutePath().toString() + " 削除失敗回数(例外) =" + (x + 1) + "\n" + str);
				try
				{
					Thread.sleep(x * 10); //x * 10ミリ秒Sleepする
				}
				catch(InterruptedException ex){}
			}
			if (x == CommonConst.RETRY_COUNT -1) {
				// 最後まで削除できなかった場合
				fileDelete = false;
			}
		}

		return fileDelete;
	}

}