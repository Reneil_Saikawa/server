/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.SystemInfoModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * システム情報_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface SystemInfoDao {

	/**
	 * システム情報を返却します。<br>
	 * @return システム情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	SystemInfoModel findAll() throws DataAccessException;

	/**
	 * システム情報を更新します。<br>
	 * @param system_name システム名
	 * @param rootpath ルートパス
	 * @param inUrl 内部URL
	 * @param outUrl 外部URL
	 * @param directory_image ディレクトリ画像
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void update(String system_name, String rootpath, String inUrl, String outUrl, String directory_image) throws DataAccessException;

}
