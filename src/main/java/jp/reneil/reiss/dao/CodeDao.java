/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.CodeModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * コード_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface CodeDao {

	/**
	 * コード情報を返却します。<br>
	 * @param code_01 分類コード
	 * @param code_02 コード
	 * @return コード情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	CodeModel getCodeData(String code_01, String code_02) throws DataAccessException;

	/**
	 * コード情報分類取得を返却します。<br>
	 * @param code_01 分類コード
	 * @return コード情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<CodeModel> getCode01Data(String code_01) throws DataAccessException;

	/**
	 * コード情報を分類で範囲取得します。<br>
	 * @param code_01min 分類コード最小範囲
	 * @param code_01max 分類コード最大範囲
	 * @return コード情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<CodeModel> getCode01RangeData(String code_01min, String code_01max) throws DataAccessException;

	/**
	 * コード情報の存在確認をします。<br>
	 * @param name_02 名称2
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int count(String name_02) throws DataAccessException;

	/**
	 * コード情報を返却します。<br>
	 * @param name_02 名称2
	 * @return コード情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	CodeModel getName02Data(String name_02) throws DataAccessException;

}
