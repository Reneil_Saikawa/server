/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.ReportAuthModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 帳票権限_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface ReportAuthDao {

	/**
	 * ユーザの帳票権限情報を返却します。<br>
	 * @param user_id ユーザーID
	 * @return ユーザ帳票権限情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<ReportAuthModel> findUser(String user_id) throws DataAccessException;

	/**
	 * 帳票権限の存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @param user_id ユーザID
	 * @param report_id 帳票ID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int count(String tenant_id, String user_id, String report_id) throws DataAccessException;

	/**
	 * 帳票情報を追加します。<br>
	 * @param reportAuthModel 帳票権限モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int insert(ReportAuthModel reportAuthModel) throws DataAccessException;

	/**
	 * 帳票権限を削除します。<br>
	 * @param user_id ユーザID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void delete(String user_id) throws DataAccessException;

}
