/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.model.ReportParamModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 帳票パラメータ_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.0
 */
public class ReportParamDaoImpl extends JdbcDaoSupport implements ReportParamDao {

	/** カラム名：テナントID */
	private static final String COLUMN_TENANT_ID = "TENANT_ID";

	/** カラム名：帳票ID */
	private static final String COLUMN_REPORT_ID = "REPORT_ID";

	/** カラム名：帳票名 */
	private static final String COLUMN_REPORT_NAME = "REPORT_NAME";

	/** カラム名：テンプレートファイル名 */
	private static final String COLUMN_FILE_NAME = "FILE_NAME";

	/** カラム名：シート名 */
	private static final String COLUMN_SHEET_NAME = "SHEET_NAME";

	/** カラム名：1ページ当たりの出力件数 */
	private static final String COLUMN_PRINT_ROWS = "PRINT_ROWS";

	/** カラム名：表紙出力フラグ */
	private static final String COLUMN_FRONT_PAGE_FLG = "FRONT_PAGE_FLG";

	/** カラム名：表紙用シート名 */
	private static final String COLUMN_FRONT_PAGE_SHEET = "FRONT_PAGE_SHEET";

	/** カラム名：有効開始日 */
	private static final String COLUMN_START_DATE = "START_DATE";

	/** カラム名：有効終了日 */
	private static final String COLUMN_END_DATE = "END_DATE";

	/** カラム名：削除フラグ */
	private static final String COLUMN_DEL_FLG = "DEL_FLG";

	/** カラム名：更新ユーザーID */
	private static final String COLUMN_UPDATE_USER_ID = "UPDATE_USER_ID";

	/** 帳票パラメータ情報件数取得 */
	private static final String SQL_COUNT =
			  "SELECT COUNT(REPORT_ID)"
			+ " FROM REPORTPARAM"
			+ " WHERE TENANT_ID = ?"
			+ " AND REPORT_ID = ?";

	/** 帳票パラメータ情報件数取得 */
	private static final String SQL_ADD_FILE_COUNT =
			  "SELECT COUNT(REPORT_ID)"
			+ " FROM REPORTPARAM"
			+ " WHERE TENANT_ID = ?"
			+ " AND FILE_NAME = ?";

	/** 帳票パラメータ情報件数取得 */
	private static final String SQL_UPDATE_FILE_COUNT =
			  "SELECT COUNT(REPORT_ID)"
			+ " FROM REPORTPARAM"
			+ " WHERE TENANT_ID = ?"
			+ " AND FILE_NAME = ?"
			+ " AND REPORT_ID <> ?";

	/** 帳票パラメータ情報取得 */
	private static final String SQL_FINDALL =
			  "SELECT"
			+ " TENANT_ID,"
			+ " REPORT_ID,"
			+ " REPORT_NAME,"
			+ " FILE_NAME,"
			+ " SHEET_NAME,"
			+ " PRINT_ROWS,"
			+ " FRONT_PAGE_FLG,"
			+ " FRONT_PAGE_SHEET,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM REPORTPARAM";

	/** 帳票パラメータ情報取得 */
	private static final String SQL_USER =
			  "SELECT"
			+ " RP.[TENANT_ID],"
			+ " RP.[REPORT_ID],"
			+ " RP.[REPORT_NAME],"
			+ " RP.[FILE_NAME],"
			+ " RP.[SHEET_NAME],"
			+ " RP.[PRINT_ROWS],"
			+ " RP.[FRONT_PAGE_FLG],"
			+ " RP.[FRONT_PAGE_SHEET],"
			+ " RP.[START_DATE],"
			+ " RP.[END_DATE],"
			+ " RP.[DEL_FLG],"
			+ " RP.[UPDATE_USER_ID],"
			+ " RP.[UPDATE_DATE]"
			+ " FROM [REPORTPARAM] RP,"
			+ "      [REPORTAUTH] RA"
			+ " WHERE RP.[TENANT_ID] = RA.[TENANT_ID]"
			+ " AND RP.[REPORT_ID] = RA.[REPORT_ID]"
			+ " AND RP.[TENANT_ID] = ?"
			+ " AND RA.[USER_ID] = ?"
			+ " AND RP.[START_DATE] <= GETDATE() AND RP.[END_DATE] >= GETDATE() AND RP.[DEL_FLG] = 0"
			+ " ORDER BY RP.[REPORT_ID]";

	/** 帳票パラメータ情報追加 */
	private static final String SQL_INSERT =
			  "INSERT INTO REPORTPARAM("
			+ " TENANT_ID,"
			+ " REPORT_ID,"
			+ " REPORT_NAME,"
			+ " FILE_NAME,"
			+ " SHEET_NAME,"
			+ " PRINT_ROWS,"
			+ " FRONT_PAGE_FLG,"
			+ " FRONT_PAGE_SHEET,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE)"
			+ " VALUES("
			+ " :TENANT_ID,"
			+ " :REPORT_ID,"
			+ " :REPORT_NAME,"
			+ " :FILE_NAME,"
			+ " :SHEET_NAME,"
			+ " :PRINT_ROWS,"
			+ " :FRONT_PAGE_FLG,"
			+ " :FRONT_PAGE_SHEET,"
			+ " :START_DATE,"
			+ " DATEADD(HOUR, 23, DATEADD(MINUTE, 59, DATEADD(SECOND, 59, :END_DATE))),"
			+ " :DEL_FLG,"
			+ " :UPDATE_USER_ID,"
			+ " GETDATE())";

	/** 帳票パラメータ情報更新 */
	private static final String SQL_UPDATE =
			  "UPDATE REPORTPARAM"
			+ " SET REPORT_NAME = :REPORT_NAME,"
			+ " FILE_NAME = :FILE_NAME,"
			+ " SHEET_NAME = :SHEET_NAME,"
			+ " PRINT_ROWS = :PRINT_ROWS,"
			+ " FRONT_PAGE_FLG = :FRONT_PAGE_FLG,"
			+ " FRONT_PAGE_SHEET = :FRONT_PAGE_SHEET,"
			+ " START_DATE = :START_DATE,"
			+ " END_DATE = DATEADD(HOUR, 23, DATEADD(MINUTE, 59, DATEADD(SECOND, 59, :END_DATE))),"
			+ " DEL_FLG = :DEL_FLG,"
			+ " UPDATE_USER_ID = :UPDATE_USER_ID,"
			+ " UPDATE_DATE = GETDATE()"
			+ " WHERE TENANT_ID = :TENANT_ID"
			+ " AND REPORT_ID = :REPORT_ID";

	/** 帳票パラメータ情報削除 */
	private static final String SQL_DELETE =
			  "DELETE FROM REPORTPARAM "
			+ "WHERE TENANT_ID = ?";

	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** JDBCテンプレートクラス（リプレースあり） */
	@Autowired
	private NamedParameterJdbcTemplate npJdbcTemplate;

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * 帳票パラメータ情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class ReportParamRowMapper implements RowMapper<ReportParamModel> {
		/**
		 * 帳票パラメータ情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public ReportParamModel mapRow(ResultSet rs, int rowNum) throws SQLException {

			ReportParamModel model = new ReportParamModel();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");

			// テナントID
			model.setTenant_id(rs.getString(COLUMN_TENANT_ID));
			// 帳票ID
			model.setReport_id(rs.getString(COLUMN_REPORT_ID));
			// 帳票名
			model.setReport_name(rs.getString(COLUMN_REPORT_NAME));
			// テンプレートファイル名
			model.setFile_name(rs.getString(COLUMN_FILE_NAME));
			// シート名
			model.setSheet_name(rs.getString(COLUMN_SHEET_NAME));
			// 1ページ当たりの出力件数
			model.setPrint_rows(rs.getShort(COLUMN_PRINT_ROWS));

			// 表紙出力フラグ
			model.setFront_page_flg(rs.getByte(COLUMN_FRONT_PAGE_FLG));
			if (rs.getByte(COLUMN_FRONT_PAGE_FLG) == (byte) 1) {
				// String 削除フラグ
				model.setStr_front_page_flg("✔");
			}

			// 表紙用シート名
			model.setFront_page_sheet(rs.getString(COLUMN_FRONT_PAGE_SHEET));
			// 有効開始日
			model.setStart_date(rs.getDate(COLUMN_START_DATE));
			// 有効終了日
			model.setEnd_date(rs.getDate(COLUMN_END_DATE));
			// String 有効開始日
			model.setStr_start_date(fmt.format(rs.getDate(COLUMN_START_DATE)));
			// String 有効終了日
			model.setStr_end_date(fmt.format(rs.getDate(COLUMN_END_DATE)));

			// 削除フラグ
			model.setDel_flg(rs.getByte(COLUMN_DEL_FLG));
			if (rs.getByte(COLUMN_DEL_FLG) == (byte) 1) {
				// String 削除フラグ
				model.setStr_del_flg("✔");
			}

			// 更新ユーザーID
			model.setUpdate_user_id(rs.getString(COLUMN_UPDATE_USER_ID));

			return model;
		}
	}

	/**
	 * 帳票パラメータの存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @param report_id 帳票ID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int count(String tenant_id, String report_id) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_COUNT, Integer.class, tenant_id, report_id);
	}

	/**
	 * 帳票パラメータ使用可能確認をします。<br>
	 * @param tenant_id テナントID
	 * @param report_id 帳票ID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int limitCount(String tenant_id, String report_id) throws DataAccessException {
		String strSQL = SQL_COUNT;
		strSQL = strSQL + " AND START_DATE <= GETDATE() AND END_DATE >= GETDATE() AND DEL_FLG = 0";
		return jdbcTemplate.queryForObject(strSQL, Integer.class, tenant_id, report_id);
	}

	/**
	 * 帳票ファイル名の存在確認をします。（追加確認用）<br>
	 * @param tenant_id テナントID
	 * @param file_name 帳票ファイル名
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int addFileCount(String tenant_id, String file_name) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_ADD_FILE_COUNT, Integer.class, tenant_id, file_name);
	}

	/**
	 * 帳票ファイル名の存在確認をします。（更新確認用）<br>
	 * @param tenant_id テナントID
	 * @param file_name 帳票ファイル名
	 * @param report_id 帳票ID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int updateFileCount(String tenant_id, String file_name, String report_id) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_UPDATE_FILE_COUNT, Integer.class, tenant_id, file_name, report_id);
	}

	/**
	 * 帳票パラメータ情報を返却します。（すべて）<br>
	 * @return 帳票パラメータ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<ReportParamModel> findAll() throws DataAccessException {
		RowMapper<ReportParamModel> rowMapper = new ReportParamRowMapper();
		return jdbcTemplate.query(SQL_FINDALL, rowMapper);
	}

	/**
	 * 帳票パラメータ情報を返却します。（テナント）<br>
	 * @param tenant_id テナントID
	 * @return 帳票パラメータ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<ReportParamModel> findTenant(String tenant_id) throws DataAccessException{
		RowMapper<ReportParamModel> rowMapper = new ReportParamRowMapper();
		String strSQL = SQL_FINDALL;
		strSQL = strSQL + " WHERE TENANT_ID = ?" + " ORDER BY REPORT_ID";
		return jdbcTemplate.query(strSQL, rowMapper, tenant_id);
	}

	/**
	 * 帳票パラメータ情報を返却します。（テナント管理者）<br>
	 * @param tenant_id テナントID
	 * @return 帳票パラメータ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<ReportParamModel> findTenantManager(String tenant_id) throws DataAccessException{
		RowMapper<ReportParamModel> rowMapper = new ReportParamRowMapper();
		String strSQL = SQL_FINDALL;
		strSQL = strSQL + " WHERE TENANT_ID = ? AND START_DATE <= GETDATE() AND END_DATE >= GETDATE() AND DEL_FLG = 0" + " ORDER BY REPORT_ID";
		return jdbcTemplate.query(strSQL, rowMapper, tenant_id);
	}

	/**
	 * 帳票パラメータ情報を返却します。（ユーザー）<br>
	 * @param tenant_id テナントID
	 * @param user_id ユーザーID
	 * @return 帳票パラメータ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<ReportParamModel> findUser(String tenant_id, String user_id) throws DataAccessException{
		RowMapper<ReportParamModel> rowMapper = new ReportParamRowMapper();
		return jdbcTemplate.query(SQL_USER, rowMapper, tenant_id, user_id);
	}

	/**
	 * 帳票パラメータ情報を返却します。（帳票出力時）<br>
	 * @param tenant_id テナントID
	 * @param report_id 帳票ID
	 * @return テナント情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public ReportParamModel findOutput(String tenant_id, String report_id) throws DataAccessException{
		RowMapper<ReportParamModel> rowMapper = new ReportParamRowMapper();
		String strSQL = SQL_FINDALL;
		strSQL = strSQL + " WHERE TENANT_ID = ? AND REPORT_ID = ? AND START_DATE <= GETDATE() AND END_DATE >= GETDATE() AND DEL_FLG = 0";
		return (ReportParamModel) jdbcTemplate.queryForObject(strSQL, rowMapper, tenant_id, report_id);
	}

	/**
	 * 帳票パラメータ情報を追加します。<br>
	 * @param reportParamModel 帳票パラメータ情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int insert(ReportParamModel reportParamModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
				.addValue(COLUMN_TENANT_ID, reportParamModel.getTenant_id())
				.addValue(COLUMN_REPORT_ID, reportParamModel.getReport_id())
				.addValue(COLUMN_REPORT_NAME, reportParamModel.getReport_name())
				.addValue(COLUMN_FILE_NAME, reportParamModel.getFile_name())
				.addValue(COLUMN_SHEET_NAME, reportParamModel.getSheet_name())
				.addValue(COLUMN_PRINT_ROWS, reportParamModel.getPrint_rows())
				.addValue(COLUMN_FRONT_PAGE_FLG, reportParamModel.getFront_page_flg())
				.addValue(COLUMN_FRONT_PAGE_SHEET, reportParamModel.getFront_page_sheet())
				.addValue(COLUMN_START_DATE, reportParamModel.getStart_date())
				.addValue(COLUMN_END_DATE, reportParamModel.getEnd_date())
				.addValue(COLUMN_DEL_FLG, reportParamModel.getDel_flg())
				.addValue(COLUMN_UPDATE_USER_ID, reportParamModel.getUpdate_user_id()));
	}

	/**
	 * 帳票パラメータ情報を更新します。<br>
	 * @param reportParamModel 帳票パラメータ情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int update(ReportParamModel reportParamModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_UPDATE, new MapSqlParameterSource()
				.addValue(COLUMN_TENANT_ID, reportParamModel.getTenant_id())
				.addValue(COLUMN_REPORT_ID, reportParamModel.getReport_id())
				.addValue(COLUMN_REPORT_NAME, reportParamModel.getReport_name())
				.addValue(COLUMN_FILE_NAME, reportParamModel.getFile_name())
				.addValue(COLUMN_SHEET_NAME, reportParamModel.getSheet_name())
				.addValue(COLUMN_PRINT_ROWS, reportParamModel.getPrint_rows())
				.addValue(COLUMN_FRONT_PAGE_FLG, reportParamModel.getFront_page_flg())
				.addValue(COLUMN_FRONT_PAGE_SHEET, reportParamModel.getFront_page_sheet())
				.addValue(COLUMN_START_DATE, reportParamModel.getStart_date())
				.addValue(COLUMN_END_DATE, reportParamModel.getEnd_date())
				.addValue(COLUMN_DEL_FLG, reportParamModel.getDel_flg())
				.addValue(COLUMN_UPDATE_USER_ID, reportParamModel.getUpdate_user_id()));
	}

	/**
	 * 帳票パラメータ情報を削除します。<br>
	 * @param tenant_id テナントID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public void delete(String tenant_id) throws DataAccessException {
		jdbcTemplate.update(SQL_DELETE, tenant_id);
	}

}
