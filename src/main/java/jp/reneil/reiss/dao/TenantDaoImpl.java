/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2020/02/14      1.0.1           女屋            地図有効化機能追加
 */
package jp.reneil.reiss.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.model.TenantModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * テナント_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.1
 */
public class TenantDaoImpl extends JdbcDaoSupport implements TenantDao {

	/** カラム名：テナントID */
	private static final String COLUMN_TENANT_ID = "TENANT_ID";

	/** カラム名：テナント名 */
	private static final String COLUMN_TENANT_NAME = "TENANT_NAME";

	/** カラム名：ユーザーライセンス数 */
	private static final String COLUMN_USER_LICENSE = "USER_LICENSE";

	/** カラム名：端末ライセンス数 */
	private static final String COLUMN_TERM_LICENSE = "TERM_LICENSE";

	/** カラム名：静止画撮影機能 */
	private static final String COLUMN_STILLIMAGE_FUNCTION = "STILLIMAGE_FUNCTION";

	/** カラム名：動画撮影機能 */
	private static final String COLUMN_MOVIE_FUNCTION = "MOVIE_FUNCTION";

	/** カラム名：ライブ撮影機能 */
	private static final String COLUMN_LIVE_FUNCTION = "LIVE_FUNCTION";

	/** カラム名：起動時一時ファイル削除 */
	private static final String COLUMN_START_TMP_DEL = "START_TMP_DEL";

	/** カラム名：一時ファイル時間経過削除 */
	private static final String COLUMN_TMP_DEL_TIME = "TMP_DEL_TIME";

	/** カラム名：アプリ自動停止時間 */
	private static final String COLUMN_STOP_TIME = "STOP_TIME";

	/** カラム名：パスワードロック回数 */
	private static final String COLUMN_PASSWORD_LOCK_TIMES = "PASSWORD_LOCK_TIMES";

	/** カラム名：サムネイルサイズ（横） */
	private static final String COLUMN_THUMBNAIL_WIDTH = "THUMBNAIL_WIDTH";

	/** カラム名：サムネイルサイズ（縦） */
	private static final String COLUMN_THUMBNAIL_HEIGHT = "THUMBNAIL_HEIGHT";

	/** カラム名：パトライト有効化 */
	private static final String COLUMN_PATLITE_ENABLE = "PATLITE_ENABLE";

	/** カラム名：パトライトアドレス */
	private static final String COLUMN_PATLITE_IP = "PATLITE_IP";

	/** カラム名：パトライトログイン名 */
	private static final String COLUMN_PATLITE_LOGIN_NAME = "PATLITE_LOGIN_NAME";

	/** カラム名：パトライト受信時オプション */
	private static final String COLUMN_PATLITE_OPTION_RCV = "PATLITE_OPTION_RCV";

	/** カラム名：パトライト受信時鳴動時間 */
	private static final String COLUMN_PATLITE_TIME_RCV = "PATLITE_TIME_RCV";

	/** カラム名：パトライト警告時オプション */
	private static final String COLUMN_PATLITE_OPTION_WARN = "PATLITE_OPTION_WARN";

	/** カラム名：パトライト警告時鳴動時間 */
	private static final String COLUMN_PATLITE_TIME_WARN = "PATLITE_TIME_WARN";

	/** カラム名：パトライトエラー時オプション */
	private static final String COLUMN_PATLITE_OPTION_ERR = "PATLITE_OPTION_ERR";

	/** カラム名：パトライトエラー時鳴動時間 */
	private static final String COLUMN_PATLITE_TIME_ERR = "PATLITE_TIME_ERR";

	/** カラム名：ライブ接続先 */
	private static final String COLUMN_LIVE_SERVER = "LIVE_SERVER";

	/** カラム名：ライブアプリ名 */
	private static final String COLUMN_LIVE_APP = "LIVE_APP";

	/** カラム名：ライブルーム名 */
	private static final String COLUMN_LIVE_ROOM = "LIVE_ROOM";

	/** カラム名：地図有効化 */
	private static final String COLUMN_MAP_ENABLE = "MAP_ENABLE";

	/** カラム名：地図サーバ */
	private static final String COLUMN_MAP_SERVER = "MAP_SERVER";

	/** カラム名：帳票有効化 */
	private static final String COLUMN_REPORT_ENABLE = "REPORT_ENABLE";

	/** カラム名：ファイルパス表示 */
	private static final String COLUMN_DISPLAY_FILE_PATH = "DISPLAY_FILE_PATH";

	/** カラム名：有効開始日 */
	private static final String COLUMN_START_DATE = "START_DATE";

	/** カラム名：有効終了日 */
	private static final String COLUMN_END_DATE = "END_DATE";

	/** カラム名：削除フラグ */
	private static final String COLUMN_DEL_FLG = "DEL_FLG";

	/** カラム名：更新ユーザーID */
	private static final String COLUMN_UPDATE_USER_ID = "UPDATE_USER_ID";

	/** テナント情報件数取得 */
	private static final String SQL_COUNT =
			  "SELECT COUNT(TENANT_ID)"
			+ " FROM TENANT"
			+ " WHERE TENANT_ID = ?";

	/** テナント情報件数取得 */
	private static final String SQL_AUTH =
			  "SELECT COUNT(TENANT_ID)"
			+ " FROM TENANT"
			+ " WHERE TENANT_ID = ?"
			+ " AND START_DATE <= GETDATE() AND END_DATE >= GETDATE() AND DEL_FLG = 0";

	/** テナント情報取得 */
	private static final String SQL_FINDALL =
			  "SELECT"
			+ " TENANT_ID,"
			+ " TENANT_NAME,"
			+ " USER_LICENSE,"
			+ " TERM_LICENSE,"
			+ " STILLIMAGE_FUNCTION,"
			+ " MOVIE_FUNCTION,"
			+ " LIVE_FUNCTION,"
			+ " START_TMP_DEL,"
			+ " TMP_DEL_TIME,"
			+ " STOP_TIME,"
			+ " PASSWORD_LOCK_TIMES,"
			+ " THUMBNAIL_WIDTH,"
			+ " THUMBNAIL_HEIGHT,"
			+ " PATLITE_ENABLE,"
			+ " PATLITE_IP,"
			+ " PATLITE_LOGIN_NAME,"
			+ " PATLITE_OPTION_RCV,"
			+ " PATLITE_TIME_RCV,"
			+ " PATLITE_OPTION_WARN,"
			+ " PATLITE_TIME_WARN,"
			+ " PATLITE_OPTION_ERR,"
			+ " PATLITE_TIME_ERR,"
			+ " LIVE_SERVER,"
			+ " LIVE_APP,"
			+ " LIVE_ROOM,"
			+ " MAP_ENABLE,"
			+ " MAP_SERVER,"
			+ " REPORT_ENABLE,"
			+ " DISPLAY_FILE_PATH,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG"
			+ " FROM TENANT";

	/** テナント情報追加 */
	private static final String SQL_INSERT =
			  "INSERT INTO TENANT("
			+ " TENANT_ID,"
			+ " TENANT_NAME,"
			+ " USER_LICENSE,"
			+ " TERM_LICENSE,"
			+ " STILLIMAGE_FUNCTION,"
			+ " MOVIE_FUNCTION,"
			+ " LIVE_FUNCTION,"
			+ " START_TMP_DEL,"
			+ " TMP_DEL_TIME,"
			+ " STOP_TIME,"
			+ " PASSWORD_LOCK_TIMES,"
			+ " THUMBNAIL_WIDTH,"
			+ " THUMBNAIL_HEIGHT,"
			+ " PATLITE_ENABLE,"
			+ " PATLITE_IP,"
			+ " PATLITE_LOGIN_NAME,"
			+ " PATLITE_OPTION_RCV,"
			+ " PATLITE_TIME_RCV,"
			+ " PATLITE_OPTION_WARN,"
			+ " PATLITE_TIME_WARN,"
			+ " PATLITE_OPTION_ERR,"
			+ " PATLITE_TIME_ERR,"
			+ " LIVE_SERVER,"
			+ " LIVE_APP,"
			+ " LIVE_ROOM,"
			+ " MAP_ENABLE,"
			+ " MAP_SERVER,"
			+ " REPORT_ENABLE,"
			+ " DISPLAY_FILE_PATH,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE)"
			+ " VALUES("
			+ " :TENANT_ID,"
			+ " :TENANT_NAME,"
			+ " :USER_LICENSE,"
			+ " :TERM_LICENSE,"
			+ " :STILLIMAGE_FUNCTION,"
			+ " :MOVIE_FUNCTION,"
			+ " :LIVE_FUNCTION,"
			+ " :START_TMP_DEL,"
			+ " :TMP_DEL_TIME,"
			+ " :STOP_TIME,"
			+ " :PASSWORD_LOCK_TIMES,"
			+ " :THUMBNAIL_WIDTH,"
			+ " :THUMBNAIL_HEIGHT,"
			+ " :PATLITE_ENABLE,"
			+ " :PATLITE_IP,"
			+ " :PATLITE_LOGIN_NAME,"
			+ " :PATLITE_OPTION_RCV,"
			+ " :PATLITE_TIME_RCV,"
			+ " :PATLITE_OPTION_WARN,"
			+ " :PATLITE_TIME_WARN,"
			+ " :PATLITE_OPTION_ERR,"
			+ " :PATLITE_TIME_ERR,"
			+ " :LIVE_SERVER,"
			+ " :LIVE_APP,"
			+ " :LIVE_ROOM,"
			+ " :MAP_ENABLE,"
			+ " :MAP_SERVER,"
			+ " :REPORT_ENABLE,"
			+ " :DISPLAY_FILE_PATH,"
			+ " :START_DATE,"
			+ " DATEADD(HOUR, 23, DATEADD(MINUTE, 59, DATEADD(SECOND, 59, :END_DATE))),"
			+ " :DEL_FLG,"
			+ " :UPDATE_USER_ID,"
			+ " GETDATE())";

	/** テナント情報更新 */
	private static final String SQL_UPDATE =
			  "UPDATE TENANT"
			+ " SET TENANT_NAME = :TENANT_NAME,"
			+ " USER_LICENSE = :USER_LICENSE,"
			+ " TERM_LICENSE = :TERM_LICENSE,"
			+ " STILLIMAGE_FUNCTION = :STILLIMAGE_FUNCTION,"
			+ " MOVIE_FUNCTION = :MOVIE_FUNCTION,"
			+ " LIVE_FUNCTION = :LIVE_FUNCTION,"
			+ " START_TMP_DEL = :START_TMP_DEL,"
			+ " TMP_DEL_TIME = :TMP_DEL_TIME,"
			+ " STOP_TIME = :STOP_TIME,"
			+ " PASSWORD_LOCK_TIMES = :PASSWORD_LOCK_TIMES,"
			+ " THUMBNAIL_WIDTH = :THUMBNAIL_WIDTH,"
			+ " THUMBNAIL_HEIGHT = :THUMBNAIL_HEIGHT,"
			+ " PATLITE_ENABLE = :PATLITE_ENABLE,"
			+ " PATLITE_IP = :PATLITE_IP,"
			+ " PATLITE_LOGIN_NAME = :PATLITE_LOGIN_NAME,"
			+ " PATLITE_OPTION_RCV = :PATLITE_OPTION_RCV,"
			+ " PATLITE_TIME_RCV = :PATLITE_TIME_RCV,"
			+ " PATLITE_OPTION_WARN = :PATLITE_OPTION_WARN,"
			+ " PATLITE_TIME_WARN = :PATLITE_TIME_WARN,"
			+ " PATLITE_OPTION_ERR = :PATLITE_OPTION_ERR,"
			+ " PATLITE_TIME_ERR = :PATLITE_TIME_ERR,"
			+ " LIVE_SERVER = :LIVE_SERVER,"
			+ " LIVE_APP = :LIVE_APP,"
			+ " LIVE_ROOM = :LIVE_ROOM,"
			+ " MAP_ENABLE = :MAP_ENABLE,"
			+ " MAP_SERVER = :MAP_SERVER,"
			+ " REPORT_ENABLE = :REPORT_ENABLE,"
			+ " DISPLAY_FILE_PATH = :DISPLAY_FILE_PATH,"
			+ " START_DATE = :START_DATE,"
			+ " END_DATE = DATEADD(HOUR, 23, DATEADD(MINUTE, 59, DATEADD(SECOND, 59, :END_DATE))),"
			+ " DEL_FLG = :DEL_FLG,"
			+ " UPDATE_USER_ID = :UPDATE_USER_ID,"
			+ " UPDATE_DATE = GETDATE()"
			+ " WHERE TENANT_ID = :TENANT_ID";

	/** テナント情報削除 */
	private static final String SQL_DELETE =
			  "DELETE FROM TENANT "
			+ "WHERE TENANT_ID = ?";

	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** JDBCテンプレートクラス（リプレースあり） */
	@Autowired
	private NamedParameterJdbcTemplate npJdbcTemplate;


	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * テナント情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class TenantRowMapper implements RowMapper<TenantModel> {
		/**
		 * テナント情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public TenantModel mapRow(ResultSet rs, int rowNum) throws SQLException {

			TenantModel model = new TenantModel();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");

			// テナントID
			model.setTenant_id(rs.getString(COLUMN_TENANT_ID));
			// テナント名
			model.setTenant_name(rs.getString(COLUMN_TENANT_NAME));
			// ユーザーライセンス数
			model.setUser_license(rs.getShort(COLUMN_USER_LICENSE));
			// 端末ライセンス数
			model.setTerm_license(rs.getShort(COLUMN_TERM_LICENSE));

			// 静止画撮影機能
			model.setStillimage_function(rs.getByte(COLUMN_STILLIMAGE_FUNCTION));
			if (rs.getByte(COLUMN_STILLIMAGE_FUNCTION) == (byte) 1) {
				// String 静止画撮影機能
				model.setStr_stillimage_function("✔");
			}
			// 動画撮影機能
			model.setMovie_function(rs.getByte(COLUMN_MOVIE_FUNCTION));
			if (rs.getByte(COLUMN_MOVIE_FUNCTION) == (byte) 1) {
				// String 動画撮影機能
				model.setStr_movie_function("✔");
			}
			// ライブ撮影機能
			model.setLive_function(rs.getByte(COLUMN_LIVE_FUNCTION));
			if (rs.getByte(COLUMN_LIVE_FUNCTION) == (byte) 1) {
				// String ライブ撮影機能
				model.setStr_live_function("✔");
			}
			// 起動時一時ファイル削除
			model.setStart_tmp_del(rs.getByte(COLUMN_START_TMP_DEL));
			if (rs.getByte(COLUMN_START_TMP_DEL) == (byte) 1) {
				// String 起動時一時ファイル削除
				model.setStr_start_tmp_del("✔");
			}

			// 一時ファイル時間経過削除
			model.setTmp_del_time(rs.getShort(COLUMN_TMP_DEL_TIME));
			// アプリ自動停止時間
			model.setStop_time(rs.getShort(COLUMN_STOP_TIME));
			// パスワードロック回数
			model.setPassword_lock_times(rs.getShort(COLUMN_PASSWORD_LOCK_TIMES));
			// サムネイルサイズ（横）
			model.setThumbnail_width(rs.getShort(COLUMN_THUMBNAIL_WIDTH));
			// サムネイルサイズ（縦）
			model.setThumbnail_height(rs.getShort(COLUMN_THUMBNAIL_HEIGHT));

			// パトライト有効化
			model.setPatlite_enable(rs.getByte(COLUMN_PATLITE_ENABLE));
			if (rs.getByte(COLUMN_PATLITE_ENABLE) == (byte) 1) {
				// String パトライト有効化
				model.setStr_patlite_enable("✔");
			}

			// パトライトアドレス
			model.setPatLite_ip(rs.getString(COLUMN_PATLITE_IP));
			// パトライトログイン名
			model.setPatLite_login_name(rs.getString(COLUMN_PATLITE_LOGIN_NAME));
			// パトライト受信時オプション
			model.setPatLite_option_rcv(rs.getString(COLUMN_PATLITE_OPTION_RCV));
			// パトライト受信時鳴動時間
			model.setPatLite_time_rcv(rs.getShort(COLUMN_PATLITE_TIME_RCV));
			// パトライト警告時オプション
			model.setPatLite_option_warm(rs.getString(COLUMN_PATLITE_OPTION_WARN));
			// パトライト警告時鳴動時間
			model.setPatLite_time_warm(rs.getShort(COLUMN_PATLITE_TIME_WARN));
			// パトライトエラー時オプション
			model.setPatLite_option_err(rs.getString(COLUMN_PATLITE_OPTION_ERR));
			// パトライトエラー時鳴動時間
			model.setPatLite_time_err(rs.getShort(COLUMN_PATLITE_TIME_ERR));
			// ライブ接続先
			model.setLive_server(rs.getString(COLUMN_LIVE_SERVER));
			// ライブアプリ名
			model.setLive_app(rs.getString(COLUMN_LIVE_APP));
			// ライブルーム名
			model.setLive_room(rs.getString(COLUMN_LIVE_ROOM));

			// 地図有効化
			model.setMap_enable(rs.getByte(COLUMN_MAP_ENABLE));
			if (rs.getByte(COLUMN_MAP_ENABLE) == (byte) 1) {
				// String 地図有効化
				model.setStr_map_enable("✔");
			}

			// 地図サーバ
			model.setMap_server(rs.getString(COLUMN_MAP_SERVER));

			// 帳票有効化
			model.setReport_enable(rs.getByte(COLUMN_REPORT_ENABLE));
			if (rs.getByte(COLUMN_REPORT_ENABLE) == (byte) 1) {
				// String 帳票有効化
				model.setStr_report_enable("✔");
			}

			// ファイルパス表示
			model.setDisplay_file_path(rs.getByte(COLUMN_DISPLAY_FILE_PATH));
			if (rs.getByte(COLUMN_DISPLAY_FILE_PATH) == (byte) 1) {
				// String ファイルパス表示
				model.setStr_display_file_path("✔");
			}

			// 有効開始日
			model.setStart_date(rs.getDate(COLUMN_START_DATE));
			// 有効終了日
			model.setEnd_date(rs.getDate(COLUMN_END_DATE));
			// String 有効開始日
			model.setStr_start_date(fmt.format(rs.getDate(COLUMN_START_DATE)));
			// String 有効終了日
			model.setStr_end_date(fmt.format(rs.getDate(COLUMN_END_DATE)));

			// 削除フラグ
			model.setDel_flg(rs.getByte(COLUMN_DEL_FLG));
			if (rs.getByte(COLUMN_DEL_FLG) == (byte) 1) {
				// String 削除フラグ
				model.setStr_del_flg("✔");
			}

			return model;
		}
	}

	/**
	 * テナントの存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int count(String tenant_id) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_COUNT, Integer.class, tenant_id);
	}

	/**
	 * テナントの有効確認をします。<br>
	 * @param tenant_id テナントID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int auth(String tenant_id) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_AUTH, Integer.class, tenant_id);
	}

	/**
	 * テナント情報を返却します。<br>
	 * @return テナント情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<TenantModel> findAll() throws DataAccessException {
		RowMapper<TenantModel> rowMapper = new TenantRowMapper();
		String strSQL = SQL_FINDALL;
		strSQL = strSQL + " ORDER BY TENANT_ID";
		return jdbcTemplate.query(strSQL, rowMapper);
	}

	/**
	 * テナント情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @return テナント情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public TenantModel findTenant(String tenant_id) throws DataAccessException{
		RowMapper<TenantModel> rowMapper = new TenantRowMapper();
		String strSQL = SQL_FINDALL;
		strSQL = strSQL + " WHERE TENANT_ID = ?";
		return (TenantModel) jdbcTemplate.queryForObject(strSQL, rowMapper,tenant_id);
	}

	/**
	 * テナント情報を追加します。<br>
	 * @param tenantModel テナント情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int insert(TenantModel tenantModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
				.addValue(COLUMN_TENANT_ID, tenantModel.getTenant_id())
				.addValue(COLUMN_TENANT_NAME, tenantModel.getTenant_name())
				.addValue(COLUMN_USER_LICENSE, tenantModel.getUser_license())
				.addValue(COLUMN_TERM_LICENSE, tenantModel.getTerm_license())
				.addValue(COLUMN_STILLIMAGE_FUNCTION, tenantModel.getStillimage_function())
				.addValue(COLUMN_MOVIE_FUNCTION, tenantModel.getMovie_function())
				.addValue(COLUMN_LIVE_FUNCTION, tenantModel.getLive_function())
				.addValue(COLUMN_START_TMP_DEL, tenantModel.getStart_tmp_del())
				.addValue(COLUMN_TMP_DEL_TIME, tenantModel.getTmp_del_time())
				.addValue(COLUMN_STOP_TIME, tenantModel.getStop_time())
				.addValue(COLUMN_PASSWORD_LOCK_TIMES, tenantModel.getPassword_lock_times())
				.addValue(COLUMN_THUMBNAIL_WIDTH, tenantModel.getThumbnail_width())
				.addValue(COLUMN_THUMBNAIL_HEIGHT, tenantModel.getThumbnail_height())
				.addValue(COLUMN_PATLITE_ENABLE, tenantModel.getPatlite_enable())
				.addValue(COLUMN_PATLITE_IP, tenantModel.getPatLite_ip())
				.addValue(COLUMN_PATLITE_LOGIN_NAME, tenantModel.getPatLite_login_name())
				.addValue(COLUMN_PATLITE_OPTION_RCV, tenantModel.getPatLite_option_rcv())
				.addValue(COLUMN_PATLITE_TIME_RCV, tenantModel.getPatLite_time_rcv())
				.addValue(COLUMN_PATLITE_OPTION_WARN, tenantModel.getPatLite_option_warm())
				.addValue(COLUMN_PATLITE_TIME_WARN, tenantModel.getPatLite_time_warm())
				.addValue(COLUMN_PATLITE_OPTION_ERR, tenantModel.getPatLite_option_err())
				.addValue(COLUMN_PATLITE_TIME_ERR, tenantModel.getPatLite_time_err())
				.addValue(COLUMN_LIVE_SERVER, tenantModel.getLive_server())
				.addValue(COLUMN_LIVE_APP, tenantModel.getLive_app())
				.addValue(COLUMN_LIVE_ROOM, tenantModel.getLive_room())
				.addValue(COLUMN_MAP_ENABLE, tenantModel.getMap_enable())
				.addValue(COLUMN_MAP_SERVER, tenantModel.getMap_server())
				.addValue(COLUMN_REPORT_ENABLE, tenantModel.getReport_enable())
				.addValue(COLUMN_DISPLAY_FILE_PATH, tenantModel.getDisplay_file_path())
				.addValue(COLUMN_START_DATE, tenantModel.getStart_date())
				.addValue(COLUMN_END_DATE, tenantModel.getEnd_date())
				.addValue(COLUMN_DEL_FLG, tenantModel.getDel_flg())
				.addValue(COLUMN_UPDATE_USER_ID, tenantModel.getUpdate_user_id()));
	}

	/**
	 * テナント情報を更新します。<br>
	 * @param tenantModel テナント情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int update(TenantModel tenantModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_UPDATE, new MapSqlParameterSource()
				.addValue(COLUMN_TENANT_ID, tenantModel.getTenant_id())
				.addValue(COLUMN_TENANT_NAME, tenantModel.getTenant_name())
				.addValue(COLUMN_USER_LICENSE, tenantModel.getUser_license())
				.addValue(COLUMN_TERM_LICENSE, tenantModel.getTerm_license())
				.addValue(COLUMN_STILLIMAGE_FUNCTION, tenantModel.getStillimage_function())
				.addValue(COLUMN_MOVIE_FUNCTION, tenantModel.getMovie_function())
				.addValue(COLUMN_LIVE_FUNCTION, tenantModel.getLive_function())
				.addValue(COLUMN_START_TMP_DEL, tenantModel.getStart_tmp_del())
				.addValue(COLUMN_TMP_DEL_TIME, tenantModel.getTmp_del_time())
				.addValue(COLUMN_STOP_TIME, tenantModel.getStop_time())
				.addValue(COLUMN_PASSWORD_LOCK_TIMES, tenantModel.getPassword_lock_times())
				.addValue(COLUMN_THUMBNAIL_WIDTH, tenantModel.getThumbnail_width())
				.addValue(COLUMN_THUMBNAIL_HEIGHT, tenantModel.getThumbnail_height())
				.addValue(COLUMN_PATLITE_ENABLE, tenantModel.getPatlite_enable())
				.addValue(COLUMN_PATLITE_IP, tenantModel.getPatLite_ip())
				.addValue(COLUMN_PATLITE_LOGIN_NAME, tenantModel.getPatLite_login_name())
				.addValue(COLUMN_PATLITE_OPTION_RCV, tenantModel.getPatLite_option_rcv())
				.addValue(COLUMN_PATLITE_TIME_RCV, tenantModel.getPatLite_time_rcv())
				.addValue(COLUMN_PATLITE_OPTION_WARN, tenantModel.getPatLite_option_warm())
				.addValue(COLUMN_PATLITE_TIME_WARN, tenantModel.getPatLite_time_warm())
				.addValue(COLUMN_PATLITE_OPTION_ERR, tenantModel.getPatLite_option_err())
				.addValue(COLUMN_PATLITE_TIME_ERR, tenantModel.getPatLite_time_err())
				.addValue(COLUMN_LIVE_SERVER, tenantModel.getLive_server())
				.addValue(COLUMN_LIVE_APP, tenantModel.getLive_app())
				.addValue(COLUMN_LIVE_ROOM, tenantModel.getLive_room())
				.addValue(COLUMN_MAP_ENABLE, tenantModel.getMap_enable())
				.addValue(COLUMN_MAP_SERVER, tenantModel.getMap_server())
				.addValue(COLUMN_REPORT_ENABLE, tenantModel.getReport_enable())
				.addValue(COLUMN_DISPLAY_FILE_PATH, tenantModel.getDisplay_file_path())
				.addValue(COLUMN_START_DATE, tenantModel.getStart_date())
				.addValue(COLUMN_END_DATE, tenantModel.getEnd_date())
				.addValue(COLUMN_DEL_FLG, tenantModel.getDel_flg())
				.addValue(COLUMN_UPDATE_USER_ID, tenantModel.getUpdate_user_id()));
	}

	/**
	 * テナント情報を削除します。<br>
	 * @param tenant_id テナントID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public void delete(String tenant_id) throws DataAccessException {
		jdbcTemplate.update(SQL_DELETE, tenant_id);
	}

}
