/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.model.LibraryModel;
import jp.reneil.reiss.model.TerminalModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 端末_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.0
 */
public class TerminalDaoImpl extends JdbcDaoSupport implements TerminalDao {

	/** ライブラリ情報テーブルアクセス制御クラス */
	@Autowired
	private LibraryDao libraryDao;

	/** コードテーブルアクセス制御クラス */
	@Autowired
	private CodeDao codeDao;

	/** カラム名：テナントID */
	private static final String COLUMN_TENANT_ID = "TENANT_ID";

	/** カラム名：端末ID */
	private static final String COLUMN_TERM_ID = "TERM_ID";

	/** カラム名：端末区分 */
	private static final String COLUMN_TERM_CTG = "TERM_CTG";

	/** カラム名：端末名 */
	private static final String COLUMN_TERM_NAME = "TERM_NAME";

	/** カラム名：ライブラリID */
	private static final String COLUMN_LIB_ID = "LIB_ID";

	/** カラム名：撮影年フォルダ作成フラグ */
	private static final String COLUMN_YEAR_FOLDER_FLG = "YEAR_FOLDER_FLG";

	/** カラム名：撮影月フォルダ作成フラグ */
	private static final String COLUMN_MONTH_FOLDER_FLG = "MONTH_FOLDER_FLG";

	/** カラム名：撮影日フォルダ作成フラグ */
	private static final String COLUMN_DAY_FOLDER_FLG = "DAY_FOLDER_FLG";

	/** カラム名：日本語表記フラグ */
	private static final String COLUMN_JAPANESE_FLG = "JAPANESE_FLG";

	/** カラム名：動画撮影最大時間 */
	private static final String COLUMN_M_MAX_TIME = "M_MAX_TIME";

	/** カラム名：ライブ撮影サイズ（横） */
	private static final String COLUMN_LIVE_WIDTH = "LIVE_WIDTH";

	/** カラム名：ライブ最小フレームレート */
	private static final String COLUMN_LIVE_MIN_FPS = "LIVE_MIN_FPS";

	/** カラム名：ライブ最大フレームレート */
	private static final String COLUMN_LIVE_MAX_FPS = "LIVE_MAX_FPS";

	/** カラム名：有効開始日 */
	private static final String COLUMN_START_DATE = "START_DATE";

	/** カラム名：有効終了日 */
	private static final String COLUMN_END_DATE = "END_DATE";

	/** カラム名：削除フラグ */
	private static final String COLUMN_DEL_FLG = "DEL_FLG";

	/** カラム名：更新ユーザーID */
	private static final String COLUMN_UPDATE_USER_ID = "UPDATE_USER_ID";

	/** カラム名：更新日*/
	private static final String COLUMN_UPDATE_DATE = "UPDATE_DATE";

	/** 端末情報件数取得 */
	private static final String SQL_COUNT =
			  "SELECT COUNT(LIB_ID)"
			+ " FROM TERMINAL"
			+ " WHERE TENANT_ID = ?"
			+ " AND LIB_ID = ?";

	/** 端末情報存在確認 */
	private static final String SQL_CHK =
			 "SELECT COUNT(TERM_ID)"
			+ " FROM TERMINAL"
			+ " WHERE TENANT_ID = ?"
			+ " AND TERM_ID = ?";

	/** 端末有効確認 */
	private static final String SQL_AUTH =
			 "SELECT COUNT(TERM_ID)"
			+ " FROM TERMINAL"
			+ " WHERE TENANT_ID = ?"
			+ " AND TERM_ID = ?"
			+ " AND START_DATE <= GETDATE() AND END_DATE >= GETDATE() AND DEL_FLG = 0";

	/** ライセンス端末数取得 */
	private static final String SQL_LICENSE_COUNT=
			"SELECT COUNT(TERM_ID)"
			+ " FROM TERMINAL"
			+ " WHERE TENANT_ID = ?"
			+ " AND DEL_FLG = 0";

	/** 全端末情報取得 */
	private static final String SQL_ALL_TERM =
			  "SELECT"
			+ " TENANT_ID,"
			+ " TERM_ID,"
			+ " TERM_CTG,"
			+ " TERM_NAME,"
			+ " LIB_ID,"
			+ " YEAR_FOLDER_FLG,"
			+ " MONTH_FOLDER_FLG,"
			+ " DAY_FOLDER_FLG,"
			+ " JAPANESE_FLG,"
			+ " M_MAX_TIME,"
			+ " LIVE_WIDTH,"
			+ " LIVE_MIN_FPS,"
			+ " LIVE_MAX_FPS,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM TERMINAL"
			+ " ORDER BY TENANT_ID,"
			+ "          TERM_ID";

	/** 端末情報取得 */
	private static final String SQL_FINDTERM =
			  "SELECT"
			+ " TENANT_ID,"
			+ " TERM_ID,"
			+ " TERM_CTG,"
			+ " TERM_NAME,"
			+ " LIB_ID,"
			+ " YEAR_FOLDER_FLG,"
			+ " MONTH_FOLDER_FLG,"
			+ " DAY_FOLDER_FLG,"
			+ " JAPANESE_FLG,"
			+ " M_MAX_TIME,"
			+ " LIVE_WIDTH,"
			+ " LIVE_MIN_FPS,"
			+ " LIVE_MAX_FPS,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM TERMINAL"
			+ " WHERE TENANT_ID = ?"
			+ " AND TERM_ID = ?"
			+ " ORDER BY TENANT_ID,"
			+ "          TERM_ID";

	/** ライブラリ端末情報取得 */
	private static final String SQL_FINDLIB =
			  "SELECT"
			+ " TENANT_ID,"
			+ " TERM_ID,"
			+ " TERM_CTG,"
			+ " TERM_NAME,"
			+ " LIB_ID,"
			+ " YEAR_FOLDER_FLG,"
			+ " MONTH_FOLDER_FLG,"
			+ " DAY_FOLDER_FLG,"
			+ " JAPANESE_FLG,"
			+ " M_MAX_TIME,"
			+ " LIVE_WIDTH,"
			+ " LIVE_MIN_FPS,"
			+ " LIVE_MAX_FPS,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM TERMINAL"
			+ " WHERE TENANT_ID = ?"
			+ " AND LIB_ID = ?"
			+ " ORDER BY TENANT_ID,"
			+ "          TERM_ID";

	/** テナントに所属する端末情報取得 */
	private static final String SQL_TENANT_TERM =
			  "SELECT"
			+ " TENANT_ID,"
			+ " TERM_ID,"
			+ " TERM_CTG,"
			+ " TERM_NAME,"
			+ " LIB_ID,"
			+ " YEAR_FOLDER_FLG,"
			+ " MONTH_FOLDER_FLG,"
			+ " DAY_FOLDER_FLG,"
			+ " JAPANESE_FLG,"
			+ " M_MAX_TIME,"
			+ " LIVE_WIDTH,"
			+ " LIVE_MIN_FPS,"
			+ " LIVE_MAX_FPS,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM TERMINAL"
			+ " WHERE TENANT_ID = ?"
			+ " ORDER BY TENANT_ID,"
			+ "          TERM_ID";

	/** 選択テナント以外に所属する端末情報取得 */
	private static final String SQL_NOT_TENANT_TERM =
			  "SELECT"
			+ " TENANT_ID,"
			+ " TERM_ID,"
			+ " TERM_CTG,"
			+ " TERM_NAME,"
			+ " LIB_ID,"
			+ " YEAR_FOLDER_FLG,"
			+ " MONTH_FOLDER_FLG,"
			+ " DAY_FOLDER_FLG,"
			+ " JAPANESE_FLG,"
			+ " M_MAX_TIME,"
			+ " LIVE_WIDTH,"
			+ " LIVE_MIN_FPS,"
			+ " LIVE_MAX_FPS,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM TERMINAL"
			+ " WHERE TENANT_ID <> ?"
			+ " ORDER BY TENANT_ID,"
			+ "          TERM_ID";

	/** 端末情報追加 */
	private static final String SQL_INSERT =
			  "INSERT INTO TERMINAL("
			+ " TENANT_ID,"
			+ " TERM_ID,"
			+ " TERM_CTG,"
			+ " TERM_NAME,"
			+ " LIB_ID,"
			+ " YEAR_FOLDER_FLG,"
			+ " MONTH_FOLDER_FLG,"
			+ " DAY_FOLDER_FLG,"
			+ " JAPANESE_FLG,"
			+ " M_MAX_TIME,"
			+ " LIVE_WIDTH,"
			+ " LIVE_MIN_FPS,"
			+ " LIVE_MAX_FPS,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE)"
			+ " VALUES("
			+ " :TENANT_ID,"
			+ " :TERM_ID,"
			+ " :TERM_CTG,"
			+ " :TERM_NAME,"
			+ " :LIB_ID,"
			+ " :YEAR_FOLDER_FLG,"
			+ " :MONTH_FOLDER_FLG,"
			+ " :DAY_FOLDER_FLG,"
			+ " :JAPANESE_FLG,"
			+ " :M_MAX_TIME,"
			+ " :LIVE_WIDTH,"
			+ " :LIVE_MIN_FPS,"
			+ " :LIVE_MAX_FPS,"
			+ " :START_DATE,"
			+ " DATEADD(HOUR, 23, DATEADD(MINUTE, 59, DATEADD(SECOND, 59, :END_DATE))),"
			+ " :DEL_FLG,"
			+ " :UPDATE_USER_ID,"
			+ " GETDATE())";

	/** 端末情報更新 */
	private static final String SQL_UPDATE =
			  "UPDATE TERMINAL"
			+ " SET TENANT_ID = :TENANT_ID,"
			+ " TERM_ID = :TERM_ID,"
			+ " TERM_CTG = :TERM_CTG,"
			+ " TERM_NAME = :TERM_NAME,"
			+ " LIB_ID = :LIB_ID,"
			+ " YEAR_FOLDER_FLG = :YEAR_FOLDER_FLG,"
			+ " MONTH_FOLDER_FLG = :MONTH_FOLDER_FLG,"
			+ " DAY_FOLDER_FLG = :DAY_FOLDER_FLG,"
			+ " JAPANESE_FLG = :JAPANESE_FLG,"
			+ " M_MAX_TIME = :M_MAX_TIME,"
			+ " LIVE_WIDTH = :LIVE_WIDTH,"
			+ " LIVE_MIN_FPS = :LIVE_MIN_FPS,"
			+ " LIVE_MAX_FPS = :LIVE_MAX_FPS,"
			+ " START_DATE = :START_DATE,"
			+ " END_DATE = DATEADD(HOUR, 23, DATEADD(MINUTE, 59, DATEADD(SECOND, 59, :END_DATE))),"
			+ " DEL_FLG = :DEL_FLG,"
			+ " UPDATE_USER_ID = :UPDATE_USER_ID,"
			+ " UPDATE_DATE = GETDATE()"
			+ " WHERE TENANT_ID = :TENANT_ID"
			+ " AND TERM_ID = :TERM_ID";


	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** JDBCテンプレートクラス（リプレースあり） */
	@Autowired
	private NamedParameterJdbcTemplate npJdbcTemplate;


	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * 端末情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class TerminalRowMapper implements RowMapper<TerminalModel> {
		/**
		 * 端末情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public TerminalModel mapRow(ResultSet rs, int rowNum) throws SQLException {
			//端末モデルクラス生成
			TerminalModel model = new TerminalModel();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");

			// テナントID
			model.setTenant_id(rs.getString(COLUMN_TENANT_ID));
			//端末ID
			model.setTerm_id(rs.getString(COLUMN_TERM_ID));
			//端末区分
			model.setTerm_ctg(rs.getString(COLUMN_TERM_CTG));
			//端末区分名
			model.setTerm_ctg_name(codeDao.getCodeData(rs.getString(COLUMN_TERM_CTG).substring(0, 2), rs.getString(COLUMN_TERM_CTG).substring(2, 4)).getName_01());
			//端末名
			model.setTerm_name(rs.getString(COLUMN_TERM_NAME));
			//ライブラリID
			model.setLib_id(rs.getString(COLUMN_LIB_ID));

			//ライブラリ名取得
			if (libraryDao.count(rs.getString(COLUMN_TENANT_ID), rs.getString(COLUMN_LIB_ID), "chk") == 1) {
				//取得したテナントIDとライブラリIDからテナント情報を取得
				LibraryModel library = libraryDao.findLib(rs.getString(COLUMN_TENANT_ID), rs.getString(COLUMN_LIB_ID));
				model.setLib_name(library.getLib_name());
			}

			// 撮影年フォルダ作成フラグ
			model.setYear_folder_flg(rs.getByte(COLUMN_YEAR_FOLDER_FLG));
			if (rs.getByte(COLUMN_YEAR_FOLDER_FLG) == (byte) 1) {
				// String 撮影年フォルダ作成フラグ
				model.setStr_year_folder_flg("✔");
			}

			// 撮影月フォルダ作成フラグ
			model.setMonth_folder_flg(rs.getByte(COLUMN_MONTH_FOLDER_FLG));
			if (rs.getByte(COLUMN_MONTH_FOLDER_FLG) == (byte) 1) {
				// String 撮影月フォルダ作成フラグ
				model.setStr_month_folder_flg("✔");
			}

			// 撮影日フォルダ作成フラグ
			model.setDay_folder_flg(rs.getByte(COLUMN_DAY_FOLDER_FLG));
			if (rs.getByte(COLUMN_DAY_FOLDER_FLG) == (byte) 1) {
				// String 撮影日フォルダ作成フラグ
				model.setStr_day_folder_flg("✔");
			}

			// 日本語表記フラグ
			model.setJapanese_flg(rs.getByte(COLUMN_JAPANESE_FLG));
			if (rs.getByte(COLUMN_JAPANESE_FLG) == (byte) 1) {
				// String 日本語表記フラグ
				model.setStr_japanese_flg("✔");
			}

			//動画撮影最大時間
			model.setM_max_time(rs.getShort(COLUMN_M_MAX_TIME));
			//ライブ撮影サイズ（横）
			model.setLive_width(rs.getShort(COLUMN_LIVE_WIDTH));
			//ライブ最小フレームレート
			model.setLive_min_fps(rs.getShort(COLUMN_LIVE_MIN_FPS));
			//ライブ最大フレームレート
			model.setLive_max_fps(rs.getShort(COLUMN_LIVE_MAX_FPS));
			// 有効開始日
			model.setStart_date(rs.getDate(COLUMN_START_DATE));
			// 有効終了日
			model.setEnd_date(rs.getDate(COLUMN_END_DATE));
			// String 有効開始日
			model.setStr_start_date(fmt.format(rs.getDate(COLUMN_START_DATE)));
			// String 有効終了日
			model.setStr_end_date(fmt.format(rs.getDate(COLUMN_END_DATE)));
			// 削除フラグ
			model.setDel_flg(rs.getByte(COLUMN_DEL_FLG));

			if (rs.getByte(COLUMN_DEL_FLG) == (byte) 1) {
				// String 削除フラグ
				model.setStr_del_flg("✔");
			}

			//更新ユーザーID
			model.setUpdate_user_id(rs.getString(COLUMN_UPDATE_USER_ID));
			//更新日
			model.setUpdate_date(rs.getTimestamp(COLUMN_UPDATE_DATE));

			return model;
		}
	}

	/**
	 * 端末の存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @param lib_id ライブラリID
	 * @param chk 実行SQLチェック
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int count(String tenant_id, String lib_id,String chk) throws DataAccessException {
		//実行するSQLを変更するチェックの確認
		String dataChk;
		if(chk.equals("chk")) {
			dataChk = SQL_CHK;  //追加更新時処理
		}else {
			dataChk = SQL_COUNT;  //再帰処理時件数確認
		}
		return jdbcTemplate.queryForObject(dataChk, Integer.class, tenant_id, lib_id);

	}

	/**
	 * ライセンス端末数を返却します。<br>
	 * @param tenant_id テナントID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int licenseCount(String tenant_id) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_LICENSE_COUNT, Integer.class, tenant_id);

	}

	/**
	 * 端末の有効確認をします。<br>
	 * @param tenant_id テナントID
	 * @param lib_id ライブラリID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int auth(String tenant_id, String lib_id) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_AUTH, Integer.class, tenant_id, lib_id);

	}

	/**
	 * 端末情報を返却します。<br>
	 * @return 端末情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<TerminalModel> findAll() throws DataAccessException {
		RowMapper<TerminalModel> rowMapper = new TerminalRowMapper();
		return jdbcTemplate.query(SQL_ALL_TERM, rowMapper);
	}

	/**
	 * 端末情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @param lib_id ライブラリID
	 * @return 端末情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<TerminalModel> findLib(String tenant_id, String lib_id) throws DataAccessException {
		RowMapper<TerminalModel> rowMapper = new TerminalRowMapper();
		return jdbcTemplate.query(SQL_FINDLIB, rowMapper, tenant_id, lib_id);
	}

	/**
	 * テナントに所属する端末情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @return 端末情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<TerminalModel> findTenantAllTerm(String tenant_id) throws DataAccessException {
		RowMapper<TerminalModel> rowMapper = new TerminalRowMapper();
		return jdbcTemplate.query(SQL_TENANT_TERM, rowMapper, tenant_id);
	}

	/**
	 * 選択テナント以外に所属する端末情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @return 端末情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<TerminalModel> findNotTenantAllTerm(String tenant_id) throws DataAccessException {
		RowMapper<TerminalModel> rowMapper = new TerminalRowMapper();
		return jdbcTemplate.query(SQL_NOT_TENANT_TERM, rowMapper, tenant_id);
	}

	/**
	 * 端末情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @param term_id 端末ID
	 * @return 端末情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public TerminalModel findTerm(String tenant_id, String term_id) throws DataAccessException {
		RowMapper<TerminalModel> rowMapper = new TerminalRowMapper();
		return (TerminalModel) jdbcTemplate.queryForObject(SQL_FINDTERM, rowMapper, tenant_id, term_id);
	}

	/**
	 * 端末情報を追加します。<br>
	 * @param terminalModel 端末情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int insert(TerminalModel terminalModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
				.addValue(COLUMN_TENANT_ID, terminalModel.getTenant_id())
				.addValue(COLUMN_TERM_ID, terminalModel.getTerm_id())
				.addValue(COLUMN_TERM_CTG, terminalModel.getTerm_ctg())
				.addValue(COLUMN_TERM_NAME, terminalModel.getTerm_name())
				.addValue(COLUMN_LIB_ID, terminalModel.getLib_id())
				.addValue(COLUMN_YEAR_FOLDER_FLG, terminalModel.getYear_folder_flg())
				.addValue(COLUMN_MONTH_FOLDER_FLG, terminalModel.getMonth_folder_flg())
				.addValue(COLUMN_DAY_FOLDER_FLG, terminalModel.getDay_folder_flg())
				.addValue(COLUMN_JAPANESE_FLG, terminalModel.getJapanese_flg())
				.addValue(COLUMN_M_MAX_TIME, terminalModel.getM_max_time())
				.addValue(COLUMN_LIVE_WIDTH, terminalModel.getLive_width())
				.addValue(COLUMN_LIVE_MIN_FPS, terminalModel.getLive_min_fps())
				.addValue(COLUMN_LIVE_MAX_FPS, terminalModel.getLive_max_fps())
				.addValue(COLUMN_START_DATE, terminalModel.getStart_date())
				.addValue(COLUMN_END_DATE, terminalModel.getEnd_date())
				.addValue(COLUMN_DEL_FLG, terminalModel.getDel_flg())
				.addValue(COLUMN_UPDATE_USER_ID, terminalModel.getUpdate_user_id()));
	}

	/**
	 * 端末情報を更新します。<br>
	 * @param terminalModel 端末情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int update(TerminalModel terminalModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_UPDATE, new MapSqlParameterSource()
				.addValue(COLUMN_TENANT_ID, terminalModel.getTenant_id())
				.addValue(COLUMN_TERM_ID, terminalModel.getTerm_id())
				.addValue(COLUMN_TERM_CTG, terminalModel.getTerm_ctg())
				.addValue(COLUMN_TERM_NAME, terminalModel.getTerm_name())
				.addValue(COLUMN_LIB_ID, terminalModel.getLib_id())
				.addValue(COLUMN_YEAR_FOLDER_FLG, terminalModel.getYear_folder_flg())
				.addValue(COLUMN_MONTH_FOLDER_FLG, terminalModel.getMonth_folder_flg())
				.addValue(COLUMN_DAY_FOLDER_FLG, terminalModel.getDay_folder_flg())
				.addValue(COLUMN_JAPANESE_FLG, terminalModel.getJapanese_flg())
				.addValue(COLUMN_M_MAX_TIME, terminalModel.getM_max_time())
				.addValue(COLUMN_LIVE_WIDTH, terminalModel.getLive_width())
				.addValue(COLUMN_LIVE_MIN_FPS, terminalModel.getLive_min_fps())
				.addValue(COLUMN_LIVE_MAX_FPS, terminalModel.getLive_max_fps())
				.addValue(COLUMN_START_DATE, terminalModel.getStart_date())
				.addValue(COLUMN_END_DATE, terminalModel.getEnd_date())
				.addValue(COLUMN_DEL_FLG, terminalModel.getDel_flg())
				.addValue(COLUMN_UPDATE_USER_ID, terminalModel.getUpdate_user_id()));
	}

}
