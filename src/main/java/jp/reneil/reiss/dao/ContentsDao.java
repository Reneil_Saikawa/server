/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.ContentsModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * コンテンツ_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface ContentsDao {

	/**
	 * コンテンツ情報を返却します。（登録番号から検索）<br>
	 * @param reg_no 登録番号
	 * @return コンテンツ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	ContentsModel getContentsData_regno(String reg_no) throws DataAccessException;

	/**
	 * コンテンツ情報を返却します。（ファイル名から検索）<br>
	 * @param file_name ファイル名
	 * @return コンテンツ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	ContentsModel getContentsData_filename(String file_name) throws DataAccessException;

	/**
	 * コンテンツ情報を返却します。（テナントID,ファイル名から検索 = 端末名取得）<br>
	 * @param tenant_id テナントID
	 * @param file_name ファイル名
	 * @return コンテンツ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	ContentsModel getContentsData_tenantid_filename(String tenant_id, String file_name) throws DataAccessException;

	/**
	 * コンテンツの存在確認をします。（登録番号から検索）<br>
	 * @param reg_no 登録番号
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int findContents_regno(String reg_no) throws DataAccessException;

	/**
	 * コンテンツの存在確認をします。（ファイル名から検索）<br>
	 * @param file_name ファイル名
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int findContents_filename(String file_name) throws DataAccessException;

	/**
	 * コンテンツ情報を追加します。<br>
	 * @param contentsModel コンテンツ情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int insert(ContentsModel contentsModel) throws DataAccessException;

	/**
	 * コンテンツ情報を更新します。<br>
	 * @param contentsModel コンテンツ情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int update(ContentsModel contentsModel) throws DataAccessException;

	/**
	 * コンテンツファイルパスを更新します。<br>
	 * @param reg_no 登録番号
	 * @param file_path ファイルパス
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void update_path(String reg_no, String file_path) throws DataAccessException;

	/**
	 * コンテンツファイルパスを更新します。(LIKE)<br>
	 * @param old_file_path 旧ファイルパス
	 * @param new_file_path 新ファイルパス
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void update_path_like(String old_file_path, String new_file_path) throws DataAccessException;

	/**
	 * コンテンツ情報を削除します。<br>
	 * @param reg_no 登録番号
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void deleteContents_regno(String reg_no) throws DataAccessException;

}
