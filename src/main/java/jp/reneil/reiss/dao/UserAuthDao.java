/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.UserAuthModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザー権限_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface UserAuthDao {

	/**
	 * ユーザー権限の存在確認をします。<br>
	 * @param user_id ユーザID
	 * @param auth_cd 権限コード
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int count(String user_id,String auth_cd) throws DataAccessException;

	/**
	 * ユーザー情報を追加します。<br>
	 * @param userAuthModel ユーザー権限モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int insert(UserAuthModel userAuthModel) throws DataAccessException;

	/**
	 * ユーザー権限を削除します。<br>
	 * @param user_id ユーザID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void delete(String user_id) throws DataAccessException;

}
