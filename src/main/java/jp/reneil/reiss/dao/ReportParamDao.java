/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.ReportParamModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 帳票パラメータ_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface ReportParamDao {

	/**
	 * 帳票パラメータの存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @param report_id 帳票ID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int count(String tenant_id, String report_id) throws DataAccessException;

	/**
	 * 帳票パラメータ使用可能確認をします。<br>
	 * @param tenant_id テナントID
	 * @param report_id 帳票ID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int limitCount(String tenant_id, String report_id) throws DataAccessException;

	/**
	 * 帳票ファイル名の存在確認をします。（追加確認用）<br>
	 * @param tenant_id テナントID
	 * @param file_name 帳票ファイル名
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int addFileCount(String tenant_id, String file_name) throws DataAccessException;

	/**
	 * 帳票ファイル名の存在確認をします。（更新確認用）<br>
	 * @param tenant_id テナントID
	 * @param file_name 帳票ファイル名
	 * @param report_id 帳票ID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int updateFileCount(String tenant_id, String file_name, String report_id) throws DataAccessException;

	/**
	 * 帳票パラメータ情報を返却します。（すべて）<br>
	 * @return 帳票パラメータ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<ReportParamModel> findAll() throws DataAccessException;

	/**
	 * 帳票パラメータ情報を返却します。（テナント）
	 * @param tenant_id テナントID
	 * @return 該当帳票パラメータ情報
	 * @throws DataAccessException
	 */
	List<ReportParamModel> findTenant(String tenant_id) throws DataAccessException;

	/**
	 * 帳票パラメータ情報を返却します。（テナント管理者）<br>
	 * @param tenant_id テナントID
	 * @return 帳票パラメータ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<ReportParamModel> findTenantManager(String tenant_id) throws DataAccessException;

	/**
	 * 帳票パラメータ情報を返却します。（ユーザー）
	 * @param tenant_id テナントID
	 * @param user_id ユーザーID
	 * @return 該当帳票パラメータ情報
	 * @throws DataAccessException
	 */
	List<ReportParamModel> findUser(String tenant_id, String user_id) throws DataAccessException;

	/**
	 * 帳票パラメータ情報を返却します。（帳票出力時）<br>
	 * @param tenant_id テナントID
	 * @param report_id 帳票ID
	 * @return テナント情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	ReportParamModel findOutput(String tenant_id, String report_id) throws DataAccessException;

	/**
	 * 帳票パラメータ情報を追加します。<br>
	 * @param reportParamModel 帳票パラメータ情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int insert(ReportParamModel reportParamModel) throws DataAccessException;

	/**
	 * 帳票パラメータ情報を更新します。<br>
	 * @param reportParamModel 帳票パラメータ情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int update(ReportParamModel reportParamModel) throws DataAccessException;

	/**
	 * 帳票パラメータ情報を削除します。<br>
	 * @param tenant_id テナントID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void delete(String tenant_id) throws DataAccessException;

}
