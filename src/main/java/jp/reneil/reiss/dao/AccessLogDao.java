/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.AccessLogCsvModel;
import jp.reneil.reiss.model.AccessLogModel;
import jp.reneil.reiss.model.AccessLogSysCsvModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * アクセスログ_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface AccessLogDao {

	/**
	 * アクセスログを返却します。<br>
	 * @return アクセスログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<AccessLogModel> select() throws DataAccessException;

	/**
	 * アクセスログを検索します。<br>
	 * @param start_date 検索開始日時
	 * @param end_date 検索終了日時
	 * @param tenant_id テナントID
	 * @param user_id ユーザーID
	 * @param execution_code 実行プログラムコード
	 * @param result_code 処理結果コード
	 * @return アクセスログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<AccessLogModel> find(String start_date, String end_date, String tenant_id, String user_id, String execution_code, String result_code) throws DataAccessException;

	/**
	 * アクセスログcsvを検索します。<br>
	 * @param start_date 検索開始日時
	 * @param end_date 検索終了日時
	 * @param tenant_id テナントID
	 * @param user_id ユーザーID
	 * @param execution_code 実行プログラムコード
	 * @param result_code 処理結果コード
	 * @return アクセスログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<AccessLogCsvModel> findCsv(String start_date, String end_date, String tenant_id, String user_id, String execution_code, String result_code) throws DataAccessException;

	/**
	 * アクセスログsysCsvを検索します。<br>
	 * @param start_date 検索開始日時
	 * @param end_date 検索終了日時
	 * @param tenant_id テナントID
	 * @param user_id ユーザーID
	 * @param execution_code 実行プログラムコード
	 * @param result_code 処理結果コード
	 * @return アクセスログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<AccessLogSysCsvModel> findSysCsv(String start_date, String end_date, String tenant_id, String user_id, String execution_code, String result_code) throws DataAccessException;

	/**
	 * アクセスログを返却します。（アクセス番号から検索）<br>
	 * @param access_no アクセス番号
	 * @return アクセスログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	AccessLogModel accessNoSelect(String access_no) throws DataAccessException;

	/**
	 * アクセスログを追加します。<br>
	 * @param accessLogModel アクセスログモデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int insert(AccessLogModel accessLogModel) throws DataAccessException;

}
