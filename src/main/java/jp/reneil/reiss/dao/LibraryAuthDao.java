/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2020/04/07      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.LibraryAuthModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ライブラリ権限_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface LibraryAuthDao {

	/**
	 * ユーザのライブラリ権限情報を返却します。<br>
	 * @param user_id ユーザーID
	 * @return ユーザライブラリ権限情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<LibraryAuthModel> findUser(String user_id) throws DataAccessException;

	/**
	 * ライブラリ権限の存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @param user_id ユーザID
	 * @param lib_id ライブラリID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int count(String tenant_id, String user_id, String lib_id) throws DataAccessException;

	/**
	 * ライブラリ情報を追加します。<br>
	 * @param libraryAuthModel ライブラリ権限モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int insert(LibraryAuthModel libraryAuthModel) throws DataAccessException;

	/**
	 * ライブラリ権限を削除します。<br>
	 * @param user_id ユーザID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void delete(String user_id) throws DataAccessException;

}
