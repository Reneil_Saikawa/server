/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.model.TenantModel;
import jp.reneil.reiss.model.UserInfoModel;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザー情報_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.0
 */
public class UserInfoDaoImpl extends JdbcDaoSupport implements UserInfoDao {

	/** ユーザ権限テーブルアクセス制御クラス */
	@Autowired
	private UserAuthDao userAuthDao;

	/** テナントテーブルアクセス制御クラス */
	@Autowired
	private TenantDao tenantDao;

	/** カラム名：ユーザーID */
	private static final String COLUMN_USER_ID = "USER_ID";

	/** カラム名：ユーザー名 */
	private static final String COLUMN_USER_NAME = "USER_NAME";

	/** カラム名：テナントID */
	private static final String COLUMN_TENANT_ID = "TENANT_ID";

	/** カラム名：権限コード */
	private static final String COLUMN_AUTH_CD = "AUTH_CD";

	/** カラム名：パスワード */
	private static final String COLUMN_PASSWORD = "PWD";

	/** カラム名：パスワード更新 */
	private static final String COLUMN_PWD_UPDATE = "PWD_UPDATE";

	/** カラム名：パスワードエラーカウント */
	private static final String COLUMN_PWD_ERRCNT = "PWD_ERRCNT";

	/** カラム名：最終ログイン日時 */
	private static final String COLUMN_LAST_LOGIN_TIME = "LAST_LOGIN_TIME";

	/** カラム名：有効開始日 */
	private static final String COLUMN_START_DATE = "START_DATE";

	/** カラム名：有効終了日 */
	private static final String COLUMN_END_DATE = "END_DATE";

	/** カラム名：削除フラグ */
	private static final String COLUMN_DEL_FLG = "DEL_FLG";

	/** カラム名：更新ユーザーID */
	private static final String COLUMN_UPDATE_USER_ID = "UPDATE_USER_ID";

	/** カラム名：更新日*/
	private static final String COLUMN_UPDATE_DATE = "UPDATE_DATE";

	/** 全ユーザー情報取得 */
	private static final String SQL_ALL_USER =
			  "SELECT"
			+ " USER_ID,"
			+ " USER_NAME,"
			+ " TENANT_ID,"
			+ " AUTH_CD,"
			+ " PWD,"
			+ " PWD_UPDATE,"
			+ " PWD_ERRCNT,"
			+ " LAST_LOGIN_TIME,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM USERINFO"
			+ " ORDER BY TENANT_ID,"
			+ "          USER_ID";

	/** テナント管理者情報取得 */
	private static final String SQL_TENANT_MANAGER =
			  "SELECT"
			+ " USER_ID,"
			+ " USER_NAME,"
			+ " TENANT_ID,"
			+ " AUTH_CD,"
			+ " PWD,"
			+ " PWD_UPDATE,"
			+ " PWD_ERRCNT,"
			+ " LAST_LOGIN_TIME,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM USERINFO"
			+ " WHERE AUTH_CD = "+ CommonConst.TENANT_MANAGER_CD;

	/** ユーザー情報取得 */
	private static final String SQL_GET_USER =
			  "SELECT"
			+ " USER_ID,"
			+ " USER_NAME,"
			+ " TENANT_ID,"
			+ " AUTH_CD,"
			+ " PWD,"
			+ " PWD_UPDATE,"
			+ " PWD_ERRCNT,"
			+ " LAST_LOGIN_TIME,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM USERINFO"
			+ " WHERE USER_ID = ?"
			+ " AND START_DATE <= GETDATE() AND END_DATE >= GETDATE() AND DEL_FLG = 0";

	/** 更新前ユーザー情報取得 */
	private static final String SQL_GET_OLD_USER =
			  "SELECT"
			+ " USER_ID,"
			+ " USER_NAME,"
			+ " TENANT_ID,"
			+ " AUTH_CD,"
			+ " PWD,"
			+ " PWD_UPDATE,"
			+ " PWD_ERRCNT,"
			+ " LAST_LOGIN_TIME,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM USERINFO"
			+ " WHERE USER_ID = ?";

	/** テナントに所属するユーザー情報取得 */
	private static final String SQL_TENANT_USER =
			  "SELECT"
			+ " USER_ID,"
			+ " USER_NAME,"
			+ " TENANT_ID,"
			+ " AUTH_CD,"
			+ " PWD,"
			+ " PWD_UPDATE,"
			+ " PWD_ERRCNT,"
			+ " LAST_LOGIN_TIME,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM USERINFO"
			+ " WHERE TENANT_ID = ?";

	/** 選択テナント以外に所属するユーザー情報取得 */
	private static final String SQL_NOT_TENANT_USER =
			  "SELECT"
			+ " USER_ID,"
			+ " USER_NAME,"
			+ " TENANT_ID,"
			+ " AUTH_CD,"
			+ " PWD,"
			+ " PWD_UPDATE,"
			+ " PWD_ERRCNT,"
			+ " LAST_LOGIN_TIME,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM USERINFO"
			+ " WHERE TENANT_ID <> ?"
			+ " ORDER BY TENANT_ID,"
			+ "          USER_ID";

	/** ユーザーの存在確認 */
	private static final String SQL_FIND_USER =
			  "SELECT COUNT(USER_ID)"
			+ " FROM USERINFO"
			+ " WHERE USER_ID = ?"
			+ " AND START_DATE <= GETDATE() AND END_DATE >= GETDATE() AND DEL_FLG = 0";

	/** 最終ログイン日時の更新（パスワードエラーカウントリセット） */
	private static final String SQL_UPDATE_LAST_LOGIN_TIME =
			  "UPDATE USERINFO"
			+ " SET PWD_ERRCNT = 0,"
			+ " LAST_LOGIN_TIME = GETDATE()"
			+ " WHERE USER_ID = ?";

	/** パスワードエラーカウントの更新 */
	private static final String SQL_UPDATE_ERRCNT =
			  "UPDATE USERINFO"
			+ " SET PWD_ERRCNT = ?"
			+ " WHERE USER_ID = ?";

	/** ユーザー情報件数取得 */
	private static final String SQL_COUNT =
			"SELECT COUNT(USER_ID)"
			+ " FROM USERINFO"
			+ " WHERE USER_ID = ?";

	/** ライセンスユーザ数取得 */
	private static final String SQL_LICENSE_COUNT=
			"SELECT COUNT(USER_ID)"
			+ " FROM USERINFO"
			+ " WHERE TENANT_ID = ?"
			+ " AND AUTH_CD = " + CommonConst.USER_CD
			+ " AND DEL_FLG = 0";

	/** ユーザー情報追加 */
	private static final String SQL_INSERT =
			"INSERT INTO USERINFO("
			+ " USER_ID,"
			+ " USER_NAME,"
			+ " TENANT_ID,"
			+ " AUTH_CD,"
			+ " PWD,"
			+ " PWD_UPDATE,"
			+ " PWD_ERRCNT,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE)"
			+ " VALUES("
			+ " :USER_ID,"
			+ " :USER_NAME,"
			+ " :TENANT_ID,"
			+ " :AUTH_CD,"
			+ " :PWD,"
			+ " GETDATE(),"
			+ " 0,"
			+ " :START_DATE,"
			+ " DATEADD(HOUR, 23, DATEADD(MINUTE, 59, DATEADD(SECOND, 59, :END_DATE))),"
			+ " :DEL_FLG,"
			+ " :UPDATE_USER_ID,"
			+ " GETDATE())";

	/** ユーザー情報更新 */
	private static final String SQL_UPDATE =
			  "UPDATE USERINFO"
			+ " SET USER_ID = :USER_ID,"
			+ " USER_NAME = :USER_NAME,"
			+ " TENANT_ID = :TENANT_ID,"
			+ " AUTH_CD = :AUTH_CD,"
			+ " PWD = :PWD,"
			+ " PWD_UPDATE = GETDATE(),"
			+ " PWD_ERRCNT = :PWD_ERRCNT,"
			+ " START_DATE = :START_DATE,"
			+ " END_DATE = DATEADD(HOUR, 23, DATEADD(MINUTE, 59, DATEADD(SECOND, 59, :END_DATE))),"
			+ " DEL_FLG = :DEL_FLG,"
			+ " UPDATE_USER_ID = :UPDATE_USER_ID,"
			+ " UPDATE_DATE = GETDATE()"
			+ " WHERE USER_ID = :USER_ID";

	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** JDBCテンプレートクラス（リプレースあり） */
	@Autowired
	private NamedParameterJdbcTemplate npJdbcTemplate;

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * ログインユーザー情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class UserRowMapper_Login implements RowMapper<UserInfoModel> {

		/**
		 * ログイン時ユーザー情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public UserInfoModel mapRow(ResultSet rs, int rowNum) throws SQLException {

			// ユーザークラス生成
			UserInfoModel model = new UserInfoModel();

			// ユーザーID
			model.setUser_id(rs.getString(COLUMN_USER_ID));
			// ユーザー名
			model.setUser_name(rs.getString(COLUMN_USER_NAME));
			// テナントID
			model.setTenant_id(rs.getString(COLUMN_TENANT_ID));
			// ユーザ種別コード
			model.setAuth_cd(rs.getString(COLUMN_AUTH_CD));
			// パスワード
			model.setPwd(rs.getString(COLUMN_PASSWORD));
			// パスワード更新日
			model.setPwd_update(rs.getTimestamp(COLUMN_PWD_UPDATE));
			// パスワードエラーカウント
			model.setPwd_errcnt(rs.getInt(COLUMN_PWD_ERRCNT));

			return model;
		}
	}

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * ユーザー情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class UserRowMapper implements RowMapper<UserInfoModel> {

		/**
		 * ユーザー情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public UserInfoModel mapRow(ResultSet rs, int rowNum) throws SQLException {

			// ユーザークラス生成
			UserInfoModel model = new UserInfoModel();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			// ユーザーID
			model.setUser_id(rs.getString(COLUMN_USER_ID));
			// ユーザー名
			model.setUser_name(rs.getString(COLUMN_USER_NAME));
			// テナントID
			model.setTenant_id(rs.getString(COLUMN_TENANT_ID));
			// ユーザ種別コード
			model.setAuth_cd(rs.getString(COLUMN_AUTH_CD));
			// 最終ログイン日時
			model.setLast_login_time(rs.getTimestamp(COLUMN_LAST_LOGIN_TIME));
			// String 最終ログイン日時
			model.setStr_last_login_time((rs.getTimestamp(COLUMN_LAST_LOGIN_TIME) == null) ? "" : fmt.format(rs.getTimestamp(COLUMN_LAST_LOGIN_TIME)));

			// 管理権限
			if (userAuthDao.count(rs.getString(COLUMN_USER_ID), CommonConst.MANAGE_AUTH) == 1) {
				model.setManage_auth("✔");
			}
			// 帳票出力権限
			if (userAuthDao.count(rs.getString(COLUMN_USER_ID), CommonConst.OUTPUT_AUTH) == 1) {
				model.setOutput_auth("✔");
			}
			// 付帯情報編集権限
			if (userAuthDao.count(rs.getString(COLUMN_USER_ID), CommonConst.EDIT_AUTH) == 1) {
				model.setEdit_auth("✔");
			}
			// ダウンロード権限
			if (userAuthDao.count(rs.getString(COLUMN_USER_ID), CommonConst.DOWNLOAD_AUTH) == 1) {
				model.setDownload_auth("✔");
			}
			// 移動権限
			if (userAuthDao.count(rs.getString(COLUMN_USER_ID), CommonConst.MOVE_AUTH) == 1) {
				model.setMove_auth("✔");
			}
			// 削除権限
			if (userAuthDao.count(rs.getString(COLUMN_USER_ID), CommonConst.DELETE_AUTH) == 1) {
				model.setDelete_auth("✔");
			}

			return model;
		}
	}

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * 管理_ユーザー情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class UserRowMapper_Manage implements RowMapper<UserInfoModel> {

		/**
		 * ユーザー情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public UserInfoModel mapRow(ResultSet rs, int rowNum) throws SQLException {

			/** TripleDES変換 */
			TripleDesConverter tripleDesConverter = null;
			try {
				tripleDesConverter = new TripleDesConverter();
			} catch (Exception e) {
				e.printStackTrace();
			}

			// ユーザークラス生成
			UserInfoModel model = new UserInfoModel();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");

			// ユーザーID
			model.setUser_id(rs.getString(COLUMN_USER_ID));
			// ユーザー名
			model.setUser_name(rs.getString(COLUMN_USER_NAME));
			// テナントID
			model.setTenant_id(rs.getString(COLUMN_TENANT_ID));
			// ユーザ種別コード
			model.setAuth_cd(rs.getString(COLUMN_AUTH_CD));
			// パスワード
			model.setPwd(rs.getString(COLUMN_PASSWORD));
			// 複合化パスワード
			try {
				model.setDec_pwd(tripleDesConverter.decrypt64(rs.getString(COLUMN_PASSWORD)));
			} catch (Exception e) {
				e.printStackTrace();
			}
			// パスワード更新日
			model.setPwd_update(rs.getTimestamp(COLUMN_PWD_UPDATE));
			// パスワードエラーカウント
			model.setPwd_errcnt(rs.getInt(COLUMN_PWD_ERRCNT));
			// 有効開始日
			model.setStart_date(rs.getDate(COLUMN_START_DATE));
			// 有効終了日
			model.setEnd_date(rs.getDate(COLUMN_END_DATE));
			// String 有効開始日
			model.setStr_start_date(fmt.format(rs.getDate(COLUMN_START_DATE)));
			// String 有効終了日
			model.setStr_end_date(fmt.format(rs.getDate(COLUMN_END_DATE)));
			// 削除フラグ
			model.setDel_flg(rs.getByte(COLUMN_DEL_FLG));

			if (rs.getByte(COLUMN_DEL_FLG) == (byte) 1) {
				// String 削除フラグ
				model.setStr_del_flg("✔");
			}

			//更新ユーザーID
			model.setUpdate_user_id(rs.getString(COLUMN_UPDATE_USER_ID));
			//更新日
			model.setUpdate_date(rs.getTimestamp(COLUMN_UPDATE_DATE));

			// 管理権限
			if (userAuthDao.count(rs.getString(COLUMN_USER_ID), CommonConst.MANAGE_AUTH) == 1) {
				model.setManage_auth("✔");
			}
			// 帳票出力権限
			if (userAuthDao.count(rs.getString(COLUMN_USER_ID), CommonConst.OUTPUT_AUTH) == 1) {
				model.setOutput_auth("✔");
			}
			// 付帯情報編集権限
			if (userAuthDao.count(rs.getString(COLUMN_USER_ID), CommonConst.EDIT_AUTH) == 1) {
				model.setEdit_auth("✔");
			}
			// ダウンロード権限
			if (userAuthDao.count(rs.getString(COLUMN_USER_ID), CommonConst.DOWNLOAD_AUTH) == 1) {
				model.setDownload_auth("✔");
			}
			// 移動権限
			if (userAuthDao.count(rs.getString(COLUMN_USER_ID), CommonConst.MOVE_AUTH) == 1) {
				model.setMove_auth("✔");
			}
			// 削除権限
			if (userAuthDao.count(rs.getString(COLUMN_USER_ID), CommonConst.DELETE_AUTH) == 1) {
				model.setDelete_auth("✔");
			}

			// テナント名取得
			if (tenantDao.count(rs.getString(COLUMN_TENANT_ID)) == 1) {
				//取得したテナントIDからテナント情報を取得
				TenantModel tenant = tenantDao.findTenant(rs.getString(COLUMN_TENANT_ID));
				model.setTenant_name(tenant.getTenant_name());
			}

			return model;
		}
	}

	/**
	 * ログインユーザー情報を返却します。<br>
	 * @param user_id ユーザーID
	 * @return ログインユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public UserInfoModel getLoginUser(String user_id) throws DataAccessException {
		RowMapper<UserInfoModel> rowMapper = new UserRowMapper_Login();
		return (UserInfoModel) jdbcTemplate.queryForObject(SQL_GET_USER, rowMapper, user_id);
	}

	/**
	 * ユーザー情報を返却します。<br>
	 * @param user_id ユーザーID
	 * @return ユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public UserInfoModel getUser(String user_id) throws DataAccessException {
		RowMapper<UserInfoModel> rowMapper = new UserRowMapper();
		return (UserInfoModel) jdbcTemplate.queryForObject(SQL_GET_USER, rowMapper, user_id);
	}

	/**
	 * 更新前ユーザー情報を返却します。<br>
	 * @param user_id ユーザーID
	 * @return ユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public UserInfoModel getOldUser(String user_id) throws DataAccessException {
		RowMapper<UserInfoModel> rowMapper = new UserRowMapper_Manage();
		return (UserInfoModel) jdbcTemplate.queryForObject(SQL_GET_OLD_USER, rowMapper, user_id);
	}

	/**
	 * 管理_ユーザー情報を返却します。<br>
	 * @param user_id ユーザーID
	 * @return ユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public UserInfoModel getUserManage(String user_id) throws DataAccessException {
		RowMapper<UserInfoModel> rowMapper = new UserRowMapper_Manage();
		return (UserInfoModel) jdbcTemplate.queryForObject(SQL_GET_USER, rowMapper, user_id);
	}

	/**
	 * ログイン時ユーザーの存在確認をします。<br>
	 * @param user_id ユーザーID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int findUser(String user_id) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_FIND_USER, Integer.class, user_id);
	}

	/**
	 * ライセンスユーザ数を返却します。<br>
	 * @param tenant_id テナントID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int licenseCount(String tenant_id) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_LICENSE_COUNT, Integer.class, tenant_id);
	}

	/**
	 * 最終ログイン日時を更新します。（パスワードエラーカウントリセット）<br>
	 * @param user_id ユーザーID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public void updateLastLoginTime(String user_id) throws DataAccessException {
		jdbcTemplate.update(SQL_UPDATE_LAST_LOGIN_TIME, user_id);
	}

	/**
	 * パスワードのエラー回数を更新します。<br>
	 * @param user_id ユーザーID
	 * @param pwd_errcnt パスワードエラーカウント
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public void updateErrcnt(String user_id, int pwd_errcnt) throws DataAccessException {
		jdbcTemplate.update(SQL_UPDATE_ERRCNT, pwd_errcnt, user_id);
	}

	/**
	 * テナント管理者情報を返却します。<br>
	 * @return テナント管理者情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<UserInfoModel> findAll() {
		RowMapper<UserInfoModel> rowMapper = new UserRowMapper();
		return jdbcTemplate.query(SQL_ALL_USER, rowMapper);
	}

	/**
	 * テナント管理者情報を返却します。<br>
	 * @return テナント管理者情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<UserInfoModel> findTenantManager() {
		RowMapper<UserInfoModel> rowMapper = new UserRowMapper_Manage();
		return jdbcTemplate.query(SQL_TENANT_MANAGER, rowMapper);
	}

	/**
	 * テナントに所属するユーザー情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @return ユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public List<UserInfoModel> findTenantAllUser(String tenant_id) throws DataAccessException {
		RowMapper<UserInfoModel> rowMapper = new UserRowMapper();
		String strSQL = SQL_TENANT_USER;
		strSQL = strSQL + " ORDER BY TENANT_ID, USER_ID";
		return jdbcTemplate.query(strSQL, rowMapper, tenant_id);
	}

	/**
	 * テナントに所属する一般ユーザー情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @return ユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public List<UserInfoModel> findTenantGeneralUser(String tenant_id) throws DataAccessException {
		RowMapper<UserInfoModel> rowMapper = new UserRowMapper_Manage();
		String strSQL = SQL_TENANT_USER;
		strSQL = strSQL + " AND AUTH_CD = "+ CommonConst.USER_CD;
		strSQL = strSQL + " ORDER BY TENANT_ID, USER_ID";
		return jdbcTemplate.query(strSQL, rowMapper, tenant_id);
	}

	/**
	 * 選択テナント以外に所属するユーザー情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @return ユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public List<UserInfoModel> findNotTenantAllUser(String tenant_id) throws DataAccessException {
		RowMapper<UserInfoModel> rowMapper = new UserRowMapper();
		return jdbcTemplate.query(SQL_NOT_TENANT_USER, rowMapper, tenant_id);
	}

	/**
	 * ユーザーの存在確認をします。<br>
	 * @param user_id ユーザーID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int count(String user_id) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_COUNT, Integer.class, user_id);
	}

	/**
	 * ユーザー情報を追加します。<br>
	 * @param userinfoModel ユーザー情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int insert(UserInfoModel userinfoModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
				.addValue(COLUMN_USER_ID, userinfoModel.getUser_id())
				.addValue(COLUMN_USER_NAME, userinfoModel.getUser_name())
				.addValue(COLUMN_TENANT_ID, userinfoModel.getTenant_id())
				.addValue(COLUMN_AUTH_CD, userinfoModel.getAuth_cd())
				.addValue(COLUMN_PASSWORD, userinfoModel.getPwd())
				.addValue(COLUMN_PWD_UPDATE, userinfoModel.getPwd_update())
				.addValue(COLUMN_PWD_ERRCNT, userinfoModel.getPwd_errcnt())
				.addValue(COLUMN_START_DATE, userinfoModel.getStart_date())
				.addValue(COLUMN_END_DATE, userinfoModel.getEnd_date())
				.addValue(COLUMN_DEL_FLG, userinfoModel.getDel_flg())
				.addValue(COLUMN_UPDATE_USER_ID, userinfoModel.getUpdate_user_id()));
	}

	/**
	 * ユーザー情報を更新します。<br>
	 * @param userinfoModel ユーザー情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int update(UserInfoModel userinfoModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_UPDATE, new MapSqlParameterSource()
				.addValue(COLUMN_USER_ID, userinfoModel.getUser_id())
				.addValue(COLUMN_USER_NAME, userinfoModel.getUser_name())
				.addValue(COLUMN_TENANT_ID, userinfoModel.getTenant_id())
				.addValue(COLUMN_AUTH_CD, userinfoModel.getAuth_cd())
				.addValue(COLUMN_PASSWORD, userinfoModel.getPwd())
				.addValue(COLUMN_PWD_UPDATE, userinfoModel.getPwd_update())
				.addValue(COLUMN_PWD_ERRCNT, userinfoModel.getPwd_errcnt())
				.addValue(COLUMN_START_DATE, userinfoModel.getStart_date())
				.addValue(COLUMN_END_DATE, userinfoModel.getEnd_date())
				.addValue(COLUMN_DEL_FLG, userinfoModel.getDel_flg())
				.addValue(COLUMN_UPDATE_USER_ID, userinfoModel.getUpdate_user_id()));
	}

}
