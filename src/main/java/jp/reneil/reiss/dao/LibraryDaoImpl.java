/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/23      1.0.1           女屋            不要マッパークラス削除
 */
package jp.reneil.reiss.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.model.LibraryModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ライブラリ情報_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.1
 */
public class LibraryDaoImpl extends JdbcDaoSupport implements LibraryDao {

	/** カラム名：テナントID */
	private static final String COLUMN_TENANT_ID = "TENANT_ID";

	/** カラム名：ライブラリID */
	private static final String COLUMN_LIB_ID = "LIB_ID";

	/** カラム名：親ライブラリID */
	private static final String COLUMN_PARENT_LID = "PARENT_LID";

	/** カラム名：ライブラリ名 */
	private static final String COLUMN_LIB_NAME = "LIB_NAME";

	/** カラム名：ライブラリパス */
	private static final String COLUMN_LIB_PATH = "LIB_PATH";

	/** カラム名：ライブルーム名 */
	private static final String COLUMN_LIVE_ROOM = "LIVE_ROOM";

	/** カラム名：表示順 */
	private static final String COLUMN_DISPLAY_NO = "DISPLAY_NO";

	/** カラム名：更新ユーザーID */
	private static final String COLUMN_UPDATE_USER_ID = "UPDATE_USER_ID";

	/** カラム名：更新日*/
	private static final String COLUMN_UPDATE_DATE = "UPDATE_DATE";

	/** ライブラリ情報取得 */
	private static final String SQL_FINDLIB =
			  "SELECT"
			+ " TENANT_ID,"
			+ " LIB_ID,"
			+ " PARENT_LID,"
			+ " LIB_NAME,"
			+ " LIB_PATH,"
			+ " LIVE_ROOM,"
			+ " DISPLAY_NO,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM LIBRARY"
			+ " WHERE TENANT_ID = ?"
			+ " AND LIB_ID = ?"
			+ " ORDER BY LIB_PATH";

	/** ライブラリ情報取得 */
	private static final String SQL_FINDPATH =
			  "SELECT"
			+ " TENANT_ID,"
			+ " LIB_ID,"
			+ " PARENT_LID,"
			+ " LIB_NAME,"
			+ " LIB_PATH,"
			+ " LIVE_ROOM,"
			+ " DISPLAY_NO,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM LIBRARY"
			+ " WHERE TENANT_ID = ?"
			+ " AND LIB_PATH = ?"
			+ " ORDER BY LIB_PATH";

	/** ライブラリ情報取得 */
	private static final String SQL_LIKEPATH =
			  "SELECT"
			+ " TENANT_ID,"
			+ " LIB_ID,"
			+ " PARENT_LID,"
			+ " LIB_NAME,"
			+ " LIB_PATH,"
			+ " LIVE_ROOM,"
			+ " DISPLAY_NO,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM LIBRARY"
			+ " WHERE TENANT_ID = ?"
			+ " AND (LIB_PATH = ? OR LIB_PATH LIKE ? ESCAPE '#')"
			+ " ORDER BY LIB_PATH";

	/** テナント所属ライブラリ情報取得 */
	private static final String SQL_TENANT_LIB =
			  "SELECT"
			+ " TENANT_ID,"
			+ " LIB_ID,"
			+ " PARENT_LID,"
			+ " LIB_NAME,"
			+ " LIB_PATH,"
			+ " LIVE_ROOM,"
			+ " DISPLAY_NO,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM LIBRARY"
			+ " WHERE TENANT_ID = ?"
			+ " ORDER BY LIB_PATH";

	/** ユーザ所属ライブラリ情報取得 */
	private static final String SQL_FINDUSER =
			  "SELECT"
			+ " LR.TENANT_ID,"
			+ " LR.LIB_ID,"
			+ " LR.PARENT_LID,"
			+ " LR.LIB_NAME,"
			+ " LR.LIB_PATH,"
			+ " LR.LIVE_ROOM,"
			+ " LR.DISPLAY_NO,"
			+ " LR.UPDATE_USER_ID,"
			+ " LR.UPDATE_DATE"
			+ " FROM LIBRARY LR,"
			+ "      LIBRARYAUTH LA"
			+ " WHERE LR.TENANT_ID = LA.TENANT_ID"
			+ " AND LR.LIB_ID = LA.LIB_ID"
			+ " AND LR.TENANT_ID = ?"
			+ " AND LA.USER_ID = ?"
			+ " ORDER BY LR.LIB_PATH";

	/** 子ライブラリ情報取得 */
	private static final String SQL_FINDKIDSLIB =
			  "SELECT"
			+ " TENANT_ID,"
			+ " LIB_ID,"
			+ " PARENT_LID,"
			+ " LIB_NAME,"
			+ " LIB_PATH,"
			+ " LIVE_ROOM,"
			+ " DISPLAY_NO,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM LIBRARY"
			+ " WHERE TENANT_ID = ?"
			+ " AND PARENT_LID = ?"
			+ " ORDER BY LIB_PATH";

	/** ライブラリ情報表示順取得 */
	private static final String SQL_DISPLAY_NO =
			  "SELECT"
			+ " LIB_PATH,"
			+ " DISPLAY_NO"
			+ " FROM LIBRARY"
			+ " WHERE TENANT_ID = ?";

	/** 子ライブラリ情報件数取得 */
	private static final String SQL_COUNT =
			"SELECT COUNT(LIB_ID)"
			+ " FROM LIBRARY"
			+ " WHERE TENANT_ID = ?"
			+ " AND PARENT_LID = ?";

	/** ライブラリ情報件数取得 */
	private static final String SQL_CHK =
			"SELECT COUNT(LIB_ID)"
			+ " FROM LIBRARY"
			+ " WHERE TENANT_ID = ?"
			+ " AND LIB_ID = ?";

	/** ライブラリ情報件数取得 */
	private static final String SQL_PATH_COUNT =
			"SELECT COUNT(LIB_ID)"
			+ " FROM LIBRARY"
			+ " WHERE TENANT_ID = ?"
			+ " AND LIB_PATH = ?";

	/** ライブラリ情報追加 */
	private static final String SQL_INSERT =
			"INSERT INTO LIBRARY("
			+ " TENANT_ID,"
			+ " LIB_ID,"
			+ " PARENT_LID,"
			+ " LIB_NAME,"
			+ " LIB_PATH,"
			+ " LIVE_ROOM,"
			+ " DISPLAY_NO,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE)"
			+ " VALUES("
			+ " :TENANT_ID,"
			+ " :LIB_ID,"
			+ " :PARENT_LID,"
			+ " :LIB_NAME,"
			+ " :LIB_PATH,"
			+ " :LIVE_ROOM,"
			+ " :DISPLAY_NO,"
			+ " :UPDATE_USER_ID,"
			+ " GETDATE())";

	/** ライブラリ情報更新 */
	private static final String SQL_UPDATE =
			  "UPDATE LIBRARY"
			+ " SET PARENT_LID = :PARENT_LID,"
			+ " LIB_NAME = :LIB_NAME,"
			+ " LIB_PATH = :LIB_PATH,"
			+ " LIVE_ROOM = :LIVE_ROOM,"
			+ " DISPLAY_NO = :DISPLAY_NO,"
			+ " UPDATE_USER_ID = :UPDATE_USER_ID,"
			+ " UPDATE_DATE = GETDATE()"
			+ " WHERE TENANT_ID = :TENANT_ID"
			+ " AND LIB_ID = :LIB_ID";

	/** ライブラリパス更新 */
	private static final String SQL_UPDATE_PATH =
			  "UPDATE LIBRARY"
			+ " SET LIB_PATH = REPLACE(LIB_PATH, ?, ?),"
			+ " UPDATE_DATE = GETDATE()"
			+ " WHERE TENANT_ID = ?"
			+ " AND (LIB_PATH = ? OR LIB_PATH LIKE ? ESCAPE '#')";

	/** ライブラリ情報削除 */
	private static final String SQL_DELETE =
			  "DELETE FROM LIBRARY"
			+ " WHERE TENANT_ID = ?"
			+ " AND LIB_PATH = ?";

	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** JDBCテンプレートクラス（リプレースあり） */
	@Autowired
	private NamedParameterJdbcTemplate npJdbcTemplate;

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * メイン管理ライブラリ情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class LibraryMapper implements RowMapper<LibraryModel> {

		/**
		 * ライブラリ情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public LibraryModel mapRow(ResultSet rs, int rowNum) throws SQLException {
			// ユーザークラス生成
			LibraryModel model = new LibraryModel();
			LibraryModel parentModel = new LibraryModel();

			//テナントID
			model.setTenant_id(rs.getString(COLUMN_TENANT_ID));
			//親ライブラリID
			model.setParent_lid(rs.getString(COLUMN_PARENT_LID));
			//ライブラリID
			model.setLib_id(rs.getString(COLUMN_LIB_ID));
			//ライブラリ名
			model.setLib_name(rs.getString(COLUMN_LIB_NAME));
			//ライブラリ名（表示用）
			model.setLib_name_text(rs.getString(COLUMN_LIB_NAME));

			if (!StringUtils.isEmpty(rs.getString(COLUMN_PARENT_LID))) {
				if (count(rs.getString(COLUMN_TENANT_ID), rs.getString(COLUMN_PARENT_LID), "chk") == 1) {
					parentModel = findLib(rs.getString(COLUMN_TENANT_ID), rs.getString(COLUMN_PARENT_LID));
					//親ライブラリ名
					model.setParent_lname(parentModel.getLib_name());
				}
			}else {
				model.setParent_lname("無し（最上位）");
			}

			//ライブラリパス
			model.setLib_path(rs.getString(COLUMN_LIB_PATH));
			//ライブラリパス(表示用)
			model.setLib_path_text(rs.getString(COLUMN_LIB_PATH) + CommonConst.YEN_MRK);
			//ライブルーム名
			model.setLive_room(rs.getString(COLUMN_LIVE_ROOM));
			//表示順
			model.setDisplay_no(rs.getInt(COLUMN_DISPLAY_NO));
			//更新ユーザーID
			model.setUpdate_user_id(rs.getString(COLUMN_UPDATE_USER_ID));
			//更新日
			model.setUpdate_date(rs.getTimestamp(COLUMN_UPDATE_DATE));

			return model;
		}
	}

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * ライブラリ表示順情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class LibraryDisplayNo implements RowMapper<LibraryModel> {

		/**
		 * ライブラリ表示順情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public LibraryModel mapRow(ResultSet rs, int rowNum) throws SQLException {
			// ユーザークラス生成
			LibraryModel model = new LibraryModel();

			//ライブラリパス
			model.setLib_path(rs.getString(COLUMN_LIB_PATH));
			//表示順
			model.setDisplay_no(rs.getInt(COLUMN_DISPLAY_NO));

			return model;
		}
	}

	/**
	 * ライブラリ情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @param lib_id ライブラリID
	 * @return ライブラリ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public LibraryModel findLib(String tenant_id, String lib_id) throws DataAccessException {
		RowMapper<LibraryModel> rowMapper = new LibraryMapper();
		return (LibraryModel) jdbcTemplate.queryForObject(SQL_FINDLIB, rowMapper,tenant_id,lib_id);
	}

	/**
	 * ライブラリ情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @param lib_path ライブラリパス
	 * @return ライブラリ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public LibraryModel findPath(String tenant_id, String lib_path) throws DataAccessException {
		RowMapper<LibraryModel> rowMapper = new LibraryMapper();
		return (LibraryModel) jdbcTemplate.queryForObject(SQL_FINDPATH, rowMapper,tenant_id,lib_path);
	}

	/**
	 * ライブラリ情報と子ライブラリ情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @param lib_path ライブラリパス
	 * @return ライブラリ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<LibraryModel> likePath(String tenant_id, String lib_path) throws DataAccessException {
		RowMapper<LibraryModel> rowMapper = new LibraryMapper();
		return jdbcTemplate.query(SQL_LIKEPATH, rowMapper, tenant_id, lib_path, lib_path.replace(CommonConst.YEN_MRK, "#" + CommonConst.YEN_MRK) + "#" + CommonConst.YEN_MRK + "%");
	}

	/**
	 * テナント所属ライブラリ情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @return ライブラリ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<LibraryModel> tenantLib(String tenant_id) throws DataAccessException {
		RowMapper<LibraryModel> rowMapper = new LibraryMapper();
		return jdbcTemplate.query(SQL_TENANT_LIB, rowMapper, tenant_id);
	}

	/**
	 * ユーザ閲覧ライブラリ情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @param user_id ユーザーID
	 * @return ライブラリ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<LibraryModel> findUser(String tenant_id, String user_id) throws DataAccessException {
		RowMapper<LibraryModel> rowMapper = new LibraryMapper();
		return jdbcTemplate.query(SQL_FINDUSER, rowMapper, tenant_id, user_id);
	}

	/**
	 * 子ライブラリ情報を返却します
	 * @param tenant_id テナントID
	 * @param lib_id ライブラリID
	 * @return ライブラリ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<LibraryModel> findPKLib(String tenant_id, String lib_id) throws DataAccessException {
		RowMapper<LibraryModel> rowMapper = new LibraryMapper();
		return jdbcTemplate.query(SQL_FINDKIDSLIB, rowMapper, tenant_id, lib_id);
	}

	/**
	 * テナント所属ライブラリパス表示順を返却します。<br>
	 * @param tenant_id テナントID
	 * @return ライブラリパス,表示順
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<LibraryModel> libDisplayNo(String tenant_id) throws DataAccessException {
		RowMapper<LibraryModel> rowMapper = new LibraryDisplayNo();
		return jdbcTemplate.query(SQL_DISPLAY_NO, rowMapper,tenant_id);
	}

	/**
	 * ライブラリの存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @param lib_id ライブラリID
	 * @param chk 実行SQLチェック
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int count(String tenant_id, String lib_id, String chk) throws DataAccessException {
		//実行するSQLを変更するチェックの確認
		String dataCount;
		if(chk.equals("chk")) {
			dataCount = SQL_CHK;
		}else {
			dataCount = SQL_COUNT;
		}
		return jdbcTemplate.queryForObject(dataCount, Integer.class, tenant_id, lib_id);
	}

	/**
	 * パスからライブラリの存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @param lib_path ライブラリパス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int libCount(String tenant_id, String lib_path) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_PATH_COUNT, Integer.class, tenant_id, lib_path);
	}

	/**
	 * ライブラリ情報を追加します。<br>
	 * @param libraryModel ライブラリ情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int insert(LibraryModel libraryModel) throws DataAccessException {
		return  npJdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
				.addValue(COLUMN_TENANT_ID, libraryModel.getTenant_id())
				.addValue(COLUMN_LIB_ID, libraryModel.getLib_id())
				.addValue(COLUMN_PARENT_LID, libraryModel.getParent_lid())
				.addValue(COLUMN_LIB_NAME, libraryModel.getLib_name())
				.addValue(COLUMN_LIB_PATH, libraryModel.getLib_path())
				.addValue(COLUMN_LIVE_ROOM, libraryModel.getLive_room())
				.addValue(COLUMN_DISPLAY_NO, libraryModel.getDisplay_no())
				.addValue(COLUMN_UPDATE_USER_ID, libraryModel.getUpdate_user_id()));
	}

	/**
	 * ライブラリ情報を更新します。<br>
	 * @param libraryModel ライブラリ情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int update(LibraryModel libraryModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_UPDATE, new MapSqlParameterSource()
				.addValue(COLUMN_TENANT_ID, libraryModel.getTenant_id())
				.addValue(COLUMN_LIB_ID, libraryModel.getLib_id())
				.addValue(COLUMN_PARENT_LID, libraryModel.getParent_lid())
				.addValue(COLUMN_LIB_NAME, libraryModel.getLib_name())
				.addValue(COLUMN_LIB_PATH, libraryModel.getLib_path())
				.addValue(COLUMN_LIVE_ROOM, libraryModel.getLive_room())
				.addValue(COLUMN_DISPLAY_NO, libraryModel.getDisplay_no())
				.addValue(COLUMN_UPDATE_USER_ID, libraryModel.getUpdate_user_id()));
	}

	/**
	 * 子ライブラリパスを更新します。<br>
	 * @param old_lib_path 旧ライブラリパス
	 * @param new_lib_path 新ライブラリパス
	 * @param tenant_id テナントID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public void update_path(String old_lib_path, String new_lib_path, String tenant_id) throws DataAccessException {
		jdbcTemplate.update(SQL_UPDATE_PATH, old_lib_path, new_lib_path, tenant_id, old_lib_path, old_lib_path.replace(CommonConst.YEN_MRK, "#" + CommonConst.YEN_MRK) + "#" + CommonConst.YEN_MRK + "%");
	}

	/**
	 * ライブラリ情報を削除します。<br>
	 * @param tenant_id テナントID
	 * @param lib_path ライブラリパス
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public void delete(String tenant_id, String lib_path) throws DataAccessException {
		jdbcTemplate.update(SQL_DELETE, tenant_id, lib_path);
	}
}
