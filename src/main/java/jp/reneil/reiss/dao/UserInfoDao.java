/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.UserInfoModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザー情報_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface UserInfoDao {

	/**
	 * ログインユーザー情報を返却します。<br>
	 * @param user_id ユーザーID
	 * @return ログインユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	UserInfoModel getLoginUser(String user_id) throws DataAccessException;

	/**
	 * ユーザー情報を返却します。<br>
	 * @param user_id ユーザーID
	 * @return ユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	UserInfoModel getUser(String user_id) throws DataAccessException;

	/**
	 * 更新前ユーザー情報を返却します。<br>
	 * @param user_id ユーザーID
	 * @return ユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	UserInfoModel getOldUser(String user_id) throws DataAccessException;

	/**
	 * 管理_ユーザー情報を返却します。<br>
	 * @param user_id ユーザーID
	 * @return ユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	UserInfoModel getUserManage(String user_id) throws DataAccessException;

	/**
	 * ユーザーの存在確認をします。<br>
	 * @param user_id ユーザーID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int findUser(String user_id) throws DataAccessException;

	/**
	 * ライセンスユーザ数を返却します。<br>
	 * @param tenant_id テナントID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int licenseCount(String tenant_id) throws DataAccessException;

	/**
	 * 最終ログイン日時を更新します。（エラーカウントリセット）<br>
	 * @param user_id ユーザーID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void updateLastLoginTime(String user_id) throws DataAccessException;

	/**
	 * パスワードのエラー回数を更新します。<br>
	 * @param user_id ユーザーID
	 * @param pwd_errcnt パスワードエラーカウント
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void updateErrcnt(String user_id, int pwd_errcnt) throws DataAccessException;

	/**
	 * ユーザー情報を返却します。<br>
	 * @return ユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<UserInfoModel> findAll() throws DataAccessException;

	/**
	 * テナント管理者情報を返却します。<br>
	 * @return テナント管理者情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<UserInfoModel> findTenantManager() throws DataAccessException;

	/**
	 * テナントに所属するユーザー情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @return ユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<UserInfoModel> findTenantAllUser(String tenant_id) throws DataAccessException;

	/**
	 * テナントに所属する一般ユーザー情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @return ユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<UserInfoModel> findTenantGeneralUser(String tenant_id) throws DataAccessException;

	/**
	 * 選択テナント以外に所属するユーザー情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @return ユーザー情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<UserInfoModel> findNotTenantAllUser(String tenant_id) throws DataAccessException;

	/**
	 * テナント管理者の存在確認をします。<br>
	 * @param user_id ユーザーID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int count(String user_id) throws DataAccessException;

	/**
	 * ユーザー情報を追加します。<br>
	 * @param userinfoModel ユーザー情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int insert(UserInfoModel userinfoModel) throws DataAccessException;

	/**
	 * ユーザー情報を更新します。<br>
	 * @param userinfoModel ユーザー情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int update(UserInfoModel userinfoModel) throws DataAccessException;

}
