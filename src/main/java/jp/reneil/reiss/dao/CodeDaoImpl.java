/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.model.CodeModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * コード_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.0
 */
public class CodeDaoImpl extends JdbcDaoSupport implements CodeDao {

	/** カラム名：分類コード */
	private static final String COLUMN_CODE_01 = "CODE_01";

	/** カラム名：コード */
	private static final String COLUMN_CODE_02 = "CODE_02";

	/** カラム名：名称1 */
	private static final String COLUMN_NAME_01 = "NAME_01";

	/** カラム名：名称2 */
	private static final String COLUMN_NAME_02 = "NAME_02";

	/** カラム名：名称3 */
	private static final String COLUMN_NAME_03 = "NAME_03";

	/** カラム名：備考 */
	private static final String COLUMN_REMARKS = "REMARKS";

	/** カラム名：更新ユーザーID */
	private static final String COLUMN_UPDATE_USER_ID = "UPDATE_USER_ID";

	/** カラム名：更新日 */
	private static final String COLUMN_UPDATE_DATE = "UPDATE_DATE";

	/** コード情報取得 */
	private static final String SQL_SELECT_CODE =
			  "SELECT * "
			+ "FROM CODE "
			+ "WHERE CODE_01 = ? "
			+ "AND CODE_02 = ? "
			+ "ORDER BY CODE_01, "
			+ "         CODE_02";

	/** コード情報分類取得 */
	private static final String SQL_SELECT_CODE_01 =
			  "SELECT * "
			+ "FROM CODE "
			+ "WHERE CODE_01 = ? "
			+ "ORDER BY CODE_01, "
			+ "         CODE_02";

	/** コード情報分類 範囲取得 */
	private static final String SQL_BETWEEN_CODE_01 =
			  "SELECT * "
			+ "FROM CODE "
			+ "WHERE CODE_01 BETWEEN ? AND ? "
			+ "ORDER BY CODE_01, "
			+ "         CODE_02";

	/** コード情報件数取得 */
	private static final String SQL_COUNT =
			"SELECT COUNT(CODE_01)"
			+ " FROM CODE"
			+ " WHERE NAME_02 = ?";

	/** コード情報取得 */
	private static final String SQL_SELECT_NAME02 =
			  "SELECT * "
			+ "FROM CODE "
			+ "WHERE NAME_02 = ? ";

	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * コード情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class CodeRowMapper implements RowMapper<CodeModel> {

		/**
		 * コード情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public CodeModel mapRow(ResultSet rs, int rowNum) throws SQLException {
			// コードクラス生成
			CodeModel codeModel = new CodeModel();

			// 分類コード
			codeModel.setCode_01(rs.getString(COLUMN_CODE_01));
			// コード
			codeModel.setCode_02(rs.getString(COLUMN_CODE_02));
			// 一括コード
			codeModel.setCode(rs.getString(COLUMN_CODE_01) + rs.getString(COLUMN_CODE_02));
			// 名称1
			codeModel.setName_01(rs.getString(COLUMN_NAME_01));
			// 名称2
			codeModel.setName_02(rs.getString(COLUMN_NAME_02));
			// 名称3
			codeModel.setName_03(rs.getString(COLUMN_NAME_03));
			// 備考
			codeModel.setRemarks(rs.getString(COLUMN_REMARKS));
			// 更新ユーザーID
			codeModel.setUpdate_user_id(rs.getString(COLUMN_UPDATE_USER_ID));
			// 更新日
			codeModel.setUpdate_date(rs.getTimestamp(COLUMN_UPDATE_DATE));

			return codeModel;
		}
	}

	/**
	 * コード情報を返却します。<br>
	 * @param code_01 分類コード
	 * @param code_02 コード
	 * @return コード情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public CodeModel getCodeData(String code_01, String code_02) throws DataAccessException {
		RowMapper<CodeModel> rowMapper = new CodeRowMapper();
		return (CodeModel) jdbcTemplate.queryForObject(SQL_SELECT_CODE, rowMapper, code_01, code_02);
	}

	/**
	 * コード情報分類取得を返却します。<br>
	 * @param code_01 分類コード
	 * @return コード情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public List<CodeModel> getCode01Data(String code_01) throws DataAccessException {
		RowMapper<CodeModel> rowMapper = new CodeRowMapper();
		return jdbcTemplate.query(SQL_SELECT_CODE_01, rowMapper, code_01);
	}

	/**
	 * コード情報を分類で範囲取得します。<br>
	 * @param code_01min 分類コード最小範囲
	 * @param code_01max 分類コード最大範囲
	 * @return コード情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public  List<CodeModel> getCode01RangeData(String code_01min, String code_01max) throws DataAccessException {
		RowMapper<CodeModel> rowMapper = new CodeRowMapper();
		return jdbcTemplate.query(SQL_BETWEEN_CODE_01, rowMapper, code_01min, code_01max);
	}

	/**
	 * コード情報の存在確認をします。<br>
	 * @param name_02 名称2
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int count(String name_02) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_COUNT, Integer.class, name_02);
	}

	/**
	 * コード情報を返却します。<br>
	 * @param name_02 名称2
	 * @return コード情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public CodeModel getName02Data(String name_02) throws DataAccessException {
		RowMapper<CodeModel> rowMapper = new CodeRowMapper();
		return (CodeModel) jdbcTemplate.queryForObject(SQL_SELECT_NAME02, rowMapper, name_02);
	}

}
