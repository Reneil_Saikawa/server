/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.model.ReportAuthModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 帳票権限_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.0
 */
public class ReportAuthDaoImpl extends JdbcDaoSupport implements ReportAuthDao {

	/** カラム名：テナントID */
	private static final String COLUMN_TENANT_ID = "TENANT_ID";

	/** カラム名：ユーザーID */
	private static final String COLUMN_USER_ID = "USER_ID";

	/** カラム名：帳票ID */
	private static final String COLUMN_REPORT_ID = "REPORT_ID";

	/** カラム名：更新ユーザーID */
	private static final String COLUMN_UPDATE_USER_ID = "UPDATE_USER_ID";

	/** 帳票権限情報取得（ユーザー） */
	private static final String SQL_FINDUSER =
			  "SELECT"
			+ " TENANT_ID,"
			+ " USER_ID,"
			+ " REPORT_ID,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM REPORTAUTH"
			+ " WHERE USER_ID = ?";

	/** 帳票権限件数取得 */
	private static final String SQL_COUNT =
			  "SELECT COUNT(USER_ID)"
			+ " FROM REPORTAUTH"
			+ " WHERE TENANT_ID = ?"
			+ " AND USER_ID = ?"
			+ " AND REPORT_ID = ?";

	/** 帳票権限追加 */
	private static final String SQL_INSERT =
			"INSERT INTO REPORTAUTH("
			+ " TENANT_ID,"
			+ " USER_ID,"
			+ " REPORT_ID,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE)"
			+ " VALUES("
			+ " :TENANT_ID,"
			+ " :USER_ID,"
			+ " :REPORT_ID,"
			+ " :UPDATE_USER_ID,"
			+ " GETDATE())";

	/** 帳票権限削除 */
	private static final String SQL_DELETE =
			  "DELETE FROM REPORTAUTH "
			+ "WHERE USER_ID = ?";

	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** JDBCテンプレートクラス（リプレースあり） */
	@Autowired
	private NamedParameterJdbcTemplate npJdbcTemplate;

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * 帳票権限情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class ReportAuthRowMapper implements RowMapper<ReportAuthModel> {
		/**
		 * 帳票パラメータ情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public ReportAuthModel mapRow(ResultSet rs, int rowNum) throws SQLException {

			ReportAuthModel model = new ReportAuthModel();

			// テナントID
			model.setTenant_id(rs.getString(COLUMN_TENANT_ID));
			// ユーザーID
			model.setUser_id(rs.getString(COLUMN_USER_ID));
			// 帳票ID
			model.setReport_id(rs.getString(COLUMN_REPORT_ID));
			// 更新ユーザーID
			model.setUpdate_user_id(rs.getString(COLUMN_UPDATE_USER_ID));

			return model;
		}
	}

	/**
	 * ユーザの帳票権限情報を返却します。<br>
	 * @param user_id ユーザーID
	 * @return ユーザ帳票権限情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<ReportAuthModel> findUser(String user_id) throws DataAccessException{
		RowMapper<ReportAuthModel> rowMapper = new ReportAuthRowMapper();
		return jdbcTemplate.query(SQL_FINDUSER, rowMapper, user_id);
	}

	/**
	 * 帳票権限の存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @param user_id ユーザID
	 * @param report_id 帳票ID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int count(String tenant_id, String user_id, String report_id) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_COUNT, Integer.class, tenant_id, user_id, report_id);
	}

	/**
	 * 帳票情報を追加します。<br>
	 * @param reportAuthModel 帳票権限モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int insert(ReportAuthModel reportAuthModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
				.addValue(COLUMN_TENANT_ID, reportAuthModel.getTenant_id())
				.addValue(COLUMN_USER_ID, reportAuthModel.getUser_id())
				.addValue(COLUMN_REPORT_ID, reportAuthModel.getReport_id())
				.addValue(COLUMN_UPDATE_USER_ID, reportAuthModel.getUpdate_user_id()));
	}

	/**
	 * 帳票権限を削除します。<br>
	 * @param user_id ユーザID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public void delete(String user_id) throws DataAccessException {
		jdbcTemplate.update(SQL_DELETE, user_id);
	}
}
