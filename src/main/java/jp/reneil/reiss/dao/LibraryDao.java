/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.LibraryModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ライブラリ情報_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface LibraryDao {

	/**
	 * ライブラリ情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @param lib_id ライブラリID
	 * @return ライブラリ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	LibraryModel findLib(String tenant_id, String lib_id)throws DataAccessException;

	/**
	 * ライブラリ情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @param lib_path ライブラリパス
	 * @return ライブラリ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	LibraryModel findPath(String tenant_id, String lib_path) throws DataAccessException;

	/**
	 * ライブラリ情報と子ライブラリ情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @param lib_path ライブラリパス
	 * @return ライブラリ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<LibraryModel> likePath(String tenant_id, String lib_path) throws DataAccessException;

	/**
	 * テナント所属ライブラリ情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @return ライブラリ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<LibraryModel> tenantLib(String tenant_id) throws DataAccessException;

	/**
	 * ユーザ閲覧ライブラリ情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @param user_id ユーザーID
	 * @return ライブラリ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<LibraryModel> findUser(String tenant_id, String user_id) throws DataAccessException;

	/**
	 * ライブラリを含むライブラリ情報を返却します
	 * @param tenant_id テナントID
	 * @param lib_id ライブラリID
	 * @return ライブラリ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<LibraryModel> findPKLib(String tenant_id, String lib_id) throws DataAccessException;

	/**
	 * テナント所属ライブラリパス表示順を返却します。<br>
	 * @param tenant_id テナントID
	 * @return ライブラリパス,表示順
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<LibraryModel> libDisplayNo(String tenant_id) throws DataAccessException;

	/**
	 * ライブラリの存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @param lib_id ライブラリID
	 * @param chk 実行チェック
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int count(String tenant_id, String lib_id, String chk) throws DataAccessException;

	/**
	 * パスからライブラリの存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @param lib_path ライブラリパス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int libCount(String tenant_id, String lib_path) throws DataAccessException;

	/**
	 * ライブラリ情報を追加します。<br>
	 * @param LibraryModel ライブラリ情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int insert(LibraryModel libraryModel) throws DataAccessException;

	/**
	 * ライブラリ情報を更新します。<br>
	 * @param LibraryModel ライブラリ情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int update(LibraryModel libraryModel) throws DataAccessException;

	/**
	 * 子ライブラリパスを更新します。<br>
	 * @param old_lib_path 旧ライブラリパス
	 * @param new_lib_path 新ライブラリパス
	 * @param tenant_id テナントID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void update_path(String old_lib_path, String new_lib_path, String tenant_id) throws DataAccessException;

	/**
	 * ライブラリ情報を削除します。<br>
	 * @param tenant_id テナントID
	 * @param lib_path ライブラリパス
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void delete(String tenant_id, String lib_path) throws DataAccessException;

}
