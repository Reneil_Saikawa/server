/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.model.AdditionalItemModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 付帯情報項目名_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.0
 */
public class AdditionalItemDaoImpl extends JdbcDaoSupport implements AdditionalItemDao {

	/** カラム名：テナントID */
	private static final String COLUMN_TENANT_ID = "TENANT_ID";

	/** カラム名：テナント名 */
	private static final String COLUMN_TENANT_NAME = "TENANT_NAME";

	/** カラム名：項目ID */
	private static final String COLUMN_ITEM_ID = "ITEM_ID";

	/** カラム名：項目名 */
	private static final String COLUMN_ITEM_NAME = "ITEM_NAME";

	/** カラム名：表示順 */
	private static final String COULUMN_DISPLAY_NO = "DISPLAY_NO";

	/** カラム名：有効開始日 */
	private static final String COLUMN_START_DATE = "START_DATE";

	/** カラム名：有効終了日 */
	private static final String COLUMN_END_DATE = "END_DATE";

	/** カラム名：削除フラグ */
	private static final String COLUMN_DEL_FLG = "DEL_FLG";

	/** カラム名：更新ユーザーID */
	private static final String COLUMN_UPDATE_USER_ID = "UPDATE_USER_ID";

	/** カラム名：更新日*/
	private static final String COLUMN_UPDATE_DATE = "UPDATE_DATE";

	/** 付帯情報項目名取得 */
	private static final String SQL_FINDALL =
			  "SELECT"
			+ " AD.[TENANT_ID],"
			+ " TE.[TENANT_NAME],"
			+ " AD.[ITEM_ID],"
			+ " AD.[ITEM_NAME],"
			+ " AD.[DISPLAY_NO],"
			+ " AD.[START_DATE],"
			+ " AD.[END_DATE],"
			+ " AD.[DEL_FLG],"
			+ " AD.[UPDATE_USER_ID],"
			+ " AD.[UPDATE_DATE]"
			+ " FROM [ADDITIONALITEM] AD,"
			+ "      [TENANT]         TE"
			+ " WHERE  AD.[TENANT_ID] = TE.[TENANT_ID]"
			+ " ORDER BY AD.[TENANT_ID],"
			+ "          AD.[ITEM_ID]";

	/** 付帯情報項目名取得 */
	private static final String SQL_FINDITEM =
			  "SELECT"
			+ " TENANT_ID,"
			+ " ITEM_ID,"
			+ " ITEM_NAME,"
			+ " DISPLAY_NO,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM ADDITIONALITEM"
			+ " WHERE TENANT_ID = ?"
			+ " ORDER BY DISPLAY_NO,"
			+ "          ITEM_ID";

	/** 使用可能付帯情報項目名取得 */
	private static final String SQL_USEITEM =
			  "SELECT"
			+ " TENANT_ID,"
			+ " ITEM_ID,"
			+ " ITEM_NAME,"
			+ " DISPLAY_NO,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM ADDITIONALITEM"
			+ " WHERE TENANT_ID = ?"
			+ " AND START_DATE <= GETDATE() AND END_DATE >= GETDATE() AND DEL_FLG = 0"
			+ " ORDER BY DISPLAY_NO,"
			+ "          ITEM_ID";

	/** 付帯情報項目名件数取得 */
	private static final String SQL_COUNT =
			"SELECT COUNT(ITEM_ID)"
			+ " FROM ADDITIONALITEM"
			+ " WHERE TENANT_ID = ?"
			+ " AND ITEM_ID = ?";

	/** 付帯情報項目名検索表示用件数取得 */
	private static final String SQL_COUNT_SEARCH =
			"SELECT COUNT(ITEM_ID)"
			+ " FROM ADDITIONALITEM"
			+ " WHERE TENANT_ID = ?"
			+ " AND START_DATE <= GETDATE() AND END_DATE >= GETDATE() AND DEL_FLG = 0";

	/** 付帯情報項目名帳票出力用件数取得 */
	private static final String SQL_COUNT_REPORT =
			"SELECT COUNT(ITEM_ID)"
			+ " FROM ADDITIONALITEM"
			+ " WHERE TENANT_ID = ?"
			+ " AND ITEM_ID = ?"
			+ " AND START_DATE <= GETDATE() AND END_DATE >= GETDATE() AND DEL_FLG = 0";

	/** 付帯情報項目名追加 */
	private static final String SQL_INSERT =
			"INSERT INTO ADDITIONALITEM("
			+ " TENANT_ID,"
			+ " ITEM_ID,"
			+ " ITEM_NAME,"
			+ " DISPLAY_NO,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " DEL_FLG,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE)"
			+ " VALUES("
			+ " :TENANT_ID,"
			+ " :ITEM_ID,"
			+ " :ITEM_NAME,"
			+ " :DISPLAY_NO,"
			+ " :START_DATE,"
			+ " DATEADD(HOUR, 23, DATEADD(MINUTE, 59, DATEADD(SECOND, 59, :END_DATE))),"
			+ " :DEL_FLG,"
			+ " :UPDATE_USER_ID,"
			+ " GETDATE())";

	/** 付帯情報項目名更新 */
	private static final String SQL_UPDATE =
			  "UPDATE ADDITIONALITEM"
			+ " SET ITEM_NAME = :ITEM_NAME,"
			+ " DISPLAY_NO = :DISPLAY_NO,"
			+ " START_DATE = :START_DATE,"
			+ " END_DATE = DATEADD(HOUR, 23, DATEADD(MINUTE, 59, DATEADD(SECOND, 59, :END_DATE))),"
			+ " DEL_FLG = :DEL_FLG,"
			+ " UPDATE_USER_ID = :UPDATE_USER_ID,"
			+ " UPDATE_DATE = GETDATE()"
			+ " WHERE TENANT_ID = :TENANT_ID"
			+ " AND ITEM_ID = :ITEM_ID";


	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** JDBCテンプレートクラス（リプレースあり） */
	@Autowired
	private NamedParameterJdbcTemplate npJdbcTemplate;

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * システム管理付帯情報項目名マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class AdditionalItemFindAllMapper implements RowMapper<AdditionalItemModel> {

		/**
		 * 付帯情報項目名をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public AdditionalItemModel mapRow(ResultSet rs, int rowNum) throws SQLException {
			// ユーザークラス生成
			AdditionalItemModel model = new AdditionalItemModel();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");

			// テナントID
			model.setTenant_id(rs.getString(COLUMN_TENANT_ID));
			// テナント名
			model.setTenant_name(rs.getString(COLUMN_TENANT_NAME));
			// 項目ID
			model.setItem_id(rs.getString(COLUMN_ITEM_ID));
			// 項目名
			model.setItem_name(rs.getString(COLUMN_ITEM_NAME));
			// 表示順
			model.setDisplay_no(rs.getInt(COULUMN_DISPLAY_NO));
			// 有効開始日
			model.setStart_date(rs.getDate(COLUMN_START_DATE));
			// 有効終了日
			model.setEnd_date(rs.getDate(COLUMN_END_DATE));
			// String 有効開始日
			model.setStr_start_date(fmt.format(rs.getDate(COLUMN_START_DATE)));
			// String 有効終了日
			model.setStr_end_date(fmt.format(rs.getDate(COLUMN_END_DATE)));
			// 削除フラグ
			model.setDel_flg(rs.getByte(COLUMN_DEL_FLG));

			if (rs.getByte(COLUMN_DEL_FLG) == (byte) 1) {
				// String 削除フラグ
				model.setStr_del_flg("✔");
			}

			// 更新ユーザーID
			model.setUpdate_user_id(rs.getString(COLUMN_UPDATE_USER_ID));
			// 更新日
			model.setUpdate_date(rs.getTimestamp(COLUMN_UPDATE_DATE));

			return model;
		}
	}

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * システム管理付帯情報項目名マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class AdditionalItemFindItemMapper implements RowMapper<AdditionalItemModel> {

		/**
		 * 付帯情報項目名をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public AdditionalItemModel mapRow(ResultSet rs, int rowNum) throws SQLException {
			// ユーザークラス生成
			AdditionalItemModel model = new AdditionalItemModel();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");

			// テナントID
			model.setTenant_id(rs.getString(COLUMN_TENANT_ID));
			// 項目ID
			model.setItem_id(rs.getString(COLUMN_ITEM_ID));
			// 項目名
			model.setItem_name(rs.getString(COLUMN_ITEM_NAME));
			// 表示順
			model.setDisplay_no(rs.getInt(COULUMN_DISPLAY_NO));
			// 有効開始日
			model.setStart_date(rs.getDate(COLUMN_START_DATE));
			// 有効終了日
			model.setEnd_date(rs.getDate(COLUMN_END_DATE));
			// String 有効開始日
			model.setStr_start_date(fmt.format(rs.getDate(COLUMN_START_DATE)));
			// String 有効終了日
			model.setStr_end_date(fmt.format(rs.getDate(COLUMN_END_DATE)));
			// 削除フラグ
			model.setDel_flg(rs.getByte(COLUMN_DEL_FLG));

			if (rs.getByte(COLUMN_DEL_FLG) == (byte) 1) {
				// String 削除フラグ
				model.setStr_del_flg("✔");
			}

			// 更新ユーザーID
			model.setUpdate_user_id(rs.getString(COLUMN_UPDATE_USER_ID));
			// 更新日
			model.setUpdate_date(rs.getTimestamp(COLUMN_UPDATE_DATE));

			return model;
		}
	}

	/**
	 * 付帯情報項目名を返却します。<br>
	 * @return 付帯情報項目名
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<AdditionalItemModel> findAll() throws DataAccessException {
		RowMapper<AdditionalItemModel> rowMapper = new AdditionalItemFindAllMapper();
		return (List<AdditionalItemModel>) jdbcTemplate.query(SQL_FINDALL, rowMapper);
	}

	/**
	 * テナントIDに沿った付帯情報項目名を返却します。<br>
	 * @return 付帯情報項目名
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<AdditionalItemModel> findItem(String tenant_id) throws DataAccessException {
		RowMapper<AdditionalItemModel> rowMapper = new AdditionalItemFindItemMapper();
		return (List<AdditionalItemModel>) jdbcTemplate.query(SQL_FINDITEM, rowMapper, tenant_id);
	}

	/**
	 * テナントIDに沿った使用可能付帯情報項目名を返却します。<br>
	 * @return 付帯情報項目名
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<AdditionalItemModel> useItem(String tenant_id) throws DataAccessException {
		RowMapper<AdditionalItemModel> rowMapper = new AdditionalItemFindItemMapper();
		return (List<AdditionalItemModel>) jdbcTemplate.query(SQL_USEITEM, rowMapper, tenant_id);
	}

	/**
	 * 付帯情報項目名の存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @param item_id 項目ID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int count(String tenant_id, String item_id) throws DataAccessException {

		return jdbcTemplate.queryForObject(SQL_COUNT, Integer.class, tenant_id, item_id);
	}

	/**
	 * 付帯情報項目名の存在確認をします。(検索表示用)<br>
	 * @param tenant_id テナントID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int searchCount(String tenant_id) throws DataAccessException {

		return jdbcTemplate.queryForObject(SQL_COUNT_SEARCH, Integer.class, tenant_id);
	}

	/**
	 * 付帯情報項目名の存在確認をします。(帳票出力用)<br>
	 * @param tenant_id テナントID
	 * @param item_id 項目ID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int reportCount(String tenant_id, String item_id) throws DataAccessException {

		return jdbcTemplate.queryForObject(SQL_COUNT_REPORT, Integer.class, tenant_id, item_id);
	}

	/**
	 * 付帯情報項目名を追加します。<br>
	 * @param additionalitemModel 付帯情報項目名モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int insert(AdditionalItemModel additionalitemModel) throws DataAccessException {
		return  npJdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
				.addValue(COLUMN_TENANT_ID, additionalitemModel.getTenant_id())
				.addValue(COLUMN_ITEM_ID, additionalitemModel.getItem_id())
				.addValue(COLUMN_ITEM_NAME, additionalitemModel.getItem_name())
				.addValue(COULUMN_DISPLAY_NO, additionalitemModel.getDisplay_no())
				.addValue(COLUMN_START_DATE, additionalitemModel.getStart_date())
				.addValue(COLUMN_END_DATE, additionalitemModel.getEnd_date())
				.addValue(COLUMN_DEL_FLG, additionalitemModel.getDel_flg())
				.addValue(COLUMN_UPDATE_USER_ID, additionalitemModel.getUpdate_user_id()));
	}

	/**
	 * 付帯情報項目名を更新します。<br>
	 * @param additionalitemModel 付帯情報項目名モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int update(AdditionalItemModel additionalitemModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_UPDATE, new MapSqlParameterSource()
				.addValue(COLUMN_TENANT_ID, additionalitemModel.getTenant_id())
				.addValue(COLUMN_ITEM_ID, additionalitemModel.getItem_id())
				.addValue(COLUMN_ITEM_NAME, additionalitemModel.getItem_name())
				.addValue(COULUMN_DISPLAY_NO, additionalitemModel.getDisplay_no())
				.addValue(COLUMN_START_DATE, additionalitemModel.getStart_date())
				.addValue(COLUMN_END_DATE, additionalitemModel.getEnd_date())
				.addValue(COLUMN_DEL_FLG, additionalitemModel.getDel_flg())
				.addValue(COLUMN_UPDATE_USER_ID, additionalitemModel.getUpdate_user_id()));
	}
}
