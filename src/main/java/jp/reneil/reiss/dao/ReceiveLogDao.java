/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.ReceiveLogCsvModel;
import jp.reneil.reiss.model.ReceiveLogModel;
import jp.reneil.reiss.model.ReceiveLogSysCsvModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 受信ログ_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface ReceiveLogDao {

	/**
	 * 受信ログを返却します。<br>
	 * @return 受信ログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<ReceiveLogModel> select() throws DataAccessException;

	/**
	 * 受信ログを検索します。<br>
	 * @param start_date 検索開始日時
	 * @param end_date 検索終了日時
	 * @param tenant_id テナントID
	 * @param term_id 端末ID
	 * @param receive_code 受信区分コード
	 * @param result_code 処理結果コード
	 * @return 受信ログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<ReceiveLogModel> find(String start_date, String end_date, String tenant_id, String term_id, String receive_code, String result_code) throws DataAccessException;

	/**
	 * 受信ログcsvを検索します。<br>
	 * @param start_date 検索開始日時
	 * @param end_date 検索終了日時
	 * @param tenant_id テナントID
	 * @param term_id 端末ID
	 * @param receive_code 受信区分コード
	 * @param result_code 処理結果コード
	 * @return 受信ログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<ReceiveLogCsvModel> findCsv(String start_date, String end_date, String tenant_id, String term_id, String receive_code, String result_code) throws DataAccessException;

	/**
	 * 受信ログsysCsvを検索します。<br>
	 * @param start_date 検索開始日時
	 * @param end_date 検索終了日時
	 * @param tenant_id テナントID
	 * @param term_id 端末ID
	 * @param receive_code 受信区分コード
	 * @param result_code 処理結果コード
	 * @return 受信ログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<ReceiveLogSysCsvModel> findSysCsv(String start_date, String end_date, String tenant_id, String term_id, String receive_code, String result_code) throws DataAccessException;

	/**
	 * 受信ログを返却します。（受信番号から検索）<br>
	 * @param recv_no 受信番号
	 * @return 受信ログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	ReceiveLogModel recvNoSelect(String recv_no) throws DataAccessException;

	/**
	 * 受信ログを追加します。<br>
	 * @param receiveLogModel 受信ログモデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int insert(ReceiveLogModel receiveLogModel) throws DataAccessException;

}
