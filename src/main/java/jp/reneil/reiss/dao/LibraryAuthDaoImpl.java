/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2020/04/07      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.model.LibraryAuthModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ライブラリ権限_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.0
 */
public class LibraryAuthDaoImpl extends JdbcDaoSupport implements LibraryAuthDao {

	/** カラム名：テナントID */
	private static final String COLUMN_TENANT_ID = "TENANT_ID";

	/** カラム名：ユーザーID */
	private static final String COLUMN_USER_ID = "USER_ID";

	/** カラム名：ライブラリID */
	private static final String COLUMN_LIB_ID = "LIB_ID";

	/** カラム名：更新ユーザーID */
	private static final String COLUMN_UPDATE_USER_ID = "UPDATE_USER_ID";

	/** ライブラリ権限情報取得（ユーザー） */
	private static final String SQL_FINDUSER =
			  "SELECT"
			+ " TENANT_ID,"
			+ " USER_ID,"
			+ " LIB_ID,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM LIBRARYAUTH"
			+ " WHERE USER_ID = ?";

	/** ライブラリ権限件数取得 */
	private static final String SQL_COUNT =
			  "SELECT COUNT(USER_ID)"
			+ " FROM LIBRARYAUTH"
			+ " WHERE TENANT_ID = ?"
			+ " AND USER_ID = ?"
			+ " AND LIB_ID = ?";

	/** ライブラリ権限追加 */
	private static final String SQL_INSERT =
			"INSERT INTO LIBRARYAUTH("
			+ " TENANT_ID,"
			+ " USER_ID,"
			+ " LIB_ID,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE)"
			+ " VALUES("
			+ " :TENANT_ID,"
			+ " :USER_ID,"
			+ " :LIB_ID,"
			+ " :UPDATE_USER_ID,"
			+ " GETDATE())";

	/** ライブラリ権限削除 */
	private static final String SQL_DELETE =
			  "DELETE FROM LIBRARYAUTH "
			+ "WHERE USER_ID = ?";

	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** JDBCテンプレートクラス（リプレースあり） */
	@Autowired
	private NamedParameterJdbcTemplate npJdbcTemplate;

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * ライブラリ権限情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class LibraryAuthRowMapper implements RowMapper<LibraryAuthModel> {
		/**
		 * ライブラリパラメータ情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public LibraryAuthModel mapRow(ResultSet rs, int rowNum) throws SQLException {

			LibraryAuthModel model = new LibraryAuthModel();

			// テナントID
			model.setTenant_id(rs.getString(COLUMN_TENANT_ID));
			// ユーザーID
			model.setUser_id(rs.getString(COLUMN_USER_ID));
			// ライブラリID
			model.setLib_id(rs.getString(COLUMN_LIB_ID));
			// 更新ユーザーID
			model.setUpdate_user_id(rs.getString(COLUMN_UPDATE_USER_ID));

			return model;
		}
	}

	/**
	 * ユーザのライブラリ権限情報を返却します。<br>
	 * @param user_id ユーザーID
	 * @return ユーザライブラリ権限情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<LibraryAuthModel> findUser(String user_id) throws DataAccessException{
		RowMapper<LibraryAuthModel> rowMapper = new LibraryAuthRowMapper();
		return jdbcTemplate.query(SQL_FINDUSER, rowMapper, user_id);
	}

	/**
	 * ライブラリ権限の存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @param user_id ユーザID
	 * @param lib_id ライブラリID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int count(String tenant_id, String user_id, String lib_id) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_COUNT, Integer.class, tenant_id, user_id, lib_id);
	}

	/**
	 * ライブラリ情報を追加します。<br>
	 * @param libraryAuthModel ライブラリ権限モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int insert(LibraryAuthModel libraryAuthModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
				.addValue(COLUMN_TENANT_ID, libraryAuthModel.getTenant_id())
				.addValue(COLUMN_USER_ID, libraryAuthModel.getUser_id())
				.addValue(COLUMN_LIB_ID, libraryAuthModel.getLib_id())
				.addValue(COLUMN_UPDATE_USER_ID, libraryAuthModel.getUpdate_user_id()));
	}

	/**
	 * ライブラリ権限を削除します。<br>
	 * @param user_id ユーザID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public void delete(String user_id) throws DataAccessException {
		jdbcTemplate.update(SQL_DELETE, user_id);
	}

}
