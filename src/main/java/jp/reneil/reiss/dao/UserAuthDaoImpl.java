/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.model.UserAuthModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザー権限_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.0
 */
public class UserAuthDaoImpl extends JdbcDaoSupport implements UserAuthDao {

	/** カラム名：ユーザーID */
	private static final String COLUMN_USER_ID = "USER_ID";

	/** カラム名：権限コード */
	private static final String COLUMN_AUTH_CD = "AUTH_CD";

	/** カラム名：更新ユーザーID */
	private static final String COLUMN_UPDATE_USER_ID = "UPDATE_USER_ID";

	/** ユーザー権限件数取得 */
	private static final String SQL_COUNT =
			  "SELECT COUNT(USER_ID)"
			+ " FROM USERAUTH"
			+ " WHERE USER_ID = ?"
			+ " AND AUTH_CD = ?";

	/** ユーザー権限追加 */
	private static final String SQL_INSERT =
			"INSERT INTO USERAUTH("
			+ " USER_ID,"
			+ " AUTH_CD,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE)"
			+ " VALUES("
			+ " :USER_ID,"
			+ " :AUTH_CD,"
			+ " :UPDATE_USER_ID,"
			+ " GETDATE())";

	/** ユーザー権限削除 */
	private static final String SQL_DELETE =
			  "DELETE FROM USERAUTH "
			+ "WHERE USER_ID = ?";

	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** JDBCテンプレートクラス（リプレースあり） */
	@Autowired
	private NamedParameterJdbcTemplate npJdbcTemplate;

	/**
	 * ユーザー権限の存在確認をします。<br>
	 * @param user_id ユーザID
	 * @param auth_cd 権限コード
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int count(String user_id,String auth_cd) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_COUNT, Integer.class, user_id, auth_cd);
	}

	/**
	 * ユーザー情報を追加します。<br>
	 * @param userAuthModel ユーザー権限モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int insert(UserAuthModel userAuthModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
				.addValue(COLUMN_USER_ID, userAuthModel.getUser_id())
				.addValue(COLUMN_AUTH_CD, userAuthModel.getAuth_cd())
				.addValue(COLUMN_UPDATE_USER_ID, userAuthModel.getUpdate_user_id()));
	}

	/**
	 * ユーザー権限を削除します。<br>
	 * @param user_id ユーザID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public void delete(String user_id) throws DataAccessException {
		jdbcTemplate.update(SQL_DELETE, user_id);
	}
}
