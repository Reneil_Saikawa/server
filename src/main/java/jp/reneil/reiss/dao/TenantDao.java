/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.TenantModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * テナント_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface TenantDao {

	/**
	 * テナントの存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int count(String tenant_id) throws DataAccessException;

	/**
	 * テナントの有効確認をします。<br>
	 * @param tenant_id テナントID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int auth(String tenant_id) throws DataAccessException;

	/**
	 * テナント情報を返却します。<br>
	 * @return テナント情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<TenantModel> findAll() throws DataAccessException;

	/**
	 * テナント情報を追加します。<br>
	 * @param tenantModel テナント情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int insert(TenantModel tenantModel) throws DataAccessException;

	/**
	 * テナント情報を更新します。<br>
	 * @param tenantModel テナント情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int update(TenantModel tenantModel) throws DataAccessException;

	/**
	 * テナント情報を返却します
	 * @param tenant_id テナントID
	 * @return 該当テナント情報
	 * @throws DataAccessException
	 */
	TenantModel findTenant(String tenant_id) throws DataAccessException;

	/**
	 * テナント情報を削除します。<br>
	 * @param tenant_id テナントID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void delete(String tenant_id) throws DataAccessException;

}
