/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.model.AccessLogCsvModel;
import jp.reneil.reiss.model.AccessLogModel;
import jp.reneil.reiss.model.AccessLogSysCsvModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * アクセスログ_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.0
 */
public class AccessLogDaoImpl extends JdbcDaoSupport implements AccessLogDao {

	/** カラム名：アクセス番号 */
	private static final String COLUMN_ACCESS_NO = "ACCESS_NO";

	/** カラム名：アクセス日時 */
	private static final String COLUMN_ACCESS_DATE = "ACCESS_DATE";

	/** カラム名：アクセスIP */
	private static final String COLUMN_ACCESS_IP = "ACCESS_IP";

	/** カラム名：アクセステナントID */
	private static final String COLUMN_ACCESS_TENANT_ID = "ACCESS_TENANT_ID";

	/** カラム名：アクセステナント名 */
	private static final String COLUMN_ACCESS_TENANT_NAME = "ACCESS_TENANT_NAME";

	/** カラム名：アクセスユーザーID */
	private static final String COLUMN_ACCESS_USER_ID = "ACCESS_USER_ID";

	/** カラム名：アクセスユーザー名 */
	private static final String COLUMN_ACCESS_USER_NAME = "ACCESS_USER_NAME";

	/** カラム名：更新実行プログラムID */
	private static final String COLUMN_PRG_ID = "PRG_ID";

	/** カラム名：更新実行プログラム */
	private static final String COLUMN_PRG_NAME = "PRG_NAME";

	/** カラム名：処理結果ID */
	private static final String COLUMN_RESULT_ID = "RESULT_ID";

	/** カラム名：処理結果 */
	private static final String COLUMN_RESULT_NAME = "RESULT_NAME";

	/** カラム名：処理内容 */
	private static final String COLUMN_CONTENTS = "CONTENTS";

	/** カラム名：エラーメッセージ */
	private static final String COLUMN_ERR_MSG = "ERR_MSG";

	/** アクセスログ取得 */
	private static final String SQL_SELECT_LOG =
			  "SELECT TOP 10000"
			+ " AL.[ACCESS_NO],"
			+ " AL.[ACCESS_DATE],"
			+ " AL.[ACCESS_IP],"
			+ " AL.[ACCESS_TENANT_ID],"
			+ " TE.[TENANT_NAME] AS ACCESS_TENANT_NAME,"
			+ " AL.[ACCESS_USER_ID],"
			+ " US.[USER_NAME] AS ACCESS_USER_NAME,"
			+ " AL.[PRG_ID],"
			+ " CD1.[NAME_01] AS PRG_NAME,"
			+ " AL.[RESULT_ID],"
			+ " CD2.[NAME_01] AS RESULT_NAME,"
			+ " AL.[CONTENTS],"
			+ " AL.[ERR_MSG]"
			+ " FROM [ACCESSLOG] AL"
			+ "      LEFT JOIN [TENANT] TE ON AL.[ACCESS_TENANT_ID] = TE.[TENANT_ID]"
			+ "      LEFT JOIN [USERINFO] US ON AL.[ACCESS_USER_ID] = US.[USER_ID],"
			+ "      [CODE] CD1,"
			+ "      [CODE] CD2"
			+ " WHERE AL.[PRG_ID] = CD1.[CODE_01] + CD1.[CODE_02]"
			+ " AND AL.[RESULT_ID] = CD2.[CODE_01] + CD2.[CODE_02]"
			+ " ORDER BY AL.[ACCESS_DATE] DESC";

	/** アクセスログ検索 */
	private static final String SQL_FIND_LOG =
			  "SELECT TOP 10000"
			+ " AL.[ACCESS_NO],"
			+ " AL.[ACCESS_DATE],"
			+ " AL.[ACCESS_IP],"
			+ " AL.[ACCESS_TENANT_ID],"
			+ " TE.[TENANT_NAME] AS ACCESS_TENANT_NAME,"
			+ " AL.[ACCESS_USER_ID],"
			+ " US.[USER_NAME] AS ACCESS_USER_NAME,"
			+ " AL.[PRG_ID],"
			+ " CD1.[NAME_01] AS PRG_NAME,"
			+ " AL.[RESULT_ID],"
			+ " CD2.[NAME_01] AS RESULT_NAME,"
			+ " AL.[CONTENTS],"
			+ " AL.[ERR_MSG]"
			+ " FROM [ACCESSLOG] AL"
			+ "      LEFT JOIN [TENANT] TE ON AL.[ACCESS_TENANT_ID] = TE.[TENANT_ID]"
			+ "      LEFT JOIN [USERINFO] US ON AL.[ACCESS_USER_ID] = US.[USER_ID],"
			+ "      [CODE] CD1,"
			+ "      [CODE] CD2"
			+ " WHERE AL.[PRG_ID] = CD1.[CODE_01] + CD1.[CODE_02]"
			+ " AND AL.[RESULT_ID] = CD2.[CODE_01] + CD2.[CODE_02]"
			+ " AND AL.[ACCESS_DATE] >= ?"
			+ " AND AL.[ACCESS_DATE] <= ?";

	/** アクセスログ検索(csv) */
	private static final String SQL_FIND_LOG_CSV =
			  "SELECT"
			+ " AL.[ACCESS_NO],"
			+ " AL.[ACCESS_DATE],"
			+ " AL.[ACCESS_IP],"
			+ " AL.[ACCESS_TENANT_ID],"
			+ " TE.[TENANT_NAME] AS ACCESS_TENANT_NAME,"
			+ " AL.[ACCESS_USER_ID],"
			+ " US.[USER_NAME] AS ACCESS_USER_NAME,"
			+ " AL.[PRG_ID],"
			+ " CD1.[NAME_01] AS PRG_NAME,"
			+ " AL.[RESULT_ID],"
			+ " CD2.[NAME_01] AS RESULT_NAME,"
			+ " AL.[CONTENTS],"
			+ " AL.[ERR_MSG]"
			+ " FROM [ACCESSLOG] AL"
			+ "      LEFT JOIN [TENANT] TE ON AL.[ACCESS_TENANT_ID] = TE.[TENANT_ID]"
			+ "      LEFT JOIN [USERINFO] US ON AL.[ACCESS_USER_ID] = US.[USER_ID],"
			+ "      [CODE] CD1,"
			+ "      [CODE] CD2"
			+ " WHERE AL.[PRG_ID] = CD1.[CODE_01] + CD1.[CODE_02]"
			+ " AND AL.[RESULT_ID] = CD2.[CODE_01] + CD2.[CODE_02]"
			+ " AND AL.[ACCESS_DATE] >= ?"
			+ " AND AL.[ACCESS_DATE] <= ?";

	/** アクセスログ検索（アクセス番号から検索） */
	private static final String SQL_ACCESS_NO_SELECT_LOG =
			  "SELECT"
			+ " AL.[ACCESS_NO],"
			+ " AL.[ACCESS_DATE],"
			+ " AL.[ACCESS_IP],"
			+ " AL.[ACCESS_TENANT_ID],"
			+ " TE.[TENANT_NAME] AS ACCESS_TENANT_NAME,"
			+ " AL.[ACCESS_USER_ID],"
			+ " US.[USER_NAME] AS ACCESS_USER_NAME,"
			+ " AL.[PRG_ID],"
			+ " CD1.[NAME_01] AS PRG_NAME,"
			+ " AL.[RESULT_ID],"
			+ " CD2.[NAME_01] AS RESULT_NAME,"
			+ " AL.[CONTENTS],"
			+ " AL.[ERR_MSG]"
			+ " FROM [ACCESSLOG] AL"
			+ "      LEFT JOIN [TENANT] TE ON AL.[ACCESS_TENANT_ID] = TE.[TENANT_ID]"
			+ "      LEFT JOIN [USERINFO] US ON AL.[ACCESS_USER_ID] = US.[USER_ID],"
			+ "      [CODE] CD1,"
			+ "      [CODE] CD2"
			+ " WHERE AL.[PRG_ID] = CD1.[CODE_01] + CD1.[CODE_02]"
			+ " AND AL.[RESULT_ID] = CD2.[CODE_01] + CD2.[CODE_02]"
			+ " AND AL.[ACCESS_NO] = ?";

	/** アクセスログ追加 */
	private static final String SQL_INSERT_LOG =
			"INSERT INTO ACCESSLOG("
			+ " ACCESS_DATE,"
			+ " ACCESS_IP,"
			+ " ACCESS_TENANT_ID,"
			+ " ACCESS_USER_ID,"
			+ " PRG_ID,"
			+ " RESULT_ID,"
			+ " CONTENTS,"
			+ " ERR_MSG)"
			+ " VALUES("
			+ " :ACCESS_DATE,"
			+ " :ACCESS_IP,"
			+ " :ACCESS_TENANT_ID,"
			+ " :ACCESS_USER_ID,"
			+ " :PRG_ID,"
			+ " :RESULT_ID,"
			+ " :CONTENTS,"
			+ " :ERR_MSG)";

	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** JDBCテンプレートクラス（リプレースあり） */
	@Autowired
	private NamedParameterJdbcTemplate npJdbcTemplate;

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * アクセスログマッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class AccessLogMapper implements RowMapper<AccessLogModel> {

		/**
		 * アクセスログをマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public AccessLogModel mapRow(ResultSet rs, int rowNum) throws SQLException {

			// アクセスログクラス生成
			AccessLogModel model = new AccessLogModel();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			//アクセス番号
			model.setAccess_no(rs.getLong(COLUMN_ACCESS_NO));
			//アクセス日時
			model.setAccess_date(rs.getTimestamp(COLUMN_ACCESS_DATE));
			//String アクセス日時
			model.setStr_access_date(fmt.format(rs.getTimestamp(COLUMN_ACCESS_DATE)));
			//アクセスIP
			model.setAccess_ip(rs.getString(COLUMN_ACCESS_IP));
			//アクセステナントID
			model.setAccess_tenant_id(rs.getString(COLUMN_ACCESS_TENANT_ID));
			//アクセステナント名
			model.setAccess_tenant_name(rs.getString(COLUMN_ACCESS_TENANT_NAME));
			//アクセスユーザーID
			model.setAccess_user_id(rs.getString(COLUMN_ACCESS_USER_ID));
			//アクセスユーザー名
			model.setAccess_user_name(rs.getString(COLUMN_ACCESS_USER_NAME));
			//実行プログラムID
			model.setPrg_id(rs.getString(COLUMN_PRG_ID));
			//実行プログラム
			model.setPrg_name(rs.getString(COLUMN_PRG_NAME));
			//処理結果ID
			model.setResult_id(rs.getString(COLUMN_RESULT_ID));
			//処理結果
			model.setResult_name(rs.getString(COLUMN_RESULT_NAME));
			//処理内容
			model.setContents(rs.getString(COLUMN_CONTENTS));
			//エラーメッセージ
			model.setErr_msg(rs.getString(COLUMN_ERR_MSG));

			return model;
		}
	}

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * アクセスログcsvマッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class AccessLogCsvMapper implements RowMapper<AccessLogCsvModel> {

		/**
		 * アクセスログをマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public AccessLogCsvModel mapRow(ResultSet rs, int rowNum) throws SQLException {

			// アクセスログクラス生成
			AccessLogCsvModel model = new AccessLogCsvModel();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			//String アクセス日時
			model.setStr_access_date(fmt.format(rs.getTimestamp(COLUMN_ACCESS_DATE)));
			//アクセスIP
			model.setAccess_ip(rs.getString(COLUMN_ACCESS_IP));
			//アクセスユーザーID
			model.setAccess_user_id(rs.getString(COLUMN_ACCESS_USER_ID));
			//アクセスユーザー名
			model.setAccess_user_name(rs.getString(COLUMN_ACCESS_USER_NAME));
			//実行プログラム
			model.setPrg_name(rs.getString(COLUMN_PRG_NAME));
			//処理結果
			model.setResult_name(rs.getString(COLUMN_RESULT_NAME));
			//処理内容
			model.setContents(rs.getString(COLUMN_CONTENTS));
			//エラーメッセージ
			model.setErr_msg(rs.getString(COLUMN_ERR_MSG));

			return model;
		}
	}

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * アクセスログsysCsvマッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class AccessLogSysCsvMapper implements RowMapper<AccessLogSysCsvModel> {

		/**
		 * アクセスログをマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public AccessLogSysCsvModel mapRow(ResultSet rs, int rowNum) throws SQLException {

			// アクセスログクラス生成
			AccessLogSysCsvModel model = new AccessLogSysCsvModel();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			//アクセス番号
			model.setAccess_no(rs.getLong(COLUMN_ACCESS_NO));
			//String アクセス日時
			model.setStr_access_date(fmt.format(rs.getTimestamp(COLUMN_ACCESS_DATE)));
			//アクセスIP
			model.setAccess_ip(rs.getString(COLUMN_ACCESS_IP));
			//アクセステナントID
			model.setAccess_tenant_id(rs.getString(COLUMN_ACCESS_TENANT_ID));
			//アクセステナント名
			model.setAccess_tenant_name(rs.getString(COLUMN_ACCESS_TENANT_NAME));
			//アクセスユーザーID
			model.setAccess_user_id(rs.getString(COLUMN_ACCESS_USER_ID));
			//アクセスユーザー名
			model.setAccess_user_name(rs.getString(COLUMN_ACCESS_USER_NAME));
			//実行プログラムID
			model.setPrg_id(rs.getString(COLUMN_PRG_ID));
			//実行プログラム
			model.setPrg_name(rs.getString(COLUMN_PRG_NAME));
			//処理結果ID
			model.setResult_id(rs.getString(COLUMN_RESULT_ID));
			//処理結果
			model.setResult_name(rs.getString(COLUMN_RESULT_NAME));
			//処理内容
			model.setContents(rs.getString(COLUMN_CONTENTS));
			//エラーメッセージ
			model.setErr_msg(rs.getString(COLUMN_ERR_MSG));

			return model;
		}
	}

	/**
	 * アクセスログを返却します。<br>
	 * @return アクセスログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<AccessLogModel> select() throws DataAccessException {
		RowMapper<AccessLogModel> rowMapper = new AccessLogMapper();
		return (List<AccessLogModel>) jdbcTemplate.query(SQL_SELECT_LOG, rowMapper);
	}

	/**
	 * アクセスログを検索します。<br>
	 * @param start_date 検索開始日時
	 * @param end_date 検索終了日時
	 * @param tenant_id テナントID
	 * @param user_id ユーザーID
	 * @param execution_code 実行プログラムコード
	 * @param result_code 処理結果コード
	 * @return アクセスログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<AccessLogModel> find(String start_date, String end_date, String tenant_id, String user_id, String execution_code, String result_code) throws DataAccessException {
		RowMapper<AccessLogModel> rowMapper = new AccessLogMapper();
		String strSQL = SQL_FIND_LOG;
		if (tenant_id.equals("")) {
			strSQL = strSQL + " AND AL.[ACCESS_TENANT_ID] <> ?";
		}else {
			strSQL = strSQL + " AND AL.[ACCESS_TENANT_ID] = ?";
		}
		if (user_id.equals("")) {
			strSQL = strSQL + " AND AL.[ACCESS_USER_ID] <> ?";
		}else {
			strSQL = strSQL + " AND AL.[ACCESS_USER_ID] = ?";
		}
		if (execution_code.equals("")) {
			strSQL = strSQL + " AND AL.[PRG_ID] <> ?";
		}else {
			strSQL = strSQL + " AND AL.[PRG_ID] = ?";
		}
		if (result_code.equals("")) {
			strSQL = strSQL + " AND AL.[RESULT_ID] <> ?";
		}else {
			strSQL = strSQL + " AND AL.[RESULT_ID] = ?";
		}
		strSQL = strSQL + " ORDER BY AL.[ACCESS_DATE] DESC";

		return (List<AccessLogModel>) jdbcTemplate.query(strSQL, rowMapper, start_date, end_date, tenant_id, user_id, execution_code, result_code);
	}

	/**
	 * アクセスログcsvを検索します。<br>
	 * @param start_date 検索開始日時
	 * @param end_date 検索終了日時
	 * @param tenant_id テナントID
	 * @param user_id ユーザーID
	 * @param execution_code 実行プログラムコード
	 * @param result_code 処理結果コード
	 * @return アクセスログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<AccessLogCsvModel> findCsv(String start_date, String end_date, String tenant_id, String user_id, String execution_code, String result_code) throws DataAccessException {
		RowMapper<AccessLogCsvModel> rowMapper = new AccessLogCsvMapper();
		String strSQL = SQL_FIND_LOG_CSV;
		if (tenant_id.equals("")) {
			strSQL = strSQL + " AND AL.[ACCESS_TENANT_ID] <> ?";
		}else {
			strSQL = strSQL + " AND AL.[ACCESS_TENANT_ID] = ?";
		}
		if (user_id.equals("")) {
			strSQL = strSQL + " AND AL.[ACCESS_USER_ID] <> ?";
		}else {
			strSQL = strSQL + " AND AL.[ACCESS_USER_ID] = ?";
		}
		if (execution_code.equals("")) {
			strSQL = strSQL + " AND AL.[PRG_ID] <> ?";
		}else {
			strSQL = strSQL + " AND AL.[PRG_ID] = ?";
		}
		if (result_code.equals("")) {
			strSQL = strSQL + " AND AL.[RESULT_ID] <> ?";
		}else {
			strSQL = strSQL + " AND AL.[RESULT_ID] = ?";
		}
		strSQL = strSQL + " ORDER BY AL.[ACCESS_DATE] DESC";

		return (List<AccessLogCsvModel>) jdbcTemplate.query(strSQL, rowMapper, start_date, end_date, tenant_id, user_id, execution_code, result_code);
	}

	/**
	 * アクセスログsysCsvを検索します。<br>
	 * @param start_date 検索開始日時
	 * @param end_date 検索終了日時
	 * @param tenant_id テナントID
	 * @param user_id ユーザーID
	 * @param execution_code 実行プログラムコード
	 * @param result_code 処理結果コード
	 * @return アクセスログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<AccessLogSysCsvModel> findSysCsv(String start_date, String end_date, String tenant_id, String user_id, String execution_code, String result_code) throws DataAccessException {
		RowMapper<AccessLogSysCsvModel> rowMapper = new AccessLogSysCsvMapper();
		String strSQL = SQL_FIND_LOG_CSV;
		if (tenant_id.equals("")) {
			strSQL = strSQL + " AND AL.[ACCESS_TENANT_ID] <> ?";
		}else {
			strSQL = strSQL + " AND AL.[ACCESS_TENANT_ID] = ?";
		}
		if (user_id.equals("")) {
			strSQL = strSQL + " AND AL.[ACCESS_USER_ID] <> ?";
		}else {
			strSQL = strSQL + " AND AL.[ACCESS_USER_ID] = ?";
		}
		if (execution_code.equals("")) {
			strSQL = strSQL + " AND AL.[PRG_ID] <> ?";
		}else {
			strSQL = strSQL + " AND AL.[PRG_ID] = ?";
		}
		if (result_code.equals("")) {
			strSQL = strSQL + " AND AL.[RESULT_ID] <> ?";
		}else {
			strSQL = strSQL + " AND AL.[RESULT_ID] = ?";
		}
		strSQL = strSQL + " ORDER BY AL.[ACCESS_DATE] DESC";

		return (List<AccessLogSysCsvModel>) jdbcTemplate.query(strSQL, rowMapper, start_date, end_date, tenant_id, user_id, execution_code, result_code);
	}

	/**
	 * アクセスログを返却します。（アクセス番号から検索）<br>
	 * @param access_no アクセス番号
	 * @return アクセスログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public AccessLogModel accessNoSelect(String access_no) throws DataAccessException {
		RowMapper<AccessLogModel> rowMapper = new AccessLogMapper();
		return (AccessLogModel) jdbcTemplate.queryForObject(SQL_ACCESS_NO_SELECT_LOG, rowMapper, access_no);
	}

	/**
	 * アクセスログを追加します。<br>
	 * @param accessLogModel アクセスログモデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int insert(AccessLogModel accessLogModel) throws DataAccessException {
		return  npJdbcTemplate.update(SQL_INSERT_LOG, new MapSqlParameterSource()
				.addValue(COLUMN_ACCESS_DATE, accessLogModel.getAccess_date())
				.addValue(COLUMN_ACCESS_IP, accessLogModel.getAccess_ip())
				.addValue(COLUMN_ACCESS_TENANT_ID, accessLogModel.getAccess_tenant_id())
				.addValue(COLUMN_ACCESS_USER_ID, accessLogModel.getAccess_user_id())
				.addValue(COLUMN_PRG_ID, accessLogModel.getPrg_id())
				.addValue(COLUMN_RESULT_ID, accessLogModel.getResult_id())
				.addValue(COLUMN_CONTENTS, accessLogModel.getContents())
				.addValue(COLUMN_ERR_MSG, accessLogModel.getErr_msg()));
	}

}
