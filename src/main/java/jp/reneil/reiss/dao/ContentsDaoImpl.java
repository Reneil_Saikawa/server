/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2020/02/20      1.0.1           女屋            コンテンツ情報取得SQL修正
 */
package jp.reneil.reiss.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.model.ContentsModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * コンテンツ_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.1
 */
public class ContentsDaoImpl extends JdbcDaoSupport implements ContentsDao {

	/** カラム名：登録番号 */
	private static final String COLUMN_REG_NO = "REG_NO";

	/** カラム名：フォーマット区分 */
	private static final String COLUMN_FILE_FORMAT = "FILE_FORMAT";

	/** カラム名：格納パス */
	private static final String COLUMN_FILE_PATH = "FILE_PATH";

	/** カラム名：ファイル名 */
	private static final String COLUMN_FILE_NAME = "FILE_NAME";

	/** カラム名：撮影端末ID */
	private static final String COLUMN_TERM_ID = "TERM_ID";

	/** カラム名：撮影端末名 */
	private static final String COLUMN_TERM_NAME = "TERM_NAME";

	/** カラム名：撮影日 */
	private static final String COLUMN_PHOTO_DATE = "PHOTO_DATE";

	/** カラム名：撮影場所（緯度） */
	private static final String COLUMN_LOCATE_LAT = "LOCATE_LAT";

	/** カラム名：撮影場所（経度） */
	private static final String COLUMN_LOCATE_LON = "LOCATE_LON";

	/** カラム名：撮影場所 */
	private static final String COLUMN_LOCATE = "LOCATE";

	/** カラム名：更新ユーザーID */
	private static final String COLUMN_UPDATE_USER_ID = "UPDATE_USER_ID";

	/** カラム名：更新日 */
	private static final String COLUMN_UPDATE_DATE = "UPDATE_DATE";

	/** コンテンツ情報取得（登録番号） */
	private static final String SQL_SELECT_CONTENTS_REGNO =
			  "SELECT"
			+ " REG_NO,"
			+ " FILE_FORMAT,"
			+ " FILE_PATH,"
			+ " FILE_NAME,"
			+ " TERM_ID,"
			+ " PHOTO_DATE,"
			+ " LOCATE_LAT,"
			+ " LOCATE_LON,"
			+ " LOCATE,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM CONTENTS"
			+ " WHERE REG_NO = ?";

	/** コンテンツ情報取得（ファイル名） */
	private static final String SQL_SELECT_CONTENTS_FILENAME =
			  "SELECT TOP 1"
			+ " REG_NO,"
			+ " FILE_FORMAT,"
			+ " FILE_PATH,"
			+ " FILE_NAME,"
			+ " TERM_ID,"
			+ " PHOTO_DATE,"
			+ " LOCATE_LAT,"
			+ " LOCATE_LON,"
			+ " LOCATE,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM CONTENTS"
			+ " WHERE FILE_NAME = ?"
			+ " ORDER BY UPDATE_DATE DESC";

	/** コンテンツ情報取得（テナントID,ファイル名） */
	private static final String SQL_SELECT_FILENAME_TERMNAME =
			  "SELECT TOP 1"
			+ " CT.[REG_NO],"
			+ " CT.[FILE_FORMAT],"
			+ " CT.[FILE_PATH],"
			+ " CT.[FILE_NAME],"
			+ " CT.[TERM_ID],"
			+ " TM.[TERM_NAME],"
			+ " CT.[PHOTO_DATE],"
			+ " CT.[LOCATE_LAT],"
			+ " CT.[LOCATE_LON],"
			+ " CT.[LOCATE],"
			+ " CT.[UPDATE_USER_ID],"
			+ " CT.[UPDATE_DATE]"
			+ " FROM [CONTENTS] CT"
			+ "      LEFT JOIN [TERMINAL] TM ON CT.[TERM_ID] = TM.[TERM_ID] AND TM.[TENANT_ID] = ?"
			+ " WHERE CT.[FILE_NAME] = ?"
			+ " ORDER BY [UPDATE_DATE] DESC";

	/** レコード件数を取得（登録番号） */
	private static final String SQL_COUNT_CONTENTS_REGNO =
			  "SELECT COUNT(FILE_NAME)"
			+ " FROM CONTENTS"
			+ " WHERE REG_NO = ?";

	/** レコード件数を取得（ファイル名） */
	private static final String SQL_COUNT_CONTENTS_FILENAME =
			  "SELECT COUNT(FILE_NAME)"
			+ " FROM CONTENTS "
			+ " WHERE FILE_NAME = ?";

	/** コンテンツ情報追加 */
	private static final String SQL_INSERT_CONTENTS =
			  "INSERT INTO CONTENTS("
			+ " REG_NO,"
			+ " FILE_FORMAT,"
			+ " FILE_PATH,"
			+ " FILE_NAME,"
			+ " TERM_ID,"
			+ " PHOTO_DATE,"
			+ " LOCATE_LAT,"
			+ " LOCATE_LON,"
			+ " LOCATE,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE)"
			+ " VALUES("
			+ " :REG_NO,"
			+ " :FILE_FORMAT,"
			+ " :FILE_PATH,"
			+ " :FILE_NAME,"
			+ " :TERM_ID,"
			+ " :PHOTO_DATE,"
			+ " :LOCATE_LAT,"
			+ " :LOCATE_LON,"
			+ " :LOCATE,"
			+ " :UPDATE_USER_ID,"
			+ " GETDATE())";

	/** コンテンツ情報更新 */
	private static final String SQL_UPDATE_CONTENTS =
			  "UPDATE CONTENTS"
			+ " SET FILE_FORMAT = :FILE_FORMAT,"
			+ " FILE_PATH = :FILE_PATH,"
			+ " FILE_NAME = :FILE_NAME,"
			+ " TERM_ID = :TERM_ID,"
			+ " PHOTO_DATE = :PHOTO_DATE,"
			+ " LOCATE_LAT = :LOCATE_LAT,"
			+ " LOCATE_LON = :LOCATE_LON,"
			+ " LOCATE = :LOCATE,"
			+ " UPDATE_USER_ID = :UPDATE_USER_ID,"
			+ " UPDATE_DATE = GETDATE()"
			+ " WHERE REG_NO = :REG_NO";

	/** コンテンツファイルパス更新 */
	private static final String SQL_UPDATE_CONTENTS_PATH =
			  "UPDATE CONTENTS"
			+ " SET FILE_PATH = ?,"
			+ " UPDATE_DATE = GETDATE()"
			+ " WHERE REG_NO = ?";

	/** コンテンツファイルパス更新(LIKE) */
	private static final String SQL_UPDATE_CONTENTS_PATH_LIKE =
			  "UPDATE CONTENTS"
			+ " SET FILE_PATH = REPLACE(FILE_PATH, ?, ?),"
			+ " UPDATE_DATE = GETDATE()"
			+ " WHERE FILE_PATH = ?"
			+ " OR FILE_PATH LIKE ? ESCAPE '#'";

	/** コンテンツ情報削除 */
	private static final String SQL_DELETE_CONTENTS =
			  "DELETE FROM CONTENTS"
			+ " WHERE REG_NO = ?";

	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** JDBCテンプレートクラス（リプレースあり） */
	@Autowired
	private NamedParameterJdbcTemplate npJdbcTemplate;

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * コンテンツ情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class ContentsRowMapper implements RowMapper<ContentsModel> {

		/**
		 * コンテンツ情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public ContentsModel mapRow(ResultSet rs, int rowNum) throws SQLException {
			// コンテンツクラス生成
			ContentsModel contentsModel = new ContentsModel();

			// 登録番号
			contentsModel.setReg_no(rs.getString(COLUMN_REG_NO));
			// フォーマット区分
			contentsModel.setFile_format(rs.getString(COLUMN_FILE_FORMAT));
			// 格納パス
			contentsModel.setFile_path(rs.getString(COLUMN_FILE_PATH));
			// ファイル名
			contentsModel.setFile_name(rs.getString(COLUMN_FILE_NAME));
			// 撮影端末ID
			contentsModel.setTerm_id(rs.getString(COLUMN_TERM_ID));
			// 撮影日
			contentsModel.setPhoto_date(rs.getTimestamp(COLUMN_PHOTO_DATE));
			// 撮影場所（緯度）
			contentsModel.setLocate_lat(rs.getDouble(COLUMN_LOCATE_LAT));
			// 撮影場所（軽度）
			contentsModel.setLocate_lon(rs.getDouble(COLUMN_LOCATE_LON));
			// 撮影場所
			contentsModel.setLocate(rs.getString(COLUMN_LOCATE));
			// 更新ユーザ
			contentsModel.setUpdate_user_id(rs.getString(COLUMN_UPDATE_USER_ID));
			// 更新日
			contentsModel.setUpdate_date(rs.getTimestamp(COLUMN_UPDATE_DATE));

			return contentsModel;
		}
	}

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * 端末名付きコンテンツ情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class ContentsTermRowMapper implements RowMapper<ContentsModel> {

		/**
		 * コンテンツ情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public ContentsModel mapRow(ResultSet rs, int rowNum) throws SQLException {
			// コンテンツクラス生成
			ContentsModel contentsModel = new ContentsModel();

			// 登録番号
			contentsModel.setReg_no(rs.getString(COLUMN_REG_NO));
			// フォーマット区分
			contentsModel.setFile_format(rs.getString(COLUMN_FILE_FORMAT));
			// 格納パス
			contentsModel.setFile_path(rs.getString(COLUMN_FILE_PATH));
			// ファイル名
			contentsModel.setFile_name(rs.getString(COLUMN_FILE_NAME));
			// 撮影端末ID
			contentsModel.setTerm_id(rs.getString(COLUMN_TERM_ID));
			// 撮影端末名
			contentsModel.setTerm_name(rs.getString(COLUMN_TERM_NAME));
			// 撮影日
			contentsModel.setPhoto_date(rs.getTimestamp(COLUMN_PHOTO_DATE));
			// 撮影場所（緯度）
			contentsModel.setLocate_lat(rs.getDouble(COLUMN_LOCATE_LAT));
			// 撮影場所（軽度）
			contentsModel.setLocate_lon(rs.getDouble(COLUMN_LOCATE_LON));
			// 撮影場所
			contentsModel.setLocate(rs.getString(COLUMN_LOCATE));
			// 更新ユーザ
			contentsModel.setUpdate_user_id(rs.getString(COLUMN_UPDATE_USER_ID));
			// 更新日
			contentsModel.setUpdate_date(rs.getTimestamp(COLUMN_UPDATE_DATE));

			return contentsModel;
		}
	}

	/**
	 * コンテンツ情報を返却します。（登録番号から検索）<br>
	 * @param reg_no 登録番号
	 * @return コンテンツ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public ContentsModel getContentsData_regno(String reg_no) throws DataAccessException {
		RowMapper<ContentsModel> rowMapper = new ContentsRowMapper();
		return (ContentsModel) jdbcTemplate.queryForObject(SQL_SELECT_CONTENTS_REGNO, rowMapper, reg_no);
	}

	/**
	 * コンテンツ情報を返却します。（ファイル名から検索）<br>
	 * @param file_name ファイル名
	 * @return コンテンツ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public ContentsModel getContentsData_filename(String file_name) throws DataAccessException {
		RowMapper<ContentsModel> rowMapper = new ContentsRowMapper();
		return (ContentsModel) jdbcTemplate.queryForObject(SQL_SELECT_CONTENTS_FILENAME, rowMapper, file_name);
	}

	/**
	 * コンテンツ情報を返却します。（テナントID,ファイル名から検索 = 端末名取得）<br>
	 * @param tenant_id テナントID
	 * @param file_name ファイル名
	 * @return コンテンツ情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public ContentsModel getContentsData_tenantid_filename(String tenant_id, String file_name) throws DataAccessException {
		RowMapper<ContentsModel> rowMapper = new ContentsTermRowMapper();
		return (ContentsModel) jdbcTemplate.queryForObject(SQL_SELECT_FILENAME_TERMNAME, rowMapper, tenant_id, file_name);
	}

	/**
	 * コンテンツの存在確認をします。（登録番号から検索）<br>
	 * @param reg_no 登録番号
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int findContents_regno(String reg_no) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_COUNT_CONTENTS_REGNO, Integer.class, reg_no);
	}

	/**
	 * コンテンツの存在確認をします。（ファイル名から検索）<br>
	 * @param file_name ファイル名
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int findContents_filename(String file_name) throws DataAccessException {
		return jdbcTemplate.queryForObject(SQL_COUNT_CONTENTS_FILENAME, Integer.class, file_name);
	}

	/**
	 * コンテンツ情報を追加します。<br>
	 * @param contentsModel コンテンツ情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int insert(ContentsModel contentsModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_INSERT_CONTENTS, new MapSqlParameterSource()
				.addValue(COLUMN_REG_NO, contentsModel.getReg_no())
				.addValue(COLUMN_FILE_FORMAT, contentsModel.getFile_format())
				.addValue(COLUMN_FILE_PATH, contentsModel.getFile_path())
				.addValue(COLUMN_FILE_NAME, contentsModel.getFile_name())
				.addValue(COLUMN_TERM_ID, contentsModel.getTerm_id())
				.addValue(COLUMN_PHOTO_DATE, contentsModel.getPhoto_date())
				.addValue(COLUMN_LOCATE_LAT, contentsModel.getLocate_lat())
				.addValue(COLUMN_LOCATE_LON, contentsModel.getLocate_lon())
				.addValue(COLUMN_LOCATE, contentsModel.getLocate())
				.addValue(COLUMN_UPDATE_USER_ID, contentsModel.getUpdate_user_id()));
	}

	/**
	 * コンテンツ情報を更新します。<br>
	 * @param contentsModel コンテンツ情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int update(ContentsModel contentsModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_UPDATE_CONTENTS, new MapSqlParameterSource()
				.addValue(COLUMN_REG_NO, contentsModel.getReg_no())
				.addValue(COLUMN_FILE_FORMAT, contentsModel.getFile_format())
				.addValue(COLUMN_FILE_PATH, contentsModel.getFile_path())
				.addValue(COLUMN_FILE_NAME, contentsModel.getFile_name())
				.addValue(COLUMN_TERM_ID, contentsModel.getTerm_id())
				.addValue(COLUMN_PHOTO_DATE, contentsModel.getPhoto_date())
				.addValue(COLUMN_LOCATE_LAT, contentsModel.getLocate_lat())
				.addValue(COLUMN_LOCATE_LON, contentsModel.getLocate_lon())
				.addValue(COLUMN_LOCATE, contentsModel.getLocate())
				.addValue(COLUMN_UPDATE_USER_ID, contentsModel.getUpdate_user_id()));
	}

	/**
	 * コンテンツファイルパスを更新します。<br>
	 * @param reg_no 登録番号
	 * @param file_path ファイルパス
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public void update_path(String reg_no, String file_path) throws DataAccessException {
		jdbcTemplate.update(SQL_UPDATE_CONTENTS_PATH, file_path, reg_no);
	}

	/**
	 * コンテンツファイルパスを更新します。(LIKE)<br>
	 * @param old_file_path 旧ファイルパス
	 * @param new_file_path 新ファイルパス
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public void update_path_like(String old_file_path, String new_file_path) throws DataAccessException {
		jdbcTemplate.update(SQL_UPDATE_CONTENTS_PATH_LIKE, old_file_path, new_file_path, old_file_path, old_file_path.replace(CommonConst.YEN_MRK, "#" + CommonConst.YEN_MRK) + "#" + CommonConst.YEN_MRK + "%");
	}

	/**
	 * コンテンツ情報を削除します。<br>
	 * @param reg_no 登録番号
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public void deleteContents_regno(String reg_no) throws DataAccessException {
		jdbcTemplate.update(SQL_DELETE_CONTENTS, reg_no);
	}

}
