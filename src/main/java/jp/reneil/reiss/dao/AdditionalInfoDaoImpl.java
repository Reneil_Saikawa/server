/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.model.AdditionalInfoModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 付帯情報_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.0
 */
public class AdditionalInfoDaoImpl extends JdbcDaoSupport implements AdditionalInfoDao {

	/** カラム名：登録番号 */
	private static final String COLUMN_REG_NO = "REG_NO";

	/** カラム名：項目ID */
	private static final String COLUMN_ITEM_ID = "ITEM_ID";

	/** カラム名：内容 */
	private static final String COLUMN_ITEM_VALUE = "ITEM_VALUE";

	/** カラム名：更新ユーザーID */
	private static final String COLUMN_UPDATE_USER_ID = "UPDATE_USER_ID";

	/** カラム名：更新日*/
	private static final String COLUMN_UPDATE_DATE = "UPDATE_DATE";

	/** 付帯情報取得 */
	private static final String SQL_FINDALL =
			  "SELECT"
			+ " REG_NO,"
			+ " ITEM_ID,"
			+ " ITEM_VALUE,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE"
			+ " FROM ADDITIONALINFO";

	/** 付帯情報件数取得 */
	private static final String SQL_COUNT =
			"SELECT COUNT(REG_NO)"
			+ " FROM ADDITIONALINFO"
			+ " WHERE REG_NO = ?"
			+ " AND ITEM_ID = ?";

	/** 付帯情報検索件数取得 */
	private static final String SQL_SEARCH =
			"SELECT COUNT(REG_NO)"
			+ " FROM ADDITIONALINFO"
			+ " WHERE REG_NO = ?"
			+ " AND ITEM_VALUE LIKE ?";

	/** 付帯情報追加 */
	private static final String SQL_INSERT =
			"INSERT INTO ADDITIONALINFO("
			+ " REG_NO,"
			+ " ITEM_ID,"
			+ " ITEM_VALUE,"
			+ " UPDATE_USER_ID,"
			+ " UPDATE_DATE)"
			+ " VALUES("
			+ " :REG_NO,"
			+ " :ITEM_ID,"
			+ " :ITEM_VALUE,"
			+ " :UPDATE_USER_ID,"
			+ " GETDATE())";

	/** 付帯情報更新 */
	private static final String SQL_UPDATE =
			  "UPDATE ADDITIONALINFO"
			+ " SET ITEM_VALUE = :ITEM_VALUE,"
			+ " UPDATE_USER_ID = :UPDATE_USER_ID,"
			+ " UPDATE_DATE = GETDATE()"
			+ " WHERE REG_NO = :REG_NO"
			+ " AND ITEM_ID = :ITEM_ID";

	/** 付帯情報削除 */
	private static final String SQL_ONE_DELETE =
			  "DELETE FROM ADDITIONALINFO "
			+ "WHERE REG_NO = ?"
			+ " AND ITEM_ID = ?";

	/** 付帯情報削除（全て） */
	private static final String SQL_ALL_DELETE =
			  "DELETE FROM ADDITIONALINFO "
			+ "WHERE REG_NO = ?";


	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** JDBCテンプレートクラス（リプレースあり） */
	@Autowired
	private NamedParameterJdbcTemplate npJdbcTemplate;

	/** 取得検索条件変更用 */
	private String Search = " WHERE REG_NO = ?";

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * システム管理付帯情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class AdditionalInfoMapper implements RowMapper<AdditionalInfoModel> {

		/**
		 * 付帯情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public AdditionalInfoModel mapRow(ResultSet rs, int rowNum) throws SQLException {
			// ユーザークラス生成
			AdditionalInfoModel model = new AdditionalInfoModel();

			//登録番号
			model.setReg_no(rs.getString(COLUMN_REG_NO));
			//項目ID
			model.setItem_id(rs.getString(COLUMN_ITEM_ID));
			//内容
			model.setItem_value(rs.getString(COLUMN_ITEM_VALUE));
			//更新ユーザーID
			model.setUpdate_user_id(rs.getString(COLUMN_UPDATE_USER_ID));
			//更新日
			model.setUpdate_date(rs.getTimestamp(COLUMN_UPDATE_DATE));

			return model;
		}
	}

	/**
	 * 付帯情報を返却します。<br>
	 * @return 付帯情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<AdditionalInfoModel> findAll() throws DataAccessException {
		RowMapper<AdditionalInfoModel> rowMapper = new AdditionalInfoMapper();
		return (List<AdditionalInfoModel>) jdbcTemplate.query(SQL_FINDALL, rowMapper);
	}

	/**
	 * 登録番号に沿った付帯情報を返却します。<br>
	 * @return 付帯情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<AdditionalInfoModel> findInfo(String reg_no) throws DataAccessException {
		RowMapper<AdditionalInfoModel> rowMapper = new AdditionalInfoMapper();
		StringBuilder sb = new StringBuilder(SQL_FINDALL);
		sb.append(Search);
		return (List<AdditionalInfoModel>) jdbcTemplate.query(sb.toString(), rowMapper, reg_no);
	}

	/**
	 * 付帯情報の存在確認をします。<br>
	 * @param reg_no 登録番号
	 * @param item_id 項目ID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int count(String reg_no,String item_id) throws DataAccessException {

		return jdbcTemplate.queryForObject(SQL_COUNT, Integer.class, reg_no, item_id);
	}

	/**
	 * 付帯情報を登録番号とキーワードから存在確認します。<br>
	 * @param reg_no 登録番号
	 * @param value 検索キーワード
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public int search(String reg_no,String value) throws DataAccessException {

		return jdbcTemplate.queryForObject(SQL_SEARCH, Integer.class, reg_no, "%" + value + "%");
	}

	/**
	 * 付帯情報を追加します。<br>
	 * @param additionalInfoModel 付帯情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int insert(AdditionalInfoModel additionalInfoModel) throws DataAccessException {
		return  npJdbcTemplate.update(SQL_INSERT, new MapSqlParameterSource()
				.addValue(COLUMN_REG_NO, additionalInfoModel.getReg_no())
				.addValue(COLUMN_ITEM_ID, additionalInfoModel.getItem_id())
				.addValue(COLUMN_ITEM_VALUE, additionalInfoModel.getItem_value())
				.addValue(COLUMN_UPDATE_USER_ID, additionalInfoModel.getUpdate_user_id()));
	}

	/**
	 * 付帯情報を更新します。<br>
	 * @param additionalInfoModel 付帯情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int update(AdditionalInfoModel additionalInfoModel) throws DataAccessException {
		return npJdbcTemplate.update(SQL_UPDATE, new MapSqlParameterSource()
				.addValue(COLUMN_REG_NO, additionalInfoModel.getReg_no())
				.addValue(COLUMN_ITEM_ID, additionalInfoModel.getItem_id())
				.addValue(COLUMN_ITEM_VALUE, additionalInfoModel.getItem_value())
				.addValue(COLUMN_UPDATE_USER_ID, additionalInfoModel.getUpdate_user_id()));
	}

	/**
	 * 対象の付帯情報を削除します。<br>
	 * @param reg_no 登録番号
	 * @param item_id 項目ID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public void delete(String reg_no, String item_id) throws DataAccessException {
		jdbcTemplate.update(SQL_ONE_DELETE, reg_no, item_id);
	}

	/**
	 * 対象の付帯情報を全て削除します。<br>
	 * @param reg_no 登録番号
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public void delete_regno(String reg_no) throws DataAccessException {
		jdbcTemplate.update(SQL_ALL_DELETE, reg_no);
	}

}
