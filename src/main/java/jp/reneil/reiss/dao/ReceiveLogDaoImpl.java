/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.model.ReceiveLogCsvModel;
import jp.reneil.reiss.model.ReceiveLogModel;
import jp.reneil.reiss.model.ReceiveLogSysCsvModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 受信ログ_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.0
 */
public class ReceiveLogDaoImpl extends JdbcDaoSupport implements ReceiveLogDao {

	/** カラム名：受信番号（連番） */
	private static final String COLUMN_RECV_NO = "RECV_NO";

	/** カラム名：送信元IP */
	private static final String COLUMN_SENDER_IP = "SENDER_IP";

	/** カラム名：ファイル名 */
	private static final String COLUMN_FILE_NAME = "FILE_NAME";

	/** カラム名：テナントID */
	private static final String COLUMN_TENANT_ID = "TENANT_ID";

	/** カラム名：テナント名 */
	private static final String COLUMN_TENANT_NAME = "TENANT_NAME";

	/** カラム名：端末ID */
	private static final String COLUMN_TERM_ID = "TERM_ID";

	/** カラム名：端末名 */
	private static final String COLUMN_TERM_NAME = "TERM_NAME";

	/** カラム名：受信区分ID */
	private static final String COLUMN_RECV_DIVISION = "RECV_DIVISION";

	/** カラム名：受信区分 */
	private static final String COLUMN_DIVISION_NAME = "DIVISION_NAME";

	/** カラム名：処理結果ID */
	private static final String COLUMN_RESULT_ID = "RESULT_ID";

	/** カラム名：処理結果 */
	private static final String COLUMN_RESULT_NAME = "RESULT_NAME";

	/** カラム名：内容 */
	private static final String COLUMN_CONTENTS = "CONTENTS";

	/** カラム名：エラーメッセージ */
	private static final String COLUMN_ERR_MSG = "ERR_MSG";

	/** カラム名：開始日時 */
	private static final String COLUMN_START_DATE = "START_DATE";

	/** カラム名：終了日時 */
	private static final String COLUMN_END_DATE = "END_DATE";

	/** カラム名：更新日時 */
	private static final String COLUMN_UPDATE_DATE = "UPDATE_DATE";

	/** 受信ログ取得 */
	private static final String SQL_SELECT_LOG =
			  "SELECT TOP 10000"
			+ " RL.[RECV_NO],"
			+ " RL.[SENDER_IP],"
			+ " RL.[FILE_NAME],"
			+ " RL.[TENANT_ID],"
			+ " TE.[TENANT_NAME] AS TENANT_NAME,"
			+ " RL.[TERM_ID],"
			+ " TM.[TERM_NAME] AS TERM_NAME,"
			+ " RL.[RECV_DIVISION],"
			+ " CD1.[NAME_01] AS DIVISION_NAME,"
			+ " RL.[RESULT_ID],"
			+ " CD2.[NAME_01] AS RESULT_NAME,"
			+ " RL.[CONTENTS],"
			+ " RL.[ERR_MSG],"
			+ " RL.[START_DATE],"
			+ " RL.[END_DATE],"
			+ " RL.[UPDATE_DATE]"
			+ " FROM [RECEIVELOG] RL"
			+ "      LEFT JOIN [TENANT] TE ON RL.[TENANT_ID] = TE.[TENANT_ID]"
			+ "      LEFT JOIN [TERMINAL] TM ON RL.[TERM_ID] = TM.[TERM_ID],"
			+ "      [CODE] CD1,"
			+ "      [CODE] CD2"
			+ " WHERE RL.[TENANT_ID] = TM.[TENANT_ID]"
			+ " AND RL.[TERM_ID] = TM.[TERM_ID]"
			+ " AND RL.[RECV_DIVISION] = CD1.[CODE_01] + CD1.[CODE_02]"
			+ " AND RL.[RESULT_ID] = CD2.[CODE_01] + CD2.[CODE_02]"
			+ " ORDER BY RL.[START_DATE] DESC";

	/** 受信ログ検索 */
	private static final String SQL_FIND_LOG =
			  "SELECT TOP 10000"
			+ " RL.[RECV_NO],"
			+ " RL.[SENDER_IP],"
			+ " RL.[FILE_NAME],"
			+ " RL.[TENANT_ID],"
			+ " TE.[TENANT_NAME] AS TENANT_NAME,"
			+ " RL.[TERM_ID],"
			+ " TM.[TERM_NAME] AS TERM_NAME,"
			+ " RL.[RECV_DIVISION],"
			+ " CD1.[NAME_01] AS DIVISION_NAME,"
			+ " RL.[RESULT_ID],"
			+ " CD2.[NAME_01] AS RESULT_NAME,"
			+ " RL.[CONTENTS],"
			+ " RL.[ERR_MSG],"
			+ " RL.[START_DATE],"
			+ " RL.[END_DATE],"
			+ " RL.[UPDATE_DATE]"
			+ " FROM [RECEIVELOG] RL"
			+ "      LEFT JOIN [TENANT] TE ON RL.[TENANT_ID] = TE.[TENANT_ID]"
			+ "      LEFT JOIN [TERMINAL] TM ON RL.[TERM_ID] = TM.[TERM_ID],"
			+ "      [CODE] CD1,"
			+ "      [CODE] CD2"
			+ " WHERE RL.[TENANT_ID] = TM.[TENANT_ID]"
			+ " AND RL.[TERM_ID] = TM.[TERM_ID]"
			+ " AND RL.[RECV_DIVISION] = CD1.[CODE_01] + CD1.[CODE_02]"
			+ " AND RL.[RESULT_ID] = CD2.[CODE_01] + CD2.[CODE_02]"
			+ " AND RL.[START_DATE] >= ?"
			+ " AND RL.[START_DATE] <= ?";

	/** 受信ログ検索 */
	private static final String SQL_FIND_LOG_CSV =
			  "SELECT"
			+ " RL.[RECV_NO],"
			+ " RL.[SENDER_IP],"
			+ " RL.[FILE_NAME],"
			+ " RL.[TENANT_ID],"
			+ " TE.[TENANT_NAME] AS TENANT_NAME,"
			+ " RL.[TERM_ID],"
			+ " TM.[TERM_NAME] AS TERM_NAME,"
			+ " RL.[RECV_DIVISION],"
			+ " CD1.[NAME_01] AS DIVISION_NAME,"
			+ " RL.[RESULT_ID],"
			+ " CD2.[NAME_01] AS RESULT_NAME,"
			+ " RL.[CONTENTS],"
			+ " RL.[ERR_MSG],"
			+ " RL.[START_DATE],"
			+ " RL.[END_DATE],"
			+ " RL.[UPDATE_DATE]"
			+ " FROM [RECEIVELOG] RL"
			+ "      LEFT JOIN [TENANT] TE ON RL.[TENANT_ID] = TE.[TENANT_ID]"
			+ "      LEFT JOIN [TERMINAL] TM ON RL.[TERM_ID] = TM.[TERM_ID],"
			+ "      [CODE] CD1,"
			+ "      [CODE] CD2"
			+ " WHERE RL.[TENANT_ID] = TM.[TENANT_ID]"
			+ " AND RL.[TERM_ID] = TM.[TERM_ID]"
			+ " AND RL.[RECV_DIVISION] = CD1.[CODE_01] + CD1.[CODE_02]"
			+ " AND RL.[RESULT_ID] = CD2.[CODE_01] + CD2.[CODE_02]"
			+ " AND RL.[START_DATE] >= ?"
			+ " AND RL.[START_DATE] <= ?";

	/** 受信ログ検索（受信番号から検索） */
	private static final String SQL_RECV_NO_SELECT_LOG =
			  "SELECT"
			+ " RL.[RECV_NO],"
			+ " RL.[SENDER_IP],"
			+ " RL.[FILE_NAME],"
			+ " RL.[TENANT_ID],"
			+ " TE.[TENANT_NAME] AS TENANT_NAME,"
			+ " RL.[TERM_ID],"
			+ " TM.[TERM_NAME] AS TERM_NAME,"
			+ " RL.[RECV_DIVISION],"
			+ " CD1.[NAME_01] AS DIVISION_NAME,"
			+ " RL.[RESULT_ID],"
			+ " CD2.[NAME_01] AS RESULT_NAME,"
			+ " RL.[CONTENTS],"
			+ " RL.[ERR_MSG],"
			+ " RL.[START_DATE],"
			+ " RL.[END_DATE],"
			+ " RL.[UPDATE_DATE]"
			+ " FROM [RECEIVELOG] RL"
			+ "      LEFT JOIN [TENANT] TE ON RL.[TENANT_ID] = TE.[TENANT_ID]"
			+ "      LEFT JOIN [TERMINAL] TM ON RL.[TERM_ID] = TM.[TERM_ID],"
			+ "      [CODE] CD1,"
			+ "      [CODE] CD2"
			+ " WHERE RL.[TENANT_ID] = TM.[TENANT_ID]"
			+ " AND RL.[TERM_ID] = TM.[TERM_ID]"
			+ " AND RL.[RECV_DIVISION] = CD1.[CODE_01] + CD1.[CODE_02]"
			+ " AND RL.[RESULT_ID] = CD2.[CODE_01] + CD2.[CODE_02]"
			+ " AND RL.[RECV_NO] = ?";

	/** 受信ログ追加 */
	private static final String SQL_INSERT_LOG =
			"INSERT INTO RECEIVELOG("
			+ " SENDER_IP,"
			+ " FILE_NAME,"
			+ " TENANT_ID,"
			+ " TERM_ID,"
			+ " RECV_DIVISION,"
			+ " RESULT_ID,"
			+ " CONTENTS,"
			+ " ERR_MSG,"
			+ " START_DATE,"
			+ " END_DATE,"
			+ " UPDATE_DATE)"
			+ " VALUES("
			+ " :SENDER_IP,"
			+ " :FILE_NAME,"
			+ " :TENANT_ID,"
			+ " :TERM_ID,"
			+ " :RECV_DIVISION,"
			+ " :RESULT_ID,"
			+ " :CONTENTS,"
			+ " :ERR_MSG,"
			+ " :START_DATE,"
			+ " :END_DATE,"
			+ " GETDATE())";

	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/** JDBCテンプレートクラス（リプレースあり） */
	@Autowired
	private NamedParameterJdbcTemplate npJdbcTemplate;

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * 受信ログマッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class ReceiveLogMapper implements RowMapper<ReceiveLogModel> {

		/**
		 * 受信ログをマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public ReceiveLogModel mapRow(ResultSet rs, int rowNum) throws SQLException {

			// 受信ログクラス生成
			ReceiveLogModel model = new ReceiveLogModel();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			//受信番号（連番）
			model.setRecv_no(rs.getLong(COLUMN_RECV_NO));
			//送信元IP
			model.setSender_ip(rs.getString(COLUMN_SENDER_IP));
			//ファイル名
			model.setFile_name(rs.getString(COLUMN_FILE_NAME));
			//テナントID
			model.setTenant_id(rs.getString(COLUMN_TENANT_ID));
			//テナント名
			model.setTenant_name(rs.getString(COLUMN_TENANT_NAME));
			//端末ID
			model.setTerm_id(rs.getString(COLUMN_TERM_ID));
			//端末名
			model.setTerm_name(rs.getString(COLUMN_TERM_NAME));
			//受信区分ID
			model.setRecv_division(rs.getString(COLUMN_RECV_DIVISION));
			//受信区分
			model.setDivision_name(rs.getString(COLUMN_DIVISION_NAME));
			//処理結果ID
			model.setResult_id(rs.getString(COLUMN_RESULT_ID));
			//処理結果
			model.setResult_name(rs.getString(COLUMN_RESULT_NAME));
			//内容
			model.setContents(rs.getString(COLUMN_CONTENTS));
			//エラーメッセージ
			model.setErr_msg(rs.getString(COLUMN_ERR_MSG));
			//開始日時
			model.setStart_date(rs.getTimestamp(COLUMN_START_DATE));
			//String 開始日時
			model.setStr_start_date(fmt.format(rs.getTimestamp(COLUMN_START_DATE)));
			//終了日時
			model.setEnd_date(rs.getTimestamp(COLUMN_END_DATE));

			if (rs.getTimestamp(COLUMN_END_DATE) != null) {
				//String 終了日時
				model.setStr_end_date(fmt.format(rs.getTimestamp(COLUMN_END_DATE)));
			}

			//更新日時
			model.setUpdate_date(rs.getTimestamp(COLUMN_UPDATE_DATE));
			//String 更新日時
			model.setStr_update_date(fmt.format(rs.getTimestamp(COLUMN_UPDATE_DATE)));

			return model;
		}
	}

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * 受信ログcsvマッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class ReceiveLogCsvMapper implements RowMapper<ReceiveLogCsvModel> {

		/**
		 * 受信ログをマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public ReceiveLogCsvModel mapRow(ResultSet rs, int rowNum) throws SQLException {

			// 受信ログクラス生成
			ReceiveLogCsvModel model = new ReceiveLogCsvModel();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			//送信元IP
			model.setSender_ip(rs.getString(COLUMN_SENDER_IP));
			//ファイル名
			model.setFile_name(rs.getString(COLUMN_FILE_NAME));
			//端末ID
			model.setTerm_id(rs.getString(COLUMN_TERM_ID));
			//端末名
			model.setTerm_name(rs.getString(COLUMN_TERM_NAME));
			//受信区分
			model.setDivision_name(rs.getString(COLUMN_DIVISION_NAME));
			//処理結果
			model.setResult_name(rs.getString(COLUMN_RESULT_NAME));
			//内容
			model.setContents(rs.getString(COLUMN_CONTENTS));
			//エラーメッセージ
			model.setErr_msg(rs.getString(COLUMN_ERR_MSG));
			//String 開始日時
			model.setStr_start_date(fmt.format(rs.getTimestamp(COLUMN_START_DATE)));

			if (rs.getTimestamp(COLUMN_END_DATE) != null) {
				//String 終了日時
				model.setStr_end_date(fmt.format(rs.getTimestamp(COLUMN_END_DATE)));
			}

			return model;
		}
	}

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * 受信ログsysCsvマッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class ReceiveLogSysCsvMapper implements RowMapper<ReceiveLogSysCsvModel> {

		/**
		 * 受信ログをマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public ReceiveLogSysCsvModel mapRow(ResultSet rs, int rowNum) throws SQLException {

			// 受信ログクラス生成
			ReceiveLogSysCsvModel model = new ReceiveLogSysCsvModel();
			SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

			//受信番号（連番）
			model.setRecv_no(rs.getLong(COLUMN_RECV_NO));
			//String 開始日時
			model.setStr_start_date(fmt.format(rs.getTimestamp(COLUMN_START_DATE)));

			if (rs.getTimestamp(COLUMN_END_DATE) != null) {
				//String 終了日時
				model.setStr_end_date(fmt.format(rs.getTimestamp(COLUMN_END_DATE)));
			}

			//String 更新日時
			model.setStr_update_date(fmt.format(rs.getTimestamp(COLUMN_UPDATE_DATE)));
			//送信元IP
			model.setSender_ip(rs.getString(COLUMN_SENDER_IP));
			//ファイル名
			model.setFile_name(rs.getString(COLUMN_FILE_NAME));
			//テナントID
			model.setTenant_id(rs.getString(COLUMN_TENANT_ID));
			//テナント名
			model.setTenant_name(rs.getString(COLUMN_TENANT_NAME));
			//端末ID
			model.setTerm_id(rs.getString(COLUMN_TERM_ID));
			//端末名
			model.setTerm_name(rs.getString(COLUMN_TERM_NAME));
			//受信区分ID
			model.setRecv_division(rs.getString(COLUMN_RECV_DIVISION));
			//受信区分
			model.setDivision_name(rs.getString(COLUMN_DIVISION_NAME));
			//処理結果ID
			model.setResult_id(rs.getString(COLUMN_RESULT_ID));
			//処理結果
			model.setResult_name(rs.getString(COLUMN_RESULT_NAME));
			//内容
			model.setContents(rs.getString(COLUMN_CONTENTS));
			//エラーメッセージ
			model.setErr_msg(rs.getString(COLUMN_ERR_MSG));

			return model;
		}
	}

	/**
	 * 受信ログを返却します。<br>
	 * @return 受信ログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<ReceiveLogModel> select() throws DataAccessException {
		RowMapper<ReceiveLogModel> rowMapper = new ReceiveLogMapper();
		return (List<ReceiveLogModel>) jdbcTemplate.query(SQL_SELECT_LOG, rowMapper);
	}

	/**
	 * 受信ログを検索します。<br>
	 * @param start_date 検索開始日時
	 * @param end_date 検索終了日時
	 * @param tenant_id テナントID
	 * @param term_id 端末ID
	 * @param receive_code 受信区分コード
	 * @param result_code 処理結果コード
	 * @return 受信ログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<ReceiveLogModel> find(String start_date, String end_date, String tenant_id, String term_id, String receive_code, String result_code) throws DataAccessException {
		RowMapper<ReceiveLogModel> rowMapper = new ReceiveLogMapper();
		String strSQL = SQL_FIND_LOG;
		if (tenant_id.equals("")) {
			strSQL = strSQL + " AND RL.[TENANT_ID] <> ?";
		}else {
			strSQL = strSQL + " AND RL.[TENANT_ID] = ?";
		}
		if (term_id.equals("")) {
			strSQL = strSQL + " AND RL.[TERM_ID] <> ?";
		}else {
			strSQL = strSQL + " AND RL.[TERM_ID] = ?";
		}
		if (receive_code.equals("")) {
			strSQL = strSQL + " AND RL.[RECV_DIVISION] <> ?";
		}else {
			strSQL = strSQL + " AND RL.[RECV_DIVISION] = ?";
		}
		if (result_code.equals("")) {
			strSQL = strSQL + " AND RL.[RESULT_ID] <> ?";
		}else {
			strSQL = strSQL + " AND RL.[RESULT_ID] = ?";
		}
		strSQL = strSQL + " ORDER BY RL.[START_DATE] DESC";

		return (List<ReceiveLogModel>) jdbcTemplate.query(strSQL, rowMapper, start_date, end_date, tenant_id, term_id, receive_code, result_code);
	}

	/**
	 * 受信ログcsvを検索します。<br>
	 * @param start_date 検索開始日時
	 * @param end_date 検索終了日時
	 * @param tenant_id テナントID
	 * @param term_id 端末ID
	 * @param receive_code 受信区分コード
	 * @param result_code 処理結果コード
	 * @return 受信ログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<ReceiveLogCsvModel> findCsv(String start_date, String end_date, String tenant_id, String term_id, String receive_code, String result_code) throws DataAccessException {
		RowMapper<ReceiveLogCsvModel> rowMapper = new ReceiveLogCsvMapper();
		String strSQL = SQL_FIND_LOG_CSV;
		if (tenant_id.equals("")) {
			strSQL = strSQL + " AND RL.[TENANT_ID] <> ?";
		}else {
			strSQL = strSQL + " AND RL.[TENANT_ID] = ?";
		}
		if (term_id.equals("")) {
			strSQL = strSQL + " AND RL.[TERM_ID] <> ?";
		}else {
			strSQL = strSQL + " AND RL.[TERM_ID] = ?";
		}
		if (receive_code.equals("")) {
			strSQL = strSQL + " AND RL.[RECV_DIVISION] <> ?";
		}else {
			strSQL = strSQL + " AND RL.[RECV_DIVISION] = ?";
		}
		if (result_code.equals("")) {
			strSQL = strSQL + " AND RL.[RESULT_ID] <> ?";
		}else {
			strSQL = strSQL + " AND RL.[RESULT_ID] = ?";
		}
		strSQL = strSQL + " ORDER BY RL.[START_DATE] DESC";

		return (List<ReceiveLogCsvModel>) jdbcTemplate.query(strSQL, rowMapper, start_date, end_date, tenant_id, term_id, receive_code, result_code);
	}

	/**
	 * 受信ログsysCsvを検索します。<br>
	 * @param start_date 検索開始日時
	 * @param end_date 検索終了日時
	 * @param tenant_id テナントID
	 * @param term_id 端末ID
	 * @param receive_code 受信区分コード
	 * @param result_code 処理結果コード
	 * @return 受信ログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public List<ReceiveLogSysCsvModel> findSysCsv(String start_date, String end_date, String tenant_id, String term_id, String receive_code, String result_code) throws DataAccessException {
		RowMapper<ReceiveLogSysCsvModel> rowMapper = new ReceiveLogSysCsvMapper();
		String strSQL = SQL_FIND_LOG_CSV;
		if (tenant_id.equals("")) {
			strSQL = strSQL + " AND RL.[TENANT_ID] <> ?";
		}else {
			strSQL = strSQL + " AND RL.[TENANT_ID] = ?";
		}
		if (term_id.equals("")) {
			strSQL = strSQL + " AND RL.[TERM_ID] <> ?";
		}else {
			strSQL = strSQL + " AND RL.[TERM_ID] = ?";
		}
		if (receive_code.equals("")) {
			strSQL = strSQL + " AND RL.[RECV_DIVISION] <> ?";
		}else {
			strSQL = strSQL + " AND RL.[RECV_DIVISION] = ?";
		}
		if (result_code.equals("")) {
			strSQL = strSQL + " AND RL.[RESULT_ID] <> ?";
		}else {
			strSQL = strSQL + " AND RL.[RESULT_ID] = ?";
		}
		strSQL = strSQL + " ORDER BY RL.[START_DATE] DESC";

		return (List<ReceiveLogSysCsvModel>) jdbcTemplate.query(strSQL, rowMapper, start_date, end_date, tenant_id, term_id, receive_code, result_code);
	}

	/**
	 * 受信ログを返却します。（受信番号から検索）<br>
	 * @param recv_no 受信番号
	 * @return 受信ログ
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	public ReceiveLogModel recvNoSelect(String recv_no) throws DataAccessException {
		RowMapper<ReceiveLogModel> rowMapper = new ReceiveLogMapper();
		return (ReceiveLogModel) jdbcTemplate.queryForObject(SQL_RECV_NO_SELECT_LOG, rowMapper, recv_no);
	}

	/**
	 * 受信ログを追加します。<br>
	 * @param receiveLogModel 受信ログモデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public int insert(ReceiveLogModel receiveLogModel) throws DataAccessException {
		return  npJdbcTemplate.update(SQL_INSERT_LOG, new MapSqlParameterSource()
				.addValue(COLUMN_SENDER_IP, receiveLogModel.getSender_ip())
				.addValue(COLUMN_FILE_NAME, receiveLogModel.getFile_name())
				.addValue(COLUMN_TENANT_ID, receiveLogModel.getTenant_id())
				.addValue(COLUMN_TERM_ID, receiveLogModel.getTerm_id())
				.addValue(COLUMN_RECV_DIVISION, receiveLogModel.getRecv_division())
				.addValue(COLUMN_RESULT_ID, receiveLogModel.getResult_id())
				.addValue(COLUMN_CONTENTS, receiveLogModel.getContents())
				.addValue(COLUMN_ERR_MSG, receiveLogModel.getErr_msg())
				.addValue(COLUMN_START_DATE, receiveLogModel.getStart_date())
				.addValue(COLUMN_END_DATE, receiveLogModel.getEnd_date()));

	}

}
