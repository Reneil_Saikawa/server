/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.AdditionalInfoModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 付帯情報_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface AdditionalInfoDao {

	/**
	 * 付帯情報を返却します。<br>
	 * @return 付帯情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<AdditionalInfoModel> findAll() throws DataAccessException;

	/**
	 * 登録番号に沿った付帯情報を返却します。<br>
	 * @return 付帯情報
	 * @param reg_no 登録番号
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<AdditionalInfoModel> findInfo(String reg_no) throws DataAccessException;

	/**
	 * 付帯情報の存在確認をします。<br>
	 * @param reg_no 登録番号
	 * @param item_id 項目ID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int count(String reg_no,String item_id) throws DataAccessException;

	/**
	 * 付帯情報を登録番号とキーワードから存在確認します。<br>
	 * @param reg_no 登録番号
	 * @param value 検索キーワード
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int search(String reg_no,String value) throws DataAccessException;

	/**
	 * 付帯情報を追加します。<br>
	 * @param additionalInfoModel 付帯情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int insert(AdditionalInfoModel additionalInfoModel) throws DataAccessException;

	/**
	 * 付帯情報を更新します。<br>
	 * @param additionalInfoModel 付帯情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int update(AdditionalInfoModel additionalInfoModel) throws DataAccessException;

	/**
	 * 対象の付帯情報を削除します。<br>
	 * @param reg_no 登録番号
	 * @param item_id 項目ID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void delete(String reg_no, String item_id) throws DataAccessException;

	/**
	 * 対象の付帯情報を全て削除します。<br>
	 * @param reg_no 登録番号
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	void delete_regno(String reg_no) throws DataAccessException;

}
