/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import jp.reneil.reiss.model.SystemInfoModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * システム情報_テーブルアクセス制御クラスです。<br>
 *
 * @version 1.0.0
 */
public class SystemInfoDaoImpl extends JdbcDaoSupport implements SystemInfoDao {

	/** カラム名：システム名 */
	private static final String COLUMN_SYSTEM_NAME = "SYSTEM_NAME";

	/** カラム名：ルートパス */
	private static final String COLUMN_SYSTEM_ROOTPATH = "SYSTEM_ROOTPATH";

	/** カラム名：内部URL */
	private static final String COLUMN_SYSTEM_IN_URL = "SYSTEM_IN_URL";

	/** カラム名：外部URL */
	private static final String COLUMN_SYSTEM_OUT_URL = "SYSTEM_OUT_URL";

	/** カラム名：ディレクトリ画像 */
	private static final String COLUMN_DIRECTORY_IMAGE = "DIRECTORY_IMAGE";

	/** システム情報取得 */
	private static final String SQL_FINDALL =
			  "SELECT TOP 1 SYSTEM_NAME, SYSTEM_ROOTPATH, SYSTEM_IN_URL, SYSTEM_OUT_URL, DIRECTORY_IMAGE"
			+ " FROM SYSTEMINFO";

	/** システム情報更新 */
	private static final String SQL_UPDATE =
			  "UPDATE SYSTEMINFO"
			+ " SET SYSTEM_NAME = ?,"
			+ " SYSTEM_ROOTPATH = ?,"
			+ " SYSTEM_IN_URL = ?,"
			+ " SYSTEM_OUT_URL = ?,"
			+ " DIRECTORY_IMAGE = ?,"
			+ " UPDATE_DATE = GETDATE()"
			+ " WHERE SYSTEM_ID = 1";

	/** JDBCテンプレートクラス */
	@Autowired
	private JdbcTemplate jdbcTemplate;

	/**
	 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
	 * システム情報マッパークラスです。<br>
	 *
	 * @version 1.0.0
	 */
	protected class SysteminfoRowMapper implements RowMapper<SystemInfoModel> {
		/**
		 * システム情報をマッピングします。<br>
		 * @param rs 結果セット
		 * @param rowNum 結果セットの行番号
		 * @throws SQLException SQL操作にてエラーが発生した場合
		 */
		public SystemInfoModel mapRow(ResultSet rs, int rowNum) throws SQLException {

			SystemInfoModel model = new SystemInfoModel();

			// システム名
			model.setSystem_name(rs.getString(COLUMN_SYSTEM_NAME));
			// システムルートパス
			model.setSystem_rootpath(rs.getString(COLUMN_SYSTEM_ROOTPATH));
			// システム内部URL
			model.setSystem_in_url(rs.getString(COLUMN_SYSTEM_IN_URL));
			// システム外部URL
			model.setSystem_out_url(rs.getString(COLUMN_SYSTEM_OUT_URL));
			// ディレクトリ画像
			model.setDirectory_image(rs.getString(COLUMN_DIRECTORY_IMAGE));

			return model;
		}
	}

	/**
	 * システム情報を返却します。<br>
	 * @return システム情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public SystemInfoModel findAll() throws DataAccessException {
		RowMapper<SystemInfoModel> rowMapper = new SysteminfoRowMapper();
		return (SystemInfoModel) jdbcTemplate.queryForObject(SQL_FINDALL, rowMapper);
	}

	/**
	 * システム情報を更新します。<br>
	 * @param system_name システム名
	 * @param rootpath ルートパス
	 * @param inUrl 内部URL
	 * @param outUrl 外部URL
	 * @param directory_image ディレクトリ画像
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	@Override
	public void update(String system_name, String rootpath, String inUrl, String outUrl, String directory_image) throws DataAccessException {
		jdbcTemplate.update(SQL_UPDATE, system_name, rootpath, inUrl, outUrl, directory_image);
	}

}
