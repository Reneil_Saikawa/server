/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.AdditionalItemModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 付帯情報項目名_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface AdditionalItemDao {

	/**
	 * 付帯情報項目名を返却します。<br>
	 * @return 付帯情報項目名
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<AdditionalItemModel> findAll() throws DataAccessException;

	/**
	 * テナントに沿った付帯情報項目名を返却します。<br>
	 * @return 付帯情報項目名
	 * @param tenant_id テナントID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<AdditionalItemModel> findItem(String tenant_id) throws DataAccessException;

	/**
	 * テナントに沿った使用可能付帯情報項目名を返却します。<br>
	 * @return 付帯情報項目名
	 * @param tenant_id テナントID
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<AdditionalItemModel> useItem(String tenant_id) throws DataAccessException;

	/**
	 * 付帯情報項目名の存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @param item_id 項目ID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int count(String tenant_id,String item_id) throws DataAccessException;

	/**
	 * 付帯情報項目名の存在確認をします。（検索表示用）<br>
	 * @param tenant_id テナントID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int searchCount(String tenant_id) throws DataAccessException;

	/**
	 * 付帯情報項目名の存在確認をします。(帳票出力用)<br>
	 * @param tenant_id テナントID
	 * @param item_id 項目ID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int reportCount(String tenant_id, String item_id) throws DataAccessException;

	/**
	 * ライブラリ情報を追加します。<br>
	 * @param additionalitemModel 付帯情報項目名モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int insert(AdditionalItemModel additionalitemModel) throws DataAccessException;

	/**
	 * ライブラリ情報を更新します。<br>
	 * @param additionalitemModel 付帯情報項目名モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int update(AdditionalItemModel additionalitemModel) throws DataAccessException;

}
