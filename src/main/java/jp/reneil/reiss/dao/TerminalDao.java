/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;

import jp.reneil.reiss.model.TerminalModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 端末_テーブル操作用インターフェースクラスです。<br>
 *
 * @version 1.0.0
 */
public interface TerminalDao {

	/**
	 * 端末の存在確認をします。<br>
	 * @param tenant_id テナントID
	 * @param lib_id ライブラリID
	 * @param chk 実行SQLチェック
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int count(String tenant_id, String lib_id, String chk) throws DataAccessException;

	/**
	 * 端末の有効確認をします。<br>
	 * @param tenant_id テナントID
	 * @param lib_id ライブラリID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int auth(String tenant_id, String lib_id) throws DataAccessException;

	/**
	 * ライセンス端末数を返却します。<br>
	 * @param tenant_id テナントID
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int licenseCount(String tenant_id) throws DataAccessException;

	/**
	 * 端末情報を返却します。<br>
	 * @return 端末情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<TerminalModel> findAll() throws DataAccessException;

	/**
	 * 端末情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @param lib_id ライブラリID
	 * @return 端末情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<TerminalModel> findLib(String tenant_id, String lib_id) throws DataAccessException;

	/**
	 * テナントに所属する端末情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @return 端末情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<TerminalModel> findTenantAllTerm(String tenant_id) throws DataAccessException;

	/**
	 * 選択テナント以外に所属する端末情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @return 端末情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	List<TerminalModel> findNotTenantAllTerm(String tenant_id) throws DataAccessException;

	/**
	 * 端末情報を返却します。<br>
	 * @param tenant_id テナントID
	 * @param term_id 端末ID
	 * @return 端末情報
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	TerminalModel findTerm(String tenant_id, String term_id) throws DataAccessException;

	/**
	 * 端末情報を追加します。<br>
	 * @param terminaltModel 端末情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int insert(TerminalModel terminalModel) throws DataAccessException;

	/**
	 * 端末情報を更新します。<br>
	 * @param terminaltModel 端末情報モデルクラス
	 * @return レコード件数
	 * @throws DataAccessException DB操作にてエラーが発生した場合
	 */
	int update(TerminalModel terminalModel) throws DataAccessException;

}
