/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.servlet;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.SessionConst;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 動画用サーブレットクラスです。<br>
 *
 * @version 1.0.0
 */
@WebServlet("/videofile")
public class VideoFileServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public VideoFileServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(false);

		if (session != null && session.getAttribute(SessionConst.USER_ROOT_PATH) != null){

			// セッション情報からユーザのルートパスを取得
			String userRootPath = (String) session.getAttribute(SessionConst.USER_ROOT_PATH);

			// ルートパスに"Library"が含めれているか確認
			if (userRootPath.contains(CommonConst.LIBRARY)) {

		        String fileName = request.getParameter("name");
		        String filePath = userRootPath + fileName;

		    	Path file = Paths.get(filePath);
		    	long fileSize = Files.size(file);

		    	response.setContentType("video/mp4");
		    	response.setHeader("Accept-Range", "bytes");
		    	response.setHeader("Content-Length", String.valueOf(fileSize));

		    	String RangeNum = StringUtils.defaultIfEmpty(request.getHeader("Range"),"").replaceAll("bytes=", "");
		    	int offset = Integer.parseInt(StringUtils.defaultIfEmpty(RangeNum.split("-")[0],"0"));
		    	int end = (int) fileSize - 1;

		    	// Rangeリクエストの末尾が指定あった場合それをセット
		    	if(RangeNum.split("-").length > 1){
		    	    end = Integer.parseInt(StringUtils.defaultIfEmpty(RangeNum.split("-")[1],"" + fileSize));
		    	}
		    	int len = end - offset + 1;

		    	// Rangeリクエストがあった場合ヘッダーに追加セット
		    	if(request.getHeader("Range") != null){
		    		response.setHeader("Accept-Range", "bytes");
		    		response.setStatus(HttpServletResponse.SC_PARTIAL_CONTENT);
		    		response.setHeader("Content-Length", String.valueOf(len));
		    		response.setHeader("Content-Range","bytes " + offset + "-" + end + "/" + fileSize);
		    	}

		    	String fileNameEncode = new String(fileName.getBytes("Windows-31J"), "8859_1");
		    	response.setHeader("Content-Disposition", "inline; filename=\"" + fileNameEncode + "\"");

		    	ServletOutputStream out = response.getOutputStream();
		    	FileInputStream fis = new FileInputStream(filePath);
		    	BufferedInputStream bis = new BufferedInputStream(fis);

		    	int contents;

		    	// 最後まで読み込む(Rangeリクエストがあった場合指定バイト数読み込み)
		    	int i = 0;
		    	try {
		        	while ((contents = bis.read()) != -1) {
		    	        if(i >=offset && i<=end){
		    		        // 読み込んだものを書き込む
		    		        out.write(contents);
		    	        }
		    	        i++;
		        	}
		    	}
		    	finally {
		           	fis.close();
		        	bis.close();
		            out.close();
		    	}

			}

		}

    }
}