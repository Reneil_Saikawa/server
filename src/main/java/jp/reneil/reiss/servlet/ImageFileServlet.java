/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.servlet;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.SessionConst;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 静止画用サーブレットクラスです。<br>
 *
 * @version 1.0.0
 */
@WebServlet("/imagefile")
public class ImageFileServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public ImageFileServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession(false);

		if (session != null && session.getAttribute(SessionConst.USER_ROOT_PATH) != null){

			// セッション情報からユーザのルートパスを取得
			String userRootPath = (String) session.getAttribute(SessionConst.USER_ROOT_PATH);

			// ルートパスに"Library"が含めれているか確認
			if (userRootPath.contains(CommonConst.LIBRARY)) {

		        String fileName = request.getParameter("name");

		        // ServletのOutputStream取得
		        ServletOutputStream out = response.getOutputStream();

		        // 画像ファイルをBufferedInputStreamを使用して読み取る
		    	FileInputStream fis = new FileInputStream(userRootPath + fileName);
		    	BufferedInputStream bis = new BufferedInputStream(fis);

		        // 画像を書き出す
		        try {
		        	bis.transferTo(out);
		        }finally {
		        	fis.close();
		        	bis.close();
		            out.close();
		        }

			}

		}

    }
}