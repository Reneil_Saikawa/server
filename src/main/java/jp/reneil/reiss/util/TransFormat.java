/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.util;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.reneil.reiss.common.CommonConst;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * FFmpegを利用した動画変換クラスです。<br>
 *
 * @version 1.0.0
 */
public class TransFormat {

	/* ログ制御 */
	private static Log log = LogFactory.getLog(TransFormat.class);

	/**
	 * デフォルトコンストラクタ<br>
	 */
	public TransFormat(){

	}

	/**
	 * ｍｐ４ => サムネイル作成<br>
	 * @param inputDir 入力ディレクトリ
	 * @param inputFile 入力ファイル名
 	 * @param outputDir 出力ディレクトリ
	 * @throws Exception
	 */
	public static void executeThumbnail(String inputDir,String inputFile,String outputDir, String ffmpegRoot) throws Exception {

		/** ファイル名（拡張子なし） */
    	String       FIL;

    	/* FFmpegTransパラメータ */
    	FFmpegTrans  sizeTran;
		FFmpegTrans  imgTran;

		/* サムネイルサイズ（縦） */
		Integer      ThumbnailSizeX;

		/* サムネイルサイズ（横） */
		Integer      ThumbnailSizeY;

    	if( inputDir  == null || inputDir.equals("")
		||  inputFile == null || inputFile.equals("")
		||  outputDir == null || outputDir.equals("") ) {
			throw new IllegalArgumentException("入力ファイル名と出力フォルダ名が指定されていません。");
		}

		//引数を解析する
		//ファイル名抽出
        if ( inputFile.lastIndexOf(CommonConst.DOT) > 0 ) {
        	FIL = inputFile.substring(0,inputFile.lastIndexOf(CommonConst.DOT));
		}else {
        	FIL = inputFile;
        }

		//入力ファイルパス編集
        String IF = inputDir + CommonConst.YEN_MRK + inputFile;

		//出力ファイルパス編集
        String OI = outputDir + CommonConst.YEN_MRK + FIL + CommonConst.JPG;

		//■ サイズ取得 ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
		//サイズを取得するパラメータを作成
		sizeTran = new FFmpegTrans(ffmpegRoot);
		sizeTran.setInputFile(IF);

		//動画の情報取得を実行
		sizeTran.exec();

		//サムネイルサイズを取得する
		ThumbnailSizeX = sizeTran.getThumbnailSizeX();
		ThumbnailSizeY = sizeTran.getThumbnailSizeY();

//		//■ サイズ指定 ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
//		//Tempファイルは削除するのでサイズ固定
//		//サムネイルサイズを指定する
//		ThumbnailSizeX = 640;
//		ThumbnailSizeY = 480;

		//■ サムネイル作成 ■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
		//サムネイルを作るパラメータを作成
		imgTran = new FFmpegTrans(ffmpegRoot);

		imgTran.setInputFile(IF);
		imgTran.setOutputFile(OI);
		imgTran.grabImage(0.0);

		imgTran.setSize(ThumbnailSizeX, ThumbnailSizeY);

		//サムネイルのトランスコードを実行
		try {
			imgTran.exec();
		}
		catch (Exception e) {
			//サムネイル作成時のエラーは無視する
			log.info("@@TransFormat.executeFLV サムネイル作成時のエラー=" + e.getMessage());
		}
	}
}
