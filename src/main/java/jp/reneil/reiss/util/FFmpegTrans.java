/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import jp.reneil.reiss.common.CommonConst;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * FFmpegのパラメータ編集クラスです。<br>
 *
 * @version 1.0.0
 */
public class FFmpegTrans {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(FFmpegTrans.class);

	/** ffmpegの実行ファイルのpath */
	private String            ffmpegExePath;

	/** 入力ファイル */
	private Path              inputFile   = null;

	/** 出力先ファイル */
	private Path              outputFile  = null;

	/** 最後の実行時のエラー */
	private String            lastErrout  = null;

	/** 最後の実行時の出力 */
	private String            lastStdout  = null;

	/** パラメータ用リスト */
	private ArrayList<String> commandList;

	/** 変換前の画面サイズ保持用（横） */
	private String            resolutionX = null;

	/** 変換前の画面サイズ保持用（縦） */
	private String            resolutionY = null;

	/** オプションを保存するMap */
	private final HashMap<String, String> OPT = new HashMap<String, String>();
	private static final String SEPARATOR     = "Video: ";

	/**
	 * デフォルトコンストラクタ（FFmpegのPATH指定を省略）<br>
	 */
	public FFmpegTrans() {
		this("./ffmpeg/ffmpeg.exe");
	}

	/**
	 * FFmpegのPATHを指定できるコンストラクタ<br>
	 * @param ffmpegExePath パス
	 */
	public FFmpegTrans(String ffmpegExePath) {
		this.ffmpegExePath = ffmpegExePath;
	}

	/**
	 * ffmpeg コマンドを実行する<br>
	 * @throws Exception : 変換に失敗した場合
	 */
	public void exec() throws Exception {
		commandList = makeCommandList();
		runProcess(commandList);
	}

	/**
	 * 入力ファイル<br>
	 * @param file パス付ファイル名
	 */
	public void setInputFile(String file) {
		inputFile  = Paths.get(file);
	}

	/**
	 * 出力ファイル<br>
	 * @param file パス付ファイル名
	 */
	public void setOutputFile(String file) {
		outputFile = Paths.get(file);
	}

	/**
	 * 出力フォーマット<br>
	 * @param format フォーマット
	 */
	public void setFormat(String format) {
		OPT.put("-f", format);
	}

	/**
	 * 動画のcodec<br>
	 */
	public void setVideoCodec264() {
		OPT.put("-c:v", "libx264");
	}

	/**
	 * 動画のcodec<br>
	 */
	public void setVideoCodec(String vcodec) {
		OPT.put("-vcodec", vcodec);
	}

	/**
	 * 動画のビットレート<br>
	 * @param kbps ビットレート
	 */
	public void setBitrate(Integer kbps) {
		String val = (kbps == null ? null : kbps.toString() + "k");
		OPT.put("-b", val);
	}

	/**
	 * 動画のフレームレート<br>
	 * @param rate フレームレート
	 */
	public void setFrameRate(Integer rate) {
		String val = (rate == null ? null : rate.toString());
		OPT.put("-r", val);
	}

	/**
	 * 動画のサイズ(文字列指定)<br>
	 * @param size サイズ
	 */
	public void setSize(String size) {
		OPT.put("-s", size);
	}

	/**
	 * 動画の縦・横サイズ<br>
	 * @param width 横サイズ
	 * @param height 縦サイズ
	 */
	public void setSize(Integer width, Integer height) {

		if(width == null || height == null) {
			throw new IllegalArgumentException("幅と高さは必須です");
		}

		setSize(width.toString() + "x" + height.toString());
	}

	/**
	 * 音声のcodec<br>
	 * @param acodec 音声のコーデック
	 */
	public void setAudioCodec(String acodec) {
		OPT.put("-acodec", acodec);
	}

	/**
	 * 音声のビットレート<br>
	 * @param rate 音声のビットレート
	 */
	public void setAudioBitRate(Integer rate) {
		String val = (rate == null ? null : rate.toString() + "k");
		OPT.put("-ab", val);
	}

	/**
	 * 音声のサンプリングレート<br>
	 * @param rate 音声のサンプリングレート
	 */
	public void setAudioSamplingRate(Integer freq) {
		String val = (freq == null ? null : freq.toString());
		OPT.put("-ar", val);
	}

	/**
	 * 最後に実行した時のエラー<br>
	 */
	public String getLastErrout() {
		return lastErrout;
	}

	/**
	 * 最後に実行した時の出力<br>
	 */
	public String getLastStdout() {
		return lastStdout;
	}

	/**
	 * 動画からsec秒後のサムネイルを作成する<br>
	 * exec()メソッドを呼んだ時点で処理が実行される<br>
	 * @param sec : 秒
	 */
	public void grabImage(Double sec) {

		if(sec ==null) {
			throw new IllegalArgumentException("秒は必須です");
		}

		/* 出力フォーマットはアクセサを使って設定 */
		this.setFormat("image2");

		/* その他のオプションのアクセサは未実装なので、直接設定 */
		OPT.put("-ss", sec.toString());
		OPT.put("-vframes", "1");
		OPT.put("-r"      , "1");
		OPT.put("-an"     , "" );
		OPT.put("-vsync"  , "2");
	}

	/**
	 * ProcessBuilder起動用のコマンドListを作る<br>
	 * @return コマンド文字列のList
	 */
	private ArrayList<String> makeCommandList() {
		ArrayList<String> commandList = new ArrayList<String>();

		//ffmpegコマンドを設定
		commandList.add(ffmpegExePath);

		//入力ファイルを設定
		commandList.add("-i");
		commandList.add(inputFile.toAbsolutePath().toString());

		//このオブジェクトに指定されているオプションを設定
		for( Map.Entry<String, String> entry : OPT.entrySet() ) {
			String key = entry.getKey();
			String val = entry.getValue();

			//値がnullのオプションは、無効とする
			if(val == null) {
				continue;
			}

			//オプション名を設定
			commandList.add(key);

			//このオプションに値があれば設定
			if ( !val.equals("") ) {
				commandList.add(val);
			}
		}

		//最後に、出力ファイル名を設定
		if (outputFile != null) {
			if (!StringUtils.isEmpty(outputFile.toAbsolutePath().toString())) {
				//上書きオプションを設定
				commandList.add("-y");
				commandList.add(outputFile.toAbsolutePath().toString());
			}
		}

		return commandList;
	}

	/**
	 * 入力されたコマンドを実行する<br>
	 * @param  commandList : コマンド文字列のList
	 * @throws Exception
	 */
	private void runProcess(List<String> commandList) throws Exception {
		int      ret;
		String   EXT;
		String[] WRK;
		String   resolution;

		ProcessBuilder PB = new ProcessBuilder(commandList);

		//出力メッセージを初期化
		lastStdout    = null;
		lastErrout    = null;

		Process   PRC = null;
		String    ERM = null; //エラーが発生した場合にメッセージを保持
		Throwable ERC = null; //エラーの原因となった例外を保持

		try {
			//エラーを標準出力にリダイレクト.
			PB.redirectErrorStream(true);

			//処理を開始し、終了まで待つ
			PRC = PB.start();

			lastStdout = inputStreamToString(PRC.getInputStream());

			//log.info("@@FFmpegTrans.runProcess Stdout=" + lastStdout);

			ret = PRC.waitFor();

			//log.info("@@FFmpegTrans.runProcess Return=" + ret);

			//戻り値を調べ、エラーがあれば記録
			EXT = inputFile.getFileName().toString().substring(inputFile.getFileName().toString().lastIndexOf(CommonConst.DOT), inputFile.getFileName().toString().length()).toUpperCase();

			if ( CommonConst.MP4.equals(EXT) && ret != 0 ) {
				ERM = "コマンドでエラーが発生しました(exit code=" + ret + ")";
			}else {
				WRK         = lastStdout.split(SEPARATOR);
				WRK         = WRK[1].split(CommonConst.COMMA, 4);

				try {

					//log.info("@@FFmpegTrans.runProcess WRK[2].indexOf(\"[\")=" + WRK[2].indexOf("["));

					if ( WRK[2].indexOf("[") > -1 ) {
						//Input Type : mp4v(GVC)
						WRK         = WRK[2].split("\\[");
						resolution  = WRK[0].replace(CommonConst.SPACE, "");
						WRK         = resolution.split("x");
						resolutionX = WRK[0];
						resolutionY = WRK[1];
					}else {
						if ( WRK[0].indexOf("flv1") > -1 || WRK[0].indexOf("vp8") > -1 || WRK[0].indexOf("vp9") > -1 ) {
							//Input Type : flv
							WRK         = WRK[2].split(CommonConst.COMMA, 2);
						}
						else {
							//Input Type : AVC1(GVC)
							WRK         = WRK[3].split(CommonConst.COMMA, 3);
						}

						if ( WRK[0].indexOf("progressive") > -1 ) {
							//プログレッシブダウンロード対応動画の場合
							resolution  = WRK[1].replace(CommonConst.SPACE, "");
						}
						else {
							resolution  = WRK[0].replace(CommonConst.SPACE, "");
						}

						WRK         = resolution.split("x");
						resolutionX = WRK[0];
						resolutionY = WRK[1];
					}

				}
				catch(ArrayIndexOutOfBoundsException e)
				{
				}

				//log.info("@@FFmpegTrans.runProcess resolutionX=" + resolutionX);
				//log.info("@@FFmpegTrans.runProcess resolutionY=" + resolutionY);
			}
		}
		catch(InterruptedException e) {
			//割り込みが発生したので、処理をkillする
			PRC.destroy();
			PRC = null;

			//上位処理に、割り込みがあったことを伝える
			Thread.currentThread().interrupt();

			ERM = "中断されました";
			ERC   = e;
		}
		catch(IOException e) {
			ERM = "コマンドが開始できませんでした";
			ERC   = e;
		}
		finally {
			//コマンドからの出力を集める
			if(PRC != null) {
				lastErrout = inputStreamToString(PRC.getErrorStream());
			}
		}

		//変換処理中にエラーが起きていれば、例外をthrowする
		if(ERM != null) {
			//ffmpegコマンドからエラー出力があれば、それを例外に記録
			if(lastErrout != null) {
				ERM += "\n" + lastErrout;
			}

			throw new Exception(ERM, ERC);
		}
	}


	/**
	 * InputStreamを文字列に変換する<br>
	 * @param stream 任意のInputStream
	 * @return streamに含まれていた文字列
	 * @throws Exception
	 */
	private String inputStreamToString(InputStream stream) throws Exception {
		StringBuilder  SB = new StringBuilder();

		//入力をバッファして読み込みを効率化する(デフォルトの文字コードを利用)
		BufferedReader BR = new BufferedReader(new InputStreamReader(stream));

		try {
			try {
				//入力ストリームからの文字列を全て連結
				String line = null;

				while ((line = BR.readLine()) != null) {
					SB.append(line).append("\n");
				}
			}
			finally {
				//必ずクローズする
				BR.close();
			}
		}
		catch (IOException e) {
			throw new Exception("結果の読み込みに失敗しました", e);
		}

		return SB.toString();
	}

	/**
	 * サムネイルサイズ（横） ゲッター<br>
	 */
	public Integer getThumbnailSizeX() {
		Float   wrkX;
		Float   wrkY;
		Float   wrk1;
		Integer wrk2;

		try {
			//元動画の画面サイズから縦横比を判定してサムネイルサイズを決定する
			wrkX = Float.valueOf(resolutionX);
			wrkY = Float.valueOf(resolutionY);

			wrk1 = wrkX / wrkY;

			log.info("@@FFmpegTrans.getThumbnailSizeX X / Y=" + wrk1);

			if ( wrk1 > 1.2 ) {
				//横撮影のサムネイルサイズ（横）
				wrk2 = 640;
			}else if ( wrk1 < 0.8 ){
				//縦撮影のサムネイルサイズ（横）
				wrk2 = 480;
			}else {
				//正方形サムネイル
				wrk2 = 640;
			}
		}
		catch(Exception e) {
			//取得できなかったときのデフォルトは 640 X 480
			wrk2 = 640;
		}

		return wrk2;
	}

	/**
	 *サムネイルサイズ（縦） ゲッター<br>
	 */
	public Integer getThumbnailSizeY() {
		Float   wrkX;
		Float   wrkY;
		Float   wrk1;
		Integer wrk2;

		try {
			//元動画の画面サイズから縦横比を判定してサムネイルサイズを決定する
			wrkX = Float.valueOf(resolutionX);
			wrkY = Float.valueOf(resolutionY);

			wrk1 = wrkX / wrkY;

			log.info("@@FFmpegTrans.getThumbnailSizeY X / Y=" + wrk1);

			if ( wrk1 > 1.2 ) {
				//横撮影のサムネイルサイズ（縦）
				wrk2 = 480;
			}else if ( wrk1 < 0.8 ){
				//縦撮影のサムネイルサイズ（縦）
				wrk2 = 640;
			}else {
				//正方形サムネイル
				wrk2 = 640;
			}
		}
		catch(Exception e) {
			//取得できなかったときのデフォルトは 640 X 480
			wrk2 = 480;
		}

		return wrk2;
	}

	/**
	 * 画面サイズ（横） ゲッター<br>
	 */
	public Float getWidthSize() {
		return Float.valueOf(resolutionX);
	}

	/**
	 * 画面サイズ（縦） ゲッター<br>
	 */
	public Float getHeightSize() {
		return Float.valueOf(resolutionY);
	}
}
