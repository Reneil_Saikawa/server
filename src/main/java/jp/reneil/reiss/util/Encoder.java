/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * エンコーダークラスです。<br>
 *
 * @version 1.0.0
 */
public class Encoder {
	/**
	 * Base64文字列にエンコードします.<br>
	 * @param filename ファイル名（絶対パス）
	 * @return Base64文字列
	 */
	public static String Base64Encode(String filename) throws Exception {

		Path p = Paths.get(filename);
		byte[] bImage = Files.readAllBytes(p);

		// バイト配列→BASE64へ変換する
		byte[] encoded = Base64.getEncoder().encode(bImage);
		String base64Image = new String(encoded);

		return base64Image;
	}
}
