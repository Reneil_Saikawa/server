/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2020/03/03      1.0.1           女屋            Linux版対応追加
 */
package jp.reneil.reiss.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * パトライト発報クラスです。<br>
 *
 * @version 1.0.1
 */
public class PatLite
{

	/* ログ制御 */
	private static Log log = LogFactory.getLog(PatLite.class);

	/**
	 * デフォルトコンストラクタ<br>
	 */
	public PatLite()
	{
	}

	/**
	 * パトライト発報．<br>
	 * @param patLiteCommand パトライトコマンド
	 * @param patLiteIP パトライトアドレス
	 * @param patLiteLoginName パトライトログイン名
	 * @param patLiteOption パトライトオプション
	 * @param patLiteTime パトライト鳴動時間(秒)
	 * @throws Exception
	 */
	public static void call(String patLiteCommand, String patLiteIP, String patLiteLoginName, String patLiteOption, String patLiteTime) throws Exception {

		log.info("@@PatLite.call patLiteCommand = " + patLiteCommand);
    	log.info("@@PatLite.call patLiteIP = " + patLiteIP);
    	log.info("@@PatLite.call patLiteLoginName = " + patLiteLoginName);
    	log.info("@@PatLite.call patLiteOption = " + patLiteOption);
    	log.info("@@PatLite.call patLiteTime = " + patLiteTime);

		//パラメータ設定
		PatLiteExec PLE = new PatLiteExec(patLiteCommand, patLiteIP, patLiteLoginName, patLiteOption, patLiteTime);

		// Linux版対応 on 2020.03.03
		if (PlatformUtil.isWindows()) {

			Path commandFile = Paths.get(patLiteCommand);

			// irshファイル存在確認
			if (Files.exists(commandFile)) {
				//パトライト呼び出しを実行
		    	PLE.exec();
		    	log.info("@@PatLite.call for Windows 実行");
			}else {
				log.info("@@PatLite.call irsh.exeファイル無し");
			}

		}else {
			// パトライト呼び出しを実行
	    	PLE.exec();
	    	log.info("@@PatLite.call for Linux 実行");
		}

	}

}
