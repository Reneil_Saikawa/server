/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2020/03/03      1.0.0           斎川            新規作成
 */
package jp.reneil.reiss.util;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 実行しているプラットフォームのＯＳを確認するクラスです。<br>
 *
 * @version 1.0.0
 */
public class PlatformUtil {
	private static final String OS_NAME = System.getProperty("os.name").toLowerCase();
	  
	public static boolean isLinux() {
		return OS_NAME.startsWith("linux");
	}
 	 
	public static boolean isWindows() {
		return OS_NAME.startsWith("windows");
	}
}
