/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.util;

import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.stereotype.Service;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * TripleDES変換ユーティリティクラスです。<br>
 *
 * @version 1.0.0
 */
@Service
public class TripleDesConverter {

	/** エンコード文字 */
	private static final char[] ENC_CHARS =
		"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-_".toCharArray();

	/** 共通鍵（文字列）・・・24文字 */
	private static final String REISS_AM_KEY = "reissTripleDESConvertUtl";

	/** 初期化ベクトル（文字列）・・・8文字 */
	private static final String REISS_AM_IV = "12345678";

	/** 暗号化キー */
	private Key skey;

	/** アルゴリズムパラメータ */
	private AlgorithmParameterSpec ivParamSpec;

	/** 暗号化・復号化クラスのインスタンス */
	private Cipher cipher;

	/**
	 * TripleDES変換ユーティリティクラスを構築します。
	 * @throws Exception 暗号化・復号化クラスの取得に失敗した場合
	 */
	public TripleDesConverter() throws Exception {
		// 暗号化キーの生成
		byte[] keyBytes = getKey();
		this.skey = new SecretKeySpec(keyBytes,"DESede");

		// アルゴリズムパラメータの生成
		byte[] vectorBytes = getInitVector();
		this.ivParamSpec = new IvParameterSpec(vectorBytes);

		// 暗号化および復号クラスの取得
		cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding", "SunJCE");
	}

	/**
	 * 暗号化を行います。
	 * @param str 平文データ
	 * @return 暗号化したデータ
	 * @throws Exception 暗号化に失敗した場合
	 */
	public String encrypt64 (String str) throws Exception {
		return bytesToString64(encrypt(str.getBytes()));
	}

	/**
	 * 復号を行います。
	 * @param str 暗号化データ
	 * @return 復号したデータ
	 * @throws CRSException 復号に失敗した場合
	 */
	public String decrypt64(String str) throws Exception {
		return new String(decrypt(string64ToBytes(str)));
	}

	/**
	 * 64進数変換を行います。
	 *
	 * @param bytes byte配列データ
	 * @return 64進数変換後文字列
	 */
	private String bytesToString64(byte[] bytes) {
		final int max = bytes.length;
		int n;
		int index;
		int code;
		int remain;
		StringBuffer buff = new StringBuffer();

		remain = 0;
		for (n = 0 ; n < max ; n++) {
			code = ((int)bytes[n]) & 0x00FF;
			switch (n % 3) {
			case 0:
				index = ( code >> 2 ) & 0x3F;
				buff.append( ENC_CHARS[index] );
				remain = ( code & 0x03 ) << 4;
				break;

			case 1:
				index = ( ( code >> 4 ) & 0x0F ) | remain;
				buff.append( ENC_CHARS[index] );
				remain = ( code & 0x0F ) << 2;
				break;

			case 2:
				index = ( ( code >> 6 ) & 0x03 ) | remain;
				buff.append( ENC_CHARS[index] );
				index = ( ( code ) & 0x3F );
				buff.append( ENC_CHARS[index] );
				remain = 0;
				break;

			default:
				break;

			}
		}
		if (( max % 3 ) != 0) {
			buff.append( ENC_CHARS[remain] );
		}

		return buff.toString();
	}

	/**
	 * byte配列変換を行います。
	 *
	 * @param str 64進数文字列
	 * @return byte配列変換後データ
	 */
	private byte[] string64ToBytes(String str) {
		int n;
		int data = 0;
		int len = str.length() * 3 / 4;
		byte[] dataArray = new byte[len];
		int cnt = 0;
		int setIdx = 0;

		for (n = 0 ; n < str.length() ; n++) {
			// 64進数文字取得
			char c = str.charAt(n);
			// 6ビットシフト
			data <<= 6;
			cnt++;

			// 64進数文字のインデックスを取得
			if (c >= '0' && c <= '9') {
				data |= c - '0';
			} else if (c >= 'A' && c <= 'Z') {
				data |= c - 'A' + 0x0a;
			} else if (c >= 'a' && c <= 'z') {
				data |= c - 'a' + 0x24;
			} else if (c == '-') {
				data |= 0x3e;
			} else if (c == '_') {
				data |= 0x3f;
			}

			if (cnt == 4) {
				// 4文字分のデータが設定された場合
				cnt = 0;
				dataArray[setIdx++] = (byte)((data >> 16) & 0x00ff);
				dataArray[setIdx++] = (byte)((data >> 8) & 0x00ff);
				dataArray[setIdx++] = (byte)(data & 0x00ff);
				data = 0;
			}
		}

		if (cnt != 0) {
			// 未設定のデータがある場合
			switch (cnt) {
			case 1:
				dataArray[setIdx] = (byte)((data >> 2) & 0x00ff);
				break;
			case 2:
				dataArray[setIdx] = (byte)((data >> 4) & 0x00ff);
				break;
			case 3:
				dataArray[setIdx++] = (byte)((data >> 10) & 0x00ff);
				dataArray[setIdx] = (byte)((data >> 2) & 0x00ff);
				break;
			default:
				break;
			}
		}
		return dataArray;
	}

	/**
	 * 暗号化を行います。
	 * @param data 平文データ
	 * @return 暗号化したデータ
	 * @throws Exception 暗号化に失敗した場合
	 */
	public byte[] encrypt(byte[] data) throws Exception {
		cipher.init(Cipher.ENCRYPT_MODE,
					getSecretKeySpec(),
					getAlgorithmParameterSpec());
		return cipher.doFinal(data);
	}
	/**
	 * 復号を行います。
	 * @param data 暗号化データ
	 * @return 復号したデータ
	 * @throws Exception 復号に失敗した場合
	 */
	public byte[] decrypt(byte[] data) throws Exception {
		cipher.init(Cipher.DECRYPT_MODE,
					getSecretKeySpec(),
					getAlgorithmParameterSpec());
		return cipher.doFinal(data);
	}

	/**
	 * 鍵を取得します。
	 * @return 鍵(24byte)
	 */
	byte[] getKey() {
		return REISS_AM_KEY.getBytes();
	}

	/**
	 * 初期化ベクトルを取得します。
	 * @return 初期化ベクトル(8byte)
	 */
	byte[] getInitVector() {
		return REISS_AM_IV.getBytes();
	}

	/**
	 * 暗号化キーを取得します。
	 * @return 暗号化キー
	 */
	Key getSecretKeySpec() {
		return this.skey;
	}

	/**
	 * アルゴリズムパラメータを取得します。
	 * @return アルゴリズムパラメータ
	 */
	AlgorithmParameterSpec getAlgorithmParameterSpec() {
		return this.ivParamSpec;
	}
}
