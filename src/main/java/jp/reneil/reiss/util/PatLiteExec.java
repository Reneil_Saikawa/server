/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * パトライト発報のパラメータ編集＆実行クラスです。<br>
 *
 * @version 1.0.0
 */
public class PatLiteExec
{

	/* ログ制御 */
	private static Log log = LogFactory.getLog(PatLiteExec.class);

	private String                                PatLiteCommand;
	private String                                PatLiteIPAdress;
	private String                                PatLiteLoginName;
	private String                                PatLiteOption;
	private String                                PatLiteTime;
	private String                                lastErrout       = null;   // 最後の実行時のエラー
	private String                                lastStdout       = null;   // 最後の実行時の出力
	private ArrayList<String>                     commandList;     	         // パラメータ用リスト

	/* オプションを保存するMap */
	private final LinkedHashMap<String, String>   OPT              = new LinkedHashMap<String, String>();

	private final static String                   IS_NONE          = "";

	/**
	 * デフォルトコンストラクタ<br>
	 */
	public PatLiteExec(){}

	/**
	 * 起動時パラメータ取得コンストラクタ. <br>
	 * @param PatLiteCommand パトライトコマンド
	 * @param PatLiteIPAdress パトライトアドレス
	 * @param PatLiteLoginName パトライトログイン名
	 * @param PatLiteOption パトライトオプション
	 * @param PatLiteTime パトライト鳴動時間(秒)
	 */
	public PatLiteExec(String PatLiteCommand, String PatLiteIPAdress, String PatLiteLoginName, String PatLiteOption, String PatLiteTime) {

		//パラメータの設定
		setParm(PatLiteCommand, PatLiteIPAdress, PatLiteLoginName, PatLiteOption, PatLiteTime);

	}

	/**
	 * 起動時パラメータ設定コンストラクタ. <br>
	 * @param PatLiteCommand パトライトコマンド
	 * @param PatLiteIPAdress パトライトアドレス
	 * @param PatLiteLoginName パトライトログイン名
	 * @param PatLiteOption パトライトオプション
	 * @param PatLiteTime パトライト鳴動時間(秒)
	 */
	private void setParm(String PatLiteCommand, String PatLiteIPAdress, String PatLiteLoginName, String PatLiteOption, String PatLiteTime)	{

		//パラメータの設定
		this.PatLiteCommand    = PatLiteCommand;
		this.PatLiteIPAdress   = PatLiteIPAdress;
		this.PatLiteLoginName  = PatLiteLoginName;
		setPatLiteLoginName(this.PatLiteLoginName);
		this.PatLiteOption     = PatLiteOption;
		setPatLiteOption(this.PatLiteOption);
		this.PatLiteTime       = PatLiteTime;

	}

	/**
	 * パトライト コマンドを実行する. <br>
	 * @throws Exception : 変換に失敗した場合
	 */
	public void exec() throws Exception
	{
		//パトライト発報用コマンドの引数の編集
		commandList = makeCommandList();

		//パトライト発報用コマンドの実行
		runProcess(commandList);
	}

	/**
	 * パトライトＩＰアドレス. <br>
	 */
	public void setPatLiteIPAdress(String PatLiteIPAdress)
	{
		this.PatLiteIPAdress = PatLiteIPAdress;
	}

	/**
	 * パトライトログイン名. <br>
	 */
	public void setPatLiteLoginName(String PatLiteLoginName)
	{
		OPT.put("-l", PatLiteLoginName);
	}

	/**
	 * コマンド・オプション. <br>
	 */
	public void setPatLiteOption(String PatLiteOption)
	{
		OPT.put("alert", PatLiteOption);
	}

	/**
	 * パトライト鳴動時間. <br>
	 */
	public void setPatLiteTime(String PatLiteTime)
	{
		this.PatLiteTime = PatLiteTime;
	}

	/**
	 * 最後に実行した時のエラー. <br>
	 */
	public String getLastErrout()
	{
		return lastErrout;
	}

	/**
	 * 最後に実行した時の出力. <br>
	 */
	public String getLastStdout()
	{
		return lastStdout;
	}

	/**
	 * ProcessBuilder起動用のコマンドListを作る. <br>
	 * @return コマンド文字列のList
	 */
	private ArrayList<String> makeCommandList()
	{
        Set     <Map.Entry<String, String>> entrySet;
        Iterator<Map.Entry<String, String>> ITE;
        Map.Entry<String, String>           entry;

		String            Prm;
		String            Val;
		ArrayList<String> commandList   = new ArrayList<String>();

		//パトライト発報用コマンドを設定
		commandList.add(PatLiteCommand);

		//パトライトＩＰアドレスを設定
		commandList.add(PatLiteIPAdress);

		//このオブジェクトに指定されているオプションを設定

        //Mapから全てのキーと値のエントリをSet型のコレクションとして取得する
        entrySet = OPT.entrySet();
        ITE      = entrySet.iterator();

        //キーと値のコレクションの反復子を取得する
        while(ITE.hasNext())
        {
			//次の要素がまだ存在する場合はtrueが返される

			//キーと値をセットを持つ、Map.Entry型のオブジェクトを取得する
			entry = ITE.next();

			//Map.Entry型のオブジェクトからキー・値を取得する
			Prm = entry.getKey();
			Val = entry.getValue();

			//値がnullのオプションは、無効とする
			if ( Prm != null && !IS_NONE.equals(Prm)
			&&   Val != null && !IS_NONE.equals(Val) )
			{
				//オプション名・値を設定
				commandList.add(Prm);
				commandList.add(Val);
			}
        }

		//鳴動時間を設定
		commandList.add(PatLiteTime);

		return commandList;
	}

	/**
	 * 入力されたコマンドを実行する. <br>
	 * @param  commandList : コマンド文字列のList
	 * @throws Exception
	 */
	private void runProcess(List<String> commandList) throws Exception
	{
		int      ret;

		ProcessBuilder PB = new ProcessBuilder(commandList);

		//出力メッセージを初期化
		lastStdout    = null;
		lastErrout    = null;

		Process   PRC = null;
		String    ERM = null; //エラーが発生した場合にメッセージを保持
		Throwable ERC = null; //エラーの原因となった例外を保持

		try
		{
			//エラーを標準出力にリダイレクト.
			PB.redirectErrorStream(true);

			//処理を開始し、終了まで待つ
			PRC = PB.start();

			lastStdout = inputStreamToString(PRC.getInputStream());

			log.info("@@PatLiteExec.runProcess Stdout = " + lastStdout);

			ret = PRC.waitFor();

			log.info("@@PatLiteExec.runProcess Return = " + ret);

			//戻り値を調べ、エラーがあれば記録
		}
		catch(InterruptedException e)
		{
			//割り込みが発生したので、処理をkillする
			PRC.destroy();
			PRC = null;

			//上位処理に、割り込みがあったことを伝える
			Thread.currentThread().interrupt();

			ERM = "中断されました";
			ERC   = e;
		}
		catch(IOException e)
		{
			ERM = "コマンドが開始できませんでした";
			ERC   = e;
		}
		finally
		{
			//コマンドからの出力を集める
			if(PRC != null)
			{
				lastErrout = inputStreamToString(PRC.getErrorStream());
			}
		}

		//変換処理中にエラーが起きていれば、例外をthrowする
		if(ERM != null)
		{
			//パトライト（リモートシェル）コマンドからエラー出力があれば、それを例外に記録
			if(lastErrout != null)
			{
				ERM += "\n" + lastErrout;
			}

			throw new Exception(ERM, ERC);
		}
	}

	/**
	 * InputStreamを文字列に変換する. <br>
	 * @param  stream 任意のInputStream
	 * @return streamに含まれていた文字列
	 * @throws Exception
	 */
	private String inputStreamToString(InputStream stream) throws Exception
	{
		StringBuilder  SB = new StringBuilder();

		//入力をバッファして読み込みを効率化する(デフォルトの文字コードを利用)
		BufferedReader BR = new BufferedReader(new InputStreamReader(stream));

		try
		{
			try
			{
				//入力ストリームからの文字列を全て連結
				String line = null;

				while ((line = BR.readLine()) != null)
				{
					SB.append(line).append("\n");
				}
			}
			finally
			{
				//必ずクローズする
				BR.close();
			}
		}
		catch (IOException e)
		{
			throw new Exception("結果の読み込みに失敗しました", e);
		}

		return SB.toString();
	}

}
