/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/23      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * コンテンツリストクラスです。<br>
 *
 * @version 1.0.0
 */
public class ContentsList {

	/** ファイルユーティリティ */
	private FileUtil fileUtil = new FileUtil();

	/** ユーザのファイルリスト */
	private List<String> contentsFileList = new ArrayList<String>();

	/**
	 * コンストラクタ<br>
	 * @throws Exception
	*/
	public ContentsList() throws Exception {
	}

	/**
	 * コンテンツファイルリスト取得<br>
	 * @param path パス
	 * @throws IOException
	 */
	public void getContentsFileList(String path) throws IOException {
		List<Path> fileList = fileUtil.getFileList(Paths.get(path));
		for (int i = 0; i < fileList.size(); i++) {
			if (Files.isDirectory(fileList.get(i))) {
				// ディレクトリのとき
				getContentsFileList(fileList.get(i).toAbsolutePath().toString()); // 再帰呼び出し
			}else if (Files.exists(fileList.get(i))) {
				// ファイルのとき
				contentsFileList.add(fileList.get(i).toAbsolutePath().toString());
			}else {
				// 存在しない場合
				contentsFileList.add(fileList.get(i).toAbsolutePath().toString() + "は削除されています:");
			}
		}
	}

	/**
	 * コンテンツファイルリストに追加<br>
	 * @param path パス
	 * @throws IOException
	 */
	public void addContentsFileList(String path) throws IOException {
		Path file = Paths.get(path);
		if (Files.isDirectory(file)) {
			// ディレクトリのとき
			getContentsFileList(file.toAbsolutePath().toString()); // コンテンツファイルリスト取得
		}else if (Files.exists(file)) {
			// ファイルのとき
			contentsFileList.add(file.toAbsolutePath().toString());
		}else {
			// 存在しない場合
			contentsFileList.add(file.toAbsolutePath().toString() + "は削除されています:");
		}
	}

	/**
	 * ファイルリスト取得<br>
	 * @return ファイルリスト
	*/
	public List<String> getFileList() {
		return contentsFileList;
	}
}
