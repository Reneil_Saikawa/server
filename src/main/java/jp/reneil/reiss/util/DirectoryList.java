/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.model.Directory;
import jp.reneil.reiss.model.LibraryModel;
import jp.reneil.reiss.model.Node;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ディレクトリリストクラスです。<br>
 *
 * @version 1.0.0
 */
public class DirectoryList {

	private Node root = null;

	private List<LibraryModel> _libNoList = new ArrayList<LibraryModel>();

	private String _tenantPath;

	/**
	 * コンストラクタ<br>
	 * @param libraryList 表示順ライブラリリスト
	 * @param tenantPath テナントパス
	 * @param path ルートパス
	 * @throws Exception
	*/
	public DirectoryList(List<LibraryModel> libNoList, String tenantPath, String path) throws Exception {

		_libNoList.addAll(libNoList);
		_tenantPath = tenantPath;

		Path obj = Paths.get(path);
		Node rootNode = getNode(obj);

		if (Files.isDirectory(obj)) {

			Directory dir = new Directory(obj.getFileName().toString());

			// 要素を作成
			List<Node> nodes = new ArrayList<Node>();
			nodes.add(rootNode);
			dir.setNodes(nodes);

			root = dir;
		}

	}

	/**
	 * ディレクトリリスト取得<br>
	 * @param obj ディレクトリ
	 * @return ディレクトリリスト
	 * @throws Exception
	*/
	public Node getNode(Path obj) throws Exception {

		Node node = null;

		if (Files.isDirectory(obj)) {

			Directory dir = new Directory(obj.getFileName().toString());

			// 表示順を設定
			if (_libNoList.size() > 0) {
				for(int i=0; i < _libNoList.size(); i++) {
					if (_libNoList.get(i).getLib_path().equals(obj.toAbsolutePath().toString().replace(_tenantPath + CommonConst.YEN_MRK, ""))) {
						dir.setDisNo(_libNoList.get(i).getDisplay_no());
						break;
					}
				}
			}

			// ディレクトリは暗号化
			TripleDesConverter tripleDesConverter = new TripleDesConverter();
			dir.setDirId(tripleDesConverter.encrypt64(obj.toAbsolutePath().toString()));

			// 子要素を作成
			List<Node> nodes = new ArrayList<Node>();

			// ディレクトリ内の一覧を取得
			FileUtil fileUtil = new FileUtil();
			List<Path> childList = fileUtil.getFileList(obj);

			for (Path child : childList) {
				if (Files.isDirectory(child)) {
					// ディレクトリのとき再帰呼び出し
					nodes.add(getNode(child));
				}
			}
			dir.setNodes(nodes);

			node = dir;
		}

		return node;
	}

	/**
	 * ルートパス取得<br>
	 * @return ルートパス
	*/
	public Node getRoot() {
		return root;
	}
}
