/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.util;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.LinearGradientPaint;
import java.awt.RenderingHints;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.imageio.ImageIO;

import jp.reneil.reiss.common.CommonConst;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * Jpeg画像から一覧用サムネイルの作成クラスです。<br>
 *
 * @version 1.0.0
 */
public class ListThumbnail {

	/** 入力ファイル */
	private String          IFN;

	/** 出力ファイル */
	private String          OFN;

	/** ファイル名（拡張子なし） */
	private String          WFN;

	/** 拡張子 */
	private String          EXT;

	/** エラー内容 */
	private Exception       er;

	/** 入力ファイルバッファ */
	private BufferedImage   IMG;

	/** 出力(シュリンク)ファイルバッファ */
	private BufferedImage   SLK;

	/** 出力ファイルグラフィック */
	private Graphics2D      G2D;

	/**
	 * デフォルトコンストラクタ（入出力ファイル名を完全なファイル名で指定）<br>
	 * @param InpFile パス付ファイル名
	 * @param OutFile パス付ファイル名
	 * @throws Exception
	 */
	public ListThumbnail(String InpFile,String OutFile, String Extension) throws Exception {
		if ( InpFile == null
		||   InpFile.equals("")
		||   OutFile == null
		||   OutFile.equals("") ) {
			er = new Exception("入出力のファイル名が指定されていません");
			throw er;
		}else {
			IFN = InpFile;
			OFN = OutFile;
			EXT = Extension;
		}
	}

	/**
	 * コンストラクタ （出力ファイルは入力ファイルと同一フォルダに同一名＋_.jpgで作成）<br>
	 * @param InpFile パス付ファイル名
	 * @throws Exception
	 */
	public ListThumbnail(String InpFile) throws Exception {
		if ( InpFile == null
		||   InpFile.equals("") ) {
			er = new Exception("入出力のファイル名が指定されていません");
			throw er;
		}else {
			//拡張子を削除してファイル名を「xxxx.jpg」に変更
	        if ( InpFile.lastIndexOf(CommonConst.DOT) > 0 ) {
	        	//ファイル名（拡張子なし）と拡張子に分離
	        	WFN = InpFile.substring(0, InpFile.lastIndexOf(CommonConst.DOT));
	        	EXT = InpFile.substring(InpFile.lastIndexOf(CommonConst.DOT), InpFile.length() );
			}else {
	        	WFN = InpFile;
	        }

			IFN = WFN + CommonConst.JPG;

			//拡張子を削除してファイル名を「xxxx_.jpg」に変更
	        if ( InpFile.lastIndexOf(CommonConst.DOT) > 0 ) {
	        	WFN = InpFile.substring(0, InpFile.lastIndexOf(CommonConst.DOT));
			}else {
	        	WFN = InpFile;
	        }

			OFN = WFN + CommonConst.THJPG;
		}
	}

	/**
	 * 画像のサムネイル作成（シュリンク）<br>
	 * @throws Exception
	 */
	public void ShrinkJpeg() throws Exception {
		//ShrinkJpeg(160,120);
		ShrinkJpeg(400,300);
	}

	/**
	 * 画像のサムネイル作成（シュリンク）（シュリンクサイズ指定あり）<br>
	 * @param Width 横サイズ
	 * @param Height 縦サイズ
	 * @throws Exception
	 */
	public void ShrinkJpeg(int Width,int Height) throws Exception {
		Path     inpFile;
		Point2D  start;
		Point2D  end;
		Integer  X0;
		Integer  X1;
		Integer  X2;
		Integer  Y2;
		Integer  H1;
		Integer  W1;
		int[]    X      = new int[3];
		int[]    Y      = new int[3];

		inpFile = Paths.get(IFN);

		if (Files.exists(inpFile)) {
			//入力ファイル
			IMG = ImageIO.read(inpFile.toFile());

			SLK = new BufferedImage(Width,Height, IMG.getType());

			G2D = SLK.createGraphics();

			//属性設定
			G2D.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION,RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
			G2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING       ,RenderingHints.VALUE_ANTIALIAS_ON               );
			G2D.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING    ,RenderingHints.VALUE_COLOR_RENDER_QUALITY       );
			G2D.setRenderingHint(RenderingHints.KEY_DITHERING          ,RenderingHints.VALUE_DITHER_ENABLE              );
			G2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING  ,RenderingHints.VALUE_TEXT_ANTIALIAS_ON          );
			G2D.setRenderingHint(RenderingHints.KEY_RENDERING          ,RenderingHints.VALUE_RENDER_QUALITY             );
			G2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION      ,RenderingHints.VALUE_INTERPOLATION_BILINEAR     );
			G2D.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS  ,RenderingHints.VALUE_FRACTIONALMETRICS_ON       );
			G2D.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL     ,RenderingHints.VALUE_STROKE_NORMALIZE           );

			//画像（サムネイル）読み込み
			G2D.drawImage(IMG, 0, 0, Width, Height, null);

			if ( CommonConst.MP4.equals(EXT) || CommonConst.WEBM.equals(EXT)) {
				//ファイルが動画の場合、再生マークの表示

				//外枠（丸枠）の描画設定
				X0 = (int)(Width * 0.3125);
				X1 = (int)(Width * 0.15 / 2);
				H1 = (Height - X0) / 2;
				W1 = (Width  - X0) / 2;

				//外枠（丸枠）の描画
				G2D.setPaint(Color.black);
				G2D.drawArc(W1, H1, X0, X0, 0, 360);

				//外枠（丸枠）の塗りつぶし設定
				start          = new Point2D.Float(Width / 2, H1);
				end            = new Point2D.Float(Width / 2, H1 + X0);
				float[] dist   = {0.0f, 0.5f, 1.0f};
				Color[] colors = {Color.white, new Color(91,153,255), new Color(205,253,255)};

				//外枠（丸枠）の塗りつぶし
			    G2D.setPaint(new LinearGradientPaint(start, end, dist, colors));
				G2D.fillArc(W1+1, H1+1, X0-1, X0-1, 0, 360);

				//再生マーク（三角形）の描画設定
				X2 = Width  / 2;
				Y2 = Height / 2;

				X[0] = X2 - (X1 / 2);
				Y[0] = (int)(Y2 + (Math.sqrt(3.0D) * X1 / 2));
				X[1] = X2 - (X1 / 2);
				Y[1] = (int)(Y2 - (Math.sqrt(3.0D) * X1 / 2));
				X[2] = X2 + X1;
				Y[2] = Y2;

				//再生マーク（三角形）の描画
				G2D.setPaint(new Color(128,128,128));
				G2D.drawPolygon(X, Y, 3);
				G2D.setPaint(new Color(0,0,0));

				//再生マーク（三角形）の塗りつぶし設定
				X[0] = X[0] + 1;
				Y[0] = Y[0] + 1;
				X[1] = X[1] + 1;
				Y[1] = Y[1] - 1;
				X[2] = X[2] - 1;

				//再生マーク（三角形）の塗りつぶし
				G2D.setPaint(Color.white);
				G2D.fillPolygon(X, Y, 3);
			}

			//サムネイルの保存
			ImageIO.write(SLK, "jpg", Files.newOutputStream(Paths.get(OFN)));
		}
	}
}
