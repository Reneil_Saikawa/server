/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.util;

import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.imageio.ImageIO;

import jp.reneil.reiss.common.CommonConst;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ファイルユーティリティークラスです。<br>
 *
 * @version 1.0.0
 */
public class FileUtil {
	/**
	 * ディレクト内のリストを取得します.<br>
	 * @param dir ディレクトリ
	 * @return　ディレクト内の要素リスト
	 * @throws IOException
	 */
	public List<Path> getFileList(Path dir) throws IOException
	{
		List<Path> childList = new ArrayList<>();

		try(DirectoryStream<Path> dirStream = Files.newDirectoryStream(dir)) {	// streamのclose用try-with-resources構文
			for(Path p : dirStream) {
				childList.add(p);
			}
		}catch (Exception e) {
			// ディレクトリストリーム取得失敗(例外)
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			String str = sw.toString();
			System.out.println("@@FileUtil.getFileList ディレクトリストリーム取得失敗ファイル(例外) = " + dir.toAbsolutePath().toString() + "\n" + str);
		}

		Collections.sort(childList, new Comparator<Path>() {
			public int compare(Path file1, Path file2){
				return file1.getFileName().toString().compareTo(file2.getFileName().toString());    //コンテンツ昇順
				//return file2.getFileName().toString().compareTo(file1.getFileName().toString());    //コンテンツ降順
			}
		});
		return childList;
	}

	/**
	 * ファイル・フォルダの存在確認を行います．<br>
	 * @param path パス
	 * @return 存在する：true / 存在しない：false
	 */
	public boolean existsDir(String path) {
		Path dir = Paths.get(path);
		return Files.exists(dir) ? true : false;
	}

	/**
	 * フォルダを作成します．<br>
	 * @param _path パス
	 * @return 存在する：true / 存在しない：false
	 */
	public boolean mkDir(String path, String baseDir) {
		StringBuilder sb = new StringBuilder(baseDir);
		sb.append(CommonConst.YEN_MRK);
		sb.append(path);

		String fullPath = sb.toString();

		Path dir = Paths.get(fullPath);

		// フォルダが存在しなかったら作成
		if (false == Files.exists(dir)) {
			try {
				Files.createDirectories(dir);
				if (false == Files.exists(dir)) {
					return false;
				}
			} catch (Exception _e) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 拡張子のチェックを行います．<br>
	 * 指定した拡張子以外は異常．<br>
	 * @param path パス
	 * @param fileExt 拡張子
	 * @return 正常：true / 異常：false
	 */
	public boolean chkFileExt(String path, String fileExt) {
		String ext = getSuffix(path);
		if (null == ext) {
			return false;
		}
		return fileExt.equals(ext.toLowerCase());
	}

	/**
	 * 拡張子のチェックを行います．<br>
	 * 指定した拡張子以外は異常．<br>
	 * @param path パス
	 * @param fileExt1 拡張子
	 * @param fileExt2 拡張子
	 * @return 正常：true / 異常：false
	 */
	public boolean chkFileExt(String path, String fileExt1, String fileExt2) {
		String ext = getSuffix(path);
		if (null == ext) {
			return false;
		}
		if (fileExt1.equals(ext.toLowerCase())) {
			return true;
		}
		if (fileExt2.equals(ext.toLowerCase())) {
			return true;
		}
		return false;
	}

	/**
	 * サムネイルファイル拡張子のチェックを行います．<br>
	 * 指定した拡張子以外は異常．<br>
	 * @param path パス
	 * @param fileExt 拡張子
	 * @return 正常：true / 異常：false
	 */
	public boolean chkTHFileExt(String path, String fileExt) {
		return path.contains(fileExt);
	}

	/**
	 * ファイル名の拡張子を返します．<br>
	 * @param _fileName ファイル名
	 * @return ファイルの拡張子
	 */
	public String getSuffix(String fileName) {
		if (null == fileName) {
			return null;
		}
		int dot = fileName.lastIndexOf(".");
		if (-1 == dot) {
			return null;
		}
		return fileName.substring(dot + 1).toUpperCase();
	}

	/**
	 * ファイル名から拡張子を取り除いた名前を返します.<br>
	 * @param fileName ファイル名
	 * @return ファイル名
	 */
	public String getPreffix(String fileName) {
		if (fileName == null) {
			return null;
		}
		int dot = fileName.lastIndexOf(".");
		if (dot != -1) {
			return fileName.substring(0, dot);
		}
		return fileName;
	}

	/**
	 * ファイル作成日時を返します.<br>
	 * @param file ファイル
	 * @return 作成日時
	 */
	public String getFileTimestamp(Path file) {
		BasicFileAttributes attrs;
		String DateTime = "";
		try {
		    attrs = Files.readAttributes(file, BasicFileAttributes.class);
		    FileTime time = attrs.creationTime();

		    String pattern = "yyyyMMddHHmmssSSS";
		    SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		    DateTime = simpleDateFormat.format(new Date(time.toMillis()));
		}catch (IOException e) {
			// ファイル作成日時取得失敗(例外)
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			String str = sw.toString();
			System.out.println("@@FileUtil.getFileTimestamp ファイル作成日時取得失敗(例外)\n" + str);
		}
		return DateTime;
	}

	/**
	* JPEG画像データの縦横サイズ取得.
	* @param file JPEGファイル.
	* @return ファイルサイズ. 幅，高さの順のintの配列.
	* @throws IOException
	*/
	public Dimension getSize(Path file) throws Exception {

		int width = 0;
		int height = 0;
		BufferedImage img = null;

		try {
			img = ImageIO.read(file.toFile());
			width = img.getWidth();
			height = img.getHeight();
		}catch (Exception ex) {
			width = 640;
			height = 480;
		}

		return new Dimension(width, height);
	}

	/**
	 * ファイルサイズ別単位付き変換<br>
	 * @param size ファイルサイズ(byte)
	 * @return 単位付きファイルサイズ
	 */
	public String getFileSizeStr(long size) {

		String units[] = {" Byte", " KB", " MB", " GB", " TB"};
		int i = 0;

		for(; size > 1024; i++) {
			size = size / 1024;
		}

		return String.format("%,d",Math.round(size)) + units[i];
	}

}
