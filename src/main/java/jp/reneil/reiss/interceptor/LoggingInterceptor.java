/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.interceptor;

import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import jp.reneil.reiss.common.SessionConst;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * メソッドIN/OUTログ出力用インターセプタークラスです。<br>
 *
 * @version 1.0.0
 */
@Aspect
@Component
public class LoggingInterceptor {

	private static Log log = LogFactory.getLog(LoggingInterceptor.class);

	/**
	 * メソッド呼出前処理を行います。<br>
	 * 対象メソッドは、パッケージ[jp.reneil.medical.questionnaire.controller]配下にあるクラスの public メソッドです。
	 * @param jp クラスオブジェクト受渡用インターフェース
	 */
	@Before("execution(public * jp.reneil.reiss.controller..*.*(..))")
	public void before(JoinPoint jp) {
		// 対象メソッドを含んでいるクラスのオブジェクトを取得
		Class<? extends Object> targetClass = jp.getTarget().getClass();
		// メソッド名を取得
		String methodName = jp.getSignature().getName();
		// セッションを取得
		HttpSession session = getSession();

		// セッションからユーザIDを取得
		String userId = (String)session.getAttribute(SessionConst.USER_ID);

		// STARTログ出力
		log.info("◆START ["+targetClass.getSimpleName() + "#" + methodName+"] USER_ID=" + userId);
	}

	/**
	 * メソッド呼出後処理を行います。<br>
	 * 対象メソッドは、パッケージ[jp.reneil.medical.questionnaire.controller]配下にあるクラスの public メソッドです。
	 * @param jp クラスオブジェクト受渡用インターフェース
	 */
	@After("execution(public * jp.reneil.reiss.controller..*.*(..))")
	public void after(JoinPoint jp) {
		// 対象メソッドを含んでいるクラスのオブジェクトを取得
		Class<? extends Object> targetClass = jp.getTarget().getClass();
		// メソッド名を取得
		String methodName = jp.getSignature().getName();
		// セッションを取得
		HttpSession session = getSession();

		// セッションからユーザIDを取得
		String userId = (String)session.getAttribute(SessionConst.USER_ID);

		// ENDログ出力
		log.info("◆END   ["+targetClass.getSimpleName() + "#" + methodName+"] USER_ID=" + userId);
	}

	/**
	 * セッションを取得します。
	 * @return HttpSession
	 */
	private HttpSession getSession() {
		HttpSession session = (HttpSession) RequestContextHolder
				.currentRequestAttributes()
				.resolveReference(RequestAttributes.REFERENCE_SESSION);
		return session;
	}
}
