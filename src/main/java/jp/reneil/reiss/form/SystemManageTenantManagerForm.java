/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/24      1.0.1           女屋            パスワードエラーカウント追加
 */
package jp.reneil.reiss.form;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * システム管理_テナント管理者_画面クラスです。<br>
 *
 * @version 1.0.1
 */
public class SystemManageTenantManagerForm {

	/** 暗号化ユーザーID */
	private String acc_userID;

	/** テナント管理者ID */
	private String user_id;

	/** パスワード */
	private String password;

	/** パスワードエラーカウント */
	private int pwd_errcnt;

	/** テナント管理者名 */
	private String user_name;

	/** テナントID */
	private String tenant_id;

	/** 有効開始日 */
	private String start_date;

	/** 有効終了日 */
	private String end_date;

	/** 削除フラグ */
	private boolean del_flg;

	public String getAcc_userID() {
		return acc_userID;
	}

	public void setAcc_userID(String acc_userID) {
		this.acc_userID = acc_userID;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getPwd_errcnt() {
		return pwd_errcnt;
	}

	public void setPwd_errcnt(int pwd_errcnt) {
		this.pwd_errcnt = pwd_errcnt;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public boolean getDel_flg() {
		return del_flg;
	}

	public void setDel_flg(boolean del_flg) {
		this.del_flg = del_flg;
	}

}