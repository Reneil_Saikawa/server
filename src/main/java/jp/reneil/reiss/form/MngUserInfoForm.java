/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/24      1.0.1           女屋            パスワードエラーカウント追加
 */
package jp.reneil.reiss.form;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザ管理_ユーザー_画面クラスです。<br>
 *
 * @version 1.0.1
 */
public class MngUserInfoForm {

	/** 暗号化ユーザーID */
	private String acc_userID;

	/** テナント有効開始日 */
	private String tenantStart;

	/** テナント有効終了日 */
	private String tenantEnd;

	/** ユーザーID */
	private String user_id;

	/** パスワード */
	private String password;

	/** パスワードエラーカウント */
	private int pwd_errcnt;

	/** ユーザー名 */
	private String user_name;

	/** ライブラリ */
	private String[] lib_id;

	/** 管理権限 */
	private boolean manage_auth;

	/** 帳票出力権限 */
	private boolean output_auth;

	/** 付帯情報編集権限 */
	private boolean edit_auth;

	/** ダウンロード権限 */
	private boolean download_auth;

	/** 移動権限 */
	private boolean move_auth;

	/** 削除権限 */
	private boolean delete_auth;

	/** 帳票権限 */
	private String[] report_auth;

	/** 有効開始日 */
	private String start_date;

	/** 有効終了日 */
	private String end_date;

	/** 削除フラグ */
	private boolean del_flg;

	public String getAcc_userID() {
		return acc_userID;
	}

	public void setAcc_userID(String acc_userID) {
		this.acc_userID = acc_userID;
	}

	public String getTenantStart() {
		return tenantStart;
	}

	public void setTenantStart(String tenantStart) {
		this.tenantStart = tenantStart;
	}

	public String getTenantEnd() {
		return tenantEnd;
	}

	public void setTenantEnd(String tenantEnd) {
		this.tenantEnd = tenantEnd;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getPwd_errcnt() {
		return pwd_errcnt;
	}

	public void setPwd_errcnt(int pwd_errcnt) {
		this.pwd_errcnt = pwd_errcnt;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String[] getLib_id() {
		return lib_id;
	}

	public void setLib_id(String[] lib_id) {
		this.lib_id = lib_id;
	}

	public boolean getManage_auth() {
		return manage_auth;
	}

	public void setManage_auth(boolean manage_auth) {
		this.manage_auth = manage_auth;
	}

	public boolean getOutput_auth() {
		return output_auth;
	}

	public void setOutput_auth(boolean output_auth) {
		this.output_auth = output_auth;
	}

	public boolean getEdit_auth() {
		return edit_auth;
	}

	public void setEdit_auth(boolean edit_auth) {
		this.edit_auth = edit_auth;
	}

	public boolean getDownload_auth() {
		return download_auth;
	}

	public void setDownload_auth(boolean download_auth) {
		this.download_auth = download_auth;
	}

	public boolean getMove_auth() {
		return move_auth;
	}

	public void setMove_auth(boolean move_auth) {
		this.move_auth = move_auth;
	}

	public boolean getDelete_auth() {
		return delete_auth;
	}

	public void setDelete_auth(boolean delete_auth) {
		this.delete_auth = delete_auth;
	}

	public String[] getReport_auth() {
		return report_auth;
	}

	public void setReport_auth(String[] report_auth) {
		this.report_auth = report_auth;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public boolean getDel_flg() {
		return del_flg;
	}

	public void setDel_flg(boolean del_flg) {
		this.del_flg = del_flg;
	}

}