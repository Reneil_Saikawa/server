/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.form;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザ管理_付帯情報項目名_画面クラスです。<br>
 *
 * @version 1.0.0
 */
public class MngAdditionalItemForm {

	/** 暗号化ユーザーID */
	private String acc_userID;

	/** テナント有効開始日 */
	private String tenantStart;

	/** テナント有効終了日 */
	private String tenantEnd;

	/** 項目ID */
	private String item_id;

	/** 項目名 */
	private String item_name;

	/** 表示順 */
	private String display_no;

	/** 有効開始日 */
	private String start_date;

	/** 有効終了日 */
	private String end_date;

	/** 削除フラグ */
	private boolean del_flg;

	public String getAcc_userID() {
		return acc_userID;
	}

	public void setAcc_userID(String acc_userID) {
		this.acc_userID = acc_userID;
	}

	public String getTenantStart() {
		return tenantStart;
	}

	public void setTenantStart(String tenantStart) {
		this.tenantStart = tenantStart;
	}

	public String getTenantEnd() {
		return tenantEnd;
	}

	public void setTenantEnd(String tenantEnd) {
		this.tenantEnd = tenantEnd;
	}

	public String getItem_id() {
		return item_id;
	}

	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}

	public String getItem_name() {
		return item_name;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public String getDisplay_no() {
		return display_no;
	}

	public void setDisplay_no(String display_no) {
		this.display_no = display_no;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public boolean getDel_flg() {
		return del_flg;
	}

	public void setDel_flg(boolean del_flg) {
		this.del_flg = del_flg;
	}
}