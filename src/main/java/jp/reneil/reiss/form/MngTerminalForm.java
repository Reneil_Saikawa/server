/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.form;

import java.util.Date;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザ管理_端末_画面クラスです。<br>
 *
 * @version 1.0.0
 */
public class MngTerminalForm {

	/** 暗号化ユーザーID */
	private String acc_userID;

	/** テナント有効開始日 */
	private String tenantStart;

	/** テナント有効終了日 */
	private String tenantEnd;

	/** 端末ID */
	private String term_id;

	/** 端末区分 */
	private String term_ctg;

	/** 端末名 */
	private String term_name;

	/** ライブラリID */
	private String lib_id;

	/** 撮影年フォルダ作成フラグ */
	private boolean year_folder_flg;

	/** 撮影月フォルダ作成フラグ */
	private boolean month_folder_flg;

	/** 撮影日フォルダ作成フラグ */
	private boolean day_folder_flg;

	/** 日本語表記フラグ */
	private boolean japanese_flg;

	/** 動画撮影最大時間 */
	private short m_max_time;

	/** ライブ撮影サイズ（横） */
	private short live_width;

	/** ライブ最小フレームレート */
	private short live_min_fps;

	/** ライブ最大フレームレート */
	private short live_max_fps;

	/** 有効開始日 */
	private String start_date;

	/** 有効終了日 */
	private String end_date;

	/** 削除フラグ */
	private boolean del_flg;

	/** 更新ユーザーID */
	private String update_user_id;

	/** 更新日 */
	private Date update_date;

	public void setAcc_userID(String acc_userID) {
		this.acc_userID = acc_userID;
	}

	public String getAcc_userID() {
		return acc_userID;
	}

	public String getTenantStart() {
		return tenantStart;
	}

	public void setTenantStart(String tenantStart) {
		this.tenantStart = tenantStart;
	}

	public String getTenantEnd() {
		return tenantEnd;
	}

	public void setTenantEnd(String tenantEnd) {
		this.tenantEnd = tenantEnd;
	}

	public String getTerm_id() {
		return term_id;
	}

	public void setTerm_id(String term_id) {
		this.term_id = term_id;
	}

	public String getTerm_ctg() {
		return term_ctg;
	}

	public void setTerm_ctg(String term_ctg) {
		this.term_ctg = term_ctg;
	}

	public String getTerm_name() {
		return term_name;
	}

	public void setTerm_name(String term_name) {
		this.term_name = term_name;
	}

	public String getLib_id() {
		return lib_id;
	}

	public void setLib_id(String lib_id) {
		this.lib_id = lib_id;
	}

	public boolean getYear_folder_flg() {
		return year_folder_flg;
	}

	public void setYear_folder_flg(boolean year_folder_flg) {
		this.year_folder_flg = year_folder_flg;
	}

	public boolean getMonth_folder_flg() {
		return month_folder_flg;
	}

	public void setMonth_folder_flg(boolean month_folder_flg) {
		this.month_folder_flg = month_folder_flg;
	}

	public boolean getDay_folder_flg() {
		return day_folder_flg;
	}

	public void setDay_folder_flg(boolean day_folder_flg) {
		this.day_folder_flg = day_folder_flg;
	}

	public boolean getJapanese_flg() {
		return japanese_flg;
	}

	public void setJapanese_flg(boolean japanese_flg) {
		this.japanese_flg = japanese_flg;
	}

	public short getM_max_time() {
		return m_max_time;
	}

	public void setM_max_time(short m_max_time) {
		this.m_max_time = m_max_time;
	}

	public short getLive_width() {
		return live_width;
	}

	public void setLive_width(short live_width) {
		this.live_width = live_width;
	}

	public short getLive_min_fps() {
		return live_min_fps;
	}

	public void setLive_min_fps(short live_min_fps) {
		this.live_min_fps = live_min_fps;
	}

	public short getLive_max_fps() {
		return live_max_fps;
	}

	public void setLive_max_fps(short live_max_fps) {
		this.live_max_fps = live_max_fps;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public boolean getDel_flg() {
		return del_flg;
	}

	public void setDel_flg(boolean del_flg) {
		this.del_flg = del_flg;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

}