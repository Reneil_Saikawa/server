/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.form;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザ管理_帳票_画面クラスです。<br>
 *
 * @version 1.0.0
 */
public class MngReportParamForm {

	/** 暗号化ユーザーID */
	private String acc_userID;

	/** テナント有効開始日 */
	private String tenantStart;

	/** テナント有効終了日 */
	private String tenantEnd;

	/** 帳票ID */
	private String report_id;

	/** 帳票名 */
	private String report_name;

	/** テンプレートファイル名 */
	private String file_name;

	/** シート名 */
	private String sheet_name;

	/** 1ページ当たりの出力件数 */
	private short print_rows;

	/** 表紙出力フラグ */
	private boolean front_page_flg;

	/** 表紙用シート名 */
	private String front_page_sheet;

	/** 有効開始日 */
	private String start_date;

	/** 有効終了日 */
	private String end_date;

	/** 削除フラグ */
	private boolean del_flg;

	public String getAcc_userID() {
		return acc_userID;
	}

	public void setAcc_userID(String acc_userID) {
		this.acc_userID = acc_userID;
	}

	public String getTenantStart() {
		return tenantStart;
	}

	public void setTenantStart(String tenantStart) {
		this.tenantStart = tenantStart;
	}

	public String getTenantEnd() {
		return tenantEnd;
	}

	public void setTenantEnd(String tenantEnd) {
		this.tenantEnd = tenantEnd;
	}

	public String getReport_id() {
		return report_id;
	}

	public void setReport_id(String report_id) {
		this.report_id = report_id;
	}

	public String getReport_name() {
		return report_name;
	}

	public void setReport_name(String report_name) {
		this.report_name = report_name;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getSheet_name() {
		return sheet_name;
	}

	public void setSheet_name(String sheet_name) {
		this.sheet_name = sheet_name;
	}

	public short getPrint_rows() {
		return print_rows;
	}

	public void setPrint_rows(short print_rows) {
		this.print_rows = print_rows;
	}

	public boolean getFront_page_flg() {
		return front_page_flg;
	}

	public void setFront_page_flg(boolean front_page_flg) {
		this.front_page_flg = front_page_flg;
	}

	public String getFront_page_sheet() {
		return front_page_sheet;
	}

	public void setFront_page_sheet(String front_page_sheet) {
		this.front_page_sheet = front_page_sheet;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public boolean getDel_flg() {
		return del_flg;
	}

	public void setDel_flg(boolean del_flg) {
		this.del_flg = del_flg;
	}

}