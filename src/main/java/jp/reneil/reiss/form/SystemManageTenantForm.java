/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2020/02/14      1.0.1           女屋            地図有効化機能追加
 *    3    2020/02/20      1.0.2           女屋            帳票有効化機能追加
 */
package jp.reneil.reiss.form;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * システム管理_テナント_画面クラスです。<br>
 *
 * @version 1.0.2
 */
public class SystemManageTenantForm {

	/** 暗号化ユーザーID */
	private String acc_userID;

	/** テナントID */
	private String tenant_id;

	/** テナント名 */
	private String tenant_name;

	/** ユーザーライセンス数 */
	private short user_license;

	/** 端末ライセンス数 */
	private short term_license;

	/** 静止画撮影機能 */
	private boolean stillimage_function;

	/** 動画撮影機能 */
	private boolean movie_function;

	/** ライブ撮影機能 */
	private boolean live_function;

	/** 起動時一時ファイル削除 */
	private boolean start_tmp_del;

	/** 一時ファイル時間経過削除 */
	private short tmp_del_time;

	/** アプリ自動停止時間 */
	private short stop_time;

	/** パスワードエラーロック回数 */
	private short password_lock_times;

	/** サムネイルサイズ（横） */
	private short thumbnail_width;

	/** サムネイルサイズ（縦） */
	private short thumbnail_height;

	/** パトライト有効化 */
	private boolean patlite_enable;

	/** パトライトアドレス */
	private String patLite_ip;

	/** パトライトログイン名 */
	private String patLite_login_name;

	/** パトライト受信時オプション */
	private String patLite_option_rcv;

	/** パトライト受信時鳴動時間 */
	private short patLite_time_rcv;

	/** パトライト警告時オプション */
	private String patLite_option_warm;

	/** パトライト警告時鳴動時間 */
	private short patLite_time_warm;

	/** パトライトエラー時オプション */
	private String patLite_option_err;

	/** パトライトエラー時鳴動時間 */
	private short patLite_time_err;

	/** ライブ接続先 */
	private String live_server;

	/** ライブアプリ名 */
	private String live_app;

	/** ライブルーム名 */
	private String live_room;

	/** 地図有効化 */
	private boolean map_enable;

	/** 地図サーバ */
	private String map_server;

	/** 帳票有効化 */
	private boolean report_enable;

	/** ファイルパス表示 */
	private boolean display_file_path;

	/** 有効開始日 */
	private String start_date;

	/** 有効終了日 */
	private String end_date;

	/** 削除フラグ */
	private boolean del_flg;

	public String getAcc_userID() {
		return acc_userID;
	}

	public void setAcc_userID(String acc_userID) {
		this.acc_userID = acc_userID;
	}

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String getTenant_name() {
		return tenant_name;
	}

	public void setTenant_name(String tenant_name) {
		this.tenant_name = tenant_name;
	}

	/**
	 * @return user_license
	 */
	public short getUser_license() {
		return user_license;
	}

	/**
	 * @param user_license セットする user_license
	 */
	public void setUser_license(short user_license) {
		this.user_license = user_license;
	}

	/**
	 * @return term_license
	 */
	public short getTerm_license() {
		return term_license;
	}

	/**
	 * @param term_license セットする term_license
	 */
	public void setTerm_license(short term_license) {
		this.term_license = term_license;
	}

	public boolean getStillimage_function() {
		return stillimage_function;
	}

	public void setStillimage_function(boolean stillimage_function) {
		this.stillimage_function = stillimage_function;
	}

	public boolean getMovie_function() {
		return movie_function;
	}

	public void setMovie_function(boolean movie_function) {
		this.movie_function = movie_function;
	}

	public boolean getLive_function() {
		return live_function;
	}

	public void setLive_function(boolean live_function) {
		this.live_function = live_function;
	}

	public boolean getStart_tmp_del() {
		return start_tmp_del;
	}

	public void setStart_tmp_del(boolean start_tmp_del) {
		this.start_tmp_del = start_tmp_del;
	}

	public short getTmp_del_time() {
		return tmp_del_time;
	}

	public void setTmp_del_time(short tmp_del_time) {
		this.tmp_del_time = tmp_del_time;
	}

	public short getStop_time() {
		return stop_time;
	}

	public void setStop_time(short stop_time) {
		this.stop_time = stop_time;
	}

	public short getPassword_lock_times() {
		return password_lock_times;
	}

	public void setPassword_lock_times(short password_lock_times) {
		this.password_lock_times = password_lock_times;
	}

	public short getThumbnail_width() {
		return thumbnail_width;
	}

	public void setThumbnail_width(short thumbnail_width) {
		this.thumbnail_width = thumbnail_width;
	}

	public short getThumbnail_height() {
		return thumbnail_height;
	}

	public void setThumbnail_height(short thumbnail_height) {
		this.thumbnail_height = thumbnail_height;
	}

	public boolean getPatlite_enable() {
		return patlite_enable;
	}

	public void setPatlite_enable(boolean patlite_enable) {
		this.patlite_enable = patlite_enable;
	}

	public String getPatLite_ip() {
		return patLite_ip;
	}

	public void setPatLite_ip(String patLite_ip) {
		this.patLite_ip = patLite_ip;
	}

	public String getPatLite_login_name() {
		return patLite_login_name;
	}

	public void setPatLite_login_name(String patLite_login_name) {
		this.patLite_login_name = patLite_login_name;
	}

	public String getPatLite_option_rcv() {
		return patLite_option_rcv;
	}

	public void setPatLite_option_rcv(String patLite_option_rcv) {
		this.patLite_option_rcv = patLite_option_rcv;
	}

	public short getPatLite_time_rcv() {
		return patLite_time_rcv;
	}

	public void setPatLite_time_rcv(short patLite_time_rcv) {
		this.patLite_time_rcv = patLite_time_rcv;
	}

	public String getPatLite_option_warm() {
		return patLite_option_warm;
	}

	public void setPatLite_option_warm(String patLite_option_warm) {
		this.patLite_option_warm = patLite_option_warm;
	}

	public short getPatLite_time_warm() {
		return patLite_time_warm;
	}

	public void setPatLite_time_warm(short patLite_time_warm) {
		this.patLite_time_warm = patLite_time_warm;
	}

	public String getPatLite_option_err() {
		return patLite_option_err;
	}

	public void setPatLite_option_err(String patLite_option_err) {
		this.patLite_option_err = patLite_option_err;
	}

	public short getPatLite_time_err() {
		return patLite_time_err;
	}

	public void setPatLite_time_err(short patLite_time_err) {
		this.patLite_time_err = patLite_time_err;
	}

	public String getLive_server() {
		return live_server;
	}

	public void setLive_server(String live_server) {
		this.live_server = live_server;
	}

	public String getLive_app() {
		return live_app;
	}

	public void setLive_app(String live_app) {
		this.live_app = live_app;
	}

	public String getLive_room() {
		return live_room;
	}

	public void setLive_room(String live_room) {
		this.live_room = live_room;
	}

	public boolean getMap_enable() {
		return map_enable;
	}

	public void setMap_enable(boolean map_enable) {
		this.map_enable = map_enable;
	}

	public String getMap_server() {
		return map_server;
	}

	public void setMap_server(String map_server) {
		this.map_server = map_server;
	}

	public boolean getReport_enable() {
		return report_enable;
	}

	public void setReport_enable(boolean report_enable) {
		this.report_enable = report_enable;
	}

	public boolean getDisplay_file_path() {
		return display_file_path;
	}

	public void setDisplay_file_path(boolean display_file_path) {
		this.display_file_path = display_file_path;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public boolean getDel_flg() {
		return del_flg;
	}

	public void setDel_flg(boolean del_flg) {
		this.del_flg = del_flg;
	}

}