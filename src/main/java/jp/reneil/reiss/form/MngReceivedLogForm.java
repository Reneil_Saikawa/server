/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.form;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザ管理_受信ログ_画面クラスです。<br>
 *
 * @version 1.0.0
 */
public class MngReceivedLogForm {

	/** 暗号化ユーザーID */
	private String acc_userID;

	/** 開始日時 */
	private String start_date;

	/** 終了日時 */
	private String end_date;

	/** テナントID */
	private String tenant_id;

	/** 端末ID */
	private String term_id;

	/** 受信区分コード */
	private String receive_code;

	/** 結果コード */
	private String result_code;

	public String getAcc_userID() {
		return acc_userID;
	}

	public void setAcc_userID(String acc_userID) {
		this.acc_userID = acc_userID;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String getTerm_id() {
		return term_id;
	}

	public void setTerm_id(String term_id) {
		this.term_id = term_id;
	}

	public String getReceive_code() {
		return receive_code;
	}

	public void setReceive_code(String receive_code) {
		this.receive_code = receive_code;
	}

	public String getResult_code() {
		return result_code;
	}

	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}

}