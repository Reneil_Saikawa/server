/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.form;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * システム管理_システム情報_画面クラスです。<br>
 *
 * @version 1.0.0
 */
public class SystemManageSystemInfoForm {

	/** 暗号化ユーザーID */
	private String acc_userID;

	/** システム名 */
	private String system_name;

	/** ルートパス */
	private String rootPath;

	/** 内部URL */
	private String inUrl;

	/** 外部URL */
	private String outUrl;

	/** ディレクトリ画像 */
	private String directory_image;

	/** 画像ソース */
	private String data_src;

	public String getAcc_userID() {
		return acc_userID;
	}

	public void setAcc_userID(String acc_userID) {
		this.acc_userID = acc_userID;
	}

	public String getSystem_name() {
		return system_name;
	}

	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}

	public String getRootPath() {
		return rootPath;
	}

	public void setRootPath(String rootPath) {
		this.rootPath = rootPath;
	}

	public String getInUrl() {
		return inUrl;
	}

	public void setInUrl(String inUrl) {
		this.inUrl = inUrl;
	}

	public String getOutUrl() {
		return outUrl;
	}

	public void setOutUrl(String outUrl) {
		this.outUrl = outUrl;
	}

	public String getDirectory_image() {
		return directory_image;
	}

	public void setDirectory_image(String directory_image) {
		this.directory_image = directory_image;
	}

	public String getData_src() {
		return data_src;
	}

	public void setData_src(String data_src) {
		this.data_src = data_src;
	}

}