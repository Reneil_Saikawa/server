/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.form;

import java.util.Date;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザ管理_ライブラリ_画面クラスです。<br>
 *
 * @version 1.0.0
 */
public class MngLibraryForm {

	/** 暗号化ユーザーID */
	private String acc_userID;

	/** ユーザーID */
	private String user_id;

	/** パスワード */
	private String password;

	/** ライブラリID */
	private String lib_id;

	/** テナントID */
	private String tenant_id;

	/** 親ライブラリID */
	private String parent_lid;

	/** ライブラリ名 */
	private String lib_name;

	/** ライブルーム名 */
	private String live_room;

	/** 表示順 */
	private String display_no;

	/** 更新日 */
	private Date update_date;

    /** 親ライブラリパス */
	private String parent_path;

	public void setAcc_userID(String acc_userID) {
		this.acc_userID = acc_userID;
	}

	public String getAcc_userID() {
		return acc_userID;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public String getLib_id() {
		return lib_id;
	}

	public void setLib_id(String lib_id) {
		this.lib_id = lib_id;
	}

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String getParent_lid() {
		return parent_lid;
	}

	public void setParent_lid(String parent_lid) {
		this.parent_lid = parent_lid;
	}

	public String getLib_name() {
		return lib_name;
	}

	public void setLib_name(String lib_name) {
		this.lib_name = lib_name;
	}

	public String getLive_room() {
		return live_room;
	}

	public void setLive_room(String live_room) {
		this.live_room = live_room;
	}

	public String getDisplay_no() {
		return display_no;
	}

	public void setDisplay_no(String display_no) {
		this.display_no = display_no;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	public String getParent_path() {
		return parent_path;
	}

	public void setParent_path(String parent_path) {
		this.parent_path = parent_path;
	}

}