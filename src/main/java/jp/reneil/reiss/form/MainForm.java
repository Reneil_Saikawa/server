/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.form;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * メイン_画面クラスです。<br>
 *
 * @version 1.0.0
 */
public class MainForm {

	/** 暗号化ユーザーID */
	private String acc_userID;

	public String getAcc_userID() {
		return acc_userID;
	}

	public void setAcc_userID(String acc_userID) {
		this.acc_userID = acc_userID;
	}

}