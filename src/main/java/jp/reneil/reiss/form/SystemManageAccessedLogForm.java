/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.form;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * システム管理_アクセスログ_画面クラスです。<br>
 *
 * @version 1.0.0
 */
public class SystemManageAccessedLogForm {

	/** 暗号化ユーザーID */
	private String acc_userID;

	/** 開始日時 */
	private String start_date;

	/** 終了日時 */
	private String end_date;

	/** テナントID */
	private String tenant_id;

	/** ユーザーID */
	private String user_id;

	/** 実行プログラムコード */
	private String execution_code;

	/** 結果コード */
	private String result_code;

	public String getAcc_userID() {
		return acc_userID;
	}

	public void setAcc_userID(String acc_userID) {
		this.acc_userID = acc_userID;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getExecution_code() {
		return execution_code;
	}

	public void setExecution_code(String execution_code) {
		this.execution_code = execution_code;
	}

	public String getResult_code() {
		return result_code;
	}

	public void setResult_code(String result_code) {
		this.result_code = result_code;
	}

}