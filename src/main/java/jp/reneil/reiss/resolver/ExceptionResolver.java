/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.resolver;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.AccessLogDao;
import jp.reneil.reiss.dao.CodeDao;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.model.AccessLogModel;
import jp.reneil.reiss.model.CodeModel;
import jp.reneil.reiss.model.UserInfoModel;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 例外ハンドラクラスです。<br>
 * ControllerクラスやDaoクラス等で送出された例外はすべて、本クラスでキャッチされます。
 *
 * @version 1.0.0
 */
public class ExceptionResolver implements HandlerExceptionResolver {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(ExceptionResolver.class);

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** コードテーブルアクセス制御クラス */
	@Autowired
	private CodeDao codeDao;

	/** ユーザー情報テーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/** アクセスログテーブルアクセス制御クラス */
	@Autowired
	private AccessLogDao accessLogDao;

	/** エラーメッセージ表示項目名 */
	private static final String ID_MESSAGE = "message";

	/** システム管理URL */
	private static final String SM_URL ="/systemManage";

	/**
	 * 例外ハンドリングメソッドです。<br>
	 */
	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object object, Exception ex) {

		String url = request.getRequestURI();
		String targetClass = object.toString().split("\\(")[0].split("\\.")[object.toString().split("\\(")[0].split("\\.").length - 2];
		String methodName = object.toString().split("\\(")[0].split("\\.")[object.toString().split("\\(")[0].split("\\.").length - 1];
		String name_02 = targetClass + "#" + methodName;

		HttpSession session = request.getSession(false);
		// セッション情報からログインユーザIDを取得
		String user_id = null;
		if (session != null) {
			user_id = (String) session.getAttribute(SessionConst.USER_ID);
		}

		log.error("[userId=" + user_id + "]例外をキャッチしました。", ex);

		ModelAndView mav = new ModelAndView();

		// システム名
		mav.addObject("systemName", systemInfoDao.findAll().getSystem_name());

		if (ex instanceof Exception) {
			// JSPに表示するメッセージをセット
			mav.addObject(ID_MESSAGE, ex.getMessage());
		} else {
			// JSPに表示するメッセージをセット
			mav.addObject(ID_MESSAGE, MessageConst.E001);
		}

		if(url.contains(SM_URL)) {
			//システム管理操作の場合
			mav.addObject("loginUrl", SM_URL + "/login.html");
		}else {
			mav.addObject("loginUrl", "/login.html	");
		}

		// 遷移先のJSPを指定(error.jspに遷移)
		mav.setViewName("error");

		//画面ID存在確認・セッションタイムアウト時例外の判定
		if (codeDao.count(name_02) == 1 && !ex.getMessage().contains(MessageConst.W002)) {

			String tenant_id = "null";

			if (user_id == null) {
				user_id = "null";
			}else {
				if (userInfoDao.findUser(user_id) == 1) {
					// ユーザー情報を取得
					UserInfoModel userinfoModel = userInfoDao.getUser(user_id);
					tenant_id = userinfoModel.getTenant_id();
				}
			}

			CodeModel codeModel = codeDao.getName02Data(name_02);

			//スタックトレースをStringに変換
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			ex.printStackTrace(pw);
			pw.flush();

			//アクセスログ準備
			AccessLogModel accessLogModel = new AccessLogModel();
			accessLogModel.setAccess_date(new Date());
			accessLogModel.setAccess_ip(request.getRemoteAddr());
			accessLogModel.setAccess_tenant_id(tenant_id);
			accessLogModel.setAccess_user_id(user_id);
			accessLogModel.setPrg_id(codeModel.getCode_01() + codeModel.getCode_02());
			accessLogModel.setResult_id(CommonConst.FAILED_CO);
			accessLogModel.setContents("例外エラーが発生しました。");
			accessLogModel.setErr_msg(sw.toString());

			//アクセスログ登録
			accessLogDao.insert(accessLogModel);

		}

		return mav;
	}

}
