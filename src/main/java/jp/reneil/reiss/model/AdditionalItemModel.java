/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.model;

import java.util.Date;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 付帯情報項目名_テーブルクラスです。<br>
 *
 * @version 1.0.0
 */
public class AdditionalItemModel {

	/** テナントID */
	private String tenant_id;

	/** テナント名 */
	private String tenant_name;

	/** 項目ID */
	private String item_id;

	/** 項目名 */
	private String item_name;

	/** 表示順 */
	private int display_no;

	/** 有効開始日 */
	private Date start_date;

	/** 有効終了日 */
	private Date end_date;

	/** String 有効開始日 */
	private String str_start_date;

	/** String 有効終了日 */
	private String str_end_date;

	/** 削除フラグ */
	private byte del_flg;

	/** String 削除フラグ */
	private String str_del_flg;

	/** 更新ユーザーID */
	private String update_user_id;

	/** 更新日 */
	private Date update_date;

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String getTenant_name() {
		return tenant_name;
	}

	public void setTenant_name(String tenant_name) {
		this.tenant_name = tenant_name;
	}

	public String getItem_id() {
		return item_id;
	}

	public void setItem_id(String item_id) {
		this.item_id = item_id;
	}

	public String getItem_name() {
		return item_name;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public int getDisplay_no() {
		return display_no;
	}

	public void setDisplay_no(int display_no) {
		this.display_no = display_no;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getStr_start_date() {
		return str_start_date;
	}

	public void setStr_start_date(String str_start_date) {
		this.str_start_date = str_start_date;
	}

	public String getStr_end_date() {
		return str_end_date;
	}

	public void setStr_end_date(String str_end_date) {
		this.str_end_date = str_end_date;
	}

	public byte getDel_flg() {
		return del_flg;
	}

	public void setDel_flg(byte del_flg) {
		this.del_flg = del_flg;
	}

	public String getStr_del_flg() {
		return str_del_flg;
	}

	public void setStr_del_flg(String str_del_flg) {
		this.str_del_flg = str_del_flg;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}
}
