/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.model;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * イメージデータクラスです。<br>
 *
 * @version 1.0.0
 */
public class ImageData {

	/** ディレクトリID */
	private String directoryID;

	/** モーダルNo */
	private Integer modalNo;

	/** 表示順 */
	private int disNo;

	/** 画像ファイル名 */
	private String fileName;

	/** 画像ファイルパス */
	private String filePath;

	/** 画像ファイルディレクトリパス */
	private String directoryPath;

	/** イメージデータ（Base64エンコード） */
	private String src;

	/** 撮影端末 */
	private String termName;

	/** 撮影日時 */
	private String photoDate;

	/** ファイル日時 */
	private String dateTime;

	/** 撮影場所（緯度） */
	private String locateLat;

	/** 撮影場所（経度） */
	private String locateLon;

	/** 撮影場所 */
	private String locate;

	/** 付帯項目html */
	private String addInfoHtml;

	/** 付帯情報html */
	private String addItemHtml;

	/** モーダルhtml */
	private String modalHtml;

	public String getDirectoryID() {
		return directoryID;
	}

	public void setDirectoryID(String directoryID) {
		this.directoryID = directoryID;
	}

	public Integer getModalNo() {
		return modalNo;
	}

	public void setModalNo(Integer modalNo) {
		this.modalNo = modalNo;
	}

	public int getDisNo() {
		return disNo;
	}

	public void setDisNo(int disNo) {
		this.disNo = disNo;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public String getDirectoryPath() {
		return directoryPath;
	}

	public void setDirectoryPath(String directoryPath) {
		this.directoryPath = directoryPath;
	}

	public String getTermName() {
		return termName;
	}

	public void setTermName(String termName) {
		this.termName = termName;
	}

	public String getPhotoDate() {
		return photoDate;
	}

	public void setPhotoDate(String photoDate) {
		this.photoDate = photoDate;
	}

	public String getDateTime() {
		return dateTime;
	}

	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public String getLocateLat() {
		return locateLat;
	}

	public void setLocateLat(String locateLat) {
		this.locateLat = locateLat;
	}

	public String getLocateLon() {
		return locateLon;
	}

	public void setLocateLon(String locateLon) {
		this.locateLon = locateLon;
	}

	public String getLocate() {
		return locate;
	}

	public void setLocate(String locate) {
		this.locate = locate;
	}

	public String getAddInfoHtml() {
		return addInfoHtml;
	}

	public void setAddInfoHtml(String addInfoHtml) {
		this.addInfoHtml = addInfoHtml;
	}

	public String getAddItemHtml() {
		return addItemHtml;
	}

	public void setAddItemHtml(String addItemHtml) {
		this.addItemHtml = addItemHtml;
	}

	public String getModalHtml() {
		return modalHtml;
	}

	public void setModalHtml(String modalHtml) {
		this.modalHtml = modalHtml;
	}
}
