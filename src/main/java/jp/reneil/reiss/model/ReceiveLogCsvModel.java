/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 受信ログ_テーブルクラスです。<br>
 *
 * @version 1.0.0
 */
@JsonPropertyOrder({"開始日時", "終了日時", "送信元IP", "ファイル名", "端末ID", "端末名", "受信区分", "処理結果", "処理内容", "エラーメッセージ"})
public class ReceiveLogCsvModel {

	/** 開始日時 */
	@JsonProperty("開始日時")
	private String str_start_date;

	/** 終了日時 */
	@JsonProperty("終了日時")
	private String str_end_date;

	/** 送信元IP */
	@JsonProperty("送信元IP")
	private String sender_ip;

	/** ファイル名 */
	@JsonProperty("ファイル名")
	private String file_name;

	/** 端末ID */
	@JsonProperty("端末ID")
	private String term_id;

	/** 端末名 */
	@JsonProperty("端末名")
	private String term_name;

	/** 受信区分 */
	@JsonProperty("受信区分")
	private String division_name;

	/** 処理結果 */
	@JsonProperty("処理結果")
	private String result_name;

	/** 処理内容 */
	@JsonProperty("処理内容")
	private String contents;

	/** エラーメッセージ */
	@JsonProperty("エラーメッセージ")
	private String err_msg;

	public String getStr_start_date() {
		return str_start_date;
	}

	public void setStr_start_date(String str_start_date) {
		this.str_start_date = str_start_date;
	}

	public String getStr_end_date() {
		return str_end_date;
	}

	public void setStr_end_date(String str_end_date) {
		this.str_end_date = str_end_date;
	}

	public String getSender_ip() {
		return sender_ip;
	}

	public void setSender_ip(String sender_ip) {
		this.sender_ip = sender_ip;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getTerm_id() {
		return term_id;
	}

	public void setTerm_id(String term_id) {
		this.term_id = term_id;
	}

	public String getTerm_name() {
		return term_name;
	}

	public void setTerm_name(String term_name) {
		this.term_name = term_name;
	}

	public String getDivision_name() {
		return division_name;
	}

	public void setDivision_name(String division_name) {
		this.division_name = division_name;
	}

	public String getResult_name() {
		return result_name;
	}

	public void setResult_name(String result_name) {
		this.result_name = result_name;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getErr_msg() {
		return err_msg;
	}

	public void setErr_msg(String err_msg) {
		this.err_msg = err_msg;
	}

}
