/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.model;

import java.util.List;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ディレクトリクラスです。<br>
 *
 * @version 1.0.0
 */
public class Directory extends Node {

	/** 表示順 */
	private int disNo = 0;

	/** ディレクトリID */
	private String dirId;

	/** ノードリスト */
	private List<Node> nodes;

	public Directory(String text) {
		super(text);
	}

	public int getDisNo() {
		return disNo;
	}

	public void setDisNo(int disNo) {
		this.disNo = disNo;
	}

	public String getDirId() {
		return dirId;
	}

	public void setDirId(String dirId) {
		this.dirId = dirId;
	}

	public List<Node> getNodes() {
		return nodes;
	}

	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}
}
