/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.model;

import java.util.Date;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザ情報_テーブルクラスです。<br>
 *
 * @version 1.0.0
 */
public class UserInfoModel {

	/** ユーザーID */
	private String user_id;

	/** ユーザー名 */
	private String user_name;

	/** テナントID */
	private String tenant_id;

	/** テナント名 */
	private String tenant_name;

	/** ライブラリ名 */
	private String lib_name;

	/** ユーザ種別コード */
	private String auth_cd;

	/** パスワード */
	private String pwd;

	/** 複合化パスワード */
	private String dec_pwd;

	/** パスワード更新日 */
	private Date pwd_update;

	/** パスワードエラーカウント */
	private int pwd_errcnt;

	/** 最終ログイン日時 */
	private Date last_login_time;

	/** String 最終ログイン日時 */
	private String str_last_login_time;

	/** 有効開始日 */
	private Date start_date;

	/** 有効終了日 */
	private Date end_date;

	/** String 有効開始日 */
	private String str_start_date;

	/** String 有効終了日 */
	private String str_end_date;

	/** 削除フラグ */
	private byte del_flg;

	/** String 削除フラグ */
	private String str_del_flg;

	/** 更新ユーザーID */
	private String update_user_id;

	/** 更新日 */
	private Date update_date;

	/** 管理権限 */
	private String manage_auth;

	/** 帳票出力権限 */
	private String output_auth;

	/** 付帯情報編集権限 */
	private String edit_auth;

	/** ダウンロード権限 */
	private String download_auth;

	/** 移動権限 */
	private String move_auth;

	/** 削除権限 */
	private String delete_auth;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String getTenant_name() {
		return tenant_name;
	}

	public void setTenant_name(String tenant_name) {
		this.tenant_name = tenant_name;
	}

	public String getLib_name() {
		return lib_name;
	}

	public void setLib_name(String lib_name) {
		this.lib_name = lib_name;
	}

	public String getAuth_cd() {
		return auth_cd;
	}

	public void setAuth_cd(String auth_cd) {
		this.auth_cd = auth_cd;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getDec_pwd() {
		return dec_pwd;
	}

	public void setDec_pwd(String dec_pwd) {
		this.dec_pwd = dec_pwd;
	}

	public Date getPwd_update() {
		return pwd_update;
	}

	public void setPwd_update(Date pwd_update) {
		this.pwd_update = pwd_update;
	}

	public int getPwd_errcnt() {
		return pwd_errcnt;
	}

	public void setPwd_errcnt(int pwd_errcnt) {
		this.pwd_errcnt = pwd_errcnt;
	}

	public Date getLast_login_time() {
		return last_login_time;
	}

	public void setLast_login_time(Date last_login_time) {
		this.last_login_time = last_login_time;
	}

	public String getStr_last_login_time() {
		return str_last_login_time;
	}

	public void setStr_last_login_time(String str_last_login_time) {
		this.str_last_login_time = str_last_login_time;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getStr_start_date() {
		return str_start_date;
	}

	public void setStr_start_date(String str_start_date) {
		this.str_start_date = str_start_date;
	}

	public String getStr_end_date() {
		return str_end_date;
	}

	public void setStr_end_date(String str_end_date) {
		this.str_end_date = str_end_date;
	}

	public byte getDel_flg() {
		return del_flg;
	}

	public void setDel_flg(byte del_flg) {
		this.del_flg = del_flg;
	}

	public String getStr_del_flg() {
		return str_del_flg;
	}

	public void setStr_del_flg(String str_del_flg) {
		this.str_del_flg = str_del_flg;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	public String getManage_auth() {
		return manage_auth;
	}

	public void setManage_auth(String manage_auth) {
		this.manage_auth = manage_auth;
	}

	public String getOutput_auth() {
		return output_auth;
	}

	public void setOutput_auth(String output_auth) {
		this.output_auth = output_auth;
	}

	public String getEdit_auth() {
		return edit_auth;
	}

	public void setEdit_auth(String edit_auth) {
		this.edit_auth = edit_auth;
	}

	public String getDownload_auth() {
		return download_auth;
	}

	public void setDownload_auth(String download_auth) {
		this.download_auth = download_auth;
	}

	public String getMove_auth() {
		return move_auth;
	}

	public void setMove_auth(String move_auth) {
		this.move_auth = move_auth;
	}

	public String getDelete_auth() {
		return delete_auth;
	}

	public void setDelete_auth(String delete_auth) {
		this.delete_auth = delete_auth;
	}

}
