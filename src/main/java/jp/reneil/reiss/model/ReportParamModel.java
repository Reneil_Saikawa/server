/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.model;

import java.util.Date;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 帳票パラメータ_テーブルクラスです。<br>
 *
 * @version 1.0.0
 */
public class ReportParamModel {

	/** テナントID */
	private String tenant_id;

	/** 帳票ID */
	private String report_id;

	/** 帳票名 */
	private String report_name;

	/** テンプレートファイル名 */
	private String file_name;

	/** シート名 */
	private String sheet_name;

	/** 1ページ当たりの出力件数 */
	private short print_rows;

	/* 表紙出力フラグ */
	private byte front_page_flg;

	/** String 表紙出力フラグ */
	private String str_front_page_flg;

	/** 表紙用シート名 */
	private String front_page_sheet;

	/* 有効開始日 */
	private Date start_date;

	/* 有効終了日 */
	private Date end_date;

	/** String 有効開始日 */
	private String str_start_date;

	/** String 有効終了日 */
	private String str_end_date;

	/* 削除フラグ */
	private byte del_flg;

	/** String 削除フラグ */
	private String str_del_flg;

	/** 更新ユーザーID */
	private String update_user_id;

	/* 更新日 */
	private Date update_date;

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String getReport_id() {
		return report_id;
	}

	public void setReport_id(String report_id) {
		this.report_id = report_id;
	}

	public String getReport_name() {
		return report_name;
	}

	public void setReport_name(String report_name) {
		this.report_name = report_name;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getSheet_name() {
		return sheet_name;
	}

	public void setSheet_name(String sheet_name) {
		this.sheet_name = sheet_name;
	}

	public short getPrint_rows() {
		return print_rows;
	}

	public void setPrint_rows(short print_rows) {
		this.print_rows = print_rows;
	}

	public byte getFront_page_flg() {
		return front_page_flg;
	}

	public void setFront_page_flg(byte front_page_flg) {
		this.front_page_flg = front_page_flg;
	}

	public String getStr_front_page_flg() {
		return str_front_page_flg;
	}

	public void setStr_front_page_flg(String str_front_page_flg) {
		this.str_front_page_flg = str_front_page_flg;
	}

	public String getFront_page_sheet() {
		return front_page_sheet;
	}

	public void setFront_page_sheet(String front_page_sheet) {
		this.front_page_sheet = front_page_sheet;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getStr_start_date() {
		return str_start_date;
	}

	public void setStr_start_date(String str_start_date) {
		this.str_start_date = str_start_date;
	}

	public String getStr_end_date() {
		return str_end_date;
	}

	public void setStr_end_date(String str_end_date) {
		this.str_end_date = str_end_date;
	}

	public byte getDel_flg() {
		return del_flg;
	}

	public void setDel_flg(byte del_flg) {
		this.del_flg = del_flg;
	}

	public String getStr_del_flg() {
		return str_del_flg;
	}

	public void setStr_del_flg(String str_del_flg) {
		this.str_del_flg = str_del_flg;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

}
