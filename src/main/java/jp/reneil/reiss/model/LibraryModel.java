/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.model;

import java.util.Date;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ライブラリ_テーブルクラスです。<br>
 *
 * @version 1.0.0
 */
public class LibraryModel {

	/** ライブラリID */
	private String lib_id;

	/** テナントID */
	private String tenant_id;

	/** 親ライブラリID */
	private String parent_lid;

	/** ライブラリ名 */
	private String lib_name;

	/** ライブラリ名（表示用） */
	private String lib_name_text;

	/** 親ライブラリ名 */
	private String parent_lname;

	/** ライブラリパス */
	private String lib_path;

	/** ライブラリパス（表示用） */
	private String lib_path_text;

	/** ライブルーム名 */
	private String live_room;

	/** 表示順 */
	private int display_no;

	/** 更新ユーザーID */
	private String update_user_id;

	/** 更新日 */
	private Date update_date;

	public String getLib_id() {
		return lib_id;
	}

	public void setLib_id(String lib_id) {
		this.lib_id = lib_id;
	}

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String getParent_lid() {
		return parent_lid;
	}

	public void setParent_lid(String parent_lid) {
		this.parent_lid = parent_lid;
	}

	public String getLib_name() {
		return lib_name;
	}

	public void setLib_name(String lib_name) {
		this.lib_name = lib_name;
	}

	public String getLib_name_text() {
		return lib_name_text;
	}

	public void setLib_name_text(String lib_name_text) {
		this.lib_name_text = lib_name_text;
	}

	public String getParent_lname() {
		return parent_lname;
	}

	public void setParent_lname(String parent_lname) {
		this.parent_lname = parent_lname;
	}

	public String getLib_path() {
		return lib_path;
	}

	public void setLib_path(String lib_path) {
		this.lib_path = lib_path;
	}

	public String getLib_path_text() {
		return lib_path_text;
	}

	public void setLib_path_text(String lib_path_text) {
		this.lib_path_text = lib_path_text;
	}

	public String getLive_room() {
		return live_room;
	}

	public void setLive_room(String live_room) {
		this.live_room = live_room;
	}

	public int getDisplay_no() {
		return display_no;
	}

	public void setDisplay_no(int display_no) {
		this.display_no = display_no;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

}
