/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.model;

import java.util.Date;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * コンテンツ情報_テーブルクラスです。<br>
 *
 * @version 1.0.0
 */
public class ContentsModel {

	/** 登録番号 */
	private String reg_no;

	/** フォーマット区分 */
	private String file_format;

	/** 格納パス */
	private String file_path;

	/** ファイル名 */
	private String file_name;

	/** 撮影端末ID */
	private String term_id;

	/** 撮影端末名 */
	private String term_name;

	/** 撮影日 */
	private Date photo_date;

	/** 撮影場所（緯度） */
	private double locate_lat;

	/** 撮影場所（軽度） */
	private double locate_lon;

	/** 撮影場所 */
	private String locate;

	/** 更新ユーザーID */
	private String update_user_id;

	/** 更新日 */
	private Date update_date;

	public String getReg_no() {
		return reg_no;
	}

	public void setReg_no(String reg_no) {
		this.reg_no = reg_no;
	}

	public String getFile_format() {
		return file_format;
	}

	public void setFile_format(String file_format) {
		this.file_format = file_format;
	}

	public String getFile_path() {
		return file_path;
	}

	public void setFile_path(String file_path) {
		this.file_path = file_path;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getTerm_id() {
		return term_id;
	}

	public void setTerm_id(String term_id) {
		this.term_id = term_id;
	}

	public String getTerm_name() {
		return term_name;
	}

	public void setTerm_name(String term_name) {
		this.term_name = term_name;
	}

	public Date getPhoto_date() {
		return photo_date;
	}

	public void setPhoto_date(Date photo_date) {
		this.photo_date = photo_date;
	}

	public double getLocate_lat() {
		return locate_lat;
	}

	public void setLocate_lat(double locate_lat) {
		this.locate_lat = locate_lat;
	}

	public double getLocate_lon() {
		return locate_lon;
	}

	public void setLocate_lon(double locate_lon) {
		this.locate_lon = locate_lon;
	}

	public String getLocate() {
		return locate;
	}

	public void setLocate(String locate) {
		this.locate = locate;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

}
