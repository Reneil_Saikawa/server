/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2020/02/14      1.0.1           女屋            地図有効化機能追加
 *    3    2020/02/20      1.0.2           女屋            帳票有効化機能追加
 */
package jp.reneil.reiss.model;

import java.util.Date;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * テナント_テーブルクラスです。<br>
 *
 * @version 1.0.2
 */
public class TenantModel {

	/** テナントID */
	private String tenant_id;

	/** テナント名 */
	private String tenant_name;

	/** ユーザーライセンス数 */
	private short user_license;

	/** 端末ライセンス数 */
	private short term_license;

	/** 静止画撮影機能 */
	private byte stillimage_function;

	/** 動画撮影機能 */
	private byte movie_function;

	/** ライブ撮影機能 */
	private byte live_function;

	/** String 静止画撮影機能 */
	private String str_stillimage_function;

	/** String 動画撮影機能 */
	private String str_movie_function;

	/** String ライブ撮影機能 */
	private String str_live_function;

	/** 起動時一時ファイル削除 */
	private byte start_tmp_del;

	/** String 起動時一時ファイル削除 */
	private String str_start_tmp_del;

	/** 一時ファイル時間経過削除 */
	private short tmp_del_time;

	/** アプリ自動停止時間 */
	private short stop_time;

	/** パスワードエラーカウント */
	private short password_lock_times;

	/** サムネイルサイズ（横） */
	private short thumbnail_width;

	/** サムネイルサイズ（縦） */
	private short thumbnail_height;

	/** パトライト有効化 */
	private byte patlite_enable;

	/** String パトライト有効化 */
	private String str_patlite_enable;

	/* パトライトアドレス */
	private String patLite_ip;

	/* パトライトログイン名 */
	private String patLite_login_name;

	/* パトライト受信時オプション */
	private String patLite_option_rcv;

	/* パトライト受信時鳴動時間 */
	private short patLite_time_rcv;

	/* パトライト警告時オプション */
	private String patLite_option_warm;

	/* パトライト警告時鳴動時間 */
	private short patLite_time_warm;

	/* パトライトエラー時オプション */
	private String patLite_option_err;

	/* パトライトエラー時鳴動時間 */
	private short patLite_time_err;

	/** ライブ接続先 */
	private String live_server;

	/** ライブアプリ名 */
	private String live_app;

	/** ライブルーム名 */
	private String live_room;

	/** 地図有効化 */
	private byte map_enable;

	/** String 地図有効化 */
	private String str_map_enable;

	/** 地図サーバ */
	private String map_server;

	/** 帳票有効化 */
	private byte report_enable;

	/** String 帳票有効化 */
	private String str_report_enable;

	/** ファイルパス表示 */
	private byte display_file_path;

	/** String ファイルパス表示 */
	private String str_display_file_path;

	/** 有効開始日 */
	private Date start_date;

	/** 有効終了日 */
	private Date end_date;

	/** String 有効開始日 */
	private String str_start_date;

	/** String 有効終了日 */
	private String str_end_date;

	/** 削除フラグ */
	private byte del_flg;

	/** String 削除フラグ */
	private String str_del_flg;

	/** 更新ユーザーID */
	private String update_user_id;

	/** 更新日 */
	private Date update_date;

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String getTenant_name() {
		return tenant_name;
	}

	public void setTenant_name(String tenant_name) {
		this.tenant_name = tenant_name;
	}

	public short getUser_license() {
		return user_license;
	}

	public void setUser_license(short user_license) {
		this.user_license = user_license;
	}

	public short getTerm_license() {
		return term_license;
	}

	public void setTerm_license(short term_license) {
		this.term_license = term_license;
	}

	public byte getStillimage_function() {
		return stillimage_function;
	}

	public void setStillimage_function(byte stillimage_function) {
		this.stillimage_function = stillimage_function;
	}

	public byte getMovie_function() {
		return movie_function;
	}

	public void setMovie_function(byte movie_function) {
		this.movie_function = movie_function;
	}

	public byte getLive_function() {
		return live_function;
	}

	public void setLive_function(byte live_function) {
		this.live_function = live_function;
	}

	public String getStr_stillimage_function() {
		return str_stillimage_function;
	}

	public void setStr_stillimage_function(String str_stillimage_function) {
		this.str_stillimage_function = str_stillimage_function;
	}

	public String getStr_movie_function() {
		return str_movie_function;
	}

	public void setStr_movie_function(String str_movie_function) {
		this.str_movie_function = str_movie_function;
	}

	public String getStr_live_function() {
		return str_live_function;
	}

	public void setStr_live_function(String str_live_function) {
		this.str_live_function = str_live_function;
	}

	public byte getStart_tmp_del() {
		return start_tmp_del;
	}

	public void setStart_tmp_del(byte start_tmp_del) {
		this.start_tmp_del = start_tmp_del;
	}

	public String getStr_start_tmp_del() {
		return str_start_tmp_del;
	}

	public void setStr_start_tmp_del(String str_start_tmp_del) {
		this.str_start_tmp_del = str_start_tmp_del;
	}

	public short getTmp_del_time() {
		return tmp_del_time;
	}

	public void setTmp_del_time(short tmp_del_time) {
		this.tmp_del_time = tmp_del_time;
	}

	public short getStop_time() {
		return stop_time;
	}

	public void setStop_time(short stop_time) {
		this.stop_time = stop_time;
	}

	public short getPassword_lock_times() {
		return password_lock_times;
	}

	public void setPassword_lock_times(short password_lock_times) {
		this.password_lock_times = password_lock_times;
	}

	public short getThumbnail_width() {
		return thumbnail_width;
	}

	public void setThumbnail_width(short thumbnail_width) {
		this.thumbnail_width = thumbnail_width;
	}

	public short getThumbnail_height() {
		return thumbnail_height;
	}

	public void setThumbnail_height(short thumbnail_height) {
		this.thumbnail_height = thumbnail_height;
	}

	public byte getPatlite_enable() {
		return patlite_enable;
	}

	public void setPatlite_enable(byte patlite_enable) {
		this.patlite_enable = patlite_enable;
	}

	public String getStr_patlite_enable() {
		return str_patlite_enable;
	}

	public void setStr_patlite_enable(String str_patlite_enable) {
		this.str_patlite_enable = str_patlite_enable;
	}

	public String getPatLite_ip() {
		return patLite_ip;
	}

	public void setPatLite_ip(String patLite_ip) {
		this.patLite_ip = patLite_ip;
	}

	public String getPatLite_login_name() {
		return patLite_login_name;
	}

	public void setPatLite_login_name(String patLite_login_name) {
		this.patLite_login_name = patLite_login_name;
	}

	public String getPatLite_option_rcv() {
		return patLite_option_rcv;
	}

	public void setPatLite_option_rcv(String patLite_option_rcv) {
		this.patLite_option_rcv = patLite_option_rcv;
	}

	public short getPatLite_time_rcv() {
		return patLite_time_rcv;
	}

	public void setPatLite_time_rcv(short patLite_time_rcv) {
		this.patLite_time_rcv = patLite_time_rcv;
	}

	public String getPatLite_option_warm() {
		return patLite_option_warm;
	}

	public void setPatLite_option_warm(String patLite_option_warm) {
		this.patLite_option_warm = patLite_option_warm;
	}

	public short getPatLite_time_warm() {
		return patLite_time_warm;
	}

	public void setPatLite_time_warm(short patLite_time_warm) {
		this.patLite_time_warm = patLite_time_warm;
	}

	public String getPatLite_option_err() {
		return patLite_option_err;
	}

	public void setPatLite_option_err(String patLite_option_err) {
		this.patLite_option_err = patLite_option_err;
	}

	public short getPatLite_time_err() {
		return patLite_time_err;
	}

	public void setPatLite_time_err(short patLite_time_err) {
		this.patLite_time_err = patLite_time_err;
	}

	public String getLive_server() {
		return live_server;
	}

	public void setLive_server(String live_server) {
		this.live_server = live_server;
	}

	public String getLive_app() {
		return live_app;
	}

	public void setLive_app(String live_app) {
		this.live_app = live_app;
	}

	public String getLive_room() {
		return live_room;
	}

	public void setLive_room(String live_room) {
		this.live_room = live_room;
	}

	public byte getMap_enable() {
		return map_enable;
	}

	public void setMap_enable(byte map_enable) {
		this.map_enable = map_enable;
	}

	public String getStr_map_enable() {
		return str_map_enable;
	}

	public void setStr_map_enable(String str_map_enable) {
		this.str_map_enable = str_map_enable;
	}

	public String getMap_server() {
		return map_server;
	}

	public void setMap_server(String map_server) {
		this.map_server = map_server;
	}

	public byte getReport_enable() {
		return report_enable;
	}

	public void setReport_enable(byte report_enable) {
		this.report_enable = report_enable;
	}

	public String getStr_report_enable() {
		return str_report_enable;
	}

	public void setStr_report_enable(String str_report_enable) {
		this.str_report_enable = str_report_enable;
	}

	public byte getDisplay_file_path() {
		return display_file_path;
	}

	public void setDisplay_file_path(byte display_file_path) {
		this.display_file_path = display_file_path;
	}

	public String getStr_display_file_path() {
		return str_display_file_path;
	}

	public void setStr_display_file_path(String str_display_file_path) {
		this.str_display_file_path = str_display_file_path;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getStr_start_date() {
		return str_start_date;
	}

	public void setStr_start_date(String str_start_date) {
		this.str_start_date = str_start_date;
	}

	public String getStr_end_date() {
		return str_end_date;
	}

	public void setStr_end_date(String str_end_date) {
		this.str_end_date = str_end_date;
	}

	public byte getDel_flg() {
		return del_flg;
	}

	public void setDel_flg(byte del_flg) {
		this.del_flg = del_flg;
	}

	public String getStr_del_flg() {
		return str_del_flg;
	}

	public void setStr_del_flg(String str_del_flg) {
		this.str_del_flg = str_del_flg;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

}
