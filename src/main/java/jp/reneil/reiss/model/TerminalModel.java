/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.model;

import java.util.Date;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 端末_テーブルクラスです。<br>
 *
 * @version 1.0.0
 */
public class TerminalModel {

	/** 端末ID */
	private String term_id;

	/** 端末区分  */
	private String term_ctg;

	/** 端末区分名  */
	private String term_ctg_name;

	/** 端末名 */
	private String term_name;

	/** テナントID */
	private String tenant_id;

	/** ライブラリID */
	private String lib_id;

	/** ライブラリ名 */
	private String lib_name;

	/** 撮影年フォルダ作成フラグ */
	private byte year_folder_flg;

	/** 撮影月フォルダ作成フラグ */
	private byte month_folder_flg;

	/** 撮影日フォルダ作成フラグ */
	private byte day_folder_flg;

	/** 日本語表記フラグ */
	private byte japanese_flg;

	/** String 撮影年フォルダ作成フラグ */
	private String str_year_folder_flg;

	/** String 撮影月フォルダ作成フラグ */
	private String str_month_folder_flg;

	/** String 撮影日フォルダ作成フラグ */
	private String str_day_folder_flg;

	/** String 日本語表記フラグ */
	private String str_japanese_flg;

	/** 動画撮影最大時間 */
	private short m_max_time;

	/** ライブ撮影サイズ（横） */
	private short live_width;

	/** ライブ最小フレームレート */
	private short live_min_fps;

	/** ライブ最大フレームレート */
	private short live_max_fps;

	/** 有効開始日 */
	private Date start_date;

	/** 有効終了日 */
	private Date end_date;

	/** String 有効開始日 */
	private String str_start_date;

	/** String 有効終了日 */
	private String str_end_date;

	/** 削除フラグ */
	private byte del_flg;

	/** String 削除フラグ */
	private String str_del_flg;

	/** 更新ユーザーID */
	private String update_user_id;

	/** 更新日 */
	private Date update_date;

	public String getTerm_id() {
		return term_id;
	}

	public void setTerm_id(String term_id) {
		this.term_id = term_id;
	}

	public String getTerm_ctg() {
		return term_ctg;
	}

	public void setTerm_ctg(String term_ctg) {
		this.term_ctg = term_ctg;
	}

	public String getTerm_ctg_name() {
		return term_ctg_name;
	}

	public void setTerm_ctg_name(String term_ctg_name) {
		this.term_ctg_name = term_ctg_name;
	}

	public String getTerm_name() {
		return term_name;
	}

	public void setTerm_name(String term_name) {
		this.term_name = term_name;
	}

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String getLib_id() {
		return lib_id;
	}

	public void setLib_id(String lib_id) {
		this.lib_id = lib_id;
	}

	public String getLib_name() {
		return lib_name;
	}

	public void setLib_name(String lib_name) {
		this.lib_name = lib_name;
	}

	public byte getYear_folder_flg() {
		return year_folder_flg;
	}

	public void setYear_folder_flg(byte year_folder_flg) {
		this.year_folder_flg = year_folder_flg;
	}

	public byte getMonth_folder_flg() {
		return month_folder_flg;
	}

	public void setMonth_folder_flg(byte month_folder_flg) {
		this.month_folder_flg = month_folder_flg;
	}

	public byte getDay_folder_flg() {
		return day_folder_flg;
	}

	public void setDay_folder_flg(byte day_folder_flg) {
		this.day_folder_flg = day_folder_flg;
	}

	public byte getJapanese_flg() {
		return japanese_flg;
	}

	public void setJapanese_flg(byte japanese_flg) {
		this.japanese_flg = japanese_flg;
	}

	public String getStr_year_folder_flg() {
		return str_year_folder_flg;
	}

	public void setStr_year_folder_flg(String str_year_folder_flg) {
		this.str_year_folder_flg = str_year_folder_flg;
	}

	public String getStr_month_folder_flg() {
		return str_month_folder_flg;
	}

	public void setStr_month_folder_flg(String str_month_folder_flg) {
		this.str_month_folder_flg = str_month_folder_flg;
	}

	public String getStr_day_folder_flg() {
		return str_day_folder_flg;
	}

	public void setStr_day_folder_flg(String str_day_folder_flg) {
		this.str_day_folder_flg = str_day_folder_flg;
	}

	public String getStr_japanese_flg() {
		return str_japanese_flg;
	}

	public void setStr_japanese_flg(String str_japanese_flg) {
		this.str_japanese_flg = str_japanese_flg;
	}

	public short getM_max_time() {
		return m_max_time;
	}

	public void setM_max_time(short m_max_time) {
		this.m_max_time = m_max_time;
	}

	public short getLive_width() {
		return live_width;
	}

	public void setLive_width(short live_width) {
		this.live_width = live_width;
	}

	public short getLive_min_fps() {
		return live_min_fps;
	}

	public void setLive_min_fps(short live_min_fps) {
		this.live_min_fps = live_min_fps;
	}

	public short getLive_max_fps() {
		return live_max_fps;
	}

	public void setLive_max_fps(short live_max_fps) {
		this.live_max_fps = live_max_fps;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getStr_start_date() {
		return str_start_date;
	}

	public void setStr_start_date(String str_start_date) {
		this.str_start_date = str_start_date;
	}

	public String getStr_end_date() {
		return str_end_date;
	}

	public void setStr_end_date(String str_end_date) {
		this.str_end_date = str_end_date;
	}

	public byte getDel_flg() {
		return del_flg;
	}

	public void setDel_flg(byte del_flg) {
		this.del_flg = del_flg;
	}

	public String getStr_del_flg() {
		return str_del_flg;
	}

	public void setStr_del_flg(String str_del_flg) {
		this.str_del_flg = str_del_flg;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

}
