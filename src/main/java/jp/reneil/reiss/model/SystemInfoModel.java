/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.model;

import java.util.Date;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * システム情報_テーブルクラスです。<br>
 *
 * @version 1.0.0
 */
public class SystemInfoModel {

	/** ID */
	private String system_id;

	/** システム名 */
	private String system_name;

	/** ルートパス */
	private String system_rootpath;

	/** 内部URL */
	private String system_in_url;

	/** 外部URL */
	private String system_out_url;

	/** ディレクトリ画像 */
	private String directory_image;

	/** 更新日 */
	private Date update_date;

	public String getSystem_id() {
		return system_id;
	}

	public void setSystem_id(String system_id) {
		this.system_id = system_id;
	}

	public String getSystem_name() {
		return system_name;
	}

	public void setSystem_name(String system_name) {
		this.system_name = system_name;
	}

	public String getSystem_rootpath() {
		return system_rootpath;
	}

	public void setSystem_rootpath(String system_rootpath) {
		this.system_rootpath = system_rootpath;
	}

	public String getSystem_in_url() {
		return system_in_url;
	}

	public void setSystem_in_url(String system_in_url) {
		this.system_in_url = system_in_url;
	}

	public String getSystem_out_url() {
		return system_out_url;
	}

	public void setSystem_out_url(String system_out_url) {
		this.system_out_url = system_out_url;
	}

	public String getDirectory_image() {
		return directory_image;
	}

	public void setDirectory_image(String directory_image) {
		this.directory_image = directory_image;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

}
