/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.model;

import java.util.Date;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * コードマスタ_テーブルクラスです。<br>
 *
 * @version 1.0.0
 */
public class CodeModel {

	/** 分類コード */
	private String code_01;

	/** コード */
	private String code_02;

	/** 一括コード */
	private String code;

	/** 名称1 */
	private String name_01;

	/** 名称2 */
	private String name_02;

	/** 名称3 */
	private String name_03;

	/** 備考 */
	private String remarks;

	/** 更新ユーザーID */
	private String update_user_id;

	/** 更新日 */
	private Date update_date;

	public String getCode_01() {
		return code_01;
	}

	public void setCode_01(String code_01) {
		this.code_01 = code_01;
	}

	public String getCode_02() {
		return code_02;
	}

	public void setCode_02(String code_02) {
		this.code_02 = code_02;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName_01() {
		return name_01;
	}

	public void setName_01(String name_01) {
		this.name_01 = name_01;
	}

	public String getName_02() {
		return name_02;
	}

	public void setName_02(String name_02) {
		this.name_02 = name_02;
	}

	public String getName_03() {
		return name_03;
	}

	public void setName_03(String name_03) {
		this.name_03 = name_03;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getUpdate_user_id() {
		return update_user_id;
	}

	public void setUpdate_user_id(String update_user_id) {
		this.update_user_id = update_user_id;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

}
