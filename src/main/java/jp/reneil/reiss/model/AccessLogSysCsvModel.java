/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * アクセスログ_テーブルクラスです。<br>
 *
 * @version 1.0.0
 */
@JsonPropertyOrder({"アクセス番号", "アクセス日", "アクセス元IP", "テナントID", "テナント名", "ユーザーID", "ユーザー名", "実行プログラムID", "実行プログラム", "処理結果ID", "処理結果", "処理内容", "エラーメッセージ"})
public class AccessLogSysCsvModel {

	/** アクセス番号 */
	@JsonProperty("アクセス番号")
	private long access_no;

	/** アクセス日 */
	@JsonProperty("アクセス日")
	private String str_access_date;

	/** アクセス元IP */
	@JsonProperty("アクセス元IP")
	private String access_ip;

	/** テナントID */
	@JsonProperty("テナントID")
	private String access_tenant_id;

	/** テナント名 */
	@JsonProperty("テナント名")
	private String access_tenant_name;

	/** ユーザーID */
	@JsonProperty("ユーザーID")
	private String access_user_id;

	/** ユーザー名 */
	@JsonProperty("ユーザー名")
	private String access_user_name;

	/** 実行プログラムID */
	@JsonProperty("実行プログラムID")
	private String prg_id;

	/** 実行プログラム */
	@JsonProperty("実行プログラム")
	private String prg_name;

	/** 処理結果ID */
	@JsonProperty("処理結果ID")
	private String result_id;

	/** 処理結果 */
	@JsonProperty("処理結果")
	private String result_name;

	/** 処理内容 */
	@JsonProperty("処理内容")
	private String contents;

	/** エラーメッセージ */
	@JsonProperty("エラーメッセージ")
	private String err_msg;

	public long getAccess_no() {
		return access_no;
	}

	public void setAccess_no(long access_no) {
		this.access_no = access_no;
	}

	public String getStr_access_date() {
		return str_access_date;
	}

	public void setStr_access_date(String str_access_date) {
		this.str_access_date = str_access_date;
	}

	public String getAccess_ip() {
		return access_ip;
	}

	public void setAccess_ip(String access_ip) {
		this.access_ip = access_ip;
	}

	public String getAccess_tenant_id() {
		return access_tenant_id;
	}

	public void setAccess_tenant_id(String access_tenant_id) {
		this.access_tenant_id = access_tenant_id;
	}

	public String getAccess_tenant_name() {
		return access_tenant_name;
	}

	public void setAccess_tenant_name(String access_tenant_name) {
		this.access_tenant_name = access_tenant_name;
	}

	public String getAccess_user_id() {
		return access_user_id;
	}

	public void setAccess_user_id(String access_user_id) {
		this.access_user_id = access_user_id;
	}

	public String getAccess_user_name() {
		return access_user_name;
	}

	public void setAccess_user_name(String access_user_name) {
		this.access_user_name = access_user_name;
	}

	public String getPrg_id() {
		return prg_id;
	}

	public void setPrg_id(String prg_id) {
		this.prg_id = prg_id;
	}

	public String getPrg_name() {
		return prg_name;
	}

	public void setPrg_name(String prg_name) {
		this.prg_name = prg_name;
	}

	public String getResult_id() {
		return result_id;
	}

	public void setResult_id(String result_id) {
		this.result_id = result_id;
	}

	public String getResult_name() {
		return result_name;
	}

	public void setResult_name(String result_name) {
		this.result_name = result_name;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getErr_msg() {
		return err_msg;
	}

	public void setErr_msg(String err_msg) {
		this.err_msg = err_msg;
	}

}
