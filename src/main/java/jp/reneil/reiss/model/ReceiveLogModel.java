/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.model;

import java.util.Date;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 受信ログ_テーブルクラスです。<br>
 *
 * @version 1.0.0
 */
public class ReceiveLogModel {

	/** 受信番号 */
	private long recv_no;

	/** 送信元IP */
	private String sender_ip;

	/** ファイル名 */
	private String file_name;

	/** テナントID */
	private String tenant_id;

	/** テナント名 */
	private String tenant_name;

	/** 端末ID */
	private String term_id;

	/** 端末名 */
	private String term_name;

	/** 受信区分ID */
	private String recv_division;

	/** 受信区分 */
	private String division_name;

	/** 処理結果ID */
	private String result_id;

	/** 処理結果 */
	private String result_name;

	/** 内容 */
	private String contents;

	/** エラーメッセージ */
	private String err_msg;

	/** 開始日時 */
	private Date start_date;

	/** String 開始日時 */
	private String str_start_date;

	/** 終了日時 */
	private Date end_date;

	/** String 終了日時 */
	private String str_end_date;

	/** 更新日時 */
	private Date update_date;

	/** String 更新日時 */
	private String str_update_date;

	public long getRecv_no() {
		return recv_no;
	}

	public void setRecv_no(long recv_no) {
		this.recv_no = recv_no;
	}

	public String getSender_ip() {
		return sender_ip;
	}

	public void setSender_ip(String sender_ip) {
		this.sender_ip = sender_ip;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getTenant_id() {
		return tenant_id;
	}

	public void setTenant_id(String tenant_id) {
		this.tenant_id = tenant_id;
	}

	public String getTenant_name() {
		return tenant_name;
	}

	public void setTenant_name(String tenant_name) {
		this.tenant_name = tenant_name;
	}

	public String getTerm_id() {
		return term_id;
	}

	public void setTerm_id(String term_id) {
		this.term_id = term_id;
	}

	public String getTerm_name() {
		return term_name;
	}

	public void setTerm_name(String term_name) {
		this.term_name = term_name;
	}

	public String getRecv_division() {
		return recv_division;
	}

	public void setRecv_division(String recv_division) {
		this.recv_division = recv_division;
	}

	public String getDivision_name() {
		return division_name;
	}

	public void setDivision_name(String division_name) {
		this.division_name = division_name;
	}

	public String getResult_id() {
		return result_id;
	}

	public void setResult_id(String result_id) {
		this.result_id = result_id;
	}

	public String getResult_name() {
		return result_name;
	}

	public void setResult_name(String result_name) {
		this.result_name = result_name;
	}

	public String getContents() {
		return contents;
	}

	public void setContents(String contents) {
		this.contents = contents;
	}

	public String getErr_msg() {
		return err_msg;
	}

	public void setErr_msg(String err_msg) {
		this.err_msg = err_msg;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public String getStr_start_date() {
		return str_start_date;
	}

	public void setStr_start_date(String str_start_date) {
		this.str_start_date = str_start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getStr_end_date() {
		return str_end_date;
	}

	public void setStr_end_date(String str_end_date) {
		this.str_end_date = str_end_date;
	}

	public Date getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}

	public String getStr_update_date() {
		return str_update_date;
	}

	public void setStr_update_date(String str_update_date) {
		this.str_update_date = str_update_date;
	}

}
