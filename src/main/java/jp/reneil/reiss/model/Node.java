/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.model;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ノードクラスです。<br>
 *
 * @version 1.0.0
 */
public class Node {

	/** ノードテキスト */
	private String text;

	public Node(String text) {
		this.text = text;
	}
	public String getText() {
		return text;
	}
}
