/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.exception;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * 共通例外情報クラスです。<br>
 *
 * @version 1.0.0
 */
public class ExException extends Exception {

	/** シリアルバージョンUID */
	private static final long serialVersionUID = 1L;

	/**
	 * 指定されたメッセージ文字列を保持する共通例外情報クラスです。<br>
	 * @param message メッセージ文字列
	 */
	public ExException(String message) {
		super(message);
	}
}