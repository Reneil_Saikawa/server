/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 */
package jp.reneil.reiss.common;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * メッセージ定数クラスです。<br>
 *
 * @version 1.0.0
 */
public class MessageConst {

	// ---------- message ----------

	/** M001：更新しました。 */
	public static final String M001 = "M001：更新しました。";

	// ---------- serverWarning ----------

	/** W000：ＩＤまたはパスワードが違います。 */
	public static final String W000 = "W000：パスワードが違います。";

	/** W001：ＩＤまたはパスワードが違います。 */
	public static final String W001 = "W001：ＩＤまたはパスワードが違います。";

	/** W002：セッション情報の取得に失敗しました。ログインし直してください。 */
	public static final String W002 = "W002：セッション情報の取得に失敗しました。ログインし直してください。";

	/** W003：パスワードの認証失敗が指定回数を超えました。システム管理者に連絡してください。 */
	public static final String W003 = "W003：パスワードの認証失敗が指定回数を超えました。システム管理者に連絡してください。";

	/** W004：セッションとユーザIDが一致しません。ログインし直してください。 */
	public static final String W004 = "W004：セッションとユーザIDが一致しません。ログインし直してください。";

	/** W015：アクセス権限がありません。 */
	public static final String W015 = "W015：アクセス権限がありません。";

	// ---------- serverFatal ----------

	/** E001：予期せぬエラーが発生しました。システム管理者に連絡してください。 */
	public static final String E001 = "E001：予期せぬエラーが発生しました。システム管理者に連絡してください。";


	// ---------- SystemManageTenantCtrlWarning ----------

	/** W520：すでに同名のテナントIDが登録されています。 */
	public static final String W520 = "W520：すでに同名のテナントIDが登録されています。";

	/** W521：ライセンス数を超えたユーザー数が登録されています。ライセンス数の修正、または不要ユーザーの削除フラグにチェックを入れてください。 */
	public static final String W521 = "W521：ライセンス数を超えたユーザー数が登録されています。ライセンス数の修正、または不要ユーザーの削除フラグにチェックを入れてください。";

	/** W522：ライセンス数を超えた端末数が登録されています。ライセンス数の修正、または不要端末の削除フラグにチェックを入れてください。 */
	public static final String W522 = "W522：ライセンス数を超えた端末数が登録されています。ライセンス数の修正、または不要端末の削除フラグにチェックを入れてください。";

	// ---------- SystemManageTenantManagerCtrlWarning ----------

	/** W530：すでに同名のテナント管理者IDが登録されています。 */
	public static final String W530 = "W530：すでに同名のテナント管理者IDが登録されています。";

	// ---------- MngLibraryCtrlWarning ----------

	/** W810：すでに同名のライブラリIDが登録されています。 */
	public static final String W810 = "W810：すでに同名のライブラリIDが登録されています。";

	/** W811：すでに同名のライブラリパスが登録されています。 */
	public static final String W811 = "W811：すでに同名のライブラリパスが登録されています。親ライブラリ、またはライブラリ名を変更してください。";

	/** W812：子ライブラリのパスが規定値を超過します。 */
	public static final String W812 = "W812：子ライブラリのパスが規定値を超過します。親ライブラリ、またはライブラリ名を変更してください。";

	// ---------- MngTerminalCtrlWarning ----------

	/** W820：すでに同名の端末IDが登録されています。 */
	public static final String W820 = "W820：すでに同名の端末IDが登録されています。";

	/** W821：ライセンス数の上限に達しています。ライセンスを購入、または不要端末の削除フラグにチェックを入れてください。 */
	public static final String W821 = "W821：ライセンス数の上限に達しています。ライセンスを購入、または不要端末の削除フラグにチェックを入れてください。";

	// ---------- MngUserinfoCtrlWarning ----------

	/** W830：すでに同名のユーザーIDが登録されています。 */
	public static final String W830 = "W830：すでに同名のユーザーIDが登録されています。";

	/** W831：ライセンス数の上限に達しています。ライセンスを購入、または不要ユーザーの削除フラグにチェックを入れてください。 */
	public static final String W831 = "W831：ライセンス数の上限に達しています。ライセンスを購入、または不要ユーザーの削除フラグにチェックを入れてください。";

	// ---------- MngAdditionalItemCtrlWarning ----------

	/** W840：すでに同名の項目IDが登録されています。 */
	public static final String W840 = "W840：すでに同名の項目IDが登録されています。";

	// ---------- MngReportParamCtrlWarning ----------

	/** W850：すでに同名の帳票IDが登録されています。 */
	public static final String W850 = "W850：すでに同名の帳票IDが登録されています。";

	/** W851：すでに同名のテンプレートファイルが登録されています。 */
	public static final String W851 = "W851：すでに同名のテンプレートファイルが登録されています。";

	/** W852：テンプレートファイルが存在しません。 */
	public static final String W852 = "W852：テンプレートファイルが存在しません。";

}
