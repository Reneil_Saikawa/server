/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/23      1.0.1           女屋            リトライ回数追加
 *    3    2020/03/03      1.0.2           斎川            ffmpegコマンド追加
 *    4    2020/03/03      1.0.3           女屋            rshコマンド追加
 */
package jp.reneil.reiss.common;

import java.io.File;

/**
 * 共通定数クラスです。<br>
 *
 * @version 1.0.3
 */
public class CommonConst {

	/** システム管理者ユーザーID */
	public static final String SYSTEM_MANAGER = "SystemManager";

	/** 成功CD */
	public static final String SUCCESS_CO = "0001";

	/** 失敗CD */
	public static final String FAILED_CO = "0002";

	/** テナント管理者のシステムCD */
	public static final String TENANT_MANAGER_CD = "0198";

	/** 一般ユーザのシステムCD */
	public static final String USER_CD = "0101";

	/** 管理権限 */
	public static final String MANAGE_AUTH = "0201";

	/** 帳票出力権限 */
	public static final String OUTPUT_AUTH = "0202";

	/** 付帯情報編集権限 */
	public static final String EDIT_AUTH = "0203";

	/** ダウンロード権限 */
	public static final String DOWNLOAD_AUTH = "0204";

	/** 移動権限 */
	public static final String MOVE_AUTH = "0205";

	/** 削除権限 */
	public static final String DELETE_AUTH = "0206";

	/** ファイルＰＡＴＨ置換用デリミッタ */
	public static final String YEN_MRK = File.separator;			// 	ファイルシステムの境界は越えないのでJava.io.File.separatorを使用

	/** 静止画ファイル頭文字 */
	public static final String STILLIMAGE = "S_";

	/** 動画ファイル頭文字 */
	public static final String MOVIE = "M_";

	/** ライブキャプチャーファイル頭文字 */
	public static final String CAPTURE = "C_";

	/** ライブ録画頭文字 */
	public static final String RECORDING = "R_";

	/** 静止画ファイル拡張子　.jpg */
	public static final String JPG = ".jpg";

	/** 動画ファイル拡張子　.mp4 */
	public static final String MP4 = ".mp4";

	/** 録画ファイル拡張子　.mp4 */
	public static final String WEBM = ".webm";

	/** サムネイルファイル拡張子　_.jpg */
	public static final String THJPG = "_.jpg";

	/** ダウンロードファイル拡張子　.zip */
	public static final String ZIP = ".zip";

	/** 帳票出力ファイル拡張子　.xls */
	public static final String XLS = ".xls";

	/** 帳票出力ファイル拡張子　.xlsx */
	public static final String XLSX = ".xlsx";

	/** 帳票出力ファイル拡張子　.csv */
	public static final String CSV = ".csv";

	/** 帳票出力タグ - イメージ */
	public static final String REPORT_TAG_IMAGE = "IMAGE";

	/** 帳票出力タグ - サムネイル */
	public static final String REPORT_TAG_THUMBNAIL = "THUMBNAIL";

	/** 帳票出力タグ - ファイル名 */
	public static final String REPORT_TAG_FILE_NAME = "FILE_NAME";

	/** 帳票出力タグ - ファイルパス */
	public static final String REPORT_TAG_FILE_PATH = "FILE_PATH";

	/** 帳票出力タグ - ファイルサイズ */
	public static final String REPORT_TAG_FILE_SIZE = "FILE_SIZE";

	/** 帳票出力タグ - 撮影端末ID */
	public static final String REPORT_TAG_TERM_ID = "TERM_ID";

	/** 帳票出力タグ - 撮影端末名 */
	public static final String REPORT_TAG_TERM_NAME = "TERM_NAME";

	/** 帳票出力タグ - 撮影日時 */
	public static final String REPORT_TAG_PHOTO_DATE = "PHOTO_DATE";

	/** 帳票出力タグ - 撮影場所 */
	public static final String REPORT_TAG_LOCATE = "LOCATE";

	/** 帳票出力タグ - 撮影場所（緯度） */
	public static final String REPORT_TAG_LOCATE_LAT = "LOCATE_LAT";

	/** 帳票出力タグ - 撮影場所（経度） */
	public static final String REPORT_TAG_LOCATE_LON = "LOCATE_LON";

	/** 画像タイプ jpeg */
	public static final String DATA_JPEG = "data:image/jpeg;base64,";

	/** ライブラリルート */
	public static final String LIBRARY = "Library";

	/** サムネイルルート */
	public static final String THUMBNAIL = "Thumbnail";

	/** 帳票テンプレートルート */
	public static final String REPORT = "Report" + YEN_MRK + "Template";

	/** アイコン表示 */
	public static final String ICONS = "icons";

	/** 詳細表示*/
	public static final String DETAILS = "details";

	/** Tempルート */
	public static final String TEMP = "Temp";

	/** FFmpegルート */
	public static final String FFMPEG = "FFmpeg" + YEN_MRK + "ffmpeg.exe";

	/** FFmpegコマンド */
	public static final String FFMPEGCOM = "ffmpeg";

	/** パトライト - irshルート */
	public static final String PATLITE = "PatLite" + YEN_MRK + "irsh.exe";

	/** パトライト - rshコマンド */
	public static final String PATLITECOM = "rsh";

	/** リトライ回数 */
	public static final int RETRY_COUNT = 10;

	/** 文字列分解記号用定数 */
	public static final String COMMA = ",";												//文字列を「,」で分解する時用
	public static final String DOT = ".";												//文字列を「.」で検索する時用
	public static final String UNDERSCORE = "_";										//文字列を「_」で検索する時用
	public static final String HYPHEN = "-";											//文字列を「-」で検索する時用
	public static final String SPACE = " ";												//文字列を「 」で検索する時用
	public static final String COLON = ":";												//文字列を「:」で検索する時用
	public static final String ATMARK = "@";											//文字列を「@」で検索する時用
	public static final String SEMICOLON = ";";										//文字列を「;」で検索する時用
	public static final String QUESTION = "?";											//文字列を「?」で検索する時用
	public static final String AMPERSAND = "&";										//文字列を「&」で検索する時用

	/** プログラムコード */
	public static final String RECEPTION_RECEIVECONTENTS = "0501";					//コンテンツ受信
	public static final String RECEPTION_RECEIVEADDITIONALINFO = "0502";			//付帯情報受信
	public static final String RECEPTION_GETTERMINALINFO = "0503";					//端末情報要求
	public static final String RECEPTION_GETADDITIONALITEM = "0504";				//付帯情報項目名要求

	public static final String SYS_LOGINCTRL_INIT = "5000";							//ログイン画面：システム管理：表示
	public static final String SYS_LOGINCTRL_LOGIN = "5001";							//ログイン：システム管理
	public static final String LOGWRITING_SYSLOGOUTWRITING = "5002";				//ログアウト：システム管理
	public static final String SYS_SYSTEMINFOCTRL_INIT = "5101";						//システム管理：システム情報：表示
	public static final String SYS_SYSTEMINFOCTRL_UPDATE = "5102";					//システム管理：システム情報：更新
	public static final String SYS_TENANTCTRL_INIT = "5201";							//システム管理：テナント：表示
	public static final String SYS_TENANTCTRL_ADD = "5202";							//システム管理：テナント：追加
	public static final String SYS_TENANTCTRL_UPDATE = "5203";						//システム管理：テナント：更新
	public static final String SYS_TENANTMANAGERCTRL_INIT = "5301";					//システム管理：テナント管理者：表示
	public static final String SYS_TENANTMANAGERCTRL_ADD = "5302";					//システム管理：テナント管理者：追加
	public static final String SYS_TENANTMANAGERCTRL_UPDATE = "5303";				//システム管理：テナント管理者：更新
	public static final String SYS_ACCESSEDLOGCTRL_INIT = "5401";					//システム管理：アクセスログ：表示
	public static final String SYS_ACCESSEDLOGCTRL_SEARCHACCESSLOG	 = "5402";		//システム管理：アクセスログ：ログ検索
	public static final String SYS_ACCESSEDLOGCTRL_SEARCHACCESSLOG_CSV	 = "5403";	//システム管理：アクセスログ：csvダウンロード
	public static final String SYS_RECEIVEDLOGCTRL_INIT = "5501";					//システム管理：受信ログ：表示
	public static final String SYS_RECEIVEDLOGCTRL_SEARCHRECEIVELOG = "5502";		//システム管理：受信ログ：ログ検索
	public static final String SYS_RECEIVEDLOGCTRL_SEARCHRECEIVELOG_CSV = "5503";	//システム管理：受信ログ：csvダウンロード
	public static final String LOGINCTRL_INIT = "7000";								//ログイン画面：表示
	public static final String LOGINCTRL_LOGIN = "7001";								//ログイン：メイン
	public static final String LOGWRITING_LOGOUTWRITING = "7002";					//ログアウト：メイン
	public static final String MAINCTRL_INIT = "7101";								//メイン：表示
	public static final String MAINCTRL_GETCONTENTS = "7102";						//メイン：ライブラリ展開
	public static final String MAINCTRL_SEARCHCONTENTS = "7103";						//メイン：コンテンツ検索
	public static final String MAINCTRL_ADDITIONALINFOEDIT = "7104";				//メイン：付帯情報更新
	public static final String MAINCTRL_DOWNLOAD = "7105";							//メイン：コンテンツダウンロード
	public static final String MAINCTRL_MOVE = "7106";								//メイン：コンテンツ移動
	public static final String MAINCTRL_DELETE = "7107";								//メイン：コンテンツ削除
	public static final String MAINCTRL_REPORT = "7108";								//メイン：帳票作成
	public static final String MNG_LIBRARYCTRL_INIT = "8101";						//管理：ライブラリ：表示
	public static final String MNG_LIBRARYCTRL_ADD = "8102";							//管理：ライブラリ：追加
	public static final String MNG_LIBRARYCTRL_UPDATE = "8103";						//管理：ライブラリ：更新
	public static final String MNG_LIBRARYCTRL_DELETE = "8104";						//管理：ライブラリ：削除
	public static final String MNG_TERMINALCTRL_INIT = "8201";						//管理：端末：表示
	public static final String MNG_TERMINALCTRL_ADD = "8202";						//管理：端末：追加
	public static final String MNG_TERMINALCTRL_UPDATE = "8203";						//管理：端末：更新
	public static final String MNG_USERINFOCTRL_INIT = "8301";						//管理：ユーザー：表示
	public static final String MNG_USERINFOCTRL_ADD = "8302";						//管理：ユーザー：追加
	public static final String MNG_USERINFOCTRL_UPDATE = "8303";						//管理：ユーザー：更新
	public static final String MNG_ADDITIONALITEMCTRL_INIT = "8401";				//管理：付帯情報項目名：表示
	public static final String MNG_ADDITIONALITEMCTRL_ADD = "8402";					//管理：付帯情報項目名：追加
	public static final String MNG_ADDITIONALITEMCTRL_UPDATE = "8403";				//管理：付帯情報項目名：更新
	public static final String MNG_REPORTPARAMCTRL_INIT = "8501";					//管理：帳票登録：表示
	public static final String MNG_REPORTPARAMCTRL_ADD = "8502";						//管理：帳票登録：追加
	public static final String MNG_REPORTPARAMCTRL_UPDATE = "8503";					//管理：帳票登録：更新
	public static final String MNG_REPORTPARAMCTRL_DOWNLOAD = "8504";				//管理：帳票登録：帳票ダウンロード
	public static final String MNG_ACCESSEDLOGCTRL_INIT = "8601";					//管理：アクセスログ：表示
	public static final String MNG_ACCESSEDLOGCTRL_SEARCHACCESSLOG = "8602";		//管理：アクセスログ：ログ検索
	public static final String MNG_ACCESSEDLOGCTRL_SEARCHACCESSLOG_CSV = "8603";	//管理：アクセスログ：csvダウンロード
	public static final String MNG_RECEIVEDLOGCTRL_INIT = "8701";					//管理：受信ログ：表示
	public static final String MNG_RECEIVEDLOGCTRL_SEARCHRECEIVELOG = "8702";		//管理：受信ログ：ログ検索
	public static final String MNG_RECEIVEDLOGCTRL_SEARCHRECEIVELOG_CSV = "8703";	//管理：受信ログ：csvダウンロード

}
