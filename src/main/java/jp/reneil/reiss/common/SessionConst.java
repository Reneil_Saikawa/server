/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/23      1.1.0           女屋            定数編集
 */
package jp.reneil.reiss.common;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * セッション関連定数クラスです。<br>
 *
 * @version 1.0.0
 */
public class SessionConst {

	/** 属性ID：ユーザID */
	public static final String USER_ID = "USER_ID";

	/** 属性ID：権限コード */
	public static final String AUTH_CODE = "AUTH_CODE";

	/** 属性ID：テナントID */
	public static final String TENANT_ID = "TENANT_ID";

	/** 属性ID：帳票ID */
	public static final String REPORT_ID = "REPORT_ID";

	/** 属性ID；ユーザのルートパス */
	public static final String USER_ROOT_PATH = "USER_ROOT_PATH";

	/** 属性ID；ユーザの現在パス */
	public static final String PWD_PATH = "PWD_PATH";

	/** 属性ID；リクエストファイル */
	public static final String REQ_FILE = "REQ_FILE";

	/** 属性ID；リクエストディレクトリ */
	public static final String REQ_DIR = "REQ_DIR";
}
