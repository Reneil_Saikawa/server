/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込み変更。TripleDES変換クラス宣言(インスタンス化)変更。
 *    3    2019/10/23      1.0.2           女屋            不要メンバ変数削除と修正
 *    4    2020/02/20      1.0.3           女屋            帳票有効化機能追加
 */
package jp.reneil.reiss.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvGenerator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.CodeDao;
import jp.reneil.reiss.dao.ReceiveLogDao;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.TenantDao;
import jp.reneil.reiss.dao.TerminalDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.exception.ExException;
import jp.reneil.reiss.form.MngReceivedLogForm;
import jp.reneil.reiss.log.LogWriting;
import jp.reneil.reiss.model.ReceiveLogCsvModel;
import jp.reneil.reiss.model.ReceiveLogModel;
import jp.reneil.reiss.model.TenantModel;
import jp.reneil.reiss.model.UserInfoModel;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザ管理_受診ログ_画面コントローラクラスです。<br>
 *
 * @version 1.0.3
 */
@Controller
@RequestMapping(value="mng_receivedlog")
public class MngReceivedLogCtrl {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(MngReceivedLogCtrl.class);

	/** ログ書き込みクラス */
	@Autowired
	private LogWriting logWriting;

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** ユーザー情報テーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/** テナント情報テーブルアクセス制御クラス */
	@Autowired
	private TenantDao tenantDao;

	/** 端末情報テーブルアクセス制御クラス */
	@Autowired
	private TerminalDao terminalDao;

	/** コードテーブルアクセス制御クラス */
	@Autowired
	private CodeDao codeDao;

	/** 受信ログテーブルアクセス制御クラス */
	@Autowired
	private ReceiveLogDao receiveLogDao;

	/** 画面表示
	 * @throws Exception */
	@RequestMapping(method=GET)
	public String init(@ModelAttribute("mngReceivedLogForm") MngReceivedLogForm mngReceivedLogForm, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MNG_RECEIVEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MNG_RECEIVEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}

		// セッション情報からログインユーザIDを取得
		String user_id = (String) session.getAttribute(SessionConst.USER_ID);
		log.info("@@MngReceivedLogCtrl.init セッション情報取得[USER_ID=" + user_id + "]");
		//セッション情報からログインユーザーのテナントIDを取得
		String strTenantid = (String)session.getAttribute(SessionConst.TENANT_ID);
		log.info("@@MngReceivedLogCtrl.init セッション情報取得[TENANT_ID=" + strTenantid + "]");

		// テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);

		// 帳票有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
			model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
		}

		// システム名
		model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());
		// 暗号化ユーザID
		model.addAttribute("acc_userID", tripleDesConverter.encrypt64(user_id));
		// テナントID取得
		model.addAttribute("tenantId", strTenantid);
		// 端末情報取得
		model.addAttribute("term", terminalDao.findTenantAllTerm(strTenantid));
		// 受信区分
		model.addAttribute("receive", codeDao.getCode01Data("05"));
		// 処理結果
		model.addAttribute("result", codeDao.getCode01Data("00"));

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_RECEIVEDLOGCTRL_INIT, CommonConst.SUCCESS_CO, "", "");

		return "mng_receivedlog";
	}

	/**
	 * 受信ログ検索<br>
	 * @param startDate 検索開始日時
	 * @param endDate 検索終了日時
	 * @param tenantId テナントID
	 * @param termId 端末ID
	 * @param receiveCode 受信区分コード
	 * @param resultCode 処理結果コード
	 * @param request リクエスト情報
	 * @return 受信ログ リストModelオブジェクト
	 * @throws Exception
	 */
	@RequestMapping(params="searchReceivelog", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String searchReceivelog(@RequestParam String acc_userID, String startDate, String endDate, String tenantId, String termId, String receiveCode, String resultCode, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_RECEIVEDLOGCTRL_SEARCHRECEIVELOG, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_RECEIVEDLOGCTRL_SEARCHRECEIVELOG, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_RECEIVEDLOGCTRL_SEARCHRECEIVELOG, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		String json = null;

		//受信ログ情報リスト
		List<ReceiveLogModel> ReceiveLogList = new ArrayList<ReceiveLogModel>();

		ReceiveLogList.addAll(receiveLogDao.find(startDate, endDate, tenantId, termId, receiveCode, resultCode));

		if (ReceiveLogList.isEmpty() ==false) {
			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(ReceiveLogList);
		}

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_RECEIVEDLOGCTRL_SEARCHRECEIVELOG, CommonConst.SUCCESS_CO, "検索期間 = " + startDate + " ～ " + endDate + "\n検索端末ID = " + termId + "\n検索受信区分コード = " + receiveCode + "\n検索処理結果コード = " + resultCode, "");

		return json;
	}

	/**
	 * CSVダウンロード<br>
	 * @param mngReceivedLogForm 受信ログ画面フォーム
	 * @param request リクエスト情報
	 * @param respons レスポンス情報
	 * @return 受信ログCSVファイルレスポンス
	 * @throws Exception
	 */
	@RequestMapping(params="searchReceivelogCsv", method=RequestMethod.POST, produces = "text/html;charset=UTF-8")
	public void searchReceivelogCsv(@ModelAttribute("mngReceivedLogForm") MngReceivedLogForm mngReceivedLogForm, HttpServletRequest request, HttpServletResponse response) throws Exception {

		// ユーザIDを取得
		String user_id = tripleDesConverter.decrypt64(mngReceivedLogForm.getAcc_userID());
		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(user_id);
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_RECEIVEDLOGCTRL_SEARCHRECEIVELOG_CSV, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_RECEIVEDLOGCTRL_SEARCHRECEIVELOG_CSV, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(user_id)) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_RECEIVEDLOGCTRL_SEARCHRECEIVELOG_CSV, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		log.info("@@MngReceivedLogCtrl.searchReceivelogCsv ユーザID = " + user_id);

		// ログ出力範囲を設定
		String start_date = "1970/01/01";
		String end_date = "9999/12/31";
		if (!StringUtils.isEmpty(mngReceivedLogForm.getStart_date())) {
			start_date = mngReceivedLogForm.getStart_date();
		}
		if (!StringUtils.isEmpty(mngReceivedLogForm.getEnd_date())) {
			end_date = mngReceivedLogForm.getEnd_date();
		}

		// ファイル名
		String reportName = null;
		// ダウンロードファイル名文字コード変更用
		String downloadFileName = null;

		try {

			// ダウンロードファイル名作成
			SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			reportName = systemInfoDao.findAll().getSystem_name() + "受信ログ" + CommonConst.HYPHEN + start_date.replaceAll("[^0-9]","") + "～" + end_date.replaceAll("[^0-9]","") + CommonConst.HYPHEN + user_id + fmt.format(new Date()) + CommonConst.CSV;
			// URLエンコード
	        try {
	        	downloadFileName = URLEncoder.encode(reportName, "UTF-8");
			}catch (UnsupportedEncodingException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			// 受信ログ(csv)
			List<ReceiveLogCsvModel> receiveLogCsvModel = receiveLogDao.findCsv(start_date, end_date, strTenantid, mngReceivedLogForm.getTerm_id(), mngReceivedLogForm.getReceive_code(), mngReceivedLogForm.getResult_code());

			CsvMapper mapper = new CsvMapper();
			// データをダブルクォーテーションで囲う
			mapper.configure(CsvGenerator.Feature.ALWAYS_QUOTE_STRINGS, true);

			// ヘッダを付ける
			CsvSchema schema = mapper.schemaFor(ReceiveLogCsvModel.class).withHeader();
			String csvText = mapper.writer(schema).writeValueAsString(receiveLogCsvModel);

			// ファイル書き込み
			response.setContentType("application/octet-stream;charset=MS932");
			response.setHeader("Content-Disposition", "attachment; filename=" + downloadFileName);

			// CSVファイル出力
			try (PrintWriter pw = response.getWriter()) {
	            pw.print(csvText);
	        }

			//アクセスログ書き込み
			StringBuilder msg = new StringBuilder();
			msg.append("検索期間 = ");
			msg.append(start_date);
			msg.append(" ～ ");
			msg.append(end_date);
			msg.append("\n検索端末ID = ");
			msg.append(mngReceivedLogForm.getTerm_id());
			msg.append("\n検索受信区分コード = ");
			msg.append(mngReceivedLogForm.getReceive_code());
			msg.append("\n検索処理結果コード = ");
			msg.append(mngReceivedLogForm.getResult_code());
			msg.append("\ncsvファイル名 = ");
			msg.append(reportName);
			log.info("@@MngReceivedLogCtrl.searchReceivelogCsv csvダウンロード実行：" + "検索期間 = " + start_date + " ～ " + end_date + ", 検索端末ID = " + mngReceivedLogForm.getTerm_id() + ", 検索受信区分コード = " + mngReceivedLogForm.getReceive_code() + ", 検索処理結果コード = " + mngReceivedLogForm.getResult_code() + ", csvファイル名 = " + reportName);
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_RECEIVEDLOGCTRL_SEARCHRECEIVELOG_CSV, CommonConst.SUCCESS_CO, msg.toString(), "");

		}catch(Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			String str = sw.toString();
			//アクセスログ書き込み
			StringBuilder msg = new StringBuilder();
			msg.append("検索期間 = ");
			msg.append(start_date);
			msg.append(" ～ ");
			msg.append(end_date);
			msg.append("\n検索端末ID = ");
			msg.append(mngReceivedLogForm.getTerm_id());
			msg.append("\n検索受信区分コード = ");
			msg.append(mngReceivedLogForm.getReceive_code());
			msg.append("\n検索処理結果コード = ");
			msg.append(mngReceivedLogForm.getResult_code());
			msg.append("\ncsvファイル名 = ");
			msg.append(reportName);
			log.info("@@MngReceivedLogCtrl.searchReceivelogCsv csvダウンロード失敗：" + "検索期間 = " + start_date + " ～ " + end_date + ", 検索端末ID = " + mngReceivedLogForm.getTerm_id() + ", 検索受信区分コード = " + mngReceivedLogForm.getReceive_code() + ", 検索処理結果コード = " + mngReceivedLogForm.getResult_code() + ", csvファイル名 = " + reportName + "\n" + str);
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_RECEIVEDLOGCTRL_SEARCHRECEIVELOG_CSV, CommonConst.FAILED_CO, msg.toString(), str);
		}
	}

	/**
	 * 受信ログ検索（受信番号から検索）<br>
	 * @param recvNo 受信番号
	 * @return 受信ログ Modelオブジェクト
	 * @throws Exception
	 */
	@RequestMapping(params="recvNoSelect", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String recvNoSelect(@RequestParam String acc_userID, String recvNo, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_RECEIVEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_RECEIVEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_RECEIVEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		String json = null;

		ReceiveLogModel receivelog = receiveLogDao.recvNoSelect(recvNo);

		if (receivelog != null) {
			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(receivelog);
		}

		return json;
	}

}