/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込み変更。TripleDES変換クラス宣言(インスタンス化)変更。
 *    3    2019/10/23      1.0.2           女屋            不要メンバ変数削除と修正
 *    4    2019/10/24      1.0.3           女屋            パスワードエラーカウント更新機能追加
 */
package jp.reneil.reiss.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.TenantDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.exception.ExException;
import jp.reneil.reiss.form.SystemManageTenantManagerForm;
import jp.reneil.reiss.log.LogWriting;
import jp.reneil.reiss.model.UserInfoModel;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * システム管理_テナント管理者_画面コントローラクラスです。<br>
 *
 * @version 1.0.3
 */
@Controller
@RequestMapping(value="systemManage/tenant_manager")
public class SystemManageTenantManagerCtrl {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(SystemManageTenantManagerCtrl.class);

	/** ログ書き込みクラス */
	@Autowired
	private LogWriting logWriting;

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** ユーザー情報テーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/** テナントテーブルアクセス制御クラス */
	@Autowired
	private TenantDao tenantDao;

	/** 画面表示
	 * @throws Exception */
	@RequestMapping(method=GET)
	public String init(@ModelAttribute("systemManageTenantManagerForm") SystemManageTenantManagerForm systemManageTenantManagerForm, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_TENANTMANAGERCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_TENANTMANAGERCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) != CommonConst.SYSTEM_MANAGER) {
			log.info("@@SystemManageTenantManagerCtrl.init アクセス権限違反[USER_ID=" + SessionConst.USER_ID + "]");
			throw new ExException(MessageConst.W015);
		}

		// セッション情報からログインユーザIDを取得
		String user_id = (String) session.getAttribute(SessionConst.USER_ID);
		log.info("@@SystemManageTenantManagerCtrl.init セッション情報取得[USER_ID=" + user_id + "]");

		// システム名
		model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());
		// 暗号化ユーザID
		model.addAttribute("acc_userID", tripleDesConverter.encrypt64(user_id));
		// テナント管理者情報取得
		model.addAttribute("list", userInfoDao.findTenantManager());
		// テナント情報取得
		model.addAttribute("tenant", tenantDao.findAll());

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), "null", user_id, CommonConst.SYS_TENANTMANAGERCTRL_INIT, CommonConst.SUCCESS_CO, "", "");

		// 画面表示
		return "systemManage/tenant_manager";
	}

	/**
	 * 追加ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="add", method=POST)
	public String add(@ModelAttribute("systemManageTenantManagerForm") SystemManageTenantManagerForm systemManageTenantManagerForm, BindingResult result, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_TENANTMANAGERCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_TENANTMANAGERCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) != CommonConst.SYSTEM_MANAGER) {
			log.info("@@SystemManageTenantManagerCtrl.add アクセス権限違反[USER_ID=" + SessionConst.USER_ID + "]");
			throw new ExException(MessageConst.W015);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(systemManageTenantManagerForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageTenantManagerForm.getAcc_userID()), CommonConst.SYS_TENANTMANAGERCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		// フォームからモデルに転記
		UserInfoModel tenantManageModel = setTenantManageModel(systemManageTenantManagerForm);

		// フォームのユーザーIDが既にテーブルにあるか確認
		if (userInfoDao.count(systemManageTenantManagerForm.getUser_id()) == 0 ) {
			// 存在しないとき（追加）
			userInfoDao.insert(tenantManageModel);

			log.info("@@SystemManageTenantManagerCtrl.add テナント管理者情報テーブルを追加しました(テナントID = " + tenantManageModel.getTenant_id()
					+ ", テナント管理者ID = " + tenantManageModel.getUser_id()
					+ ", テナント管理者名 = " + tenantManageModel.getUser_name()
					+ ", 有効期限 = " + systemManageTenantManagerForm.getStart_date() + " ～ " + systemManageTenantManagerForm.getEnd_date()
					+ ", 削除フラグ = " + tenantManageModel.getDel_flg() + ")");

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageTenantManagerForm.getAcc_userID()), CommonConst.SYS_TENANTMANAGERCTRL_ADD, CommonConst.SUCCESS_CO,
					  "テナントID = " + tenantManageModel.getTenant_id()
					+ "\nテナント管理者ID = " + tenantManageModel.getUser_id()
					+ "\nテナント管理者名 = " + tenantManageModel.getUser_name()
					+ "\n有効期限 = " + systemManageTenantManagerForm.getStart_date() + " ～ " + systemManageTenantManagerForm.getEnd_date()
					+ "\n削除フラグ = " + tenantManageModel.getDel_flg(), "");

			// successメッセージ
			model.addAttribute("success", MessageConst.M001);
		} else {
			// 存在するとき

			log.info("@@SystemManageTenantManagerCtrl.add " + MessageConst.W530 + "(テナントID = " + tenantManageModel.getTenant_id()
					+ ", テナント管理者ID = " + tenantManageModel.getUser_id()
					+ ", テナント管理者名 = " + tenantManageModel.getUser_name()
					+ ", 有効期限 = " + systemManageTenantManagerForm.getStart_date() + " ～ " + systemManageTenantManagerForm.getEnd_date()
					+ ", 削除フラグ = " + tenantManageModel.getDel_flg() + ")");

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageTenantManagerForm.getAcc_userID()), CommonConst.SYS_TENANTMANAGERCTRL_ADD, CommonConst.FAILED_CO,
					  "テナントID = " + tenantManageModel.getTenant_id()
					+ "\nテナント管理者ID = " + tenantManageModel.getUser_id()
					+ "\nテナント管理者名 = " + tenantManageModel.getUser_name()
					+ "\n有効期限 = " + systemManageTenantManagerForm.getStart_date() + " ～ " + systemManageTenantManagerForm.getEnd_date()
					+ "\n削除フラグ = " + tenantManageModel.getDel_flg(), MessageConst.W530);

			// warningメッセージ
			model.addAttribute("add_danger", MessageConst.W530);
		}

		// システム名
		model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());
		// テナント管理者情報取得
		model.addAttribute("list", userInfoDao.findTenantManager());
		// テナント情報取得 続けて選択ができるようにする
		model.addAttribute("tenant", tenantDao.findAll());
		// メッセージをセット
		model.addAttribute("message", MessageConst.M001);

		return "systemManage/tenant_manager";
	}

	/**
	 * 更新ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="update", method=POST)
	public String update(@ModelAttribute("systemManageTenantManagerForm") SystemManageTenantManagerForm systemManageTenantManagerForm, BindingResult result, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_TENANTMANAGERCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_TENANTMANAGERCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) != CommonConst.SYSTEM_MANAGER) {
			log.info("@@SystemManageTenantManagerCtrl.update アクセス権限違反[USER_ID=" + SessionConst.USER_ID + "]");
			throw new ExException(MessageConst.W015);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(systemManageTenantManagerForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageTenantManagerForm.getAcc_userID()), CommonConst.SYS_TENANTMANAGERCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		// フォームからモデルに転記
		UserInfoModel tenantManageModel = setTenantManageModel(systemManageTenantManagerForm);

		// フォームのユーザーIDが既にテーブルにあるか確認
		if (userInfoDao.count(systemManageTenantManagerForm.getUser_id()) == 0 ) {
			// 存在しないとき（追加）
			userInfoDao.insert(tenantManageModel);

			log.info("@@SystemManageTenantManagerCtrl.update テナント管理者情報テーブルを追加しました(テナントID = " + tenantManageModel.getTenant_id()
					+ ", テナント管理者ID = " + tenantManageModel.getUser_id()
					+ ", テナント管理者名 = " + tenantManageModel.getUser_name()
					+ ", 有効期限 = " + systemManageTenantManagerForm.getStart_date() + " ～ " + systemManageTenantManagerForm.getEnd_date()
					+ ", 削除フラグ = " + tenantManageModel.getDel_flg() + ")");

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageTenantManagerForm.getAcc_userID()), CommonConst.SYS_TENANTMANAGERCTRL_ADD, CommonConst.SUCCESS_CO,
					  "テナントID = " + tenantManageModel.getTenant_id()
					+ "\nテナント管理者ID = " + tenantManageModel.getUser_id()
					+ "\nテナント管理者名 = " + tenantManageModel.getUser_name()
					+ "\n有効期限 = " + systemManageTenantManagerForm.getStart_date() + " ～ " + systemManageTenantManagerForm.getEnd_date()
					+ "\n削除フラグ = " + tenantManageModel.getDel_flg(), "");

		} else {
			// 存在するとき（更新）
			userInfoDao.update(tenantManageModel);

			log.info("@@SystemManageTenantManagerCtrl.update テナント管理者情報テーブルを更新しました(テナントID = " + tenantManageModel.getTenant_id()
					+ ", テナント管理者ID = " + tenantManageModel.getUser_id()
					+ ", テナント管理者名 = " + tenantManageModel.getUser_name()
					+ ", パスワードエラーカウント = " + tenantManageModel.getPwd_errcnt()
					+ ", 有効期限 = " + systemManageTenantManagerForm.getStart_date() + " ～ " + systemManageTenantManagerForm.getEnd_date()
					+ ", 削除フラグ = " + tenantManageModel.getDel_flg() + ")");

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageTenantManagerForm.getAcc_userID()), CommonConst.SYS_TENANTMANAGERCTRL_UPDATE, CommonConst.SUCCESS_CO,
					  "テナントID = " + tenantManageModel.getTenant_id()
					+ "\nテナント管理者ID = " + tenantManageModel.getUser_id()
					+ "\nテナント管理者名 = " + tenantManageModel.getUser_name()
					+ "\nパスワードエラーカウント = " + tenantManageModel.getPwd_errcnt()
					+ "\n有効期限 = " + systemManageTenantManagerForm.getStart_date() + " ～ " + systemManageTenantManagerForm.getEnd_date()
					+ "\n削除フラグ = " + tenantManageModel.getDel_flg(), "");

		}

		// システム名
		model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());
		// successメッセージ
		model.addAttribute("success", MessageConst.M001);
		// テナント管理者情報取得
		model.addAttribute("list", userInfoDao.findTenantManager());
		// テナント情報取得 続けて選択ができるようにする
		model.addAttribute("tenant", tenantDao.findAll());

		return "systemManage/tenant_manager";
	}

	/**
	 * フォームからモデルに入力情報を展開します。
	 * @param form システム管理_テナント管理者_画面フォーム
	 * @return テナント管理者モデル
	 * @throws Exception
	 */
	private UserInfoModel setTenantManageModel(SystemManageTenantManagerForm form) throws Exception {

		UserInfoModel model = new UserInfoModel();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");

		//ユーザーID
		model.setUser_id(form.getUser_id());
		//パスワード(暗号化)
		model.setPwd(tripleDesConverter.encrypt64(form.getPassword()));
		//パスワードエラーカウント
		model.setPwd_errcnt(form.getPwd_errcnt());
		//ユーザー名
		model.setUser_name(form.getUser_name());
		// テナントID
		model.setTenant_id(form.getTenant_id());
		// 有効開始日
		model.setStart_date(fmt.parse(form.getStart_date()));
		// 有効終了日
		model.setEnd_date(fmt.parse(form.getEnd_date()));
		// 削除フラグ
		if (form.getDel_flg()) {
			model.setDel_flg((byte) 1);
		} else {
			model.setDel_flg((byte) 0);
		}
		//権限情報
		model.setAuth_cd(CommonConst.TENANT_MANAGER_CD);
		// 更新ユーザーID
		model.setUpdate_user_id(CommonConst.SYSTEM_MANAGER);

		return model;
	}

}