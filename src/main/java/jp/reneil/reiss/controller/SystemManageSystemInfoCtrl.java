/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込み変更。TripleDES変換クラス宣言(インスタンス化)変更。
 *    3    2019/10/23      1.0.2           女屋            不要メンバ変数削除と修正
 */
package jp.reneil.reiss.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.exception.ExException;
import jp.reneil.reiss.form.SystemManageSystemInfoForm;
import jp.reneil.reiss.log.LogWriting;
import jp.reneil.reiss.model.SystemInfoModel;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * システム管理_システム情報_画面コントローラクラスです。<br>
 *
 * @version 1.0.2
 */
@Controller
@RequestMapping(value="systemManage/systeminfo")
public class SystemManageSystemInfoCtrl {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(SystemManageLoginCtrl.class);

	/** ログ書き込みクラス */
	@Autowired
	private LogWriting logWriting;

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** 画面表示
	 * @throws Exception */
	@RequestMapping(method=GET)
	public String init(@ModelAttribute("systemManageSystemInfoForm") SystemManageSystemInfoForm systemManageSystemInfoForm, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_SYSTEMINFOCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_SYSTEMINFOCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) != CommonConst.SYSTEM_MANAGER) {
			log.info("@@SystemManageSystemInfoCtrl.init アクセス権限違反[USER_ID=" + SessionConst.USER_ID + "]");
			throw new ExException(MessageConst.W015);
		}

		// セッション情報からログインユーザIDを取得
		String user_id = (String) session.getAttribute(SessionConst.USER_ID);
		log.info("@@SystemManageSystemInfoCtrl.init セッション情報取得[USER_ID=" + user_id + "]");

		// システム情報取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();

		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());
		// 暗号化ユーザID
		model.addAttribute("acc_userID", tripleDesConverter.encrypt64(user_id));

		// フォームクラスにセット
		systemManageSystemInfoForm.setSystem_name(systeminfoModel.getSystem_name());
		systemManageSystemInfoForm.setRootPath(systeminfoModel.getSystem_rootpath());
		systemManageSystemInfoForm.setInUrl(systeminfoModel.getSystem_in_url());
		systemManageSystemInfoForm.setOutUrl(systeminfoModel.getSystem_out_url());

		if (!StringUtils.isEmpty(systeminfoModel.getDirectory_image())) {
			// 画像ファイルコメント
			model.addAttribute("comment", "下記画像を登録済みです");
			// base64ディレクトリ画像
			model.addAttribute("data_src", systeminfoModel.getDirectory_image());
		}

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), "null", user_id, CommonConst.SYS_SYSTEMINFOCTRL_INIT, CommonConst.SUCCESS_CO, "", "");

		return "systemManage/systeminfo";
	}

	/**
	 * 更新ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="update", method=POST)
	public String update(@ModelAttribute("systemManageSystemInfoForm") SystemManageSystemInfoForm systemManageSystemInfoForm, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_SYSTEMINFOCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_SYSTEMINFOCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) != CommonConst.SYSTEM_MANAGER) {
			log.info("@@SystemManageSystemInfoCtrl.update アクセス権限違反[USER_ID=" + SessionConst.USER_ID + "]");
			throw new ExException(MessageConst.W015);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(systemManageSystemInfoForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageSystemInfoForm.getAcc_userID()), CommonConst.SYS_SYSTEMINFOCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		// システム情報を更新
		systemInfoDao.update(systemManageSystemInfoForm.getSystem_name(), systemManageSystemInfoForm.getRootPath(), systemManageSystemInfoForm.getInUrl(), systemManageSystemInfoForm.getOutUrl(), systemManageSystemInfoForm.getData_src());

		// システム情報取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();

		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());

		// base64ディレクトリ画像
		model.addAttribute("data_src", systeminfoModel.getDirectory_image());

		// 画面にメッセージをセット
		model.addAttribute("message", MessageConst.M001);

		log.info("@@SystemManageSystemInfoCtrl.update システム情報テーブルを更新しました(システム名 = " + systemManageSystemInfoForm.getSystem_name()
				+ ", システムルートパス = " + systemManageSystemInfoForm.getRootPath()
				+ ", 内部URL = " + systemManageSystemInfoForm.getInUrl()
				+ ", 外部URL = " + systemManageSystemInfoForm.getOutUrl());

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageSystemInfoForm.getAcc_userID()), CommonConst.SYS_SYSTEMINFOCTRL_UPDATE, CommonConst.SUCCESS_CO,
				  "システム名 = " + systemManageSystemInfoForm.getSystem_name()
				+ "\nシステムルートパス = " + systemManageSystemInfoForm.getRootPath()
				+ "\n内部URL = " + systemManageSystemInfoForm.getInUrl()
				+ "\n外部URL = " + systemManageSystemInfoForm.getOutUrl(), "");

		return "systemManage/systeminfo";
	}

}