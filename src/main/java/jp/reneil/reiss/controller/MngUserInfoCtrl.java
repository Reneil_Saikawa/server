/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込み変更。TripleDES変換クラス宣言(インスタンス化)変更。
 *    3    2019/10/23      1.0.2           女屋            不要メンバ変数削除と修正
 *    4    2019/10/24      1.0.3           女屋            パスワードエラーカウント更新機能追加
 *    5    2020/02/20      1.0.4           女屋            帳票有効化機能追加
 */
package jp.reneil.reiss.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.LibraryAuthDao;
import jp.reneil.reiss.dao.LibraryDao;
import jp.reneil.reiss.dao.ReportAuthDao;
import jp.reneil.reiss.dao.ReportParamDao;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.TenantDao;
import jp.reneil.reiss.dao.UserAuthDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.exception.ExException;
import jp.reneil.reiss.form.MngUserInfoForm;
import jp.reneil.reiss.log.LogWriting;
import jp.reneil.reiss.model.Directory;
import jp.reneil.reiss.model.LibraryAuthModel;
import jp.reneil.reiss.model.LibraryModel;
import jp.reneil.reiss.model.Node;
import jp.reneil.reiss.model.ReportAuthModel;
import jp.reneil.reiss.model.SystemInfoModel;
import jp.reneil.reiss.model.TenantModel;
import jp.reneil.reiss.model.UserAuthModel;
import jp.reneil.reiss.model.UserInfoModel;
import jp.reneil.reiss.util.FileUtil;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザ管理_ユーザー_画面コントローラクラスです。<br>
 *
 * @version 1.0.4
 */
@Controller
@RequestMapping(value="mng_userinfo")
public class MngUserInfoCtrl {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(MngUserInfoCtrl.class);

	/** ログ書き込みクラス */
	@Autowired
	private LogWriting logWriting;

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** テナント情報テーブルアクセス制御クラス */
	@Autowired
	private TenantDao tenantDao;

	/** ユーザ情報テーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/** ユーザ権限テーブルアクセス制御クラス */
	@Autowired
	private UserAuthDao userAuthDao;

	/** ライブラリ情報テーブルアクセス制御クラス */
	@Autowired
	private LibraryDao librarydao;

	/** 帳票パラメータテーブルアクセス制御クラス */
	@Autowired
	private ReportParamDao reportParamDao;

	/** 帳票権限テーブルアクセス制御クラス */
	@Autowired
	private ReportAuthDao reportAuthDao;

	/** ライブラリ権限テーブルアクセス制御クラス */
	@Autowired
	private LibraryAuthDao libraryAuthDao;

	/** 画面表示
	 * @throws Exception */
	@RequestMapping(method=GET)
	public String init(@ModelAttribute("mngUserForm") MngUserInfoForm mngUserForm, Model model,  HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MNG_USERINFOCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MNG_USERINFOCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}

		// セッション情報からログインユーザIDを取得
		String user_id = (String) session.getAttribute(SessionConst.USER_ID);
		log.info("@@MngUserInfoCtrl.init セッション情報取得[USER_ID=" + user_id + "]");
		//セッション情報からログインユーザーのテナントIDを取得
		String strTenantid = (String)session.getAttribute(SessionConst.TENANT_ID);
		log.info("@@MngUserInfoCtrl.init セッション情報取得[TENANT_ID=" + strTenantid + "]");

		// テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);

		// 帳票有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
			model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
			// 帳票パラメータデータ
			model.addAttribute("report", reportParamDao.findTenant(strTenantid));
		}

		// システム情報を取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();

		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());
		// 暗号化ユーザID
		model.addAttribute("acc_userID", tripleDesConverter.encrypt64(user_id));
		// テナント有効期限取得
		model.addAttribute("tenantStart", tenantModel.getStr_start_date());
		model.addAttribute("tenantEnd", tenantModel.getStr_end_date());
		// システムルート＋サムネイルルート＋テナントID
		StringBuilder sb = new StringBuilder(systeminfoModel.getSystem_rootpath());
		sb.append(CommonConst.YEN_MRK);
		sb.append(CommonConst.THUMBNAIL);
		sb.append(CommonConst.YEN_MRK);
		sb.append(strTenantid);
		// ライブラリツリーデータ
		model.addAttribute("libraryTreeData", getLibraryTreeData(sb.toString(), strTenantid));
		// ユーザーリスト
		model.addAttribute("user", userInfoDao.findTenantGeneralUser(strTenantid));

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_USERINFOCTRL_INIT, CommonConst.SUCCESS_CO, "", "");

		return "mng_userinfo";
	}

	/**
	 * 追加ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="add", method=POST)
	public String add(@ModelAttribute("mngUserForm") MngUserInfoForm mngUserForm, BindingResult result, Model model, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザのテナントID設定
		String strTenantid = userInfoDao.getUser(tripleDesConverter.decrypt64(mngUserForm.getAcc_userID())).getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()), CommonConst.MNG_USERINFOCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()), CommonConst.MNG_USERINFOCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()), CommonConst.MNG_USERINFOCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		// テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);

		// フォームからモデルに転記
		UserInfoModel userinfoModel = setUserModel(mngUserForm, strTenantid);

		// ユーザーライセンス数確認
		if (tenantModel.getUser_license() > userInfoDao.licenseCount(strTenantid)) {
			// ライセンス範囲内
			// フォームのユーザーIDが既にテーブルにあるか確認
			if (userInfoDao.count(userinfoModel.getUser_id()) == 0 ) {
				// 存在しないとき（追加）
				userInfoDao.insert(userinfoModel);
				// ユーザ権限更新
				updateUserAuth(mngUserForm);
				// ライブラリ権限更新
				updateLibraryAuth(mngUserForm, strTenantid);
				// 帳票有効化確認
				if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
					// 帳票権限更新
					updateReportAuth(mngUserForm, strTenantid);
				}

				log.info("@@MngUserInfoCtrl.add ユーザー情報テーブルを追加しました(テナントID = " + strTenantid
						+ ", ユーザーID = " + userinfoModel.getUser_id()
						+ ", ユーザー名 = " + userinfoModel.getUser_name()
						+ ", 閲覧ライブラリID = " + Arrays.toString(mngUserForm.getLib_id())
						+ ", 管理権限 = " + mngUserForm.getManage_auth()
						+ ", 帳票出力権限 = " + mngUserForm.getOutput_auth()
						+ ", 付帯情報編集権限 = " + mngUserForm.getEdit_auth()
						+ ", ダウンロード権限 = " + mngUserForm.getDownload_auth()
						+ ", 移動権限 = " + mngUserForm.getMove_auth()
						+ ", 削除権限 = " + mngUserForm.getDelete_auth()
						+ ", 有効期限 = " + mngUserForm.getStart_date() + " ～ " + mngUserForm.getEnd_date()
						+ ", 削除フラグ = " + userinfoModel.getDel_flg() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()), CommonConst.MNG_USERINFOCTRL_ADD, CommonConst.SUCCESS_CO,
						  "ユーザーID = " + userinfoModel.getUser_id()
						+ "\nユーザー名 = " + userinfoModel.getUser_name()
						+ "\n閲覧ライブラリID = " + Arrays.toString(mngUserForm.getLib_id())
						+ "\n管理権限 = " + mngUserForm.getManage_auth()
						+ "\n帳票出力権限 = " + mngUserForm.getOutput_auth()
						+ "\n付帯情報編集権限 = " + mngUserForm.getEdit_auth()
						+ "\nダウンロード権限 = " + mngUserForm.getDownload_auth()
						+ "\n移動権限 = " + mngUserForm.getMove_auth()
						+ "\n削除権限 = " + mngUserForm.getDelete_auth()
						+ "\n有効期限 = " + mngUserForm.getStart_date() + " ～ " + mngUserForm.getEnd_date()
						+ "\n削除フラグ = " + userinfoModel.getDel_flg(), "");

				//successメッセージ
				model.addAttribute("success", MessageConst.M001);
			} else {
				// 存在するとき

				log.info("@@MngUserInfoCtrl.add " + MessageConst.W830 + "(テナントID = " + strTenantid
						+ ", ユーザーID = " + userinfoModel.getUser_id()
						+ ", ユーザー名 = " + userinfoModel.getUser_name()
						+ ", 閲覧ライブラリID = " + Arrays.toString(mngUserForm.getLib_id())
						+ ", 管理権限 = " + mngUserForm.getManage_auth()
						+ ", 帳票出力権限 = " + mngUserForm.getOutput_auth()
						+ ", 付帯情報編集権限 = " + mngUserForm.getEdit_auth()
						+ ", ダウンロード権限 = " + mngUserForm.getDownload_auth()
						+ ", 移動権限 = " + mngUserForm.getMove_auth()
						+ ", 削除権限 = " + mngUserForm.getDelete_auth()
						+ ", 有効期限 = " + mngUserForm.getStart_date() + " ～ " + mngUserForm.getEnd_date()
						+ ", 削除フラグ = " + userinfoModel.getDel_flg() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()), CommonConst.MNG_USERINFOCTRL_ADD, CommonConst.FAILED_CO,
						  "ユーザーID = " + userinfoModel.getUser_id()
						+ "\nユーザー名 = " + userinfoModel.getUser_name()
						+ "\n閲覧ライブラリID = " + Arrays.toString(mngUserForm.getLib_id())
						+ "\n管理権限 = " + mngUserForm.getManage_auth()
						+ "\n帳票出力権限 = " + mngUserForm.getOutput_auth()
						+ "\n付帯情報編集権限 = " + mngUserForm.getEdit_auth()
						+ "\nダウンロード権限 = " + mngUserForm.getDownload_auth()
						+ "\n移動権限 = " + mngUserForm.getMove_auth()
						+ "\n削除権限 = " + mngUserForm.getDelete_auth()
						+ "\n有効期限 = " + mngUserForm.getStart_date() + " ～ " + mngUserForm.getEnd_date()
						+ "\n削除フラグ = " + userinfoModel.getDel_flg(), MessageConst.W830);

				//warningメッセージ
				model.addAttribute("add_danger", MessageConst.W830);
			}
		}else {
			// ライセンス超過
			log.info("@@MngUserInfoCtrl.add " + MessageConst.W831 + "(テナントID = " + strTenantid
					+ ", ユーザーID = " + userinfoModel.getUser_id()
					+ ", ユーザー名 = " + userinfoModel.getUser_name()
					+ ", 閲覧ライブラリID = " + Arrays.toString(mngUserForm.getLib_id())
					+ ", 管理権限 = " + mngUserForm.getManage_auth()
					+ ", 帳票出力権限 = " + mngUserForm.getOutput_auth()
					+ ", 付帯情報編集権限 = " + mngUserForm.getEdit_auth()
					+ ", ダウンロード権限 = " + mngUserForm.getDownload_auth()
					+ ", 移動権限 = " + mngUserForm.getMove_auth()
					+ ", 削除権限 = " + mngUserForm.getDelete_auth()
					+ ", 有効期限 = " + mngUserForm.getStart_date() + " ～ " + mngUserForm.getEnd_date()
					+ ", 削除フラグ = " + userinfoModel.getDel_flg() + ")");

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()), CommonConst.MNG_USERINFOCTRL_ADD, CommonConst.FAILED_CO,
					  "ユーザーID = " + userinfoModel.getUser_id()
					+ "\nユーザー名 = " + userinfoModel.getUser_name()
					+ "\n閲覧ライブラリID = " + Arrays.toString(mngUserForm.getLib_id())
					+ "\n管理権限 = " + mngUserForm.getManage_auth()
					+ "\n帳票出力権限 = " + mngUserForm.getOutput_auth()
					+ "\n付帯情報編集権限 = " + mngUserForm.getEdit_auth()
					+ "\nダウンロード権限 = " + mngUserForm.getDownload_auth()
					+ "\n移動権限 = " + mngUserForm.getMove_auth()
					+ "\n削除権限 = " + mngUserForm.getDelete_auth()
					+ "\n有効期限 = " + mngUserForm.getStart_date() + " ～ " + mngUserForm.getEnd_date()
					+ "\n削除フラグ = " + userinfoModel.getDel_flg(), MessageConst.W831);

			//warningメッセージ
			model.addAttribute("add_license_danger", MessageConst.W831);
		}

		// 帳票有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
			model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
			// 帳票パラメータデータ
			model.addAttribute("report", reportParamDao.findTenant(strTenantid));
		}

		// システム情報を取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();

		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());
		// システムルート＋サムネイルルート＋テナントID
		StringBuilder sb = new StringBuilder(systeminfoModel.getSystem_rootpath());
		sb.append(CommonConst.YEN_MRK);
		sb.append(CommonConst.THUMBNAIL);
		sb.append(CommonConst.YEN_MRK);
		sb.append(strTenantid);
		// ライブラリツリーデータ
		model.addAttribute("libraryTreeData", getLibraryTreeData(sb.toString(), strTenantid));
		// ユーザーリスト
		model.addAttribute("user", userInfoDao.findTenantGeneralUser(strTenantid));

		return "mng_userinfo";
	}

	/**
	 * 更新ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="update", method=POST)
	public String update(@ModelAttribute("mngUserForm") MngUserInfoForm mngUserForm, BindingResult result, Model model, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザのテナントID設定
		String strTenantid = userInfoDao.getUser(tripleDesConverter.decrypt64(mngUserForm.getAcc_userID())).getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()), CommonConst.MNG_USERINFOCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()), CommonConst.MNG_USERINFOCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()), CommonConst.MNG_USERINFOCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		// テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);

		// フォームからモデルに転記
		UserInfoModel userinfoModel = setUserModel(mngUserForm, strTenantid);

		// フォームのユーザーIDが既にテーブルにあるか確認
		if (userInfoDao.count(userinfoModel.getUser_id()) == 0 ) {
			// 存在しないとき

			// ユーザーライセンス数確認
			if (tenantModel.getUser_license() > userInfoDao.licenseCount(strTenantid)) {
				// ライセンス範囲内
				// 追加
				userInfoDao.insert(userinfoModel);
				// ユーザ権限更新
				updateUserAuth(mngUserForm);
				// ライブラリ権限更新
				updateLibraryAuth(mngUserForm, strTenantid);
				// 帳票有効化確認
				if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
					// 帳票権限更新
					updateReportAuth(mngUserForm, strTenantid);
				}

				log.info("@@MngUserInfoCtrl.update ユーザー情報テーブルを追加しました(テナントID = " + strTenantid
						+ ", ユーザーID = " + userinfoModel.getUser_id()
						+ ", ユーザー名 = " + userinfoModel.getUser_name()
						+ ", 閲覧ライブラリID = " + Arrays.toString(mngUserForm.getLib_id())
						+ ", 管理権限 = " + mngUserForm.getManage_auth()
						+ ", 帳票出力権限 = " + mngUserForm.getOutput_auth()
						+ ", 付帯情報編集権限 = " + mngUserForm.getEdit_auth()
						+ ", ダウンロード権限 = " + mngUserForm.getDownload_auth()
						+ ", 移動権限 = " + mngUserForm.getMove_auth()
						+ ", 削除権限 = " + mngUserForm.getDelete_auth()
						+ ", 有効期限 = " + mngUserForm.getStart_date() + " ～ " + mngUserForm.getEnd_date()
						+ ", 削除フラグ = " + userinfoModel.getDel_flg() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()), CommonConst.MNG_USERINFOCTRL_ADD, CommonConst.SUCCESS_CO,
						  "ユーザーID = " + userinfoModel.getUser_id()
						+ "\nユーザー名 = " + userinfoModel.getUser_name()
						+ "\n閲覧ライブラリID = " + Arrays.toString(mngUserForm.getLib_id())
						+ "\n管理権限 = " + mngUserForm.getManage_auth()
						+ "\n帳票出力権限 = " + mngUserForm.getOutput_auth()
						+ "\n付帯情報編集権限 = " + mngUserForm.getEdit_auth()
						+ "\nダウンロード権限 = " + mngUserForm.getDownload_auth()
						+ "\n移動権限 = " + mngUserForm.getMove_auth()
						+ "\n削除権限 = " + mngUserForm.getDelete_auth()
						+ "\n有効期限 = " + mngUserForm.getStart_date() + " ～ " + mngUserForm.getEnd_date()
						+ "\n削除フラグ = " + userinfoModel.getDel_flg(), "");

				//successメッセージ
				model.addAttribute("success", MessageConst.M001);
			}else {
				// ライセンス超過
				log.info("@@MngUserInfoCtrl.update " + MessageConst.W831 + "(テナントID = " + strTenantid
						+ ", ユーザーID = " + userinfoModel.getUser_id()
						+ ", ユーザー名 = " + userinfoModel.getUser_name()
						+ ", 閲覧ライブラリID = " + Arrays.toString(mngUserForm.getLib_id())
						+ ", 管理権限 = " + mngUserForm.getManage_auth()
						+ ", 帳票出力権限 = " + mngUserForm.getOutput_auth()
						+ ", 付帯情報編集権限 = " + mngUserForm.getEdit_auth()
						+ ", ダウンロード権限 = " + mngUserForm.getDownload_auth()
						+ ", 移動権限 = " + mngUserForm.getMove_auth()
						+ ", 削除権限 = " + mngUserForm.getDelete_auth()
						+ ", 有効期限 = " + mngUserForm.getStart_date() + " ～ " + mngUserForm.getEnd_date()
						+ ", 削除フラグ = " + userinfoModel.getDel_flg() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()), CommonConst.MNG_USERINFOCTRL_ADD, CommonConst.FAILED_CO,
						  "ユーザーID = " + userinfoModel.getUser_id()
						+ "\nユーザー名 = " + userinfoModel.getUser_name()
						+ "\n閲覧ライブラリID = " + Arrays.toString(mngUserForm.getLib_id())
						+ "\n管理権限 = " + mngUserForm.getManage_auth()
						+ "\n帳票出力権限 = " + mngUserForm.getOutput_auth()
						+ "\n付帯情報編集権限 = " + mngUserForm.getEdit_auth()
						+ "\nダウンロード権限 = " + mngUserForm.getDownload_auth()
						+ "\n移動権限 = " + mngUserForm.getMove_auth()
						+ "\n削除権限 = " + mngUserForm.getDelete_auth()
						+ "\n有効期限 = " + mngUserForm.getStart_date() + " ～ " + mngUserForm.getEnd_date()
						+ "\n削除フラグ = " + userinfoModel.getDel_flg(), MessageConst.W831);

				//warningメッセージ
				model.addAttribute("up_license_danger", MessageConst.W831);
			}
		} else {
			// 存在するとき

			// ユーザーIDから更新前ユーザー情報を取得
			UserInfoModel oldUserinfoModel = userInfoDao.getOldUser(userinfoModel.getUser_id());

			// ライセンスユーザ数取得
			int licenseCount = userInfoDao.licenseCount(strTenantid);

			// ユーザーライセンス数確認(削除フラグ外しによってライセンス超過を防ぐ)
			if (tenantModel.getUser_license() >= licenseCount && !(tenantModel.getUser_license() == licenseCount && oldUserinfoModel.getDel_flg() == (byte) 1 && userinfoModel.getDel_flg() == (byte) 0)) {
				// ライセンス範囲内
				// 更新
				userInfoDao.update(userinfoModel);
				// ユーザ権限更新
				updateUserAuth(mngUserForm);
				// ライブラリ権限更新
				updateLibraryAuth(mngUserForm, strTenantid);
				// 帳票有効化確認
				if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
					// 帳票権限更新
					updateReportAuth(mngUserForm, strTenantid);
				}

				log.info("@@MngUserInfoCtrl.update ユーザー情報テーブルを更新しました(テナントID = " + strTenantid
						+ ", ユーザーID = " + userinfoModel.getUser_id()
						+ ", ユーザー名 = " + userinfoModel.getUser_name()
						+ ", パスワードエラーカウント = " + userinfoModel.getPwd_errcnt()
						+ ", 閲覧ライブラリID = " + Arrays.toString(mngUserForm.getLib_id())
						+ ", 管理権限 = " + mngUserForm.getManage_auth()
						+ ", 帳票出力権限 = " + mngUserForm.getOutput_auth()
						+ ", 付帯情報編集権限 = " + mngUserForm.getEdit_auth()
						+ ", ダウンロード権限 = " + mngUserForm.getDownload_auth()
						+ ", 移動権限 = " + mngUserForm.getMove_auth()
						+ ", 削除権限 = " + mngUserForm.getDelete_auth()
						+ ", 有効期限 = " + mngUserForm.getStart_date() + " ～ " + mngUserForm.getEnd_date()
						+ ", 削除フラグ = " + userinfoModel.getDel_flg() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()), CommonConst.MNG_USERINFOCTRL_UPDATE, CommonConst.SUCCESS_CO,
						  "ユーザーID = " + userinfoModel.getUser_id()
						+ "\nユーザー名 = " + userinfoModel.getUser_name()
						+ "\nパスワードエラーカウント = " + userinfoModel.getPwd_errcnt()
						+ "\n閲覧ライブラリID = " + Arrays.toString(mngUserForm.getLib_id())
						+ "\n管理権限 = " + mngUserForm.getManage_auth()
						+ "\n帳票出力権限 = " + mngUserForm.getOutput_auth()
						+ "\n付帯情報編集権限 = " + mngUserForm.getEdit_auth()
						+ "\nダウンロード権限 = " + mngUserForm.getDownload_auth()
						+ "\n移動権限 = " + mngUserForm.getMove_auth()
						+ "\n削除権限 = " + mngUserForm.getDelete_auth()
						+ "\n有効期限 = " + mngUserForm.getStart_date() + " ～ " + mngUserForm.getEnd_date()
						+ "\n削除フラグ = " + userinfoModel.getDel_flg(), "");

				//successメッセージ
				model.addAttribute("success", MessageConst.M001);
			}else {
				// ライセンス超過
				log.info("@@MngUserInfoCtrl.update " + MessageConst.W831 + "(テナントID = " + strTenantid
						+ ", ユーザーID = " + userinfoModel.getUser_id()
						+ ", ユーザー名 = " + userinfoModel.getUser_name()
						+ ", 閲覧ライブラリID = " + Arrays.toString(mngUserForm.getLib_id())
						+ ", 管理権限 = " + mngUserForm.getManage_auth()
						+ ", 帳票出力権限 = " + mngUserForm.getOutput_auth()
						+ ", 付帯情報編集権限 = " + mngUserForm.getEdit_auth()
						+ ", ダウンロード権限 = " + mngUserForm.getDownload_auth()
						+ ", 移動権限 = " + mngUserForm.getMove_auth()
						+ ", 削除権限 = " + mngUserForm.getDelete_auth()
						+ ", 有効期限 = " + mngUserForm.getStart_date() + " ～ " + mngUserForm.getEnd_date()
						+ ", 削除フラグ = " + userinfoModel.getDel_flg() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngUserForm.getAcc_userID()), CommonConst.MNG_USERINFOCTRL_UPDATE, CommonConst.FAILED_CO,
						  "ユーザーID = " + userinfoModel.getUser_id()
						+ "\nユーザー名 = " + userinfoModel.getUser_name()
						+ "\n閲覧ライブラリID = " + Arrays.toString(mngUserForm.getLib_id())
						+ "\n管理権限 = " + mngUserForm.getManage_auth()
						+ "\n帳票出力権限 = " + mngUserForm.getOutput_auth()
						+ "\n付帯情報編集権限 = " + mngUserForm.getEdit_auth()
						+ "\nダウンロード権限 = " + mngUserForm.getDownload_auth()
						+ "\n移動権限 = " + mngUserForm.getMove_auth()
						+ "\n削除権限 = " + mngUserForm.getDelete_auth()
						+ "\n有効期限 = " + mngUserForm.getStart_date() + " ～ " + mngUserForm.getEnd_date()
						+ "\n削除フラグ = " + userinfoModel.getDel_flg(), MessageConst.W831);

				//warningメッセージ
				model.addAttribute("up_license_danger", MessageConst.W831);
			}
		}

		// 帳票有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
			model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
			// 帳票パラメータデータ
			model.addAttribute("report", reportParamDao.findTenant(strTenantid));
		}

		// システム情報を取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();

		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());
		// システムルート＋サムネイルルート＋テナントID
		StringBuilder sb = new StringBuilder(systeminfoModel.getSystem_rootpath());
		sb.append(CommonConst.YEN_MRK);
		sb.append(CommonConst.THUMBNAIL);
		sb.append(CommonConst.YEN_MRK);
		sb.append(strTenantid);
		// ライブラリツリーデータ
		model.addAttribute("libraryTreeData", getLibraryTreeData(sb.toString(), strTenantid));
		// ユーザーリスト
		model.addAttribute("user", userInfoDao.findTenantGeneralUser(strTenantid));

		return "mng_userinfo";
	}

	/**
	 * フォームからモデルに入力情報を展開します。
	 * @param form 管理_ユーザー_画面フォーム
	 * @return ユーザーモデル
	 * @throws Exception
	 */
	private UserInfoModel setUserModel(MngUserInfoForm form, String strTenantid) throws Exception {

		UserInfoModel model = new UserInfoModel();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");

		//テナントID
		model.setTenant_id(strTenantid);
		//ユーザーID
		model.setUser_id(form.getUser_id());
		//パスワード(暗号化)
		model.setPwd(tripleDesConverter.encrypt64(form.getPassword()));
		//パスワードエラーカウント
		model.setPwd_errcnt(form.getPwd_errcnt());
		//ユーザー名
		model.setUser_name(form.getUser_name());
		//ユーザ種別コード
		model.setAuth_cd(CommonConst.USER_CD);
		// 有効開始日
		model.setStart_date(fmt.parse(form.getStart_date()));
		// 有効終了日
		model.setEnd_date(fmt.parse(form.getEnd_date()));
		// String 有効開始日
		model.setStr_start_date(form.getStart_date());
		// String 有効終了日
		model.setStr_end_date(form.getEnd_date());
		// 削除フラグ
		if (form.getDel_flg()) {
			model.setDel_flg((byte) 1);
			model.setStr_del_flg("✔");
		} else {
			model.setDel_flg((byte) 0);
		}
		// 更新ユーザーID
		model.setUpdate_user_id(tripleDesConverter.decrypt64(form.getAcc_userID()));

		return model;
	}

	/**
	 * ユーザ権限を更新します。
	 * @param form 管理_ユーザー_画面フォーム
	 * @throws Exception
	 */
	public void updateUserAuth(MngUserInfoForm form) throws Exception  {
		UserAuthModel model = new UserAuthModel();

		//ユーザーID
		model.setUser_id(form.getUser_id());
		// 更新ユーザーID
		model.setUpdate_user_id(tripleDesConverter.decrypt64(form.getAcc_userID()));

		// 権限削除
		userAuthDao.delete(form.getUser_id());

		// 管理権限
		if (form.getManage_auth()) {
			model.setAuth_cd(CommonConst.MANAGE_AUTH);
			userAuthDao.insert(model);
		}
		// 帳票出力権限
		if (form.getOutput_auth()) {
			model.setAuth_cd(CommonConst.OUTPUT_AUTH);
			userAuthDao.insert(model);
		}
		// 付帯情報編集権限
		if (form.getEdit_auth()) {
			model.setAuth_cd(CommonConst.EDIT_AUTH);
			userAuthDao.insert(model);
		}
		// ダウンロード権限
		if (form.getDownload_auth()) {
			model.setAuth_cd(CommonConst.DOWNLOAD_AUTH);
			userAuthDao.insert(model);
		}
		// 移動権限
		if (form.getMove_auth()) {
			model.setAuth_cd(CommonConst.MOVE_AUTH);
			userAuthDao.insert(model);
		}
		// 削除権限
		if (form.getDelete_auth()) {
			model.setAuth_cd(CommonConst.DELETE_AUTH);
			userAuthDao.insert(model);
		}

	}

	/**
	 * ライブラリ権限を更新します。
	 * @param form 管理_ユーザー_画面フォーム
	 * @throws Exception
	 */
	public void updateLibraryAuth(MngUserInfoForm form, String strTenantid) throws Exception  {
		LibraryAuthModel model = new LibraryAuthModel();

		// テナントID
		model.setTenant_id(strTenantid);
		// ユーザーID
		model.setUser_id(form.getUser_id());
		// 更新ユーザーID
		model.setUpdate_user_id(tripleDesConverter.decrypt64(form.getAcc_userID()));

		// ライブラリ権限全削除
		libraryAuthDao.delete(form.getUser_id());

		if (!ArrayUtils.isEmpty(form.getLib_id())) {
			for (int i=0; i < form.getLib_id().length; i++) {
				// ライブラリID
				model.setLib_id(form.getLib_id()[i]);
				// 権限追加
				libraryAuthDao.insert(model);
			}
		}

	}

	/**
	 * 帳票権限を更新します。
	 * @param form 管理_ユーザー_画面フォーム
	 * @throws Exception
	 */
	public void updateReportAuth(MngUserInfoForm form, String strTenantid) throws Exception  {
		ReportAuthModel model = new ReportAuthModel();

		// テナントID
		model.setTenant_id(strTenantid);
		// ユーザーID
		model.setUser_id(form.getUser_id());
		// 更新ユーザーID
		model.setUpdate_user_id(tripleDesConverter.decrypt64(form.getAcc_userID()));

		// 帳票権限全削除
		reportAuthDao.delete(form.getUser_id());

		if (!ArrayUtils.isEmpty(form.getReport_auth())) {
			for (int i=0; i < form.getReport_auth().length; i++) {
				// 帳票ID
				model.setReport_id(form.getReport_auth()[i]);
				// 権限追加
				reportAuthDao.insert(model);
			}
		}

	}

	/**
	 * ライブラリ権限取得<br>
	 * @param acc_userID 閲覧者暗号化ユーザーID
	 * @param userId 選択ユーザーID
	 * @param request リクエスト情報
	 * @return ライブラリ権限 リストModelオブジェクト
	 * @throws Exception
	 */
	@RequestMapping(params="getLibraryAuth", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getLibraryAuth(@RequestParam String acc_userID, String userId, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザのテナントID設定
		String strTenantid = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID)).getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_USERINFOCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_USERINFOCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_USERINFOCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		String json = null;

		//帳票権限情報格納
		List<LibraryAuthModel> LibraryAuthList = new ArrayList<LibraryAuthModel>();
		LibraryAuthList = libraryAuthDao.findUser(userId);

		if (LibraryAuthList.isEmpty() ==false) {
			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(LibraryAuthList);
		}

		return json;
	}

	/**
	 * 帳票権限取得<br>
	 * @param acc_userID 閲覧者暗号化ユーザーID
	 * @param userId 選択ユーザーID
	 * @param request リクエスト情報
	 * @return 帳票権限 リストModelオブジェクト
	 * @throws Exception
	 */
	@RequestMapping(params="getReportAuth", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getReportAuth(@RequestParam String acc_userID, String userId, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザのテナントID設定
		String strTenantid = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID)).getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_USERINFOCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_USERINFOCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_USERINFOCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		String json = null;

		//帳票権限情報格納
		List<ReportAuthModel> ReportAuthList = new ArrayList<ReportAuthModel>();
		ReportAuthList = reportAuthDao.findUser(userId);

		if (ReportAuthList.isEmpty() ==false) {
			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(ReportAuthList);
		}

		return json;
	}

	/**
	 * ライブラリツリーデータ取得 <br>
	 * @param path ルートパス
	 * @param strTenantid テナントID
	 * @return ライブラリツリーデータ
	 * @throws Exception
	 */
	private String getLibraryTreeData(String path, String strTenantid) throws Exception {

		String json = "error";
		Path userRootDir = Paths.get(path);

		// ユーザのルートディレクトリ存在確認
		if (Files.exists(userRootDir)) {
			// 表示順ライブラリリスト取得
			List<LibraryModel> libNoList = librarydao.libDisplayNo(strTenantid);
			// ライブラリツリーデータ作成
			Node root = getNode(userRootDir, path, strTenantid, libNoList);
			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(((Directory) root).getNodes());
			json = json.replace(",\"nodes\":[]", ""); // 空の子要素は削除する
			json = json.substring(1, json.length() - 1);
		}

		return json;
	}

	/**
	 * ライブラリリスト取得<br>
	 * @param obj ディレクトリ
	 * @param path ルートパス
	 * @param strTenantid テナントID
	 * @param libNoList 表示順ライブラリリスト
	 * @return ライブラリリスト
	 * @throws Exception
	*/
	public Node getNode(Path obj, String path, String strTenantid, List<LibraryModel> libNoList) throws Exception {

		Node node = null;

		if (Files.isDirectory(obj)) {

			Directory dir = new Directory(obj.getFileName().toString());

			// テナントを最上位とする
			if (path.equals(obj.toAbsolutePath().toString())) {
				dir = new Directory("無し（最上位）");
				dir.setDirId("");
			}else {
				dir.setDirId(librarydao.findPath(strTenantid, obj.toAbsolutePath().toString().replace(path + CommonConst.YEN_MRK, "")).getLib_id());
				// 表示順を設定
				if (libNoList.size() > 0) {
					for(int i=0; i < libNoList.size(); i++) {
						if (libNoList.get(i).getLib_path().equals(obj.toAbsolutePath().toString().replace(path + CommonConst.YEN_MRK, ""))) {
							dir.setDisNo(libNoList.get(i).getDisplay_no());
							break;
						}
					}
				}
			}

			// 子要素を作成
			List<Node> nodes = new ArrayList<Node>();

			// ディレクトリ内の一覧を取得
			FileUtil fileUtil = new FileUtil();
			List<Path> childList = fileUtil.getFileList(obj);

			for (Path child : childList) {
				if (Files.isDirectory(child) && librarydao.libCount(strTenantid, child.toAbsolutePath().toString().replace(path + CommonConst.YEN_MRK, "")) == 1) {
					// 登録ライブラリのとき再帰呼び出し
					nodes.add(getNode(child, path, strTenantid, libNoList));
				}
			}
			dir.setNodes(nodes);

			node = dir;
		}

		return node;
	}

}