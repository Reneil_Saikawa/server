/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込み変更。TripleDES変換クラス宣言(インスタンス化)変更。
 *    3    2019/10/23      1.0.2           女屋            不要メンバ変数削除と修正
 */
package jp.reneil.reiss.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.form.SystemManageLoginForm;
import jp.reneil.reiss.log.LogWriting;
import jp.reneil.reiss.model.UserInfoModel;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * システム管理_ログイン_画面コントローラクラスです。<br>
 *
 * @version 1.0.2
 */
@Controller
@RequestMapping("systemManage/login")
public class SystemManageLoginCtrl {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(SystemManageLoginCtrl.class);

	/** ログ書き込みクラス */
	@Autowired
	private LogWriting logWriting;

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** ユーザーテーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/**
	 * 画面表示<br>
	 * @param systemManageloginForm ログイン画面フォーム
	 * @param request リクエスト情報
	 * @return ログイン画面
	 * @throws Exception
	 */
	@RequestMapping(method = GET)
	public String init(@ModelAttribute("systemManageLoginForm") SystemManageLoginForm systemManageLoginForm, Model model, HttpServletRequest request) throws Exception {
		// セッション情報をクリアする
		HttpSession session = request.getSession(false);
		if (session != null){
			session.removeAttribute(SessionConst.USER_ID);
		}

		// システム名
		model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());

		// IPアドレス取得
		log.info("@@SystemManageLoginCtrl.init アクセスIP = " + request.getRemoteAddr());
		log.info("@@SystemManageLoginCtrl.init アクセスPC = " + request.getRemoteHost());

		log.info("@@SystemManageLoginCtrl.init ログイン画面表示");

		// アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_LOGINCTRL_INIT, CommonConst.SUCCESS_CO, "システム管理ログイン画面表示", "");

		return "systemManage/login";
	}

	/**
	 * ログイン処理<br>
	 * @param systemManageloginForm ログイン画面フォーム
	 * @param model Modelオブジェクト
	 * @param request リクエスト情報
	 * @return メイン画面
	 * @throws Exception
	 */
	@RequestMapping(method = POST)
	public String login(@ModelAttribute("systemManageLoginForm") SystemManageLoginForm systemManageLoginForm, Model model, HttpServletRequest request) throws Exception {

		UserInfoModel userinfoModel = userInfoDao.getLoginUser(CommonConst.SYSTEM_MANAGER);

		// 画面のパスワードを暗号化
		String scrPass = tripleDesConverter.encrypt64(systemManageLoginForm.getPassword());

		// パスワードは暗号化しない
//		String scrPass = systemManageLoginForm.getPassword();

		// 入力されたパスワードが正しいかチェック
		if (!userinfoModel.getPwd().equals(scrPass)) {
			model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());
			model.addAttribute("error", MessageConst.W000);
			// ログ出力
			log.info("@@SystemManageLoginCtrl.login " + MessageConst.W000 + "[user_id=" + CommonConst.SYSTEM_MANAGER + "]");

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_LOGINCTRL_LOGIN, CommonConst.FAILED_CO, "ユーザID = " + CommonConst.SYSTEM_MANAGER, "パスワードが違います。");

			return "systemManage/login";
		}

		// ユーザーIDをセッションに格納
		HttpSession session = request.getSession(true);
		session.setAttribute(SessionConst.USER_ID, CommonConst.SYSTEM_MANAGER);
		log.info("@@SystemManageLoginCtrl.login セッション情報追加[USER_ID = " + CommonConst.SYSTEM_MANAGER + "]");

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_LOGINCTRL_LOGIN, CommonConst.SUCCESS_CO, "システム管理ログイン処理", "");

		// システム情報更新画面に遷移
		return "redirect:systeminfo.html";
	}

}