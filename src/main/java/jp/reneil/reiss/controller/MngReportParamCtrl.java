/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込み変更。TripleDES変換クラス宣言(インスタンス化)変更。
 *    3    2019/10/23      1.0.2           女屋            不要メンバ変数削除と修正
 */
package jp.reneil.reiss.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.io.BufferedInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URLEncoder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.ReportParamDao;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.TenantDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.exception.ExException;
import jp.reneil.reiss.form.MngReportParamForm;
import jp.reneil.reiss.log.LogWriting;
import jp.reneil.reiss.model.AnythingModel;
import jp.reneil.reiss.model.ReportParamModel;
import jp.reneil.reiss.model.SystemInfoModel;
import jp.reneil.reiss.model.TenantModel;
import jp.reneil.reiss.model.UserInfoModel;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザ管理_帳票_画面コントローラクラスです。<br>
 *
 * @version 1.0.2
 */
@Controller
@RequestMapping(value="mng_reportparam")
public class MngReportParamCtrl {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(MngReportParamCtrl.class);

	/** ログ書き込みクラス */
	@Autowired
	private LogWriting logWriting;

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** テナント情報テーブルアクセス制御クラス */
	@Autowired
	private TenantDao tenantDao;

	/** ユーザー情報テーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/** 帳票パラメータテーブルアクセス制御クラス */
	@Autowired
	private ReportParamDao reportParamDao;

	/** 画面表示
	 * @throws Exception */
	@RequestMapping(method=GET)
	public String init(@ModelAttribute("mngReportParamForm") MngReportParamForm mngReportParamForm, Model model,  HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MNG_REPORTPARAMCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MNG_REPORTPARAMCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}

		// セッション情報からログインユーザIDを取得
		String user_id = (String) session.getAttribute(SessionConst.USER_ID);
		log.info("@@MngReportParamCtrl.init セッション情報取得[USER_ID=" + user_id + "]");
		//セッション情報からログインユーザーのテナントIDを取得
		String strTenantid = (String)session.getAttribute(SessionConst.TENANT_ID);
		log.info("@@MngReportParamCtrl.init セッション情報取得[TENANT_ID=" + strTenantid + "]");

		//セッション情報から権限コードを取得
		String authcode = (String) session.getAttribute(SessionConst.AUTH_CODE);
		model.addAttribute("auth",authcode);

		// システム名
		model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());
		// 暗号化ユーザID
		model.addAttribute("acc_userID", tripleDesConverter.encrypt64(user_id));
		// テナント有効期限取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);
		model.addAttribute("tenantStart", tenantModel.getStr_start_date());
		model.addAttribute("tenantEnd", tenantModel.getStr_end_date());
		// 帳票パラメータデータ取得
		model.addAttribute("report", reportParamDao.findTenant(strTenantid));
		//1ページ当たりの出力件数
		List<AnythingModel> anythingList = new ArrayList<AnythingModel>();
		AnythingModel anythingModel = new AnythingModel();
		for (int i = 1; i <= 200; i++) {
			anythingModel = new AnythingModel();
			anythingModel.setLabel(String.valueOf(i));
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("printRows",anythingList);

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_REPORTPARAMCTRL_INIT, CommonConst.SUCCESS_CO, "", "");

		return "mng_reportparam";
	}

	/**
	 * 追加ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="add", method=POST)
	public String add(@ModelAttribute("mngReportParamForm") MngReportParamForm mngReportParamForm, BindingResult result, Model model, HttpServletRequest request, @RequestParam(value="uploadFile",required=false) MultipartFile uploadFile) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		//セッション情報から権限コードを取得
		String authcode = (String) session.getAttribute(SessionConst.AUTH_CODE);
		model.addAttribute("auth",authcode);

		// システム情報取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();

		// フォームからモデルに転記
		ReportParamModel reportParamModel = setReportParamModel(mngReportParamForm, strTenantid);

		// フォームの帳票IDが既にテーブルにあるか確認
		if (reportParamDao.count(strTenantid, reportParamModel.getReport_id()) == 0 ) {

			// 同名のテンプレートファイルが存在するか確認
			if (reportParamDao.addFileCount(strTenantid, reportParamModel.getFile_name()) == 0 ) {
				// 存在しないとき（追加）

				// 物理フォルダ作成
				String repFilePath = systeminfoModel.getSystem_rootpath() + CommonConst.YEN_MRK + CommonConst.REPORT + CommonConst.YEN_MRK + strTenantid;
				Path repFolder = Paths.get(repFilePath);
				// 帳票テンプレートフォルダが存在しない場合作成
				if (!Files.exists(repFolder)) {
					// 失敗したらリトライ
					for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
						try {
							Files.createDirectories(repFolder);
							if (Files.exists(repFolder)) {
								// 帳票テンプレートフォルダ作成に成功したらループから抜ける
								break;
							}else {
								// 帳票テンプレートフォルダ作成失敗
								log.info("@@MngReportParamCtrl.add " + repFolder.toAbsolutePath().toString() + " 帳票テンプレートフォルダ作成失敗回数 = " + (i + 1));
								try
								{
									Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
								}
								catch(InterruptedException ex){}
							}
						}catch (Exception e) {
							// 帳票テンプレートフォルダ作成失敗(例外)
							StringWriter sw = new StringWriter();
							PrintWriter pw = new PrintWriter(sw);
							e.printStackTrace(pw);
							pw.flush();
							String str = sw.toString();
							log.info("@@MngReportParamCtrl.add " + repFolder.toAbsolutePath().toString() + " 帳票テンプレートフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}
					}
				}

				// アップロードファイルの存在確認
				if (!uploadFile.isEmpty()) {
					//MultipartFileからファイルを生成
					//失敗したらリトライ
					for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
						InputStream is = null;
						Path convFile = Paths.get(repFilePath + CommonConst.YEN_MRK + reportParamModel.getFile_name());
						try {
							is = new BufferedInputStream(uploadFile.getInputStream());
							Files.copy(is, convFile, StandardCopyOption.REPLACE_EXISTING);
						    log.info("@@MngReportParamCtrl.add 帳票テンプレートファイル生成成功 = " + convFile.toAbsolutePath().toString());
							//ファイル生成に成功したらループから抜ける
						    break;
						}catch (Exception e) {
							//ファイル作成失敗
							StringWriter sw = new StringWriter();
							PrintWriter pw = new PrintWriter(sw);
							e.printStackTrace(pw);
							pw.flush();
							String str = sw.toString();
							log.info("@@MngReportParamCtrl.add " + convFile.toAbsolutePath().toString() + " 帳票テンプレートファイル生成失敗回数 = " + (i + 1) + "\n" + str);
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}finally {
							if (is != null) {
								is.close();
							}
						}
					}

					// DB登録（追加）
					reportParamDao.insert(reportParamModel);

					log.info("@@MngReportParamCtrl.add 帳票パラメータ情報テーブルを追加しました(テナントID = " + strTenantid
							+ ", 帳票ID = " + reportParamModel.getReport_id()
							+ ", 帳票名 = " + reportParamModel.getReport_name()
							+ ", テンプレートファイル名 = " + reportParamModel.getFile_name()
							+ ", シート名 = " + reportParamModel.getSheet_name()
							+ ", 1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
							+ ", 表紙出力フラグ = " + reportParamModel.getFront_page_flg()
							+ ", 表紙用シート名 = " + reportParamModel.getFront_page_sheet()
							+ ", 有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
							+ ", 削除フラグ = " + reportParamModel.getDel_flg() + ")");

					// アクセスログ書き込み
					logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_ADD, CommonConst.SUCCESS_CO,
							  "帳票ID = " + reportParamModel.getReport_id()
							+ "\n帳票名 = " + reportParamModel.getReport_name()
							+ "\nテンプレートファイル名 = " + reportParamModel.getFile_name()
							+ "\nシート名 = " + reportParamModel.getSheet_name()
							+ "\n1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
							+ "\n表紙出力フラグ = " + reportParamModel.getFront_page_flg()
							+ "\n表紙用シート名 = " + reportParamModel.getFront_page_sheet()
							+ "\n有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
							+ "\n削除フラグ = " + reportParamModel.getDel_flg(), "");

					// successメッセージ
					model.addAttribute("success", MessageConst.M001);

				} else {
					// テンプレートファイルがアップロードされていないとき

					log.info("@@MngReportParamCtrl.add " + MessageConst.W852 + "(テナントID = " + strTenantid
							+ ", 帳票ID = " + reportParamModel.getReport_id()
							+ ", 帳票名 = " + reportParamModel.getReport_name()
							+ ", テンプレートファイル名 = " + reportParamModel.getFile_name()
							+ ", シート名 = " + reportParamModel.getSheet_name()
							+ ", 1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
							+ ", 表紙出力フラグ = " + reportParamModel.getFront_page_flg()
							+ ", 表紙用シート名 = " + reportParamModel.getFront_page_sheet()
							+ ", 有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
							+ ", 削除フラグ = " + reportParamModel.getDel_flg() + ")");

					// アクセスログ書き込み
					logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_ADD, CommonConst.FAILED_CO,
							  "帳票ID = " + reportParamModel.getReport_id()
							+ "\n帳票名 = " + reportParamModel.getReport_name()
							+ "\nテンプレートファイル名 = " + reportParamModel.getFile_name()
							+ "\nシート名 = " + reportParamModel.getSheet_name()
							+ "\n1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
							+ "\n表紙出力フラグ = " + reportParamModel.getFront_page_flg()
							+ "\n表紙用シート名 = " + reportParamModel.getFront_page_sheet()
							+ "\n有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
							+ "\n削除フラグ = " + reportParamModel.getDel_flg(), MessageConst.W852);

					// warningメッセージ
					model.addAttribute("add_file_danger", MessageConst.W852);
				}



			} else {
				// テンプレートファイルが存在するとき

				log.info("@@MngReportParamCtrl.add " + MessageConst.W851 + "(テナントID = " + strTenantid
						+ ", 帳票ID = " + reportParamModel.getReport_id()
						+ ", 帳票名 = " + reportParamModel.getReport_name()
						+ ", テンプレートファイル名 = " + reportParamModel.getFile_name()
						+ ", シート名 = " + reportParamModel.getSheet_name()
						+ ", 1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
						+ ", 表紙出力フラグ = " + reportParamModel.getFront_page_flg()
						+ ", 表紙用シート名 = " + reportParamModel.getFront_page_sheet()
						+ ", 有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
						+ ", 削除フラグ = " + reportParamModel.getDel_flg() + ")");

				// アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_ADD, CommonConst.FAILED_CO,
						  "帳票ID = " + reportParamModel.getReport_id()
						+ "\n帳票名 = " + reportParamModel.getReport_name()
						+ "\nテンプレートファイル名 = " + reportParamModel.getFile_name()
						+ "\nシート名 = " + reportParamModel.getSheet_name()
						+ "\n1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
						+ "\n表紙出力フラグ = " + reportParamModel.getFront_page_flg()
						+ "\n表紙用シート名 = " + reportParamModel.getFront_page_sheet()
						+ "\n有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
						+ "\n削除フラグ = " + reportParamModel.getDel_flg(), MessageConst.W851);

				// warningメッセージ
				model.addAttribute("add_file_danger", MessageConst.W851);
			}

		} else {
			// 帳票IDが存在するとき

			log.info("@@MngReportParamCtrl.add " + MessageConst.W850 + "(テナントID = " + strTenantid
					+ ", 帳票ID = " + reportParamModel.getReport_id()
					+ ", 帳票名 = " + reportParamModel.getReport_name()
					+ ", テンプレートファイル名 = " + reportParamModel.getFile_name()
					+ ", シート名 = " + reportParamModel.getSheet_name()
					+ ", 1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
					+ ", 表紙出力フラグ = " + reportParamModel.getFront_page_flg()
					+ ", 表紙用シート名 = " + reportParamModel.getFront_page_sheet()
					+ ", 有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
					+ ", 削除フラグ = " + reportParamModel.getDel_flg() + ")");

			// アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_ADD, CommonConst.FAILED_CO,
					  "帳票ID = " + reportParamModel.getReport_id()
					+ "\n帳票名 = " + reportParamModel.getReport_name()
					+ "\nテンプレートファイル名 = " + reportParamModel.getFile_name()
					+ "\nシート名 = " + reportParamModel.getSheet_name()
					+ "\n1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
					+ "\n表紙出力フラグ = " + reportParamModel.getFront_page_flg()
					+ "\n表紙用シート名 = " + reportParamModel.getFront_page_sheet()
					+ "\n有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
					+ "\n削除フラグ = " + reportParamModel.getDel_flg(), MessageConst.W850);

			// warningメッセージ
			model.addAttribute("add_danger", MessageConst.W850);
		}

		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());

		// 帳票パラメータデータ取得
		model.addAttribute("report", reportParamDao.findTenant(strTenantid));
		//1ページ当たりの出力件数
		List<AnythingModel> anythingList = new ArrayList<AnythingModel>();
		AnythingModel anythingModel = new AnythingModel();
		for (int i = 1; i <= 200; i++) {
			anythingModel = new AnythingModel();
			anythingModel.setLabel(String.valueOf(i));
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("printRows",anythingList);

		return "mng_reportparam";
	}

	/**
	 * 更新ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="update", method=POST)
	public String update(@ModelAttribute("mngReportParamForm") MngReportParamForm mngReportParamForm, BindingResult result, Model model, HttpServletRequest request, @RequestParam(value="uploadFile",required=false) MultipartFile uploadFile) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		// セッション情報から権限コードを取得
		String authcode = (String) session.getAttribute(SessionConst.AUTH_CODE);
		model.addAttribute("auth",authcode);

		// システム情報取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();

		// フォームからモデルに転記
		ReportParamModel reportParamModel = setReportParamModel(mngReportParamForm, strTenantid);

		// フォームの帳票IDが既にテーブルにあるか確認
		if (reportParamDao.count(strTenantid, reportParamModel.getReport_id()) == 0 ) {

			// 同名のテンプレートファイルが存在するか確認
			if (reportParamDao.addFileCount(strTenantid, reportParamModel.getFile_name()) == 0 ) {
				// 同名のテンプレートファイルが存在しないとき（追加）

				// 物理フォルダ作成
				String repFilePath = systeminfoModel.getSystem_rootpath() + CommonConst.YEN_MRK + CommonConst.REPORT + CommonConst.YEN_MRK + strTenantid;
				Path repFolder = Paths.get(repFilePath);
				// 帳票テンプレートフォルダが存在しない場合作成
				if (!Files.exists(repFolder)) {
					// 失敗したらリトライ
					for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
						try {
							Files.createDirectories(repFolder);
							if (Files.exists(repFolder)) {
								// 帳票テンプレートフォルダ作成に成功したらループから抜ける
								break;
							}else {
								// 帳票テンプレートフォルダ作成失敗
								log.info("@@MngReportParamCtrl.update " + repFolder.toAbsolutePath().toString() + " 帳票テンプレートフォルダ作成失敗回数 = " + (i + 1));
								try
								{
									Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
								}
								catch(InterruptedException ex){}
							}
						}catch (Exception e) {
							// 帳票テンプレートフォルダ作成失敗(例外)
							StringWriter sw = new StringWriter();
							PrintWriter pw = new PrintWriter(sw);
							e.printStackTrace(pw);
							pw.flush();
							String str = sw.toString();
							log.info("@@MngReportParamCtrl.update " + repFolder.toAbsolutePath().toString() + " 帳票テンプレートフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}
					}
				}

				// アップロードファイルの存在確認
				if (!uploadFile.isEmpty()) {
					// MultipartFileからファイルを生成
					// 失敗したらリトライ
					for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
						InputStream is = null;
						Path convFile = Paths.get(repFilePath + CommonConst.YEN_MRK + reportParamModel.getFile_name());
						try {
							is = new BufferedInputStream(uploadFile.getInputStream());
							Files.copy(is, convFile, StandardCopyOption.REPLACE_EXISTING);
						    log.info("@@MngReportParamCtrl.update 帳票テンプレートファイル生成成功 = " + convFile.toAbsolutePath().toString());
							//ファイル生成に成功したらループから抜ける
						    break;
						}catch (Exception e) {
							//ファイル作成失敗
							StringWriter sw = new StringWriter();
							PrintWriter pw = new PrintWriter(sw);
							e.printStackTrace(pw);
							pw.flush();
							String str = sw.toString();
							log.info("@@MngReportParamCtrl.update " + convFile.toAbsolutePath().toString() + " 帳票テンプレートファイル生成失敗回数 = " + (i + 1) + "\n" + str);
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}finally {
							if (is != null) {
								is.close();
							}
						}
					}
				}

				// DB登録（追加）
				reportParamDao.insert(reportParamModel);

				log.info("@@MngReportParamCtrl.update 帳票パラメータ情報テーブルを追加しました(テナントID = " + strTenantid
						+ ", 帳票ID = " + reportParamModel.getReport_id()
						+ ", 帳票名 = " + reportParamModel.getReport_name()
						+ ", テンプレートファイル名 = " + reportParamModel.getFile_name()
						+ ", シート名 = " + reportParamModel.getSheet_name()
						+ ", 1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
						+ ", 表紙出力フラグ = " + reportParamModel.getFront_page_flg()
						+ ", 表紙用シート名 = " + reportParamModel.getFront_page_sheet()
						+ ", 有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
						+ ", 削除フラグ = " + reportParamModel.getDel_flg() + ")");

				// アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_ADD, CommonConst.SUCCESS_CO,
						  "帳票ID = " + reportParamModel.getReport_id()
						+ "\n帳票名 = " + reportParamModel.getReport_name()
						+ "\nテンプレートファイル名 = " + reportParamModel.getFile_name()
						+ "\nシート名 = " + reportParamModel.getSheet_name()
						+ "\n1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
						+ "\n表紙出力フラグ = " + reportParamModel.getFront_page_flg()
						+ "\n表紙用シート名 = " + reportParamModel.getFront_page_sheet()
						+ "\n有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
						+ "\n削除フラグ = " + reportParamModel.getDel_flg(), "");

				// successメッセージ
				model.addAttribute("success", MessageConst.M001);
			} else {
				// テンプレートファイルが存在するとき

				log.info("@@MngReportParamCtrl.update " + MessageConst.W851 + "(テナントID = " + strTenantid
						+ ", 帳票ID = " + reportParamModel.getReport_id()
						+ ", 帳票名 = " + reportParamModel.getReport_name()
						+ ", テンプレートファイル名 = " + reportParamModel.getFile_name()
						+ ", シート名 = " + reportParamModel.getSheet_name()
						+ ", 1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
						+ ", 表紙出力フラグ = " + reportParamModel.getFront_page_flg()
						+ ", 表紙用シート名 = " + reportParamModel.getFront_page_sheet()
						+ ", 有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
						+ ", 削除フラグ = " + reportParamModel.getDel_flg() + ")");

				// アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_ADD, CommonConst.FAILED_CO,
						  "帳票ID = " + reportParamModel.getReport_id()
						+ "\n帳票名 = " + reportParamModel.getReport_name()
						+ "\nテンプレートファイル名 = " + reportParamModel.getFile_name()
						+ "\nシート名 = " + reportParamModel.getSheet_name()
						+ "\n1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
						+ "\n表紙出力フラグ = " + reportParamModel.getFront_page_flg()
						+ "\n表紙用シート名 = " + reportParamModel.getFront_page_sheet()
						+ "\n有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
						+ "\n削除フラグ = " + reportParamModel.getDel_flg(), MessageConst.W851);

				// warningメッセージ
				model.addAttribute("up_danger", MessageConst.W851);
			}

		} else {
			// 帳票IDが存在するとき（更新）

			// 更新帳票以外に同名のテンプレートファイルが存在するか確認
			if (reportParamDao.updateFileCount(strTenantid, reportParamModel.getFile_name(), reportParamModel.getReport_id()) == 0 ) {
				// 同名のテンプレートファイルが存在しないとき（更新）

				// 物理フォルダ作成
				String repFilePath = systeminfoModel.getSystem_rootpath() + CommonConst.YEN_MRK + CommonConst.REPORT + CommonConst.YEN_MRK + strTenantid;
				Path repFolder = Paths.get(repFilePath);
				// 帳票テンプレートフォルダが存在しない場合作成
				if (!Files.exists(repFolder)) {
					// 失敗したらリトライ
					for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
						try {
							Files.createDirectories(repFolder);
							if (Files.exists(repFolder)) {
								// 帳票テンプレートフォルダ作成に成功したらループから抜ける
								break;
							}else {
								// 帳票テンプレートフォルダ作成失敗
								log.info("@@MngReportParamCtrl.update " + repFolder.toAbsolutePath().toString() + " 帳票テンプレートフォルダ作成失敗回数 = " + (i + 1));
								try
								{
									Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
								}
								catch(InterruptedException ex){}
							}
						}catch (Exception e) {
							// 帳票テンプレートフォルダ作成失敗(例外)
							StringWriter sw = new StringWriter();
							PrintWriter pw = new PrintWriter(sw);
							e.printStackTrace(pw);
							pw.flush();
							String str = sw.toString();
							log.info("@@MngReportParamCtrl.update " + repFolder.toAbsolutePath().toString() + " 帳票テンプレートフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}
					}
				}

				// アップロードファイルの存在確認
				if (!uploadFile.isEmpty()) {
					// MultipartFileからファイルを生成
					// 失敗したらリトライ
					for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
						InputStream is = null;
						Path convFile = Paths.get(repFilePath + CommonConst.YEN_MRK + reportParamModel.getFile_name());
						try {
							is = new BufferedInputStream(uploadFile.getInputStream());
							Files.copy(is, convFile, StandardCopyOption.REPLACE_EXISTING);
						    log.info("@@MngReportParamCtrl.update 帳票テンプレートファイル生成成功 = " + convFile.toAbsolutePath().toString());
							//ファイル生成に成功したらループから抜ける
						    break;
						}catch (Exception e) {
							//ファイル作成失敗
							StringWriter sw = new StringWriter();
							PrintWriter pw = new PrintWriter(sw);
							e.printStackTrace(pw);
							pw.flush();
							String str = sw.toString();
							log.info("@@MngReportParamCtrl.update " + convFile.toAbsolutePath().toString() + " 帳票テンプレートファイル生成失敗回数 = " + (i + 1) + "\n" + str);
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}finally {
							if (is != null) {
								is.close();
							}
						}
					}
				}

				// DB登録（更新）
				reportParamDao.update(reportParamModel);

				log.info("@@MngReportParamCtrl.update 帳票パラメータ情報テーブルを更新しました(テナントID = " + strTenantid
						+ ", 帳票ID = " + reportParamModel.getReport_id()
						+ ", 帳票名 = " + reportParamModel.getReport_name()
						+ ", テンプレートファイル名 = " + reportParamModel.getFile_name()
						+ ", シート名 = " + reportParamModel.getSheet_name()
						+ ", 1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
						+ ", 表紙出力フラグ = " + reportParamModel.getFront_page_flg()
						+ ", 表紙用シート名 = " + reportParamModel.getFront_page_sheet()
						+ ", 有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
						+ ", 削除フラグ = " + reportParamModel.getDel_flg() + ")");

				// アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_UPDATE, CommonConst.SUCCESS_CO,
						  "帳票ID = " + reportParamModel.getReport_id()
						+ "\n帳票名 = " + reportParamModel.getReport_name()
						+ "\nテンプレートファイル名 = " + reportParamModel.getFile_name()
						+ "\nシート名 = " + reportParamModel.getSheet_name()
						+ "\n1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
						+ "\n表紙出力フラグ = " + reportParamModel.getFront_page_flg()
						+ "\n表紙用シート名 = " + reportParamModel.getFront_page_sheet()
						+ "\n有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
						+ "\n削除フラグ = " + reportParamModel.getDel_flg(), "");

				// successメッセージ
				model.addAttribute("success", MessageConst.M001);
			} else {
				// テンプレートファイルが存在するとき

				log.info("@@MngReportParamCtrl.update " + MessageConst.W851 + "(テナントID = " + strTenantid
						+ ", 帳票ID = " + reportParamModel.getReport_id()
						+ ", 帳票名 = " + reportParamModel.getReport_name()
						+ ", テンプレートファイル名 = " + reportParamModel.getFile_name()
						+ ", シート名 = " + reportParamModel.getSheet_name()
						+ ", 1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
						+ ", 表紙出力フラグ = " + reportParamModel.getFront_page_flg()
						+ ", 表紙用シート名 = " + reportParamModel.getFront_page_sheet()
						+ ", 有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
						+ ", 削除フラグ = " + reportParamModel.getDel_flg() + ")");

				// アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_UPDATE, CommonConst.FAILED_CO,
						  "帳票ID = " + reportParamModel.getReport_id()
						+ "\n帳票名 = " + reportParamModel.getReport_name()
						+ "\nテンプレートファイル名 = " + reportParamModel.getFile_name()
						+ "\nシート名 = " + reportParamModel.getSheet_name()
						+ "\n1ページ当たりの出力件数 = " + reportParamModel.getPrint_rows()
						+ "\n表紙出力フラグ = " + reportParamModel.getFront_page_flg()
						+ "\n表紙用シート名 = " + reportParamModel.getFront_page_sheet()
						+ "\n有効期限 = " + mngReportParamForm.getStart_date() + " ～ " + mngReportParamForm.getEnd_date()
						+ "\n削除フラグ = " + reportParamModel.getDel_flg(), MessageConst.W851);

				// warningメッセージ
				model.addAttribute("up_danger", MessageConst.W851);
			}

		}

		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());

		// 帳票パラメータデータ取得
		model.addAttribute("report", reportParamDao.findTenant(strTenantid));
		// 1ページ当たりの出力件数
		List<AnythingModel> anythingList = new ArrayList<AnythingModel>();
		AnythingModel anythingModel = new AnythingModel();
		for (int i = 1; i <= 200; i++) {
			anythingModel = new AnythingModel();
			anythingModel.setLabel(String.valueOf(i));
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("printRows",anythingList);

		return "mng_reportparam";
	}

	/**
	 * ダウンロードボタンクリック時<br>
	 * @return 帳票テンプレートファイル
	 * @throws Exception
	 */
	@RequestMapping(params="download", method=POST)
	public void download(@ModelAttribute("mngReportParamForm") MngReportParamForm mngReportParamForm, BindingResult result, Model model, HttpServletRequest request, HttpServletResponse response) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_DOWNLOAD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_DOWNLOAD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_DOWNLOAD, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		// システム情報取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();

		// ダウンロードファイルパス作成
		String repFilePath = systeminfoModel.getSystem_rootpath() + CommonConst.YEN_MRK + CommonConst.REPORT + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK + mngReportParamForm.getFile_name();
		Path repFile = Paths.get(repFilePath);

		try (OutputStream os = response.getOutputStream()) {

			if (Files.exists(repFile)) {

				byte[] fileByteArray = Files.readAllBytes(repFile);

				response.setContentType("application/octet-stream");
				response.setHeader("Content-Disposition", "attachment; filename=\"" + URLEncoder.encode(mngReportParamForm.getFile_name(),"UTF-8") + "\"");
				response.setContentLength(fileByteArray.length);

				os.write(fileByteArray);
				os.flush();

				log.info("@@MngReportParamCtrl.download 帳票テンプレートファイルダウンロード 対象ファイル = " + repFile.toAbsolutePath().toString());
		   	    // アクセスログ書き込み
	    	    logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_DOWNLOAD, CommonConst.SUCCESS_CO, "ダウンロードファイル = " + mngReportParamForm.getFile_name(), "");

			}else {
				// ファイルが存在しない
				log.info("@@MngReportParamCtrl.download 帳票テンプレートファイル " + repFile.toAbsolutePath().toString() + " が存在しません。");
		   	    // アクセスログ書き込み
	    	    logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngReportParamForm.getAcc_userID()), CommonConst.MNG_REPORTPARAMCTRL_DOWNLOAD, CommonConst.FAILED_CO, "ダウンロードファイル = " + mngReportParamForm.getFile_name(), "ファイル " + mngReportParamForm.getFile_name() + " がシステムに存在しません。");
			}

		}catch (Exception e) {
			// 帳票テンプレートファイルダウンロード失敗(例外)
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			String str = sw.toString();
			log.info("@@MngReportParamCtrl.download 帳票テンプレートファイルダウンロード失敗(例外) 対象ファイル = " + repFile.toAbsolutePath().toString() + "\n" + str);
		}

	}

	/**
	 * フォームからモデルに入力情報を展開します。
	 * @param form ユーザ管理_帳票_画面フォーム
	 * @return 帳票パラメータモデル
	 * @throws Exception
	 */
	private ReportParamModel setReportParamModel(MngReportParamForm form, String strTenantid) throws Exception {
		ReportParamModel model = new ReportParamModel();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");

		//テナントID
		model.setTenant_id(strTenantid);
		//帳票ID
		model.setReport_id(form.getReport_id());
		//帳票名
		model.setReport_name(form.getReport_name());
		//テンプレートファイル名
		model.setFile_name(form.getFile_name());
		//シート名
		model.setSheet_name(form.getSheet_name());
		//1ページ当たりの出力件数
		model.setPrint_rows(form.getPrint_rows());
		//表紙出力フラグ
		if (form.getFront_page_flg()) {
			model.setFront_page_flg((byte) 1);
			model.setStr_front_page_flg("✔");
		} else {
			model.setFront_page_flg((byte) 0);
		}
		//表紙用シート名
		model.setFront_page_sheet(form.getFront_page_sheet());
		// 有効開始日
		model.setStart_date(fmt.parse(form.getStart_date()));
		// 有効終了日
		model.setEnd_date(fmt.parse(form.getEnd_date()));
		// String 有効開始日
		model.setStr_start_date(form.getStart_date());
		// String 有効終了日
		model.setStr_end_date(form.getEnd_date());
		// 削除フラグ
		if (form.getDel_flg()) {
			model.setDel_flg((byte) 1);
			model.setStr_del_flg("✔");
		} else {
			model.setDel_flg((byte) 0);
		}
		// 更新ユーザーID
		model.setUpdate_user_id(tripleDesConverter.decrypt64(form.getAcc_userID()));

		return model;
	}

}