/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込み変更。TripleDES変換クラス宣言(インスタンス化)変更。
 *    3    2019/10/23      1.0.2           女屋            不要メンバ変数削除と修正
 *    4    2020/02/20      1.0.3           女屋            帳票有効化機能追加
 */
package jp.reneil.reiss.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvGenerator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.AccessLogDao;
import jp.reneil.reiss.dao.CodeDao;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.TenantDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.exception.ExException;
import jp.reneil.reiss.form.MngAccessedLogForm;
import jp.reneil.reiss.log.LogWriting;
import jp.reneil.reiss.model.AccessLogCsvModel;
import jp.reneil.reiss.model.AccessLogModel;
import jp.reneil.reiss.model.TenantModel;
import jp.reneil.reiss.model.UserInfoModel;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザ管理_アクセスログ_画面コントローラクラスです。<br>
 *
 * @version 1.0.3
 */
@Controller
@RequestMapping(value="mng_accessedlog")
public class MngAccessedLogCtrl {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(MngAccessedLogCtrl.class);

	/** ログ書き込みクラス */
	@Autowired
	private LogWriting logWriting;

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** テナント情報テーブルアクセス制御クラス */
	@Autowired
	private TenantDao tenantDao;

	/** ユーザー情報テーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/** コードテーブルアクセス制御クラス */
	@Autowired
	private CodeDao codeDao;

	/** アクセスログテーブルアクセス制御クラス */
	@Autowired
	private AccessLogDao accessLogDao;

	/** 画面表示
	 * @throws Exception */
	@RequestMapping(method=GET)
	public String init(@ModelAttribute("mngAccessedLogForm") MngAccessedLogForm mngAccessedLogForm, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MNG_ACCESSEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MNG_ACCESSEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}

		// セッション情報からログインユーザIDを取得
		String user_id = (String) session.getAttribute(SessionConst.USER_ID);
		log.info("@@MngAccessedLogCtrl.init セッション情報取得[USER_ID=" + user_id + "]");
		//セッション情報からログインユーザーのテナントIDを取得
		String strTenantid = (String)session.getAttribute(SessionConst.TENANT_ID);
		log.info("@@MngAccessedLogCtrl.init セッション情報取得[TENANT_ID=" + strTenantid + "]");

		// テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);

		// 帳票有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
			model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
		}

		// システム名
		model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());
		// 暗号化ユーザID
		model.addAttribute("acc_userID", tripleDesConverter.encrypt64(user_id));
		// テナントID
		model.addAttribute("tenantId", strTenantid);
		// ユーザー情報取得
		model.addAttribute("user", userInfoDao.findTenantAllUser(strTenantid));
		// 実行プログラム
		model.addAttribute("execution", codeDao.getCode01RangeData("70", "99"));
		// 処理結果
		model.addAttribute("result", codeDao.getCode01Data("00"));

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_ACCESSEDLOGCTRL_INIT, CommonConst.SUCCESS_CO, "", "");

		return "mng_accessedlog";
	}

	/**
	 * アクセスログ検索<br>
	 * @param startDate 検索開始日時
	 * @param endDate 検索終了日時
	 * @param tenantId テナントID
	 * @param userId ユーザーID
	 * @param executionCode 実行プログラムコード
	 * @param resultCode 処理結果コード
	 * @param request リクエスト情報
	 * @return アクセスログ リストModelオブジェクト
	 * @throws Exception
	 */
	@RequestMapping(params="searchAccesslog", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String searchAccesslog(@RequestParam String acc_userID, String startDate, String endDate, String tenantId, String userId, String executionCode, String resultCode, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_ACCESSEDLOGCTRL_SEARCHACCESSLOG, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_ACCESSEDLOGCTRL_SEARCHACCESSLOG, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_ACCESSEDLOGCTRL_SEARCHACCESSLOG, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		String json = null;

		//アクセスログ情報リスト
		List<AccessLogModel> AccessLogList = new ArrayList<AccessLogModel>();

		AccessLogList.addAll(accessLogDao.find(startDate, endDate, tenantId, userId, executionCode, resultCode));

		if (AccessLogList.isEmpty() ==false) {
			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(AccessLogList);
		}

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_ACCESSEDLOGCTRL_SEARCHACCESSLOG, CommonConst.SUCCESS_CO, "検索期間 = " + startDate + " ～ " + endDate + "\n検索ユーザID = " + userId + "\n検索実行プログラムコード = " + executionCode + "\n検索処理結果コード = " + resultCode, "");

		return json;
	}

	/**
	 * CSVダウンロード<br>
	 * @param mngAccessedLogForm アクセスログ画面フォーム
	 * @param request リクエスト情報
	 * @param respons レスポンス情報
	 * @return アクセスログCSVファイルレスポンス
	 * @throws Exception
	 */
	@RequestMapping(params="searchAccesslogCsv", method=RequestMethod.POST, produces = "text/html;charset=UTF-8")
	public void searchAccesslogCsv(@ModelAttribute("mngAccessedLogForm") MngAccessedLogForm mngAccessedLogForm, HttpServletRequest request, HttpServletResponse response) throws Exception {

		// ユーザIDを取得
		String user_id = tripleDesConverter.decrypt64(mngAccessedLogForm.getAcc_userID());
		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(user_id);
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_ACCESSEDLOGCTRL_SEARCHACCESSLOG_CSV, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_ACCESSEDLOGCTRL_SEARCHACCESSLOG_CSV, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(user_id)) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_ACCESSEDLOGCTRL_SEARCHACCESSLOG_CSV, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		log.info("@@MngAccessedLogCtrl.searchAccesslogCsv ユーザID = " + user_id);

		// ログ出力範囲を設定
		String start_date = "1970/01/01";
		String end_date = "9999/12/31";
		if (!StringUtils.isEmpty(mngAccessedLogForm.getStart_date())) {
			start_date = mngAccessedLogForm.getStart_date();
		}
		if (!StringUtils.isEmpty(mngAccessedLogForm.getEnd_date())) {
			end_date = mngAccessedLogForm.getEnd_date();
		}

		// ファイル名
		String reportName = null;
		// ダウンロードファイル名文字コード変更用
		String downloadFileName = null;

		try {

			// ダウンロードファイル名作成
			SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			reportName = systemInfoDao.findAll().getSystem_name() + "アクセスログ" + CommonConst.HYPHEN + start_date.replaceAll("[^0-9]","") + "～" + end_date.replaceAll("[^0-9]","") + CommonConst.HYPHEN + user_id + fmt.format(new Date()) + CommonConst.CSV;
			// URLエンコード
	        try {
	        	downloadFileName = URLEncoder.encode(reportName, "UTF-8");
			}catch (UnsupportedEncodingException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			// アクセスログ(csv)
			List<AccessLogCsvModel> accessLogCsvModel = accessLogDao.findCsv(start_date, end_date, strTenantid, mngAccessedLogForm.getUser_id(), mngAccessedLogForm.getExecution_code(), mngAccessedLogForm.getResult_code());

			CsvMapper mapper = new CsvMapper();
			// データをダブルクォーテーションで囲う
			mapper.configure(CsvGenerator.Feature.ALWAYS_QUOTE_STRINGS, true);

			// ヘッダを付ける
			CsvSchema schema = mapper.schemaFor(AccessLogCsvModel.class).withHeader();
			String csvText = mapper.writer(schema).writeValueAsString(accessLogCsvModel);

			// ファイル書き込み
			response.setContentType("application/octet-stream;charset=MS932");
			response.setHeader("Content-Disposition", "attachment; filename=" + downloadFileName);

			// CSVファイル出力
			try (PrintWriter pw = response.getWriter()) {
	            pw.print(csvText);
	        }

			//アクセスログ書き込み
			StringBuilder msg = new StringBuilder();
			msg.append("検索期間 = ");
			msg.append(start_date);
			msg.append(" ～ ");
			msg.append(end_date);
			msg.append("\n検索ユーザID = ");
			msg.append(mngAccessedLogForm.getUser_id());
			msg.append("\n検索実行プログラムコード = ");
			msg.append(mngAccessedLogForm.getExecution_code());
			msg.append("\n検索処理結果コード = ");
			msg.append(mngAccessedLogForm.getResult_code());
			msg.append("\ncsvファイル名 = ");
			msg.append(reportName);
			log.info("@@MngAccessedLogCtrl.searchAccesslogCsv csvダウンロード実行：" + "検索期間 = " + start_date + " ～ " + end_date + ", 検索ユーザID = " + mngAccessedLogForm.getUser_id() + ", 検索実行プログラムコード = " + mngAccessedLogForm.getExecution_code() + ", 検索処理結果コード = " + mngAccessedLogForm.getResult_code() + ", csvファイル名 = " + reportName);
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_ACCESSEDLOGCTRL_SEARCHACCESSLOG_CSV, CommonConst.SUCCESS_CO, msg.toString(), "");

		}catch(Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			String str = sw.toString();
			//アクセスログ書き込み
			StringBuilder msg = new StringBuilder();
			msg.append("検索期間 = ");
			msg.append(start_date);
			msg.append(" ～ ");
			msg.append(end_date);
			msg.append("\n検索ユーザID = ");
			msg.append(mngAccessedLogForm.getUser_id());
			msg.append("\n検索実行プログラムコード = ");
			msg.append(mngAccessedLogForm.getExecution_code());
			msg.append("\n検索処理結果コード = ");
			msg.append(mngAccessedLogForm.getResult_code());
			msg.append("\ncsvファイル名 = ");
			msg.append(reportName);
			log.info("@@MngAccessedLogCtrl.searchAccesslogCsv csvダウンロード失敗：" + "検索期間 = " + start_date + " ～ " + end_date + ", 検索ユーザID = " + mngAccessedLogForm.getUser_id() + ", 検索実行プログラムコード = " + mngAccessedLogForm.getExecution_code() + ", 検索処理結果コード = " + mngAccessedLogForm.getResult_code() + ", csvファイル名 = " + reportName + "\n" + str);
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_ACCESSEDLOGCTRL_SEARCHACCESSLOG_CSV, CommonConst.FAILED_CO, msg.toString(), str);
		}
	}

	/**
	 * アクセスログ検索（アクセス番号から検索）<br>
	 * @param accessNo アクセス番号
	 * @return アクセスログ Modelオブジェクト
	 * @throws Exception
	 */
	@RequestMapping(params="accessNoSelect", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String accessNoSelect(@RequestParam String acc_userID, String accessNo, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_ACCESSEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_ACCESSEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_ACCESSEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		String json = null;

		AccessLogModel accessLog = accessLogDao.accessNoSelect(accessNo);

		if (accessLog != null) {
			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(accessLog);
		}

		return json;
	}

}