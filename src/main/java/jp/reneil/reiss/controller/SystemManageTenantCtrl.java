/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込み変更。TripleDES変換クラス宣言(インスタンス化)変更。
 *    3    2019/10/23      1.0.2           女屋            不要メンバ変数削除と修正
 *    4    2020/02/14      1.0.3           女屋            地図有効化機能追加
 *    5    2020/02/20      1.0.4           女屋            帳票有効化機能追加
 */
package jp.reneil.reiss.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.TenantDao;
import jp.reneil.reiss.dao.TerminalDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.exception.ExException;
import jp.reneil.reiss.form.SystemManageTenantForm;
import jp.reneil.reiss.log.LogWriting;
import jp.reneil.reiss.model.AnythingModel;
import jp.reneil.reiss.model.SystemInfoModel;
import jp.reneil.reiss.model.TenantModel;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * システム管理_テナント_画面コントローラクラスです。<br>
 *
 * @version 1.0.4
 */
@Controller
@RequestMapping(value="systemManage/tenant")
public class SystemManageTenantCtrl {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(SystemManageTenantCtrl.class);

	/** ログ書き込みクラス */
	@Autowired
	private LogWriting logWriting;

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** テナントテーブルアクセス制御クラス */
	@Autowired
	private TenantDao tenantDao;

	/** ユーザ情報テーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/** 端末情報テーブルアクセス制御クラス */
	@Autowired
	private TerminalDao terminalDao;

	/** 画面表示
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(method=GET)
	public String init(@ModelAttribute("systemManageTenantForm") SystemManageTenantForm systemManageTenantForm, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_TENANTCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_TENANTCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) != CommonConst.SYSTEM_MANAGER) {
			log.info("@@SystemManageTenantCtrl.init アクセス権限違反[USER_ID=" + SessionConst.USER_ID + "]");
			throw new ExException(MessageConst.W015);

		}

		// セッション情報からログインユーザIDを取得
		String user_id = (String) session.getAttribute(SessionConst.USER_ID);
		log.info("@@SystemManageTenantCtrl.init セッション情報取得[USER_ID=" + user_id + "]");

		// システム名
		model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());
		// 暗号化ユーザID
		model.addAttribute("acc_userID", tripleDesConverter.encrypt64(user_id));
		// 一時ファイル時間経過削除
		List<AnythingModel> anythingList = new ArrayList<AnythingModel>();
		AnythingModel anythingModel = new AnythingModel();
		for (int i = 0; i <= 99; i++) {
			anythingModel = new AnythingModel();
			if (i == 0) {
				anythingModel.setLabel("無効");
			}else {
				anythingModel.setLabel(String.valueOf(i) + "時間後");
			}
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("tmpDelTime",anythingList);
		// アプリ自動停止時間
		anythingList = new ArrayList<AnythingModel>();
		for (int i = 0; i <= 60; i++) {
			anythingModel = new AnythingModel();
			if (i == 0) {
				anythingModel.setLabel("無効");
			}else {
				anythingModel.setLabel(String.valueOf(i) + "分後");
			}
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("stopTime",anythingList);
		// パスワードエラーロック回数
		anythingList = new ArrayList<AnythingModel>();
		for (int i = 0; i <= 99; i++) {
			anythingModel = new AnythingModel();
			if (i == 0) {
				anythingModel.setLabel("無効");
			}else {
				anythingModel.setLabel(String.valueOf(i) + "回");
			}
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("pwdErrCount",anythingList);
		// パトライト-鳴動時間
		anythingList = new ArrayList<AnythingModel>();
		for (int i = 0; i <= 60; i++) {
			anythingModel = new AnythingModel();
			if (i == 0) {
				anythingModel.setLabel("無効");
			}else {
				anythingModel.setLabel(String.valueOf(i) + "秒");
			}
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("patLiteTime",anythingList);
		// テナント情報取得
		model.addAttribute("list", tenantDao.findAll());

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), "null", user_id, CommonConst.SYS_TENANTCTRL_INIT, CommonConst.SUCCESS_CO, "", "");

		return "systemManage/tenant";
	}

	/**
	 * 追加ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="add", method=POST)
	public String add(@ModelAttribute("systemManageTenantForm") SystemManageTenantForm systemManageTenantForm, BindingResult result, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_TENANTCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_TENANTCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) != CommonConst.SYSTEM_MANAGER) {
			log.info("@@SystemManageTenantCtrl.add アクセス権限違反[USER_ID=" + SessionConst.USER_ID + "]");
			throw new ExException(MessageConst.W015);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(systemManageTenantForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageTenantForm.getAcc_userID()), CommonConst.SYS_TENANTCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		// システム情報取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();

		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());

		// フォームからモデルに転記
		TenantModel tenantModel = setTenantModel(systemManageTenantForm);

		// フォームのテナントIDが既にテーブルにあるか確認
		if (tenantDao.count(systemManageTenantForm.getTenant_id()) == 0 ) {
			// 存在しないとき（追加）
			tenantDao.insert(tenantModel);
			// 物理フォルダ作成
			Path libFolder = Paths.get(systeminfoModel.getSystem_rootpath() + CommonConst.YEN_MRK + CommonConst.LIBRARY + CommonConst.YEN_MRK + tenantModel.getTenant_id());
			Path thuFolder = Paths.get(systeminfoModel.getSystem_rootpath() + CommonConst.YEN_MRK + CommonConst.THUMBNAIL + CommonConst.YEN_MRK + tenantModel.getTenant_id());
			Path repFolder = Paths.get(systeminfoModel.getSystem_rootpath() + CommonConst.YEN_MRK + CommonConst.REPORT + CommonConst.YEN_MRK + tenantModel.getTenant_id());
			//ライブラリフォルダ作成
			//失敗したらリトライ
			for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
				try {
					Files.createDirectories(libFolder);
					if (Files.exists(libFolder)) {
						//ライブラリフォルダ作成に成功したらループから抜ける
						break;
					}else {
						//ライブラリフォルダ作成失敗
						log.info("@@SystemManageTenantCtrl.add " + libFolder.toAbsolutePath().toString() + " ライブラリフォルダ作成失敗回数 = " + (i + 1));
						try
						{
							Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
						}
						catch(InterruptedException ex){}
					}
				}catch (Exception e) {
					// ライブラリフォルダ作成失敗(例外)
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw);
					e.printStackTrace(pw);
					pw.flush();
					String str = sw.toString();
					log.info("@@SystemManageTenantCtrl.add " + libFolder.toAbsolutePath().toString() + " ライブラリフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
					try
					{
						Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
					}
					catch(InterruptedException ex){}
				}
			}
			//サムネイルフォルダ作成
			//失敗したらリトライ
			for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
				try {
					Files.createDirectories(thuFolder);
					if (Files.exists(thuFolder)) {
						//サムネイルフォルダ作成に成功したらループから抜ける
						break;
					}else {
						//サムネイルフォルダ作成失敗
						log.info("@@SystemManageTenantCtrl.add " + thuFolder.toAbsolutePath().toString() + " サムネイルフォルダ作成失敗回数 = " + (i + 1));
						try
						{
							Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
						}
						catch(InterruptedException ex){}
					}
				}catch (Exception e) {
					// サムネイルフォルダ作成失敗(例外)
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw);
					e.printStackTrace(pw);
					pw.flush();
					String str = sw.toString();
					log.info("@@SystemManageTenantCtrl.add " + thuFolder.toAbsolutePath().toString() + " サムネイルフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
					try
					{
						Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
					}
					catch(InterruptedException ex){}
				}
			}
			//帳票テンプレートフォルダ作成
			//失敗したらリトライ
			for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
				try {
					Files.createDirectories(repFolder);
					if (Files.exists(repFolder)) {
						// 帳票テンプレートフォルダ作成に成功したらループから抜ける
						break;
					}else {
						// 帳票テンプレートフォルダ作成失敗
						log.info("@@SystemManageTenantCtrl.add " + repFolder.toAbsolutePath().toString() + " 帳票テンプレートフォルダ作成失敗回数 = " + (i + 1));
						try
						{
							Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
						}
						catch(InterruptedException ex){}
					}
				}catch (Exception e) {
					// 帳票テンプレートフォルダ作成失敗(例外)
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw);
					e.printStackTrace(pw);
					pw.flush();
					String str = sw.toString();
					log.info("@@SystemManageTenantCtrl.add " + repFolder.toAbsolutePath().toString() + " 帳票テンプレートフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
					try
					{
						Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
					}
					catch(InterruptedException ex){}
				}
			}

			log.info("@@SystemManageTenantCtrl.add テナント情報テーブルを追加しました(テナントID = " + tenantModel.getTenant_id()
					+ ", テナント名 = " + tenantModel.getTenant_name()
					+ ", ライセンス数 = ユーザー:" + tenantModel.getUser_license() + " 端末:" + tenantModel.getTerm_license()
					+ ", 端末-静止画撮影機能 = " + tenantModel.getStillimage_function()
					+ ", 端末-動画撮影機能 = " + tenantModel.getMovie_function()
					+ ", 端末-ライブ撮影機能 = " + tenantModel.getLive_function()
					+ ", 端末-起動時一時ファイル削除 = " + tenantModel.getStart_tmp_del()
					+ ", 端末-一時ファイル時間経過削除(時間) = " + tenantModel.getTmp_del_time()
					+ ", 端末-アプリ自動停止時間(分) = " + tenantModel.getStop_time()
					+ ", パスワードエラーロック回数 = " + tenantModel.getPassword_lock_times()
					+ ", サムネイルサイズ(pixel) = 幅:" + tenantModel.getThumbnail_width() + " 高さ:" + tenantModel.getThumbnail_height()
					+ ", パトライト-有効化 = " + tenantModel.getPatlite_enable()
					+ ", パトライト-アドレス = " + tenantModel.getPatLite_ip()
					+ ", パトライト-ログイン名 = " + tenantModel.getPatLite_login_name()
					+ ", パトライト-受信時オプション = " + tenantModel.getPatLite_option_rcv()
					+ ", パトライト-受信時鳴動時間(秒) = " + tenantModel.getPatLite_time_rcv()
					+ ", パトライト-警告時オプション = " + tenantModel.getPatLite_option_warm()
					+ ", パトライト-警告時鳴動時間(秒) = " + tenantModel.getPatLite_time_warm()
					+ ", パトライト-エラー時オプション = " + tenantModel.getPatLite_option_err()
					+ ", パトライト-エラー時鳴動時間(秒) = " + tenantModel.getPatLite_time_err()
					+ ", ライブ接続先 = " + tenantModel.getLive_server()
					+ ", ライブアプリ名 = " + tenantModel.getLive_app()
					+ ", ライブルーム名 = " + tenantModel.getLive_room()
					+ ", 地図有効化 = " + tenantModel.getMap_enable()
					+ ", 地図サーバ = " + tenantModel.getMap_server()
					+ ", 帳票有効化 = " + tenantModel.getReport_enable()
					+ ", ファイルパス表示 = " + tenantModel.getDisplay_file_path()
					+ ", 有効期限 = " + systemManageTenantForm.getStart_date() + " ～ " + systemManageTenantForm.getEnd_date()
					+ ", 削除フラグ = " + tenantModel.getDel_flg() + ")");

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageTenantForm.getAcc_userID()), CommonConst.SYS_TENANTCTRL_ADD, CommonConst.SUCCESS_CO,
					  "テナントID = " + tenantModel.getTenant_id()
					+ "\nテナント名 = " + tenantModel.getTenant_name()
					+ "\nライセンス数 = ユーザー:" + tenantModel.getUser_license() + " 端末:" + tenantModel.getTerm_license()
					+ "\n端末-静止画撮影機能 = " + tenantModel.getStillimage_function()
					+ "\n端末-動画撮影機能 = " + tenantModel.getMovie_function()
					+ "\n端末-ライブ撮影機能 = " + tenantModel.getLive_function()
					+ "\n端末-起動時一時ファイル削除 = " + tenantModel.getStart_tmp_del()
					+ "\n端末-一時ファイル時間経過削除(時間) = " + tenantModel.getTmp_del_time()
					+ "\n端末-アプリ自動停止時間(分) = " + tenantModel.getStop_time()
					+ "\nパスワードエラーロック回数 = " + tenantModel.getPassword_lock_times()
					+ "\nサムネイルサイズ(pixel) = 幅:" + tenantModel.getThumbnail_width() + " 高さ:" + tenantModel.getThumbnail_height()
					+ "\nパトライト-有効化 = " + tenantModel.getPatlite_enable()
					+ "\nパトライト-アドレス = " + tenantModel.getPatLite_ip()
					+ "\nパトライト-ログイン名 = " + tenantModel.getPatLite_login_name()
					+ "\nパトライト-受信時オプション = " + tenantModel.getPatLite_option_rcv()
					+ "\nパトライト-受信時鳴動時間(秒) = " + tenantModel.getPatLite_time_rcv()
					+ "\nパトライト-警告時オプション = " + tenantModel.getPatLite_option_warm()
					+ "\nパトライト-警告時鳴動時間(秒) = " + tenantModel.getPatLite_time_warm()
					+ "\nパトライト-エラー時オプション = " + tenantModel.getPatLite_option_err()
					+ "\nパトライト-エラー時鳴動時間(秒) = " + tenantModel.getPatLite_time_err()
					+ "\nライブ接続先 = " + tenantModel.getLive_server()
					+ "\nライブアプリ名 = " + tenantModel.getLive_app()
					+ "\nライブルーム名 = " + tenantModel.getLive_room()
					+ "\n地図有効化 = " + tenantModel.getMap_enable()
					+ "\n地図サーバ = " + tenantModel.getMap_server()
					+ "\n帳票有効化 = " + tenantModel.getReport_enable()
					+ "\nファイルパス表示 = " + tenantModel.getDisplay_file_path()
					+ "\n有効期限 = " + systemManageTenantForm.getStart_date() + " ～ " + systemManageTenantForm.getEnd_date()
					+ "\n削除フラグ = " + tenantModel.getDel_flg(), "");

			// successメッセージ
			model.addAttribute("success", MessageConst.M001);

		}else {
			// 存在するとき

			log.info("@@SystemManageTenantCtrl.add " + MessageConst.W520 + "(テナントID = " + tenantModel.getTenant_id()
					+ ", テナント名 = " + tenantModel.getTenant_name()
					+ ", ライセンス数 = ユーザー:" + tenantModel.getUser_license() + " 端末:" + tenantModel.getTerm_license()
					+ ", 端末-静止画撮影機能 = " + tenantModel.getStillimage_function()
					+ ", 端末-動画撮影機能 = " + tenantModel.getMovie_function()
					+ ", 端末-ライブ撮影機能 = " + tenantModel.getLive_function()
					+ ", 端末-起動時一時ファイル削除 = " + tenantModel.getStart_tmp_del()
					+ ", 端末-一時ファイル時間経過削除(時間) = " + tenantModel.getTmp_del_time()
					+ ", 端末-アプリ自動停止時間(分) = " + tenantModel.getStop_time()
					+ ", パスワードエラーロック回数 = " + tenantModel.getPassword_lock_times()
					+ ", サムネイルサイズ(pixel) = 幅:" + tenantModel.getThumbnail_width() + " 高さ:" + tenantModel.getThumbnail_height()
					+ ", パトライト-有効化 = " + tenantModel.getPatlite_enable()
					+ ", パトライト-アドレス = " + tenantModel.getPatLite_ip()
					+ ", パトライト-ログイン名 = " + tenantModel.getPatLite_login_name()
					+ ", パトライト-受信時オプション = " + tenantModel.getPatLite_option_rcv()
					+ ", パトライト-受信時鳴動時間(秒) = " + tenantModel.getPatLite_time_rcv()
					+ ", パトライト-警告時オプション = " + tenantModel.getPatLite_option_warm()
					+ ", パトライト-警告時鳴動時間(秒) = " + tenantModel.getPatLite_time_warm()
					+ ", パトライト-エラー時オプション = " + tenantModel.getPatLite_option_err()
					+ ", パトライト-エラー時鳴動時間(秒) = " + tenantModel.getPatLite_time_err()
					+ ", ライブ接続先 = " + tenantModel.getLive_server()
					+ ", ライブアプリ名 = " + tenantModel.getLive_app()
					+ ", ライブルーム名 = " + tenantModel.getLive_room()
					+ ", 地図有効化 = " + tenantModel.getMap_enable()
					+ ", 地図サーバ = " + tenantModel.getMap_server()
					+ ", 帳票有効化 = " + tenantModel.getReport_enable()
					+ ", ファイルパス表示 = " + tenantModel.getDisplay_file_path()
					+ ", 有効期限 = " + systemManageTenantForm.getStart_date() + " ～ " + systemManageTenantForm.getEnd_date()
					+ ", 削除フラグ = " + tenantModel.getDel_flg() + ")");

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageTenantForm.getAcc_userID()), CommonConst.SYS_TENANTCTRL_ADD, CommonConst.FAILED_CO,
					  "テナントID = " + tenantModel.getTenant_id()
					+ "\nテナント名 = " + tenantModel.getTenant_name()
					+ "\nライセンス数 = ユーザー:" + tenantModel.getUser_license() + " 端末:" + tenantModel.getTerm_license()
					+ "\n端末-静止画撮影機能 = " + tenantModel.getStillimage_function()
					+ "\n端末-動画撮影機能 = " + tenantModel.getMovie_function()
					+ "\n端末-ライブ撮影機能 = " + tenantModel.getLive_function()
					+ "\n端末-起動時一時ファイル削除 = " + tenantModel.getStart_tmp_del()
					+ "\n端末-一時ファイル時間経過削除(時間) = " + tenantModel.getTmp_del_time()
					+ "\n端末-アプリ自動停止時間(分) = " + tenantModel.getStop_time()
					+ "\nパスワードエラーロック回数 = " + tenantModel.getPassword_lock_times()
					+ "\nサムネイルサイズ(pixel) = 幅:" + tenantModel.getThumbnail_width() + " 高さ:" + tenantModel.getThumbnail_height()
					+ "\nパトライト-有効化 = " + tenantModel.getPatlite_enable()
					+ "\nパトライト-アドレス = " + tenantModel.getPatLite_ip()
					+ "\nパトライト-ログイン名 = " + tenantModel.getPatLite_login_name()
					+ "\nパトライト-受信時オプション = " + tenantModel.getPatLite_option_rcv()
					+ "\nパトライト-受信時鳴動時間(秒) = " + tenantModel.getPatLite_time_rcv()
					+ "\nパトライト-警告時オプション = " + tenantModel.getPatLite_option_warm()
					+ "\nパトライト-警告時鳴動時間(秒) = " + tenantModel.getPatLite_time_warm()
					+ "\nパトライト-エラー時オプション = " + tenantModel.getPatLite_option_err()
					+ "\nパトライト-エラー時鳴動時間(秒) = " + tenantModel.getPatLite_time_err()
					+ "\nライブ接続先 = " + tenantModel.getLive_server()
					+ "\nライブアプリ名 = " + tenantModel.getLive_app()
					+ "\nライブルーム名 = " + tenantModel.getLive_room()
					+ "\n地図有効化 = " + tenantModel.getMap_enable()
					+ "\n地図サーバ = " + tenantModel.getMap_server()
					+ "\n帳票有効化 = " + tenantModel.getReport_enable()
					+ "\nファイルパス表示 = " + tenantModel.getDisplay_file_path()
					+ "\n有効期限 = " + systemManageTenantForm.getStart_date() + " ～ " + systemManageTenantForm.getEnd_date()
					+ "\n削除フラグ = " + tenantModel.getDel_flg(), MessageConst.W520);

			// warningメッセージ
			model.addAttribute("add_danger", MessageConst.W520);

		}

		// 一時ファイル時間経過削除
		List<AnythingModel> anythingList = new ArrayList<AnythingModel>();
		AnythingModel anythingModel = new AnythingModel();
		for (int i = 0; i <= 99; i++) {
			anythingModel = new AnythingModel();
			if (i == 0) {
				anythingModel.setLabel("無効");
			}else {
				anythingModel.setLabel(String.valueOf(i) + "時間後");
			}
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("tmpDelTime",anythingList);
		// アプリ自動停止時間
		anythingList = new ArrayList<AnythingModel>();
		for (int i = 0; i <= 60; i++) {
			anythingModel = new AnythingModel();
			if (i == 0) {
				anythingModel.setLabel("無効");
			}else {
				anythingModel.setLabel(String.valueOf(i) + "分後");
			}
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("stopTime",anythingList);
		// パスワードエラーロック回数
		anythingList = new ArrayList<AnythingModel>();
		for (int i = 0; i <= 99; i++) {
			anythingModel = new AnythingModel();
			if (i == 0) {
				anythingModel.setLabel("無効");
			}else {
				anythingModel.setLabel(String.valueOf(i) + "回");
			}
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("pwdErrCount",anythingList);
		// パトライト-鳴動時間
		anythingList = new ArrayList<AnythingModel>();
		for (int i = 0; i <= 60; i++) {
			anythingModel = new AnythingModel();
			if (i == 0) {
				anythingModel.setLabel("無効");
			}else {
				anythingModel.setLabel(String.valueOf(i) + "秒");
			}
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("patLiteTime",anythingList);
		// テナント情報取得
		model.addAttribute("list", tenantDao.findAll());

		return "systemManage/tenant";
	}

	/**
	 * 更新ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="update", method=POST)
	public String update(@ModelAttribute("systemManageTenantForm") SystemManageTenantForm systemManageTenantForm, BindingResult result, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_TENANTCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_TENANTCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) != CommonConst.SYSTEM_MANAGER) {
			log.info("@@SystemManageTenantCtrl.update アクセス権限違反[USER_ID=" + SessionConst.USER_ID + "]");
			throw new ExException(MessageConst.W015);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(systemManageTenantForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageTenantForm.getAcc_userID()), CommonConst.SYS_TENANTCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		// システム情報取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();

		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());

		// フォームからモデルに転記
		TenantModel tenantModel = setTenantModel(systemManageTenantForm);

		// フォームのテナントIDが既にテーブルにあるか確認
		if (tenantDao.count(systemManageTenantForm.getTenant_id()) == 0 ) {
			// 存在しないとき（追加）
			tenantDao.insert(tenantModel);
			// 物理フォルダ作成
			Path libFolder = Paths.get(systeminfoModel.getSystem_rootpath() + CommonConst.YEN_MRK + CommonConst.LIBRARY + CommonConst.YEN_MRK + tenantModel.getTenant_id());
			Path thuFolder = Paths.get(systeminfoModel.getSystem_rootpath() + CommonConst.YEN_MRK + CommonConst.THUMBNAIL + CommonConst.YEN_MRK + tenantModel.getTenant_id());
			Path repFolder = Paths.get(systeminfoModel.getSystem_rootpath() + CommonConst.YEN_MRK + CommonConst.REPORT + CommonConst.YEN_MRK + tenantModel.getTenant_id());
			//ライブラリフォルダ作成
			//失敗したらリトライ
			for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
				try {
					Files.createDirectories(libFolder);
					if (Files.exists(libFolder)) {
						//ライブラリフォルダ作成に成功したらループから抜ける
						break;
					}else {
						//ライブラリフォルダ作成失敗
						log.info("@@SystemManageTenantCtrl.update " + libFolder.toAbsolutePath().toString() + " ライブラリフォルダ作成失敗回数 = " + (i + 1));
						try
						{
							Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
						}
						catch(InterruptedException ex){}
					}
				}catch (Exception e) {
					// ライブラリフォルダ作成失敗(例外)
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw);
					e.printStackTrace(pw);
					pw.flush();
					String str = sw.toString();
					log.info("@@SystemManageTenantCtrl.update " + libFolder.toAbsolutePath().toString() + " ライブラリフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
					try
					{
						Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
					}
					catch(InterruptedException ex){}
				}
			}
			//サムネイルフォルダ作成
			//失敗したらリトライ
			for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
				try {
					Files.createDirectories(thuFolder);
					if (Files.exists(thuFolder)) {
						//サムネイルフォルダ作成に成功したらループから抜ける
						break;
					}else {
						//サムネイルフォルダ作成失敗
						log.info("@@SystemManageTenantCtrl.update " + thuFolder.toAbsolutePath().toString() + " サムネイルフォルダ作成失敗回数 = " + (i + 1));
						try
						{
							Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
						}
						catch(InterruptedException ex){}
					}
				}catch (Exception e) {
					// サムネイルフォルダ作成失敗(例外)
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw);
					e.printStackTrace(pw);
					pw.flush();
					String str = sw.toString();
					log.info("@@SystemManageTenantCtrl.update " + thuFolder.toAbsolutePath().toString() + " サムネイルフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
					try
					{
						Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
					}
					catch(InterruptedException ex){}
				}
			}
			//帳票テンプレートフォルダ作成
			//失敗したらリトライ
			for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
				try {
					Files.createDirectories(repFolder);
					if (Files.exists(repFolder)) {
						// 帳票テンプレートフォルダ作成に成功したらループから抜ける
						break;
					}else {
						// 帳票テンプレートフォルダ作成失敗
						log.info("@@SystemManageTenantCtrl.update " + repFolder.toAbsolutePath().toString() + " 帳票テンプレートフォルダ作成失敗回数 = " + (i + 1));
						try
						{
							Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
						}
						catch(InterruptedException ex){}
					}
				}catch (Exception e) {
					// 帳票テンプレートフォルダ作成失敗(例外)
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw);
					e.printStackTrace(pw);
					pw.flush();
					String str = sw.toString();
					log.info("@@SystemManageTenantCtrl.update " + repFolder.toAbsolutePath().toString() + " 帳票テンプレートフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
					try
					{
						Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
					}
					catch(InterruptedException ex){}
				}
			}

			log.info("@@SystemManageTenantCtrl.update テナント情報テーブルを追加しました(テナントID = " + tenantModel.getTenant_id()
					+ ", テナント名 = " + tenantModel.getTenant_name()
					+ ", ライセンス数 = ユーザー:" + tenantModel.getUser_license() + " 端末:" + tenantModel.getTerm_license()
					+ ", 端末-静止画撮影機能 = " + tenantModel.getStillimage_function()
					+ ", 端末-動画撮影機能 = " + tenantModel.getMovie_function()
					+ ", 端末-ライブ撮影機能 = " + tenantModel.getLive_function()
					+ ", 端末-起動時一時ファイル削除 = " + tenantModel.getStart_tmp_del()
					+ ", 端末-一時ファイル時間経過削除(時間) = " + tenantModel.getTmp_del_time()
					+ ", 端末-アプリ自動停止時間(分) = " + tenantModel.getStop_time()
					+ ", パスワードエラーロック回数 = " + tenantModel.getPassword_lock_times()
					+ ", サムネイルサイズ(pixel) = 幅:" + tenantModel.getThumbnail_width() + " 高さ:" + tenantModel.getThumbnail_height()
					+ ", パトライト-有効化 = " + tenantModel.getPatlite_enable()
					+ ", パトライト-アドレス = " + tenantModel.getPatLite_ip()
					+ ", パトライト-ログイン名 = " + tenantModel.getPatLite_login_name()
					+ ", パトライト-受信時オプション = " + tenantModel.getPatLite_option_rcv()
					+ ", パトライト-受信時鳴動時間(秒) = " + tenantModel.getPatLite_time_rcv()
					+ ", パトライト-警告時オプション = " + tenantModel.getPatLite_option_warm()
					+ ", パトライト-警告時鳴動時間(秒) = " + tenantModel.getPatLite_time_warm()
					+ ", パトライト-エラー時オプション = " + tenantModel.getPatLite_option_err()
					+ ", パトライト-エラー時鳴動時間(秒) = " + tenantModel.getPatLite_time_err()
					+ ", ライブ接続先 = " + tenantModel.getLive_server()
					+ ", ライブアプリ名 = " + tenantModel.getLive_app()
					+ ", ライブルーム名 = " + tenantModel.getLive_room()
					+ ", 地図有効化 = " + tenantModel.getMap_enable()
					+ ", 地図サーバ = " + tenantModel.getMap_server()
					+ ", 帳票有効化 = " + tenantModel.getReport_enable()
					+ ", ファイルパス表示 = " + tenantModel.getDisplay_file_path()
					+ ", 有効期限 = " + systemManageTenantForm.getStart_date() + " ～ " + systemManageTenantForm.getEnd_date()
					+ ", 削除フラグ = " + tenantModel.getDel_flg() + ")");

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageTenantForm.getAcc_userID()), CommonConst.SYS_TENANTCTRL_ADD, CommonConst.SUCCESS_CO,
					  "テナントID = " + tenantModel.getTenant_id()
					+ "\nテナント名 = " + tenantModel.getTenant_name()
					+ "\nライセンス数 = ユーザー:" + tenantModel.getUser_license() + " 端末:" + tenantModel.getTerm_license()
					+ "\n端末-静止画撮影機能 = " + tenantModel.getStillimage_function()
					+ "\n端末-動画撮影機能 = " + tenantModel.getMovie_function()
					+ "\n端末-ライブ撮影機能 = " + tenantModel.getLive_function()
					+ "\n端末-起動時一時ファイル削除 = " + tenantModel.getStart_tmp_del()
					+ "\n端末-一時ファイル時間経過削除(時間) = " + tenantModel.getTmp_del_time()
					+ "\n端末-アプリ自動停止時間(分) = " + tenantModel.getStop_time()
					+ "\nパスワードエラーロック回数 = " + tenantModel.getPassword_lock_times()
					+ "\nサムネイルサイズ(pixel) = 幅:" + tenantModel.getThumbnail_width() + " 高さ:" + tenantModel.getThumbnail_height()
					+ "\nパトライト-有効化 = " + tenantModel.getPatlite_enable()
					+ "\nパトライト-アドレス = " + tenantModel.getPatLite_ip()
					+ "\nパトライト-ログイン名 = " + tenantModel.getPatLite_login_name()
					+ "\nパトライト-受信時オプション = " + tenantModel.getPatLite_option_rcv()
					+ "\nパトライト-受信時鳴動時間(秒) = " + tenantModel.getPatLite_time_rcv()
					+ "\nパトライト-警告時オプション = " + tenantModel.getPatLite_option_warm()
					+ "\nパトライト-警告時鳴動時間(秒) = " + tenantModel.getPatLite_time_warm()
					+ "\nパトライト-エラー時オプション = " + tenantModel.getPatLite_option_err()
					+ "\nパトライト-エラー時鳴動時間(秒) = " + tenantModel.getPatLite_time_err()
					+ "\nライブ接続先 = " + tenantModel.getLive_server()
					+ "\nライブアプリ名 = " + tenantModel.getLive_app()
					+ "\nライブルーム名 = " + tenantModel.getLive_room()
					+ "\n地図有効化 = " + tenantModel.getMap_enable()
					+ "\n地図サーバ = " + tenantModel.getMap_server()
					+ "\n帳票有効化 = " + tenantModel.getReport_enable()
					+ "\nファイルパス表示 = " + tenantModel.getDisplay_file_path()
					+ "\n有効期限 = " + systemManageTenantForm.getStart_date() + " ～ " + systemManageTenantForm.getEnd_date()
					+ "\n削除フラグ = " + tenantModel.getDel_flg(), "");

			// successメッセージ
			model.addAttribute("success", MessageConst.M001);

		}else {

			// 登録済みユーザー・端末数取得
			int userCount = userInfoDao.licenseCount(tenantModel.getTenant_id());
			int termCount = terminalDao.licenseCount(tenantModel.getTenant_id());

			// 存在するとき
			// 登録済みユーザー・端末ライセンス数確認
			if (tenantModel.getUser_license() >= userCount && tenantModel.getTerm_license() >= termCount) {
				// 更新
				tenantDao.update(tenantModel);

				log.info("@@SystemManageTenantCtrl.update テナント情報テーブルを更新しました(テナントID = " + tenantModel.getTenant_id()
						+ ", テナント名 = " + tenantModel.getTenant_name()
						+ ", ライセンス数 = ユーザー:" + tenantModel.getUser_license() + " 端末:" + tenantModel.getTerm_license()
						+ ", 端末-静止画撮影機能 = " + tenantModel.getStillimage_function()
						+ ", 端末-動画撮影機能 = " + tenantModel.getMovie_function()
						+ ", 端末-ライブ撮影機能 = " + tenantModel.getLive_function()
						+ ", 端末-起動時一時ファイル削除 = " + tenantModel.getStart_tmp_del()
						+ ", 端末-一時ファイル時間経過削除(時間) = " + tenantModel.getTmp_del_time()
						+ ", 端末-アプリ自動停止時間(分) = " + tenantModel.getStop_time()
						+ ", パスワードエラーロック回数 = " + tenantModel.getPassword_lock_times()
						+ ", サムネイルサイズ(pixel) = 幅:" + tenantModel.getThumbnail_width() + " 高さ:" + tenantModel.getThumbnail_height()
						+ ", パトライト-有効化 = " + tenantModel.getPatlite_enable()
						+ ", パトライト-アドレス = " + tenantModel.getPatLite_ip()
						+ ", パトライト-ログイン名 = " + tenantModel.getPatLite_login_name()
						+ ", パトライト-受信時オプション = " + tenantModel.getPatLite_option_rcv()
						+ ", パトライト-受信時鳴動時間(秒) = " + tenantModel.getPatLite_time_rcv()
						+ ", パトライト-警告時オプション = " + tenantModel.getPatLite_option_warm()
						+ ", パトライト-警告時鳴動時間(秒) = " + tenantModel.getPatLite_time_warm()
						+ ", パトライト-エラー時オプション = " + tenantModel.getPatLite_option_err()
						+ ", パトライト-エラー時鳴動時間(秒) = " + tenantModel.getPatLite_time_err()
						+ ", ライブ接続先 = " + tenantModel.getLive_server()
						+ ", ライブアプリ名 = " + tenantModel.getLive_app()
						+ ", ライブルーム名 = " + tenantModel.getLive_room()
						+ ", 地図有効化 = " + tenantModel.getMap_enable()
						+ ", 地図サーバ = " + tenantModel.getMap_server()
						+ ", 帳票有効化 = " + tenantModel.getReport_enable()
						+ ", ファイルパス表示 = " + tenantModel.getDisplay_file_path()
						+ ", 有効期限 = " + systemManageTenantForm.getStart_date() + " ～ " + systemManageTenantForm.getEnd_date()
						+ ", 削除フラグ = " + tenantModel.getDel_flg() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageTenantForm.getAcc_userID()), CommonConst.SYS_TENANTCTRL_UPDATE, CommonConst.SUCCESS_CO,
						  "テナントID = " + tenantModel.getTenant_id()
						+ "\nテナント名 = " + tenantModel.getTenant_name()
						+ "\nライセンス数 = ユーザー:" + tenantModel.getUser_license() + " 端末:" + tenantModel.getTerm_license()
						+ "\n端末-静止画撮影機能 = " + tenantModel.getStillimage_function()
						+ "\n端末-動画撮影機能 = " + tenantModel.getMovie_function()
						+ "\n端末-ライブ撮影機能 = " + tenantModel.getLive_function()
						+ "\n端末-起動時一時ファイル削除 = " + tenantModel.getStart_tmp_del()
						+ "\n端末-一時ファイル時間経過削除(時間) = " + tenantModel.getTmp_del_time()
						+ "\n端末-アプリ自動停止時間(分) = " + tenantModel.getStop_time()
						+ "\nパスワードエラーロック回数 = " + tenantModel.getPassword_lock_times()
						+ "\nサムネイルサイズ(pixel) = 幅:" + tenantModel.getThumbnail_width() + " 高さ:" + tenantModel.getThumbnail_height()
						+ "\nパトライト-有効化 = " + tenantModel.getPatlite_enable()
						+ "\nパトライト-アドレス = " + tenantModel.getPatLite_ip()
						+ "\nパトライト-ログイン名 = " + tenantModel.getPatLite_login_name()
						+ "\nパトライト-受信時オプション = " + tenantModel.getPatLite_option_rcv()
						+ "\nパトライト-受信時鳴動時間(秒) = " + tenantModel.getPatLite_time_rcv()
						+ "\nパトライト-警告時オプション = " + tenantModel.getPatLite_option_warm()
						+ "\nパトライト-警告時鳴動時間(秒) = " + tenantModel.getPatLite_time_warm()
						+ "\nパトライト-エラー時オプション = " + tenantModel.getPatLite_option_err()
						+ "\nパトライト-エラー時鳴動時間(秒) = " + tenantModel.getPatLite_time_err()
						+ "\nライブ接続先 = " + tenantModel.getLive_server()
						+ "\nライブアプリ名 = " + tenantModel.getLive_app()
						+ "\nライブルーム名 = " + tenantModel.getLive_room()
						+ "\n地図有効化 = " + tenantModel.getMap_enable()
						+ "\n地図サーバ = " + tenantModel.getMap_server()
						+ "\n帳票有効化 = " + tenantModel.getReport_enable()
						+ "\nファイルパス表示 = " + tenantModel.getDisplay_file_path()
						+ "\n有効期限 = " + systemManageTenantForm.getStart_date() + " ～ " + systemManageTenantForm.getEnd_date()
						+ "\n削除フラグ = " + tenantModel.getDel_flg(), "");

				// successメッセージ
				model.addAttribute("success", MessageConst.M001);

			}else {

				String ErrMessage = "";

				if (tenantModel.getUser_license() < userCount) {
					// 登録済みユーザー ライセンス数超過
					// warningメッセージ
					model.addAttribute("user_license_danger", MessageConst.W521);
					ErrMessage = MessageConst.W521;
				}
				if (tenantModel.getTerm_license() < termCount) {
					// 登録済み端末 ライセンス数超過
					// warningメッセージ
					model.addAttribute("term_license_danger", MessageConst.W522);
					if (ErrMessage.equals("")) {
						ErrMessage = MessageConst.W522;
					}else {
						ErrMessage += "\n" + MessageConst.W522;
					}
				}

				log.info("@@SystemManageTenantCtrl.update " + ErrMessage + "(テナントID = " + tenantModel.getTenant_id()
						+ ", テナント名 = " + tenantModel.getTenant_name()
						+ ", ライセンス数 = ユーザー:" + tenantModel.getUser_license() + " 端末:" + tenantModel.getTerm_license()
						+ ", 端末-静止画撮影機能 = " + tenantModel.getStillimage_function()
						+ ", 端末-動画撮影機能 = " + tenantModel.getMovie_function()
						+ ", 端末-ライブ撮影機能 = " + tenantModel.getLive_function()
						+ ", 端末-起動時一時ファイル削除 = " + tenantModel.getStart_tmp_del()
						+ ", 端末-一時ファイル時間経過削除(時間) = " + tenantModel.getTmp_del_time()
						+ ", 端末-アプリ自動停止時間(分) = " + tenantModel.getStop_time()
						+ ", パスワードエラーロック回数 = " + tenantModel.getPassword_lock_times()
						+ ", サムネイルサイズ(pixel) = 幅:" + tenantModel.getThumbnail_width() + " 高さ:" + tenantModel.getThumbnail_height()
						+ ", パトライト-有効化 = " + tenantModel.getPatlite_enable()
						+ ", パトライト-アドレス = " + tenantModel.getPatLite_ip()
						+ ", パトライト-ログイン名 = " + tenantModel.getPatLite_login_name()
						+ ", パトライト-受信時オプション = " + tenantModel.getPatLite_option_rcv()
						+ ", パトライト-受信時鳴動時間(秒) = " + tenantModel.getPatLite_time_rcv()
						+ ", パトライト-警告時オプション = " + tenantModel.getPatLite_option_warm()
						+ ", パトライト-警告時鳴動時間(秒) = " + tenantModel.getPatLite_time_warm()
						+ ", パトライト-エラー時オプション = " + tenantModel.getPatLite_option_err()
						+ ", パトライト-エラー時鳴動時間(秒) = " + tenantModel.getPatLite_time_err()
						+ ", ライブ接続先 = " + tenantModel.getLive_server()
						+ ", ライブアプリ名 = " + tenantModel.getLive_app()
						+ ", ライブルーム名 = " + tenantModel.getLive_room()
						+ ", 地図有効化 = " + tenantModel.getMap_enable()
						+ ", 地図サーバ = " + tenantModel.getMap_server()
						+ ", 帳票有効化 = " + tenantModel.getReport_enable()
						+ ", ファイルパス表示 = " + tenantModel.getDisplay_file_path()
						+ ", 有効期限 = " + systemManageTenantForm.getStart_date() + " ～ " + systemManageTenantForm.getEnd_date()
						+ ", 削除フラグ = " + tenantModel.getDel_flg() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(systemManageTenantForm.getAcc_userID()), CommonConst.SYS_TENANTCTRL_UPDATE, CommonConst.FAILED_CO,
						"テナントID = " + tenantModel.getTenant_id()
						+ "\nテナント名 = " + tenantModel.getTenant_name()
						+ "\nライセンス数 = ユーザー:" + tenantModel.getUser_license() + " 端末:" + tenantModel.getTerm_license()
						+ "\n端末-静止画撮影機能 = " + tenantModel.getStillimage_function()
						+ "\n端末-動画撮影機能 = " + tenantModel.getMovie_function()
						+ "\n端末-ライブ撮影機能 = " + tenantModel.getLive_function()
						+ "\n端末-起動時一時ファイル削除 = " + tenantModel.getStart_tmp_del()
						+ "\n端末-一時ファイル時間経過削除(時間) = " + tenantModel.getTmp_del_time()
						+ "\n端末-アプリ自動停止時間(分) = " + tenantModel.getStop_time()
						+ "\nパスワードエラーロック回数 = " + tenantModel.getPassword_lock_times()
						+ "\nサムネイルサイズ(pixel) = 幅:" + tenantModel.getThumbnail_width() + " 高さ:" + tenantModel.getThumbnail_height()
						+ "\nパトライト-有効化 = " + tenantModel.getPatlite_enable()
						+ "\nパトライト-アドレス = " + tenantModel.getPatLite_ip()
						+ "\nパトライト-ログイン名 = " + tenantModel.getPatLite_login_name()
						+ "\nパトライト-受信時オプション = " + tenantModel.getPatLite_option_rcv()
						+ "\nパトライト-受信時鳴動時間(秒) = " + tenantModel.getPatLite_time_rcv()
						+ "\nパトライト-警告時オプション = " + tenantModel.getPatLite_option_warm()
						+ "\nパトライト-警告時鳴動時間(秒) = " + tenantModel.getPatLite_time_warm()
						+ "\nパトライト-エラー時オプション = " + tenantModel.getPatLite_option_err()
						+ "\nパトライト-エラー時鳴動時間(秒) = " + tenantModel.getPatLite_time_err()
						+ "\nライブ接続先 = " + tenantModel.getLive_server()
						+ "\nライブアプリ名 = " + tenantModel.getLive_app()
						+ "\nライブルーム名 = " + tenantModel.getLive_room()
						+ "\n地図有効化 = " + tenantModel.getMap_enable()
						+ "\n地図サーバ = " + tenantModel.getMap_server()
						+ "\n帳票有効化 = " + tenantModel.getReport_enable()
						+ "\nファイルパス表示 = " + tenantModel.getDisplay_file_path()
						+ "\n有効期限 = " + systemManageTenantForm.getStart_date() + " ～ " + systemManageTenantForm.getEnd_date()
						+ "\n削除フラグ = " + tenantModel.getDel_flg(), ErrMessage);

			}

		}

		// 一時ファイル時間経過削除
		List<AnythingModel> anythingList = new ArrayList<AnythingModel>();
		AnythingModel anythingModel = new AnythingModel();
		for (int i = 0; i <= 99; i++) {
			anythingModel = new AnythingModel();
			if (i == 0) {
				anythingModel.setLabel("無効");
			}else {
				anythingModel.setLabel(String.valueOf(i) + "時間後");
			}
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("tmpDelTime",anythingList);
		// アプリ自動停止時間
		anythingList = new ArrayList<AnythingModel>();
		for (int i = 0; i <= 60; i++) {
			anythingModel = new AnythingModel();
			if (i == 0) {
				anythingModel.setLabel("無効");
			}else {
				anythingModel.setLabel(String.valueOf(i) + "分後");
			}
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("stopTime",anythingList);
		// パスワードエラーロック回数
		anythingList = new ArrayList<AnythingModel>();
		for (int i = 0; i <= 99; i++) {
			anythingModel = new AnythingModel();
			if (i == 0) {
				anythingModel.setLabel("無効");
			}else {
				anythingModel.setLabel(String.valueOf(i) + "回");
			}
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("pwdErrCount",anythingList);
		// パトライト-鳴動時間
		anythingList = new ArrayList<AnythingModel>();
		for (int i = 0; i <= 60; i++) {
			anythingModel = new AnythingModel();
			if (i == 0) {
				anythingModel.setLabel("無効");
			}else {
				anythingModel.setLabel(String.valueOf(i) + "秒");
			}
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("patLiteTime",anythingList);
		// テナント情報取得
		model.addAttribute("list", tenantDao.findAll());

		return "systemManage/tenant";
	}

	/**
	 * フォームからモデルに入力情報を展開します。
	 * @param form システム管理_テナント_画面フォーム
	 * @return テナントモデル
	 * @throws ParseException
	 */
	private TenantModel setTenantModel(SystemManageTenantForm form) throws ParseException {
		TenantModel model = new TenantModel();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");

		// テナントID
		model.setTenant_id(form.getTenant_id());
		// テナント名
		model.setTenant_name(form.getTenant_name());
		// ユーザーライセンス数
		model.setUser_license(form.getUser_license());
		// 端末ライセンス数
		model.setTerm_license(form.getTerm_license());
		// 静止画撮影機能
		if (form.getStillimage_function()) {
			model.setStillimage_function((byte) 1);
		} else {
			model.setStillimage_function((byte) 0);
		}
		// 動画撮影機能
		if (form.getMovie_function()) {
			model.setMovie_function((byte) 1);
		} else {
			model.setMovie_function((byte) 0);
		}
		// ライブ撮影機能
		if (form.getLive_function()) {
			model.setLive_function((byte) 1);
		} else {
			model.setLive_function((byte) 0);
		}
		// 起動時一時ファイル削除
		if (form.getStart_tmp_del()) {
			model.setStart_tmp_del((byte) 1);
		} else {
			model.setStart_tmp_del((byte) 0);
		}

		// 一時ファイル時間経過削除
		model.setTmp_del_time(form.getTmp_del_time());
		// アプリ自動停止時間
		model.setStop_time(form.getStop_time());
		// パスワードエラーカウント
		model.setPassword_lock_times(form.getPassword_lock_times());
		// サムネイルサイズ（横）
		model.setThumbnail_width(form.getThumbnail_width());
		// サムネイルサイズ（縦）
		model.setThumbnail_height(form.getThumbnail_height());

		// パトライト有効化
		if (form.getPatlite_enable()) {
			model.setPatlite_enable((byte) 1);
		} else {
			model.setPatlite_enable((byte) 0);
		}

		// パトライトアドレス
		model.setPatLite_ip(form.getPatLite_ip());
		// パトライトログイン名
		model.setPatLite_login_name(form.getPatLite_login_name());
		// パトライト受信時オプション
		model.setPatLite_option_rcv(form.getPatLite_option_rcv());
		// パトライト受信時鳴動時間
		model.setPatLite_time_rcv(form.getPatLite_time_rcv());
		// パトライト警告時オプション
		model.setPatLite_option_warm(form.getPatLite_option_warm());
		// パトライト警告時鳴動時間
		model.setPatLite_time_warm(form.getPatLite_time_warm());
		// パトライトエラー時オプション
		model.setPatLite_option_err(form.getPatLite_option_err());
		// パトライトエラー時鳴動時間
		model.setPatLite_time_err(form.getPatLite_time_err());
		// ライブ接続先
		model.setLive_server(form.getLive_server());
		// ライブアプリ名
		model.setLive_app(form.getLive_app());
		// ライブルーム名
		model.setLive_room(form.getLive_room());

		// 地図有効化
		if (form.getMap_enable()) {
			model.setMap_enable((byte) 1);
		} else {
			model.setMap_enable((byte) 0);
		}

		// 地図サーバ
		model.setMap_server(form.getMap_server());

		// 帳票有効化
		if (form.getReport_enable()) {
			model.setReport_enable((byte) 1);
		} else {
			model.setReport_enable((byte) 0);
		}

		// ファイルパス表示
		if (form.getDisplay_file_path()) {
			model.setDisplay_file_path((byte) 1);
		} else {
			model.setDisplay_file_path((byte) 0);
		}

		// 有効開始日
		model.setStart_date(fmt.parse(form.getStart_date()));
		// 有効終了日
		model.setEnd_date(fmt.parse(form.getEnd_date()));

		// 削除フラグ
		if (form.getDel_flg()) {
			model.setDel_flg((byte) 1);
		} else {
			model.setDel_flg((byte) 0);
		}

		// 更新ユーザーID
		model.setUpdate_user_id(CommonConst.SYSTEM_MANAGER);

		return model;
	}

}