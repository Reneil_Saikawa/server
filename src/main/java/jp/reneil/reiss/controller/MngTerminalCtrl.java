/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込み変更。TripleDES変換クラス宣言(インスタンス化)変更。
 *    3    2019/10/23      1.0.2           女屋            不要メンバ変数削除と修正
 *    4    2020/02/20      1.0.3           女屋            帳票有効化機能追加
 */
package jp.reneil.reiss.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.databind.ObjectMapper;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.CodeDao;
import jp.reneil.reiss.dao.LibraryDao;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.TenantDao;
import jp.reneil.reiss.dao.TerminalDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.exception.ExException;
import jp.reneil.reiss.form.MngTerminalForm;
import jp.reneil.reiss.log.LogWriting;
import jp.reneil.reiss.model.AnythingModel;
import jp.reneil.reiss.model.Directory;
import jp.reneil.reiss.model.LibraryModel;
import jp.reneil.reiss.model.Node;
import jp.reneil.reiss.model.SystemInfoModel;
import jp.reneil.reiss.model.TenantModel;
import jp.reneil.reiss.model.TerminalModel;
import jp.reneil.reiss.model.UserInfoModel;
import jp.reneil.reiss.util.FileUtil;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザ管理_端末_画面コントローラクラスです。<br>
 *
 * @version 1.0.3
 */
@Controller
@RequestMapping(value="mng_terminal")
public class MngTerminalCtrl {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(MngTerminalCtrl.class);

	/** ログ書き込みクラス */
	@Autowired
	private LogWriting logWriting;

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** テナント情報テーブルアクセス制御クラス */
	@Autowired
	private TenantDao tenantDao;

	/** ユーザー情報テーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/** ライブラリ情報テーブルアクセス制御クラス */
	@Autowired
	private LibraryDao librarydao;

	/** 端末情報テーブルアクセス制御クラス */
	@Autowired
	private TerminalDao terminalDao;

	/** コードテーブルアクセス制御クラス */
	@Autowired
	private CodeDao codeDao;

	/** 画面表示
	 * @throws Exception */
	@RequestMapping(method=GET)
	public String init(@ModelAttribute("mngTerminalForm") MngTerminalForm mngterminalForm, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MNG_TERMINALCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MNG_TERMINALCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}

		// セッション情報からログインユーザIDを取得
		String user_id = (String) session.getAttribute(SessionConst.USER_ID);
		log.info("@@MngTerminalCtrl.init セッション情報取得[USER_ID=" + user_id + "]");
		//セッション情報からログインユーザーのテナントIDを取得
		String strTenantid = (String)session.getAttribute(SessionConst.TENANT_ID);
		log.info("@@MngTerminalCtrl.init セッション情報取得[TENANT_ID=" + strTenantid + "]");

		// システム情報を取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();
		// テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);

		// 動画撮影有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_movie_function())) {
			model.addAttribute("movieFunction", tenantModel.getStr_movie_function());
		}
		// ライブ撮影有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_live_function())) {
			model.addAttribute("liveFunction", tenantModel.getStr_live_function());
		}
		// 帳票有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
			model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
		}

		// システムルート＋サムネイルルート＋テナントID
		StringBuilder sb = new StringBuilder(systeminfoModel.getSystem_rootpath());
		sb.append(CommonConst.YEN_MRK);
		sb.append(CommonConst.THUMBNAIL);
		sb.append(CommonConst.YEN_MRK);
		sb.append(strTenantid);
		// ライブラリツリーデータ
		model.addAttribute("libraryTreeData", getLibraryTreeData(sb.toString(), strTenantid));
		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());
		// 暗号化ユーザID
		model.addAttribute("acc_userID", tripleDesConverter.encrypt64(user_id));
		// 端末情報
		model.addAttribute("terminal", terminalDao.findTenantAllTerm(strTenantid));
		// 端末区分
		model.addAttribute("code",codeDao.getCode01Data("03"));
		// 動画撮影最大時間
		List<AnythingModel> anythingList = new ArrayList<AnythingModel>();
		AnythingModel anythingModel = new AnythingModel();
		for (int i = 30; i <= 180; i = i + 30) {
			anythingModel = new AnythingModel();
			anythingModel.setLabel(String.valueOf(i));
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("maxTime",anythingList);
		// ライブフレームレート
		anythingList = new ArrayList<AnythingModel>();
		for (int i = 2; i <= 30; i++) {
			anythingModel = new AnythingModel();
			anythingModel.setLabel(String.valueOf(i));
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("fps", anythingList);
		// システムURL
		model.addAttribute("inUrl", systeminfoModel.getSystem_in_url());
		model.addAttribute("outUrl", systeminfoModel.getSystem_out_url());
		// テナントID
		model.addAttribute("tenantId", strTenantid);
		// テナント有効期限取得
		model.addAttribute("tenantStart", tenantModel.getStr_start_date());
		model.addAttribute("tenantEnd", tenantModel.getStr_end_date());

		// アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_TERMINALCTRL_INIT, CommonConst.SUCCESS_CO, "", "");

		return "mng_terminal";
	}

	/**
	 * 追加ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="add", method=POST)
	public String add(@ModelAttribute("mngTerminalForm") MngTerminalForm mngterminalForm, BindingResult result, Model model, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()), CommonConst.MNG_TERMINALCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()), CommonConst.MNG_TERMINALCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()), CommonConst.MNG_TERMINALCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		// テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);

		// フォームからモデルに転記
		TerminalModel terminalModel = setTerminalModel(mngterminalForm);
		terminalModel.setTenant_id(strTenantid);

		// 端末ライセンス数確認
		if (tenantModel.getTerm_license() > terminalDao.licenseCount(strTenantid)) {
			// ライセンス範囲内
			// フォームのライブラリIDが既にテーブルにあるか確認
			if (terminalDao.count(strTenantid,mngterminalForm.getTerm_id(),"chk") == 0 ) {
				// 存在しないとき（追加）
				terminalDao.insert(terminalModel);

				log.info("@@MngTerminalCtrl.add 端末情報テーブルを追加しました(テナントID = " + strTenantid
						+ ", 端末ID = " + terminalModel.getTerm_id()
						+ ", 端末区分 = " + terminalModel.getTerm_ctg()
						+ ", 端末名 = " + terminalModel.getTerm_name()
						+ ", 保存先ライブラリID = " + terminalModel.getLib_id()
						+ ", 撮影年フォルダ作成 = " + terminalModel.getYear_folder_flg()
						+ ", 撮影月フォルダ作成 = " + terminalModel.getMonth_folder_flg()
						+ ", 撮影日フォルダ作成 = " + terminalModel.getDay_folder_flg()
						+ ", 撮影日付フォルダ日本語 = " + terminalModel.getJapanese_flg()
						+ ", 動画撮影最大時間(秒) = " + terminalModel.getM_max_time()
						+ ", ライブ撮影サイズ(横) = " + terminalModel.getLive_width()
						+ ", ライブフレームレート(fps) = 最小:" + terminalModel.getLive_min_fps() + " 最大:" + terminalModel.getLive_max_fps()
						+ ", 有効期限 = " + mngterminalForm.getStart_date() + " ～ " + mngterminalForm.getEnd_date()
						+ ", 削除フラグ = " + terminalModel.getDel_flg() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()), CommonConst.MNG_TERMINALCTRL_ADD, CommonConst.SUCCESS_CO,
						  "端末ID = " + terminalModel.getTerm_id()
						+ "\n端末区分 = " + terminalModel.getTerm_ctg()
						+ "\n端末名 = " + terminalModel.getTerm_name()
						+ "\n保存先ライブラリID = " + terminalModel.getLib_id()
						+ "\n撮影年フォルダ作成 = " + terminalModel.getYear_folder_flg()
						+ "\n撮影月フォルダ作成 = " + terminalModel.getMonth_folder_flg()
						+ "\n撮影日フォルダ作成 = " + terminalModel.getDay_folder_flg()
						+ "\n撮影日付フォルダ日本語 = " + terminalModel.getJapanese_flg()
						+ "\n動画撮影最大時間(秒) = " + terminalModel.getM_max_time()
						+ "\nライブ撮影サイズ(横) = " + terminalModel.getLive_width()
						+ "\nライブフレームレート(fps) = 最小:" + terminalModel.getLive_min_fps() + " 最大:" + terminalModel.getLive_max_fps()
						+ "\n有効期限 = " + mngterminalForm.getStart_date() + " ～ " + mngterminalForm.getEnd_date()
						+ "\n削除フラグ =" + terminalModel.getDel_flg(), "");

				//successメッセージ
				model.addAttribute("success", MessageConst.M001);
			}else {
				// 存在するとき

				log.info("@@MngTerminalCtrl.add " + MessageConst.W820 + "(テナントID = " + strTenantid
						+ ", 端末ID = " + terminalModel.getTerm_id()
						+ ", 端末区分 = " + terminalModel.getTerm_ctg()
						+ ", 端末名 = " + terminalModel.getTerm_name()
						+ ", 保存先ライブラリID = " + terminalModel.getLib_id()
						+ ", 撮影年フォルダ作成 = " + terminalModel.getYear_folder_flg()
						+ ", 撮影月フォルダ作成 = " + terminalModel.getMonth_folder_flg()
						+ ", 撮影日フォルダ作成 = " + terminalModel.getDay_folder_flg()
						+ ", 撮影日付フォルダ日本語 = " + terminalModel.getJapanese_flg()
						+ ", 動画撮影最大時間(秒) = " + terminalModel.getM_max_time()
						+ ", ライブ撮影サイズ(横) = " + terminalModel.getLive_width()
						+ ", ライブフレームレート(fps) = 最小:" + terminalModel.getLive_min_fps() + " 最大:" + terminalModel.getLive_max_fps()
						+ ", 有効期限 = " + mngterminalForm.getStart_date() + " ～ " + mngterminalForm.getEnd_date()
						+ ", 削除フラグ = " + terminalModel.getDel_flg() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()), CommonConst.MNG_TERMINALCTRL_ADD, CommonConst.FAILED_CO,
						  "端末ID = " + terminalModel.getTerm_id()
						+ "\n端末区分 = " + terminalModel.getTerm_ctg()
						+ "\n端末名 = " + terminalModel.getTerm_name()
						+ "\n保存先ライブラリID = " + terminalModel.getLib_id()
						+ "\n撮影年フォルダ作成 = " + terminalModel.getYear_folder_flg()
						+ "\n撮影月フォルダ作成 = " + terminalModel.getMonth_folder_flg()
						+ "\n撮影日フォルダ作成 = " + terminalModel.getDay_folder_flg()
						+ "\n撮影日付フォルダ日本語 = " + terminalModel.getJapanese_flg()
						+ "\n動画撮影最大時間(秒) = " + terminalModel.getM_max_time()
						+ "\nライブ撮影サイズ(横) = " + terminalModel.getLive_width()
						+ "\nライブフレームレート(fps) = 最小:" + terminalModel.getLive_min_fps() + " 最大:" + terminalModel.getLive_max_fps()
						+ "\n有効期限 = " + mngterminalForm.getStart_date() + " ～ " + mngterminalForm.getEnd_date()
						+ "\n削除フラグ =" + terminalModel.getDel_flg(), MessageConst.W820);

				//warningメッセージ
				model.addAttribute("add_danger", MessageConst.W820);
			}
		}else {
			// ライセンス超過

			log.info("@@MngTerminalCtrl.add " + MessageConst.W821 + "(テナントID = " + strTenantid
					+ ", 端末ID = " + terminalModel.getTerm_id()
					+ ", 端末区分 = " + terminalModel.getTerm_ctg()
					+ ", 端末名 = " + terminalModel.getTerm_name()
					+ ", 保存先ライブラリID = " + terminalModel.getLib_id()
					+ ", 撮影年フォルダ作成 = " + terminalModel.getYear_folder_flg()
					+ ", 撮影月フォルダ作成 = " + terminalModel.getMonth_folder_flg()
					+ ", 撮影日フォルダ作成 = " + terminalModel.getDay_folder_flg()
					+ ", 撮影日付フォルダ日本語 = " + terminalModel.getJapanese_flg()
					+ ", 動画撮影最大時間(秒) = " + terminalModel.getM_max_time()
					+ ", ライブ撮影サイズ(横) = " + terminalModel.getLive_width()
					+ ", ライブフレームレート(fps) = 最小:" + terminalModel.getLive_min_fps() + " 最大:" + terminalModel.getLive_max_fps()
					+ ", 有効期限 = " + mngterminalForm.getStart_date() + " ～ " + mngterminalForm.getEnd_date()
					+ ", 削除フラグ = " + terminalModel.getDel_flg() + ")");

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()), CommonConst.MNG_TERMINALCTRL_ADD, CommonConst.FAILED_CO,
					  "端末ID = " + terminalModel.getTerm_id()
					+ "\n端末区分 = " + terminalModel.getTerm_ctg()
					+ "\n端末名 = " + terminalModel.getTerm_name()
					+ "\n保存先ライブラリID = " + terminalModel.getLib_id()
					+ "\n撮影年フォルダ作成 = " + terminalModel.getYear_folder_flg()
					+ "\n撮影月フォルダ作成 = " + terminalModel.getMonth_folder_flg()
					+ "\n撮影日フォルダ作成 = " + terminalModel.getDay_folder_flg()
					+ "\n撮影日付フォルダ日本語 = " + terminalModel.getJapanese_flg()
					+ "\n動画撮影最大時間(秒) = " + terminalModel.getM_max_time()
					+ "\nライブ撮影サイズ(横) = " + terminalModel.getLive_width()
					+ "\nライブフレームレート(fps) = 最小:" + terminalModel.getLive_min_fps() + " 最大:" + terminalModel.getLive_max_fps()
					+ "\n有効期限 = " + mngterminalForm.getStart_date() + " ～ " + mngterminalForm.getEnd_date()
					+ "\n削除フラグ =" + terminalModel.getDel_flg(), MessageConst.W821);

			//warningメッセージ
			model.addAttribute("add_license_danger", MessageConst.W821);
		}

		// 動画撮影有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_movie_function())) {
			model.addAttribute("movieFunction", tenantModel.getStr_movie_function());
		}
		// ライブ撮影有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_live_function())) {
			model.addAttribute("liveFunction", tenantModel.getStr_live_function());
		}
		// 帳票有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
			model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
		}

		// システム情報を取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();

		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());
		// システムルート＋サムネイルルート＋テナントID
		StringBuilder sb = new StringBuilder(systeminfoModel.getSystem_rootpath());
		sb.append(CommonConst.YEN_MRK);
		sb.append(CommonConst.THUMBNAIL);
		sb.append(CommonConst.YEN_MRK);
		sb.append(strTenantid);
		// ライブラリツリーデータ
		model.addAttribute("libraryTreeData", getLibraryTreeData(sb.toString(), strTenantid));
		// 端末情報
		model.addAttribute("terminal", terminalDao.findTenantAllTerm(strTenantid));
		// 端末区分
		model.addAttribute("code",codeDao.getCode01Data("03"));
		// 動画撮影最大時間
		List<AnythingModel> anythingList = new ArrayList<AnythingModel>();
		AnythingModel anythingModel = new AnythingModel();
		for (int i = 30; i <= 180; i = i + 30) {
			anythingModel = new AnythingModel();
			anythingModel.setLabel(String.valueOf(i));
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("maxTime",anythingList);
		// ライブフレームレート
		anythingList = new ArrayList<AnythingModel>();
		for (int i = 2; i <= 30; i++) {
			anythingModel = new AnythingModel();
			anythingModel.setLabel(String.valueOf(i));
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("fps",anythingList);
		// システムURL
		model.addAttribute("inUrl", systeminfoModel.getSystem_in_url());
		model.addAttribute("outUrl", systeminfoModel.getSystem_out_url());
		// テナントID
		model.addAttribute("tenantId", strTenantid);

		return "mng_terminal";
	}

	/**
	 * 更新ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="update", method=POST)
	public String update(@ModelAttribute("mngTerminalForm") MngTerminalForm mngterminalForm, BindingResult result, Model model, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()), CommonConst.MNG_TERMINALCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()), CommonConst.MNG_TERMINALCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()), CommonConst.MNG_TERMINALCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		// テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);

		// フォームからモデルに転記
		TerminalModel terminalModel = setTerminalModel(mngterminalForm);
		terminalModel.setTenant_id(strTenantid);

		// フォームのライブラリIDが既にテーブルにあるか確認
		if (terminalDao.count(strTenantid,mngterminalForm.getTerm_id(),"chk") == 0 ) {
			// 存在しないとき

			// 端末ライセンス数確認
			if (tenantModel.getTerm_license() > terminalDao.licenseCount(strTenantid)) {
				// ライセンス範囲内
				// 追加
				terminalDao.insert(terminalModel);

				log.info("@@MngTerminalCtrl.update 端末情報テーブルを追加しました(テナントID = " + strTenantid
						+ ", 端末ID = " + terminalModel.getTerm_id()
						+ ", 端末区分 = " + terminalModel.getTerm_ctg()
						+ ", 端末名 = " + terminalModel.getTerm_name()
						+ ", 保存先ライブラリID = " + terminalModel.getLib_id()
						+ ", 撮影年フォルダ作成 = " + terminalModel.getYear_folder_flg()
						+ ", 撮影月フォルダ作成 = " + terminalModel.getMonth_folder_flg()
						+ ", 撮影日フォルダ作成 = " + terminalModel.getDay_folder_flg()
						+ ", 撮影日付フォルダ日本語 = " + terminalModel.getJapanese_flg()
						+ ", 動画撮影最大時間(秒) = " + terminalModel.getM_max_time()
						+ ", ライブ撮影サイズ(横) = " + terminalModel.getLive_width()
						+ ", ライブフレームレート(fps) = 最小:" + terminalModel.getLive_min_fps() + " 最大:" + terminalModel.getLive_max_fps()
						+ ", 有効期限 = " + mngterminalForm.getStart_date() + " ～ " + mngterminalForm.getEnd_date()
						+ ", 削除フラグ = " + terminalModel.getDel_flg() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()), CommonConst.MNG_TERMINALCTRL_ADD, CommonConst.SUCCESS_CO,
						  "端末ID = " + terminalModel.getTerm_id()
						+ "\n端末区分 = " + terminalModel.getTerm_ctg()
						+ "\n端末名 = " + terminalModel.getTerm_name()
						+ "\n保存先ライブラリID = " + terminalModel.getLib_id()
						+ "\n撮影年フォルダ作成 = " + terminalModel.getYear_folder_flg()
						+ "\n撮影月フォルダ作成 = " + terminalModel.getMonth_folder_flg()
						+ "\n撮影日フォルダ作成 = " + terminalModel.getDay_folder_flg()
						+ "\n撮影日付フォルダ日本語 = " + terminalModel.getJapanese_flg()
						+ "\n動画撮影最大時間(秒) = " + terminalModel.getM_max_time()
						+ "\nライブ撮影サイズ(横) = " + terminalModel.getLive_width()
						+ "\nライブフレームレート(fps) = 最小:" + terminalModel.getLive_min_fps() + " 最大:" + terminalModel.getLive_max_fps()
						+ "\n有効期限 = " + mngterminalForm.getStart_date() + " ～ " + mngterminalForm.getEnd_date()
						+ "\n削除フラグ =" + terminalModel.getDel_flg(), "");

				// メッセージをセット
				model.addAttribute("success", MessageConst.M001);
			}else {
				// ライセンス超過

				log.info("@@MngTerminalCtrl.update " + MessageConst.W821 + "(テナントID = " + strTenantid
						+ ", 端末ID = " + terminalModel.getTerm_id()
						+ ", 端末区分 = " + terminalModel.getTerm_ctg()
						+ ", 端末名 = " + terminalModel.getTerm_name()
						+ ", 保存先ライブラリID = " + terminalModel.getLib_id()
						+ ", 撮影年フォルダ作成 = " + terminalModel.getYear_folder_flg()
						+ ", 撮影月フォルダ作成 = " + terminalModel.getMonth_folder_flg()
						+ ", 撮影日フォルダ作成 = " + terminalModel.getDay_folder_flg()
						+ ", 撮影日付フォルダ日本語 = " + terminalModel.getJapanese_flg()
						+ ", 動画撮影最大時間(秒) = " + terminalModel.getM_max_time()
						+ ", ライブ撮影サイズ(横) = " + terminalModel.getLive_width()
						+ ", ライブフレームレート(fps) = 最小:" + terminalModel.getLive_min_fps() + " 最大:" + terminalModel.getLive_max_fps()
						+ ", 有効期限 = " + mngterminalForm.getStart_date() + " ～ " + mngterminalForm.getEnd_date()
						+ ", 削除フラグ = " + terminalModel.getDel_flg() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()), CommonConst.MNG_TERMINALCTRL_ADD, CommonConst.FAILED_CO,
						  "端末ID = " + terminalModel.getTerm_id()
						+ "\n端末区分 = " + terminalModel.getTerm_ctg()
						+ "\n端末名 = " + terminalModel.getTerm_name()
						+ "\n保存先ライブラリID = " + terminalModel.getLib_id()
						+ "\n撮影年フォルダ作成 = " + terminalModel.getYear_folder_flg()
						+ "\n撮影月フォルダ作成 = " + terminalModel.getMonth_folder_flg()
						+ "\n撮影日フォルダ作成 = " + terminalModel.getDay_folder_flg()
						+ "\n撮影日付フォルダ日本語 = " + terminalModel.getJapanese_flg()
						+ "\n動画撮影最大時間(秒) = " + terminalModel.getM_max_time()
						+ "\nライブ撮影サイズ(横) = " + terminalModel.getLive_width()
						+ "\nライブフレームレート(fps) = 最小:" + terminalModel.getLive_min_fps() + " 最大:" + terminalModel.getLive_max_fps()
						+ "\n有効期限 = " + mngterminalForm.getStart_date() + " ～ " + mngterminalForm.getEnd_date()
						+ "\n削除フラグ =" + terminalModel.getDel_flg(), MessageConst.W821);

				//warningメッセージ
				model.addAttribute("up_license_danger", MessageConst.W821);
			}
		}else {
			// 存在するとき

			// 端末IDから更新前端末情報を取得
			TerminalModel oldTerminalModel = terminalDao.findTerm(strTenantid, terminalModel.getTerm_id());

			// ライセンス端末数取得
			int licenseCount = terminalDao.licenseCount(strTenantid);

			// 端末ライセンス数確認(削除フラグ外しによってライセンス超過を防ぐ)
			if (tenantModel.getTerm_license() >= terminalDao.licenseCount(strTenantid) && !(tenantModel.getTerm_license() == licenseCount && oldTerminalModel.getDel_flg() == (byte) 1 && terminalModel.getDel_flg() == (byte) 0)) {
				// ライセンス範囲内
				// 更新
				terminalDao.update(terminalModel);

				log.info("@@MngTerminalCtrl.update 端末情報テーブルを更新しました(テナントID = " + strTenantid
						+ ", 端末ID = " + terminalModel.getTerm_id()
						+ ", 端末区分 = " + terminalModel.getTerm_ctg()
						+ ", 端末名 = " + terminalModel.getTerm_name()
						+ ", 保存先ライブラリID = " + terminalModel.getLib_id()
						+ ", 撮影年フォルダ作成 = " + terminalModel.getYear_folder_flg()
						+ ", 撮影月フォルダ作成 = " + terminalModel.getMonth_folder_flg()
						+ ", 撮影日フォルダ作成 = " + terminalModel.getDay_folder_flg()
						+ ", 撮影日付フォルダ日本語 = " + terminalModel.getJapanese_flg()
						+ ", 動画撮影最大時間(秒) = " + terminalModel.getM_max_time()
						+ ", ライブ撮影サイズ(横) = " + terminalModel.getLive_width()
						+ ", ライブフレームレート(fps) = 最小:" + terminalModel.getLive_min_fps() + " 最大:" + terminalModel.getLive_max_fps()
						+ ", 有効期限 = " + mngterminalForm.getStart_date() + " ～ " + mngterminalForm.getEnd_date()
						+ ", 削除フラグ = " + terminalModel.getDel_flg() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()), CommonConst.MNG_TERMINALCTRL_UPDATE, CommonConst.SUCCESS_CO,
						  "端末ID = " + terminalModel.getTerm_id()
						+ "\n端末区分 = " + terminalModel.getTerm_ctg()
						+ "\n端末名 = " + terminalModel.getTerm_name()
						+ "\n保存先ライブラリID = " + terminalModel.getLib_id()
						+ "\n撮影年フォルダ作成 = " + terminalModel.getYear_folder_flg()
						+ "\n撮影月フォルダ作成 = " + terminalModel.getMonth_folder_flg()
						+ "\n撮影日フォルダ作成 = " + terminalModel.getDay_folder_flg()
						+ "\n撮影日付フォルダ日本語 = " + terminalModel.getJapanese_flg()
						+ "\n動画撮影最大時間(秒) = " + terminalModel.getM_max_time()
						+ "\nライブ撮影サイズ(横) = " + terminalModel.getLive_width()
						+ "\nライブフレームレート(fps) = 最小:" + terminalModel.getLive_min_fps() + " 最大:" + terminalModel.getLive_max_fps()
						+ "\n有効期限 = " + mngterminalForm.getStart_date() + " ～ " + mngterminalForm.getEnd_date()
						+ "\n削除フラグ =" + terminalModel.getDel_flg(), "");

				// メッセージをセット
				model.addAttribute("success", MessageConst.M001);
			}else {
				// ライセンス超過

				log.info("@@MngTerminalCtrl.update " + MessageConst.W821 + "(テナントID = " + strTenantid
						+ ", 端末ID = " + terminalModel.getTerm_id()
						+ ", 端末区分 = " + terminalModel.getTerm_ctg()
						+ ", 端末名 = " + terminalModel.getTerm_name()
						+ ", 保存先ライブラリID = " + terminalModel.getLib_id()
						+ ", 撮影年フォルダ作成 = " + terminalModel.getYear_folder_flg()
						+ ", 撮影月フォルダ作成 = " + terminalModel.getMonth_folder_flg()
						+ ", 撮影日フォルダ作成 = " + terminalModel.getDay_folder_flg()
						+ ", 撮影日付フォルダ日本語 = " + terminalModel.getJapanese_flg()
						+ ", 動画撮影最大時間(秒) = " + terminalModel.getM_max_time()
						+ ", ライブ撮影サイズ(横) = " + terminalModel.getLive_width()
						+ ", ライブフレームレート(fps) = 最小:" + terminalModel.getLive_min_fps() + " 最大:" + terminalModel.getLive_max_fps()
						+ ", 有効期限 = " + mngterminalForm.getStart_date() + " ～ " + mngterminalForm.getEnd_date()
						+ ", 削除フラグ = " + terminalModel.getDel_flg() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngterminalForm.getAcc_userID()), CommonConst.MNG_TERMINALCTRL_UPDATE, CommonConst.FAILED_CO,
						  "端末ID = " + terminalModel.getTerm_id()
						+ "\n端末区分 = " + terminalModel.getTerm_ctg()
						+ "\n端末名 = " + terminalModel.getTerm_name()
						+ "\n保存先ライブラリID = " + terminalModel.getLib_id()
						+ "\n撮影年フォルダ作成 = " + terminalModel.getYear_folder_flg()
						+ "\n撮影月フォルダ作成 = " + terminalModel.getMonth_folder_flg()
						+ "\n撮影日フォルダ作成 = " + terminalModel.getDay_folder_flg()
						+ "\n撮影日付フォルダ日本語 = " + terminalModel.getJapanese_flg()
						+ "\n動画撮影最大時間(秒) = " + terminalModel.getM_max_time()
						+ "\nライブ撮影サイズ(横) = " + terminalModel.getLive_width()
						+ "\nライブフレームレート(fps) = 最小:" + terminalModel.getLive_min_fps() + " 最大:" + terminalModel.getLive_max_fps()
						+ "\n有効期限 = " + mngterminalForm.getStart_date() + " ～ " + mngterminalForm.getEnd_date()
						+ "\n削除フラグ =" + terminalModel.getDel_flg(), MessageConst.W821);

				//warningメッセージ
				model.addAttribute("up_license_danger", MessageConst.W821);
			}
		}

		// 動画撮影有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_movie_function())) {
			model.addAttribute("movieFunction", tenantModel.getStr_movie_function());
		}
		// ライブ撮影有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_live_function())) {
			model.addAttribute("liveFunction", tenantModel.getStr_live_function());
		}
		// 帳票有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
			model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
		}

		// システム情報を取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();

		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());
		// システムルート＋サムネイルルート＋テナントID
		StringBuilder sb = new StringBuilder(systeminfoModel.getSystem_rootpath());
		sb.append(CommonConst.YEN_MRK);
		sb.append(CommonConst.THUMBNAIL);
		sb.append(CommonConst.YEN_MRK);
		sb.append(strTenantid);
		// ライブラリツリーデータ
		model.addAttribute("libraryTreeData", getLibraryTreeData(sb.toString(), strTenantid));
		// 端末情報
		model.addAttribute("terminal", terminalDao.findTenantAllTerm(strTenantid));
		// 端末区分
		model.addAttribute("code",codeDao.getCode01Data("03"));
		// 動画撮影最大時間
		List<AnythingModel> anythingList = new ArrayList<AnythingModel>();
		AnythingModel anythingModel = new AnythingModel();
		for (int i = 30; i <= 180; i = i + 30) {
			anythingModel = new AnythingModel();
			anythingModel.setLabel(String.valueOf(i));
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("maxTime",anythingList);
		// ライブフレームレート
		anythingList = new ArrayList<AnythingModel>();
		for (int i = 2; i <= 30; i++) {
			anythingModel = new AnythingModel();
			anythingModel.setLabel(String.valueOf(i));
			anythingModel.setValue(String.valueOf(i));
			anythingList.add(anythingModel);
		}
		model.addAttribute("fps",anythingList);
		// システムURL
		model.addAttribute("inUrl", systeminfoModel.getSystem_in_url());
		model.addAttribute("outUrl", systeminfoModel.getSystem_out_url());
		// テナントID
		model.addAttribute("tenantId",strTenantid);

		return "mng_terminal";
	}

	/**
	 * フォームからモデルに入力情報を展開します。
	 * @param form 管理_端末_画面フォーム
	 * @return 端末モデル
	 * @throws Exception
	 */
	private TerminalModel setTerminalModel(MngTerminalForm form) throws Exception {
		TerminalModel model = new TerminalModel();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");

		// 端末ID
		model.setTerm_id(form.getTerm_id());
		// 端末区分
		model.setTerm_ctg(form.getTerm_ctg());
		// 端末名
		model.setTerm_name(form.getTerm_name());
		// ライブラリID
		model.setLib_id(form.getLib_id());
		// 撮影年フォルダ作成フラグ
		if (form.getYear_folder_flg()) {
			model.setYear_folder_flg((byte) 1);
		}else {
			model.setYear_folder_flg((byte) 0);
		}
		// 撮影月フォルダ作成フラグ
		if (form.getMonth_folder_flg()) {
			model.setMonth_folder_flg((byte) 1);
		}else {
			model.setMonth_folder_flg((byte) 0);
		}
		// 撮影日フォルダ作成フラグ
		if (form.getDay_folder_flg()) {
			model.setDay_folder_flg((byte) 1);
		}else {
			model.setDay_folder_flg((byte) 0);
		}
		// 日本語表記フラグ
		if (form.getJapanese_flg()) {
			model.setJapanese_flg((byte) 1);
		}else {
			model.setJapanese_flg((byte) 0);
		}
		// 動画撮影最大時間
		model.setM_max_time(form.getM_max_time());
		// ライブ撮影サイズ（横）
		model.setLive_width(form.getLive_width());
		// ライブ最小フレームレート
		model.setLive_min_fps(form.getLive_min_fps());
		// ライブ最大フレームレート
		model.setLive_max_fps(form.getLive_max_fps());
		// 有効開始日
		model.setStart_date(fmt.parse(form.getStart_date()));
		// 有効終了日
		model.setEnd_date(fmt.parse(form.getEnd_date()));
		// 削除フラグ
		if (form.getDel_flg()) {
			model.setDel_flg((byte) 1);
		}else {
			model.setDel_flg((byte) 0);
		}

		// 更新ユーザーID
		model.setUpdate_user_id(tripleDesConverter.decrypt64(form.getAcc_userID()));

		return model;
	}

	/**
	 * ライブラリツリーデータ取得 <br>
	 * @param path ルートパス
	 * @param strTenantid テナントID
	 * @return ライブラリツリーデータ
	 * @throws Exception
	 */
	private String getLibraryTreeData(String path, String strTenantid) throws Exception {

		String json = "error";
		Path userRootDir = Paths.get(path);

		// ユーザのルートディレクトリ存在確認
		if (Files.exists(userRootDir)) {
			// 表示順ライブラリリスト取得
			List<LibraryModel> libNoList = librarydao.libDisplayNo(strTenantid);
			// ライブラリツリーデータ作成
			Node root = getNode(userRootDir, path, strTenantid, libNoList);
			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(((Directory) root).getNodes());
			json = json.replace(",\"nodes\":[]", ""); // 空の子要素は削除する
			json = json.substring(1, json.length() - 1);
		}

		return json;
	}

	/**
	 * ライブラリリスト取得<br>
	 * @param obj ディレクトリ
	 * @param path ルートパス
	 * @param strTenantid テナントID
	 * @param libNoList 表示順ライブラリリスト
	 * @return ライブラリリスト
	 * @throws Exception
	*/
	public Node getNode(Path obj, String path, String strTenantid, List<LibraryModel> libNoList) throws Exception {

		Node node = null;

		if (Files.isDirectory(obj)) {

			Directory dir = new Directory(obj.getFileName().toString());

			// テナントを最上位とする
			if (path.equals(obj.toAbsolutePath().toString())) {
				dir = new Directory("無し（最上位）");
				dir.setDirId("");
			}else {
				dir.setDirId(librarydao.findPath(strTenantid, obj.toAbsolutePath().toString().replace(path + CommonConst.YEN_MRK, "")).getLib_id());
				// 表示順を設定
				if (libNoList.size() > 0) {
					for(int i=0; i < libNoList.size(); i++) {
						if (libNoList.get(i).getLib_path().equals(obj.toAbsolutePath().toString().replace(path + CommonConst.YEN_MRK, ""))) {
							dir.setDisNo(libNoList.get(i).getDisplay_no());
							break;
						}
					}
				}
			}

			// 子要素を作成
			List<Node> nodes = new ArrayList<Node>();

			// ディレクトリ内の一覧を取得
			FileUtil fileUtil = new FileUtil();
			List<Path> childList = fileUtil.getFileList(obj);

			for (Path child : childList) {
				if (Files.isDirectory(child) && librarydao.libCount(strTenantid, child.toAbsolutePath().toString().replace(path + CommonConst.YEN_MRK, "")) == 1) {
					// 登録ライブラリのとき再帰呼び出し
					nodes.add(getNode(child, path, strTenantid, libNoList));
				}
			}
			dir.setNodes(nodes);

			node = dir;
		}

		return node;
	}

}