/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込み変更。TripleDES変換クラス宣言(インスタンス化)変更。
 *    3    2019/10/23      1.0.2           女屋            不要メンバ変数削除と修正
 *    4    2020/02/14      1.0.3           女屋            地図有効化機能追加。ライブ有効化設定追加。
 *    5    2020/02/20      1.0.4           女屋            帳票有効化機能追加
 *    6    2020/03/03      1.0.5           斎川            ffmpegのLinux対応
 *    7    2021/05/29      1.0.6           斎川            WebRTCサーバーの判定追加
 */
package jp.reneil.reiss.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.awt.Dimension;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bbreak.excella.reports.exporter.ExcelOutputStreamExporter;
import org.bbreak.excella.reports.model.ReportBook;
import org.bbreak.excella.reports.model.ReportSheet;
import org.bbreak.excella.reports.processor.ReportProcessor;
import org.bbreak.excella.reports.tag.ImageParamParser;
import org.bbreak.excella.reports.tag.SingleParamParser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.AdditionalInfoDao;
import jp.reneil.reiss.dao.AdditionalItemDao;
import jp.reneil.reiss.dao.ContentsDao;
import jp.reneil.reiss.dao.LibraryDao;
import jp.reneil.reiss.dao.ReportParamDao;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.TenantDao;
import jp.reneil.reiss.dao.TerminalDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.exception.ExException;
import jp.reneil.reiss.form.MainForm;
import jp.reneil.reiss.log.LogWriting;
import jp.reneil.reiss.model.AdditionalInfoModel;
import jp.reneil.reiss.model.AdditionalItemModel;
import jp.reneil.reiss.model.ContentsModel;
import jp.reneil.reiss.model.Directory;
import jp.reneil.reiss.model.ImageData;
import jp.reneil.reiss.model.LibraryModel;
import jp.reneil.reiss.model.Node;
import jp.reneil.reiss.model.ReportParamModel;
import jp.reneil.reiss.model.SystemInfoModel;
import jp.reneil.reiss.model.TenantModel;
import jp.reneil.reiss.model.UserInfoModel;
import jp.reneil.reiss.util.ContentsList;
import jp.reneil.reiss.util.DirectoryList;
import jp.reneil.reiss.util.Encoder;
import jp.reneil.reiss.util.FFmpegTrans;
import jp.reneil.reiss.util.FileUtil;
import jp.reneil.reiss.util.PlatformUtil;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * メイン_画面コントローラクラスです。<br>
 *
 * @version 1.0.5
 */
@Controller
@RequestMapping(value="main")
public class MainCtrl {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(MainCtrl.class);

	/** ログ書き込みクラス */
	@Autowired
	private LogWriting logWriting;

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** コンテンツテーブルアクセス制御クラス */
	@Autowired
	private ContentsDao contentsDao;

	/** ユーザー情報テーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/** テナント情報テーブルアクセス制御クラス */
	@Autowired
	private TenantDao tenantDao;

	/** ライブラリ情報テーブルアクセス制御クラス */
	@Autowired
	private LibraryDao libraryDao;

	/** 端末情報テーブルアクセス制御クラス */
	@Autowired
	private TerminalDao terminalDao;

	/** 付帯情報項目名テーブルアクセス制御クラス */
	@Autowired
	private AdditionalItemDao additionalItemDao;

	/** 付帯情報テーブルアクセス制御クラス */
	@Autowired
	private AdditionalInfoDao additionalInfoDao;

	/** 帳票パラメータテーブルアクセス制御クラス */
	@Autowired
	private ReportParamDao reportParamDao;

	/** ファイルユーティリティ */
	private FileUtil fileUtil = new FileUtil();

	/** FFmpegTransパラメータクラス */
	private FFmpegTrans ffmpegTrans;

	/** ファイルロック中テナント */
	public static ArrayList<String> fileLockTenant = new ArrayList<String>();

	/**
	 * 画面表示<br>
	 * @param mainForm メイン画面
	 * @param model Modelオブジェクト
	 * @param request リクエスト情報
	 * @return メイン画面
	 * @throws Exception
	 */
	@RequestMapping(method=GET)
	public String init(@ModelAttribute("mainForm") MainForm mainForm, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			// アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MAINCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);

		}else if (session.getAttribute(SessionConst.USER_ID) == null) {
			// アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MAINCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);

		}else {
			// システム情報を取得
			SystemInfoModel systeminfoModel = systemInfoDao.findAll();
			// システムルートパスを取得
			String strSystemRoot = systeminfoModel.getSystem_rootpath();
			// セッション情報からログインユーザIDを取得
			String user_id = (String) session.getAttribute(SessionConst.USER_ID);
			log.info("@@MainCtrl.init セッション情報取得[USER_ID=" + user_id + "]");
			// セッション情報から権限コードを取得
			String authcode = (String) session.getAttribute(SessionConst.AUTH_CODE);

			// システム名をセット
			model.addAttribute("systemName", systeminfoModel.getSystem_name());
			// ユーザIDをセット
			model.addAttribute("acc_userID", tripleDesConverter.encrypt64(user_id));
			// ユーザ種別コードをセット
			model.addAttribute("auth",authcode);

			// セッション情報からログインユーザーのテナントIDを取得
			String strTenantid = (String)session.getAttribute(SessionConst.TENANT_ID);
			log.info("@@MainCtrl.init セッション情報取得[TENANT_ID=" + strTenantid + "]");

			// ブラウザ判定
			String bwUserAgent = request.getHeader("user-agent");
			if (notLiveBrowser(bwUserAgent)) {
				// ライブ中継不可ブラウザ
				model.addAttribute("notLiveBrowser", true);
			}

			// ログインユーザーIDからユーザー情報を取得
			UserInfoModel userinfoModel = userInfoDao.getUser(user_id);

			// テナント情報を取得
			TenantModel tenantModel = tenantDao.findTenant(strTenantid);

			// ユーザ名表示
			model.addAttribute("userName", userinfoModel.getUser_name());

			// 前回ログイン日時表示
			model.addAttribute("lastLoginTime", userinfoModel.getStr_last_login_time());

			// 最終ログイン日時の更新（パスワードエラーカウントリセット）
			userInfoDao.updateLastLoginTime(user_id);

			// 端末リスト取得(検索用)
			model.addAttribute("termList", terminalDao.findTenantAllTerm(strTenantid));

			// 取得したユーザ情報から権限を設定
			// 管理権限
			if (!StringUtils.isEmpty(userinfoModel.getManage_auth())) {
				model.addAttribute("manageAuth", userinfoModel.getManage_auth());
			}
			// 付帯情報編集権限
			if (!StringUtils.isEmpty(userinfoModel.getEdit_auth())) {
				model.addAttribute("editAuth", userinfoModel.getEdit_auth());
			}
			// ダウンロード権限
			if (!StringUtils.isEmpty(userinfoModel.getDownload_auth())) {
				model.addAttribute("downloadAuth", userinfoModel.getDownload_auth());
			}
			// 移動権限
			if (!StringUtils.isEmpty(userinfoModel.getMove_auth())) {
				model.addAttribute("moveAuth", userinfoModel.getMove_auth());
			}
			// 削除権限
			if (!StringUtils.isEmpty(userinfoModel.getDelete_auth())) {
				model.addAttribute("deleteAuth", userinfoModel.getDelete_auth());
			}

			// ライブ機能有効化判定
			if (!StringUtils.isEmpty(tenantModel.getStr_live_function())) {
				model.addAttribute("liveEnable", tenantModel.getStr_live_function());
				model.addAttribute("mainSize", "col-md-10");
				// テナントのSkyWayAPIキー設定、Janusの場合はサーバーのアドレス
				model.addAttribute("liveApp", tenantModel.getLive_app());
				// ライブの接続先をセット（SkyWayAPIキー, ルーム名）
				if (!StringUtils.isEmpty(tenantModel.getLive_app())) {
					// No.7 WebRTCサーバーの判定追加 2021-05-29
					if (tenantModel.getLive_app().contains("https://")) {
						// Janus
						model.addAttribute("liveServer", "Janus");
					} else {
						// SkyWay
						model.addAttribute("liveServer", "SkyWay");
					}
				}
				// ルーム一覧作成
				StringBuilder liveRoom = new StringBuilder();
				if (!StringUtils.isEmpty(tenantModel.getLive_room())) {
					// テナントのルーム設定
					liveRoom.append(tenantModel.getLive_room());
					liveRoom.append(CommonConst.COMMA);
				}
				if (!CommonConst.TENANT_MANAGER_CD.equals(authcode)) {
					// 一般ユーザー
					// ユーザー閲覧ライブラリリスト取得
					List<LibraryModel> userLibrary = libraryDao.findUser(strTenantid, user_id);
					if (userLibrary.size() > 0) {
						List<LibraryModel> userLibraryAll = new ArrayList<LibraryModel>();
						for(int i=0; i < userLibrary.size(); i++) {
							// ユーザー閲覧ライブラリ + 子ライブラリのリストを取得
							userLibraryAll.addAll(libraryDao.likePath(strTenantid, userLibrary.get(i).getLib_path()));
						}
						if (userLibraryAll.size() > 0) {
							for(int x=0; x < userLibraryAll.size(); x++) {
								if (!StringUtils.isEmpty(userLibraryAll.get(x).getLive_room())) {
									// ライブラリのルーム設定
									liveRoom.append(userLibraryAll.get(x).getLive_room());
									liveRoom.append(CommonConst.COMMA);
								}
							}
						}
					}
				}else {
					// テナント管理者
					// テナント登録ライブラリリスト取得
					List<LibraryModel> tenantLibraryAll = libraryDao.tenantLib(strTenantid);
					if (tenantLibraryAll.size() > 0) {
						for(int i=0; i < tenantLibraryAll.size(); i++) {
							if (!StringUtils.isEmpty(tenantLibraryAll.get(i).getLive_room())) {
								// ライブラリのルーム設定
								liveRoom.append(tenantLibraryAll.get(i).getLive_room());
								liveRoom.append(CommonConst.COMMA);
							}
						}
					}
				}
				if (!StringUtils.isEmpty(liveRoom)) {
					// 末尾からカンマ分を削除
					liveRoom.setLength(liveRoom.length() - 1);
					// ルーム設定
					model.addAttribute("liveRoom", liveRoom);
				}
			}else {
				model.addAttribute("mainSize", "col-md-12");
			}

			// 地図有効化制御
			if (!StringUtils.isEmpty(tenantModel.getStr_map_enable())) {
				model.addAttribute("mapEnable", tenantModel.getStr_map_enable());
				if (!StringUtils.isEmpty(tenantModel.getMap_server())) {
					// 地図サーバーのURLをセット
					model.addAttribute("mapServer", tenantModel.getMap_server());
				}
			}

			// 帳票有効化制御
			if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
				if (!CommonConst.TENANT_MANAGER_CD.equals(authcode)) {
					// ユーザー
					// 帳票出力権限
					if (!StringUtils.isEmpty(userinfoModel.getOutput_auth())) {
						model.addAttribute("reportEnable", userinfoModel.getOutput_auth());
						// 帳票パラメータデータ取得
						model.addAttribute("report", reportParamDao.findUser(strTenantid, user_id));
					}
				}else {
					// テナント管理者
					model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
					// 帳票パラメータデータ取得
					model.addAttribute("report", reportParamDao.findTenantManager(strTenantid));
				}
			}

			// ユーザーのルートパス
			// システムルート＋サムネイルルート＋テナントID（＋ライブラリパス）
			StringBuilder sb = new StringBuilder(strSystemRoot);
			sb.append(CommonConst.YEN_MRK);
			sb.append(CommonConst.THUMBNAIL);
			sb.append(CommonConst.YEN_MRK);
			sb.append(strTenantid);
			// セッションにユーザのLibraryルートパスをセット
			session.setAttribute(SessionConst.USER_ROOT_PATH, sb.toString().replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY) + CommonConst.YEN_MRK);
			if (!CommonConst.TENANT_MANAGER_CD.equals(authcode)) {
				// 一般ユーザー
				// ユーザー閲覧ライブラリリスト取得
				List<LibraryModel> userLibrary = libraryDao.findUser(strTenantid, user_id);
				if (userLibrary.size() > 0) {
					String strTenantPath = sb.toString();
					sb.append(CommonConst.YEN_MRK);
					StringBuilder directoryTreeData = new StringBuilder();
					String tmpDirectoryData;
					for(int i=0; i < userLibrary.size(); i++) {
						tmpDirectoryData = getDirectoryTreeData(strTenantid, strTenantPath, sb.toString() + userLibrary.get(i).getLib_path());
						if (!tmpDirectoryData.equals("error")) {
							directoryTreeData.append(tmpDirectoryData);
							userLibrary.get(i).getLib_path();
							if (i != userLibrary.size() - 1) {
								directoryTreeData.append(CommonConst.COMMA);
							}
						}else {
							directoryTreeData.setLength(0);
							directoryTreeData.append("error");
							break;
						}
					}
					model.addAttribute("directoryTreeData", directoryTreeData.toString());
				}else {
					// 閲覧ライブラリ無し
					model.addAttribute("directoryTreeData", "error");
				}
			}else {
				// テナント管理者
				// ディレクトリツリーデータをセット
				model.addAttribute("directoryTreeData", getDirectoryTreeData(strTenantid, sb.toString(), sb.toString()));
			}

			// 付帯情報検索表示判定
			if (additionalItemDao.searchCount(strTenantid) == 0) {
				model.addAttribute("searchInfoFlg", 0);
			}else {
				model.addAttribute("searchInfoFlg", 1);
			}

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MAINCTRL_INIT, CommonConst.SUCCESS_CO, "", "");
		}

		return "main";
	}

	/**
	 * ライブ中継不可ブラウザか判定 <br>
	 * @param bwUserAgent ユーザエージェント
	 * @return 結果
	 */
	public static boolean notLiveBrowser(String bwUserAgent) {
		if (bwUserAgent.contains("MSIE") || bwUserAgent.contains("Trident") || bwUserAgent.contains("Edge")) {
			// IE or 旧Edge
			return true;
		}
		return false;
	}

	/**
	 * ディレクトリツリーデータ取得 <br>
	 * @param strTenantid テナントID
	 * @param strTenantPath テナントパス
	 * @param path ルートパス
	 * @return ディレクトリツリーデータ
	 * @throws Exception
	 */
	private String getDirectoryTreeData(String strTenantid, String strTenantPath, String path) throws Exception {

		String json = "error";
		Path userRootDir = Paths.get(path);

		// ユーザのルートディレクトリ存在確認
		if (Files.exists(userRootDir)) {
			// 表示順ライブラリリスト取得
			List<LibraryModel> libNoList = libraryDao.libDisplayNo(strTenantid);
			// ディレクトリツリーデータ作成
			DirectoryList dirList = new DirectoryList(libNoList, strTenantPath, path);
			Node root = dirList.getRoot();
			List<Node> nodes = ((Directory) root).getNodes();

			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(nodes);
			json = json.replace(",\"nodes\":[]", ""); // 空の子要素は削除する
			json = json.substring(1, json.length() - 1);
		}

		return json;
	}

	/**
	 * ユーザ閲覧ライブラリリスト取得 <br>
	 * @param strTenantid テナントID
	 * @param path[] パス
	 * @param view 表示
	 * @param strUserRootPath ユーザのルートパス
	 * @param DisplayPathFlg ルートパス表示フラグ
	 * @return ライブラリリスト
	 * @throws Exception
	 * @throws JsonProcessingException
	 */
	private List<ImageData> getUserLibraryList(String strTenantid, String[] path, String view, String strUserRootPath, boolean DisplayPathFlg) throws Exception {

		List<ImageData> imageDataList = new ArrayList<ImageData>();

		// システム情報を取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();
		// システム情報から画像を取得
		String strDirectoryImageSrc = systeminfoModel.getDirectory_image();
		// ディレクトリリスト格納用
		ImageData directoryImage;

		// システムルート＋サムネイルルート＋テナントID
		StringBuilder sb = new StringBuilder(systeminfoModel.getSystem_rootpath());
		sb.append(CommonConst.YEN_MRK);
		sb.append(CommonConst.THUMBNAIL);
		sb.append(CommonConst.YEN_MRK);
		sb.append(strTenantid);

		// 表示順ライブラリリスト取得
		List<LibraryModel> libNoList = libraryDao.libDisplayNo(strTenantid);

		for(int i=0; i < path.length; i++) {

			Path dir = Paths.get(path[i]);

			if (Files.isDirectory(dir)) {

				// ディレクトリのとき
				if (!view.equals("map")) {
					// 地図表示以外の時
					directoryImage = new ImageData();
					directoryImage.setFileName(dir.getFileName().toString());
					directoryImage.setDirectoryPath(tripleDesConverter.encrypt64(dir.getParent().toString().replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY)));
					directoryImage.setDirectoryID(tripleDesConverter.encrypt64(dir.toAbsolutePath().toString()));
					directoryImage.setSrc(strDirectoryImageSrc);
					directoryImage.setDateTime(dir.getFileName().toString());				// ソート用(撮影日時にファイル名を設定)
					directoryImage.setTermName(dir.getFileName().toString());				// ソート用(撮影端末にファイル名を設定)

					// 表示順を設定
					if (libNoList.size() > 0) {
						for(int x=0; x < libNoList.size(); x++) {
							if (libNoList.get(x).getLib_path().equals(dir.toAbsolutePath().toString().replace(sb.toString() + CommonConst.YEN_MRK, ""))) {
								directoryImage.setDisNo(libNoList.get(x).getDisplay_no());
								break;
							}
						}
					}

					// 詳細表示の場合
					if (view.equals("details")) {
						// DataTable生成
						directoryImage.setAddItemHtml(detailsTheadHtml(strTenantid));
						directoryImage.setAddInfoHtml(detailsTbodyHtml(strTenantid, ""));
						if (DisplayPathFlg) {
							// ファイルパス表示
							directoryImage.setFilePath(dir.toAbsolutePath().toString().replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY));
						}
					}

					imageDataList.add(directoryImage);
				}

			}
		}

		return imageDataList;
	}

	/**
	 * ファイル画像リスト取得 <br>
	 * @param strTenantid テナントID
	 * @param path パス
	 * @param view 表示
	 * @param strUserRootPath ユーザのルートパス
	 * @param DisplayPathFlg ルートパス表示フラグ
	 * @return ファイル画像リスト
	 * @throws Exception
	 * @throws JsonProcessingException
	 */
	private List<ImageData> getImageDataList(String strTenantid, String path, String view, String strUserRootPath, boolean DisplayPathFlg) throws Exception {

		// 現在パスを格納
		String pwdPath = path.replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY);
		// システム情報を取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();
		// システム情報から画像を取得
		String strDirectoryImageSrc = systeminfoModel.getDirectory_image();
		// FFmpegシステムルートパスを取得
		String ffmpegRoot = systeminfoModel.getSystem_rootpath() + CommonConst.YEN_MRK + CommonConst.FFMPEG;
		// Linux対応 on 2020.03.03
		if (PlatformUtil.isLinux()) {
			ffmpegRoot = CommonConst.FFMPEGCOM;
		}

		// システムルート＋サムネイルルート＋テナントID
		StringBuilder sb = new StringBuilder(systeminfoModel.getSystem_rootpath());
		sb.append(CommonConst.YEN_MRK);
		sb.append(CommonConst.THUMBNAIL);
		sb.append(CommonConst.YEN_MRK);
		sb.append(strTenantid);

		// 表示順ライブラリリスト取得
		List<LibraryModel> libNoList = libraryDao.libDisplayNo(strTenantid);

		List<ImageData> imageDataList = new ArrayList<ImageData>();

		Path dir = Paths.get(path);

		if (!Files.exists(dir)) {
			return imageDataList;
		}

		List<Path> fileList = fileUtil.getFileList(dir);

		ImageData directoryImage = new ImageData();

		String ThuFileName = "";
		String LibfileName = "";
		String filePath = "";
		String servletPath = "";
		String modalHtml = "";
		String fileDateTime = "";

		ImageData image = new ImageData();

		String term_name = "";
		String photo_date = "";
		String locate_lat = "0.0";
		String locate_lon = "0.0";
		String locate = "";

		ContentsModel contentsModel = null;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		// ファイルカウント
		int fileCount = 0;

		for (Path file : fileList) {
			String fileName = file.getFileName().toString();				// ファイル名
			String absolutePath = file.toAbsolutePath().toString();			// ファイルパス
			if (Files.isDirectory(file)) {
				// ディレクトリのとき
				if (!view.equals("map")) {
					// 地図表示以外の時
					directoryImage = new ImageData();
					directoryImage.setFileName(fileName);
					directoryImage.setDirectoryPath(tripleDesConverter.encrypt64(pwdPath));
					directoryImage.setDirectoryID(tripleDesConverter.encrypt64(absolutePath));
					directoryImage.setSrc(strDirectoryImageSrc);
					directoryImage.setDateTime(fileName);				// ソート用(撮影日時にファイル名を設定)
					directoryImage.setTermName(fileName);				// ソート用(撮影端末にファイル名を設定)

					// 表示順を設定
					if (libNoList.size() > 0) {
						for(int i=0; i < libNoList.size(); i++) {
							if (libNoList.get(i).getLib_path().equals(file.toAbsolutePath().toString().replace(sb.toString() + CommonConst.YEN_MRK, ""))) {
								directoryImage.setDisNo(libNoList.get(i).getDisplay_no());
								break;
							}
						}
					}

					// 詳細表示の場合
					if (view.equals("details")) {
						// DataTable生成
						directoryImage.setAddItemHtml(detailsTheadHtml(strTenantid));
						directoryImage.setAddInfoHtml(detailsTbodyHtml(strTenantid, ""));
						if (DisplayPathFlg) {
							// ファイルパス表示
							directoryImage.setFilePath(absolutePath.replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY));
						}
					}

					imageDataList.add(directoryImage);
				}
			}else if (Files.exists(file)) {
				// ファイルのとき
				if (fileUtil.chkTHFileExt(fileName, CommonConst.THJPG)) {
					// サムネイル形式のファイルのみ処理
					boolean contentsData = false;									// コンテンツデータ判断
					ThuFileName = fileName;
					LibfileName = "";
					filePath = absolutePath.replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY);
					modalHtml = "";

	    			fileDateTime = ThuFileName.split(CommonConst.UNDERSCORE)[ThuFileName.split(CommonConst.UNDERSCORE).length - 3] + ThuFileName.split(CommonConst.UNDERSCORE)[ThuFileName.split(CommonConst.UNDERSCORE).length - 2];

	    			// 日時が17桁(yyyyMMddHHmmssSSS)ない場合は0で埋める
	    			fileDateTime = zeroPadding(fileDateTime, 17);

			        // ファイル名から静止画・動画の判断
					// 静止画の場合
			        if (ThuFileName.startsWith(CommonConst.STILLIMAGE) || ThuFileName.startsWith(CommonConst.CAPTURE)) {
						filePath = filePath.replace(CommonConst.THJPG, CommonConst.JPG);
				        LibfileName = ThuFileName.replace(CommonConst.THJPG, CommonConst.JPG);
				        servletPath = filePath.replace(strUserRootPath, "");
				    // 動画の場合
			        }else {
			        	if (ThuFileName.startsWith(CommonConst.MOVIE)) {
							filePath = filePath.replace(CommonConst.THJPG, CommonConst.MP4);
							LibfileName = ThuFileName.replace(CommonConst.THJPG, CommonConst.MP4);
							servletPath = filePath.replace(strUserRootPath, "");
			        	}else {
							filePath = filePath.replace(CommonConst.THJPG, CommonConst.WEBM);
							LibfileName = ThuFileName.replace(CommonConst.THJPG, CommonConst.WEBM);
							servletPath = filePath.replace(strUserRootPath, "");
			        	}
			        }

					// 撮影情報設定
					term_name = "";
					photo_date = "";
					// ファイル名でCONTENTSテーブルを検索
					if (contentsDao.findContents_filename(LibfileName) == 1) {
						contentsModel = contentsDao.getContentsData_tenantid_filename(strTenantid, LibfileName);
						if (!StringUtils.isEmpty(contentsModel.getTerm_name())) {
							term_name = contentsModel.getTerm_name();
						}
						if (contentsModel.getPhoto_date() != null) {
							photo_date = sdf.format(contentsModel.getPhoto_date());
						}
						contentsData = true;
					}else {
						contentsData = false;
					}

					// 静止画の場合
			        if (ThuFileName.startsWith(CommonConst.STILLIMAGE) || ThuFileName.startsWith(CommonConst.CAPTURE)) {
				        modalHtml = imageModalHtml(fileCount, filePath, LibfileName, photo_date, servletPath);
				    // 動画の場合
			        }else {
				        modalHtml = videoModalHtml(fileCount, filePath, LibfileName, photo_date, ffmpegRoot, servletPath);
			        }

					// ファイル用データをセット
					image = new ImageData();

					image.setModalNo(fileCount);
					image.setFileName(LibfileName);
					image.setDirectoryPath(tripleDesConverter.encrypt64(pwdPath));
    				image.setDateTime(fileDateTime);
					image.setModalHtml(modalHtml);
					image.setSrc(cnvBase64(absolutePath));
					image.setTermName(term_name);
					image.setPhotoDate(photo_date);

					// 詳細表示の場合
					if (view.equals("details")) {
						// DataTable生成
						image.setAddItemHtml(detailsTheadHtml(strTenantid));
						image.setAddInfoHtml(detailsTbodyHtml(strTenantid, LibfileName));
						if (DisplayPathFlg) {
							// ファイルパス表示
							image.setFilePath(filePath);
						}
					}

					// 地図表示の場合
					if (view.equals("map")) {
						// 位置情報設定
						locate_lat = "0.0";
						locate_lon = "0.0";
						locate = "";
						if (contentsData) {
							if (!Double.isNaN(contentsModel.getLocate_lat())) {
								locate_lat = BigDecimal.valueOf(contentsModel.getLocate_lat()).toPlainString();
							}
							if (!Double.isNaN(contentsModel.getLocate_lon())) {
								locate_lon = BigDecimal.valueOf(contentsModel.getLocate_lon()).toPlainString();
							}
							if (contentsModel.getLocate() != null) {
								locate = contentsModel.getLocate();
							}
						}
						image.setLocateLat(locate_lat);
						image.setLocateLon(locate_lon);
						image.setLocate(locate);
					}

					imageDataList.add(image);
					fileCount++;
				}
			}
		}

		return imageDataList;
	}

	/**
	 * 検索ファイル画像リスト取得 （ファイルのみ）<br>
	 * @param strTenantid テナントID
	 * @param path パス
	 * @param view 表示
	 * @param searchTerm 検索端末ID
	 * @param startDateTime 検索範囲 開始時間
	 * @param endDateTime 検索範囲 終了時間
	 * @param searchAddInfo 付帯情報 検索キーワード
	 * @param strUserRootPath ユーザのルートパス
	 * @param ffmpegRoot FFmpegルートパス
	 * @param DisplayPathFlg ルートパス表示フラグ
	 * @param searchFileCount 検索ファイルカウント
	 * @return ファイル画像リスト
	 * @throws Exception
	 * @throws JsonProcessingException
	 */
	private List<ImageData> searchImageDataList(String strTenantid, String path, String view, String searchTerm, String startDateTime, String endDateTime, String searchAddInfo, String strUserRootPath, String ffmpegRoot, boolean DisplayPathFlg, int searchFileCount) throws Exception {

		// リスト初期化
		List<String> contentsFileList = new ArrayList<String>();
		List<ImageData> imageDataList = new ArrayList<ImageData>();

		// コンテンツリストデータ作成
		ContentsList contentsList = new ContentsList();
		// ファイルリスト取得
		contentsList.addContentsFileList(path);
		contentsFileList = contentsList.getFileList();

		Path dir = Paths.get(path);

		if (!Files.exists(dir)) {
			return imageDataList;
		}

		String ThuFileName = "";
		String LibfileName = "";
		String filePath = "";
		String servletPath = "";
		String directoryPath = "";
		String modalHtml = "";
		String fileDateTime = "";

		ImageData image = new ImageData();

		String term_name = "";
		String photo_date = "";
		String locate_lat = "0.0";
		String locate_lon = "0.0";
		String locate = "";

		ContentsModel contentsModel = null;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		// ファイルカウント
		int fileCount = searchFileCount;

		// ファイルリストをimageDataListにセット
        for(String strfile : contentsFileList) {

    		// リスト取得時のファイル存在確認
    		if (strfile.contains("は削除されています:")) {
    			// ファイルが存在しない
    			continue;
    		}

        	boolean contentsData = false;
    		boolean searchTermId = false;
    		boolean searchAddText = false;

    		Path file = Paths.get(strfile);
			ThuFileName = file.getFileName().toString();

			if (ThuFileName.startsWith(CommonConst.STILLIMAGE) || ThuFileName.startsWith(CommonConst.CAPTURE)){
				LibfileName = ThuFileName.replace(CommonConst.THJPG, CommonConst.JPG);
			}else {
	        	if (ThuFileName.startsWith(CommonConst.MOVIE)) {
	        		LibfileName = ThuFileName.replace(CommonConst.THJPG, CommonConst.MP4);
	        	}else {
	        		LibfileName = ThuFileName.replace(CommonConst.THJPG, CommonConst.WEBM);
	        	}
			}

			// ファイル名でCONTENTSテーブルを検索
			if (contentsDao.findContents_filename(LibfileName) == 1) {
				contentsModel = contentsDao.getContentsData_tenantid_filename(strTenantid, LibfileName);
				contentsData = true;
			}else {
				contentsData = false;
			}

			if (!StringUtils.isEmpty(searchTerm)) {
				if (contentsData) {
					if (!StringUtils.isEmpty(contentsModel.getTerm_id())) {
						if (searchTerm.equals(contentsModel.getTerm_id())) {
							searchTermId = true;
						}else {
							searchTermId = false;
						}
					}else {
						searchTermId = false;
					}
				}else {
					searchTermId = false;
				}
			}else {
				searchTermId = true;
			}

			if (!StringUtils.isEmpty(searchAddInfo)) {
				if (contentsData && searchTermId) {
					if (contentsModel.getReg_no() != null) {
						if (additionalInfoDao.search(contentsModel.getReg_no(), searchAddInfo) > 0) {
							searchAddText = true;
						}else {
							searchAddText = false;
						}
					}else {
						searchAddText = false;
					}
				}else {
					searchAddText = false;
				}
			}else {
				searchAddText = true;
			}

			// サムネイル形式のファイルのみ処理
			if (fileUtil.chkTHFileExt(ThuFileName, CommonConst.THJPG) && searchTermId && searchAddText) {

				// 検索期間範囲判定
		       	if (between(ThuFileName, startDateTime, endDateTime)) {

	    			filePath = file.toAbsolutePath().toString().replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY);
	    			directoryPath = filePath.replace(CommonConst.YEN_MRK + ThuFileName, "");
	    			modalHtml = "";
	    			fileDateTime = ThuFileName.split(CommonConst.UNDERSCORE)[ThuFileName.split(CommonConst.UNDERSCORE).length - 3] + ThuFileName.split(CommonConst.UNDERSCORE)[ThuFileName.split(CommonConst.UNDERSCORE).length - 2];

	    			// 日時が17桁(yyyyMMddHHmmssSSS)ない場合は0で埋める
	    			fileDateTime = zeroPadding(fileDateTime, 17);

	    	        // ファイル名から静止画・動画の判断
	    			// 静止画の場合
	    	        if (ThuFileName.startsWith(CommonConst.STILLIMAGE) || ThuFileName.startsWith(CommonConst.CAPTURE)){
	    				filePath = filePath.replace(CommonConst.THJPG, CommonConst.JPG);
	    				servletPath = filePath.replace(strUserRootPath, "");
	    		    // 動画の場合
	    	        }else {
	    	        	if (ThuFileName.startsWith(CommonConst.MOVIE)) {
	    	        		filePath = filePath.replace(CommonConst.THJPG, CommonConst.MP4);
	    	        		servletPath = filePath.replace(strUserRootPath, "");
	    	        	}else {
	    	        		filePath = filePath.replace(CommonConst.THJPG, CommonConst.WEBM);
	    	        		servletPath = filePath.replace(strUserRootPath, "");
	    	        	}
	    	        }

					// 撮影情報設定
					term_name = "";
					photo_date = "";
					if (contentsData) {
						if (!StringUtils.isEmpty(contentsModel.getTerm_name())) {
							term_name = contentsModel.getTerm_name();
						}
						if (contentsModel.getPhoto_date() != null) {
							photo_date = sdf.format(contentsModel.getPhoto_date());
						}
					}

					// 静止画の場合
			        if (ThuFileName.startsWith(CommonConst.STILLIMAGE) || ThuFileName.startsWith(CommonConst.CAPTURE)) {
				        modalHtml = imageModalHtml(fileCount, filePath, LibfileName, photo_date, servletPath);
				    // 動画の場合
			        }else {
				        modalHtml = videoModalHtml(fileCount, filePath, LibfileName, photo_date, ffmpegRoot, servletPath);
			        }

    				// ファイル用データをセット
    				image = new ImageData();

    				image.setModalNo(fileCount);
    				image.setFileName(LibfileName);
    				image.setDirectoryPath(tripleDesConverter.encrypt64(directoryPath));
    				image.setDateTime(fileDateTime);
    				image.setModalHtml(modalHtml);
    				image.setSrc(cnvBase64(file.toAbsolutePath().toString()));
    				image.setTermName(term_name);
					image.setPhotoDate(photo_date);

    				// 詳細表示の場合
    				if (view.equals("details")) {
    					// DataTable生成
    					image.setAddItemHtml(detailsTheadHtml(strTenantid));
    					image.setAddInfoHtml(detailsTbodyHtml(strTenantid, LibfileName));
    					if (DisplayPathFlg) {
							// ファイルパス表示
							image.setFilePath(filePath);
						}
    				}

					// 地図表示の場合
					if (view.equals("map")) {
						// 位置情報設定
						locate_lat = "0.0";
						locate_lon = "0.0";
						locate = "";
						if (contentsData) {
							if (!Double.isNaN(contentsModel.getLocate_lat())) {
								locate_lat = BigDecimal.valueOf(contentsModel.getLocate_lat()).toPlainString();
							}
							if (!Double.isNaN(contentsModel.getLocate_lon())) {
								locate_lon = BigDecimal.valueOf(contentsModel.getLocate_lon()).toPlainString();
							}
							if (contentsModel.getLocate() != null) {
								locate = contentsModel.getLocate();
							}
						}
						image.setLocateLat(locate_lat);
						image.setLocateLon(locate_lon);
						image.setLocate(locate);
					}

    				imageDataList.add(image);
    				fileCount++;
	        	}
			}
        }

		return imageDataList;
	}

	/**
	 * 検索期間範囲判定<br>
	 * @param fileName ファイル名
	 * @param startDateTime 開始日時
	 * @param endDateTime 終了日時
	 * @return boolean
	 */
	boolean between(String fileName, String startDateTime, String endDateTime) {

		// 検索 - 撮影日時欄が空欄の場合は何もしない
		if (startDateTime.equals("1900/01/01 00:00") && endDateTime.equals("9999/12/31 23:59")) {
			return true;
		}

		String fileDateTime = fileName.split(CommonConst.UNDERSCORE)[fileName.split(CommonConst.UNDERSCORE).length - 3] + fileName.split(CommonConst.UNDERSCORE)[fileName.split(CommonConst.UNDERSCORE).length - 2].substring(0, 4);

		String fileSdf = "yyyyMMddHHmm";
		String formSdf = "yyyy/MM/dd HH:mm";

		// LocalDateTime型変換
		LocalDateTime file = LocalDateTime.parse(fileDateTime, DateTimeFormatter.ofPattern(fileSdf));
		LocalDateTime start = LocalDateTime.parse(startDateTime, DateTimeFormatter.ofPattern(formSdf));
		LocalDateTime end = LocalDateTime.parse(endDateTime, DateTimeFormatter.ofPattern(formSdf));

		return !(start.isAfter(file) || end.isBefore(file));
	}

	/**
	 *  lengthで指定した桁数までゼロ埋めした文字列を返却する<br>
	 *  与えられた文字列よりlengthが短い場合、そのまま返却する<br>
	 *   @param  input  入力文字列
	 *   @param  length ゼロ埋めする桁数
	 *   @return   length分まで0を付加したinput
	 */
	private String zeroPadding(String input, int length) {
		int len = length - input.length();
		if (len > 0) {
			String pad = String.format("%" + len + "s", "").replace(" ", "0");
			return input + pad;
		}else {
			return input;
		}
	}


	/**
	 * 詳細表示ヘッダhtml作成<br>
	 * @return html文字列
	 */
	private String detailsTheadHtml(String strTenantid) {

		String theadHtml = "";

		//テナントIDから付帯情報項目名を検索
		List<AdditionalItemModel> addItemModel = additionalItemDao.useItem(strTenantid);

		for(int t=0; t < addItemModel.size(); t++) {
			theadHtml += "<th>" + addItemModel.get(t).getItem_name() + "</th>";
		}

		return theadHtml;
	}

	/**
	 * 詳細表示ボディhtml作成<br>
	 * @param fileName ファイル名
	 * @return html文字列
	 */
	private String detailsTbodyHtml(String strTenantid, String fileName) {

		String reg_no = "";
		String term_name = "";
		String photo_date = "";
		String locate = "";
		String update_user_id = "";
		String update_date = "";

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		// ディレクトリの場合は空欄
		if (!fileName.equals("")) {
			// ファイル名でCONTENTSテーブルを検索
			if (contentsDao.findContents_filename(fileName) == 1) {

				ContentsModel contentsModel = contentsDao.getContentsData_tenantid_filename(strTenantid, fileName);

				if (contentsModel.getReg_no() != null) {
					reg_no = contentsModel.getReg_no();
				}
				if (!StringUtils.isEmpty(contentsModel.getTerm_name())) {
					term_name = contentsModel.getTerm_name();
				}
				if (contentsModel.getPhoto_date() != null) {
					photo_date = sdf.format(contentsModel.getPhoto_date());
				}
				if (contentsModel.getLocate() != null) {
					locate = contentsModel.getLocate();
				}
				if (contentsModel.getUpdate_user_id() != null) {
					update_user_id = contentsModel.getUpdate_user_id();
				}
				if (contentsModel.getUpdate_date() != null) {
					update_date = sdf.format(contentsModel.getUpdate_date());
				}

			}
		}

		String tbodyHtml = "<td>"
							+ term_name
							+ "</td><td>"
							+ photo_date
							+ "</td><td>"
							+ locate
							+ "</td>";

		//テナントIDから付帯情報項目名を検索
		List<AdditionalItemModel> addItemModel = additionalItemDao.useItem(strTenantid);
		//登録番号から付帯情報を検索
		List<AdditionalInfoModel> addInfoModel = additionalInfoDao.findInfo(reg_no);
		//付帯情報項目名の数分リストを作る
		for(int t=0; t < addItemModel.size(); t++) {
			String ItemValue = "";
			//項目IDが一致するまでループ
			for(int i=0; i < addInfoModel.size(); i++) {
				if (addItemModel.get(t).getItem_id().equals(addInfoModel.get(i).getItem_id())) {
					ItemValue = addInfoModel.get(i).getItem_value();
					break;
				}
			}
			tbodyHtml += "<td>" + ItemValue + "</td>";
		}

		tbodyHtml += "<td>"
					+ update_user_id
					+ "</td><td>"
					+ update_date
					+ "</td>";

		return tbodyHtml;
	}

	/**
	 * 画像モーダル用html作成<br>
	 * @param number id用ナンバー
	 * @param filePath 画像ファイルパス
	 * @param fileName 画像ファイル名
	 * @param photo_date 撮影日時
	 * @param servletPath サーブレットパス
	 * @return html文字列
	 * @throws Exception
	 */
	private String imageModalHtml(int number, String filePath, String fileName, String photo_date, String servletPath) throws Exception {

		Float wrkX;
		Float wrkY;
		Float wrk1;
		String izimodalWidth = "";
		String izimodalTitle = "";

		// リスト取得時のファイル存在確認
		if (!filePath.contains("は削除されています:")) {
			Path file = Paths.get(filePath);
			// ファイル存在確認
			if (Files.exists(file)) {
				//画像サイズ比率からモーダルサイズを判定
				try {
					Dimension size = fileUtil.getSize(file);
					wrkX = Float.valueOf(size.width);
					wrkY = Float.valueOf(size.height);
					wrk1 = wrkX / wrkY;
					if ( wrk1 > 1.5555 ) {
						//縦横比 16:9
						izimodalWidth = "width:@izimodal-width16:9;";
					}else if ( wrk1 > 1.2 ) {
						//縦横比 4:3
						izimodalWidth = "width:@izimodal-width4:3;";
					}else if ( wrk1 < 0.6 ) {
						//縦横比 9:16
						izimodalWidth = "width:@izimodal-width9:16;";
					}else if ( wrk1 < 0.8 ) {
						//縦横比 3:4
						izimodalWidth = "width:@izimodal-width3:4;";
					}else {
						//ほぼ正方形
						izimodalWidth = "width:@izimodal-width1:1;";
					}
				}catch (Exception e1) {
					// エラー時は4:3サイズ
					izimodalWidth = "width:@izimodal-width4:3;";
					e1.printStackTrace();
				}
				izimodalTitle = photo_date;
			}else {
				izimodalWidth = "width:@izimodal-width4:3;";
				izimodalTitle = "画像は削除されています";
			}
		}else {
			izimodalWidth = "width:@izimodal-width4:3;";
			izimodalTitle = "画像は削除されています";
		}

		//URLエンコード
        try {
        	servletPath = URLEncoder.encode(servletPath, "UTF-8");
		}catch (UnsupportedEncodingException e) {
			// URLエンコード失敗(例外)
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			String str = sw.toString();
			log.info("@@MainCtrl.imageModalHtml URLエンコード失敗(例外)\n" + str);
		}

		String imageModal = "<div class='iziModal' id='Modal-No"
							+ number
							+ "'data-iziModal-icon='glyphicon glyphicon-picture' style='"
							+ izimodalWidth
							+ "' data-izimodal-title='"
							+ izimodalTitle
							+ "' fileName='"
							+ fileName
							+ "'>"
							+ "<figure style='text-align:center;'><img id='"
							+ fileName
							+ "'src='./imagefile?name="
							+ servletPath
							+ "' width='100%' /></figure></div>";

		return imageModal;
	}

	/**
	 * 動画モーダル用html作成<br>
	 * @param number id用ナンバー
	 * @param filePath 動画ファイルパス
	 * @param fileName 動画ファイル名
	 * @param photo_date 撮影日時
	 * @param ffmpegRoot FFmpegルートパス
	 * @param servletPath サーブレットパス
	 * @return html文字列
	 */
	private String videoModalHtml(int number, String filePath, String fileName, String photo_date, String ffmpegRoot, String servletPath) {

		Float wrkX;
		Float wrkY;
		Float wrk1;
		String izimodalWidth = "";
		String izimodalTitle = "";

		// リスト取得時のファイル存在確認
		if (!filePath.contains("は削除されています:")) {
			Path file = Paths.get(filePath);
			//ファイル存在確認
			if (Files.exists(file)) {
		    	//動画サイズ比率からモーダルサイズを判定
		    	try {
		    		//サイズを取得するパラメータを作成
		        	/* FFmpegTransパラメータ */
		        	ffmpegTrans = new FFmpegTrans(ffmpegRoot);
		        	ffmpegTrans.setInputFile(filePath);
		        	//実行
					ffmpegTrans.exec();
					//画面サイズを取得する
					wrkX = ffmpegTrans.getWidthSize();
					wrkY = ffmpegTrans.getHeightSize();
					wrk1 = wrkX / wrkY;
					if ( wrk1 > 1.5555 ) {
						//縦横比 16:9
						izimodalWidth = "width:@izimodal-width16:9;";
					}else if ( wrk1 > 1.2 ) {
						//縦横比 4:3
						izimodalWidth = "width:@izimodal-width4:3;";
					}else if ( wrk1 < 0.6 ) {
						//縦横比 9:16
						izimodalWidth = "width:@izimodal-width9:16;";
					}else if ( wrk1 < 0.8 ) {
						//縦横比 3:4
						izimodalWidth = "width:@izimodal-width3:4;";
					}else {
						//ほぼ正方形
						izimodalWidth = "width:@izimodal-width1:1;";
					}
				}catch (Exception e1) {
					// エラー時は4:3サイズ
					izimodalWidth = "width:@izimodal-width4:3;";
					e1.printStackTrace();
				}
		    	izimodalTitle = photo_date;
			}else {
				izimodalWidth = "width:@izimodal-width4:3;";
				izimodalTitle = "動画は削除されています";
			}
		}else {
			izimodalWidth = "width:@izimodal-width4:3;";
			izimodalTitle = "動画は削除されています";
		}

		//URLエンコード
        try {
        	servletPath = URLEncoder.encode(servletPath, "UTF-8");
		}catch (UnsupportedEncodingException e) {
			// URLエンコード失敗(例外)
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			String str = sw.toString();
			log.info("@@MainCtrl.videoModalHtml URLエンコード失敗(例外)\n" + str);
		}

		String videoModal = "<div class='iziModal' id='Modal-No"
							+ number
							+ "'data-iziModal-icon='glyphicon glyphicon-film' style='"
							+ izimodalWidth
							+ "' data-izimodal-title='"
							+ izimodalTitle
							+ "' fileName='"
							+ fileName
							+ "'>"
							+ "<figure style='text-align:center;'><video id='"
							+ fileName
							+ "' src='./videofile?name="
							+ servletPath
							+ "' width='100%' controls controlslist='nodownload'/></figure></div>";

		return videoModal;
	}

	/**
	 * 画像データをBase64エンコード<br>
	 * @param path パス
	 * @return 画像データ
	 */
	private String cnvBase64(String path) {
		StringBuffer srcData = new StringBuffer();

		// 画像タイプセット（JPEG）
		srcData.append(CommonConst.DATA_JPEG);

		// 画像データをBase64エンコードしてセット
		try {
			srcData.append(Encoder.Base64Encode(path));
		}catch (Exception e) {
			// Base64エンコード失敗(例外)
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			String str = sw.toString();
			log.info("@@MainCtrl.cnvBase64 Base64エンコード失敗(例外)\n" + str);
		}

		return srcData.toString();
	}

	/**
	 * コンテンツ取得<br>
	 * @param acc_userID 暗号化アクセスユーザID
	 * @param dirId ディレクトリID
	 * @param view 表示
	 * @param request リクエスト情報
	 * @return 画像イメージリストModelオブジェクト
	 * @throws Exception
	 */
	@RequestMapping(params="getContents", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getContents(@RequestParam String acc_userID, String dirId, String view, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		String json = null;

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_GETCONTENTS, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_GETCONTENTS, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_GETCONTENTS, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}else {

			String[] strPath = null;

			// DBからシステムルートパスを取得
			String strSystemRoot = systemInfoDao.findAll().getSystem_rootpath();
			// セッション情報からログインユーザIDを取得
			String user_id = (String) session.getAttribute(SessionConst.USER_ID);
			log.info("@@MainCtrl.getContents セッション情報取得[USER_ID=" + user_id + "]");
			// セッション情報から権限コードを取得
			String authcode = (String) session.getAttribute(SessionConst.AUTH_CODE);

			// テナント情報を取得
			TenantModel tenantModel = tenantDao.findTenant(strTenantid);
			// 詳細表示時ファイルパス表示フラグ
			boolean DisplayPathFlg = false;
			if (!StringUtils.isEmpty(tenantModel.getStr_display_file_path())) {
				// 詳細表示時ファイルパス表示
				DisplayPathFlg = true;
			}

			if (dirId.equals("-1")) {
				// ユーザのルート
				// ユーザーのルートパス
				// システムルート＋サムネイルルート＋テナントID（＋ライブラリパス）
				StringBuilder sb = new StringBuilder(strSystemRoot);
				sb.append(CommonConst.YEN_MRK);
				sb.append(CommonConst.THUMBNAIL);
				sb.append(CommonConst.YEN_MRK);
				sb.append(strTenantid);
				// セッションにユーザのLibraryルートパスをセット
				session.setAttribute(SessionConst.USER_ROOT_PATH, sb.toString().replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY) + CommonConst.YEN_MRK);
				if (!CommonConst.TENANT_MANAGER_CD.equals(authcode)) {
					// 一般ユーザー
					// ユーザー閲覧ライブラリリスト取得
					List<LibraryModel> userLibrary = libraryDao.findUser(strTenantid, user_id);
					if (userLibrary.size() > 0) {
						sb.append(CommonConst.YEN_MRK);
						strPath = new String[userLibrary.size()];
						for(int i=0; i < userLibrary.size(); i++) {
							strPath[i] = sb.toString() + userLibrary.get(i).getLib_path();
						}
					}else {
						// 閲覧ライブラリ無し
						strPath = new String[1];
						strPath[0] = "閲覧ライブラリ無し";
					}
				}else {
					// テナント管理者
					strPath = new String[1];
					strPath[0] = sb.toString();
				}
			}else {
				// ディレクトリ情報を複合化
				strPath = new String[1];
				strPath[0] = tripleDesConverter.decrypt64(dirId);
			}

			log.info("@@MainCtrl.getContents ユーザID = " + tripleDesConverter.decrypt64(acc_userID));
			log.info("@@MainCtrl.getContents 選択ライブラリ = " + Arrays.toString(strPath));
			log.info("@@MainCtrl.getContents 表示 = " + view);

			// セッション情報からユーザのルートパスを取得
			String strUserRootPath = (String) session.getAttribute(SessionConst.USER_ROOT_PATH);

			String[] pwdPath = new String[strPath.length];
			List<ImageData> imageDataList = new ArrayList<ImageData>();
			if (dirId.equals("-1")) {
				if (!strPath[0].equals("閲覧ライブラリ無し")) {
					String[] rootPwdPath = new String[strPath.length];
					for (int i=0; i < strPath.length; i++) {
						pwdPath[i] = strPath[i].replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY);
						Path dir = Paths.get(pwdPath[i]);
						rootPwdPath[i] = dir.getParent().toString();
					}
					imageDataList.addAll(getUserLibraryList(strTenantid, strPath, view, strUserRootPath, DisplayPathFlg));
					// 現在パスをセッションに格納(root)
					session.setAttribute(SessionConst.PWD_PATH, rootPwdPath);
				}
			}else {
				pwdPath[0] = strPath[0].replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY);
				imageDataList.addAll(getImageDataList(strTenantid, strPath[0], view, strUserRootPath, DisplayPathFlg));
				// 現在パスをセッションに格納
				session.setAttribute(SessionConst.PWD_PATH, pwdPath);
			}

			if (imageDataList.isEmpty() ==false) {
				ObjectMapper mapper = new ObjectMapper();
				json = mapper.writeValueAsString(imageDataList);
			}

			// 見せないパス
			String replacePath = strSystemRoot + CommonConst.YEN_MRK + CommonConst.THUMBNAIL + CommonConst.YEN_MRK;

			// アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_GETCONTENTS, CommonConst.SUCCESS_CO, "選択ライブラリ = " + Arrays.toString(strPath).replace(replacePath,"") + "\n表示 = " + view, "");
		}

		return json;
	}

	/**
	 * コンテンツ検索<br>
	 * @param acc_userID 暗号化アクセスユーザID
	 * @param view 表示
	 * @param searchTerm 検索端末ID
	 * @param startDateTime 検索範囲 開始時間
	 * @param endDateTime 検索範囲 終了時間
	 * @param searchAddInfo 付帯情報 検索キーワード
	 * @param sortCheck 検索ディレクトリID
	 * @param request リクエスト情報
	 * @return 画像イメージリストModelオブジェクト
	 * @throws Exception
	 */
	@RequestMapping(params="search", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String searchContents(@RequestParam String acc_userID, String view, String searchTerm, String startDateTime, String endDateTime, String searchAddInfo, @RequestParam("sortCheck[]") String[] sortCheck, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		String json = null;

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_SEARCHCONTENTS, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_SEARCHCONTENTS, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_SEARCHCONTENTS, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}else {

			// 現在パスをセッションから削除
			session.setAttribute(SessionConst.PWD_PATH, null);

			// セッション情報からログインユーザIDを取得
			String user_id = (String) session.getAttribute(SessionConst.USER_ID);
			log.info("@@MainCtrl.searchContents セッション情報取得[USER_ID=" + user_id + "]");
			//セッション情報から権限コードを取得
			String authcode = (String) session.getAttribute(SessionConst.AUTH_CODE);
			// テナント情報を取得
			TenantModel tenantModel = tenantDao.findTenant(strTenantid);
			// 詳細表示時ファイルパス表示フラグ
			boolean DisplayPathFlg = false;
			if (!StringUtils.isEmpty(tenantModel.getStr_display_file_path())) {
				// 詳細表示時ファイルパス表示
				DisplayPathFlg = true;
			}

			// 検索結果ファイル数格納用
			int searchFileCount = 0;

			String[] strPath = null;
			String searchLibLog = "";

			// システム情報を取得
			SystemInfoModel systeminfoModel = systemInfoDao.findAll();
			// システム情報からシステムルートパスを取得
			String strSystemRoot = systeminfoModel.getSystem_rootpath();
			// FFmpegシステムルートパスを取得
			String ffmpegRoot = strSystemRoot + CommonConst.YEN_MRK + CommonConst.FFMPEG;
			// Linux対応 on 2020.03.03
			if (PlatformUtil.isLinux()) {
				ffmpegRoot = CommonConst.FFMPEGCOM;
			}

			//正規表現を無効化
			String regex = Pattern.quote(strSystemRoot + CommonConst.YEN_MRK + CommonConst.THUMBNAIL + CommonConst.YEN_MRK);

			List<ImageData> imageDataList = new ArrayList<ImageData>();

			if (sortCheck[0].equals("-1")) {
				// ユーザのルート
				// ユーザーのルートパス
				// システムルート＋サムネイルルート＋テナントID（＋ライブラリパス）
				StringBuilder sb = new StringBuilder(strSystemRoot);
				sb.append(CommonConst.YEN_MRK);
				sb.append(CommonConst.THUMBNAIL);
				sb.append(CommonConst.YEN_MRK);
				sb.append(strTenantid);
				// セッションにユーザのLibraryルートパスをセット
				session.setAttribute(SessionConst.USER_ROOT_PATH, sb.toString().replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY) + CommonConst.YEN_MRK);
				if (!CommonConst.TENANT_MANAGER_CD.equals(authcode)) {
					// 一般ユーザー
					// ユーザー閲覧ライブラリリスト取得
					List<LibraryModel> userLibrary = libraryDao.findUser(strTenantid, user_id);
					if (userLibrary.size() > 0) {
						sb.append(CommonConst.YEN_MRK);
						strPath = new String[userLibrary.size()];
						for(int i=0; i < userLibrary.size(); i++) {
							strPath[i] = sb.toString() + userLibrary.get(i).getLib_path();
						}
					}else {
						// 閲覧ライブラリ無し
						strPath = new String[1];
						strPath[0] = "閲覧ライブラリ無し";
					}
				}else {
					// テナント管理者
					strPath = new String[1];
					strPath[0] = sb.toString();
				}
				if (!strPath[0].equals("閲覧ライブラリ無し")) {
					for (int i=0; i < strPath.length; i++) {
						searchLibLog += strPath[i].replaceFirst(regex,"") + CommonConst.COMMA + " ";
						imageDataList.addAll(searchImageDataList(strTenantid, strPath[i], view, searchTerm, startDateTime, endDateTime, searchAddInfo, sb.toString().replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY) + CommonConst.YEN_MRK, ffmpegRoot, DisplayPathFlg, searchFileCount));
					}
				}else {
					searchLibLog = strPath[0];
				}
			}else {
				// セッション情報からユーザのルートパスを取得
				String strUserRootPath = (String) session.getAttribute(SessionConst.USER_ROOT_PATH);
				strPath = new String[1];
				for(int i=0; i<sortCheck.length; i++) {
					// ディレクトリ情報を複合化
					strPath[0] = tripleDesConverter.decrypt64(sortCheck[i]);
					searchLibLog += strPath[0].replaceFirst(regex,"") + CommonConst.COMMA + " ";
					imageDataList.addAll(searchImageDataList(strTenantid, strPath[0], view, searchTerm, startDateTime, endDateTime, searchAddInfo, strUserRootPath, ffmpegRoot, DisplayPathFlg, searchFileCount));
					searchFileCount = imageDataList.size();
				}
			}

			if (imageDataList.isEmpty() == false) {
				ObjectMapper mapper = new ObjectMapper();
				json = mapper.writeValueAsString(imageDataList);
			}

			log.info("@@MainCtrl.searchContents ユーザID = " + tripleDesConverter.decrypt64(acc_userID));
			log.info("@@MainCtrl.searchContents 検索ライブラリ = " + searchLibLog);
			log.info("@@MainCtrl.searchContents 表示 = " + view);
			log.info("@@MainCtrl.searchContents 検索端末ID = " + searchTerm);
			log.info("@@MainCtrl.searchContents 検索期間 = " + startDateTime + " ～ " + endDateTime);
			log.info("@@MainCtrl.searchContents 検索キーワード = " + searchAddInfo);

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_SEARCHCONTENTS, CommonConst.SUCCESS_CO, "検索ライブラリ = " + searchLibLog + "\n表示 = " + view + "\n検索端末ID = " + searchTerm + "\n検索期間 = " + startDateTime + " ～ " + endDateTime + "\n検索キーワード = " + searchAddInfo, "");
		}

		return json;
	}

	/**
	 * 帳票出力要求取得<br>
	 * @param acc_userID 暗号化アクセスユーザID
	 * @param report_ID 出力帳票ID
	 * @param orderCheck 選択ファイル名
	 * @param orderDir 暗号化選択ファイルディレクトリ
	 * @param request リクエスト情報
	 * @return ダウンロードURL
	 * @throws Exception
	 */
	@RequestMapping(params="report", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String report(@RequestParam String acc_userID, String report_ID, @RequestParam("orderCheck[]") String[] orderCheck, @RequestParam("orderDir[]") String[] orderDir, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		String requestHtml = "false";

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_REPORT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_REPORT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_REPORT, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}else {

			// 帳票の存在と期限確認
			if (reportParamDao.limitCount(strTenantid, report_ID) == 1) {

				// 帳票IDをセッションに格納
				session.setAttribute(SessionConst.REPORT_ID, report_ID);
				// 帳票出力リクエストファイルをセッションに格納
				session.setAttribute(SessionConst.REQ_FILE, orderCheck);
				// 帳票出力リクエストディレクトリをセッションに格納
				session.setAttribute(SessionConst.REQ_DIR, orderDir);
				requestHtml = request.getRequestURL().toString().replace(".html", "/report/output.html");

			}else {
				log.info("@@MainCtrl.report ユーザID = " + tripleDesConverter.decrypt64(acc_userID));
				log.info("@@MainCtrl.report 帳票が削除されているか有効期限切れです。");
				requestHtml = "false";
			}

		}

		return requestHtml;
	}

	/**
	 * 帳票出力実行<br>
	 * @param response レスポンス情報
	 * @param request リクエスト情報
	 * @throws Exception
	 */
	@RequestMapping("report/output")
	public void reportOutput(HttpServletResponse response, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);
		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MAINCTRL_REPORT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MAINCTRL_REPORT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else {

			// セッション情報からログインユーザーのテナントIDを取得
			String strTenantid = (String)session.getAttribute(SessionConst.TENANT_ID);
			// テナント情報を取得
			TenantModel tenantModel = tenantDao.findTenant(strTenantid);
			// 詳細表示時ファイルパス表示フラグ
			boolean DisplayPathFlg = false;
			if (!StringUtils.isEmpty(tenantModel.getStr_display_file_path())) {
				// 詳細表示時ファイルパス表示
				DisplayPathFlg = true;
			}
			// セッションから帳票IDを取得
			String reportID = (String) session.getAttribute(SessionConst.REPORT_ID);
			// 帳票パラメータ情報取得
			ReportParamModel reportParamModel = reportParamDao.findOutput(strTenantid, reportID);
			// コンテンツ情報
			ContentsModel contentsModel;
			// 付帯情報リストを初期化
			List<AdditionalInfoModel> addInfoModel = null;
			// セッションからリクエストファイルを取得
			String[] targetFiles = (String[]) session.getAttribute(SessionConst.REQ_FILE);
			// セッションからリクエストディレクトリを取得
			String[] targetDir = (String[]) session.getAttribute(SessionConst.REQ_DIR);
			// 帳票IDをセッションから削除
			session.setAttribute(SessionConst.REPORT_ID, null);
			// 帳票出力リクエストファイルをセッションから削除
			session.setAttribute(SessionConst.REQ_FILE, null);
			// 帳票出力リクエストディレクトリをセッションから削除
			session.setAttribute(SessionConst.REQ_DIR, null);
			// ファイル名用
			String reportFileName = null;
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			// ダウンロードファイル名文字コード変更用
			String downloadFileName = null;
			// ログ用
			String downloadFileLog = "";
			// 行カウンタ
			int RowCnt = 0;
			// ページ(シート)カウンタ
			int PageCnt = 1;

			// ファイル名作成
			if (reportParamModel.getFile_name().endsWith(CommonConst.XLS)) {
				reportFileName = reportParamModel.getReport_name() + CommonConst.HYPHEN + sdf.format(new Date()) + CommonConst.XLS;
			}else {
				reportFileName = reportParamModel.getReport_name() + CommonConst.HYPHEN + sdf.format(new Date()) + CommonConst.XLSX;
			}

			try {
				// ダウンロードファイル名文字コード変更
				downloadFileName= new String(reportFileName.getBytes("MS932"), "ISO-8859-1");
			}catch (UnsupportedEncodingException e) {
				// Excelファイル名作成失敗(例外)
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				pw.flush();
				String str = sw.toString();
				log.info("@@MainCtrl.reportOutput Excelファイル名作成失敗(例外)\n" + str);
			}

			if (downloadFileName != null) {

				response.setContentType("application/vnd.ms-excel");
				response.setHeader("Content-Disposition", "attachment; filename=" + downloadFileName);

				// システム情報を取得
				SystemInfoModel systeminfoModel = systemInfoDao.findAll();
				// システム情報からシステムルートパスを取得
				String strSystemRoot = systeminfoModel.getSystem_rootpath();

				// テンプレートから出力ブック作成
				ReportBook outputBook = new ReportBook(strSystemRoot + CommonConst.YEN_MRK + CommonConst.REPORT + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK + reportParamModel.getFile_name(), "", ExcelOutputStreamExporter.FORMAT_TYPE);
				// 出力シート宣言
				ReportSheet outputSheet;

				// 表紙作成確認
				if (!StringUtils.isEmpty(reportParamModel.getStr_front_page_flg())) {
					// 表紙作成
					outputSheet = new ReportSheet(reportParamModel.getFront_page_sheet());
					outputBook.addReportSheet(outputSheet);
				}

				outputSheet = new ReportSheet(reportParamModel.getSheet_name(), reportParamModel.getSheet_name() + String.valueOf(PageCnt));

				for(int i=0; i<targetFiles.length; i++) {

					// 1ページ当たりの出力件数確認
					if (RowCnt == reportParamModel.getPrint_rows()) {
						RowCnt = 0;
						PageCnt++;
						outputBook.addReportSheet(outputSheet);
						outputSheet = new ReportSheet(reportParamModel.getSheet_name(), reportParamModel.getSheet_name() + String.valueOf(PageCnt));
					}

					// ファイルパス作成
					String filePath = tripleDesConverter.decrypt64(targetDir[i]) + CommonConst.YEN_MRK + targetFiles[i];
					String thuPath = filePath.replaceFirst(CommonConst.LIBRARY, CommonConst.THUMBNAIL).replace(CommonConst.JPG, CommonConst.THJPG);
					Path existsFile = Paths.get(filePath);
					Path existsThu = Paths.get(thuPath);

					// ファイル存在確認
					if (Files.exists(existsFile) && Files.exists(existsThu)) {

						// 行カウンタのカウントアップ
						RowCnt++;

						// イメージ
						outputSheet.addParam(ImageParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_IMAGE + String.valueOf(RowCnt), filePath);
						outputSheet.addParam(ImageParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_THUMBNAIL + String.valueOf(RowCnt), thuPath);
						// ファイル名
						outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_FILE_NAME + String.valueOf(RowCnt), targetFiles[i]);
						// ファイルパス
						if (DisplayPathFlg) {
							outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_FILE_PATH + String.valueOf(RowCnt), filePath);
						}
						// ファイルサイズ
						outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_FILE_SIZE + String.valueOf(RowCnt), fileUtil.getFileSizeStr(Files.size(existsFile)));

						// コンテンツ情報が存在するか確認
						if (contentsDao.findContents_filename(targetFiles[i]) == 1) {

							// ファイル名からコンテンツ情報取得
							contentsModel = contentsDao.getContentsData_tenantid_filename(strTenantid, targetFiles[i]);

							// 撮影端末ID
							outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_TERM_ID + String.valueOf(RowCnt), contentsModel.getTerm_id());
							// 撮影端末名
							outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_TERM_NAME + String.valueOf(RowCnt), contentsModel.getTerm_name());
							// 撮影日時
							outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_PHOTO_DATE + String.valueOf(RowCnt), contentsModel.getPhoto_date());
							// 撮影場所
							outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_LOCATE + String.valueOf(RowCnt), contentsModel.getLocate());
							// 撮影場所（緯度）
							outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_LOCATE_LAT + String.valueOf(RowCnt), contentsModel.getLocate_lat());
							// 撮影場所（経度）
							outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_LOCATE_LON + String.valueOf(RowCnt), contentsModel.getLocate_lon());

							// 登録番号から付帯情報を抽出
							addInfoModel = additionalInfoDao.findInfo(contentsModel.getReg_no());

							// 付帯情報
							for(int x=0; x < addInfoModel.size(); x++) {
								// 付帯情報項目の有効確認
								if (additionalItemDao.reportCount(strTenantid, addInfoModel.get(x).getItem_id()) == 1) {
									outputSheet.addParam(SingleParamParser.DEFAULT_TAG, addInfoModel.get(x).getItem_id() + String.valueOf(RowCnt), addInfoModel.get(x).getItem_value());
								}
							}

							// 対象ファイル名をログに出力
							downloadFileLog += targetFiles[i] + CommonConst.COMMA + " ";

						}else {
							// 対象ファイル情報が存在しない

							// 撮影端末ID
							outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_TERM_ID + String.valueOf(RowCnt), "ファイル情報が存在しません");
							// 撮影端末名
							outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_TERM_NAME + String.valueOf(RowCnt), "ファイル情報が存在しません");
							// 撮影日時
							outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_PHOTO_DATE + String.valueOf(RowCnt), "ファイル情報が存在しません");
							// 撮影場所
							outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_LOCATE + String.valueOf(RowCnt), "ファイル情報が存在しません");
							// 撮影場所（緯度）
							outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_LOCATE_LAT + String.valueOf(RowCnt), "ファイル情報が存在しません");
							// 撮影場所（経度）
							outputSheet.addParam(SingleParamParser.DEFAULT_TAG, CommonConst.REPORT_TAG_LOCATE_LON + String.valueOf(RowCnt), "ファイル情報が存在しません");

							// 対象ファイル名をログに出力
							downloadFileLog += targetFiles[i] + "(ファイル情報が存在しません)" + CommonConst.COMMA + " ";
						}

					}else {
						// 対象ファイルが存在しない
						// 対象ファイル名をログに出力
						downloadFileLog += targetFiles[i] + "(ファイルが削除されています)" + CommonConst.COMMA + " ";
					}

				}

				outputBook.addReportSheet(outputSheet);

				// レスポンスにストリーム出力
				ReportProcessor reportProcessor = new ReportProcessor();
				reportProcessor.addReportBookExporter( new ExcelOutputStreamExporter(response.getOutputStream()));
				reportProcessor.process(outputBook);

				log.info("@@MainCtrl.reportOutput ユーザID = " + (String) session.getAttribute(SessionConst.USER_ID));
				log.info("@@MainCtrl.reportOutput 帳票名 = " + reportParamModel.getReport_name());
				log.info("@@MainCtrl.reportOutput ダウンロードExcelファイル名 = " + reportFileName);
				log.info("@@MainCtrl.reportOutput 対象ファイル = [" + downloadFileLog + "]");

				// アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, session.getAttribute(SessionConst.USER_ID).toString(), CommonConst.MAINCTRL_REPORT, CommonConst.SUCCESS_CO, "帳票名 = " + reportParamModel.getReport_name() + "\nダウンロードExcelファイル名 = " + reportFileName + "\n対象ファイル = [" + downloadFileLog + "]", "");

			}else {
				// Excelファイル名作成失敗

				log.info("@@MainCtrl.reportOutput ユーザID = " + (String) session.getAttribute(SessionConst.USER_ID));
				log.info("@@MainCtrl.reportOutput Excelファイル名作成失敗");

				// アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, session.getAttribute(SessionConst.USER_ID).toString(), CommonConst.MAINCTRL_REPORT, CommonConst.FAILED_CO, "", "Excelファイル名作成失敗");
			}

		}

	}

	/**
	 * 付帯情報要求<br>
	 * @param acc_userID 暗号化アクセスユーザID
	 * @param orderCheck 選択ファイルID
	 * @param request リクエスト情報
	 * @return 付帯情報HTML
	 * @throws Exception
	 */
	@RequestMapping(params="additionalInfo", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String additionalInfo(@RequestParam String acc_userID, @RequestParam("orderCheck[]") String[] orderCheck, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		String json = null;

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_GETCONTENTS, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_GETCONTENTS, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_GETCONTENTS, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}else {

			log.info("@@MainCtrl.additionalInfo ユーザID = " + tripleDesConverter.decrypt64(acc_userID));

			// テナント情報を取得
			TenantModel tenantModel = tenantDao.findTenant(strTenantid);

			// 選択ファイル名を配列に格納
			String[] targetFiles = orderCheck;

			String additionalInfoHtml = "";

			String file_name = "";
			String term_name = "";
			String photo_date = "";
			String locate_lat = "0.0";
			String locate_lon = "0.0";
			String locate = "";
			String locateHtml = "";

			String additionalInfoMapHtml = "";

			ContentsModel contentsModel;

			if (targetFiles.length > 1) {
				additionalInfoHtml = "※複数のファイル<br><br>";
				for(int x=0; x<targetFiles.length; x++) {
					if (!targetFiles[x].endsWith(CommonConst.JPG) && !targetFiles[x].endsWith(CommonConst.MP4) && !targetFiles[x].endsWith(CommonConst.WEBM)) {
						additionalInfoHtml += "<font color='#FF0000'>※フォルダが選択されています</font><br><br>";
						break;
					}
				}
			}else if (targetFiles.length == 1) {
				if (contentsDao.findContents_filename(targetFiles[0]) == 1) {

					contentsModel = contentsDao.getContentsData_tenantid_filename(strTenantid, targetFiles[0]);
					file_name = targetFiles[0];
					if (!StringUtils.isEmpty(contentsModel.getTerm_name())) {
						term_name = contentsModel.getTerm_name();
					}
					if (contentsModel.getPhoto_date() != null) {
						photo_date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(contentsModel.getPhoto_date());
					}
					// 地図有効化判定
					if (!StringUtils.isEmpty(tenantModel.getStr_map_enable())) {
						if (!Double.isNaN(contentsModel.getLocate_lat())) {
							locate_lat = BigDecimal.valueOf(contentsModel.getLocate_lat()).toPlainString();
						}
						if (!Double.isNaN(contentsModel.getLocate_lon())) {
							locate_lon = BigDecimal.valueOf(contentsModel.getLocate_lon()).toPlainString();
						}
					}
					if (!StringUtils.isEmpty(contentsModel.getLocate())) {
						locate = contentsModel.getLocate();
						locateHtml = "</label></div><div class='form-group'><label class='control-label'>撮影場所：" + locate;
					}
					if (!locate_lat.equals("0.0") && !locate_lon.equals("0.0")) {
						additionalInfoMapHtml = "<div id='additionalInfoMap'></div>";
					}

					additionalInfoHtml = "<table style='table-layout:fixed; width:100%;'><tr><td style='word-wrap:break-word; width:50%; padding:10px 20px 10px 0px;'><div class='form-group'><label class='control-label'>ファイル：" + file_name + "</label></div><div class='form-group'><label class='control-label'>撮影端末：" + term_name + "</label></div><div class='form-group'><label class='control-label'>撮影日時：" + photo_date + locateHtml + "</label></div></td><td style='width:50%;'>" + additionalInfoMapHtml + "</td></tr></table>";

				}else {
					additionalInfoHtml = "<font color='#FF0000'>※ファイル情報が存在しません</font><br><br>";
				}
			}

			//テナントIDから付帯情報項目名を検索
			List<AdditionalItemModel> addItemModel = additionalItemDao.useItem(strTenantid);
			//付帯情報リストを初期化
			List<AdditionalInfoModel> addInfoModel = null;

			//付帯情報項目名の数分リストを作る
			for(int t=0; t < addItemModel.size(); t++) {
				String[] itemValues = new String[targetFiles.length];
				String[] valueCompares = new String[targetFiles.length];
				String itemValue = "";
				boolean ctsError = true;
				for(int x=0; x<targetFiles.length; x++) {
					itemValues[x] = "";
					if (!targetFiles[x].equals("")) {
						// ファイル名でCONTENTSテーブルを検索
						if (contentsDao.findContents_filename(targetFiles[x]) == 1) {
							contentsModel = contentsDao.getContentsData_filename(targetFiles[x]);
							if (contentsModel.getReg_no() != null) {
								//登録番号から付帯情報を検索
								addInfoModel = additionalInfoDao.findInfo(contentsModel.getReg_no());
								//項目IDが一致するまでループ
								for(int i=0; i < addInfoModel.size(); i++) {
									if (addItemModel.get(t).getItem_id().equals(addInfoModel.get(i).getItem_id())) {
										itemValues[x] = addInfoModel.get(i).getItem_value();
										break;
									}
								}
							}
						}else {
							ctsError = false;
							break;
						}
					}
				}

				if (ctsError) {
					//最初ファイルの値で比較用配列を初期化
					Arrays.fill(valueCompares, itemValues[0]);
					//配列を比較
					if (Arrays.equals(itemValues, valueCompares)) {
						//すべて同じであれば画面表示
						itemValue = itemValues[0];
					}
					additionalInfoHtml += "<div class='form-group'><label for='additionalInfo" + t + "' class='control-label'>" + addItemModel.get(t).getItem_name() + "</label><textarea class='form-control addInfo' id='additionalInfo" + t + "' addinfo-id = '" + addItemModel.get(t).getItem_id() + "' maxlength='2000'>" + itemValue + "</textarea></div>";
				}else {
					additionalInfoHtml = "<font color='#FF0000'>※ファイル情報が存在しないファイルが選択されています</font><br><br>";
					break;
				}
			}

			// ファイル用データをセット
			ImageData image = new ImageData();

			image.setFileName(file_name);
			image.setLocateLat(locate_lat);
			image.setLocateLon(locate_lon);
			image.setLocate(locate);
			image.setModalHtml(additionalInfoHtml);

			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(image);
		}

		return json;
	}

	/**
	 * 付帯情報更新<br>
	 * @param acc_userID 暗号化アクセスユーザID
	 * @param orderFile 更新ファイル
	 * @param orderInfoId 付帯情報項目ID
	 * @param orderInfoValue 付帯情報
	 * @param request リクエスト情報
	 * @return 付帯情報HTML
	 * @throws Exception
	 */
	@RequestMapping(params="additionalInfoEdit", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String additionalInfoEdit(@RequestParam String acc_userID, @RequestParam("orderFile[]") String[] orderFile, @RequestParam("orderInfoId[]") String[] orderInfoId, @RequestParam("orderInfoValue[]") String[] orderInfoValue, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		// 更新の成否
		String addInfoUpdate = "true";

		if (session == null){
			addInfoUpdate = null;
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_ADDITIONALINFOEDIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (session.getAttribute(SessionConst.USER_ID) == null) {
			addInfoUpdate = null;
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_ADDITIONALINFOEDIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			addInfoUpdate = null;
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_ADDITIONALINFOEDIT, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}else {

			log.info("@@MainCtrl.additionalInfoEdit ユーザID = " + tripleDesConverter.decrypt64(acc_userID));

			ContentsModel contentsModel;
			AdditionalInfoModel additionalInfoModel = new AdditionalInfoModel();
			String addInfoFileLog = "";
			String addInfoEditLog = "";
			String exceptionLog = "";

			try {
				for (int i=0; i < orderFile.length; i++) {
					addInfoFileLog += orderFile[i] + CommonConst.COMMA + " ";
					if (contentsDao.findContents_filename(orderFile[i]) == 1) {
						contentsModel = contentsDao.getContentsData_filename(orderFile[i]);
						additionalInfoModel.setReg_no(contentsModel.getReg_no());
						if (orderFile.length == 1) {
							// 対象ファイルが１つの場合
							additionalInfoDao.delete_regno(contentsModel.getReg_no());			// 対象ファイルの付帯情報を一旦すべて削除
						}
						if (orderInfoValue.length > 0) {										// 付帯情報項目名が1個場合のvalue空欄対策
							for (int x=0; x < orderInfoId.length; x++) {
								if (!StringUtils.isEmpty(orderInfoValue[x])) {
									// 内容が入力されている場合
									if (orderFile.length > 1) {
										// 対象ファイルが複数の場合
										if (additionalInfoDao.count(contentsModel.getReg_no(), orderInfoId[x]) != 0) {
											// 付帯情報が存在する場合削除
											additionalInfoDao.delete(contentsModel.getReg_no(), orderInfoId[x]);
										}
									}
									additionalInfoModel.setItem_id(orderInfoId[x]);
									additionalInfoModel.setItem_value(orderInfoValue[x]);
									additionalInfoModel.setUpdate_user_id( (String) session.getAttribute(SessionConst.USER_ID));
									additionalInfoDao.insert(additionalInfoModel);					// 付帯情報を登録
									if (i == 0) {
										addInfoEditLog += orderInfoId[x] + CommonConst.COLON + orderInfoValue[x] + CommonConst.COMMA + " ";
									}
								}
							}
						}
						// コンテンツ情報を更新
						contentsModel.setUpdate_user_id(tripleDesConverter.decrypt64(acc_userID));
						contentsDao.update(contentsModel);
					}else {
						addInfoUpdate = "false";
						break;
					}
				}
			}catch (Exception e) {
				addInfoUpdate = "false";
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				pw.flush();
				exceptionLog = sw.toString();
				log.info("@@MainCtrl.additionalInfoEdit " + exceptionLog);
			}

			log.info("@@MainCtrl.additionalInfoEdit 対象ファイル = [" + addInfoFileLog + "]");
			log.info("@@MainCtrl.additionalInfoEdit 付帯情報 = [" + addInfoEditLog + "]");

			//アクセスログ書き込み
			if (addInfoUpdate.equals("true")) {
				log.info("@@MainCtrl.additionalInfoEdit 付帯情報更新しました。");
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_ADDITIONALINFOEDIT, CommonConst.SUCCESS_CO, "対象ファイル = [" + addInfoFileLog + "]" + "\n付帯情報 = [" + addInfoEditLog + "]", "");
			}else {
				log.info("@@MainCtrl.additionalInfoEdit 付帯情報更新に失敗しました。");
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_ADDITIONALINFOEDIT, CommonConst.FAILED_CO, "対象ファイル = [" + addInfoFileLog + "]" + "\n付帯情報 = [" + addInfoEditLog + "]", exceptionLog);
			}

		}

		return addInfoUpdate;
	}

	/**
	 * ダウンロード要求取得<br>
	 * @param acc_userID 暗号化アクセスユーザID
	 * @param orderCheck ダウンロードファイル名
	 * @param orderDir 暗号化ダウンロードファイルディレクトリ
	 * @param request リクエスト情報
	 * @return ダウンロードURL
	 * @throws Exception
	 */
	@RequestMapping(params="download", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String download(@RequestParam String acc_userID, @RequestParam("orderCheck[]") String[] orderCheck, @RequestParam("orderDir[]") String[] orderDir, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		String requestHtml = null;

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_DOWNLOAD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_DOWNLOAD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_DOWNLOAD, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}else {

			log.info("@@MainCtrl.download ユーザID = " + tripleDesConverter.decrypt64(acc_userID));

			// ダウンロードリクエストをセッションに格納
			session.setAttribute(SessionConst.REQ_FILE, orderCheck);
			// ダウンロードディレクトリをセッションに格納
			session.setAttribute(SessionConst.REQ_DIR, orderDir);

			requestHtml = request.getRequestURL().toString().replace(".html", "/download/zip.html");
		}

		return requestHtml;
	}

	/**
	 * ダウンロード実行<br>
	 * @param response レスポンス情報
	 * @param request リクエスト情報
	 * @throws Exception
	 */
	@RequestMapping("download/zip")
	public void downloadZip(HttpServletResponse response, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);
		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MAINCTRL_DOWNLOAD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MAINCTRL_DOWNLOAD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else {

			log.info("@@MainCtrl.downloadZip ユーザID = " + (String) session.getAttribute(SessionConst.USER_ID));

			//暗号化ユーザーIDからユーザー情報を取得
			UserInfoModel userinfoModel = userInfoDao.getUser((String) session.getAttribute(SessionConst.USER_ID));
			//ユーザのテナントID設定
			String strTenantid = userinfoModel.getTenant_id();
			// セッションからユーザの現在パスを取得
			String[] targetPath = (String[]) session.getAttribute(SessionConst.PWD_PATH);
			// セッションからリクエストファイルを取得
			String[] targetFiles = (String[]) session.getAttribute(SessionConst.REQ_FILE);
			// セッションからリクエストディレクトリを取得
			String[] targetDir = (String[]) session.getAttribute(SessionConst.REQ_DIR);
			// ダウンロードリクエストファイルをセッションから削除
			session.setAttribute(SessionConst.REQ_FILE, null);
			// ダウンロードリクエストディレクトリをセッションから削除
			session.setAttribute(SessionConst.REQ_DIR, null);

			// zipファイル名作成
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			String zipFileName = session.getAttribute(SessionConst.USER_ID) + CommonConst.HYPHEN + sdf.format(new Date()) + CommonConst.ZIP;
			String downloadFileName = null;

			// ファイル存在確認用
			String filePath = null;

			//ログ用
			String downloadFileLog = "";
			try {
				downloadFileName= new String(zipFileName.getBytes("MS932"), "ISO-8859-1");
			}catch (UnsupportedEncodingException e) {
				// zipファイル名作成失敗(例外)
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				pw.flush();
				String str = sw.toString();
				log.info("@@MainCtrl.downloadZip zipファイル名作成失敗(例外)\n" + str);
			}

			if (downloadFileName != null) {
				response.setContentType("application/octet-stream;charset=MS932");
				response.setHeader("Content-Disposition", "attachment; filename=" + downloadFileName);
				response.setHeader("Content-Transfer-Encoding", "binary");

				OutputStream os = response.getOutputStream();
				ZipOutputStream zos = new ZipOutputStream(new BufferedOutputStream(os));

				try {
					// ファイルリスト初期化
					List<String> contentsFileList = new ArrayList<String>();
					// コンテンツリストデータ作成
					ContentsList contentsList = new ContentsList();

					for(int i=0; i<targetFiles.length; i++) {

						// ファイルパス作成
						if (targetPath != null) {
							for (int x=0; x<targetPath.length; x++) {
								if (tripleDesConverter.decrypt64(targetDir[i]).contains(targetPath[x])) {
									filePath = targetPath[x] + CommonConst.YEN_MRK + targetFiles[i];
									break;
								}
							}
						}else {
							filePath = tripleDesConverter.decrypt64(targetDir[i]) + CommonConst.YEN_MRK + targetFiles[i];
						}

						// ファイルリストに追加
						contentsList.addContentsFileList(filePath);

						Path existsFile = Paths.get(filePath);
						//ファイル存在確認
						if (Files.exists(existsFile)) {
							// 対象ファイル名をログに出力
							downloadFileLog += targetFiles[i] + CommonConst.COMMA + " ";
						}else {
							// 対象ファイルが存在しない
							// 対象ファイル名をログに出力
							downloadFileLog += targetFiles[i] + "(ファイルが削除されています)" + CommonConst.COMMA + " ";
						}

					}

					// ファイルリスト取得
					contentsFileList = contentsList.getFileList();

					// ファイルリストをzip化
					for(String file : contentsFileList) {

						ZipEntry ze = null;

						// ファイルが存在しない場合
						if (file.contains("は削除されています:")) {
							if (targetPath != null) {
								for (int x=0; x<targetPath.length; x++) {
									if (file.contains(targetPath[x])) {
										ze = new ZipEntry(file.replace(targetPath[x] + CommonConst.YEN_MRK, "").replace(":", ""));
										break;
									}
								}
							}else {
								ze = new ZipEntry(file.substring(file.lastIndexOf(CommonConst.YEN_MRK) + 1).replace(":", ""));
							}
							zos.putNextEntry(ze);
						// ファイルが存在する
						}else {
							if (targetPath != null) {
								for (int x=0; x<targetPath.length; x++) {
									if (file.contains(targetPath[x])) {
										ze = new ZipEntry(file.replace(targetPath[x] + CommonConst.YEN_MRK, ""));
										break;
									}
								}
							}else {
								ze = new ZipEntry(file.substring(file.lastIndexOf(CommonConst.YEN_MRK) + 1));
							}
							zos.putNextEntry(ze);
							Path path = Paths.get(file);
							if (Files.exists(path)) {
								BufferedInputStream in = new BufferedInputStream(new FileInputStream(file));
								try {
									byte[] data = new byte[8192];
									int len;
									while((len = in.read(data)) != -1) {
										zos.write(data, 0, len);
									}
								}
								catch(Exception e){
									// ファイル読み取り失敗
									StringWriter sw = new StringWriter();
									PrintWriter pw = new PrintWriter(sw);
									e.printStackTrace(pw);
									pw.flush();
									String str = sw.toString();
									log.info("@@MainCtrl.downloadZip ファイル読み取り失敗 対象ファイル = " + path.toAbsolutePath().toString() + "\n" + str);
								}finally {
									in.close();
								}
							}
						}

						zos.closeEntry();
					}
				}catch (Exception e) {
					// ダウンロード処理失敗(例外)
					StringWriter sw = new StringWriter();
					PrintWriter pw = new PrintWriter(sw);
					e.printStackTrace(pw);
					pw.flush();
					String str = sw.toString();
					log.info("@@MainCtrl.downloadZip ダウンロード処理失敗(例外)\n" + str);
				}finally {
					zos.close();
					os.close();
				}

				log.info("@@MainCtrl.downloadZip zipファイル名 = " + zipFileName);
				log.info("@@MainCtrl.downloadZip 対象ファイル = [" + downloadFileLog + "]");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, session.getAttribute(SessionConst.USER_ID).toString(), CommonConst.MAINCTRL_DOWNLOAD, CommonConst.SUCCESS_CO, "zipファイル名 = " + zipFileName + "\n対象ファイル = [" + downloadFileLog + "]", "");
			}else {
				//zipファイル名作成失敗

				log.info("@@MainCtrl.downloadZip zipファイル名作成失敗");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, session.getAttribute(SessionConst.USER_ID).toString(), CommonConst.MAINCTRL_DOWNLOAD, CommonConst.FAILED_CO, "", "zipファイル名作成失敗");
			}

		}

	}

	/**
	 * 移動実行<br>
	 * @param acc_userID 暗号化アクセスユーザID
	 * @param orderCheck 移動ファイル名
	 * @param orderDir 暗号化移動ファイルディレクトリ
	 * @param moveCheckDir 暗号化移動先ディレクトリID
	 * @param request リクエスト情報
	 * @return 移動成否
	 * @throws Exception
	 */
	@RequestMapping(params="move", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String move(@RequestParam String acc_userID, @RequestParam("orderCheck[]") String[] orderCheck, @RequestParam("orderDir[]") String[] orderDir, String moveCheckDir, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		// 移動失敗カウンタ
		int cnt = 0;

		// 移動の成否
		String fileMove = "true";

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_MOVE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_MOVE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_MOVE, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}else {

			log.info("@@MainCtrl.move ユーザID = " + tripleDesConverter.decrypt64(acc_userID));

			// 移動ファイル名を配列に格納
			String[] targetFiles = orderCheck;

			// 移動ファイルディレクトリを格納
			String[] targetDir = orderDir;

			// 移動先ディレクトリパスを作成
			String thumbnailDestPath = tripleDesConverter.decrypt64(moveCheckDir);
			String lbraryDestPath = thumbnailDestPath.replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY);
			Path thumbnailDestPaths = Paths.get(thumbnailDestPath);
			Path lbraryDestPaths = Paths.get(lbraryDestPath);

			// 移動先ディレクトリパスログ用
			String strSystemRoot = systemInfoDao.findAll().getSystem_rootpath();
			StringBuilder sb = new StringBuilder(strSystemRoot);
			sb.append(CommonConst.YEN_MRK);
			sb.append(CommonConst.THUMBNAIL);
			sb.append(CommonConst.YEN_MRK);
			String moveDestDirLog = thumbnailDestPath.replace(sb.toString(), "");

			// DB登録パス作成用
			String replacePath = sb.toString().replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY);

			for (int i = 0 ; i < 200 ; i++) {
				if (!fileLockTenant.contains(strTenantid)) {
					// テナント内で移動、または削除が行われていなければ処理実行
					fileLockTenant.add(strTenantid);
					break;
				}else {
					// テナント内で移動、または削除が行われている
					log.info("@@MainCtrl.move 移動実行待ち = " + (i + 1) + "回");
					if (i > 149) {
						// ファイルロック中テナント確認が150回を超えたら強制解除
						fileLockTenant.remove(fileLockTenant.indexOf(strTenantid));
						log.info("@@MainCtrl.move ファイルロック中テナント強制破棄");
					}
					try
					{
						Thread.sleep(i * 5); //i * 5ミリ秒Sleepする
					}
					catch(InterruptedException ex){}
				}
			}

			// 移動先ディレクトリの存在確認
			if (Files.exists(lbraryDestPaths) && Files.exists(thumbnailDestPaths)) {

				// ファイル移動のループ
				for(int i=0; i<targetFiles.length; i++) {

					String lbraryPath = tripleDesConverter.decrypt64(targetDir[i]) + CommonConst.YEN_MRK + targetFiles[i];
					String lbraryDestFilePath = lbraryDestPath + CommonConst.YEN_MRK + targetFiles[i];
					String thumbnailPath = lbraryPath.replaceFirst(CommonConst.LIBRARY, CommonConst.THUMBNAIL).replace(CommonConst.JPG, CommonConst.THJPG).replace(CommonConst.MP4, CommonConst.THJPG).replace(CommonConst.WEBM, CommonConst.THJPG);
					String thumbnailDestFilePath = (thumbnailDestPath + CommonConst.YEN_MRK + targetFiles[i]).replace(CommonConst.JPG, CommonConst.THJPG).replace(CommonConst.MP4, CommonConst.THJPG).replace(CommonConst.WEBM, CommonConst.THJPG);

					if (!recursiveMoveFile(lbraryPath, lbraryDestFilePath, replacePath)) {
						// ライブラリファイル移動失敗
						cnt++;
					}

					if (!recursiveMoveFile(thumbnailPath, thumbnailDestFilePath, replacePath)) {
						// サムネイルファイル移動失敗
						cnt++;
					}

				}

				if (cnt > 0) {
					fileMove = "false";
				}

				if (fileMove.equals("true")) {
					log.info("@@MainCtrl.move ファイルを移動しました。テナントID = " + strTenantid + ", ユーザーID = " + tripleDesConverter.decrypt64(acc_userID) + ", 移動先 = " + moveDestDirLog + ", 対象ファイル = " + Arrays.toString(targetFiles));
					//アクセスログ書き込み
					logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, session.getAttribute(SessionConst.USER_ID).toString(), CommonConst.MAINCTRL_MOVE, CommonConst.SUCCESS_CO, "移動先 = " + moveDestDirLog + "\n対象ファイル = " + Arrays.toString(targetFiles), "");
				}else {
					log.info("@@MainCtrl.move ファイル移動に失敗しました。いくつかのファイルが移動されていません。テナントID = " + strTenantid + ", ユーザーID = " + tripleDesConverter.decrypt64(acc_userID) + ", 移動先 = " + moveDestDirLog + ", 対象ファイル = " + Arrays.toString(targetFiles) + ", 移動失敗ファイル数 = " + cnt);
					//アクセスログ書き込み
					logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, session.getAttribute(SessionConst.USER_ID).toString(), CommonConst.MAINCTRL_MOVE, CommonConst.FAILED_CO, "移動先 = " + moveDestDirLog + "\n対象ファイル = " + Arrays.toString(targetFiles), "いくつかのファイルが移動されていません。\n移動失敗ファイル数 = " + cnt);
				}

			}else {
				// 移動先ディレクトリが存在しない
				fileMove = "false";

			}

			// ファイルロック中テナント解除
			fileLockTenant.remove(fileLockTenant.indexOf(strTenantid));
		}

		return fileMove;
	}

	/**
	 * 対象のファイルオブジェクトの移動を行う.<BR>
	 * @param filePath 移動元ファイルパス
	 * @param destFilePath 移動先ファイルパス
	 * @param replacePath DB登録パス作成用
	 */
	private boolean recursiveMoveFile(final String filePath, final String destFilePath, final String replacePath) {

		// 移動失敗カウンタ
		int cnt = 0;

		boolean fileMove = true;

		Path filePaths = Paths.get(filePath);
		Path destFilePaths = Paths.get(destFilePath);

		// 移動元ファイルが存在しない場合は処理終了
		if (!Files.exists(filePaths)) {
			log.info("@@MainCtrl.recursiveMoveFile ファイル = " + filePath + " が存在しません。");
			fileMove = true;
			return fileMove;
		}

		// ファイルを移動する
		// 失敗したらリトライ
		for (int x = 0 ; x < CommonConst.RETRY_COUNT ; x++) {
			try {
				Files.move(filePaths, destFilePaths);
				log.info("@@MainCtrl.recursiveMoveFile ファイル移動 対象ファイル = " + filePath + ", 移動後ファイル = " + destFilePath);
				//ファイル移動に成功したらループから抜ける
			    break;
			}catch (Exception e) {
				//ファイル移動失敗
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				pw.flush();
				String str = sw.toString();
				log.info("@@MainCtrl.recursiveMoveFile ファイル移動失敗回数 = " + (x + 1) + "\n" + str);
				try
				{
					Thread.sleep(x * 10); //x * 10ミリ秒Sleepする
				}
				catch(InterruptedException ex){}
			}

			if (x == CommonConst.RETRY_COUNT -1) {
				// 最後まで移動できなかった場合
				cnt++;
			}
		}

		if (cnt > 0) {
			fileMove = false;
		}else {
			// ファイル移動に成功したらDBデータファイルパス変更
			if (contentsDao.findContents_filename(filePaths.getFileName().toString()) == 1) {
				ContentsModel contentsModel = contentsDao.getContentsData_filename(filePaths.getFileName().toString());
				// CONTENTSテーブルのファイルパス変更
				contentsDao.update_path(contentsModel.getReg_no(), destFilePaths.getParent().toString().replace(replacePath, ""));
				log.info("@@MainCtrl.recursiveMoveFile DBファイルパス変更登録番号 = " + contentsModel.getReg_no());
			}
		}

		return fileMove;
	}

	/**
	 * 削除実行<br>
	 * @param acc_userID 暗号化アクセスユーザID
	 * @param orderCheck 削除ファイル名
	 * @param orderDir 暗号化削除ファイルディレクトリ
	 * @param request リクエスト情報
	 * @return 削除成否
	 * @throws Exception
	 */
	@RequestMapping(params="delete", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String delete(@RequestParam String acc_userID, @RequestParam("orderCheck[]") String[] orderCheck, @RequestParam("orderDir[]") String[] orderDir, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		// 削除失敗カウンタ
		int cnt = 0;

		// 削除の成否
		String fileDelete = "true";

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_DELETE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_DELETE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MAINCTRL_DELETE, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}else {

			log.info("@@MainCtrl.delete ユーザID = " + tripleDesConverter.decrypt64(acc_userID));

			// 削除ファイル名を配列に格納
			String[] targetFiles = orderCheck;

			// 削除ファイルディレクトリを格納
			String[] targetDir = orderDir;

			for (int i = 0 ; i < 200 ; i++) {
				if (!fileLockTenant.contains(strTenantid)) {
					// テナント内で移動、または削除が行われていなければ処理実行
					fileLockTenant.add(strTenantid);
					break;
				}else {
					// テナント内で移動、または削除が行われている
					log.info("@@MainCtrl.delete 削除実行待ち = " + (i + 1) + "回");
					if (i > 149) {
						// ファイルロック中テナント確認が150回を超えたら強制解除
						fileLockTenant.remove(fileLockTenant.indexOf(strTenantid));
						log.info("@@MainCtrl.delete ファイルロック中テナント強制破棄");
					}
					try
					{
						Thread.sleep(i * 5); //i * 5ミリ秒Sleepする
					}
					catch(InterruptedException ex){}
				}
			}

			// ファイル削除のループ
			for(int i=0; i<targetFiles.length; i++) {

				String lbraryPath = tripleDesConverter.decrypt64(targetDir[i]) + CommonConst.YEN_MRK + targetFiles[i];
				String thumbnailPath = lbraryPath.replaceFirst(CommonConst.LIBRARY, CommonConst.THUMBNAIL).replace(CommonConst.JPG, CommonConst.THJPG).replace(CommonConst.MP4, CommonConst.THJPG).replace(CommonConst.WEBM, CommonConst.THJPG);
				Path FILIB = Paths.get(lbraryPath);
				Path FITHU = Paths.get(thumbnailPath);

				if (!recursiveDeleteFile(FILIB)) {
					// ライブラリファイル削除失敗
					cnt++;
				}

				if (!recursiveDeleteFile(FITHU)) {
					// サムネイルファイル削除失敗
					cnt++;
				}

			}

			if (cnt > 0) {
				fileDelete = "false";
			}

			if (fileDelete.equals("true")) {
				log.info("@@MainCtrl.delete ファイルを削除しました。テナントID = " + strTenantid + ", ユーザーID = " + tripleDesConverter.decrypt64(acc_userID) + ", 対象ファイル = " + Arrays.toString(targetFiles));
				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, session.getAttribute(SessionConst.USER_ID).toString(), CommonConst.MAINCTRL_DELETE, CommonConst.SUCCESS_CO, "対象ファイル = " + Arrays.toString(targetFiles), "");
			}else {
				log.info("@@MainCtrl.delete ファイル削除に失敗しました。いくつかのファイルが削除されていません。テナントID = " + strTenantid + ", ユーザーID = " + tripleDesConverter.decrypt64(acc_userID) + ", 対象ファイル = " + Arrays.toString(targetFiles) + ", 削除失敗ファイル数 = " + cnt);
				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, session.getAttribute(SessionConst.USER_ID).toString(), CommonConst.MAINCTRL_DELETE, CommonConst.FAILED_CO, "対象ファイル = " + Arrays.toString(targetFiles), "いくつかのファイルが削除されていません。\n削除失敗ファイル数 = " + cnt);
			}

			// ファイルロック中テナント解除
			fileLockTenant.remove(fileLockTenant.indexOf(strTenantid));
		}

		return fileDelete;
	}

	/**
	 * 対象のファイルオブジェクトの削除を行う.<BR>
	 * ディレクトリの場合は再帰処理を行い、削除する。
	 * @param file ファイルオブジェクト
	 * @throws Exception
	 */
	private boolean recursiveDeleteFile(final Path file) throws Exception {

		// 削除失敗カウンタ
		int cnt = 0;

		boolean fileDelete = true;

		// 存在しない場合は処理終了
		if (Files.notExists(file)) {
			log.info("@@MainCtrl.recursiveDeleteFile ファイル = " + file.toAbsolutePath().toString() + " が存在しません。");
			fileDelete = true;
			return fileDelete;
		}
		// 対象がディレクトリの場合は再帰処理
		if (Files.isDirectory(file)) {
			try(DirectoryStream<Path> dirStream = Files.newDirectoryStream(file)) {	// streamのclose用try-with-resources構文
				for (Path child : dirStream) {
					if (!recursiveDeleteFile(child)) {
						// 削除失敗
						cnt++;
					}
				}
			}catch (Exception e) {
				// ディレクトリストリーム取得失敗(例外)
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				pw.flush();
				String str = sw.toString();
				log.info("@@MainCtrl.recursiveDeleteFile ディレクトリストリーム取得失敗ファイル(例外) = " + file.toAbsolutePath().toString() + "\n" + str);
			}
		}
		// 対象がファイルもしくは配下が空のディレクトリの場合は削除する
		// 失敗したらリトライ
		for (int x = 0 ; x < CommonConst.RETRY_COUNT ; x++) {
			try {
				Files.deleteIfExists(file);
				if (Files.notExists(file)) {
					// 削除に成功したらループから抜ける
					log.info("@@MainCtrl.recursiveDeleteFile ファイル削除 対象ファイル = " + file.toAbsolutePath().toString());
					break;
				}else {
					// 削除失敗
					log.info("@@MainCtrl.recursiveDeleteFile 削除失敗ファイル = " + file.toAbsolutePath().toString() + " 削除失敗回数 =" + (x + 1));
					try
					{
						Thread.sleep(x * 10); //x * 10ミリ秒Sleepする
					}
					catch(InterruptedException ex){}
				}
			}catch (Exception e) {
				// 削除失敗(例外)
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				pw.flush();
				String str = sw.toString();
				log.info("@@MainCtrl.recursiveDeleteFile 削除失敗ファイル = " + file.toAbsolutePath().toString() + " 削除失敗回数(例外) =" + (x + 1) + "\n" + str);
				try
				{
					Thread.sleep(x * 10); //x * 10ミリ秒Sleepする
				}
				catch(InterruptedException ex){}
			}
			if (x == CommonConst.RETRY_COUNT -1) {
				// 最後まで削除できなかった場合
				cnt++;
			}
		}

		if (cnt > 0) {
			fileDelete = false;
		}else {
			// ファイル削除に成功したらDBデータ削除
			if (contentsDao.findContents_filename(file.getFileName().toString()) == 1) {
				ContentsModel contentsModel = contentsDao.getContentsData_filename(file.getFileName().toString());
				// CONTENTSテーブルからデータを削除
				contentsDao.deleteContents_regno(contentsModel.getReg_no());
				// ADDITIONALINFOテーブルからデータを削除
				additionalInfoDao.delete_regno(contentsModel.getReg_no());
				log.info("@@MainCtrl.recursiveDeleteFile DB削除ファイル登録番号 = " + contentsModel.getReg_no());
			}
		}

		return fileDelete;
	}

	/**
	 * ディレクトリツリーデータ再取得<br>
	 * @param acc_userID 暗号化アクセスユーザID
	 * @param request リクエスト情報
	 * @return ディレクトリツリーデータ
	 * @throws Exception
	 */
	@RequestMapping(params="updateTree", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String updateTree(@RequestParam String acc_userID, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		String json = null;

		if (session == null){
			throw new ExException(MessageConst.W002);
		}else if (session.getAttribute(SessionConst.USER_ID) == null) {
			throw new ExException(MessageConst.W002);
		}else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			throw new ExException(MessageConst.W002);
		}else {

			// DBからシステムルートパスを取得
			String strSystemRoot = systemInfoDao.findAll().getSystem_rootpath();
			// セッション情報からログインユーザIDを取得
			String user_id = (String) session.getAttribute(SessionConst.USER_ID);
			// セッション情報からログインユーザーのテナントIDを取得
			String strTenantid = (String)session.getAttribute(SessionConst.TENANT_ID);
			// セッション情報から権限コードを取得
			String authcode = (String) session.getAttribute(SessionConst.AUTH_CODE);
			// ユーザーのルートパス
			// システムルート＋サムネイルルート＋テナントID（＋ライブラリパス）
			StringBuilder sb = new StringBuilder(strSystemRoot);
			sb.append(CommonConst.YEN_MRK);
			sb.append(CommonConst.THUMBNAIL);
			sb.append(CommonConst.YEN_MRK);
			sb.append(strTenantid);
			// セッションにユーザのLibraryルートパスをセット
			session.setAttribute(SessionConst.USER_ROOT_PATH, sb.toString().replaceFirst(CommonConst.THUMBNAIL, CommonConst.LIBRARY) + CommonConst.YEN_MRK);
			if (!CommonConst.TENANT_MANAGER_CD.equals(authcode)) {
				// 一般ユーザー
				// ユーザー閲覧ライブラリリスト取得
				List<LibraryModel> userLibrary = libraryDao.findUser(strTenantid, user_id);
				if (userLibrary.size() > 0) {
					String strTenantPath = sb.toString();
					sb.append(CommonConst.YEN_MRK);
					StringBuilder directoryTreeData = new StringBuilder();
					String tmpDirectoryData;
					for(int i=0; i < userLibrary.size(); i++) {
						tmpDirectoryData = getDirectoryTreeData(strTenantid, strTenantPath, sb.toString() + userLibrary.get(i).getLib_path());
						if (!tmpDirectoryData.equals("error")) {
							directoryTreeData.append(tmpDirectoryData);
							userLibrary.get(i).getLib_path();
							if (i != userLibrary.size() - 1) {
								directoryTreeData.append(CommonConst.COMMA);
							}
						}else {
							directoryTreeData.setLength(0);
							directoryTreeData.append("error");
							break;
						}
					}
					json = directoryTreeData.toString();
				}else {
					// 閲覧ライブラリ無し
					json = "error";
				}
			}else {
				// テナント管理者
				json = getDirectoryTreeData(strTenantid, sb.toString(), sb.toString());
			}

		}

		return json;
	}

}