/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込み変更。TripleDES変換クラス宣言(インスタンス化)変更。
 *    3    2019/10/23      1.0.2           女屋            不要メンバ変数削除と修正
 */
package jp.reneil.reiss.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvGenerator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.AccessLogDao;
import jp.reneil.reiss.dao.CodeDao;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.TenantDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.exception.ExException;
import jp.reneil.reiss.form.SystemManageAccessedLogForm;
import jp.reneil.reiss.log.LogWriting;
import jp.reneil.reiss.model.AccessLogModel;
import jp.reneil.reiss.model.AccessLogSysCsvModel;
import jp.reneil.reiss.model.UserInfoModel;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * システム管理_アクセスログ_画面コントローラクラスです。<br>
 *
 * @version 1.0.2
 */
@Controller
@RequestMapping(value="systemManage/accessedlog")
public class SystemManageAccessedLogCtrl {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(SystemManageAccessedLogCtrl.class);

	/** ログ書き込みクラス */
	@Autowired
	private LogWriting logWriting;

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** テナントテーブルアクセス制御クラス */
	@Autowired
	private TenantDao tenantDao;

	/** ユーザー情報テーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/** コードテーブルアクセス制御クラス */
	@Autowired
	private CodeDao codeDao;

	/** アクセスログテーブルアクセス制御クラス */
	@Autowired
	private AccessLogDao accessLogDao;

	/** 画面表示
	 * @throws Exception */
	@RequestMapping(method=GET)
	public String init(@ModelAttribute("systemManageAccessedLogForm") SystemManageAccessedLogForm systemManageAccessedLogForm, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_ACCESSEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_ACCESSEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) != CommonConst.SYSTEM_MANAGER) {
			log.info("@@SystemManageAccessedLogCtrl.init アクセス権限違反[USER_ID=" + SessionConst.USER_ID + "]");
			throw new ExException(MessageConst.W015);
		}

		// セッション情報からログインユーザIDを取得
		String user_id = (String) session.getAttribute(SessionConst.USER_ID);
		log.info("@@SystemManageAccessedLogCtrl.init セッション情報取得[USER_ID=" + user_id + "]");

		// システム名
		model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());
		// 暗号化ユーザID
		model.addAttribute("acc_userID", tripleDesConverter.encrypt64(user_id));
		// テナント情報取得
		model.addAttribute("tenant", tenantDao.findAll());
		// ユーザー情報取得
		model.addAttribute("user", userInfoDao.findAll());
		// 実行プログラム
		model.addAttribute("execution", codeDao.getCode01RangeData("50", "99"));
		// 処理結果
		model.addAttribute("result", codeDao.getCode01Data("00"));

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), "null", user_id, CommonConst.SYS_ACCESSEDLOGCTRL_INIT, CommonConst.SUCCESS_CO, "", "");

		return "systemManage/accessedlog";
	}

	/**
	 * 選択したテナント以外に所属するユーザー情報取得<br>
	 * @param tenantId テナントID
	 * @param request リクエスト情報
	 * @return ユーザー リストModelオブジェクト
	 * @throws Exception
	 */
	@RequestMapping(params="getNotTenantUser", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String getNotTenantUser(@RequestParam String acc_userID, String tenantId, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_ACCESSEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_ACCESSEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) != CommonConst.SYSTEM_MANAGER) {
			log.info("@@SystemManageAccessedLogCtrl.getNotTenantUser アクセス権限違反[USER_ID=" + SessionConst.USER_ID + "]");
			throw new ExException(MessageConst.W015);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(acc_userID), CommonConst.SYS_ACCESSEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		String json = null;

		//ユーザー情報格納
		List<UserInfoModel> UserList = new ArrayList<UserInfoModel>();
		UserList.addAll(userInfoDao.findNotTenantAllUser(tenantId));

		if (UserList.isEmpty() ==false) {
			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(UserList);
		}

		return json;
	}

	/**
	 * アクセスログ検索<br>
	 * @param startDate 検索開始日時
	 * @param endDate 検索終了日時
	 * @param tenantId テナントID
	 * @param userId ユーザーID
	 * @param executionCode 実行プログラムコード
	 * @param resultCode 処理結果コード
	 * @param request リクエスト情報
	 * @return アクセスログ リストModelオブジェクト
	 * @throws Exception
	 */
	@RequestMapping(params="searchAccesslog", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String searchAccesslog(@RequestParam String acc_userID, String startDate, String endDate, String tenantId, String userId, String executionCode, String resultCode, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_ACCESSEDLOGCTRL_SEARCHACCESSLOG, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_ACCESSEDLOGCTRL_SEARCHACCESSLOG, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) != CommonConst.SYSTEM_MANAGER) {
			log.info("@@SystemManageAccessedLogCtrl.searchAccesslog アクセス権限違反[USER_ID=" + SessionConst.USER_ID + "]");
			throw new ExException(MessageConst.W015);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(acc_userID), CommonConst.SYS_ACCESSEDLOGCTRL_SEARCHACCESSLOG, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		String json = null;

		/** アクセスログ情報格納 */
		List<AccessLogModel> AccessLogList = new ArrayList<AccessLogModel>();
		AccessLogList.addAll(accessLogDao.find(startDate, endDate, tenantId, userId, executionCode, resultCode));

		if (AccessLogList.isEmpty() ==false) {
			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(AccessLogList);
		}

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(acc_userID), CommonConst.SYS_ACCESSEDLOGCTRL_SEARCHACCESSLOG, CommonConst.SUCCESS_CO, "検索期間 = " + startDate + " ～ " + endDate + "\n検索テナントID = " + tenantId + "\n検索ユーザID = " + userId + "\n検索実行プログラムコード = " + executionCode + "\n検索処理結果コード = " + resultCode, "");

		return json;
	}

	/**
	 * CSVダウンロード<br>
	 * @param systemManageAccessedLogForm アクセスログ画面フォーム
	 * @param request リクエスト情報
	 * @param respons レスポンス情報
	 * @return アクセスログCSVファイルレスポンス
	 * @throws Exception
	 */
	@RequestMapping(params="searchAccesslogCsv", method=RequestMethod.POST, produces = "text/html;charset=UTF-8")
	public void searchAccesslogCsv(@ModelAttribute("systemManageAccessedLogForm") SystemManageAccessedLogForm systemManageAccessedLogForm, HttpServletRequest request, HttpServletResponse response) throws Exception {

		// ユーザIDを取得
		String user_id = tripleDesConverter.decrypt64(systemManageAccessedLogForm.getAcc_userID());

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", user_id, CommonConst.SYS_ACCESSEDLOGCTRL_SEARCHACCESSLOG_CSV, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", user_id, CommonConst.SYS_ACCESSEDLOGCTRL_SEARCHACCESSLOG_CSV, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(user_id)) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", user_id, CommonConst.SYS_ACCESSEDLOGCTRL_SEARCHACCESSLOG_CSV, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		log.info("@@SystemManageAccessedLogCtrl.searchAccesslogCsv ユーザID = " + user_id);

		// ログ出力範囲を設定
		String start_date = "1970/01/01";
		String end_date = "9999/12/31";
		if (!StringUtils.isEmpty(systemManageAccessedLogForm.getStart_date())) {
			start_date = systemManageAccessedLogForm.getStart_date();
		}
		if (!StringUtils.isEmpty(systemManageAccessedLogForm.getEnd_date())) {
			end_date = systemManageAccessedLogForm.getEnd_date();
		}

		// ファイル名
		String reportName = null;
		// ダウンロードファイル名文字コード変更用
		String downloadFileName = null;

		try {

			// ダウンロードファイル名作成
			SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddHHmmssSSS");
			reportName = systemInfoDao.findAll().getSystem_name() + "アクセスログ" + CommonConst.HYPHEN + start_date.replaceAll("[^0-9]","") + "～" + end_date.replaceAll("[^0-9]","") + CommonConst.HYPHEN + user_id + fmt.format(new Date()) + CommonConst.CSV;
			// URLエンコード
	        try {
	        	downloadFileName = URLEncoder.encode(reportName, "UTF-8");
			}catch (UnsupportedEncodingException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
			}

			// アクセスログ(csv)
			List<AccessLogSysCsvModel> accessLogSysCsvModel = accessLogDao.findSysCsv(start_date, end_date, systemManageAccessedLogForm.getTenant_id(), systemManageAccessedLogForm.getUser_id(), systemManageAccessedLogForm.getExecution_code(), systemManageAccessedLogForm.getResult_code());

			CsvMapper mapper = new CsvMapper();
			// データをダブルクォーテーションで囲う
			mapper.configure(CsvGenerator.Feature.ALWAYS_QUOTE_STRINGS, true);

			// ヘッダを付ける
			CsvSchema schema = mapper.schemaFor(AccessLogSysCsvModel.class).withHeader();
			String csvText = mapper.writer(schema).writeValueAsString(accessLogSysCsvModel);

			// ファイル書き込み
			response.setContentType("application/octet-stream;charset=MS932");
			response.setHeader("Content-Disposition", "attachment; filename=" + downloadFileName);

			// CSVファイル出力
			try (PrintWriter pw = response.getWriter()) {
	            pw.print(csvText);
	        }

			//アクセスログ書き込み
			StringBuilder msg = new StringBuilder();
			msg.append("検索期間 = ");
			msg.append(start_date);
			msg.append(" ～ ");
			msg.append(end_date);
			msg.append("\n検索テナントID = ");
			msg.append(systemManageAccessedLogForm.getTenant_id());
			msg.append("\n検索ユーザID = ");
			msg.append(systemManageAccessedLogForm.getUser_id());
			msg.append("\n検索実行プログラムコード = ");
			msg.append(systemManageAccessedLogForm.getExecution_code());
			msg.append("\n検索処理結果コード = ");
			msg.append(systemManageAccessedLogForm.getResult_code());
			msg.append("\ncsvファイル名 = ");
			msg.append(reportName);
			log.info("@@SystemManageAccessedLogCtrl.searchAccesslogCsv csvダウンロード実行：" + "検索期間 = " + start_date + " ～ " + end_date + ", 検索テナントID = " + systemManageAccessedLogForm.getTenant_id() + ", 検索ユーザID = " + systemManageAccessedLogForm.getUser_id() + ", 検索実行プログラムコード = " + systemManageAccessedLogForm.getExecution_code() + ", 検索処理結果コード = " + systemManageAccessedLogForm.getResult_code() + ", csvファイル名 = " + reportName);
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", user_id, CommonConst.SYS_ACCESSEDLOGCTRL_SEARCHACCESSLOG_CSV, CommonConst.SUCCESS_CO, msg.toString(), "");

		}catch(Exception e) {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			pw.flush();
			String str = sw.toString();
			//アクセスログ書き込み
			StringBuilder msg = new StringBuilder();
			msg.append("検索期間 = ");
			msg.append(start_date);
			msg.append(" ～ ");
			msg.append(end_date);
			msg.append("\n検索テナントID = ");
			msg.append(systemManageAccessedLogForm.getTenant_id());
			msg.append("\n検索ユーザID = ");
			msg.append(systemManageAccessedLogForm.getUser_id());
			msg.append("\n検索実行プログラムコード = ");
			msg.append(systemManageAccessedLogForm.getExecution_code());
			msg.append("\n検索処理結果コード = ");
			msg.append(systemManageAccessedLogForm.getResult_code());
			msg.append("\ncsvファイル名 = ");
			msg.append(reportName);
			log.info("@@SystemManageAccessedLogCtrl.searchAccesslogCsv csvダウンロード失敗：" + "検索期間 = " + start_date + " ～ " + end_date + ", 検索テナントID = " + systemManageAccessedLogForm.getTenant_id() + ", 検索ユーザID = " + systemManageAccessedLogForm.getUser_id() + ", 検索実行プログラムコード = " + systemManageAccessedLogForm.getExecution_code() + ", 検索処理結果コード = " + systemManageAccessedLogForm.getResult_code() + ", csvファイル名 = " + reportName + "\n" + str);
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", user_id, CommonConst.SYS_ACCESSEDLOGCTRL_SEARCHACCESSLOG_CSV, CommonConst.FAILED_CO, msg.toString(), str);
		}
	}

	/**
	 * アクセスログ検索（アクセス番号から検索）<br>
	 * @param accessNo アクセス番号
	 * @return アクセスログ Modelオブジェクト
	 * @throws Exception
	 */
	@RequestMapping(params="accessNoSelect", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String accessNoSelect(@RequestParam String acc_userID, String accessNo, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_ACCESSEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", CommonConst.SYSTEM_MANAGER, CommonConst.SYS_ACCESSEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) != CommonConst.SYSTEM_MANAGER) {
			log.info("@@SystemManageAccessedLogCtrl.accessNoSelect アクセス権限違反[USER_ID=" + SessionConst.USER_ID + "]");
			throw new ExException(MessageConst.W015);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", tripleDesConverter.decrypt64(acc_userID), CommonConst.SYS_ACCESSEDLOGCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		String json = null;

		AccessLogModel accessLog = accessLogDao.accessNoSelect(accessNo);

		if (accessLog != null) {
			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(accessLog);
		}

		return json;
	}

}