/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込み変更。TripleDES変換クラス宣言(インスタンス化)変更。
 *    3    2019/10/23      1.0.2           女屋            不要メンバ変数削除と修正
 *    4    2019/10/25      1.0.3           女屋            子ライブラリ取得処理修正
 *    5    2020/02/20      1.0.4           女屋            帳票有効化機能追加
 */
package jp.reneil.reiss.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectMapper;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.AdditionalInfoDao;
import jp.reneil.reiss.dao.ContentsDao;
import jp.reneil.reiss.dao.LibraryDao;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.TenantDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.exception.ExException;
import jp.reneil.reiss.form.MngLibraryForm;
import jp.reneil.reiss.log.LogWriting;
import jp.reneil.reiss.model.ContentsModel;
import jp.reneil.reiss.model.Directory;
import jp.reneil.reiss.model.LibraryModel;
import jp.reneil.reiss.model.Node;
import jp.reneil.reiss.model.SystemInfoModel;
import jp.reneil.reiss.model.TenantModel;
import jp.reneil.reiss.model.UserInfoModel;
import jp.reneil.reiss.util.FileUtil;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザ管理_ライブラリ_画面コントローラクラスです。<br>
 *
 * @version 1.0.4
 */
@Controller
@RequestMapping(value="mng_library")
public class MngLibraryCtrl {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(MngLibraryCtrl.class);

	/** ログ書き込みクラス */
	@Autowired
	private LogWriting logWriting;

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** ユーザー情報テーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/** テナント情報テーブルアクセス制御クラス */
	@Autowired
	private TenantDao tenantDao;

	/** ライブラリ情報テーブルアクセス制御クラス */
	@Autowired
	private LibraryDao librarydao;

	/** コンテンツテーブルアクセス制御クラス */
	@Autowired
	private ContentsDao contentsDao;

	/** 付帯情報テーブルアクセス制御クラス */
	@Autowired
	private AdditionalInfoDao additionalInfoDao;

	/** 画面表示
	 * @throws Exception */
	@RequestMapping(method=GET)
	public String init(@ModelAttribute("mngLibraryForm") MngLibraryForm mngLibraryForm, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MNG_LIBRARYCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MNG_LIBRARYCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}

		// セッション情報からログインユーザIDを取得
		String user_id = (String) session.getAttribute(SessionConst.USER_ID);
		log.info("@@MngLibraryCtrl.init セッション情報取得[USER_ID=" + user_id + "]");
		//セッション情報からログインユーザーのテナントIDを取得
		String strTenantid = (String)session.getAttribute(SessionConst.TENANT_ID);
		log.info("@@MngLibraryCtrl.init セッション情報取得[TENANT_ID=" + strTenantid + "]");

		// システム情報を取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();
		// システムルートパスを取得
		String strSystemRoot = systeminfoModel.getSystem_rootpath();

		// ユーザーのルートパス取得
		// システムルート＋サムネイルルート＋テナントID
		StringBuilder sb = new StringBuilder(strSystemRoot);
		sb.append(CommonConst.YEN_MRK);
		sb.append(CommonConst.THUMBNAIL);
		sb.append(CommonConst.YEN_MRK);
		sb.append(strTenantid);

		// ライブラリツリーデータ
		model.addAttribute("libraryTreeData", getLibraryTreeData(sb.toString(), strTenantid));

		LibraryModel libraryModel = new LibraryModel();

		//ライブラリリスト
		List<LibraryModel> LibraryAll = new ArrayList<LibraryModel>();
		List<LibraryModel> ParentLidAll = new ArrayList<LibraryModel>();

		//最上位は空データ
		libraryModel.setLib_name_text("無し（最上位）");
		//最初に最上位のデータをセット
		ParentLidAll.add(libraryModel);
		LibraryAll.addAll(librarydao.tenantLib(strTenantid));
		ParentLidAll.addAll(LibraryAll);

		// テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);

		// ライブ撮影有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_live_function())) {
			model.addAttribute("liveFunction", tenantModel.getStr_live_function());
		}
		// 帳票有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
			model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
		}

		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());
		// 暗号化ユーザID
		model.addAttribute("acc_userID", tripleDesConverter.encrypt64(user_id));
		//親ライブラリフォーム
		model.addAttribute("parentLid",ParentLidAll);
		//ライブラリ一覧
		model.addAttribute("library",LibraryAll);

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_LIBRARYCTRL_INIT, CommonConst.SUCCESS_CO, "", "");

		return "mng_library";
	}

	/**
	 * 追加ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="add", method=POST)
	public String add(@ModelAttribute("mngLibraryForm") MngLibraryForm mngLibraryForm, BindingResult result, Model model, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()), CommonConst.MNG_LIBRARYCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()), CommonConst.MNG_LIBRARYCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()), CommonConst.MNG_LIBRARYCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		// システム情報を取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();
		// システムルートパスを取得
		String strSystemRoot = systeminfoModel.getSystem_rootpath();

		// フォームからモデルに転記
		LibraryModel libraryModel = setLibraryModel(mngLibraryForm, strTenantid);
		libraryModel.setTenant_id(strTenantid);

		// フォームのライブラリIDが既にテーブルにあるか確認
		if (librarydao.count(strTenantid,mngLibraryForm.getLib_id(),"chk") == 0 ) {
			// 存在しないとき（追加）

			Path libFolder = Paths.get(strSystemRoot + CommonConst.YEN_MRK + CommonConst.LIBRARY + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK + libraryModel.getLib_path());
			Path thuFolder = Paths.get(strSystemRoot + CommonConst.YEN_MRK + CommonConst.THUMBNAIL + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK + libraryModel.getLib_path());

			//フォルダ存在確認
			if (!Files.exists(libFolder) && !Files.exists(thuFolder)) {
				//ライブラリフォルダ作成
				//失敗したらリトライ
				for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
					try {
						Files.createDirectories(libFolder);
						if (Files.exists(libFolder)) {
							//ライブラリフォルダ作成に成功したらループから抜ける
							break;
						}else {
							//ライブラリフォルダ作成失敗
							log.info("@@MngLibraryCtrl.add " + libFolder.toAbsolutePath().toString() + " ライブラリフォルダ作成失敗回数 = " + (i + 1));
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}
					}catch (Exception e) {
						// ライブラリフォルダ作成失敗(例外)
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw);
						e.printStackTrace(pw);
						pw.flush();
						String str = sw.toString();
						log.info("@@MngLibraryCtrl.add " + libFolder.toAbsolutePath().toString() + " ライブラリフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
						try
						{
							Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
						}
						catch(InterruptedException ex){}
					}
				}
				//サムネイルフォルダ作成
				//失敗したらリトライ
				for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
					try {
						Files.createDirectories(thuFolder);
						if (Files.exists(thuFolder)) {
							//サムネイルフォルダ作成に成功したらループから抜ける
							break;
						}else {
							//サムネイルフォルダ作成失敗
							log.info("@@MngLibraryCtrl.add " + thuFolder.toAbsolutePath().toString() + " サムネイルフォルダ作成失敗回数 = " + (i + 1));
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}
					}catch (Exception e) {
						// サムネイルフォルダ作成失敗(例外)
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw);
						e.printStackTrace(pw);
						pw.flush();
						String str = sw.toString();
						log.info("@@MngLibraryCtrl.add " + thuFolder.toAbsolutePath().toString() + " サムネイルフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
						try
						{
							Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
						}
						catch(InterruptedException ex){}
					}
				}
				//DBに追加
				librarydao.insert(libraryModel);

				log.info("@@MngLibraryCtrl.add ライブラリ情報テーブルを追加しました(テナントID = " + strTenantid + ", ライブラリID = " + libraryModel.getLib_id() + ", 親ライブラリID = " + libraryModel.getParent_lid() + ", ライブラリ名 = " + libraryModel.getLib_name() + ", ライブラリルーム名 = " + libraryModel.getLive_room() + ", 表示順 = " + libraryModel.getDisplay_no() + ")");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()), CommonConst.MNG_LIBRARYCTRL_ADD, CommonConst.SUCCESS_CO, "ライブラリID = " + libraryModel.getLib_id() + "\n親ライブラリID = " + libraryModel.getParent_lid() + "\nライブラリ名 = " + libraryModel.getLib_name() + "\nライブラリルーム名 = " + libraryModel.getLive_room() + "\n表示順 = " + libraryModel.getDisplay_no(), "");

				//successメッセージ
				model.addAttribute("success", MessageConst.M001);

			}else {
				//フォルダが既に存在する場合
				log.info("@@MngLibraryCtrl.add " + MessageConst.W811 + "(テナントID = " + strTenantid + ", ライブラリID = " + libraryModel.getLib_id() + ", 親ライブラリID = " + libraryModel.getParent_lid() + ", ライブラリ名 = " + libraryModel.getLib_name() + ", ライブラリルーム名 = " + libraryModel.getLive_room() + ", 表示順 = " + libraryModel.getDisplay_no() + ")");
				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()), CommonConst.MNG_LIBRARYCTRL_ADD, CommonConst.FAILED_CO, "ライブラリID = " + libraryModel.getLib_id() + "\n親ライブラリID = " + libraryModel.getParent_lid() + "\nライブラリ名 = " + libraryModel.getLib_name() + "\nライブラリルーム名 = " + libraryModel.getLive_room() + "\n表示順 = " + libraryModel.getDisplay_no(), MessageConst.W811);
				//warningメッセージ
				model.addAttribute("path_add_danger", MessageConst.W811);
			}

		}else {
			//ライブラリIDが既に存在する場合
			log.info("@@MngLibraryCtrl.add " + MessageConst.W810 + "(テナントID = " + strTenantid + ", ライブラリID = " + libraryModel.getLib_id() + ", 親ライブラリID = " + libraryModel.getParent_lid() + ", ライブラリ名 = " + libraryModel.getLib_name() + ", ライブラリルーム名 = " + libraryModel.getLive_room() + ", 表示順 = " + libraryModel.getDisplay_no() + ")");
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()), CommonConst.MNG_LIBRARYCTRL_ADD, CommonConst.FAILED_CO, "ライブラリID = " + libraryModel.getLib_id() + "\n親ライブラリID = " + libraryModel.getParent_lid() + "\nライブラリ名 = " + libraryModel.getLib_name() + "\nライブラリルーム名 = " + libraryModel.getLive_room() + "\n表示順 = " + libraryModel.getDisplay_no(), MessageConst.W810);
			//warningメッセージ
			model.addAttribute("id_add_danger", MessageConst.W810);
		}

		//ユーザのルートパス
		String strUserRootpath = "";

		//ライブラリリスト
		List<LibraryModel> LibraryAll = new ArrayList<LibraryModel>();
		List<LibraryModel> ParentLidAll = new ArrayList<LibraryModel>();

		//最上位は空データ
		libraryModel = new LibraryModel();
		libraryModel.setLib_name_text("無し（最上位）");

		//最初に最上位のデータをセット
		ParentLidAll.add(libraryModel);

		LibraryAll.addAll(librarydao.tenantLib(strTenantid));
		ParentLidAll.addAll(LibraryAll);

		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());

		// テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);

		// ライブ撮影有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_live_function())) {
			model.addAttribute("liveFunction", tenantModel.getStr_live_function());
		}
		// 帳票有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
			model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
		}

		// ユーザーのルートパス取得
		// システムルート＋サムネイルルート＋テナントID
		StringBuilder sb = new StringBuilder(strSystemRoot);
		sb.append(CommonConst.YEN_MRK);
		sb.append(CommonConst.THUMBNAIL);
		sb.append(CommonConst.YEN_MRK);
		sb.append(strTenantid);
		// ライブラリツリーデータ
		model.addAttribute("libraryTreeData", getLibraryTreeData(sb.toString(), strTenantid));
		//親ライブラリフォーム
		model.addAttribute("parentLid",ParentLidAll);
		//ライブラリ一覧
		model.addAttribute("library",LibraryAll);
		//ユーザパス文字数
		model.addAttribute("userPathLength",strUserRootpath.length());

		return "mng_library";
	}

	/**
	 * 更新ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="update", method=POST)
	public String update(@ModelAttribute("mngLibraryForm") MngLibraryForm mngLibraryForm, BindingResult result, Model model, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()), CommonConst.MNG_LIBRARYCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()), CommonConst.MNG_LIBRARYCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()), CommonConst.MNG_LIBRARYCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		// システム情報を取得
		SystemInfoModel systeminfoModel = systemInfoDao.findAll();
		// システムルートパスを取得
		String strSystemRoot = systeminfoModel.getSystem_rootpath();

		// フォームからモデルに転記
		LibraryModel libraryModel = setLibraryModel(mngLibraryForm, strTenantid);
		libraryModel.setTenant_id(strTenantid);

		// フォームのライブラリIDが既にテーブルにあるか確認
		if (librarydao.count(strTenantid,mngLibraryForm.getLib_id(),"chk") == 0 ) {
			// ライブラリIDが存在しない場合
			Path libFolder = Paths.get(strSystemRoot + CommonConst.YEN_MRK + CommonConst.LIBRARY + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK + libraryModel.getLib_path());
			Path thuFolder = Paths.get(strSystemRoot + CommonConst.YEN_MRK + CommonConst.THUMBNAIL + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK + libraryModel.getLib_path());

			// フォルダ存在確認
			if (!Files.exists(libFolder) && !Files.exists(thuFolder)) {
				// ライブラリフォルダ作成
				// 失敗したらリトライ
				for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
					try {
						Files.createDirectories(libFolder);
						if (Files.exists(libFolder)) {
							// ライブラリフォルダ作成に成功したらループから抜ける
							break;
						}else {
							// ライブラリフォルダ作成失敗
							log.info("@@MngLibraryCtrl.update " + libFolder.toAbsolutePath().toString() + " ライブラリフォルダ作成失敗回数 = " + (i + 1));
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}
					}catch (Exception e) {
						// ライブラリフォルダ作成失敗(例外)
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw);
						e.printStackTrace(pw);
						pw.flush();
						String str = sw.toString();
						log.info("@@MngLibraryCtrl.update " + libFolder.toAbsolutePath().toString() + " ライブラリフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
						try
						{
							Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
						}
						catch(InterruptedException ex){}
					}
				}
				// サムネイルフォルダ作成
				// 失敗したらリトライ
				for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
					try {
						Files.createDirectories(thuFolder);
						if (Files.exists(thuFolder)) {
							// サムネイルフォルダ作成に成功したらループから抜ける
							break;
						}else {
							// サムネイルフォルダ作成失敗
							log.info("@@MngLibraryCtrl.update " + thuFolder.toAbsolutePath().toString() + " サムネイルフォルダ作成失敗回数 = " + (i + 1));
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}
					}catch (Exception e) {
						// サムネイルフォルダ作成失敗(例外)
						StringWriter sw = new StringWriter();
						PrintWriter pw = new PrintWriter(sw);
						e.printStackTrace(pw);
						pw.flush();
						String str = sw.toString();
						log.info("@@MngLibraryCtrl.update " + thuFolder.toAbsolutePath().toString() + " サムネイルフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
						try
						{
							Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
						}
						catch(InterruptedException ex){}
					}
				}
				// DBに追加
				librarydao.insert(libraryModel);

				log.info("@@MngLibraryCtrl.update ライブラリ情報テーブルを追加しました(テナントID = " + strTenantid + ", ライブラリID = " + libraryModel.getLib_id() + ", 親ライブラリID = " + libraryModel.getParent_lid() + ", ライブラリ名 = " + libraryModel.getLib_name() + ", ライブラリルーム名 = " + libraryModel.getLive_room() + ", 表示順 = " + libraryModel.getDisplay_no() + ")");

				// アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()), CommonConst.MNG_LIBRARYCTRL_ADD, CommonConst.SUCCESS_CO, "ライブラリID = " + libraryModel.getLib_id() + "\n親ライブラリID = " + libraryModel.getParent_lid() + "\nライブラリ名 = " + libraryModel.getLib_name() + "\nライブラリルーム名 = " + libraryModel.getLive_room() + "\n表示順 = " + libraryModel.getDisplay_no(), "");

			}

			// successメッセージ
			model.addAttribute("success", MessageConst.M001);

		}else {
			// ライブラリIDが存在する場合
			// 更新前ライブラリ情報を保持
			LibraryModel oldLibraryModel = librarydao.findLib(strTenantid,libraryModel.getLib_id());

			// 物理フォルダ名更新
			// 取得時フォルダ名と更新時ライブラリ名が違う、または取得時親フォルダidと更新時親ライブラリidが違う場合
			if(!(oldLibraryModel.getLib_name().equals(libraryModel.getLib_name())) || !(oldLibraryModel.getParent_lid().equals(libraryModel.getParent_lid()))) {
				Path oldLibFolder = Paths.get(strSystemRoot + CommonConst.YEN_MRK + CommonConst.LIBRARY + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK + oldLibraryModel.getLib_path());
				Path oldThuFolder = Paths.get(strSystemRoot + CommonConst.YEN_MRK + CommonConst.THUMBNAIL + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK + oldLibraryModel.getLib_path());
				Path libFolder = Paths.get(strSystemRoot + CommonConst.YEN_MRK + CommonConst.LIBRARY + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK + libraryModel.getLib_path());
				Path thuFolder = Paths.get(strSystemRoot + CommonConst.YEN_MRK + CommonConst.THUMBNAIL + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK + libraryModel.getLib_path());

				// フォルダ存在確認
				if (!Files.exists(libFolder) && !Files.exists(thuFolder)) {

					// 更新する子ライブラリのリストを取得
					List<LibraryModel> LibraryAll = librarydao.likePath(strTenantid, oldLibraryModel.getLib_path());

					boolean lengthChk = true;

					// 子ライブラリの確認
					for(int i=0; i< LibraryAll.size() ;i++) {
						if (LibraryAll.get(i).getLib_path().replaceFirst(Pattern.quote(oldLibraryModel.getLib_path()), libraryModel.getLib_path()).length() > 120) {
							lengthChk = false;
							break;
						}
					}

					if (lengthChk) {

						// ライブラリ更新
						librarydao.update(libraryModel);

						// 子ライブラリの更新
						librarydao.update_path(oldLibraryModel.getLib_path(), libraryModel.getLib_path(), strTenantid);

						// コンテンツファイルパスの更新
						contentsDao.update_path_like(strTenantid + CommonConst.YEN_MRK + oldLibraryModel.getLib_path(), strTenantid + CommonConst.YEN_MRK + libraryModel.getLib_path());

						// ライブラリフォルダリネーム
						// 失敗したらリトライ
						for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
							try {
								Files.move(oldLibFolder, libFolder);
								log.info("@@MngLibraryCtrl.update ライブラリフォルダリネーム 対象ファイル = " + oldLibFolder.toAbsolutePath().toString() + ", リネーム後ファイル = " + libFolder.toAbsolutePath().toString());
								// ライブラリフォルダリネームに成功したらループから抜ける
							    break;
							}catch (Exception e) {
								// ライブラリフォルダリネーム失敗
								StringWriter sw = new StringWriter();
								PrintWriter pw = new PrintWriter(sw);
								e.printStackTrace(pw);
								pw.flush();
								String str = sw.toString();
								log.info("@@MngLibraryCtrl.update ライブラリフォルダリネーム失敗(例外) 対象ファイル = " + oldLibFolder.toAbsolutePath().toString() + ", リネーム後ファイル = " + libFolder.toAbsolutePath().toString());
								log.info("@@MngLibraryCtrl.update ライブラリフォルダリネーム失敗回数(例外) = " + (i + 1) + "\n" + str);
								try
								{
									Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
								}
								catch(InterruptedException ex){}
							}
						}
						// サムネイルフォルダリネーム
						// 失敗したらリトライ
						for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
							try {
								Files.move(oldThuFolder, thuFolder);
								log.info("@@MngLibraryCtrl.update サムネイルフォルダリネーム 対象ファイル = " + oldThuFolder.toAbsolutePath().toString() + ", リネーム後ファイル = " + thuFolder.toAbsolutePath().toString());
								// ライブラリフォルダリネームに成功したらループから抜ける
							    break;
							}catch (Exception e) {
								// ライブラリフォルダリネーム失敗
								StringWriter sw = new StringWriter();
								PrintWriter pw = new PrintWriter(sw);
								e.printStackTrace(pw);
								pw.flush();
								String str = sw.toString();
								log.info("@@MngLibraryCtrl.update サムネイルフォルダリネーム失敗(例外) 対象ファイル = " + oldThuFolder.toAbsolutePath().toString() + ", リネーム後ファイル = " + thuFolder.toAbsolutePath().toString());
								log.info("@@MngLibraryCtrl.update サムネイルフォルダリネーム失敗回数(例外) = " + (i + 1) + "\n" + str);
								try
								{
									Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
								}
								catch(InterruptedException ex){}
							}
						}

						log.info("@@MngLibraryCtrl.update ライブラリ情報テーブルを更新しました(テナントID = " + strTenantid + ", ライブラリID = " + libraryModel.getLib_id() + ", 親ライブラリID = " + libraryModel.getParent_lid() + ", ライブラリ名 = " + libraryModel.getLib_name() + ", ライブラリルーム名 = " + libraryModel.getLive_room() + ", 表示順 = " + libraryModel.getDisplay_no() + ")");

						// アクセスログ書き込み
						logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()), CommonConst.MNG_LIBRARYCTRL_UPDATE, CommonConst.SUCCESS_CO, "ライブラリID = " + libraryModel.getLib_id() + "\n親ライブラリID = " + libraryModel.getParent_lid() + "\nライブラリ名 = " + libraryModel.getLib_name() + "\nライブラリルーム名 = " + libraryModel.getLive_room() + "\n表示順 = " + libraryModel.getDisplay_no(), "");

						// successメッセージ
						model.addAttribute("success", MessageConst.M001);

					}else {
						// 子ライブラリのパスが規定値(120文字)を超えている場合
						log.info("@@MngLibraryCtrl.update " + MessageConst.W812 + "(テナントID = " + strTenantid + ", ライブラリID = " + libraryModel.getLib_id() + ", 親ライブラリID = " + libraryModel.getParent_lid() + ", ライブラリ名 = " + libraryModel.getLib_name() + ", ライブラリルーム名 = " + libraryModel.getLive_room() + ", 表示順 = " + libraryModel.getDisplay_no() + ")");
						// アクセスログ書き込み
						logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()), CommonConst.MNG_LIBRARYCTRL_UPDATE, CommonConst.FAILED_CO, "ライブラリID = " + libraryModel.getLib_id() + "\n親ライブラリID = " + libraryModel.getParent_lid() + "\nライブラリ名 = " + libraryModel.getLib_name() + "\nライブラリルーム名 = " + libraryModel.getLive_room() + "\n表示順 = " + libraryModel.getDisplay_no(), MessageConst.W812);
						//warningメッセージ
						model.addAttribute("up_danger", MessageConst.W812);
					}

				}else {
					// フォルダが既に存在する場合
					log.info("@@MngLibraryCtrl.update " + MessageConst.W811 + "(テナントID = " + strTenantid + ", ライブラリID = " + libraryModel.getLib_id() + ", 親ライブラリID = " + libraryModel.getParent_lid() + ", ライブラリ名 = " + libraryModel.getLib_name() + ", ライブラリルーム名 = " + libraryModel.getLive_room() + ", 表示順 = " + libraryModel.getDisplay_no() + ")");
					// アクセスログ書き込み
					logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()), CommonConst.MNG_LIBRARYCTRL_UPDATE, CommonConst.FAILED_CO, "ライブラリID = " + libraryModel.getLib_id() + "\n親ライブラリID = " + libraryModel.getParent_lid() + "\nライブラリ名 = " + libraryModel.getLib_name() + "\nライブラリルーム名 = " + libraryModel.getLive_room() + "\n表示順 = " + libraryModel.getDisplay_no(), MessageConst.W811);
					// warningメッセージ
					model.addAttribute("up_danger", MessageConst.W811);
				}

			}else {
				Path libFolder = Paths.get(strSystemRoot + CommonConst.YEN_MRK + CommonConst.LIBRARY + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK + libraryModel.getLib_path());
				Path thuFolder = Paths.get(strSystemRoot + CommonConst.YEN_MRK + CommonConst.THUMBNAIL + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK + libraryModel.getLib_path());

				//フォルダ存在確認
				if (!Files.exists(libFolder) && !Files.exists(thuFolder)) {

					// ライブラリフォルダ作成
					// 失敗したらリトライ
					for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
						try {
							Files.createDirectories(libFolder);
							if (Files.exists(libFolder)) {
								// ライブラリフォルダ作成に成功したらループから抜ける
								break;
							}else {
								// ライブラリフォルダ作成失敗
								log.info("@@MngLibraryCtrl.update " + libFolder.toAbsolutePath().toString() + " ライブラリフォルダ作成失敗回数 = " + (i + 1));
								try
								{
									Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
								}
								catch(InterruptedException ex){}
							}
						}catch (Exception e) {
							// ライブラリフォルダ作成失敗(例外)
							StringWriter sw = new StringWriter();
							PrintWriter pw = new PrintWriter(sw);
							e.printStackTrace(pw);
							pw.flush();
							String str = sw.toString();
							log.info("@@MngLibraryCtrl.update " + libFolder.toAbsolutePath().toString() + " ライブラリフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}
					}
					// サムネイルフォルダ作成
					// 失敗したらリトライ
					for (int i = 0 ; i < CommonConst.RETRY_COUNT ; i++) {
						try {
							Files.createDirectories(thuFolder);
							if (Files.exists(thuFolder)) {
								// サムネイルフォルダ作成に成功したらループから抜ける
								break;
							}else {
								// サムネイルフォルダ作成失敗
								log.info("@@MngLibraryCtrl.update " + thuFolder.toAbsolutePath().toString() + " サムネイルフォルダ作成失敗回数 = " + (i + 1));
								try
								{
									Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
								}
								catch(InterruptedException ex){}
							}
						}catch (Exception e) {
							// サムネイルフォルダ作成失敗(例外)
							StringWriter sw = new StringWriter();
							PrintWriter pw = new PrintWriter(sw);
							e.printStackTrace(pw);
							pw.flush();
							String str = sw.toString();
							log.info("@@MngLibraryCtrl.update " + thuFolder.toAbsolutePath().toString() + " サムネイルフォルダ作成失敗回数(例外) =" + (i + 1) + "\n" + str);
							try
							{
								Thread.sleep(i * 10); //i * 10ミリ秒Sleepする
							}
							catch(InterruptedException ex){}
						}
					}

					log.info("@@MngLibraryCtrl.update 物理フォルダを追加しました(テナントID = " + strTenantid + ", ライブラリID = " + libraryModel.getLib_id() + ", 親ライブラリID = " + libraryModel.getParent_lid() + ", ライブラリ名 = " + libraryModel.getLib_name() + ", ライブラリルーム名 = " + libraryModel.getLive_room() + ", 表示順 = " + libraryModel.getDisplay_no() + ")");
				}

				// ライブラリ更新
				librarydao.update(libraryModel);

				// アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngLibraryForm.getAcc_userID()), CommonConst.MNG_LIBRARYCTRL_UPDATE, CommonConst.SUCCESS_CO, "ライブラリID = " + libraryModel.getLib_id() + "\n親ライブラリID = " + libraryModel.getParent_lid() + "\nライブラリ名 = " + libraryModel.getLib_name() + "\nライブラリルーム名 = " + libraryModel.getLive_room() + "\n表示順 = " + libraryModel.getDisplay_no(), "");

				// successメッセージ
				model.addAttribute("success", MessageConst.M001);
			}

		}

		// ユーザのルートパス
		String strUserRootpath = "";

		// ライブラリリスト
		List<LibraryModel> LibraryAll = new ArrayList<LibraryModel>();
		List<LibraryModel> ParentLidAll = new ArrayList<LibraryModel>();

		// 最上位は空データ
		libraryModel = new LibraryModel();
		libraryModel.setLib_name_text("無し（最上位）");

		// 最初に最上位のデータをセット
		ParentLidAll.add(libraryModel);

		LibraryAll.addAll(librarydao.tenantLib(strTenantid));
		ParentLidAll.addAll(LibraryAll);

		// システム名
		model.addAttribute("systemName", systeminfoModel.getSystem_name());

		// テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);

		// ライブ撮影有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_live_function())) {
			model.addAttribute("liveFunction", tenantModel.getStr_live_function());
		}
		// 帳票有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
			model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
		}

		// ユーザーのルートパス取得
		// システムルート＋サムネイルルート＋テナントID
		StringBuilder sb = new StringBuilder(strSystemRoot);
		sb.append(CommonConst.YEN_MRK);
		sb.append(CommonConst.THUMBNAIL);
		sb.append(CommonConst.YEN_MRK);
		sb.append(strTenantid);
		// ライブラリツリーデータ
		model.addAttribute("libraryTreeData", getLibraryTreeData(sb.toString(), strTenantid));
		// 親ライブラリフォーム
		model.addAttribute("parentLid",ParentLidAll);
		// ライブラリ一覧
		model.addAttribute("library",LibraryAll);
		// ユーザパス文字数
		model.addAttribute("userPathLength",strUserRootpath.length());

		return "mng_library";
	}

	/**
	 * フォームからモデルに入力情報を展開します。
	 * @param form 管理_ライブラリ_画面フォーム
	 * @return テナントモデル
	 * @throws Exception
	 */
	private LibraryModel setLibraryModel(MngLibraryForm form, String strTenantid) throws Exception {
		LibraryModel model = new LibraryModel();
		LibraryModel parentModel = new LibraryModel();

		//ライブラリID
		model.setLib_id(form.getLib_id());
		//親ライブラリID
		model.setParent_lid(form.getParent_lid());
		//ライブラリ名
		model.setLib_name(form.getLib_name());
		//ライブラリパス
		if (!StringUtils.isEmpty(form.getParent_lid())) {
			parentModel =librarydao.findLib(strTenantid, form.getParent_lid());
			model.setLib_path(parentModel.getLib_path() + CommonConst.YEN_MRK + form.getLib_name());
		}else {
			model.setLib_path(form.getLib_name());
		}
		//ライブルーム名
		model.setLive_room(form.getLive_room());
		//表示順
		if (!StringUtils.isEmpty(form.getDisplay_no())) {
			model.setDisplay_no(Integer.parseInt(form.getDisplay_no()));
		}
		// 更新ユーザーID
		model.setUpdate_user_id(tripleDesConverter.decrypt64(form.getAcc_userID()));

		return model;
	}

	/**
	 * ライブラリ削除<br>
	 * @param libId ディレクトリID
	 * @param request リクエスト情報
	 * @return 実行結果(true/false)
	 * @throws Exception
	 */
	@RequestMapping(params="delete", method=POST, produces = "text/html;charset=UTF-8")
	@ResponseBody
	public String delete(@RequestParam String acc_userID, String libId, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(acc_userID));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		// 削除失敗カウンタ
		int cnt = 0;

		// 削除の成否
		String fileDelete = "true";

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_LIBRARYCTRL_DELETE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_LIBRARYCTRL_DELETE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(acc_userID))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID), CommonConst.MNG_LIBRARYCTRL_DELETE, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		// システム情報からシステムルートパスを取得
		String strSystemRoot = systemInfoDao.findAll().getSystem_rootpath();

		LibraryModel libraryModel = librarydao.findLib(strTenantid, libId);

		Path FILIB = Paths.get(strSystemRoot + CommonConst.YEN_MRK + CommonConst.LIBRARY + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK + libraryModel.getLib_path());
		Path FITHU = Paths.get(strSystemRoot + CommonConst.YEN_MRK + CommonConst.THUMBNAIL + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK + libraryModel.getLib_path());

		for (int i = 0 ; i < 200 ; i++) {
			if (!MainCtrl.fileLockTenant.contains(strTenantid)) {
				// テナント内で削除が行われていなければ処理実行
				MainCtrl.fileLockTenant.add(strTenantid);
				break;
			}else {
				// テナント内で削除が行われている
				log.info("@@MngLibraryCtrl.delete 削除実行待ち = " + (i + 1) + "回");
				if (i > 149) {
					// ファイルロック中テナント確認が150回を超えたら強制解除
					MainCtrl.fileLockTenant.remove(MainCtrl.fileLockTenant.indexOf(strTenantid));
					log.info("@@MngLibraryCtrl.delete ファイルロック中テナント強制破棄");
				}
				try
				{
					Thread.sleep(i * 5); //i * 5ミリ秒Sleepする
				}
				catch(InterruptedException ex){}
			}
		}

		if (!recursiveDeleteFile(FILIB, strSystemRoot, strTenantid)) {
			// ライブラリファイル削除失敗
			cnt++;
		}

		if (!recursiveDeleteFile(FITHU, strSystemRoot, strTenantid)) {
			// サムネイルファイル削除失敗
			cnt++;
		}

		if (cnt > 0) {
			fileDelete = "false";
		}

		if (fileDelete.equals("true")) {
			log.info("@@MngLibraryCtrl.delete ライブラリ情報を削除しました(テナントID = " + strTenantid + ", ライブラリID = " + libraryModel.getLib_id() + ", 親ライブラリID = " + libraryModel.getParent_lid() + ", ライブラリ名 = " + libraryModel.getLib_name() + ", ライブラリルーム名 = " + libraryModel.getLive_room() + ", 表示順 = " + libraryModel.getDisplay_no() + ", 物理ライブラリ = " + libraryModel.getLib_path() + ")");
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID).toString(), CommonConst.MNG_LIBRARYCTRL_DELETE, CommonConst.SUCCESS_CO, "対象ライブラリ = " + libraryModel.getLib_path(), "");
		}else {
			log.info("@@MngLibraryCtrl.delete ライブラリ情報削除に失敗しました。いくつかのファイルが削除されていません。(テナントID = " + strTenantid + ", ライブラリID = " + libraryModel.getLib_id() + ", 親ライブラリID = " + libraryModel.getParent_lid() + ", ライブラリ名 = " + libraryModel.getLib_name() + ", ライブラリルーム名 = " + libraryModel.getLive_room() + ", 表示順 = " + libraryModel.getDisplay_no() + ", 物理ライブラリ = " + libraryModel.getLib_path() + ", 削除失敗回数 = " + cnt + ")");
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(acc_userID).toString(), CommonConst.MNG_LIBRARYCTRL_DELETE, CommonConst.FAILED_CO, "対象ライブラリ = " + libraryModel.getLib_path(), "いくつかのファイルが削除されていません。\n削除失敗回数 = " + cnt);
		}

		// ファイルロック中テナント解除
		MainCtrl.fileLockTenant.remove(MainCtrl.fileLockTenant.indexOf(strTenantid));

		return fileDelete;
	}

	/**
	 * 対象のファイルオブジェクトの削除を行う.<BR>
	 * ディレクトリの場合は再帰処理を行い、削除する。
	 * @param file ファイルオブジェクト
	 * @throws Exception
	 */
	private boolean recursiveDeleteFile(final Path file, String strSystemRoot, String strTenantid) throws Exception {

		// 削除失敗カウンタ
		int cnt = 0;

		boolean fileDelete = true;

	    // 存在しない場合は処理終了
	    if (Files.notExists(file)) {
	    	fileDelete = true;
	        return fileDelete;
	    }
	    // 対象がディレクトリの場合は再帰処理
	    if (Files.isDirectory(file)) {
			try(DirectoryStream<Path> dirStream = Files.newDirectoryStream(file)) {	// streamのclose用try-with-resources構文
		        for (Path child : dirStream) {
		        	if (!recursiveDeleteFile(child, strSystemRoot, strTenantid)) {
		        		// 削除失敗
						cnt++;
		        	}
		        }
			}catch (Exception e) {
				// ディレクトリストリーム取得失敗(例外)
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				pw.flush();
				String str = sw.toString();
				log.info("@@MngLibraryCtrl.recursiveDeleteFile ディレクトリストリーム取得失敗ファイル(例外) = " + file.toAbsolutePath().toString() + "\n" + str);
			}
	    }
	    // 対象がファイルもしくは配下が空のディレクトリの場合は削除する
	    // 失敗したらリトライ
		for (int x = 0 ; x < CommonConst.RETRY_COUNT ; x++) {
			try {
				Files.deleteIfExists(file);
				if (Files.notExists(file)) {
					// 削除に成功したらループから抜ける
					log.info("@@MngLibraryCtrl.recursiveDeleteFile ファイル削除 対象ファイル = " + file.toAbsolutePath().toString());
					break;
				}else {
					// 削除失敗
					log.info("@@MngLibraryCtrl.recursiveDeleteFile 削除失敗ファイル = " + file.toAbsolutePath().toString() + " 削除失敗回数 =" + (x + 1));
					try
					{
						Thread.sleep(x * 10); //x * 10ミリ秒Sleepする
					}
					catch(InterruptedException ex){}
				}
			}catch (Exception e) {
				// 削除失敗(例外)
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				pw.flush();
				String str = sw.toString();
				log.info("@@MngLibraryCtrl.recursiveDeleteFile 削除失敗ファイル = " + file.toAbsolutePath().toString() + " 削除失敗回数(例外) =" + (x + 1) + "\n" + str);
				try
				{
					Thread.sleep(x * 10); //x * 10ミリ秒Sleepする
				}
				catch(InterruptedException ex){}
			}

			if (x == CommonConst.RETRY_COUNT -1) {
				// 最後まで削除できなかった場合
				cnt++;
			}
		}

		if (cnt > 0) {
			fileDelete = false;
		}else {
			// フォルダ・ファイル削除に成功したらDBデータ削除
			if (contentsDao.findContents_filename(file.getFileName().toString()) == 1) {
				ContentsModel contentsModel = contentsDao.getContentsData_filename(file.getFileName().toString());
				// CONTENTSテーブルからデータを削除
				contentsDao.deleteContents_regno(contentsModel.getReg_no());
				// ADDITIONALINFOテーブルからデータを削除
				additionalInfoDao.delete_regno(contentsModel.getReg_no());
				log.info("@@MngLibraryCtrl.recursiveDeleteFile DB削除ファイル登録番号 = " + contentsModel.getReg_no());
			}else if (librarydao.libCount(strTenantid, file.toAbsolutePath().toString().replace(strSystemRoot + CommonConst.YEN_MRK + CommonConst.LIBRARY + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK, "")) == 1){
				librarydao.delete(strTenantid, file.toAbsolutePath().toString().replace(strSystemRoot + CommonConst.YEN_MRK + CommonConst.LIBRARY + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK, ""));
				log.info("@@MngLibraryCtrl.recursiveDeleteFile DB削除ライブラリ = " + file.toAbsolutePath().toString());
			}else if (librarydao.libCount(strTenantid, file.toAbsolutePath().toString().replace(strSystemRoot + CommonConst.YEN_MRK + CommonConst.THUMBNAIL + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK, "")) == 1){
				librarydao.delete(strTenantid, file.toAbsolutePath().toString().replace(strSystemRoot + CommonConst.YEN_MRK + CommonConst.THUMBNAIL + CommonConst.YEN_MRK + strTenantid + CommonConst.YEN_MRK, ""));
				log.info("@@MngLibraryCtrl.recursiveDeleteFile DB削除ライブラリ = " + file.toAbsolutePath().toString());
			}
		}

		return fileDelete;
	}

	/**
	 * ライブラリツリーデータ取得 <br>
	 * @param path ルートパス
	 * @param strTenantid テナントID
	 * @return ライブラリツリーデータ
	 * @throws Exception
	 */
	private String getLibraryTreeData(String path, String strTenantid) throws Exception {

		String json = "error";
		Path userRootDir = Paths.get(path);

		// ユーザのルートディレクトリ存在確認
		if (Files.exists(userRootDir)) {
			// 表示順ライブラリリスト取得
			List<LibraryModel> libNoList = librarydao.libDisplayNo(strTenantid);
			// ライブラリツリーデータ作成
			Node root = getNode(userRootDir, path, strTenantid, libNoList);

			// 要素を作成
			List<Node> rootNodes = new ArrayList<Node>();
			rootNodes.add(root);

			ObjectMapper mapper = new ObjectMapper();
			json = mapper.writeValueAsString(rootNodes);
			json = json.replace(",\"nodes\":[]", ""); // 空の子要素は削除する
			json = json.substring(1, json.length() - 1);
		}

		return json;
	}

	/**
	 * ライブラリリスト取得<br>
	 * @param obj ディレクトリ
	 * @param path ルートパス
	 * @param strTenantid テナントID
	 * @param libNoList 表示順ライブラリリスト
	 * @return ライブラリリスト
	 * @throws Exception
	*/
	public Node getNode(Path obj, String path, String strTenantid, List<LibraryModel> libNoList) throws Exception {

		Node node = null;

		if (Files.isDirectory(obj)) {

			Directory dir = new Directory(obj.getFileName().toString());

			// テナントを最上位とする
			if (path.equals(obj.toAbsolutePath().toString())) {
				dir = new Directory("無し（最上位）");
				dir.setDirId("");
			}else {
				dir.setDirId(librarydao.findPath(strTenantid, obj.toAbsolutePath().toString().replace(path + CommonConst.YEN_MRK, "")).getLib_id());
				// 表示順を設定
				if (libNoList.size() > 0) {
					for(int i=0; i < libNoList.size(); i++) {
						if (libNoList.get(i).getLib_path().equals(obj.toAbsolutePath().toString().replace(path + CommonConst.YEN_MRK, ""))) {
							dir.setDisNo(libNoList.get(i).getDisplay_no());
							break;
						}
					}
				}
			}

			// 子要素を作成
			List<Node> nodes = new ArrayList<Node>();

			// ディレクトリ内の一覧を取得
			FileUtil fileUtil = new FileUtil();
			List<Path> childList = fileUtil.getFileList(obj);

			for (Path child : childList) {
				if (Files.isDirectory(child) && librarydao.libCount(strTenantid, child.toAbsolutePath().toString().replace(path + CommonConst.YEN_MRK, "")) == 1) {
					// 登録ライブラリのとき再帰呼び出し
					nodes.add(getNode(child, path, strTenantid, libNoList));
				}
			}
			dir.setNodes(nodes);

			node = dir;
		}

		return node;
	}

}