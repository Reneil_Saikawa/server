/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込み変更。TripleDES変換クラス宣言(インスタンス化)変更。
 *    3    2019/10/23      1.0.2           女屋            不要メンバ変数削除と修正
 *    4    2020/02/20      1.0.3           女屋            帳票有効化機能追加
 */
package jp.reneil.reiss.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.text.SimpleDateFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.AdditionalItemDao;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.TenantDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.exception.ExException;
import jp.reneil.reiss.form.MngAdditionalItemForm;
import jp.reneil.reiss.log.LogWriting;
import jp.reneil.reiss.model.AdditionalItemModel;
import jp.reneil.reiss.model.TenantModel;
import jp.reneil.reiss.model.UserInfoModel;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ユーザ管理_付帯情報項目名_画面コントローラクラスです。<br>
 *
 * @version 1.0.3
 */
@Controller
@RequestMapping(value="mng_additionalItem")
public class MngAdditionalItemCtrl {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(MngAdditionalItemCtrl.class);

	/** ログ書き込みクラス */
	@Autowired
	private LogWriting logWriting;

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** テナント情報テーブルアクセス制御クラス */
	@Autowired
	private TenantDao tenantDao;

	/** ユーザー情報テーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/** 付帯情報項目名テーブルアクセス制御クラス */
	@Autowired
	private AdditionalItemDao additionalItemDao;

	/** 画面表示
	 * @throws Exception */
	@RequestMapping(method=GET)
	public String init(@ModelAttribute("mngAdditionalitemForm") MngAdditionalItemForm mngAdditionalitemForm, Model model, HttpServletRequest request) throws Exception {

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MNG_ADDITIONALITEMCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.MNG_ADDITIONALITEMCTRL_INIT, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		}

		// セッション情報からログインユーザIDを取得
		String user_id = (String) session.getAttribute(SessionConst.USER_ID);
		log.info("@@MngAdditionalItemCtrl.init セッション情報取得[USER_ID=" + user_id + "]");
		// セッション情報からログインユーザーのテナントIDを取得
		String strTenantid = (String)session.getAttribute(SessionConst.TENANT_ID);
		log.info("@@MngAdditionalItemCtrl.init セッション情報取得[TENANT_ID=" + strTenantid + "]");

		// テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);

		// 帳票有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
			model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
		}

		// システム名
		model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());
		// 暗号化ユーザID
		model.addAttribute("acc_userID", tripleDesConverter.encrypt64(user_id));
		// 付帯情報項目名データ取得
		model.addAttribute("item", additionalItemDao.findItem(strTenantid));
		// テナント有効期限取得
		model.addAttribute("tenantStart", tenantModel.getStr_start_date());
		model.addAttribute("tenantEnd", tenantModel.getStr_end_date());

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, user_id, CommonConst.MNG_ADDITIONALITEMCTRL_INIT, CommonConst.SUCCESS_CO, "", "");

		return "mng_additionalItem";
	}

	/**
	 * 追加ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="add", method=POST)
	public String add(@ModelAttribute("mngAdditionalitemForm") MngAdditionalItemForm mngAdditionalitemForm, Model model, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(mngAdditionalitemForm.getAcc_userID()));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngAdditionalitemForm.getAcc_userID()), CommonConst.MNG_ADDITIONALITEMCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngAdditionalitemForm.getAcc_userID()), CommonConst.MNG_ADDITIONALITEMCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(mngAdditionalitemForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngAdditionalitemForm.getAcc_userID()), CommonConst.MNG_ADDITIONALITEMCTRL_ADD, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		//フォームからモデルに転記
		AdditionalItemModel addItemModel = setItemManageModel(mngAdditionalitemForm, strTenantid);

		//フォームの項目IDが既に存在しているか確認
		if(additionalItemDao.count(addItemModel.getTenant_id(), addItemModel.getItem_id()) == 0) {
			//存在しない時(追加)
			additionalItemDao.insert(addItemModel);
			log.info("@@MngAdditionalItemCtrl.add 付帯情報項目名テーブルを更新しました(テナントID = " + strTenantid + ", 項目ID = " + addItemModel.getItem_id() +", 項目名 = " + addItemModel.getItem_name() +", 表示順 = " + addItemModel.getDisplay_no() +", 有効期限 = " + mngAdditionalitemForm.getStart_date() + " ～ " + mngAdditionalitemForm.getEnd_date() +", 削除フラグ = " + addItemModel.getDel_flg() + ")");
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngAdditionalitemForm.getAcc_userID()), CommonConst.MNG_ADDITIONALITEMCTRL_ADD, CommonConst.SUCCESS_CO, "項目ID = " + addItemModel.getItem_id() +"\n項目名 = " + addItemModel.getItem_name() +"\n表示順 = " + addItemModel.getDisplay_no() +"\n有効期限 = " + mngAdditionalitemForm.getStart_date() + " ～ " + mngAdditionalitemForm.getEnd_date() +"\n削除フラグ = " + addItemModel.getDel_flg(), "");
			//successメッセージ
			model.addAttribute("success", MessageConst.M001);
		}else {
			//存在する時
			log.info("@@MngAdditionalItemCtrl.add " + MessageConst.W840 + "(テナントID = " + strTenantid + ", 項目ID = " + addItemModel.getItem_id() +", 項目名 = " + addItemModel.getItem_name() +", 表示順 = " + addItemModel.getDisplay_no() +", 有効期限 = " + mngAdditionalitemForm.getStart_date() + " ～ " + mngAdditionalitemForm.getEnd_date() +", 削除フラグ = " + addItemModel.getDel_flg() + ")");
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngAdditionalitemForm.getAcc_userID()), CommonConst.MNG_ADDITIONALITEMCTRL_ADD, CommonConst.FAILED_CO, "項目ID = " + addItemModel.getItem_id() +"\n項目名 = " + addItemModel.getItem_name() +"\n表示順 = " + addItemModel.getDisplay_no() +"\n有効期限 = " + mngAdditionalitemForm.getStart_date() + " ～ " + mngAdditionalitemForm.getEnd_date() +"\n削除フラグ = " + addItemModel.getDel_flg(), MessageConst.W840);
			//warningメッセージ
			model.addAttribute("add_danger", MessageConst.W840);
		}

		// システム名
		model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());

		// テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);

		// 帳票有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
			model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
		}

		//付帯情報項目名データ取得
		model.addAttribute("item", additionalItemDao.findItem(strTenantid));

		return "mng_additionalItem";
	}

	/**
	 * 更新ボタンクリック時<br>
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(params="update", method=POST)
	public String update(@ModelAttribute("mngAdditionalitemForm") MngAdditionalItemForm mngAdditionalitemForm, Model model, HttpServletRequest request) throws Exception {

		//暗号化ユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(tripleDesConverter.decrypt64(mngAdditionalitemForm.getAcc_userID()));
		//ユーザのテナントID設定
		String strTenantid = userinfoModel.getTenant_id();

		HttpSession session = request.getSession(false);

		if (session == null){
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngAdditionalitemForm.getAcc_userID()), CommonConst.MNG_ADDITIONALITEMCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (session.getAttribute(SessionConst.USER_ID) == null) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngAdditionalitemForm.getAcc_userID()), CommonConst.MNG_ADDITIONALITEMCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W002);
			throw new ExException(MessageConst.W002);
		} else if (!session.getAttribute(SessionConst.USER_ID).equals(tripleDesConverter.decrypt64(mngAdditionalitemForm.getAcc_userID()))) {
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngAdditionalitemForm.getAcc_userID()), CommonConst.MNG_ADDITIONALITEMCTRL_UPDATE, CommonConst.FAILED_CO, "", MessageConst.W004);
			throw new ExException(MessageConst.W002);
		}

		//フォームからモデルに転記
		AdditionalItemModel addItemModel = setItemManageModel(mngAdditionalitemForm, strTenantid);

		//フォームの項目IDが既に存在しているか確認
		if(additionalItemDao.count(addItemModel.getTenant_id(), addItemModel.getItem_id()) == 0) {
			//存在しない時(追加)
			additionalItemDao.insert(addItemModel);
			log.info("@@MngAdditionalItemCtrl.update 付帯情報項目名テーブルを追加しました(テナントID = " + strTenantid + ", 項目ID = " + addItemModel.getItem_id() + ", 項目名 = " + addItemModel.getItem_name() +", 表示順 = " + addItemModel.getDisplay_no() +", 有効期限 = " + mngAdditionalitemForm.getStart_date() + " ～ " + mngAdditionalitemForm.getEnd_date() +", 削除フラグ = " + addItemModel.getDel_flg() + ")");
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngAdditionalitemForm.getAcc_userID()), CommonConst.MNG_ADDITIONALITEMCTRL_ADD, CommonConst.SUCCESS_CO, "項目ID = " + addItemModel.getItem_id() +"\n項目名 = " + addItemModel.getItem_name() +"\n表示順 = " + addItemModel.getDisplay_no() +"\n有効期限 = " + mngAdditionalitemForm.getStart_date() + " ～ " + mngAdditionalitemForm.getEnd_date() +"\n削除フラグ = " + addItemModel.getDel_flg(), "");
		}else {
			//存在する時(更新)
			additionalItemDao.update(addItemModel);
			log.info("@@MngAdditionalItemCtrl.update 付帯情報項目名テーブルを更新しました(テナントID = " + strTenantid + ", 項目ID = " + addItemModel.getItem_id() +", 項目名 = " + addItemModel.getItem_name() +", 表示順 = " + addItemModel.getDisplay_no() +", 有効期限 = " + mngAdditionalitemForm.getStart_date() + " ～ " + mngAdditionalitemForm.getEnd_date() +", 削除フラグ = " + addItemModel.getDel_flg() + ")");
			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), strTenantid, tripleDesConverter.decrypt64(mngAdditionalitemForm.getAcc_userID()), CommonConst.MNG_ADDITIONALITEMCTRL_UPDATE, CommonConst.SUCCESS_CO, "項目ID = " + addItemModel.getItem_id() +"\n項目名 = " + addItemModel.getItem_name() +"\n表示順 = " + addItemModel.getDisplay_no() +"\n有効期限 = " + mngAdditionalitemForm.getStart_date() + " ～ " + mngAdditionalitemForm.getEnd_date() +"\n削除フラグ = " + addItemModel.getDel_flg(), "");
		}

		//successメッセージ
		model.addAttribute("success", MessageConst.M001);

		// システム名
		model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());

		// テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(strTenantid);

		// 帳票有効化制御
		if (!StringUtils.isEmpty(tenantModel.getStr_report_enable())) {
			model.addAttribute("reportEnable", tenantModel.getStr_report_enable());
		}

		//付帯情報項目名データ取得
		model.addAttribute("item", additionalItemDao.findItem(strTenantid));

		return "mng_additionalItem";
	}

	/**
	 * フォームからモデルに入力情報を展開します。
	 * @param form システム管理_付帯情報項目名_画面フォーム
	 * @return 付帯情報項目名モデル
	 * @throws Exception
	 */
	private AdditionalItemModel setItemManageModel(MngAdditionalItemForm form, String strTenantid) throws Exception{
		AdditionalItemModel model = new AdditionalItemModel();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyy/MM/dd");

		//テナントID
		model.setTenant_id(strTenantid);
		//項目ID
		model.setItem_id(form.getItem_id());
		//項目名
		model.setItem_name(form.getItem_name());
		//表示順
		if (!StringUtils.isEmpty(form.getDisplay_no())) {
			model.setDisplay_no(Integer.parseInt(form.getDisplay_no()));
		}
		// 有効開始日
		model.setStart_date(fmt.parse(form.getStart_date()));
		// 有効終了日
		model.setEnd_date(fmt.parse(form.getEnd_date()));
		// 削除フラグ
		if (form.getDel_flg()) {
			model.setDel_flg((byte) 1);
		} else {
			model.setDel_flg((byte) 0);
		}

		// 更新ユーザーID
		model.setUpdate_user_id(tripleDesConverter.decrypt64(form.getAcc_userID()));

		return model;
	}

}