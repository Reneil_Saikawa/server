/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込み変更。TripleDES変換クラス宣言(インスタンス化)変更。
 */
package jp.reneil.reiss.controller;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.common.MessageConst;
import jp.reneil.reiss.common.SessionConst;
import jp.reneil.reiss.dao.SystemInfoDao;
import jp.reneil.reiss.dao.TenantDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.form.LoginForm;
import jp.reneil.reiss.log.LogWriting;
import jp.reneil.reiss.model.TenantModel;
import jp.reneil.reiss.model.UserInfoModel;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ログイン_画面コントローラクラスです。<br>
 *
 * @version 1.0.1
 */
@Controller
@RequestMapping("login")
public class LoginCtrl {

	/** ログ制御 */
	private static Log log = LogFactory.getLog(LoginCtrl.class);

	/** ログ書き込みクラス */
	@Autowired
	private LogWriting logWriting;

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** システム情報テーブルアクセス制御クラス */
	@Autowired
	private SystemInfoDao systemInfoDao;

	/** ユーザー情報テーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/** テナントテーブルアクセス制御クラス */
	@Autowired
	private TenantDao tenantDao;

	/**
	 * 画面表示<br>
	 * @param loginForm ログイン画面フォーム
	 * @param request リクエスト情報
	 * @return ログイン画面
	 * @throws Exception
	 */
	@RequestMapping(method = GET)
	public String init(@ModelAttribute("loginForm") LoginForm loginForm, Model model, HttpServletRequest request) throws Exception {
		// セッション情報をクリアする
		HttpSession session = request.getSession(false);
		if (session != null){
			//セッションを破棄する
			session.invalidate();
		}

		// システム名
		model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());

		// IPアドレス取得
		log.info("@@LoginCtrl.init アクセスIP = " + request.getRemoteAddr());
		log.info("@@LoginCtrl.init アクセスPC = " + request.getRemoteHost());

		log.info("@@LoginCtrl.init ログイン画面表示");

		// アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), "null", "null", CommonConst.LOGINCTRL_INIT, CommonConst.SUCCESS_CO, "ログイン画面表示", "");

		return "login";
	}

	/**
	 * ログイン処理<br>
	 * @param loginForm ログイン画面フォーム
	 * @param model Modelオブジェクト
	 * @param request リクエスト情報
	 * @return メイン画面
	 * @throws Exception
	 */
	@RequestMapping(method = POST)
	public String login(@ModelAttribute("loginForm") LoginForm loginForm, Model model, HttpServletRequest request) throws Exception {

		// 画面のIDでユーザー情報テーブルを検索
		if (userInfoDao.findUser(loginForm.getUser_id()) == 0 ) {
			model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());
			model.addAttribute("error", MessageConst.W001);
			log.info("@@LoginCtrl.login " + MessageConst.W001 + "[USER_ID=" + loginForm.getUser_id() + "]");

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), "null", loginForm.getUser_id(), CommonConst.LOGINCTRL_LOGIN, CommonConst.FAILED_CO, "ユーザID = " + loginForm.getUser_id(), "該当ユーザが存在しません。");

			return "login";
		}

		// ユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getLoginUser(loginForm.getUser_id());

		// テナント有効期限をチェック
		if (tenantDao.auth(userinfoModel.getTenant_id()) == 0) {
			model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());
			model.addAttribute("error", MessageConst.W001);
			log.info("@@LoginCtrl.login 所属テナントが無効または期限切れです。ログインを制限しています。" + "[USER_ID=" + loginForm.getUser_id() + "]");

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), userinfoModel.getTenant_id(), loginForm.getUser_id(), CommonConst.LOGINCTRL_LOGIN, CommonConst.FAILED_CO, "ユーザID = " + loginForm.getUser_id(), "所属テナントが無効または期限切れです。ログインを制限しています。");

			return "login";
		}

		//テナント情報を取得
		TenantModel tenantModel = tenantDao.findTenant(userinfoModel.getTenant_id());

		if (tenantModel.getPassword_lock_times() > 0) {
			// パスワードエラー回数をチェック
			if (userinfoModel.getPwd_errcnt() >= tenantModel.getPassword_lock_times()) {
				model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());
				model.addAttribute("error", MessageConst.W003);
				log.info("@@LoginCtrl.login " + MessageConst.W003 + "[USER_ID=" + loginForm.getUser_id() + "]");

				//アクセスログ書き込み
				logWriting.accessLogWriting(request.getRemoteAddr(), userinfoModel.getTenant_id(), loginForm.getUser_id(), CommonConst.LOGINCTRL_LOGIN, CommonConst.FAILED_CO, "ユーザID = " + loginForm.getUser_id(), "パスワードエラー回数が上限に達しました。ログインを制限しています。");

				return "login";
			}
		}

		// 画面のパスワードを暗号化
		String scrPass = tripleDesConverter.encrypt64(loginForm.getPassword());

		// パスワードは暗号化しない
//		String scrPass = loginForm.getPassword();

		// 入力されたパスワードが正しいかチェック
		if (!userinfoModel.getPwd().equals(scrPass)) {
			model.addAttribute("systemName", systemInfoDao.findAll().getSystem_name());
			model.addAttribute("error", MessageConst.W001);
			// パスワードエラーカウントを更新
			userInfoDao.updateErrcnt(userinfoModel.getUser_id(), (userinfoModel.getPwd_errcnt() + 1));
			log.info("@@LoginCtrl.login " + MessageConst.W001 + "[USER_ID=" + userinfoModel.getUser_id() + "]");

			//アクセスログ書き込み
			logWriting.accessLogWriting(request.getRemoteAddr(), userinfoModel.getTenant_id(), loginForm.getUser_id(), CommonConst.LOGINCTRL_LOGIN, CommonConst.FAILED_CO, "ユーザID = " + loginForm.getUser_id(), "パスワードが違います。\nパスワードエラー回数:" + (userinfoModel.getPwd_errcnt() + 1));

			return "login";
		}

		// ユーザーID、権限CD、テナントIDをセッションに格納
		HttpSession session = request.getSession(true);
		session.setAttribute(SessionConst.USER_ID, userinfoModel.getUser_id());
		session.setAttribute(SessionConst.AUTH_CODE, userinfoModel.getAuth_cd());
		session.setAttribute(SessionConst.TENANT_ID, userinfoModel.getTenant_id());
		log.info("@@LoginCtrl.login セッションに格納[USER_ID=" + userinfoModel.getUser_id() + "]");

		//アクセスログ書き込み
		logWriting.accessLogWriting(request.getRemoteAddr(), userinfoModel.getTenant_id(), userinfoModel.getUser_id(), CommonConst.LOGINCTRL_LOGIN, CommonConst.SUCCESS_CO, "ログイン処理", "");

		// メイン画面に遷移
		return "redirect:main.html";
	}

}