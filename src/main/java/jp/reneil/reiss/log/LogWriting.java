/*
 * Copyright (C) 2019 Reneil All Rights Reserved.
 *
 * システム名：画像共有システム Reiss/Smiss
 *
 * 履歴：
 *   NO    日付            バージョン      更新者          内容
 *    1    2019/10/01      1.0.0           女屋            新規作成
 *    2    2019/10/02      1.0.1           女屋            アクセスログ書き込みをpublicに変更。TripleDES変換クラス宣言(インスタンス化)変更。
 */
package jp.reneil.reiss.log;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import jp.reneil.reiss.common.CommonConst;
import jp.reneil.reiss.dao.AccessLogDao;
import jp.reneil.reiss.dao.UserInfoDao;
import jp.reneil.reiss.model.AccessLogModel;
import jp.reneil.reiss.model.UserInfoModel;
import jp.reneil.reiss.util.TripleDesConverter;

/**
 * <META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=Shift_JIS">
 * ログ書き込みクラスです。<br>
 *
 * @version 1.0.1
 */
@Controller
public class LogWriting {

	/* ログ制御 */
	private static Log log = LogFactory.getLog(LogWriting.class);

	/** TripleDES変換ユーティリティクラス */
	@Autowired
	private TripleDesConverter tripleDesConverter;

	/** ユーザー情報テーブルアクセス制御クラス */
	@Autowired
	private UserInfoDao userInfoDao;

	/** アクセスログテーブルアクセス制御クラス */
	@Autowired
	private AccessLogDao accessLogDao;

	/**
	 * ログアウトログ書き込み．<br>
	 * @param acc_userID 暗号化アクセスユーザID
	 * @return 処理結果情報
	 * @throws Exception
	 */
	@RequestMapping(value = "logOutWriting", method = RequestMethod.POST, produces="text/html;charset=UTF-8")
	@ResponseBody
	public String logOutWriting(@RequestParam String acc_userID, HttpServletRequest request) throws Exception {

		// セッション情報をクリアする
		HttpSession session = request.getSession(false);
		if (session != null){
			//セッションを破棄する
			session.invalidate();
		}

		//ユーザーID複合化
		String conv_acc_userID = tripleDesConverter.decrypt64(acc_userID);

		log.info("@@AjaxLogWriting.logOutWriting ログアウト処理 acc_userID=" + conv_acc_userID);

		//ログインユーザーIDからユーザー情報を取得
		UserInfoModel userinfoModel = userInfoDao.getUser(conv_acc_userID);

		if (userInfoDao.findUser(conv_acc_userID) == 1) {
			//アクセスログ書き込み
			accessLogWriting(request.getRemoteAddr(), userinfoModel.getTenant_id(), conv_acc_userID, CommonConst.LOGWRITING_LOGOUTWRITING, CommonConst.SUCCESS_CO, "ログアウト処理", "");
		}

		return "true";
    }

	/**
	 * システム管理ログアウトログ書き込み．<br>
	 * @param acc_userID 暗号化アクセスユーザID
	 * @return 処理結果情報
	 * @throws Exception
	 */
	@RequestMapping(value = "sysLogOutWriting", method = RequestMethod.POST, produces="text/html;charset=UTF-8")
	@ResponseBody
	public String sysLogOutWriting(@RequestParam String acc_userID, HttpServletRequest request) throws Exception {

		// セッション情報をクリアする
		HttpSession session = request.getSession(false);
		if (session != null){
			//セッションを破棄する
			session.invalidate();
		}

		//ユーザーID複合化
		String conv_acc_userID = tripleDesConverter.decrypt64(acc_userID);

		log.info("@@AjaxLogWriting.sysLogOutWriting システム管理ログアウト処理 acc_userID=" + conv_acc_userID);

		if (userInfoDao.findUser(conv_acc_userID) == 1) {
			//アクセスログ書き込み
			accessLogWriting(request.getRemoteAddr(), "null", conv_acc_userID, CommonConst.LOGWRITING_SYSLOGOUTWRITING, CommonConst.SUCCESS_CO, "システム管理ログアウト処理", "");
		}

		return "true";
    }

	/**
	 * アクセスログ書き込み.<br>
	 * @param access_ip アクセスIP
	 * @param tenant_id テナントID
	 * @param user_id ユーザID
	 * @param prg_id 実行プログラムID
	 * @param result_id 処理結果ID
	 * @param contents 処理寧陽
	 * @param err_msg エラーメッセージ
	 */
	public void accessLogWriting(String access_ip, String tenant_id, String user_id, String prg_id, String result_id, String contents, String err_msg) throws Exception {

		//アクセスログ準備
		AccessLogModel accessLogModel = new AccessLogModel();
		accessLogModel.setAccess_date(new Date());
		accessLogModel.setAccess_ip(access_ip);
		accessLogModel.setAccess_tenant_id(tenant_id);
		if (user_id.length() > 32) {
			accessLogModel.setAccess_user_id(user_id.substring(0, 32));
		}else {
			accessLogModel.setAccess_user_id(user_id);
		}
		accessLogModel.setPrg_id(prg_id);
		accessLogModel.setResult_id(result_id);
		accessLogModel.setContents(contents);
		accessLogModel.setErr_msg(err_msg);

		//アクセスログ登録
		accessLogDao.insert(accessLogModel);

	}

}